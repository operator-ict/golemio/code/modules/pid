# Implementační dokumentace modulu _pid_

## Záměr

Modul slouží k ukládání a poskytování informací o jízdních řádech a real-time polohových datech v Praze a Středočeském kraji.

## Vstupní data

### Data aktivně stahujeme

**Dostupní poskytovatelé**:

-   ROPID FTP
    -   GTFS (General Transit Feed Specification) jízdní rády
    -   CIS (Celostátní informační systém) zastávkový číselník
    -   OIS (Odbavovací a informační systém) mapovací číselník pro tramvaje
    -   Oběhy vozidel
    -   Presets (definice pro zkracovač URL)
-   ROPID VYMI (Výluky a mimořádnosti)
-   Seznam Autobusu API (informace o vozidlech, např. dostupnost klimatizace nebo USB zásuvek)
-   Statická data
    -   Ukazatelé směru jízdy
    -   Kolejové obvody pro metro
-   Grafana Loki
    -   Request logy z URL proxy pro monitorovací centrum od ROPIDU
-   JIS Infotexty

#### _ROPID FTP (obecně)_

-   přístupy
    -   host: config.datasources.RopidFTP.host
    -   user: config.datasources.RopidFTP.user
    -   password: config.datasources.RopidFTP.password
    -   secure: config.datasources.RopidFTP.secure

#### _GTFS jízdní řády_

-   identifikátor: PID_GTFS
-   zdroj dat
    -   cesta: /GTFS (config.datasources.RopidGTFSPath)
    -   soubory:
        -   PID_GTFS.zip (config.datasources.RopidGTFSFilename) -> agency.txt, calendar.txt, calendar_dates.txt, shapes.txt, stop_times.txt, stops.txt, routes.txt, route_sub_agencies.txt, trips.txt
-   formát dat
    -   protokol: ftp
    -   datový typ: csv
    -   validační schéma: není, mapování rovnou do tabulek, viz [Dto jsonSchema](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/blob/development/src/schema-definitions/ropid-gtfs/models)
    -   příklad vstupnich dat https://gitlab.com/operator-ict/golemio/code/modules/pid/-/snippets/2462385
-   frekvence stahování
    -   cron definice:
        -   cron.vehicle-positions.ropidgtfs.checkForNewData
            -   rabin `0 35 5,12 * * *`
            -   prod `0 20 5 * * *`, `0 50 12 * * *`
-   názvy rabbitmq front
    -   vehicle-positions.ropidgtfs.checkForNewData
    -   vehicle-positions.ropidgtfs.downloadDatasets se zprávou `["PID_GTFS"]`

#### _CIS zastávkový číselník_

-   identifikátor: CIS_STOPS
-   zdroj dat
    -   cesta: /PUBLIC/ZASTAVKY/JSON (config.datasources.RopidGTFSCisStopsPath)
    -   soubory:
        -   Stops.Min.json (config.datasources.RopidGTFSCisStopsFilename)
-   formát dat
    -   protokol: ftp
    -   datový typ: json
    -   validační schéma: [datasourceJsonSchema](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/blob/development/src/schema-definitions/ropid-gtfs/RopidGTFSCisStops.ts#L31)
    -   příklad vstupnich dat https://gitlab.com/operator-ict/golemio/code/modules/pid/-/snippets/2462390
-   frekvence stahování
    -   cron definice:
        -   viz GTFS jízdní řády
-   názvy rabbitmq front
    -   vehicle-positions.ropidgtfs.checkForNewData
    -   vehicle-positions.ropidgtfs.downloadDatasets se zprávou `["CIS_STOPS"]`

#### _OIS mapovací číselník pro tramvaje_

_:warning: Původním záměrem bylo využití OIS číselníku během zpracování poloh tramvají z MPVNETU. Momentálně zůstavá nevyužit, v budoucnu bude smazán_

-   identifikátor: OIS_MAPPING
-   zdroj dat
    -   cesta: / (config.datasources.RopidGTFSOisPath)
    -   soubory:
        -   oisMapping.json (config.datasources.RopidGTFSOisFilename)
-   formát dat
    -   protokol: ftp
    -   datový typ: json
    -   validační schéma: [datasourceJsonSchema](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/blob/development/src/schema-definitions/ropid-gtfs/RopidGTFSOisMapping.ts#L13)
    -   příklad vstupnich dat
    ```json
    [
        {
            "ois": 5067,
            "node": 67,
            "name": "Čechovo náměstí"
        }
    ]
    ```
-   frekvence stahování
    -   cron definice:
        -   viz GTFS jízdní řády
-   názvy rabbitmq front
    -   vehicle-positions.ropidgtfs.checkForNewData
    -   vehicle-positions.ropidgtfs.downloadDatasets se zprávou `["OIS_MAPPING"]`

#### _Oběhy vozidel_

-   identifikátor: RUN_NUMBERS
-   zdroj dat
    -   cesta: / (config.datasources.RopidGTFSRunNumbersPath)
    -   soubory:
        -   obehy.csv (config.datasources.RopidGTFSRunNumbersFilename)
-   formát dat
    -   protokol: ftp
    -   datový typ: csv
    -   validační schéma: [datasourceJsonSchema](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/blob/development/src/schema-definitions/ropid-gtfs/RopidGTFSRunNumbers.ts#L19)
    -   příklad vstupnich dat
    ```csv
    route_id,run_number,service_id,trip_id,vehicle_type
    L991,1,1111100-1,991_1342_220901,3
    L991,1,1111100-1,991_1343_220901,3
    ```
-   frekvence stahování
    -   cron definice:
        -   viz GTFS jízdní řády
-   názvy rabbitmq front
    -   vehicle-positions.ropidgtfs.checkForNewData
    -   vehicle-positions.ropidgtfs.downloadDatasets se zprávou `["RUN_NUMBERS"]`

#### _Presets definice pro zkracovač URL_

-   identifikátor: DEPARTURES_PRESETS
-   zdroj dat
    -   cesta: / (config.datasources.RopidDeparturesPresetsPath)
    -   soubory:
        -   presets.json (config.datasources.RopidDeparturesPresetsFilename)
-   formát dat
    -   protokol: ftp
    -   datový typ: json
    -   validační schéma: [outputPresetsJsonSchema](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/blob/development/src/schema-definitions/ropid-departures-presets/RopidDeparturesPresetsOutputSchemas.ts)
    -   příklad vstupnich dat: [ropiddeparturespresets-data.json](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/blob/development/test/integration-engine/ropid-gtfs/data/ropiddeparturespresets-data.json)
-   frekvence stahování
    -   cron definice:
        -   cron.vehicle-positions.ropidpresets.checkForNewDeparturesPresets
            -   rabin `15 */4 * * * *`
            -   prod `15 */4 * * * *`
-   názvy rabbitmq front
    -   vehicle-positions.ropidpresets.checkForNewDeparturesPresets
    -   vehicle-positions.ropidpresets.downloadDeparturesPresets

#### _VYMI výluky a mimořádnosti_

-   zdroj dat
    -   url: https://vymipid.ropid.cz/ws/list_publ (config.datasources.RopidVYMIApiUrl)
    -   parametry dotazu: config.datasources.RopidVYMIApiHeaders
-   formát dat
    -   protokol: http
    -   datový typ: json
    -   validační schéma: [datasourceJsonSchema](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/blob/development/src/schema-definitions/ropid-vymi/index.ts#L62)
    -   příklad vstupnich dat: [ropidvymi-datasource.json](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/blob/development/test/integration-engine/ropid-vymi/data/ropidvymi-datasource.json)
-   frekvence stahování
    -   cron definice:
        -   cron.vehicle-positions.ropidvymi.checkForNewEvents
            -   rabin `*/30 * * * * *`
            -   prod `*/30 * * * * *`
-   názvy rabbitmq front
    -   vehicle-positions.ropidvymi.checkForNewEvents

#### _Seznam Autobusu API_

-   zdroj dat
    -   url: config.datasources.SeznamAutobusuApiUrl
    -   parametry dotazu: config.datasources.SeznamAutobusuApiHeaders
-   formát dat
    -   protokol: http
    -   datový typ: json
    -   validační schéma: [seznamAutobusuJsonSchema](../src/schema-definitions/vehicle-descriptors/datasources/SeznamAutobusuJsonSchema.ts)
    -   příklad vstupnich dat: [InputDataFixture](../test/integration-engine/vehicle-positions/workers/vehicle-descriptors/fixtures/InputDataFixture.ts)
-   frekvence stahování
    -   cron definice:
        -   cron.vehicle-positions.vehicledescriptors.refreshDescriptors
            -   rabin `0 15 5,12 * * *`
            -   prod `0 10 5,12 * * *`
-   názvy rabbitmq front
    -   vehicle-positions.vehicledescriptors.refreshDescriptors

#### _Ukazatelé směru jízdy_

-   zdroj dat
    -   pomocná statická data
    -   data nahrávány přímo do tabulky https://gitlab.com/operator-ict/golemio/code/modules/pid/-/snippets/2462793
-   formát dat
    -   datový typ: sql
    -   validační schéma: [outputDeparturesDirectionsJsonSchema](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/blob/development/src/schema-definitions/ropid-departures-directions/RopidDeparturesDirectionsOutputSchemas.ts#L1)
    -   příklad vstupnich dat: viz zdroj dat
-   frekvence stahování
    -   manuální refresh dat
-   název rabbitmq fronty
    -   neexistuje

#### _Grafana Loki_

-   zdroj dat
    -   url: config.datasources.GrafanaLokiApiUrl
    -   parametry dotazu: config.datasources.GrafanaLokiApiHeaders
-   formát dat
    -   protokol: http
    -   datový typ: json
    -   validační schéma: [grafanaLokiJsonSchema](../src/schema-definitions/ropid-departures-preset-logs/datasources/GrafanaLokiJsonSchema.ts)
    -   příklad vstupnich dat: [InputLogsFixture](../test/integration-engine/ropid-gtfs/workers/presets/fixtures/InputLogsFixture.ts)
-   frekvence stahování
    -   cron definice:
        -   cron.vehicle-positions.ropidpresets.collectAndSaveLogs
            -   rabin `0 */1 * * * *`
            -   prod `0 */1 * * * *`
-   názvy rabbitmq front
    -   vehicle-positions.ropidpresets.collectAndSaveLogs se zprávou `{ "targetMinutes": n }`
### _Ropid staticka data_

-   zdroj dat
    -   baseUrl: module.pid.staticData.baseUrl
    -   departure directions url: module.pid.staticData.departuresDirection.path
    -   metro rail track url: module.pid.staticData.metroRailTracks.path
-   formát dat
    -   protokol: http
    -   datový typ: csv
    -   validační schéma:
        -   [MetroRailTrack](../src/schema-definitions/datasources/static-data/MetroRailTrackJsonSchema.ts)
        -   [DeparturesDirection](../src/schema-definitions/datasources/static-data/DeparturesDirectionsJsonSchema.ts)
    -   příklad vstupnich dat: viz zdroj data
-   frekvence stahování
    -   Po nacteni jizdnich radu (fronta: vehicle-positions.ropidgtfs.checkForNewData)
-   názvy rabbitmq front
    -    vehicle-positions.ropidgtfs.saveStaticData

### Data nám jsou posílána

**Dostupní poskytovatelé**:

-   MPVNET (Monitorování provozu vozidel) - ROPID + CHAPS
    -   autobusy (mimo DPP), vlaky, přívozy, lanovky
-   TCP zprávy o obězích vozidel - DPP + CHAPS
    -   autobusy a trolejbusy, tramvaje
    -   metro
-   TCP zprávy o obězích vozidel - TELMAX
    -   autobusy (ARRIVA City)

#### _Vehicle Positions_

-   formát dat
    -   protokol: http
    -   datový typ: xml
    -   validační schéma: pro parsovaný json [SaveDataToDBSchema](../src/integration-engine/vehicle-positions/workers/vehicle-positions/schema/SaveDataToDBSchema.ts)
    -   příklad vstupních dat
    ```xml
    <?xml version="1.0" encoding="UTF-8"?>
    <m disp="http://77.93.194.81:8716/api">
        <spoj lin="999999" alias="155" spoj="1" t="3" sled="2" np="true" zrus="false" lat="50.08323" lng="14.51035" cpoz="11:09:06" po="1" zast="56699" zpoz_prij="426" zpoz_odj="426">
            <zast zast="57517" stan="C" prij="" odj="11:00" zpoz_typ="3" zpoz_prij="311" zpoz_odj="311"></zast>
        </spoj>
    </m>
    ```
-   endpoint v input-gateway
    -   openapi specifikace [/vehiclepositions](https://api.golemio.cz/v2/pid/input/docs/openapi/#/%F0%9F%9B%A4%20Vehicle%20Positions/post_vehiclepositions)
-   nastavení práv v permission-proxy
    -   endpoint `/input-gateway/v1/vehiclepositions/spoje`
    -   přeposíláno z [produkční data proxy](https://gitlab.com/operator-ict/golemio/devops/infrastructure/-/blob/master/cluster_production/golemio_data-proxy-values.yaml#L65)
-   transformace raw dat
    -   zprávy jsou xml body parserem převedeny do jsonu a přeposlány do rabbitmq fronty
-   název rabbitmq fronty
    -   vehicle-positions.vehiclepositions.saveDataToDB
-   ukládání raw dat na blob storage
    -   název kontejneru: {rabin,golem}-input-gateway
    -   cesta: /vehiclepositions/yyyy-MM-dd/HH_mm_ss.SSS.xml
    -   agregace dat: není
-   odhadovaná zátěž
    -   jednotky až desítky za minutu

#### _Vehicle Positions Common Runs_

-   formát dat
    -   protokol: tcp
    -   datový typ: xml
    -   validační schéma: základní kontrola xml v [parserech](https://gitlab.com/operator-ict/golemio/code/tcp-input-gateway/-/tree/development/src/receivers/parsers), validační schéma pro parsovaný json [CommonRunsSchema](../src/integration-engine/vehicle-positions/workers/runs/schema/CommonRunsSchema.ts)
    -   příklad vstupnich dat (autobus)
    ```xml
    <M>
        <V turnus="134/3" line="134" evc="3738" np="ano" lat="50.03952" lng="14.42919" akt="07960001" takt="2022-06-29T17:28:13" konc="01100005" tjr="2022-06-29T17:23:00" pkt="12709236" tm="2022-06-29T17:28:22" events="O" />
    </M>
    ```
    -   příklad vstupnich dat (trolejbus)
    ```xml
    <M>
        <V turnus="58/1" line="58" evc="0509" lat="50.11614" lng="14.49470" akt="07540001" takt="2023-06-03T08:16:19" konc="03840001" tjr="2023-06-03T08:16:00" pkt="733775" tm="2023-06-03T08:16:18" events="O" />
    </M>
    ```
-   adresa: api.golemio.cz:3003 (tramvaje) a api.golemio.cz:3004 (autobusy)
    -   dokumentace tcp receiverů: [transport input gateway#dpp-trams](https://gitlab.com/operator-ict/golemio/code/tcp-input-gateway#dpp-trams), [transport input gateway#dpp-busses](https://gitlab.com/operator-ict/golemio/code/tcp-input-gateway#dpp-busses)
-   řízení přístupů řešeno na síťové úrovni mimo permission proxy
    -   přeposíláno z [produkční data proxy](https://gitlab.com/operator-ict/golemio/devops/infrastructure/-/blob/master/cluster_production/golemio_data-proxy-values.yaml#L35)
-   transformace raw dat
    -   úplné zprávy jsou knihovnou `xml2js` převedeny do jsonu a přeposlány do rabbitmq fronty
    -   implementace tram/bus receiverů je totožná, pouze se zprávy přeposílají do jiných front a ukládají se do jiné složky na blob storage
-   název rabbitmq front
    -   vehicle-positions.vehiclepositions.saveTramRunsToDB
    -   vehicle-positions.vehiclepositions.saveBusRunsToDB
-   ukládání raw dat na blob storage
    -   název kontejneru: {rabin,golem}-input-gateway
    -   cesta: /tcp-dpp-{tram,bus}-data/yyyy-MM-dd/HH_mm_ss.SSS.xml
    -   agregace dat: 1 MB
-   odhadovaná zátěž
    -   tramvaje - desítky zpráv za minutu
    -   autobusy - jednotky až desítky zpráv za sekundu

#### _Vehicle Positions Metro Runs_

-   formát dat
    -   protokol: tcp
    -   datový typ: xml
    -   validační schéma: základní kontrola xml v [parseru](https://gitlab.com/operator-ict/golemio/code/tcp-input-gateway/-/blob/development/src/receivers/parsers/dpp-metro.parser.ts), validační schéma pro parsovaný json [MetroRunsSchema](../src/integration-engine/vehicle-positions/workers/runs/schema/MetroRunsSchema.ts)
    -   příklad vstupnich dat
    ```xml
    <m linka="A" tm="2022-07-20T13:45:34Z" gvd="GD20a">
        <vlak csp=" 3" csr=" 3" cv="279" ko="1809" odch="25" />
    </m>
    ```
-   adresa: tcp.golemio.cz:3006
    -   dokumentace tcp receiveru: [transport input gateway#dpp-metro](https://gitlab.com/operator-ict/golemio/code/tcp-input-gateway#dpp-metro)
-   řízení přístupů řešeno na síťové úrovni mimo permission proxy
    -   přeposíláno z [produkční data proxy](https://gitlab.com/operator-ict/golemio/devops/infrastructure/-/blob/master/cluster_production/golemio_data-proxy-values.yaml#L55)
-   transformace raw dat
    -   úplné zprávy jsou knihovnou `xml2js` převedeny do jsonu a přeposlány do rabbitmq fronty
-   název rabbitmq front
    -   vehicle-positions.vehiclepositionsruns.saveMetroRunsToDB
-   ukládání raw dat na blob storage
    -   název kontejneru: {rabin,golem}-input-gateway
    -   cesta: /tcp-dpp-metro-data/yyyy-MM-dd/HH_mm_ss.SSS.xml
    -   agregace dat: 1 MB
-   odhadovaná zátěž
    -   desítky až stovky zpráv za minutu

#### _Vehicle Positions ARRIVA City Bus Runs_

-   formát dat
    -   protokol: tcp
    -   datový typ: xml
    -   validační schéma: základní kontrola xml v [parseru](https://gitlab.com/operator-ict/golemio/code/tcp-input-gateway/-/blob/development/src/receivers/parsers/regional-bus.parser.ts), validační schéma pro parsovaný json [RegionalBusRunsSchema](../src/integration-engine/vehicle-positions/workers/runs/schema/RegionalBusRunsSchema.ts)
    -   příklad vstupnich dat
    ```xml
    <M>
        <V imei="867377023812625" rz="6AB8491" pkt="3107" lat="50.00201" lng="14.4054" tm="2023-07-02T23:59:44" events="R" rych="11" smer="149" />
    </M>
    ```
-   adresa: tcp.golemio.cz:3008 (TELMAX)
    -   dokumentace tcp receiveru: [transport input gateway#arriva-city-bus-data](https://gitlab.com/operator-ict/golemio/code/tcp-input-gateway/-/tree/development#arriva-city-bus-data)
-   řízení přístupů řešeno na síťové úrovni mimo permission proxy
    -   přeposíláno z [produkční data proxy](https://gitlab.com/operator-ict/security/golemio-golem/-/blob/master/data-proxy/values-secret.yaml)
-   transformace raw dat
    -   úplné zprávy jsou knihovnou `xml2js` převedeny do jsonu a přeposlány do rabbitmq fronty
-   název rabbitmq front
    -   vehicle-positions.vehiclepositionsruns.saveArrivaCityRunsToDB
-   ukládání raw dat na blob storage
    -   název kontejneru: {rabin,golem}-input-gateway
    -   cesta: /tcp-arriva-city-data/yyyy-MM-dd/HH_mm_ss.SSS.xml
    -   agregace dat: 1 MB
-   odhadovaná zátěž
    -   desítky až stovky zpráv za minutu
#### _JIS Infotexty_

-   formát dat
    -   protokol: http
    -   datový typ: json
    -   validační schéma: [JISInfotextsJsonSchema](../src/schema-definitions/jis/datasources/JISInfotextsJsonSchema.ts)
    -   příklad vstupnich dat: [infotextsDataSourceFixture](../test/integration-engine/jis/fixtures/infotextsDataSourceFixture.ts)
-   název rabbitmq fronty
    -   vehicle-positions.jis.refreshJISInfotexts

## Výstupní data

### Data aktivně posíláme na push endpoint

#### _ROPID monitorovací centrum_

-   cíl dat
    -   url: config.datasources.RopidMonitoringPushUrl
    -   parametry dotazu: config.datasources.RopidMonitoringPushHeaders
-   formát dat
    -   protokol: http
    -   datový typ: json
    -   příklad dat
    ```json
    [
        {
            "deviceAlias": "test-povstani",
            "receivedAt": "2023-06-27T14:39:58Z"
        }
    ]
    ```
-   frekvence stahování
    -   cron definice:
        -   cron.vehicle-positions.ropidpresets.collectAndSaveLogs
            -   rabin `0 */1 * * * *`
            -   prod `0 */1 * * * *`
-   názvy rabbitmq front
    -   vehicle-positions.ropidpresets.collectAndSaveLogs se zprávou `{ "targetMinutes": n }`

## Zpracování dat / transformace

Všechny tabulky se nachází ve schématu `pid`

### TimetableWorker

#### task _CheckForNewData_

-   vstupní rabbitmq fronta
    -   název: vehicle-positions.ropidgtfs.checkForNewData
    -   TTL: 19 minut
    -   parametry: `{ forceRefresh }`
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   název: vehicle-positions.ropidgtfs.downloadDatasets
-   datové zdroje
    -   ROPID FTP
-   transformace
    -   žádné
-   data modely
    -   RopidGTFSMetadataModel `ropidgtfs_metadata`

#### task _DownloadDatasets_

-   vstupní rabbitmq fronta
    -   název: vehicle-positions.ropidgtfs.downloadDatasets
    -   TTL: 59 minut
    -   parametry: `["PID_GTFS", "CIS_STOPS", "OIS_MAPPING", "RUN_NUMBERS"]`
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   název: vehicle-positions.ropidgtfs.transformAndSaveData
    -   název: vehicle-positions.ropidgtfs.checkSavedRowsAndReplaceTables
-   datové zdroje
    -   ROPID FTP
-   transformace
    -   žádné
-   data modely
    -   RopidGTFSMetadataModel `ropidgtfs_metadata`

#### task _TransformAndSaveData_

-   vstupní rabbitmq fronta
    -   název: vehicle-positions.ropidgtfs.transformAndSaveData
    -   TTL: 23 hodin
    -   parametry: `{ filepath, name, mtime, type, dataset }`
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   žádné
-   datové zdroje
    -   ROPID FTP
-   transformace
    -   [RopidGTFSTransformation](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/blob/development/src/integration-engine/ropid-gtfs/transformations/RopidGTFSTransformation.ts)
    -   [RopidGTFSCisStopsTransformation](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/blob/development/src/integration-engine/ropid-gtfs/transformations/RopidGTFSCisStopsTransformation.ts)
    -   [RopidGTFSOisMappingTransformation](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/blob/development/src/integration-engine/ropid-gtfs/transformations/RopidGTFSOisTransformation.ts)
    -   [RopidGTFSRunNumbersTransformation](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/blob/development/src/integration-engine/ropid-gtfs/transformations/RopidGTFSRunNumbersTransformation.ts)
    -   [RopidGtfsRouteSubAgencyTransformation](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/blob/development/src/integration-engine/ropid-gtfs/transformations/RopidGtfsRouteSubAgencyTransformation.ts)
        -   on top of transformation functionality it also removes duplicates. Rule: If there are multiple rows with same `route_id` and `sub_agency_id` only the first with filled licence number is kept.
-   data modely
    -   RopidGTFSMetadataModel `ropidgtfs_metadata`
    -   RopidGTFSAgencyModel `ropidgtfs_agency`
    -   RopidGTFSCalendarModel `ropidgtfs_calendar`
    -   RopidGTFSCalendarDatesModel `ropidgtfs_calendar_dates`
    -   RopidGTFSRoutesModel `ropidgtfs_routes`
    -   RouteSubAgencyDto `ropidgtfs_route_sub_agencies`
    -   RopidGTFSShapesModel `ropidgtfs_shapes`
    -   RopidGTFSStopTimesModel `ropidgtfs_stop_times`
    -   RopidGTFSStopsModel `ropidgtfs_stops`
    -   RopidGTFSTripsModel `ropidgtfs_trips`
    -   RopidGTFSCisStopsModel `ropidgtfs_cis_stops`
    -   RopidGTFSCisStopGroupsModel `ropidgtfs_cis_stop_groups`
    -   RopidGTFSOisModel `ropidgtfs_ois`
    -   RopidGTFSRunNumbersModel `ropidgtfs_run_numbers`

#### task _CheckSavedRowsAndReplaceTables_

-   vstupní rabbitmq fronta
    -   název: vehicle-positions.ropidgtfs.checkSavedRowsAndReplaceTables
    -   TTL: 23 hodin
    -   parametry: `["PID_GTFS", "CIS_STOPS", "OIS_MAPPING", "RUN_NUMBERS"]`
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   název: vehicle-positions.ropidgtfs.downloadDatasets
    -   název: vehicle-positions.ropidgtfs.refreshPrecomputedTables
    -   název: vehicle-positions.ropidgtfs.refreshPublicGtfsDepartureCache
    -   název: vehicle-positions.vehiclepositions.refreshGTFSTripData
-   datové zdroje
    -   ROPID FTP
-   transformace
    -   žádné
-   data modely
    -   RopidGTFSMetadataModel `ropidgtfs_metadata`
    -   RopidGTFSAgencyModel `ropidgtfs_agency`
    -   RopidGTFSCalendarModel `ropidgtfs_calendar`
    -   RopidGTFSCalendarDatesModel `ropidgtfs_calendar_dates`
    -   RouteSubAgencyDto `ropidgtfs_route_sub_agencies`
    -   RopidGTFSRoutesModel `ropidgtfs_routes`
    -   RopidGTFSShapesModel `ropidgtfs_shapes`
    -   RopidGTFSStopTimesModel `ropidgtfs_stop_times`
    -   RopidGTFSStopsModel `ropidgtfs_stops`
    -   RopidGTFSTripsModel `ropidgtfs_trips`
    -   RopidGTFSCisStopsModel `ropidgtfs_cis_stops`
    -   RopidGTFSCisStopGroupsModel `ropidgtfs_cis_stop_groups`
    -   RopidGTFSOisModel `ropidgtfs_ois`
    -   RopidGTFSRunNumbersModel `ropidgtfs_run_numbers`
    -   RopidGTFSPrecomputedServicesCalendarModel `ropidgtfs_precomputed_services_calendar`
    -   RopidGTFSPrecomputedMinMaxStopSequencesModel `ropidgtfs_precomputed_minmax_stop_sequences`
    -   RopidGTFSPrecomputedDeparturesModel `ropidgtfs_precomputed_departures`
    -   RopidGTFSPrecomputedTripScheduleModel `ropidgtfs_precomputed_trip_schedule`

#### task _RefreshPrecomputedTables_

-   vstupní rabbitmq fronta
    -   název: vehicle-positions.ropidgtfs.refreshPrecomputedTables
    -   TTL: 59 minut
    -   parametry: žádné
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   žádné
-   datové zdroje
    -   ROPID FTP
-   transformace
    -   žádné
-   data modely
    -   RopidGTFSMetadataModel `ropidgtfs_metadata`
    -   RopidGTFSPrecomputedServicesCalendarModel `ropidgtfs_precomputed_services_calendar`
    -   RopidGTFSPrecomputedMinMaxStopSequencesModel `ropidgtfs_precomputed_minmax_stop_sequences`
    -   RopidGTFSPrecomputedDeparturesModel `ropidgtfs_precomputed_departures`
    -   RopidGTFSPrecomputedTripScheduleModel `ropidgtfs_precomputed_trip_schedule`

#### task _RefreshPublicGtfsDepartureCacheTask_

Task se stará o aktualizaci cache pro public odjezdy. Pouští se pravidelně cronem a po dokončení načtení nových JŘ.

-  vstupní rabbitmq fronta
    -   název: vehicle-positions.ropidgtfs.refreshPublicGtfsDepartureCache
    -   TTL: 59 minut
    -   parametry: `{intervalFromHours, intervalToHours}`
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   žádné
-   datové zdroje
    -   ROPID FTP
-   transformace
    -   [PublicDepartureCacheTransformation](../src/integration-engine/ropid-gtfs/transformations/PublicDepartureCacheTransformation.ts)
-   data modely
    -   RopidGTFSPrecomputedDeparturesModel `ropidgtfs_precomputed_departures`

#### task _SaveStaticData_

Task se stará o aktualizaci statickych dat. Pouští se po dokončení načtení nových JŘ.

-  vstupní rabbitmq fronta
    -   název: vehicle-positions.ropidgtfs.saveStaticData
    -   TTL: 5 minut
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   žádné
-   datové zdroje
    -   Blob storage
-   transformace
    -   [DeparturesDirectionTransformation](../src/integration-engine/ropid-gtfs/workers/timetables/tasks/transformations/DeparturesDirectionTransformation.ts)
    -   [MetroRailtrackDataTransformation](../src/integration-engine/ropid-gtfs/workers/timetables/tasks/transformations/MetroRailtrackDataTransformation.ts)
-   data modely
    -   RopidDeparturesDirections `ropid_departures_directions`
    -   RopidGtfsMetroRailtrackGps `ropidgtfs_metro_railtrack_gps`



### PresetWorker

#### _Task ~ SavePresetsDataTask_

-   vstupní rabbitmq fronta
    -   název: vehicle-positions.ropidpresets.savePresets
    -   TTL: 5 minut
    -   tělo zprávy: obsahuje příchozí json presetů
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   žádné
-   datové zdroje
    -   IG /ropidgtfs/presets
-   transformace
    -   [RopidDeparturesPresetsTransformation](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/blob/development/src/integration-engine/ropid-gtfs/transformations/RopidDeparturesPresetsTransformation.ts)
-   data modely
    -   RopidGTFSMetadataModel `ropidgtfs_metadata`,
    -   RopidDeparturesPresetsModel `ropid_departures_presets`

#### _task: CollectAndSaveLogsTask_

-   vstupní rabbitmq fronta
    -   název: vehicle-positions.ropidpresets.collectAndSaveLogs
    -   TTL: 59 sekund
    -   parametry: `{ targetMinutes }`
    -   validační schéma: [LogCollectionSchema](../src/integration-engine/ropid-gtfs/workers/presets/schema/LogCollectionSchema.ts)
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   název: vehicle-positions.ropidpresets.processAndSendLogs
-   datové zdroje
    -   Loki
-   transformace
    -   [PresetLogTransformation](../src/integration-engine/ropid-gtfs/workers/presets/transformations/PresetLogTransformation.ts)
-   data modely
    -   PresetLogModel `ropid_departures_preset_logs`

#### _task: ProcessAndSendLogsTask_

-   vstupní rabbitmq fronta
    -   název: vehicle-positions.ropidpresets.processAndSendLogs
    -   TTL: 59 sekund
    -   parametry: žádné
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   žádné
-   datové zdroje
    -   Loki
-   transformace
    -   žádné
-   data modely
    -   RopidDeparturesPresetsModel `ropid_departures_presets`
    -   PresetLogModel `ropid_departures_preset_logs`

#### _task: CheckForNewDeparturesPresetsTask_

-   vstupní rabbitmq fronta
    -   název: vehicle-positions.ropidpresets.checkForNewDeparturesPresets
    -   TTL: 4 minuty
    -   parametry: žádné
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   název: vehicle-positions.ropidpresets.downloadDeparturesPresets
-   datové zdroje
    -   ROPID FTP
-   transformace
    -   žádné
-   data modely
    -   RopidGTFSMetadataModel `ropidgtfs_metadata`

#### _DownloadDeparturesPresetsTask_

-   vstupní rabbitmq fronta
    -   název: vehicle-positions.ropidpresets.downloadDeparturesPresets
    -   TTL: 4 minuty
    -   parametry: žádné
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   žádné
-   datové zdroje
    -   ROPID FTP
-   transformace
    -   [RopidDeparturesPresetsTransformation](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/blob/development/src/integration-engine/ropid-gtfs/transformations/RopidDeparturesPresetsTransformation.ts)
-   data modely
    -   RopidGTFSMetadataModel `ropidgtfs_metadata`,
    -   RopidDeparturesPresetsModel `ropid_departures_presets`

### _RopidVYMIWorker_

Worker má na starost přípravu podkladových dat pro infotexty pro odjezdové tabule a GTFS real-time alerty

#### _task: CheckForNewEventsTask_

-   vstupní rabbitmq fronta
    -   název: vehicle-positions.ropidvymi.checkForNewEvents
    -   TTL: 1 minuta
    -   parametry: žádné
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   název: vehicle-positions.ropidvymi.fetchAndProcessEvents
-   datové zdroje
    -   ROPID VYMI
-   transformace
    -   žádné
-   data modely
    -   RopidVYMIMetadataModel `ropidvymi_metadata`

#### _task: FetchAndProcessEventsTask_

-   vstupní rabbitmq fronta
    -   název: vehicle-positions.ropidvymi.fetchAndProcessEvents
    -   TTL: 1 minuta
    -   parametry: `{ digest, data }`
    -   validační schéma: [EventFetchOutputValidationSchema](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/blob/development/src/integration-engine/ropid-vymi/workers/tasks/schema/EventFetchOutputValidationSchema.ts)
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   žádné
-   datové zdroje
    -   ROPID VYMI
-   transformace
    -   [RopidVYMIEventsTransformation](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/blob/development/src/integration-engine/ropid-vymi/RopidVYMIEventsTransformation.ts)
-   data modely
    -   RopidVYMIMetadataModel `ropidvymi_metadata`
    -   RopidVYMIEventsModel `ropidvymi_events`
    -   RopidVYMIEventsRoutesModel `ropidvymi_events_routes`
    -   RopidVYMIEventsStopsModel `ropidvymi_events_stops`

### _VPWorker_

Worker má na starost zpracování a transformaci polohových dat, výpočet zpoždění a přípravu GTFS real-time výstupních dat

#### _task: SaveDataToDBTask_

-   vstupní rabbitmq fronta
    -   název: vehicle-positions.vehiclepositions.saveDataToDB
    -   TTL: 5 minut
    -   parametry: `{ $, zast }`
    -   validační schéma: [SaveDataToDBSchema](../src/integration-engine/vehicle-positions/workers/vehicle-positions/schema/SaveDataToDBSchema.ts)
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   název: vehicle-positions.vehiclepositions.updateGTFSTripId
-   datové zdroje
    -   MPVNET
-   transformace
    -   [PositionsTransformation](../src/integration-engine//vehicle-positions//workers//vehicle-positions/transformations/PositionsTransformation.ts)
-   data modely
    -   VPTripsModel `vehiclepositions_trips`
    -   CisStopModel `vehiclepositions_cis_stops`

#### _task: UpdateGtfsTripIdTask_

-   vstupní rabbitmq fronta
    -   název: vehicle-positions.vehiclepositions.updateGTFSTripId
    -   TTL: 5 minut
    -   parametry: `{ trips, positions }`
    -   validační schéma: [UpdateGtfsTripIdSchema](../src/integration-engine/vehicle-positions/workers/vehicle-positions/schema/UpdateGtfsTripIdSchema.ts)
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   název: vehicle-positions.vehiclepositions.updateDelay
-   datové zdroje
    -   MPVNET
-   transformace
    -   žádné
-   data modely
    -   RopidGTFSPrecomputedTripScheduleModel `ropidgtfs_precomputed_trip_schedule`
    -   VPPositionsModel `vehiclepositions_positions`

#### _task: RefreshGtfsTripDataTask_

-   vstupní rabbitmq fronta
    -   název: vehicle-positions.vehiclepositions.refreshGTFSTripData
    -   TTL: 10 minut
    -   parametry: žádné
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   žádné
-   datové zdroje
    -   ROPID FTP
-   transformace
    -   žádné
-   data modely
    -   RopidGTFSPrecomputedTripScheduleModel `ropidgtfs_precomputed_trip_schedule`
    -   VPTripsModel `vehiclepositions_trips`
    -   VPTripWithLastPositionModel `v_vehiclepositions_all_trips_with_last_position`

#### _task: UpdateRunsGtfsTripIdTask_

-   vstupní rabbitmq fronta
    -   název: vehicle-positions.vehiclepositions.updateRunsGTFSTripId
    -   TTL: 5 minut
    -   parametry: `{ run, run_message }`
    -   validační schéma: [UpdateRunsGtfsTripIdSchema](../src/integration-engine/vehicle-positions/workers/vehicle-positions/schema/UpdateRunsGtfsTripIdSchema.ts)
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   název: vehicle-positions.vehiclepositions.updateDelay
-   datové zdroje
    -   CHAPS TCP
-   transformace
    -   oběh a nalezené GTFS scheduled trips se transformují na trips [TripsMapper.mapCommonRunToDto](../src/integration-engine/vehicle-positions/workers/vehicle-positions/data-access/helpers/TripsMapper.ts) a pozice [PositionsMapper.mapCommonRunToDto](../src/integration-engine/vehicle-positions/workers/vehicle-positions/data-access/helpers/PositionsMapper.ts) pro frontu `vehiclepositions.updateDelay`
-   data modely
    -   RopidGTFSPrecomputedTripScheduleModel `ropidgtfs_precomputed_trip_schedule`
    -   VPTripsModel `vehiclepositions_trips`
    -   VPPositionsModel `vehiclepositions_positions`

#### _task: UpdateDelayTask_

-   vstupní rabbitmq fronta
    -   název: vehicle-positions.vehiclepositions.updateDelay
    -   TTL: 50 sekund (včetně zpracování v předchozích frontách)
    -   parametry: `{ updatedTrips, positions, schedule }`
    -   validační schéma: [UpdateDelaySchema](../src/integration-engine/vehicle-positions/workers/vehicle-positions/schema/UpdateDelaySchema.ts)
    -   očekává parametrech zprávy `timestamp` s časovou značkou z první fronty, která obdržela nová data např. saveDataToDB
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   název: vehicle-positions.vehiclepositions.propagateDelay
-   datové zdroje
    -   MPVNET, CHAPS TCP
-   transformace
    -   determinace pozic tripů v rámci jednoho GTFS block id (platí pouze pro některé vlakové linky)
    -   [PositionsManager.computePositions](../src/integration-engine/vehicle-positions/workers/vehicle-positions/helpers/PositionsManager.ts#L32): obohacení pozicových dat, aproximace pozice, dopočet zpoždění
-   data modely
    -   VPTripsModel `vehiclepositions_trips`
    -   VPPositionsModel `vehiclepositions_positions`

#### _task: ProcessRegionalBusPositionsTask_

-   vstupní rabbitmq fronta
    -   název: vehicle-positions.vehiclepositions.processRegionalBusPositions
    -   TTL: 3 minuty
    -   parametry: `{ updatedTrips }`
    -   validační schéma: [UpdateDelaySchema](../src/integration-engine/vehicle-positions/workers/vehicle-positions/schema/UpdateDelaySchema.ts)
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   název: vehicle-positions.vehiclepositions.propagateDelay
-   datové zdroje
    -   TELMAX TCP
-   transformace
    -   [RegionalBusPositionsManager.computePositions](../src/integration-engine/vehicle-positions/workers/vehicle-positions/helpers/regional-bus/RegionalBusPositionsManager.ts): obohacení pozicových dat, aproximace pozice, dopočet zpoždění
-   data modely
    -   VPTripsModel `vehiclepositions_trips`
    -   VPPositionsModel `vehiclepositions_positions`

#### _task: PropagateDelayTask_

-   vstupní rabbitmq fronta
    -   název: vehicle-positions.vehiclepositions.propagateDelay
    -   TTL: 5 minut
    -   parametry: `{ processedPositions, trips }`
    -   validační schéma: [PropagateDelaySchema](../src/integration-engine/vehicle-positions/workers/vehicle-positions/schema/PropagateDelaySchema.ts)
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   vehicle-positions.vehiclepositions.propagateTrainDelay
-   datové zdroje
    -   MPVNET, CHAPS TCP, TELMAX TCP
-   transformace
    -   [mapDelayedPositionsToRunTrips](../src/integration-engine/vehicle-positions/workers/vehicle-positions/tasks/PropagateDelayTask.ts#L447): transformace výstupních pozic z updateDelay na oběhy
    -   [propagateDelayForGtfsTrip](../src/integration-engine/vehicle-positions/workers/vehicle-positions/tasks/PropagateDelayTask.ts#L474): přepočet zpoždění u spojů se zpožděním před tratí
    -   VPPositionsModel `vehiclepositions_positions`

#### _task: PropagateTrainDelayTask_

-   vstupní rabbitmq fronta
    -   název: vehicle-positions.vehiclepositions.propagateTrainDelay
    -   TTL: 5 minut
        -   validační schéma: [PropagateDelaySchema](../src/integration-engine/vehicle-positions/workers/vehicle-positions/schema/PropagateTrainDelaySchema.ts)
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   žádné
-   datové zdroje
    -   MPVNET
-   popis
    -   propaguje zpoždění pro následující blokové spoje

#### _task: DataRetentionTask_

-   vstupní rabbitmq fronta
    -   název: vehicle-positions.vehiclepositions.dataRetention
    -   TTL: 10 minut
    -   parametry: žádné
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   žádné
-   datové zdroje
    -   MPVNET, CHAPS TCP
-   transformace
    -   žádné
-   data modely
    -   VPPositionsModel `vehiclepositions_positions`
    -   VPPositionsHistoryModel `vehiclepositions_positions_history`
    -   VPTripsModel `vehiclepositions_trips`
    -   VPTripsHistoryModel `vehiclepositions_trips_history`
    -   CisStopModel `vehiclepositions_cis_stops`

#### _task: RefreshPublicTripCacheTask_

-   vstupní rabbitmq fronta
    -   název: vehicle-positions.vehiclepositions.refreshPublicTripCache
    -   TTL: 10s
    -   parametry: žádné
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   `vehicle-positions.vehiclepositions.refreshPublicTripCache` task se spouští sám sebe a cron plní doplňující funkci.
-   popis
    -   před samotným spuštěním se zkontroluje pomocí semaforu zdali task již někde neběží
    -   připraví zjednodušené informace o aktuálních pozicích, které se odešlou do redisu
    -   pomocí pub/sub se pak pošle do og informace o novém setu v redisu
-   data modely
    -   VPPositionsModel `vehiclepositions_positions`
    -   VPTripsModel `vehiclepositions_trips`

#### _task: RefreshPublicStopTimeCacheTask_

-   vstupní rabbitmq fronta
    -   název: vehicle-positions.vehiclepositions.refreshPublicStopTimeCache
    -   TTL: 10s
    -   parametry: žádné
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   `vehicle-positions.vehiclepositions.refreshPublicStopTimeCache` task se spouští sám sebe a cron plní doplňující funkci.
-   popis
    -   před samotným spuštěním se zkontroluje pomocí semaforu zdali task již někde neběží
    -   připraví informace o historických a predikovaných zpožděních aktivních spojů, které se odešlou do redisu
-   data modely
    -   VPPositionsModel `vehiclepositions_positions`
    -   VPTripsModel `vehiclepositions_trips`
    -   DescriptorModel `vehiclepositions_vehicle_descriptors`
    -   GtfsShapesModel `ropidgtfs_shapes`
    -   PublicStopTimeModel `v_public_vehiclepositions_combined_stop_times`

### _RunsWorker_

Worker má na starost zpracování a transformaci TCP oběhů

#### _task: SaveTramRunsToDBTask_

-   vstupní rabbitmq fronta
    -   název: vehicle-positions.vehiclepositionsruns.saveTramRunsToDB
    -   TTL: 3 minuty
    -   parametry: `{ M: V: $[] }`
    -   validační schéma: [CommonRunsSchema](../src/integration-engine/vehicle-positions/workers/runs/schema/CommonRunsSchema.ts)
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   název: vehicle-positions.vehiclepositions.updateRunsGTFSTripId
-   datové zdroje
    -   CHAPS TCP
-   transformace
    -   [CommonRunsMessagesTransformation](../src/integration-engine/vehicle-positions/workers/runs/transformations/CommonRunsMessagesTransformation.ts)
-   data modely
    -   CommonRunsModel `vehiclepositions_runs`
    -   CommonRunsMessagesModel `vehiclepositions_runs_messages`

#### _task: SaveBusRunsToDBTask_

-   vstupní rabbitmq fronta
    -   název: vehicle-positions.vehiclepositionsruns.saveBusRunsToDB
    -   TTL: 2 minuty
    -   parametry: `{ M: V: $[] }`
    -   validační schéma: [CommonRunsSchema](../src/integration-engine/vehicle-positions/workers/runs/schema/CommonRunsSchema.ts)
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   název: vehicle-positions.vehiclepositions.updateRunsGTFSTripId
-   datové zdroje
    -   CHAPS TCP
-   transformace
    -   [CommonRunsMessagesTransformation](../src/integration-engine/vehicle-positions/workers/runs/transformations/CommonRunsMessagesTransformation.ts)
-   data modely
    -   CommonRunsModel `vehiclepositions_runs`
    -   CommonRunsMessagesModel `vehiclepositions_runs_messages`

#### _task: SaveMetroRunsToDBTask_

-   vstupní rabbitmq fronta
    -   název: vehicle-positions.vehiclepositionsruns.saveMetroRunsToDB
    -   TTL: 3 minuty
    -   parametry: `{ m: { $, vlak } }`
    -   validační schéma: [MetroRunsSchema](../src/integration-engine/vehicle-positions/workers/runs/schema/MetroRunsSchema.ts)
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   název: vehicle-positions.vehiclepositionsruns.processMetroRunMessages
-   datové zdroje
    -   CHAPS TCP
-   transformace
    -   [MetroRunsMessagesTransformation](../src/integration-engine/vehicle-positions/workers/runs/transformations/MetroRunsMessagesTransformation.ts)
-   data modely
    -   MetroRunsMessagesModel `vehiclepositions_metro_runs_messages`

#### _task: ProcessMetroRunsMessagesTask_

-   vstupní rabbitmq fronta
    -   název: vehicle-positions.vehiclepositionsruns.processMetroRunMessages
    -   TTL: 10 minut
    -   parametry: `{ routeName, messages }`
    -   validační schéma: [MetroTransformedRunsSchema](../src/integration-engine/vehicle-positions/workers/runs/tasks/schema/MetroTransformedRunsSchema.ts)
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   název: vehicle-positions.vehiclepositions.updateDelay
-   datové zdroje
    -   CHAPS TCP
-   transformace
    -   [MetroRunsMessageProcessingTransformation](../src/integration-engine/vehicle-positions/workers/runs/tasks/transformations/MetroRunsMessageProcessingTransformation.ts), dále se oběhy a nalezené GTFS scheduled trips transformují na trips [TripsMapper.mapMetroRunToDto](../src/integration-engine/vehicle-positions/workers/vehicle-positions/data-access/helpers/TripsMapper.ts)s) a pozice [PositionsMapper.mapMetroRunToDto](../src/integration-engine/vehicle-positions/workers/vehicle-positions/data-access/helpers/PositionsMapper.ts) pro frontu `vehiclepositions.updateDelay`
-   data modely
    -   MetroRailtrackGPSModel `ropidgtfs_metro_railtrack_gps`
    -   RopidGTFSScheduledTripsModel `ropidgtfs_precomputed_trip_schedule`
    -   VPPositionsModel `vehiclepositions_positions`
    -   VPTripsModel `vehiclepositions_trips`
    -   CommonRunsModel `vehiclepositions_runs`

#### _task: SaveArrivaCityRunsToDB_

-   vstupní rabbitmq fronta
    -   název: vehicle-positions.vehiclepositionsruns.saveArrivaCityRunsToDB
    -   TTL: 3 minuty
    -   parametry: `{ M: V: $[] }`
    -   validační schéma: [RegionalBusRunsSchema](../src/integration-engine/vehicle-positions/workers/runs/schema/RegionalBusRunsSchema.ts)
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   název: vehicle-positions.vehiclepositionsruns.processRegionalBusRunMessages
-   datové zdroje
    -   TELMAX TCP
-   transformace
    -   [RegionalBusRunsMessagesTransformation](../src/integration-engine/vehicle-positions/workers/runs/transformations/RegionalBusRunsMessagesTransformation.ts)
-   data modely
    -   RegionalBusRunsMessagesModel `vehiclepositions_regional_bus_runs_messages`

#### _task: ProcessRegionalBusRunMessagesTask_

-   vstupní rabbitmq fronta
    -   název: vehicle-positions.vehiclepositionsruns.processRegionalBusRunMessages
    -   TTL: 5 minut
    -   parametry: `{ messages }`
    -   validační schéma: [RegionalBusTransformedRunsValidationSchema](../src/integration-engine/vehicle-positions/workers/runs/schema/RegionalBusTransformedRunsSchema.ts)
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   název: vehicle-positions.vehiclepositions.processRegionalBusPositions
-   datové zdroje
    -   TELMAX TCP
-   transformace
    -   Oběhy a nalezené GTFS scheduled trips transformují na trips [TripsMapper.mapRegionalBusRunToDto](../src/integration-engine/vehicle-positions/workers/vehicle-positions/data-access/helpers/TripsMapper.ts) a pozice [PositionsMapper.mapRegionalBusRunToDto](../src/integration-engine/vehicle-positions/workers/vehicle-positions/data-access/helpers/PositionsMapper.ts) pro frontu `vehiclepositions.processRegionalBusPositions`
-   data modely
    -   RopidGTFSScheduledTripsModel `ropidgtfs_precomputed_trip_schedule`
    -   RouteSubAgencyModel `ropidgtfs_route_sub_agencies`
    -   RunNumberModel `ropidgtfs_run_numbers`
    -   DescriptorModel `vehiclepositions_vehicle_descriptors`
    -   VPPositionsModel `vehiclepositions_positions`
    -   VPTripsModel `vehiclepositions_trips`

### _GtfsRealTimeWorker_

Worker má na starost generování souborů s realtime daty pro `gtfsrt` endpointy

#### _task: GenerateFilesTask_

-   vstupní rabbitmq fronta
    -   název: vehicle-positions.vehiclepositionsgtfsrt.generateFiles
    -   TTL: 29 sekund
    -   parametry: žádné
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   žádné
-   datové zdroje
    -   MPVNET, CHAPS TCP + UDP, ROPID FTP, ROPID VYMI
-   transformace
    -   žádné
-   data modely
    -   RopidGTFSStopTimesModel `ropidgtfs_stop_times`
    -   RopidVYMIEventsModel `ropidvymi_events`
    -   VPTripsModel `vehiclepositions_trips`
    -   VPPositionsModel `vehiclepositions_positions`

**Určení zpoždění v trip_updates.pb**

Predikce odjezdů probíhá podle vzorce:

    departure_delay = arrival_delay - min( max(0, arrival_delay), max(0, stop_dwell_time_seconds - 60) );
    departure_delay = if provider_source_type = http then max(0, departure_delay) else departure_delay;

-   Poslední projetá zastávka
    -   arrival_delay - zpoždění na příjezdu v poslední zastávce
    -   departure_delay - zpoždění na odjezdu v poslední zastávce
-   Následující zastávka
    -   arrival_delay - aktuální zpoždění spoje
    -   departure_delay - predikce podle vzorce výše
-   Všechny další zastávky až po konečnou
    -   arrival_delay - zpoždění na odjezdu z předchozí zastávky (departure_delay předchozího záznamu)
    -   departure_delay - predikce podle vzorce výše

```js
// stopTimeUpdate[]
[
    { arrival: {}, departure: { delay: 60 }, stopSequence: 2, stopId: "U474Z4P" },
    { arrival: { delay: 30 }, departure: { delay: 30 }, stopSequence: 3, stopId: "U603Z3P" }, // dwell time 0s
    { arrival: { delay: 30 }, departure: { delay: 30 }, stopSequence: 4, stopId: "U665Z2P" }, // dwell time 0s
    { arrival: { delay: 30 }, departure: { delay: 20 }, stopSequence: 5, stopId: "U332Z2P" }, // dwell time 70s
    { arrival: { delay: 20 }, departure: { delay: 20 }, stopSequence: 6, stopId: "U467Z2P" }, // dwell time 0s
    { arrival: { delay: 20 }, departure: { delay: 0 }, stopSequence: 7, stopId: "U740Z3P" }, // dwell time 100s
    { arrival: { delay: 0 }, departure: { delay: 0 }, stopSequence: 8, stopId: "U78Z3P" }, // dwell time 0s
    { arrival: { delay: 0 }, departure: { delay: 0 }, stopSequence: 9, stopId: "U675Z3P" }, // dwell time 0s
];
```

### _DescriptorWorker_

Worker má na starost stahování informací o vozidlech (např. dostupnost klimatizace nebo USB zásuvek) a jejich uložení do databáze

#### _task: RefreshDescriptorsTask_

-   vstupní rabbitmq fronta
    -   název: vehicle-positions.vehicledescriptors.refreshDescriptors
    -   TTL: 10 minut
    -   parametry: žádné
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   žádné
-   datové zdroje
    -   Seznam Autobusu API
-   transformace
    -   [DescriptorTransformation](../src/integration-engine/vehicle-positions/workers/vehicle-descriptors/transformations/DescriptorTransformation.ts)
-   data modely
    -   VehicleDescriptorModel `vehiclepositions_vehicle_descriptors`

### _DataRetentionWorker_

Worker má na starost retenci dat tabulek Azure Table Storage

#### _task: DeleteDataTask_

-   vstupní rabbitmq fronta
    -   název: vehicle-positions.dataretention.deleteData
    -   TTL: 15 minut
    -   parametry: `{ targetHours, repoName }`
    -   validační schéma: [DataDeletionSchema](../src/integration-engine/shared/schema/DataDeletionSchema.ts)
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   žádné
-   datové zdroje
    -   podle [RepositoryTableNameEnum](../src/integration-engine/data-retention/workers/constants/RepositoryTableNameEnum.ts)
-   transformace
    -   žádné
-   data modely
    -   žádné

#### _task: TableStorageRetentionTask_

-   vstupní rabbitmq fronta
    -   název: vehicle-positions.dataretention.deleteMonthOldStorageData
    -   TTL: 1 hodina
    -   parametry: `{ tableName }`
    -   validační schéma: [TableStorageRetentionSchema](../src/integration-engine/data-retention/workers/schema/TableStorageRetentionSchema.ts)
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   žádné
-   datové zdroje
    -   CHAPS TCP, ukládane v TCP IG
-   transformace
    -   žádné
-   data modely
    -   žádné

### _JISWorker_

Worker má na starost stahování VYMI (JIS) infotextů, jejich transformaci a uložení do databáze

#### _task: RefreshJISInfotextsTask_

-   vstupní rabbitmq fronta
    -   název: vehicle-positions.jis.refreshJISInfotexts
    -   TTL: 20 sekund
    -   parametry: žádné
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   žádné
-   datové zdroje
    -   JIS Infotexty
-   transformace
    -   [JISInfotextsTransformation](../src/integration-engine/jis/transformations/JISInfotextsTransformation.ts)
-   popis
    -   z Redisu je získán ETag poslední přijaté odpovědi datového zdroje
    -   z datového zdroje jsou podmíněným dotazem s posledním ETagem získána data a metadata
    -   pokud na straně datového zdroje došlo k aktualizaci dat, je provedena transformace
    -   transformovaná data jsou uložena do Postgres a starší záznamy jsou odstraněny
    -   v Redisu je aktualizován poslední přijatý ETag
-   data modely
    -   JISInfotextsModel `jis_infotexts`
    -   JISInfotextsRopidGTFSStopsModel `jis_infotexts_ropidgtfs_stops`

## Uložení dat

-   typ databáze
    -   PSQL
-   databázové schéma

    -   GTFS JŘ<br/>
        ![pid scheduled diagram](./assets/pid_scheduled_erd.png)
    -   GTFS statické a předpočítatné tabulky<br/>
        ![pid scheduled diagram](./assets/pid_precomputed_erd.png)
    -   Preset logy<br/>
        ![pid preset logs diagram](./assets/pid_preset_logs_erd.png)
    -   VYMI<br/>
        ![pid vymi diagram](./assets/pid_vymi_erd.png)
    -   Polohy vozů (tabulky) <br/>
        ![pid vp diagram](./assets/pid_vp_erd.png)
    -   Polohy vozů (views) <br/>
        ![pid vp view diagram](./assets/pid_vp_view_erd.png)
    -   Polohy vozů (historie spojů podle projetých zastávek) <br/>
        ![pid vp stop time history diagram](./assets/pid_vp_st_hist_erd.png)
    -   Popis vozidel<br/>
        ![pid vp desc diagram](./assets/pid_vp_desc_erd.png)
    -   Zastávky RT spojů (pouze MPV)<br/>
        ![pid vp cis  diagram](./assets/pid_vp_cis_erd.jpg)
    -   JIS infotexty<br/>
        ![pid jis diagram](./assets/pid_jis_erd.png)

-   retence dat
    -   jízdní řády se generují 1-2x denně na 14 dní dopředu
    -   polohová data si uchováváme 24 hodin (30 minut od poslední aktualizace, poté v historických tabulkách)
    -   popis vozidel si uchováváme 24 hodin
    -   preset logy si uchováváme 8 hodin
    -   JIS infotexty si uchováváme do příštího úspěšného znovunačtení (ke kterému by mělo docházet dvakrát za minutu)

## Input API

### Obecné

-   OpenAPI v3 dokumentace
    -   zdrojový soubor: [openapi-input.yaml](./openapi-input.yaml)
    -   rabin: https://rabin.golemio.cz/pid/input-gateway/docs/openapi/
    -   golem: https://api.golemio.cz/pid/input-gateway/docs/openapi/
-   api je neveřejné
-   postman kolekce
    -   TBD

### Vehicle Positions

#### _/vehiclepositions_

-   cílová rabbitmq fronta
    -   vehicle-positions.vehiclepositions.saveDataToDB
-   data nám pravidelně přichází z MPVNETu

### GTFS

#### _/ropidgtfs/presets_

-   cílová rabbitmq fronta
    -   vehicle-positions.ropidpresets.savePresets
-   data nám budou zasílána z dohledového centra

## Output API

### Obecné

-   OpenAPI v3 dokumentace
    -   zdrojový soubor: [openapi-output.yaml](./openapi-output.yaml)
    -   rabin: https://rabin.golemio.cz/pid/docs/openapi/
    -   golem: https://api.golemio.cz/pid/docs/openapi/
-   GTFS-RT a `/v2/public` data jsou veřejná, zbývající endpointy jsou k dispozici všem registrovaným uživatelům
-   postman kolekce
    -   TBD

### GTFS v2

#### _/v2/gtfs/services_

-   zdrojové tabulky
    -   `ropidgtfs_calendar`

#### _/v2/gtfs/routes_

-   zdrojové tabulky
    -   `ropidgtfs_routes`

#### _/v2/gtfs/routes/{id}_

-   zdrojové tabulky
    -   `ropidgtfs_routes`

#### _/v2/gtfs/trips_

-   zdrojové tabulky
    -   `ropidgtfs_trips`, `ropidgtfs_stop_times`, `ropidgtfs_calendar`

#### _/v2/gtfs/trips/{id}_

-   zdrojové tabulky
    -   `ropidgtfs_trips`, `ropidgtfs_stop_times`, `ropidgtfs_stops`, `ropidgtfs_shapes`, `ropidgtfs_calendar`, `ropidgtfs_routes`

#### _/v2/gtfs/shapes/{id}_

-   zdrojové tabulky
    -   `ropidgtfs_shapes`

#### _/v2/gtfs/stops_

-   zdrojové tabulky
    -   `ropidgtfs_stops`, `ropidgtfs_cis_stops`
-   dodatečná transformace: Feature collection

#### _/v2/gtfs/stops/{id}_

-   zdrojové tabulky
    -   `ropidgtfs_stops`, `ropidgtfs_cis_stops`
-   dodatečná transformace: Feature

#### _/v2/gtfs/stoptimes/{id}_

-   zdrojové tabulky
    -   `ropidgtfs_stop_times`, `ropidgtfs_stops`, `ropidgtfs_calendar`, `ropidgtfs_trips`

### Vehicle Positions v2

#### _/v2/vehiclepositions_

-   zdrojové tabulky
    -   `v_vehiclepositions_all_trips_with_last_position` (`vehiclepositions_positions`, `vehiclepositions_trips`)
    -   `vehiclepositions_vehicle_types`
    -   `vehiclepositions_vehicle_descriptors`
-   dodatečná transformace: Feature collection
-   je použit `CompressionByDefaultMiddleware` a odpovědi jsou tak na output gateway komprimovány i pokud klient explicitně nepovolí komprimaci vhodnou hodnotou hlavičky `Accept-Encoding` (lze však komprimaci explicitně zakázat hodnotou `identity`)

#### _/v2/vehiclepositions/{gtfsTripId}_

-   zdrojové tabulky
    -   `v_vehiclepositions_all_trips_with_last_position` (`vehiclepositions_positions`, `vehiclepositions_trips`)
    -   `v_vehiclepositions_all_processed_positions` (`vehiclepositions_positions`)
    -   `vehiclepositions_vehicle_types`
    -   `vehiclepositions_vehicle_descriptors`
-   dodatečná transformace: Feature
-   je použit `CompressionByDefaultMiddleware` a odpovědi jsou tak na output gateway komprimovány i pokud klient explicitně nepovolí komprimaci vhodnou hodnotou hlavičky `Accept-Encoding` (lze však komprimaci explicitně zakázat hodnotou `identity`)

### Vehicle Positions GTFS RT v2

#### _/v2/vehiclepositions/gtfsrt/trip_updates.pb_

-   načtení souboru z Redis cache
    -   HGET files:gtfsRt trip_updates.pb

#### _/v2/vehiclepositions/gtfsrt/vehicle_positions.pb_

-   načtení souboru z Redis cache
    -   HGET files:gtfsRt vehicle_positions.pb

#### _/v2/vehiclepositions/gtfsrt/pid_feed.pb_

-   načtení souboru z Redis cache
    -   HGET files:gtfsRt pid_feed.pb

#### _/v2/vehiclepositions/gtfsrt/alerts.pb_

-   načtení souboru z Redis cache
    -   HGET files:gtfsRt alerts.pb

### PID Departure Boards v2

#### _/v2/pid/departureboards_

-   zdrojové tabulky
    -   `ropidgtfs_precomputed_departures`, `ropidgtfs_stops`, `ropidgtfs_cis_stops`, `jis_infotexts`, `jis_infotexts_ropidgtfs_stops`, `ropid_departures_directions`, `vehiclepositions_positions`, `vehiclepositions_trips`

#### _/v2/pid/infotexts_

-   zdrojové tabulky
    -   `ropidvymi_events`, `ropidvymi_events_stops`, `ropidvymi_events_routes`, `ropidgtfs_stops`

### PID Departure Boards v3

#### _/v3/pid/transferboards_

-   načte odjezdy pro přestupní tabule ve vozidlech a aktivní infotexty exportované z aplikace VYMI (JIS) Infotexty

#### _/v3/pid/infotexts_

-  načte aktivní infotexty exportované z aplikace VYMI (JIS) Infotexty
-  zdrojové tabulky
    -   `jis_infotexts`, `jis_infotexts_ropidgtfs_stops`, `ropidgtfs_stops`

### Public v2

#### _/v2/public/gtfs/trips/{gtfsTripId}_

-   načte statické informace o GTFS spoji z PSQL, případně jeho oběhové číslo pro současný den (pokud je k dispozici), shapes a zastávky
    -   `info`: statické informace o GTFS spoji a jeho oběhové číslo pro současný den (pokud je k dispozici)
    -   `stop_times`: seznam zastávek ve formátu geojson
    -   `shapes`: seznam tvarů trasy podle GTFS ve formátu geojson
    -   `vehicle_descriptor`: informace o vozidle - pouze očekáváná nízkopodlažnost vozidla načtená z GTFS trips.txt
-   cachováno na 4 hodiny pomocí cache-control hlavičky
-   je použit `CompressionByDefaultMiddleware` a odpovědi jsou tak na output gateway komprimovány i pokud klient explicitně nepovolí komprimaci vhodnou hodnotou hlavičky `Accept-Encoding` (lze však komprimaci explicitně zakázat hodnotou `identity`)

#### _/v2/public/vehiclepositions_

-   načte aktuální polohy vozidel z redisu a vrátí je ve formátu geojson
    -   možno filtrovat dle bouding box na straně redisu
    -   podle typu vozidla a gtfs route number na klientské straně
-   je použit `CompressionByDefaultMiddleware` a odpovědi jsou tak na output gateway komprimovány i pokud klient explicitně nepovolí komprimaci vhodnou hodnotou hlavičky `Accept-Encoding` (lze však komprimaci explicitně zakázat hodnotou `identity`)

#### _/v2/public/vehiclepositions/{vehicleId}_

-   načte jednu aktuální polohu z redisu
    -   výstup možno filtrovat podle query parametru `scopes`
    -   `info`: detailní informace o spoji, vč. realtime dat
    -   `stop_times`: seznam zastávek, které vozidlo projelo a projede, vč. informací o zpoždění (historických/predikovaných), ve formátu geojson
    -   `shapes`: seznam tvarů trasy (podle GTFS; nejedná se o naše předpočítané anchor pointy, které se mimo zastávky oproti GTFS shapes mohou lišit), ve formátu geojson
    -   `vehicle_descriptor`: informace o vozidle (dopravce, dostupnost klimatizace, USB, ...)
-   je použit `CompressionByDefaultMiddleware` a odpovědi jsou tak na output gateway komprimovány i pokud klient explicitně nepovolí komprimaci vhodnou hodnotou hlavičky `Accept-Encoding` (lze však komprimaci explicitně zakázat hodnotou `identity`)

#### _/v2/public/vehiclepositions/{vehicleId}{gtfsTripId}_

-   rozšíření endpointu výše `/public/vehiclepositions/{vehicleId}` o možnost dotázat se na before track (delayed) spoje podle `gtfsTripId`
-   je použit `CompressionByDefaultMiddleware` a odpovědi jsou tak na output gateway komprimovány i pokud klient explicitně nepovolí komprimaci vhodnou hodnotou hlavičky `Accept-Encoding` (lze však komprimaci explicitně zakázat hodnotou `identity`)

#### _/v2/public/departureboards_

-   načte nacacheované odjezdy z redisu
    - možno filtrovat podle skupin zastávek `stopIds` (podle GTFS číselníku) s možností nastavení pořadí navrácených skupin s odjezdy - příklad `?stopIds[]={"0": ["U717Z5P"]}&stopIds[]={"1": ["U718Z5P", "U719Z5P"]}`
    - podle názvů linek a jak daleko do budoucnosti se mají odjezdy navracet
    - limitování počtu odjezdů v každé skupině
-   EP je optimalizován pro vyšší zátěž z mobilní aplikace PID lítačka - požadavek do databáze z OG pouze v případě nutnosti obnovit cache s vehicle descriptory (klimatizace, usb nabíječky), jinak je vše z Redisu
-   je použit `CompressionByDefaultMiddleware` a odpovědi jsou tak na output gateway komprimovány i pokud klient explicitně nepovolí komprimaci vhodnou hodnotou hlavičky `Accept-Encoding` (lze však komprimaci explicitně zakázat hodnotou `identity`)
