# TCP common state_position

-   Data chodí na TCP-IG

```xml
<M>
    <V turnus="17/3" line="17" evc="9168" np="ano" lat="50.05440" lng="14.41812" akt="03030001" takt="2022-08-02T21:56:22" konc="26200002" tjr="2022-08-02T21:56:00" pkt="1885317" tm="2022-08-02T21:56:22" events="O" />
</M>
```

```xml
<M>
    <V turnus="134/3" line="134" evc="3738" np="ano" lat="50.03952" lng="14.42919" akt="07960001" takt="2022-06-29T17:28:13" konc="01100005" tjr="2022-06-29T17:23:00" pkt="12709236" tm="2022-06-29T17:28:22" events="O" />
</M>
```

-   Transport (TCP) Input Gateway
    -   Raw data se uloží na blob storage (1MB buffer)
    -   knihovnou `xml2js` transformují na JSON
    -   pošlou na Rabbita do fronty `saveTramRunsToDB/saveBusRunsToDB`
-   saveTramRunsToDB/saveBusRunsToDB
    -   zprávy se uloží do DB
    -   následně se pošlou do fronty `updateRunsGTFSTripId`
-   updateRunsGTFSTripId
    -   data se napasují na struktury tabulek `vehiclepositions_trips` a `vehiclepositions_positions` (stav je `unknown`)
        -   pokud je tramvaj, je `tracking 2`
        -   nebo pokud je autobus
            -   pokud vozidlo odeslalo zprávu `O`, je `tracking 2`
            -   pokud je vozidlo v první zastávce (tjr), nebo pokud vozidlo vyjelo z první zastávky (takt), je `tracking 2`
        -   nebo je `tjr` atribut větší než příjezd do první zastávky spoje, je `tracking 2`
        -   jinak je `tracking 0`
    -   Rovněž se podle kmenové linky a oběhu vytvoří následující spoje (stav je `unknown`, `tracking` je vždy 0)
    -   navíc se předchozímu spoji vytvoří nová pozice s `tracking 0`, všechny spoje se odešlou na `updateDelay`
        -   Pokud je spoj vyhodnocen jako neveřejný (např. mazačka), vytvoří se nový spoj a pozice se stavem `not_public`, tím processing končí
-   updateDelay
    -   upsert asociovaných pozic
    -   opětovné načtení asociovaných pozic z DB
    -   určení trasy spoje podle GTFS shapes a stop times
    -   processing pozic, uložení do databáze
        -   Not tracking (`tracking` je `0`)
            -   pokud je poslední pozice neznámá, nebo je známá a ve stavu `on_track` nebo `at_stop`
                -   pokud vozidlo jede z garáže, je stav `invisible`
                -   jinak je stav `before_track`
            -   pokud je poslední známá pozice s `tracking 2`
                -   pokud je pozice duplicitní (existuje pozice se stejným `origin_timestamp`), stav je `duplicate`
                -   pokud vozidlo jede do garáže, je stav `invisible`
                -   jinak je stav `after_track`
        -   Tracking (`tracking` je `2`)
            -   pokud je vozidlo 200 metrů od nejblizšího bodu na trase, stav je `off_track`
            -   pokud je vozidlo v zastávce, je stav `at_stop`
            -   pokud je vozidlo mimo zastávku a zároveň vozidlo poslalo zprávu `O` mimo poslední zastávku, je stav `on_track`
            -   pokud je vozidlo v zastávce, nebo poslalo zprávu `V`, nebo poslalo zprávu v poslední zastávce, nebo poslalo zprávu z `tjr` atributem větší než je příjezd do poslední zastávky, je stav `after_track`
-   propagateDelay
    -   podle kmenové linky a oběhu vyhledáme navazující spoje
    -   podle GTFS trip id a registračního čísla poslední pozici ve stavu `before_track` nebo `invisible`
    -   přepsání stavu na `before_track_delayed`

```mermaid
flowchart TB;
  A[TCP data]-->B[Transport TCP Input Gateway]--raw data 1MB buffer-->Blob[Azure Blob Storage];
  B-->Qu{Datový zdroj?};
  Qu--tram-->X1[AMQP saveTramRunsToDB]-->|oběhy| N[AMQP updateRunsGTFSTripId];
  Qu--bus-->X2[AMQP saveBusRunsToDB]-->|oběhy| N;
  X1--upsert-->runs[(DB runs)];
  X2--upsert-->runs;

  N-->Qn{Je neveřejný? Např. mazačka};
  Qn--Ano-->X3[updateNotPublicRunTrip];
  Qn--Ne-->X4[generateDelayMsg];
  X3--upsert-->trips;
  X3--upsert-->positions;
  X4--vygenerované spoje s GTFS daty-->E[AMQP updateDelay];
  X4--upsert-->trips;
  X4--upsert-->positions;
  E--upsert-->trips[(DB trips)];
  E--upsert-->positions[(DB positions)];
  E--select-->trips;
  E--select-->positions;
  E--uložené spoje-->F[AMQP propagateDelay];
  F--select-->trips;
  F--select-->positions;
  F--upsert-->positions;
```
