# HTTP state_position

-   Batch data chodí na VP-IG (několik MB)

```xml
<?xml version="1.0" encoding="utf-8"?>
<m disp="http://77.93.194.81:8716/api">
    <spoj ka="2" kb="0" den="2023-03-21" lin="100115" alias="115" spoj="1061" dopr="ČSAD POLKOST"
        doprSkut="ČSAD POLKOST" kmenl="115" t="3" tcis="A" np="true" po="39" vuzevc="1861"
        skol="false" posl="false" sled="0" lat="50.0307" lng="14.49185" cpoz="10:00:04" azimut="337"
        rychl="26">
        <zast stan="B" odj="10:30" zast="56714" asw="520091" zpoz_typ_odj="2" zpoz_odj="0" />
        <zast stan="B" odj="10:32" zast="56794" asw="11310002" zpoz_typ_odj="2" zpoz_odj="0" />
        <zast stan="A" odj="10:34" zast="59066" asw="13410001" zpoz_typ_odj="2" zpoz_odj="0" />
        <zast stan="A" odj="10:36" zast="67975" asw="11320001" zpoz_typ_odj="2" zpoz_odj="0" />
        <zast stan="B" odj="10:37" zast="58953" asw="13340002" zpoz_typ_odj="2" zpoz_odj="0" />
        <zast stan="A" odj="10:40" zast="58789" asw="1810001" zpoz_typ_odj="2" zpoz_odj="0" />
        <zast stan="A" odj="10:41" zast="56794" asw="11310001" />
        <zast stan="F" prij="10:43" zast="56714" asw="520002" />
    </spoj>
    <spoj ka="2" kb="0" den="2023-03-21" lin="100117" alias="117" spoj="1016" dopr="ABOUT ME"
        doprSkut="ABOUT ME" kmenl="117" t="3" tcis="A" np="true" po="6" vuzevc="1950" skol="false"
        posl="false" sled="0" lat="50.04645" lng="14.46061" cpoz="09:59:56" azimut="165" rychl="0"
        zast="57408" asw="500006" zpoz_prij="14">
        <zast stan="B" odj="08:55" zast="66826" asw="32360002" zpoz_typ_odj="3" zpoz_odj="35" />
        <zast stan="B" odj="08:55" zast="67375" asw="9910002" zpoz_typ_odj="3" zpoz_odj="81" />
        <zast stan="C" odj="08:56" zast="55206" asw="2800053" zpoz_typ_odj="3" zpoz_odj="119" />
        <zast stan="B" odj="08:58" zast="55181" asw="620002" zpoz_typ_odj="3" zpoz_odj="113" />
        <zast stan="H8" odj="08:59" zast="51228" asw="10060052" zpoz_typ_odj="3" zpoz_odj="121" />
        <zast stan="A" odj="09:00" zast="67477" asw="26020001" zpoz_typ_odj="3" zpoz_odj="85" />
        <zast stan="E" odj="09:02" zast="51227" asw="9290005" zpoz_typ_odj="3" zpoz_odj="68" />
        <zast stan="A" odj="09:03" zast="51226" asw="9390001" zpoz_typ_odj="3" zpoz_odj="91" />
        <zast stan="A" odj="09:05" zast="55233" asw="11580001" zpoz_typ_odj="3" zpoz_odj="81" />
        <zast stan="D" odj="09:07" zast="43729" asw="760051" zpoz_typ_odj="3" zpoz_odj="2" />
        <zast stan="A" odj="09:08" zast="55214" asw="170001" zpoz_typ_odj="3" zpoz_odj="31" />
        <zast stan="A" odj="09:09" zast="55183" asw="7270001" zpoz_typ_odj="3" zpoz_odj="36" />
        <zast stan="A" odj="09:11" zast="55257" asw="7720001" zpoz_typ_odj="3" zpoz_odj="-9" />
        <zast stan="A" odj="09:12" zast="55220" asw="9950001" zpoz_typ_odj="3" zpoz_odj="-15" />
        <zast stan="B" odj="09:13" zast="55188" asw="9270002" zpoz_typ_odj="3" zpoz_odj="-2" />
        <zast stan="B" odj="09:15" zast="58807" asw="6250002" zpoz_typ_odj="3" zpoz_odj="-20" />
        <zast stan="B" odj="09:16" zast="58781" asw="4460002" zpoz_typ_odj="3" zpoz_odj="-5" />
        <zast stan="B" odj="09:17" zast="58863" asw="6580002" zpoz_typ_odj="3" zpoz_odj="1" />
        <zast stan="D" odj="09:18" zast="58863" asw="6580004" zpoz_typ_odj="3" zpoz_odj="-9" />
        <zast stan="A" odj="09:19" zast="59095" asw="3280001" zpoz_typ_odj="3" zpoz_odj="-24" />
        <zast stan="A" odj="09:20" zast="58972" asw="600001" zpoz_typ_odj="3" zpoz_odj="-8" />
        <zast stan="B" odj="09:21" zast="58836" asw="4940002" zpoz_typ_odj="3" zpoz_odj="-11" />
        <zast stan="B" odj="09:22" zast="58976" asw="8570002" zpoz_typ_odj="3" zpoz_odj="22" />
        <zast stan="D" odj="09:24" zast="58837" asw="4970004" zpoz_typ_odj="3" zpoz_odj="-4" />
        <zast stan="A" odj="09:25" zast="58869" asw="7220001" zpoz_typ_odj="3" zpoz_odj="-3" />
        <zast stan="B" odj="09:27" zast="47119" asw="8930002" zpoz_typ_odj="3" zpoz_odj="-28" />
        <zast stan="H" odj="09:29" zast="27948" asw="4880008" zpoz_typ_odj="3" zpoz_odj="-56" />
        <zast stan="A" odj="09:32" zast="57400" asw="4410001" zpoz_typ_odj="3" zpoz_odj="-38" />
        <zast stan="B" odj="09:33" zast="57401" asw="10480002" zpoz_typ_odj="3" zpoz_odj="-7" />
        <zast stan="B" odj="09:34" zast="59152" asw="9370002" zpoz_typ_odj="3" zpoz_odj="-11" />
        <zast stan="B" odj="09:35" zast="59258" asw="9380002" zpoz_typ_odj="3" zpoz_odj="-6" />
        <zast stan="B" odj="09:37" zast="58814" asw="3430002" zpoz_typ_odj="3" zpoz_odj="48" />
        <zast stan="L" prij="09:39" zast="57408" asw="500006" zpoz_typ_prij="3" zpoz_prij="14" />
    </spoj>
    <!--...-->
</m>
```

-   (HTTP) Input Gateway
    -   Raw data se uloží na blob storage
    -   knihovnou `xml2js` se transformují na JSON
    -   validují se vůči JSON schématu a pošlou na Rabbita do fronty `saveDataToDB`
-   saveDataToDB
    -   data se napasují na struktury tabulek `vehiclepositions_trips` a `vehiclepositions_positions` (stav je `unknown`, `tracking` se bere ze vstupních dat)
    -   všechny spoje se upsertují, nové spoje a jejich pozice se pošlou do fronty `updateGTFSTripId`, existující do `updateDelay`
-   updateGTFSTripId
    -   duplikace vlakových spojů a pozic podle block id (určení tracking podle cis stop id)
    -   obohacení spojů GTFS daty (včetně oběhu a kmenové linky)
    -   upsert asociovaných pozic
    -   spoje se pošlou do fronty `updateDelay`
-   updateDelay
    -   opět duplikace vlakových spojů, upsert asociovaných pozic
    -   opětovné načtení asociovaných pozic z DB
    -   určení trasy spoje podle GTFS shapes a stop times
    -   processing pozic, uložení do databáze
        -   Not tracking (`tracking` je `0` a nebo nepřišly souřadnice)
            -   pokud je poslední pozice neznámá, nebo je známá a ve stavu `on_track` nebo `at_stop`, je aktuální stav `before_track`
            -   pokud je poslední známá pozice s `tracking 2`
                -   pokud je pozice duplicitní (existuje pozice se stejným `origin_timestamp`), stav je `duplicate`
                -   jinak je stav `after_track`
        -   Tracking (`tracking` je `2`, souřadnice jsou známy)
            -   pokud je vozidlo 200 metrů od nejblizšího bodu (anchor points) na trase, stav je `off_track`
            -   pokud je nejbližší bod v zastávce, nastaví se stav na `at_stop`
            -   jinak je stav `on_track`
        -   Zrušený spoj (příznak `canceled` u pozice)
            -   stav je vždy `canceled`
-   propagateDelay
    -   podle kmenové linky a oběhu vyhledáme navazující spoje
    -   podle GTFS trip id a registračního čísla poslední pozici ve stavu `before_track`
    -   přepsání stavu na `before_track_delayed`

```mermaid
flowchart TB;
  A[HTTP data]-->B[VP HTTP Input Gateway]--raw data-->Blob[Azure Blob Storage];
  B--JSON xml2js-->C[AMQP saveDataToDB]--upsert-->trips[(DB trips)];
  C--nové spoje bez GTFS dat-->D[AMQP updateGTFSTripId];
  C--existující spoje-->E[AMQP updateDelay];
  D--upsert-->trips;
  D--upsert-->positions[(DB positions)];
  D--nové spoje s GTFS daty-->E;
  E--upsert-->trips;
  E--upsert-->positions;
  E--select-->trips;
  E--select-->positions;
  E--uložené spoje-->F[AMQP propagateDelay];
  F--select-->trips;
  F--select-->positions;
  F--upsert-->positions;
```
