# Usage:   gtfs_rt_pid.py <server_url_prefix> <feed_name> <n_messages_to_print>
# Example: gtfs_rt_pid.py api.golemio.cz vehicle_positions 1

import sys
import json
import urllib.request as urllib
from google.protobuf.json_format import MessageToDict
from definitions import gtfs_realtime_pb2
# OVApi Extension
from definitions import gtfs_realtime_OVapi_pb2

server_url_prefix = sys.argv[1]
feed_name = sys.argv[2]
n_messages_to_print = int(sys.argv[3])

# Download the feed file and parse it as a FeedMessage
feed = gtfs_realtime_pb2.FeedMessage()
feed.ParseFromString(urllib.urlopen('https://' + server_url_prefix + '/v2/vehiclepositions/gtfsrt/' + feed_name + '.pb').read())

# Print the header
print(feed.header)

# Print the first n messages
for entity in feed.entity[:n_messages_to_print]:
    print(json.dumps(MessageToDict(entity), ensure_ascii=False, indent=2))

# Save the feed as a JSON file
with open(feed_name + '.json', 'w') as outfile:
    json.dump(MessageToDict(feed), outfile, ensure_ascii=False, indent=2)

print('Written to ' + feed_name + '.json')
