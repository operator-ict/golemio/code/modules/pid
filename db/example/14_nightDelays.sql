/* Fixes data from previous tests */
update vehiclepositions_trips set gtfs_date = start_timestamp::DATE;

/* test data for night delay */
INSERT INTO vehiclepositions_trips
(cis_line_id, cis_trip_number, run_number, cis_line_short_name, created_at, gtfs_route_id, gtfs_route_short_name, gtfs_trip_id, id, updated_at, start_cis_stop_id, start_cis_stop_platform_code, start_time, start_timestamp, vehicle_type_id, wheelchair_accessible, create_batch_id, created_by, update_batch_id, updated_by, agency_name_scheduled, origin_route_name, agency_name_real, vehicle_registration_number, gtfs_trip_headsign, start_asw_stop_id, gtfs_route_type, gtfs_block_id, last_position_id, is_canceled, end_timestamp, gtfs_trip_short_name, last_position_context, internal_run_number, internal_route_name,gtfs_date)
VALUES(NULL, NULL, 65, NULL, '2023-03-21 00:00:15.399', 'L94', '94', '94_979_230320', '2023-03-21T01:30:00+01:00_94_979_230320_8388', '2023-03-21 02:44:25.833', NULL, NULL, '01:30:00', '2023-03-21T00:30:00.000Z', 6, false, NULL, NULL, NULL, NULL, 'DP PRAHA', '94', 'DP PRAHA', 8388, 'Sídliště Barrandov', NULL, 0, NULL, 554214346, NULL, '2023-03-21T01:40:00.000Z', NULL, NULL, 65, '94',current_date - interval '1 day');

INSERT INTO vehiclepositions_positions
(created_at, delay, delay_stop_arrival, delay_stop_departure, next_stop_id, shape_dist_traveled, is_canceled, lat, lng, origin_time, origin_timestamp, is_tracked, trips_id, create_batch_id, created_by, update_batch_id, updated_at, updated_by, id, bearing, cis_last_stop_id, cis_last_stop_sequence, last_stop_id, last_stop_sequence, next_stop_sequence, speed, last_stop_arrival_time, last_stop_departure_time, next_stop_arrival_time, next_stop_departure_time, asw_last_stop_id, state_process, state_position, this_stop_id, this_stop_sequence, tcp_event, last_stop_headsign, last_stop_name, next_stop_name, this_stop_name, valid_to, scheduled_timestamp)
VALUES('2023-03-21 02:44:25.787', 10, NULL, NULL, 'U1019Z2P', 22.263, NULL, 50.03141, 14.36749, '02:44:20', '2023-03-21T01:44:20.000Z', true, '2023-03-21T01:30:00+01:00_94_979_230320_8388', NULL, NULL, NULL, '2023-03-21 02:44:25.822', NULL, 554214346, 257, NULL, NULL, 'U1019Z2P', 8, 8, NULL, '2023-03-21T01:40:00.000Z', '2023-03-21T01:40:00.000Z', '2023-03-21T01:40:00.000Z', '2023-03-21T01:40:00.000Z', '10190002', 'processed', 'on_track', '10190002', NULL, 'V', NULL, 'Sídliště Barrandov', NULL, NULL, NULL, '2023-03-21T01:40:00.000Z');

WITH init_values AS (SELECT NOW()::DATE AS now_date)
INSERT INTO ropidgtfs_calendar_actual (end_date,friday,monday,saturday,service_id,start_date,sunday,thursday,tuesday,wednesday,create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by) VALUES
	 (TO_CHAR((SELECT now_date FROM init_values)+INTERVAL '7 days','YYYYMMDD'),1,1,1,'1111111-3',TO_CHAR((SELECT now_date FROM init_values)-INTERVAL '1 days','YYYYMMDD'),1,1,1,1,-1,'2022-10-24 10:18:51.362+02',NULL,-1,'2022-10-24 10:18:51.362+02',NULL);

INSERT INTO ropidgtfs_stops_actual
(location_type, parent_station, platform_code, stop_id, stop_lat, stop_lon, stop_name, stop_url, wheelchair_boarding, zone_id, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, level_id, stop_code, stop_desc, stop_timezone, asw_node_id, asw_stop_id)
VALUES(0, NULL, 'A', 'U873Z1P', 50.110622, 14.508335, 'Poštovská', NULL, 1, 'P', NULL, '2023-03-21 05:47:11.042', NULL, NULL, '2023-03-21 05:47:11.042', NULL, NULL, NULL, NULL, NULL, 873, 1);

INSERT INTO ropidgtfs_stop_times_actual (arrival_time,arrival_time_seconds,departure_time,departure_time_seconds,drop_off_type,pickup_type,shape_dist_traveled,stop_headsign,stop_id,stop_sequence,timepoint,trip_id,create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by) VALUES
	 ('25:38:00',NULL,'25:38:00',NULL,'3','3',0.0,NULL,'U873Z1P',8,NULL,'94_979_230320',-1,'2022-10-24 10:21:13.104+02',NULL,-1,'2022-10-24 10:21:13.104+02',NULL),
     ('25:39:00',NULL,'25:39:00',NULL,'3','3',0.0,NULL,NULL,9,NULL,'94_979_230320',-1,'2022-10-24 10:21:13.104+02',NULL,-1,'2022-10-24 10:21:13.104+02',NULL);

INSERT INTO ropidgtfs_routes_actual (agency_id,is_night,is_regional,is_substitute_transport,route_color,route_desc,route_id,route_long_name,route_short_name,route_text_color,route_type,route_url,create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by) VALUES
	 ('99','0','0','0','00A562',NULL,'L94','Dummy','94','FFFFFF','1','',-1,'2022-10-24 10:18:51.722+02',NULL,-1,'2022-10-24 10:18:51.722+02',NULL);

INSERT INTO ropidgtfs_trips_actual (bikes_allowed,block_id,direction_id,exceptional,route_id,service_id,shape_id,trip_headsign,trip_id,trip_operation_type,trip_short_name,wheelchair_accessible,create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by) VALUES
	 (1,NULL,1,0,'L94','1111111-3','L94VX','Sídliště Barrandov','94_979_230320',NULL,NULL,1,-1,'2022-10-24 10:22:47.459+02',NULL,-1,'2022-10-24 10:22:47.459+02',NULL);
