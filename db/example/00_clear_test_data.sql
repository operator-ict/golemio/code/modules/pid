DROP TABLE IF EXISTS "tmp"."ropidvymi_events";

TRUNCATE
    "jis_infotexts_ropidgtfs_stops",
    "jis_infotexts",
    "ropidgtfs_agency_actual",
    "ropidgtfs_calendar_actual",
    "ropidgtfs_calendar_dates_actual",
    "ropidgtfs_cis_stop_groups_actual",
    "ropidgtfs_cis_stops_actual",
    "ropidgtfs_metadata",
    "ropidgtfs_routes_actual",
    "ropidgtfs_shapes_actual",
    "ropidgtfs_stop_times_actual",
    "ropidgtfs_stops_actual",
    "ropidgtfs_trips_actual",
    "ropidgtfs_run_numbers_actual",
    "ropidgtfs_metro_railtrack_gps",
    "ropidvymi_events",
    "ropidvymi_events_stops",
    "ropidvymi_events_routes",
    "ropidvymi_metadata",
    "vehiclepositions_positions",
    "vehiclepositions_trips",
    "vehiclepositions_runs_messages",
    "vehiclepositions_runs",
    "vehiclepositions_metro_runs_messages",
    "vehiclepositions_regional_bus_runs_messages",
    "vehiclepositions_vehicle_descriptors",
    "vehiclepositions_cis_stops",
    "ropid_departures_directions",
    "ropid_departures_presets",
    "ropid_departures_preset_logs",
    "ropidgtfs_precomputed_trip_schedule_actual",
    "ropidgtfs_precomputed_services_calendar_actual",
    "ropidgtfs_precomputed_minmax_stop_sequences_actual",
    "ropidgtfs_precomputed_departures";

ALTER SEQUENCE ropid_departures_directions_id_seq RESTART;
ALTER SEQUENCE ropid_departures_presets_id_seq RESTART;
ALTER SEQUENCE ropid_departures_preset_logs_id_seq RESTART;
ALTER SEQUENCE ropidgtfs_metadata_id_seq RESTART;
ALTER SEQUENCE ropidvymi_metadata_id_seq RESTART;
ALTER SEQUENCE vehiclepositions_positions_id_seq RESTART;
ALTER SEQUENCE vehiclepositions_runs_messages_id_seq RESTART;
