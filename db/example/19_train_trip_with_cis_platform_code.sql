-- ropidgtfs_cis_stops_actual
INSERT INTO ropidgtfs_cis_stops_actual (alt_idos_name,cis,id,jtsk_x,jtsk_y,lat,lon,platform,wheelchair_access,"zone",create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by) VALUES
	 ('Praha hl.n.',5457076,'142/301',-741816.7,-1043640.06,50.0830956,14.4361944,NULL,'possible','P',NULL,'2024-02-06 05:23:14.828',NULL,NULL,'2024-02-06 05:23:14.828',NULL);

-- ropidgtfs_stops_actual
INSERT INTO ropidgtfs_stops_actual (location_type,parent_station,platform_code,stop_id,stop_lat,stop_lon,stop_name,stop_url,wheelchair_boarding,zone_id,level_id,stop_code,stop_desc,stop_timezone,asw_node_id,asw_stop_id,create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by) VALUES
	 (0,NULL,NULL,'U142Z301',50.083096,14.436194,'Praha hl.n.',NULL,0,'P',NULL,NULL,NULL,NULL,142,301,NULL,'2024-02-06 05:23:14.828',NULL,NULL,'2024-02-06 05:23:14.828',NULL);

-- ropidgtfs_stop_times_actual
INSERT INTO ropidgtfs_stop_times_actual (arrival_time,arrival_time_seconds,departure_time,departure_time_seconds,drop_off_type,pickup_type,shape_dist_traveled,stop_headsign,stop_id,stop_sequence,trip_id,timepoint,create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by) VALUES
	 ('11:05:00',NULL,'11:05:00',NULL,'0','0',0.0,NULL,'U1384Z301',1,'1309_2530_231210',NULL,NULL,'2024-02-06 05:23:14.828',NULL,NULL,'2024-02-06 05:23:14.828',NULL),
	 ('11:10:30',NULL,'11:10:30',NULL,'0','0',6.965987,NULL,'U1592Z301',2,'1309_2530_231210',NULL,NULL,'2024-02-06 05:23:14.828',NULL,NULL,'2024-02-06 05:23:14.828',NULL),
	 ('11:13:00',NULL,'11:13:30',NULL,'0','0',9.234362,NULL,'U1383Z301',3,'1309_2530_231210',NULL,NULL,'2024-02-06 05:23:14.828',NULL,NULL,'2024-02-06 05:23:14.828',NULL),
	 ('11:15:30',NULL,'11:15:30',NULL,'0','0',10.599476,NULL,'U1553Z301',4,'1309_2530_231210',NULL,NULL,'2024-02-06 05:23:14.828',NULL,NULL,'2024-02-06 05:23:14.828',NULL),
	 ('11:16:00',NULL,'11:16:00',NULL,'1','1',10.977712,NULL,'T55045',5,'1309_2530_231210',NULL,NULL,'2024-02-06 05:23:14.828',NULL,NULL,'2024-02-06 05:23:14.828',NULL),
	 ('11:18:00',NULL,'11:18:00',NULL,'0','0',12.081005,NULL,'U1595Z301',6,'1309_2530_231210',NULL,NULL,'2024-02-06 05:23:14.828',NULL,NULL,'2024-02-06 05:23:14.828',NULL),
	 ('11:21:30',NULL,'11:22:00',NULL,'0','0',15.744629,NULL,'U1554Z301',7,'1309_2530_231210',NULL,NULL,'2024-02-06 05:23:14.828',NULL,NULL,'2024-02-06 05:23:14.828',NULL),
	 ('11:24:00',NULL,'11:24:00',NULL,'0','0',17.446669,NULL,'U1863Z301',8,'1309_2530_231210',NULL,NULL,'2024-02-06 05:23:14.828',NULL,NULL,'2024-02-06 05:23:14.828',NULL),
	 ('11:27:00',NULL,'11:27:00',NULL,'0','0',20.199308,NULL,'U2277Z301',9,'1309_2530_231210',NULL,NULL,'2024-02-06 05:23:14.828',NULL,NULL,'2024-02-06 05:23:14.828',NULL),
	 ('11:30:30',NULL,'11:31:00',NULL,'0','0',23.272876,NULL,'U1696Z301',10,'1309_2530_231210',NULL,NULL,'2024-02-06 05:23:14.828',NULL,NULL,'2024-02-06 05:23:14.828',NULL);
INSERT INTO ropidgtfs_stop_times_actual (arrival_time,arrival_time_seconds,departure_time,departure_time_seconds,drop_off_type,pickup_type,shape_dist_traveled,stop_headsign,stop_id,stop_sequence,trip_id,timepoint,create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by) VALUES
	 ('11:34:30',NULL,'11:34:30',NULL,'0','0',26.724406,NULL,'U1928Z301',11,'1309_2530_231210',NULL,NULL,'2024-02-06 05:23:14.828',NULL,NULL,'2024-02-06 05:23:14.828',NULL),
	 ('11:38:00',NULL,'11:38:30',NULL,'0','0',30.18869,NULL,'U2142Z301',12,'1309_2530_231210',NULL,NULL,'2024-02-06 05:23:14.828',NULL,NULL,'2024-02-06 05:23:14.828',NULL),
	 ('11:40:00',NULL,'11:40:00',NULL,'1','1',31.640481,NULL,'T54985',13,'1309_2530_231210',NULL,NULL,'2024-02-06 05:23:14.828',NULL,NULL,'2024-02-06 05:23:14.828',NULL),
	 ('11:41:30',NULL,'11:41:30',NULL,'0','0',33.55939,NULL,'U1175Z301',14,'1309_2530_231210',NULL,NULL,'2024-02-06 05:23:14.828',NULL,NULL,'2024-02-06 05:23:14.828',NULL),
	 ('11:45:00',NULL,'11:45:30',NULL,'0','0',36.897057,NULL,'U461Z301',15,'1309_2530_231210',NULL,NULL,'2024-02-06 05:23:14.828',NULL,NULL,'2024-02-06 05:23:14.828',NULL),
	 ('11:48:00',NULL,'11:48:00',NULL,'0','0',39.372307,NULL,'U1069Z301',16,'1309_2530_231210',NULL,NULL,'2024-02-06 05:23:14.828',NULL,NULL,'2024-02-06 05:23:14.828',NULL),
	 ('11:50:30',NULL,'11:51:00',NULL,'0','0',41.600968,NULL,'U453Z301',17,'1309_2530_231210',NULL,NULL,'2024-02-06 05:23:14.828',NULL,NULL,'2024-02-06 05:23:14.828',NULL),
	 ('11:53:30',NULL,'11:53:30',NULL,'0','0',44.317898,NULL,'U2781Z301',18,'1309_2530_231210',NULL,NULL,'2024-02-06 05:23:14.828',NULL,NULL,'2024-02-06 05:23:14.828',NULL),
	 ('11:57:00',NULL,'11:57:00',NULL,'0','0',46.670035,NULL,'U680Z301',19,'1309_2530_231210',NULL,NULL,'2024-02-06 05:23:14.828',NULL,NULL,'2024-02-06 05:23:14.828',NULL),
	 ('12:00:00',NULL,'12:00:00',NULL,'0','0',48.583367,NULL,'U463Z301',20,'1309_2530_231210',NULL,NULL,'2024-02-06 05:23:14.828',NULL,NULL,'2024-02-06 05:23:14.828',NULL);
INSERT INTO ropidgtfs_stop_times_actual (arrival_time,arrival_time_seconds,departure_time,departure_time_seconds,drop_off_type,pickup_type,shape_dist_traveled,stop_headsign,stop_id,stop_sequence,trip_id,timepoint,create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by) VALUES
	 ('12:03:00',NULL,'12:03:00',NULL,'1','1',50.333377,NULL,'T57073',21,'1309_2530_231210',NULL,NULL,'2024-02-06 05:23:14.828',NULL,NULL,'2024-02-06 05:23:14.828',NULL),
	 ('12:05:00',NULL,'12:05:00',NULL,'0','0',51.193462,NULL,'U142Z301',22,'1309_2530_231210',NULL,NULL,'2024-02-06 05:23:14.828',NULL,NULL,'2024-02-06 05:23:14.828',NULL);


-- ropidgtfs_routes_actual
INSERT INTO ropidgtfs_routes_actual (agency_id,is_night,route_color,route_desc,route_id,route_long_name,route_short_name,route_text_color,route_type,route_url,is_regional,is_substitute_transport,create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by) VALUES
	 ('99','0','251E62',NULL,'L1309','Lysá nad Labem - Praha - Říčany - Strančice - Čerčany - Benešov u Prahy','S9','FFFFFF',2,'https://cz/linka/S9','1','0',NULL,'2024-02-06 05:23:14.828',NULL,NULL,'2024-02-06 05:23:14.828',NULL);


-- ropidgtfs_trips_actual
INSERT INTO ropidgtfs_trips_actual (bikes_allowed,block_id,direction_id,exceptional,route_id,service_id,shape_id,trip_headsign,trip_id,wheelchair_accessible,create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by,trip_operation_type,trip_short_name) VALUES
	 (1,NULL,0,0,'L1309','1111111-2','L1249V1','Praha hl.n.','1309_2530_231210',1,NULL,'2024-02-06 05:23:14.828',NULL,NULL,'2024-02-06 05:23:14.828',NULL,NULL,'Os 2530');


-- vehiclepositions_trips
INSERT INTO vehiclepositions_trips (cis_line_id,cis_trip_number,run_number,cis_line_short_name,created_at,gtfs_route_id,gtfs_route_short_name,gtfs_trip_id,id,updated_at,start_cis_stop_id,start_cis_stop_platform_code,start_time,start_timestamp,vehicle_type_id,wheelchair_accessible,create_batch_id,created_by,update_batch_id,updated_by,agency_name_scheduled,origin_route_name,agency_name_real,vehicle_registration_number,gtfs_trip_headsign,start_asw_stop_id,gtfs_route_type,gtfs_block_id,last_position_id,is_canceled,end_timestamp,gtfs_trip_short_name,last_position_context,internal_run_number,internal_route_name,gtfs_date,provider_source_type,gtfs_direction_id,gtfs_shape_id) VALUES
	 ('none',2530,NULL,'S9',concat(CURRENT_DATE,'T10:05:52Z')::timestamp,'L1309','S9','1309_2530_231210',concat(CURRENT_DATE,'T10:05:00Z_2530'),concat(CURRENT_DATE,'T11:07:25Z')::timestamp,5455106,NULL,'11:05:00',concat(CURRENT_DATE,'T10:05:00Z')::timestamp,0,true,NULL,NULL,NULL,NULL,'ČESKÉ DRÁHY',NULL,NULL,NULL,'Praha hl.n.',NULL,2,NULL,2679563888,false,concat(CURRENT_DATE,'T11:05:00Z')::timestamp,'Os 2530',NULL,NULL,NULL,CURRENT_DATE,'1',0,'L1249V1');

-- vehiclepositions_positions
INSERT INTO vehiclepositions_positions (created_at,delay,delay_stop_arrival,delay_stop_departure,next_stop_id,shape_dist_traveled,is_canceled,lat,lng,origin_time,origin_timestamp,is_tracked,trips_id,create_batch_id,created_by,update_batch_id,updated_at,updated_by,id,bearing,cis_last_stop_id,cis_last_stop_sequence,last_stop_id,last_stop_sequence,next_stop_sequence,speed,last_stop_arrival_time,last_stop_departure_time,next_stop_arrival_time,next_stop_departure_time,asw_last_stop_id,state_process,state_position,this_stop_id,this_stop_sequence,tcp_event,last_stop_headsign,last_stop_name,next_stop_name,this_stop_name,valid_to,scheduled_timestamp) VALUES
	 (concat(CURRENT_DATE,'T11:06:05Z')::timestamp,35,60,NULL,'U142Z301',51.193,false,50.0829926,14.4362745,'12:05:35',concat(CURRENT_DATE,'T11:05:35Z')::timestamp,true,concat(CURRENT_DATE,'T10:05:00Z_2530'),NULL,NULL,NULL,'2024-02-06 12:06:06.762',NULL,2679563888,25,5457076,19,'U142Z301',22,22,NULL,concat(CURRENT_DATE,'T11:05:00Z')::timestamp,concat(CURRENT_DATE,'T11:05:00Z')::timestamp,concat(CURRENT_DATE,'T11:05:00Z')::timestamp,concat(CURRENT_DATE,'T11:05:00Z')::timestamp,NULL,'processed','at_stop','U142Z301',22,NULL,NULL,'Praha hl.n.',NULL,NULL,NULL,NULL);

-- vehiclepositions_cis_stops
insert into vehiclepositions_cis_stops (rt_trip_id, cis_stop_group_id, cis_stop_platform_code, created_at, updated_at) VALUES
    (concat(CURRENT_DATE,'T10:05:00Z_2530'), 5457076, '1A/1', now(), now());
