-- 0
INSERT INTO jis_infotexts(id, severity_level, display_type, active_period_start, active_period_end, description_text, created_at, updated_at)
    VALUES ('40507cee-6468-488e-ba78-e36d9c20e67c', 'SEVERE', 'INLINE', '2024-02-07 13:11:00.000+01', NULL, '{"cs": "Provoz tramvají směrem z centra na Starý Hloubětín z důvodu údržby přerušen do neděle 18:00. Zaveden autobus X-8.", "en": "No tram service in direction to Starý Hloubětín until Sunday 18:00 due to track maintenance. Replacement bus X-8 instated."}', '2024-02-07 13:24:55.000', now());

INSERT INTO jis_infotexts_ropidgtfs_stops(infotext_id, stop_id, created_at, updated_at)
    VALUES ('40507cee-6468-488e-ba78-e36d9c20e67c', 'U1131Z1P', '2024-04-19 09:00:00+02', now() + interval '1 day');

-- 1
INSERT INTO jis_infotexts(id, severity_level, display_type, active_period_start, active_period_end, description_text, created_at, updated_at)
    VALUES ('6bdc3fdb-dd18-438b-878a-7972bddb0d92', 'SEVERE', 'INLINE', '2024-03-05 08:30:00.000+02', NULL, '{"cs": "cca 8:30 - 9:15: VŠECHNY LINKY ODKLON do zast. BUDĚJOVICKÁ, stanice Kačerov uzavřena.", "en": "approx. 8:30 - 9:15: ALL LINES DEPARTURE to stop. BUDĚJOVICKÁ, station Kačerov closed."}', '2024-02-07 13:24:55.000', now());

INSERT INTO jis_infotexts_ropidgtfs_stops(infotext_id, stop_id, created_at, updated_at)
    VALUES ('6bdc3fdb-dd18-438b-878a-7972bddb0d92', 'U1132Z1P', '2024-04-19 09:00:00+02', now() + interval '1 day'),
('6bdc3fdb-dd18-438b-878a-7972bddb0d92', 'U663Z2P', '2024-04-19 09:00:00+02', now() + interval '1 day');

-- 2
INSERT INTO jis_infotexts(id, severity_level, display_type, active_period_start, active_period_end, description_text, created_at, updated_at)
    VALUES ('eba39046-86d2-4f8c-aa39-932dfcfd6d0f', 'SEVERE', 'INLINE', '2024-03-04 13:11:00.000+02', NULL, '{"cs": "Cizí dopravní nehoda ve Psárském lese. Linka 332 je odkloněna přes Libeř, přičemž neobsluhuje zastávku Psáry,Domov Laguna.", "en": "Traffic accident in the Psar Forest. Line 332 is diverted via Libeř, while it does not serve the stop Psáry,Domov Laguna.."}', '2024-02-07 13:24:55.000', now());

INSERT INTO jis_infotexts_ropidgtfs_stops(infotext_id, stop_id, created_at, updated_at)
    VALUES ('eba39046-86d2-4f8c-aa39-932dfcfd6d0f', 'U713Z102P', '2024-04-19 09:00:00+02', now() + interval '1 day'),
('eba39046-86d2-4f8c-aa39-932dfcfd6d0f', 'U921Z102P', '2024-04-19 09:00:00+02', now() + interval '1 day');

-- 3
INSERT INTO jis_infotexts(id, severity_level, display_type, active_period_start, active_period_end, description_text, created_at, updated_at)
    VALUES ('8556cf18-786b-453a-a516-85c8fe2b9878', 'SEVERE', 'INLINE', '2024-03-02 10:00:00.000+02', NULL, '{"cs": "Dnes v noci tramvaje mimo provoz. Zastávky náhradních autobusů X8 a X90: směr Lehovec - vlevo, u Lidlu; směr Palmovka - na chodníku u původní tramvajové zastávky.", "en": "Trams out of service tonight. Stopping points for replacement buses X8 and X90: direction Lehovec - left, near Lidl; direction Palmovka - on the pavement near the original tram stop."}', '2024-02-07 13:24:55.000', now());

INSERT INTO jis_infotexts_ropidgtfs_stops(infotext_id, stop_id, created_at, updated_at)
    VALUES ('8556cf18-786b-453a-a516-85c8fe2b9878', 'U1334Z2P', '2024-04-19 09:00:00+02', now() + interval '1 day');

-- 4
INSERT INTO jis_infotexts(id, severity_level, display_type, active_period_start, active_period_end, description_text, created_at, updated_at)
    VALUES ('9923c199-d61c-47fa-9f7e-14d01473eb18', 'SEVERE', 'GENERAL', '2024-03-18 16:00:00.000+02', NULL, '{"cs": "Dnes v sobotu 8. konání půl maratonu. Od 16h do 23h změna trasy linek 392, 395, 500, 501, 502, 503, 506, 517, 531, 754. Podrobnosti na zastávkách a pid.cz/zmeny", "en": "Today on Saturday the 8th holding of the half marathon. From 16h to 23h change of route of lines 392, 395, 500, 501, 502, 503, 506, 517, 531, 754. Details at the stops and pid.cz/changes"}', '2024-02-07 13:24:55.000', now());

INSERT INTO jis_infotexts_ropidgtfs_stops(infotext_id, stop_id, created_at, updated_at)
    VALUES ('9923c199-d61c-47fa-9f7e-14d01473eb18', 'U376Z105P', '2024-04-19 09:00:00+02', now() + interval '1 day'),
('8556cf18-786b-453a-a516-85c8fe2b9878', 'U21Z1P', '2024-04-19 09:00:00+02', now() + interval '1 day');

-- 5
INSERT INTO jis_infotexts(id, severity_level, display_type, active_period_start, active_period_end, description_text, created_at, updated_at)
    VALUES ('0a3c97d0-43d4-4882-8a1d-27079044f51b', 'SEVERE', 'INLINE', '2024-03-25 10:00:00.000+02', NULL, '{"cs": "Do 15:00 je zastávka mimo provoz. Linky 1, 12 a 25 odjíždí ze stanoviště D, od nábřeží. Linky 8 jede ze stanoviště E od Výstaviště, linka 26 zde neprojíždí.", "en": "The stop is out of service until 15:00. Lines 1, 12 and 25 depart from Station D, from the waterfront. Line 8 departs from station E from the Exhibition Grounds, line 26 does not run here."}', '2024-02-07 13:24:55.000', now());

INSERT INTO jis_infotexts_ropidgtfs_stops(infotext_id, stop_id, created_at, updated_at)
    VALUES ('0a3c97d0-43d4-4882-8a1d-27079044f51b', 'U476Z104P', '2024-04-19 09:00:00+02', now() + interval '1 day'),
('0a3c97d0-43d4-4882-8a1d-27079044f51b', 'U476Z103P', '2024-04-19 09:00:00+02', now() + interval '1 day');

-- 6
INSERT INTO jis_infotexts(id, severity_level, display_type, active_period_start, active_period_end, description_text, created_at, updated_at)
    VALUES ('ba52df69-5db3-4c60-94be-8f4b5442c1e0', 'SEVERE', 'INLINE', '2024-03-11 13:11:00.000+02', NULL, '{"cs": "Do 16:00 zde nejezdí tramvaje z důvodu údržby tratě, mezi Palmovkou a Lehovcem využijte náhradní autobus X-8.", "en": "Trams do not run here until 16:00 due to track maintenance, use the alternative bus X-8 between Palmovka and Lehovec."}', '2024-02-07 13:24:55.000', now());

INSERT INTO jis_infotexts_ropidgtfs_stops(infotext_id, stop_id, created_at, updated_at)
    VALUES ('ba52df69-5db3-4c60-94be-8f4b5442c1e0', 'U953Z102P', '2024-04-19 09:00:00+02', now() + interval '1 day'),
('ba52df69-5db3-4c60-94be-8f4b5442c1e0', 'U21Z2P', '2024-04-19 09:00:00+02', now() + interval '1 day');

-- 7
INSERT INTO jis_infotexts(id, severity_level, display_type, active_period_start, active_period_end, description_text, created_at, updated_at)
    VALUES ('ee330c7f-cf53-4762-a231-08af9f22442f', 'SEVERE', 'INLINE', '2024-03-04 10:00:00.000+02', '2024-08-10 18:00:00.000+02', '{"cs": "Do 18:00 údržba tramvajové trati Hlubočepy – Slivenec. Tram 12 a 20 zkráceny do zastávky Hlubočepy.", "en": "Until 18:00 maintenance of the tram line Hlubočepy - Slivenec. Tram 12 and 20 shortened to the Hlubočepy stop."}', '2024-02-07 13:24:55.000', now());

INSERT INTO jis_infotexts_ropidgtfs_stops(infotext_id, stop_id, created_at, updated_at)
    VALUES ('ee330c7f-cf53-4762-a231-08af9f22442f', 'U118Z102P', '2024-04-19 09:00:00+02', now() + interval '1 day');

-- 8
INSERT INTO jis_infotexts(id, severity_level, display_type, active_period_start, active_period_end, description_text, created_at, updated_at)
    VALUES ('265cdc5b-d6da-48e0-a114-6901200c4d46', 'SEVERE', 'GENERAL', '2024-03-27 10:00:00.000+02', '2024-09-11 00:00:00.000+02', '{"cs": "Do 10.9. 24:00 nepojedou tramvaje mezi Výstavištěm a Strossmayerovým náměstím. Pro cestu na Strossmayerovo náměstí použijte linky 12 nebo 17.", "en": "Trams will not run between Výstaviště and Strossmayer Square until 24:00 on 24 April. Use lines 12 or 17 to get to Strossmayer Square."}', '2024-02-07 13:24:55.000', now());

INSERT INTO jis_infotexts_ropidgtfs_stops(infotext_id, stop_id, created_at, updated_at)
    VALUES ('265cdc5b-d6da-48e0-a114-6901200c4d46', 'U462Z102P', '2024-04-19 09:00:00+02', now() + interval '1 day');

-- 9
INSERT INTO jis_infotexts(id, severity_level, display_type, active_period_start, active_period_end, description_text, created_at, updated_at)
    VALUES ('adfd2cef-9c2c-4ed0-bd0d-be0b3216faad', 'SEVERE', 'INLINE', '2024-03-28 09:00:00.000+02', '2024-09-22 00:00:00.000+02', '{"cs": "Do nedělní půlnoci přerušen provoz odsud na Výstaviště. Linka 6 jede přes Pražskou tržnici. Polovina spojů 17 odkloněna přes Pražskou tržnici a Nádraží Holešovice. Druhá polovina pokračuje jako 27 přes Letnou na Špejchar. Využijte náhradní autobus X6.", "en": "Until Sunday midnight no tram service from here to Výstaviště. Tram 6 diverted via Pražská tržnice. Every other tram 17 diverted via Pražská tržnice and Nádraží Holešovice. The rest of 17 turns left to end at Špejchar. Use substitute bus X6."}', '2024-02-07 13:24:55.000', now());

INSERT INTO jis_infotexts_ropidgtfs_stops(infotext_id, stop_id, created_at, updated_at)
    VALUES ('adfd2cef-9c2c-4ed0-bd0d-be0b3216faad', 'U209Z102P', '2024-04-19 09:00:00+02', now() + interval '1 day');

-- 10
INSERT INTO jis_infotexts(id, severity_level, display_type, active_period_start, active_period_end, description_text, created_at, updated_at)
    VALUES ('15bd72da-a3da-484b-b592-742deab84b55', 'WARNING', 'INLINE', '2024-03-22 13:11:00.000+02', '2024-10-06 13:11:00.000+02', '{"cs": "Do pondělní půlnoci probíhá výluka metra C mezi I. P. Pavlova a Nádraží Holešovice. Náhradní autobus X-C navazuje ze zastávky I. P. Pavlova.", "en": "Until Monday midnight, the C metro station is closed between I. P. Pavlova and Nádraží Holešovice. A replacement bus X-C will connect from the stop I. P. Pavlova."}', '2024-02-07 13:24:55.000', now());

INSERT INTO jis_infotexts_ropidgtfs_stops(infotext_id, stop_id, created_at, updated_at)
    VALUES ('15bd72da-a3da-484b-b592-742deab84b55', 'U476Z102P', '2024-04-19 09:00:00+02', now() + interval '1 day'),
('15bd72da-a3da-484b-b592-742deab84b55', 'U276Z106P', '2024-04-19 09:00:00+02', now() + interval '1 day'),
('15bd72da-a3da-484b-b592-742deab84b55', 'U181Z1P', '2024-04-19 09:00:00+02', now() + interval '1 day'),
('15bd72da-a3da-484b-b592-742deab84b55', 'U1131Z2P', '2024-04-19 09:00:00+02', now() + interval '1 day');

-- 11
INSERT INTO jis_infotexts(id, severity_level, display_type, active_period_start, active_period_end, description_text, created_at, updated_at)
    VALUES ('a7446544-a907-4b9f-877b-b29458ad9c82', 'SEVERE', 'INLINE', '2024-03-16 10:00:00.000+02', '2024-05-15 00:00:00.000+02', '{"cs": "Do pondělní půlnoci probíhá výluka metra C mezi Nádražím Holešovice a I. P. Pavlova. Náhradní autobus X-C odjíždí z nástupiště C (jako bus 201).", "en": "Until Monday midnight, the C metro station is closed between Nádraž Holešovice and I. P. Pavlova. The replacement bus X-C departs from platform C (as bus 201)."}', '2024-02-07 13:24:55.000', now());

INSERT INTO jis_infotexts_ropidgtfs_stops(infotext_id, stop_id, created_at, updated_at)
    VALUES ('a7446544-a907-4b9f-877b-b29458ad9c82', 'U1131Z1P', '2024-04-19 09:00:00+02', now() + interval '1 day'),
('a7446544-a907-4b9f-877b-b29458ad9c82', 'U276Z106P', '2024-04-19 09:00:00+02', now() + interval '1 day'),
('a7446544-a907-4b9f-877b-b29458ad9c82', 'U181Z1P', '2024-04-19 09:00:00+02', now() + interval '1 day'),
('a7446544-a907-4b9f-877b-b29458ad9c82', 'U1131Z2P', '2024-04-19 09:00:00+02', now() + interval '1 day');

-- 12
INSERT INTO jis_infotexts(id, severity_level, display_type, active_period_start, active_period_end, description_text, created_at, updated_at)
    VALUES ('07a8449a-52b2-4266-a611-89d2b8758637', 'SEVERE', 'INLINE', '2024-09-10 17:00:00.000+02', '2024-11-20 00:00:00.000+02', '{"cs": "Do pondělní půlnoci probíhá výluka metra C mezi Nádražím Holešovice a I. P. Pavlova. Zastávky náhradního autobusu X-C jsou na Florenci nahoře na magistrále.", "en": "Until Monday midnight, the C metro station is closed between Nádraž Holešovice and I. P. Pavlova. The stops of the replacement bus X-C are at Florence at the top of the main line."}', '2024-02-07 13:24:55.000', now());

INSERT INTO jis_infotexts_ropidgtfs_stops(infotext_id, stop_id, created_at, updated_at)
    VALUES ('07a8449a-52b2-4266-a611-89d2b8758637', 'U1131Z2P', '2024-04-19 09:00:00+02', now() + interval '1 day');

-- 13
INSERT INTO jis_infotexts(id, severity_level, display_type, active_period_start, active_period_end, description_text, created_at, updated_at)
    VALUES ('59a0b461-df44-42a6-940f-2abf0fd4e980', 'INFO', 'GENERAL', '2024-03-01 00:00.000+02', '2024-06-12 00:00.000+02', '{"cs": "Do pondělí 12.6. bude uzavřen tento vstup do metra. Vstup od zastávky tramvaje v ulici Plynární zůstává v provozu.", "en": "This entrance to the metro will be closed until Monday 12 June. The entrance from the tram stop in Plynární Street will remain open."}', '2024-02-07 13:24:55.000', now());

INSERT INTO jis_infotexts_ropidgtfs_stops(infotext_id, stop_id, created_at, updated_at)
    VALUES ('59a0b461-df44-42a6-940f-2abf0fd4e980', 'U52Z4P', '2024-04-19 09:00:00+02', now() + interval '1 day'),
('59a0b461-df44-42a6-940f-2abf0fd4e980', 'U1341Z1P', '2024-04-19 09:00:00+02', now() + interval '1 day');

-- 14
INSERT INTO jis_infotexts(id, severity_level, display_type, active_period_start, active_period_end, description_text, created_at, updated_at)
    VALUES ('e9a15133-e606-406b-b492-9af05c643e7a', 'SEVERE', 'INLINE', '2024-03-27 00:00:00.000+02', '2024-09-22 00:00:00.000+02', '{"cs": "Do pondělí probíhá výluka metra C mezi I. P. Pavlova a Nádraží Holešovice. Náhradní autobus X-C navazuje ze zastávky I. P. Pavlova.", "en": "Until Monday, the C metro station is closed between I. P. Pavlova and Nádraží Holešovice. The replacement bus X-C will connect from the stop I. P. Pavlova."}', '2024-02-07 13:24:55.000', now());

INSERT INTO jis_infotexts_ropidgtfs_stops(infotext_id, stop_id, created_at, updated_at)
    VALUES ('e9a15133-e606-406b-b492-9af05c643e7a', 'U52Z2P', '2024-04-19 09:00:00+02', now() + interval '1 day');

-- 15
INSERT INTO jis_infotexts(id, severity_level, display_type, active_period_start, active_period_end, description_text, created_at, updated_at)
    VALUES ('cdcc2d15-2cdc-4205-b82a-5cc819a79738', 'SEVERE', 'INLINE', '2023-03-27 00:00:00.000+02', NULL, '{"cs": "Nejede to."}', '2024-02-07 13:24:55.000', now());

INSERT INTO jis_infotexts_ropidgtfs_stops(infotext_id, stop_id, created_at, updated_at)
    VALUES ('cdcc2d15-2cdc-4205-b82a-5cc819a79738', 'U142Z301', '2024-04-19 09:00:00+02', now() + interval '1 day');

