INSERT INTO "ropidgtfs_stops_actual" (level_id,location_type,parent_station,platform_code,stop_id,stop_lat,stop_lon,stop_name,wheelchair_boarding,zone_id,asw_node_id,asw_stop_id) VALUES
	 ('U1040L2',0,'U1040S1','1','U1040Z101P',50.069991,14.403664,'Anděl',0,'P',1040,101),
	 ('U1040L2',0,'U1040S1','2','U1040Z102P',50.069967,14.403892,'Anděl',0,'P',1040,102),
	 (NULL,0,NULL,'V','U1040Z16P',50.069199,14.405263,'Na Knížecí',0,'P',1040,16),
	 (NULL,0,NULL,'P','U1040Z18P',50.068867,14.404408,'Na Knížecí',0,'P',1040,18),
	 (NULL,0,NULL,'O','U1040Z19P',50.068737,14.404152,'Na Knížecí',0,'P',1040,19),
	 (NULL,0,NULL,'R','U1040Z21P',50.068611,14.404253,'Na Knížecí',0,'P',1040,21),
	 (NULL,0,NULL,'J','U1040Z5P',50.06852,14.406499,'Na Knížecí',2,'P',1040,5),
	 (NULL,0,NULL,'K','U1040Z6P',50.066879,14.407246,'Na Knížecí',2,'P',1040,6);
