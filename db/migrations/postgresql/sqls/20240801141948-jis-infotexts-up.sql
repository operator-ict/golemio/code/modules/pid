create table jis_infotexts (
    id uuid not null,
    severity_level varchar(255) not null,
    display_type varchar(255) not null,
    active_period_start timestamptz not null,
    active_period_end timestamptz,
    description_text jsonb not null,
    created_at timestamptz not null,
    updated_at timestamptz not null,
    constraint jis_infotexts_pk primary key (id)
);

create table jis_infotexts_ropidgtfs_stops (
    infotext_id uuid not null,
    stop_id varchar(50) not null,
    created_at timestamptz not null,
    updated_at timestamptz not null,
    constraint jis_infotexts_ropidgtfs_stops_pk primary key (infotext_id, stop_id)
);
