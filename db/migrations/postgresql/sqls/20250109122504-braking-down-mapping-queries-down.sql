DROP FUNCTION get_service_ids_from_ropidgtfs_calendar(varchar, date);

DROP FUNCTION normalize_departure_time(varchar);

DROP FUNCTION get_stop_ids_from_cis(int4, varchar, varchar);

DROP FUNCTION is_within_12_hours(date, text, timestamptz);

DROP FUNCTION match_trip_id(text, int4);

DROP FUNCTION get_wanted_trip(varchar);

