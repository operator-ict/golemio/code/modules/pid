DROP INDEX vehiclepositions_trips_gtfs_date_idx;

ALTER TABLE vehiclepositions_trips DROP COLUMN gtfs_date;
ALTER TABLE vehiclepositions_trips_history DROP COLUMN gtfs_date;