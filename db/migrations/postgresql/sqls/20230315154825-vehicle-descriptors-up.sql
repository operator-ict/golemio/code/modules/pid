CREATE TYPE vehicle_descriptor_state AS ENUM ('dilny', 'doco', 'nzr', 'zar');

CREATE TABLE vehiclepositions_vehicle_descriptors (
    id int NOT NULL,
    "state" vehicle_descriptor_state NOT NULL,
    registration_number int NOT NULL,
    registration_number_index smallint NOT NULL,
    license_plate varchar(20),
    "operator" varchar(50) NOT NULL,
    manufacturer varchar(50) NOT NULL,
    "type" varchar(100) NOT NULL,
    traction varchar(20) NOT NULL,
    gtfs_route_type smallint NOT NULL,
    is_air_conditioned boolean NOT NULL,
    has_usb_chargers boolean NOT NULL,
    paint text,
    thumbnail_url text,
    photo_url text,

    -- audit fields
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),

    CONSTRAINT vp_vehicle_descriptors_pkey PRIMARY KEY (id)
);

CREATE UNIQUE INDEX vp_vehicle_descriptors_unique_idx ON vehiclepositions_vehicle_descriptors (registration_number, gtfs_route_type);
