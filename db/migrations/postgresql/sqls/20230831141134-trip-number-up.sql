ALTER TABLE ropidgtfs_run_numbers ADD COLUMN trip_number SMALLINT;

ALTER TABLE ropidgtfs_precomputed_trip_schedule ADD COLUMN trip_number SMALLINT;
