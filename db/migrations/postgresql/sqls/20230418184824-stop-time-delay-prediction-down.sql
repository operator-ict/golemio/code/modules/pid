drop view v_vehiclepositions_stop_time_delay_prediction;

alter table ropidgtfs_stop_times drop column computed_dwell_time_seconds;
alter table ropidgtfs_stop_times_actual drop column computed_dwell_time_seconds;

alter table vehiclepositions_trips drop column provider_source_type;
alter table vehiclepositions_trips_history drop column provider_source_type;

drop function calculate_dwell_time_seconds (varchar, varchar);
drop function predict_delay_seconds (int, smallint, varchar);
