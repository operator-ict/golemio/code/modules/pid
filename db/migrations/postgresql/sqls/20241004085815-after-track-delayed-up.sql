-- Add after_track_delayed state to possible states for vehicle positions
CREATE OR REPLACE VIEW v_vehiclepositions_past_stop_times
AS SELECT sub.trips_id AS rt_trip_id,
    sub.last_stop_sequence AS stop_sequence,
    sub.last_stop_id AS stop_id,
    sub.last_stop_arrival_time AS stop_arrival,
    sub.last_stop_departure_time AS stop_departure,
    sub.stop_arr_delay,
    sub.stop_dep_delay,
    sub.stop_name,
    sub.lat,
    sub.lng
   FROM ( SELECT rt_position.trips_id,
            rt_position.last_stop_sequence,
            rt_position.last_stop_id,
            rt_position.last_stop_arrival_time,
            rt_position.last_stop_departure_time,
            rs.stop_name,
            rs.stop_lat AS lat,
            rs.stop_lon AS lng,
            max(rt_position.delay_stop_arrival) AS stop_arr_delay,
            min(rt_position.delay_stop_departure) AS stop_dep_delay
           FROM vehiclepositions_positions rt_position
             JOIN ropidgtfs_stops rs ON rs.stop_id::text = rt_position.last_stop_id::text
          WHERE (rt_position.delay_stop_arrival IS NOT NULL OR rt_position.delay_stop_departure IS NOT NULL) AND (rt_position.state_position::text = ANY (ARRAY['on_track'::character varying::text, 'at_stop'::character varying::text, 'after_track'::character varying::text, 'after_track_delayed'::character varying::text]))
          GROUP BY rt_position.trips_id, rt_position.last_stop_sequence, rt_position.last_stop_id, rt_position.last_stop_arrival_time, rt_position.last_stop_departure_time, rs.stop_name, rs.stop_lat, rs.stop_lon) sub
  ORDER BY sub.trips_id, sub.last_stop_sequence;

-- Add after_track_delayed state to possible states for vehicle positions
CREATE OR REPLACE VIEW v_public_vehiclepositions_past_stop_times AS
SELECT
    sub.trips_id AS rt_trip_id,
    sub.last_stop_sequence AS stop_sequence,
    sub.stop_arr_delay,
    sub.stop_dep_delay,
    sub.last_stop_id AS stop_id
FROM (
    SELECT
        rt_position.trips_id,
        rt_position.last_stop_id,
        rt_position.last_stop_sequence,
        CASE WHEN (rt_position.last_stop_departure_time::time +(interval '1 second' * min(rt_position.delay_stop_departure)) > rt_position.next_stop_arrival_time::time +(interval '1 second' * max(rt_position.delay_stop_arrival))) THEN
            NULL
        ELSE
            max(rt_position.delay_stop_arrival)
        END AS stop_arr_delay,
        CASE WHEN (rt_position.last_stop_arrival_time::time +(interval '1 second' * max(rt_position.delay_stop_arrival)) > rt_position.last_stop_departure_time::time +(interval '1 second' * min(rt_position.delay_stop_departure))) THEN
            NULL
        ELSE
            min(rt_position.delay_stop_departure)
        END AS stop_dep_delay
    FROM
        vehiclepositions_positions rt_position
    WHERE (rt_position.delay_stop_arrival IS NOT NULL
        OR rt_position.delay_stop_departure IS NOT NULL)
    AND (rt_position.state_position::text = ANY (ARRAY['on_track'::character varying::text, 'at_stop'::character varying::text, 'after_track'::character varying::text, 'after_track_delayed'::character varying::text]))
GROUP BY
    rt_position.trips_id,
    rt_position.last_stop_id,
    rt_position.last_stop_sequence,
    rt_position.last_stop_arrival_time,
    rt_position.last_stop_departure_time,
    rt_position.next_stop_arrival_time) sub
ORDER BY
    sub.trips_id,
    sub.last_stop_sequence;

