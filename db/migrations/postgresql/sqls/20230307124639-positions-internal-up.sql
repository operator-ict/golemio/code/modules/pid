-- Add origin_route_name to ropidgtfs_scheduled_trips
ALTER TABLE ropidgtfs_scheduled_trips
    ADD origin_route_name varchar(50);

CREATE INDEX ropidgtfs_departures_origin_route_name_idx ON ropidgtfs_scheduled_trips_actual (origin_route_name);
CREATE INDEX ropidgtfs_departures_start_timestamp_idx ON ropidgtfs_scheduled_trips_actual (start_timestamp);

UPDATE ropidgtfs_scheduled_trips_actual
    SET origin_route_name = right(origin_route_id, -1);

-- Rename sequence_id to run_number
ALTER TABLE vehiclepositions_trips
    RENAME sequence_id TO run_number;

ALTER TABLE vehiclepositions_trips_history
    RENAME sequence_id TO run_number;

-- Add internal_run_number and internal_route_name
ALTER TABLE vehiclepositions_trips
    ADD internal_run_number int,
    ADD internal_route_name varchar(50);

ALTER TABLE vehiclepositions_trips_history
    ADD internal_run_number int,
    ADD internal_route_name varchar(50);

-- Copy data from run_number and origin_route_name to internal_run_number and internal_route_name
UPDATE vehiclepositions_trips
    SET internal_run_number = run_number,
        internal_route_name = origin_route_name;

UPDATE vehiclepositions_trips_history
    SET internal_run_number = run_number,
        internal_route_name = origin_route_name;
