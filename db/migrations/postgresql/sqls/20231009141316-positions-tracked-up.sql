drop view v_vehiclepositions_alerts_last_position;
drop view v_vehiclepositions_all_trips_with_last_position;
drop view v_vehiclepositions_trip_position_vehicle_info;

alter table vehiclepositions_positions rename column tracking to is_tracked;
alter table vehiclepositions_positions alter column is_tracked type boolean using is_tracked::boolean;

alter table vehiclepositions_positions_history rename column tracking to is_tracked;
alter table vehiclepositions_positions_history alter column is_tracked type boolean using is_tracked::boolean;

-- recreate views
create view v_vehiclepositions_alerts_last_position as
  SELECT t2.created_at,
    t2.updated_at,
    t2.delay,
    t2.delay_stop_arrival,
    t2.delay_stop_departure,
    t2.next_stop_id,
    t2.shape_dist_traveled,
    t2.is_canceled,
    t2.lat,
    t2.lng,
    t2.origin_time,
    t2.origin_timestamp,
    t2.is_tracked,
    t2.trips_id,
    t2.create_batch_id,
    t2.created_by,
    t2.update_batch_id,
    t2.updated_by,
    t2.id,
    t2.bearing,
    t2.cis_last_stop_id,
    t2.cis_last_stop_sequence,
    t2.last_stop_id,
    t2.last_stop_sequence,
    t2.next_stop_sequence,
    t2.speed,
    t2.last_stop_arrival_time,
    t2.last_stop_departure_time,
    t2.next_stop_arrival_time,
    t2.next_stop_departure_time,
    t2.asw_last_stop_id,
    t1.gtfs_route_id,
    t1.gtfs_route_short_name,
    t1.gtfs_route_type,
    t1.gtfs_trip_id,
    t1.gtfs_trip_headsign,
    t1.vehicle_type_id,
    t1.wheelchair_accessible
   FROM (vehiclepositions_trips t1
     LEFT JOIN vehiclepositions_positions t2 ON ((t1.last_position_id = t2.id)))
  WHERE ((t2.updated_at > (now() - '00:10:00'::interval)) AND ((t2.state_position)::text = ANY (ARRAY[('on_track'::character varying)::text, ('off_track'::character varying)::text, ('at_stop'::character varying)::text, ('before_track'::character varying)::text, ('after_track'::character varying)::text, ('canceled'::character varying)::text])))
  ORDER BY t2.trips_id, t2.updated_at DESC;

comment on view v_vehiclepositions_alerts_last_position is '
Used only for alerting purposes.
';

create view v_vehiclepositions_all_trips_with_last_position as
select
    vt.id,
	vt.agency_name_real,
	vt.agency_name_scheduled,
	vt.cis_line_id,
	vt.cis_trip_number,
	vt.gtfs_route_id,
	vt.gtfs_route_short_name,
	vt.gtfs_route_type,
	vt.gtfs_trip_headsign,
	vt.gtfs_trip_short_name,
	vt.gtfs_trip_id,
	vt.origin_route_name,
	vt.run_number,
	vt.vehicle_registration_number,
    vt.vehicle_type_id,
    vt.wheelchair_accessible,
    vt.updated_at,
	convert_timestamptz_to_iso_string(vt.start_timestamp) as start_timestamp_isostring,
	vp.bearing,
	vp.delay,
	vp.delay_stop_arrival,
	vp.delay_stop_departure,
	vp.is_canceled,
	vp.last_stop_id,
	vp.last_stop_sequence,
    vp.last_stop_headsign,
    vp.lat,
    vp.lng,
	vp.next_stop_id,
	vp.next_stop_sequence,
	vp.shape_dist_traveled,
	vp.speed,
	vp.state_position,
	vp.is_tracked,
	convert_timestamptz_to_iso_string(vp.last_stop_arrival_time) as last_stop_arrival_time_isostring,
	convert_timestamptz_to_iso_string(vp.last_stop_departure_time) as last_stop_departure_time_isostring,
	convert_timestamptz_to_iso_string(vp.next_stop_arrival_time) as next_stop_arrival_time_isostring,
	convert_timestamptz_to_iso_string(vp.next_stop_departure_time) as next_stop_departure_time_isostring,
	convert_timestamptz_to_iso_string(vp.origin_timestamp) as origin_timestamp_isostring
from
	vehiclepositions_trips vt
inner join vehiclepositions_positions vp on
	vt.last_position_id = vp.id
	and (vp.valid_to is null
		or vp.valid_to >= now())
	and vp.state_process = 'processed';

comment on view v_vehiclepositions_all_trips_with_last_position is '
View of all trips with last processed position. Used for serving data to the vehiclepositions output API.
';

create view v_vehiclepositions_trip_position_vehicle_info as
	select
		"trip->last_position"."delay" as "delay_seconds",
		"trip"."wheelchair_accessible" as "real_wheelchair_accessible",
		"trip->last_position"."is_canceled" as "is_canceled",
		"trip"."agency_name_real" as "trip.agency_name_real",
		"trip"."agency_name_scheduled" as "trip.agency_name_scheduled",
		"trip"."cis_line_id" as "trip.cis_line_id",
		"trip"."cis_line_short_name" as "trip.cis_line_short_name",
		"trip"."cis_trip_number" as "trip.cis_trip_number",
		"trip"."gtfs_block_id" as "trip.gtfs_block_id",
		"trip"."gtfs_route_id" as "trip.gtfs_route_id",
		"trip"."gtfs_route_short_name" as "trip.gtfs_route_short_name",
		"trip"."gtfs_route_type" as "trip.gtfs_route_type",
		"trip"."gtfs_trip_headsign" as "trip.gtfs_trip_headsign",
		"trip"."gtfs_trip_short_name" as "trip.gtfs_trip_short_name",
		"trip"."gtfs_trip_id" as "trip.gtfs_trip_id",
		"trip"."gtfs_date" as "trip.gtfs_date",
		"trip"."id" as "trip.id",
		"trip"."is_canceled" as "trip.is_canceled",
		"trip"."last_position_id" as "trip.last_position_id",
		"trip"."origin_route_name" as "trip.origin_route_name",
		"trip"."run_number" as "trip.run_number",
		"trip"."start_asw_stop_id" as "trip.start_asw_stop_id",
		"trip"."start_cis_stop_id" as "trip.start_cis_stop_id",
		"trip"."start_cis_stop_platform_code" as "trip.start_cis_stop_platform_code",
		"trip"."start_time" as "trip.start_time",
		"trip"."start_timestamp" as "trip.start_timestamp",
		"trip"."end_timestamp" as "trip.end_timestamp",
		"trip"."vehicle_registration_number" as "trip.vehicle_registration_number",
		"trip"."vehicle_type_id" as "trip.vehicle_type_id",
		"trip"."wheelchair_accessible" as "trip.wheelchair_accessible",
		"trip"."internal_route_name" as "trip.internal_route_name",
		"trip"."internal_run_number" as "trip.internal_run_number",
		"trip->last_position"."asw_last_stop_id" as "trip.last_position.asw_last_stop_id",
		"trip->last_position"."bearing" as "trip.last_position.bearing",
		"trip->last_position"."cis_last_stop_id" as "trip.last_position.cis_last_stop_id",
		"trip->last_position"."cis_last_stop_sequence" as "trip.last_position.cis_last_stop_sequence",
		"trip->last_position"."delay" as "trip.last_position.delay",
		"trip->last_position"."delay_stop_arrival" as "trip.last_position.delay_stop_arrival",
		"trip->last_position"."delay_stop_departure" as "trip.last_position.delay_stop_departure",
		"trip->last_position"."id" as "trip.last_position.id",
		"trip->last_position"."is_canceled" as "trip.last_position.is_canceled",
		"trip->last_position"."last_stop_arrival_time" as "trip.last_position.last_stop_arrival_time",
		"trip->last_position"."last_stop_departure_time" as "trip.last_position.last_stop_departure_time",
		"trip->last_position"."last_stop_id" as "trip.last_position.last_stop_id",
		"trip->last_position"."last_stop_sequence" as "trip.last_position.last_stop_sequence",
		"trip->last_position"."lat" as "trip.last_position.lat",
		"trip->last_position"."lng" as "trip.last_position.lng",
		"trip->last_position"."next_stop_arrival_time" as "trip.last_position.next_stop_arrival_time",
		"trip->last_position"."next_stop_departure_time" as "trip.last_position.next_stop_departure_time",
		"trip->last_position"."next_stop_id" as "trip.last_position.next_stop_id",
		"trip->last_position"."next_stop_sequence" as "trip.last_position.next_stop_sequence",
		"trip->last_position"."origin_time" as "trip.last_position.origin_time",
		"trip->last_position"."origin_timestamp" as "trip.last_position.origin_timestamp",
		"trip->last_position"."shape_dist_traveled" as "trip.last_position.shape_dist_traveled",
		"trip->last_position"."speed" as "trip.last_position.speed",
		"trip->last_position"."state_position" as "trip.last_position.state_position",
		"trip->last_position"."state_process" as "trip.last_position.state_process",
		"trip->last_position"."this_stop_id" as "trip.last_position.this_stop_id",
		"trip->last_position"."this_stop_sequence" as "trip.last_position.this_stop_sequence",
		"trip->last_position"."is_tracked" as "trip.last_position.is_tracked",
		"trip->last_position"."trips_id" as "trip.last_position.trips_id",
		"trip->last_position"."tcp_event" as "trip.last_position.tcp_event",
		"trip->last_position"."last_stop_headsign" as "trip.last_position.last_stop_headsign",
		"trip->last_position"."last_stop_name" as "trip.last_position.last_stop_name",
		"trip->last_position"."valid_to" as "trip.last_position.valid_to",
		"trip->last_position"."scheduled_timestamp" as "trip.last_position.scheduled_timestamp",
		"trip->vehicle_descriptor"."id" as "trip.vehicle_descriptor.id",
		"trip->vehicle_descriptor"."is_air_conditioned" as "trip.vehicle_descriptor.is_air_conditioned"
	from
		"vehiclepositions_trips" as "trip"
	inner join "vehiclepositions_positions" as "trip->last_position" on
		"trip"."last_position_id" = "trip->last_position"."id"
		and ("trip->last_position"."valid_to" is null or "trip->last_position"."valid_to" >= current_timestamp)
		and "trip->last_position"."state_position" in ('at_stop', 'before_track', 'before_track_delayed', 'canceled', 'off_track', 'on_track')
	left outer join "vehiclepositions_vehicle_descriptors" as "trip->vehicle_descriptor" on
		"trip"."vehicle_registration_number" = "trip->vehicle_descriptor"."registration_number"
		and trip.gtfs_route_type = "trip->vehicle_descriptor".gtfs_route_type;
