ALTER TABLE ropidgtfs_scheduled_trips
  ADD first_stop_id varchar(255) NULL,
  ADD last_stop_id varchar(255) NULL;
