DROP INDEX IF EXISTS ropidvymi_events_routes_idx;
DROP TABLE IF EXISTS ropidvymi_events_routes;
DROP SEQUENCE IF EXISTS ropidvymi_events_routes_id_seq;

DROP INDEX IF EXISTS ropidvymi_events_stops_idx;
DROP TABLE IF EXISTS ropidvymi_events_stops;
DROP SEQUENCE IF EXISTS ropidvymi_events_stops_id_seq;

DROP TABLE IF EXISTS ropidvymi_events;
DROP SEQUENCE IF EXISTS ropidvymi_events_id_seq;
