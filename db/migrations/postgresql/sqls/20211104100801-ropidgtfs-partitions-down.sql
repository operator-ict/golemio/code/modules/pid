-- Table: ropidgtfs_agency
ALTER TABLE ropidgtfs_agency_actual NO INHERIT ropidgtfs_agency;
DROP TABLE ropidgtfs_agency_actual;
ALTER TABLE ropidgtfs_agency ADD CONSTRAINT ropidgtfs_agency_pkey PRIMARY KEY (agency_id);


-- Table: ropidgtfs_calendar
ALTER TABLE ropidgtfs_calendar_actual NO INHERIT ropidgtfs_calendar;
DROP TABLE ropidgtfs_calendar_actual;
ALTER TABLE ropidgtfs_calendar ADD CONSTRAINT ropidgtfs_calendar_pkey1 PRIMARY KEY (service_id);


-- Table: ropidgtfs_calendar_dates
ALTER TABLE ropidgtfs_calendar_dates_actual NO INHERIT ropidgtfs_calendar_dates;
DROP TABLE ropidgtfs_calendar_dates_actual;
ALTER TABLE ropidgtfs_calendar_dates ADD CONSTRAINT ropidgtfs_calendar_dates_pkey1 PRIMARY KEY (date, service_id);


-- Table: ropidgtfs_routes
ALTER TABLE ropidgtfs_routes_actual NO INHERIT ropidgtfs_routes;
DROP TABLE ropidgtfs_routes_actual;
ALTER TABLE ropidgtfs_routes ADD CONSTRAINT ropidgtfs_routes_pkey_pkey PRIMARY KEY (route_id);


-- Table: ropidgtfs_shapes
ALTER TABLE ropidgtfs_shapes_actual NO INHERIT ropidgtfs_shapes;
DROP TABLE ropidgtfs_shapes_actual;
ALTER TABLE ropidgtfs_shapes ADD CONSTRAINT ropidgtfs_shapes_pkey PRIMARY KEY (shape_id, shape_pt_sequence);


-- Table: ropidgtfs_stop_times
ALTER TABLE ropidgtfs_stop_times_actual NO INHERIT ropidgtfs_stop_times;
DROP TABLE ropidgtfs_stop_times_actual;
ALTER TABLE ropidgtfs_stop_times ADD CONSTRAINT ropidgtfs_stop_times_pkey1 PRIMARY KEY (stop_sequence, trip_id);
CREATE INDEX ropidgtfs_stop_times_trip_id ON ropidgtfs_stop_times USING btree (trip_id);
--DROP INDEX ropidgtfs_stop_times_trip_id;


-- Table: ropidgtfs_stops
ALTER TABLE ropidgtfs_stops_actual NO INHERIT ropidgtfs_stops;
DROP TABLE ropidgtfs_stops_actual;
ALTER TABLE ropidgtfs_stops ADD CONSTRAINT ropidgtfs_stops_pkey PRIMARY KEY (stop_id);


-- Table: ropidgtfs_trips
ALTER TABLE ropidgtfs_trips_actual NO INHERIT ropidgtfs_trips;
DROP TABLE ropidgtfs_trips_actual;
ALTER TABLE ropidgtfs_trips ADD CONSTRAINT ropidgtfs_trips_pkey PRIMARY KEY (trip_id);


-- Table: ropidgtfs_cis_stop_groups
ALTER TABLE ropidgtfs_cis_stop_groups_actual NO INHERIT ropidgtfs_cis_stop_groups;
DROP TABLE ropidgtfs_cis_stop_groups_actual;
ALTER TABLE ropidgtfs_cis_stop_groups ADD CONSTRAINT ropidgtfs_cis_stop_groups_pkey PRIMARY KEY (cis);


-- Table: ropidgtfs_cis_stops
ALTER TABLE ropidgtfs_cis_stops_actual NO INHERIT ropidgtfs_cis_stops;
DROP TABLE ropidgtfs_cis_stops_actual;
ALTER TABLE ropidgtfs_cis_stops ADD CONSTRAINT ropidgtfs_cis_stops_pkey PRIMARY KEY (id);


-- Table: ropidgtfs_run_numbers
ALTER TABLE ropidgtfs_run_numbers_actual NO INHERIT ropidgtfs_run_numbers;
DROP TABLE ropidgtfs_run_numbers_actual;
ALTER TABLE ropidgtfs_run_numbers ADD CONSTRAINT ropidgtfs_run_numbers_pkey PRIMARY KEY (run_number, service_id, trip_id);


-- Table: ropidgtfs_ois
ALTER TABLE ropidgtfs_ois_actual NO INHERIT ropidgtfs_ois;
DROP TABLE ropidgtfs_ois_actual;
ALTER TABLE ropidgtfs_ois ADD CONSTRAINT ropidgtfs_ois_pkey PRIMARY KEY (ois);


-- Materialized view: v_ropidgtfs_services_first14days
CREATE MATERIALIZED VIEW IF NOT EXISTS v_ropidgtfs_services_first14days AS
 SELECT dates.date,
    (date_part('day'::text, ((dates.date)::timestamp without time zone - (to_char(timezone('Europe/Prague'::text, now()), 'YYYY-MM-DD'::text))::timestamp without time zone)))::integer AS day_diff,
    ropidgtfs_calendar.service_id
   FROM (( SELECT (date_trunc('day'::text, dd.dd))::date AS date
           FROM generate_series(((( SELECT (CURRENT_TIMESTAMP - '1 day'::interval)))::date)::timestamp with time zone, ((( SELECT (CURRENT_TIMESTAMP + '12 days'::interval)))::date)::timestamp with time zone, '1 day'::interval) dd(dd)) dates
     JOIN ropidgtfs_calendar ON ((1 = 1)))
  WHERE (((dates.date >= (ropidgtfs_calendar.start_date)::date) AND (dates.date <= (ropidgtfs_calendar.end_date)::date) AND (((btrim(to_char((dates.date)::timestamp with time zone, 'day'::text)) = 'monday'::text) AND (ropidgtfs_calendar.monday = 1)) OR ((btrim(to_char((dates.date)::timestamp with time zone, 'day'::text)) = 'tuesday'::text) AND (ropidgtfs_calendar.tuesday = 1)) OR ((btrim(to_char((dates.date)::timestamp with time zone, 'day'::text)) = 'wednesday'::text) AND (ropidgtfs_calendar.wednesday = 1)) OR ((btrim(to_char((dates.date)::timestamp with time zone, 'day'::text)) = 'thursday'::text) AND (ropidgtfs_calendar.thursday = 1)) OR ((btrim(to_char((dates.date)::timestamp with time zone, 'day'::text)) = 'friday'::text) AND (ropidgtfs_calendar.friday = 1)) OR ((btrim(to_char((dates.date)::timestamp with time zone, 'day'::text)) = 'saturday'::text) AND (ropidgtfs_calendar.saturday = 1)) OR ((btrim(to_char((dates.date)::timestamp with time zone, 'day'::text)) = 'sunday'::text) AND (ropidgtfs_calendar.sunday = 1))) AND (NOT ((ropidgtfs_calendar.service_id)::text IN ( SELECT ropidgtfs_calendar_dates.service_id
           FROM ropidgtfs_calendar_dates
          WHERE ((ropidgtfs_calendar_dates.exception_type = 2) AND (dates.date = (ropidgtfs_calendar_dates.date)::date)))))) OR ((ropidgtfs_calendar.service_id)::text IN ( SELECT ropidgtfs_calendar_dates.service_id
           FROM ropidgtfs_calendar_dates
          WHERE ((ropidgtfs_calendar_dates.exception_type = 1) AND (dates.date = (ropidgtfs_calendar_dates.date)::date)))))
  ORDER BY dates.date;

CREATE UNIQUE INDEX IF NOT EXISTS v_ropidgtfs_services_first14days_date_service_id_idx ON v_ropidgtfs_services_first14days USING btree (date, service_id);

REFRESH MATERIALIZED VIEW "v_ropidgtfs_services_first14days";

-- Table: ropidgtfs_services_first14days
DROP TABLE ropidgtfs_services_first14days_actual;
DROP TABLE ropidgtfs_services_first14days;


-- Materialized view: v_ropidgtfs_trips_minmaxsequences
CREATE MATERIALIZED VIEW IF NOT EXISTS v_ropidgtfs_trips_minmaxsequences AS
 SELECT ropidgtfs_stop_times.trip_id,
    max(ropidgtfs_stop_times.stop_sequence) AS max_stop_sequence,
    min(ropidgtfs_stop_times.stop_sequence) AS min_stop_sequence,
    (GREATEST(max((ropidgtfs_stop_times.arrival_time)::interval), max((ropidgtfs_stop_times.departure_time)::interval)))::text AS max_stop_time,
    (LEAST(min((ropidgtfs_stop_times.arrival_time)::interval), min((ropidgtfs_stop_times.departure_time)::interval)))::text AS min_stop_time
   FROM ropidgtfs_stop_times
  GROUP BY ropidgtfs_stop_times.trip_id;

CREATE UNIQUE INDEX IF NOT EXISTS v_ropidgtfs_trips_minmaxsequences_trip_id_idx ON v_ropidgtfs_trips_minmaxsequences USING btree (trip_id);

REFRESH MATERIALIZED VIEW "v_ropidgtfs_trips_minmaxsequences";

-- Table: ropidgtfs_trips_minmaxsequences
DROP TABLE ropidgtfs_trips_minmaxsequences_actual;
DROP TABLE ropidgtfs_trips_minmaxsequences;


-- materialized v_ropidgtfs_departures
CREATE MATERIALIZED VIEW IF NOT EXISTS v_ropidgtfs_departures AS
 SELECT t.stop_sequence,
    t.stop_headsign,
    t.pickup_type,
    t.drop_off_type,
    t.arrival_time,
        CASE
            WHEN (date_part('hour'::text, (t.arrival_time)::interval) > (23)::double precision) THEN make_timestamptz((date_part('year'::text, (t4.date + '1 day'::interval)))::integer, (date_part('month'::text, (t4.date + '1 day'::interval)))::integer, (date_part('day'::text, (t4.date + '1 day'::interval)))::integer, ((date_part('hour'::text, (t.arrival_time)::interval))::integer - 24), (date_part('minute'::text, (t.arrival_time)::interval))::integer, date_part('second'::text, (t.arrival_time)::interval), 'Europe/Prague'::text)
            ELSE make_timestamptz((date_part('year'::text, t4.date))::integer, (date_part('month'::text, t4.date))::integer, (date_part('day'::text, t4.date))::integer, (date_part('hour'::text, (t.arrival_time)::interval))::integer, (date_part('minute'::text, (t.arrival_time)::interval))::integer, date_part('second'::text, (t.arrival_time)::interval), 'Europe/Prague'::text)
        END AS arrival_datetime,
    t.departure_time,
        CASE
            WHEN (date_part('hour'::text, (t.departure_time)::interval) > (23)::double precision) THEN make_timestamptz((date_part('year'::text, (t4.date + '1 day'::interval)))::integer, (date_part('month'::text, (t4.date + '1 day'::interval)))::integer, (date_part('day'::text, (t4.date + '1 day'::interval)))::integer, ((date_part('hour'::text, (t.departure_time)::interval))::integer - 24), (date_part('minute'::text, (t.departure_time)::interval))::integer, date_part('second'::text, (t.departure_time)::interval), 'Europe/Prague'::text)
            ELSE make_timestamptz((date_part('year'::text, t4.date))::integer, (date_part('month'::text, t4.date))::integer, (date_part('day'::text, t4.date))::integer, (date_part('hour'::text, (t.departure_time)::interval))::integer, (date_part('minute'::text, (t.departure_time)::interval))::integer, date_part('second'::text, (t.departure_time)::interval), 'Europe/Prague'::text)
        END AS departure_datetime,
    t0.stop_id,
    t0.stop_name,
    t0.platform_code,
    t0.wheelchair_boarding,
    t1.min_stop_sequence,
    t1.max_stop_sequence,
    t2.trip_id,
    t2.trip_headsign,
    t2.trip_short_name,
    t2.wheelchair_accessible,
    t3.service_id,
    t4.date,
    t5.route_short_name,
    t5.route_type,
    t5.route_id,
    t5.is_night,
    t5.is_regional,
    t5.is_substitute_transport,
    t6.stop_sequence AS next_stop_sequence,
    t6.stop_id AS next_stop_id,
    t7.stop_sequence AS last_stop_sequence,
    t7.stop_id AS last_stop_id
   FROM ((((((((ropidgtfs_stop_times t
     LEFT JOIN ropidgtfs_stops t0 ON (((t.stop_id)::text = (t0.stop_id)::text)))
     LEFT JOIN ropidgtfs_trips t2 ON (((t.trip_id)::text = (t2.trip_id)::text)))
     JOIN v_ropidgtfs_services_first14days t4 ON (((t2.service_id)::text = (t4.service_id)::text)))
     LEFT JOIN v_ropidgtfs_trips_minmaxsequences t1 ON (((t.trip_id)::text = (t1.trip_id)::text)))
     LEFT JOIN ropidgtfs_calendar t3 ON (((t2.service_id)::text = (t3.service_id)::text)))
     LEFT JOIN ropidgtfs_routes t5 ON (((t2.route_id)::text = (t5.route_id)::text)))
     LEFT JOIN ropidgtfs_stop_times t6 ON ((((t.trip_id)::text = (t6.trip_id)::text) AND (t6.stop_sequence = (t.stop_sequence + 1)))))
     LEFT JOIN ropidgtfs_stop_times t7 ON ((((t.trip_id)::text = (t7.trip_id)::text) AND (t7.stop_sequence = (t.stop_sequence - 1)))));

CREATE INDEX IF NOT EXISTS v_ropidgtfs_departures_stop_id_idx ON v_ropidgtfs_departures USING btree (stop_id);
CREATE UNIQUE INDEX IF NOT EXISTS v_ropidgtfs_departures_unique_id ON v_ropidgtfs_departures USING btree (service_id, date, trip_id, stop_sequence);

REFRESH MATERIALIZED VIEW "v_ropidgtfs_departures";

-- Table: ropidgtfs_departures
DROP TABLE ropidgtfs_departures_actual;
DROP TABLE ropidgtfs_departures;
