alter table vehiclepositions_metro_runs_messages drop constraint "metro_runs_messages_pkey";
alter table vehiclepositions_metro_runs_messages
    add constraint "metro_runs_messages_pkey" primary key (route_name, message_timestamp, train_number, track_id, train_set_number_real);
