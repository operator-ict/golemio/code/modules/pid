create type jis_infotext_severity_level as enum ('INFO', 'WARNING', 'SEVERE');

alter table jis_infotexts
alter column severity_level type jis_infotext_severity_level using severity_level::jis_infotext_severity_level;

alter table jis_infotexts
add column created_timestamp timestamptz not null default now();
