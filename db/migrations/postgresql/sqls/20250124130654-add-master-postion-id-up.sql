ALTER TABLE vehiclepositions_positions
    ADD origin_position_id bigint;

ALTER TABLE vehiclepositions_positions_history
    ADD origin_position_id bigint;

