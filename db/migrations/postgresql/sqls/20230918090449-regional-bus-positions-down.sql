alter table vehiclepositions_vehicle_descriptors drop column is_wheelchair_accessible;

alter table vehiclepositions_regional_bus_runs_messages
    rename cis_line_id to cis_route_id;

alter table vehiclepositions_regional_bus_runs_messages
    rename cis_trip_number to cis_trip_id;

alter table vehiclepositions_regional_bus_runs_messages
    alter column cis_trip_id type text using cis_trip_id::text;

drop index ropidgtfs_route_sub_agencies_cis_idx;
drop index if exists ropidgtfs_route_sub_agencies_actual_cis_idx;

drop index ropidgtfs_run_numbers_lookup_idx;
drop index if exists ropidgtfs_run_numbers_lookup_actual_idx;
