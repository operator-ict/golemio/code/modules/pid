create function convert_timestamp_ms_to_iso_string(timestamp_ms bigint, timezone varchar DEFAULT 'Europe/Prague') RETURNS varchar AS $$
    select TO_CHAR(DATE_TRUNC('second', TO_TIMESTAMP(timestamp_ms / 1000)) at TIME zone timezone, 'YYYY-MM-DD"T"HH24:MI:SS') || replace('+' || to_char(DATE_TRUNC('second', TO_TIMESTAMP(timestamp_ms / 1000)) at TIME zone timezone - DATE_TRUNC('second', TO_TIMESTAMP(timestamp_ms / 1000)) at TIME zone 'UTC', 'HH24:MMFM'), '+-', '-')
$$ LANGUAGE SQL IMMUTABLE;


create view v_vehiclepositions_all_trips_with_last_position as
select
    vt.id,
	vt.agency_name_real,
	vt.agency_name_scheduled,
	vt.cis_line_id,
	vt.cis_trip_number,
	vt.gtfs_route_id,
	vt.gtfs_route_short_name,
	vt.gtfs_route_type,
	vt.gtfs_trip_headsign,
	vt.gtfs_trip_short_name,
	vt.gtfs_trip_id,
	vt.origin_route_name,
	vt.run_number,
	vt.vehicle_registration_number,
    vt.vehicle_type_id,
    vt.wheelchair_accessible,
    vt.updated_at,
	convert_timestamp_ms_to_iso_string(vt.start_timestamp) as start_timestamp_isostring,
	vp.bearing,
	vp.delay,
	vp.delay_stop_arrival,
	vp.delay_stop_departure,
	vp.is_canceled,
	vp.last_stop_id,
	vp.last_stop_sequence,
    vp.last_stop_headsign,
    vp.lat,
    vp.lng,
	vp.next_stop_id,
	vp.next_stop_sequence,
	vp.shape_dist_traveled,
	vp.speed,
	vp.state_position,
	vp.tracking,
	convert_timestamp_ms_to_iso_string(vp.last_stop_arrival_time) as last_stop_arrival_time_isostring,
	convert_timestamp_ms_to_iso_string(vp.last_stop_departure_time) as last_stop_departure_time_isostring,
	convert_timestamp_ms_to_iso_string(vp.next_stop_arrival_time) as next_stop_arrival_time_isostring,
	convert_timestamp_ms_to_iso_string(vp.next_stop_departure_time) as next_stop_departure_time_isostring,
	convert_timestamp_ms_to_iso_string(vp.origin_timestamp) as origin_timestamp_isostring
from
	vehiclepositions_trips vt
inner join vehiclepositions_positions vp on
	vt.last_position_id = vp.id
	and (vp.valid_to is null
		or vp.valid_to >= extract(epoch from now()) * 1000)
	and vp.state_process = 'processed';

comment on view v_vehiclepositions_all_trips_with_last_position is '
View of all trips with last processed position. Used for serving data to the vehiclepositions output API.
';


create view v_vehiclepositions_all_processed_positions as
select
    trips_id,
	bearing,
	"delay",
	delay_stop_arrival,
	delay_stop_departure,
	vp.is_canceled,
    convert_timestamp_ms_to_iso_string(last_stop_arrival_time) as last_stop_arrival_time_isostring,
	convert_timestamp_ms_to_iso_string(last_stop_departure_time) as last_stop_departure_time_isostring,
	last_stop_id,
	last_stop_sequence,
	lat,
	lng,
    convert_timestamp_ms_to_iso_string(next_stop_arrival_time) as next_stop_arrival_time_isostring,
	convert_timestamp_ms_to_iso_string(next_stop_departure_time) as next_stop_departure_time_isostring,
	next_stop_id,
	next_stop_sequence,
	convert_timestamp_ms_to_iso_string(origin_timestamp) as origin_timestamp_isostring,
	shape_dist_traveled,
	speed,
    state_position
from
	vehiclepositions_positions vp
where state_process = 'processed';

comment on view v_vehiclepositions_all_processed_positions is '
View of all processed positions. Used for enriching trips from v_vehiclepositions_all_trips_with_last_position with all positions.
';


alter view v_vehiclepositions_last_position rename to v_vehiclepositions_alerts_last_position;
comment on view v_vehiclepositions_alerts_last_position is '
Used only for alerting purposes.
';
