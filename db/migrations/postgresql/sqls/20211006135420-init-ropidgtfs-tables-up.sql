-- ropidgtfs_agency
CREATE TABLE IF NOT EXISTS ropidgtfs_agency (
    agency_fare_url character varying(255),
    agency_id character varying(255) NOT NULL,
    agency_lang character varying(255),
    agency_name character varying(255),
    agency_phone character varying(255),
    agency_timezone character varying(255),
    agency_url character varying(255),
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),
    agency_email character varying(255),

    CONSTRAINT ropidgtfs_agency_pkey PRIMARY KEY (agency_id)
);

-- ropidgtfs_calendar
CREATE TABLE IF NOT EXISTS ropidgtfs_calendar (
    end_date character varying(255),
    friday integer,
    monday integer,
    saturday integer,
    service_id character varying(255) NOT NULL,
    start_date character varying(255),
    sunday integer,
    thursday integer,
    tuesday integer,
    wednesday integer,
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),

    CONSTRAINT ropidgtfs_calendar_pkey1 PRIMARY KEY (service_id)
);

-- ropidgtfs_calendar_dates
CREATE TABLE IF NOT EXISTS ropidgtfs_calendar_dates (
    date character varying(255) NOT NULL,
    exception_type integer,
    service_id character varying(255) NOT NULL,
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),

    CONSTRAINT ropidgtfs_calendar_dates_pkey1 PRIMARY KEY (date, service_id)
);

-- ropidgtfs_cis_stops
CREATE TABLE IF NOT EXISTS ropidgtfs_cis_stops (
    alt_idos_name character varying(255),
    cis integer,
    id character varying(255) NOT NULL,
    jtsk_x double precision,
    jtsk_y double precision,
    lat double precision,
    lon double precision,
    platform character varying(255),
    created_at timestamp with time zone,
    wheelchair_access character varying(255),
    zone character varying(255),
    create_batch_id bigint,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),

    CONSTRAINT ropidgtfs_cis_stops_pkey PRIMARY KEY (id)
);

-- ropidgtfs_cis_stop_groups
CREATE TABLE IF NOT EXISTS ropidgtfs_cis_stop_groups (
    avg_jtsk_x double precision,
    avg_jtsk_y double precision,
    avg_lat double precision,
    avg_lon double precision,
    cis integer NOT NULL,
    district_code character varying(255),
    full_name character varying(255),
    idos_category character varying(255),
    idos_name character varying(255),
    municipality character varying(255),
    name character varying(255),
    node integer,
    created_at timestamp with time zone,
    unique_name character varying(255),
    create_batch_id bigint,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),

    CONSTRAINT ropidgtfs_cis_stop_groups_pkey PRIMARY KEY (cis)
);

-- ropidgtfs_metadata
CREATE SEQUENCE IF NOT EXISTS ropidgtfs_metadata_id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

CREATE TABLE IF NOT EXISTS ropidgtfs_metadata (
    id integer DEFAULT nextval('ropidgtfs_metadata_id_seq'::regclass) NOT NULL,
    dataset character varying(255),
    key character varying(255),
    type character varying(255),
    value character varying(255),
    version integer,
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),

    CONSTRAINT ropidgtfs_metadata_pkey PRIMARY KEY (id)
);

-- ropidgtfs_ois
CREATE TABLE IF NOT EXISTS ropidgtfs_ois (
    ois integer NOT NULL,
    node integer NOT NULL,
    name character varying(255) NOT NULL,
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),

    CONSTRAINT ropidgtfs_ois_pkey PRIMARY KEY (ois)
);

-- ropidgtfs_routes
CREATE TABLE IF NOT EXISTS ropidgtfs_routes (
    agency_id character varying(255),
    is_night character varying(255),
    route_color character varying(255),
    route_desc character varying(255),
    route_id character varying(255) NOT NULL,
    route_long_name character varying(255),
    route_short_name character varying(255),
    route_text_color character varying(255),
    route_type character varying(255),
    route_url character varying(255),
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),
    is_regional character varying(255),
    is_substitute_transport character varying(255),

    CONSTRAINT ropidgtfs_routes_pkey PRIMARY KEY (route_id)
);

-- ropidgtfs_run_numbers
CREATE TABLE IF NOT EXISTS ropidgtfs_run_numbers (
    route_id character varying(50) NOT NULL,
    run_number integer NOT NULL,
    service_id character varying(50) NOT NULL,
    trip_id character varying(50) NOT NULL,
    vehicle_type integer NOT NULL,
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),

    CONSTRAINT ropidgtfs_run_numbers_pkey PRIMARY KEY (run_number, service_id, trip_id)
);

-- ropidgtfs_shapes
CREATE TABLE IF NOT EXISTS ropidgtfs_shapes (
    shape_dist_traveled double precision,
    shape_id character varying(255) NOT NULL,
    shape_pt_lat double precision,
    shape_pt_lon double precision,
    shape_pt_sequence integer NOT NULL,
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),

    CONSTRAINT ropidgtfs_shapes_pkey PRIMARY KEY (shape_id, shape_pt_sequence)
);

-- ropidgtfs_stop_times
CREATE TABLE IF NOT EXISTS ropidgtfs_stop_times (
    arrival_time character varying(255),
    arrival_time_seconds integer,
    departure_time character varying(255),
    departure_time_seconds integer,
    drop_off_type character varying(255),
    pickup_type character varying(255),
    shape_dist_traveled double precision,
    stop_headsign character varying(255),
    stop_id character varying(255),
    stop_sequence integer NOT NULL,
    trip_id character varying(255) NOT NULL,
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),
    timepoint integer,

    CONSTRAINT ropidgtfs_stop_times_pkey1 PRIMARY KEY (stop_sequence, trip_id)
);

-- ropidgtfs_stops
CREATE TABLE IF NOT EXISTS ropidgtfs_stops (
    location_type integer,
    parent_station character varying(255),
    platform_code character varying(255),
    stop_id character varying(255) NOT NULL,
    stop_lat double precision,
    stop_lon double precision,
    stop_name character varying(255),
    stop_url character varying(255),
    wheelchair_boarding integer,
    zone_id character varying(255),
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),
    level_id character varying(255),
    stop_code character varying(255),
    stop_desc character varying(255),
    stop_timezone character varying(255),

    CONSTRAINT ropidgtfs_stops_pkey PRIMARY KEY (stop_id)
);

-- ropidgtfs_trips
CREATE TABLE IF NOT EXISTS ropidgtfs_trips (
    bikes_allowed integer,
    block_id character varying(255),
    direction_id integer,
    exceptional integer,
    route_id character varying(255),
    service_id character varying(255),
    shape_id character varying(255),
    trip_headsign character varying(255),
    trip_id character varying(255) NOT NULL,
    wheelchair_accessible integer,
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),
    trip_operation_type integer,
    trip_short_name character varying(255),

    CONSTRAINT ropidgtfs_trips_pkey PRIMARY KEY (trip_id)
);
