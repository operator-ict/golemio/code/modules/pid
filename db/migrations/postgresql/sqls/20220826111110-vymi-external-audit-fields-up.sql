-- Rename columns
ALTER TABLE ropidvymi_events
    RENAME pa_01 TO ropid_created_at;

ALTER TABLE ropidvymi_events
    RENAME pa_02 TO ropid_created_by;

ALTER TABLE ropidvymi_events
    RENAME pa_03 TO ropid_updated_at;

ALTER TABLE ropidvymi_events
    RENAME pa_04 TO ropid_updated_by;

-- Change types
ALTER TABLE ropidvymi_events
    ALTER COLUMN ropid_created_at TYPE TIMESTAMP WITH TIME ZONE
        USING to_timestamp(ropid_created_at, 'YYYYMMDD" "HH24:MI:SS') AT TIME ZONE 'Europe/Prague',
    ALTER COLUMN ropid_updated_at TYPE TIMESTAMP WITH TIME ZONE
        USING to_timestamp(ropid_updated_at, 'YYYYMMDD" "HH24:MI:SS') AT TIME ZONE 'Europe/Prague';
