-- vehicle type trolleybus
delete from vehiclepositions_vehicle_types where id = 18; -- see MPVRouteTypesEnum.TROLLEYBUS

-- shape id
alter table vehiclepositions_trips drop column gtfs_shape_id;
alter table vehiclepositions_trips_history drop column gtfs_shape_id;
alter table ropidgtfs_precomputed_trip_schedule drop column shape_id;

-- combined view
drop view v_public_vehiclepositions_combined_stop_times;

-- history view
drop view v_public_vehiclepositions_past_stop_times;

-- prediction view
drop view v_public_vehiclepositions_future_stop_times;
