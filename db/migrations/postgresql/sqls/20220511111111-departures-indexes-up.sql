CREATE INDEX IF NOT EXISTS vehiclepositions_positions_valid_to ON vehiclepositions_positions USING btree (valid_to);
CREATE INDEX IF NOT EXISTS vehiclepositions_trips_gtfs_trip_id ON vehiclepositions_trips USING btree (gtfs_trip_id);
CREATE INDEX IF NOT EXISTS ropidgtfs_departures_actual_stop_id ON ropidgtfs_departures_actual USING btree (stop_id);
