create table vehiclepositions_regional_bus_runs_messages (
    external_trip_id varchar(100) not null,
    cis_route_id varchar(10),
    cis_trip_id varchar(10),
    events varchar(10) not null,
    coordinates geometry not null,
    vehicle_timestamp timestamptz not null,
    registration_number int,
    speed_kmh smallint not null,
    bearing smallint not null,

    -- Audit fields
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),

    constraint vehiclepositions_regional_bus_run_messages_pkey primary key (external_trip_id, vehicle_timestamp)
);
