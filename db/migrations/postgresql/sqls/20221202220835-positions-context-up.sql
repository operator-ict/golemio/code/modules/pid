ALTER TABLE vehiclepositions_trips_history ADD COLUMN "last_position_context" JSON;
ALTER TABLE vehiclepositions_trips ADD COLUMN "last_position_context" JSON;

CREATE INDEX IF NOT EXISTS vehiclepositions_positions_state_process ON vehiclepositions_positions USING btree (state_process);
