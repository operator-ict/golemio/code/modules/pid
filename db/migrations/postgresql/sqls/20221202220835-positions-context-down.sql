ALTER TABLE vehiclepositions_trips DROP COLUMN "last_position_context";
ALTER TABLE vehiclepositions_trips_history DROP COLUMN "last_position_context";

DROP INDEX IF EXISTS vehiclepositions_positions_state_process;
