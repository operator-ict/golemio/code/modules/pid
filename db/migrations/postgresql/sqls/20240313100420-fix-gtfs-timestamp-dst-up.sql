CREATE OR REPLACE FUNCTION gtfs_timestamp(gtfs_time varchar, target_date date)
RETURNS timestamptz AS
$$
DECLARE
    timezone text := 'Europe/Prague';
BEGIN
    RETURN concat(target_date, 'T12:00:00') :: timestamp AT TIME ZONE timezone - '12:00:00' :: interval + gtfs_time :: interval;
END;
$$
LANGUAGE plpgsql;

COMMENT ON FUNCTION gtfs_timestamp(gtfs_time varchar, target_date date) IS '
Converts a time string from GTFS feed to a timestamp.

gtfs_time - Time in the HH:MM:SS format (H:MM:SS is also accepted). The time is measured from "noon minus 12h" of the service day
(effectively midnight except for days on which daylight savings time changes occur). For times occurring after midnight on the
service day, enter the time as a value greater than 24:00:00 in HH:MM:SS.

target_date - Service day in the YYYYMMDD (or YYYY-MM-DD) format. Since time within a service day may be above 24 :00 :00,
a service day may contain information for the subsequent day(s).

See also GTFS Schedule Reference for Date and Time at <https://gtfs.org/schedule/reference/>.
';
