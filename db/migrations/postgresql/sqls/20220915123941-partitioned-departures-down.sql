DROP TABLE IF EXISTS ropidgtfs_departures;
CREATE TABLE ropidgtfs_departures (
	stop_sequence int4 NULL,
	stop_headsign varchar(255) NULL,
	pickup_type varchar(255) NULL,
	drop_off_type varchar(255) NULL,
	arrival_time varchar(255) NULL,
	arrival_datetime timestamptz NULL,
	departure_time varchar(255) NULL,
	departure_datetime timestamptz NULL,
	stop_id varchar(255) NULL,
	stop_name varchar(255) NULL,
	platform_code varchar(255) NULL,
	wheelchair_boarding int4 NULL,
	min_stop_sequence int4 NULL,
	max_stop_sequence int4 NULL,
	trip_id varchar(255) NULL,
	trip_headsign varchar(255) NULL,
	trip_short_name varchar(255) NULL,
	wheelchair_accessible int4 NULL,
	service_id varchar(255) NULL,
	"date" date NULL,
	route_short_name varchar(255) NULL,
	route_type varchar(255) NULL,
	route_id varchar(255) NULL,
	is_night varchar(255) NULL,
	is_regional varchar(255) NULL,
	is_substitute_transport varchar(255) NULL,
	next_stop_sequence int4 NULL,
	next_stop_id varchar(255) NULL,
	last_stop_sequence int4 NULL,
	last_stop_id varchar(255) null
);

CREATE TABLE ropidgtfs_departures_actual (LIKE ropidgtfs_departures);
CREATE INDEX ropidgtfs_departures_stop_id_idx ON ropidgtfs_departures_actual USING btree (stop_id);
CREATE UNIQUE INDEX ropidgtfs_departures_unique_id ON ropidgtfs_departures_actual USING btree (service_id, date, trip_id, stop_sequence);

ALTER TABLE ropidgtfs_departures_actual INHERIT ropidgtfs_departures;

DROP FUNCTION departure_part (text, text, boolean);
