CREATE OR REPLACE FUNCTION departure_part (par_stop_id text, t_schema text, is_tmp boolean DEFAULT false)
    RETURNS integer
    LANGUAGE plpgsql
AS $function$
    declare txt_sql text;
    declare master_table_name text;
begin
    txt_sql = 'SET search_path TO ' || t_schema || ';';

    if is_tmp = true then
        txt_sql = txt_sql ||' ALTER TABLE IF EXISTS ropidgtfs_departures_' || trim(par_stop_id) || ' RENAME TO ropidgtfs_departures_' || trim(par_stop_id) || '_drop;';
        master_table_name = 'ropidgtfs_departures_tmp';
    else
        master_table_name = 'ropidgtfs_departures';
    end if;

    txt_sql = txt_sql || ' CREATE TABLE IF NOT EXISTS ropidgtfs_departures_' || trim(par_stop_id);
    txt_sql = txt_sql || ' PARTITION OF ' || master_table_name || ' FOR VALUES IN (''' || trim(par_stop_id) || ''')';
    execute txt_sql;
return 0;
end;
$function$
;
