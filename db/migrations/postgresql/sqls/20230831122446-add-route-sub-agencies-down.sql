DROP TABLE ropidgtfs_route_sub_agencies_actual;
DROP TABLE ropidgtfs_route_sub_agencies;

CREATE OR REPLACE PROCEDURE ropidgtfs_preparetmptables()
 LANGUAGE plpgsql
 SET search_path from CURRENT
AS $procedure$
	begin
		CALL ropidgtfs_cleanTmpTables();
		CALL ropidgtfs_cleanOldTables();
		CALL ropidgtfs_prepareTmpTable('ropidgtfs_agency');
		CALL ropidgtfs_prepareTmpTable('ropidgtfs_calendar');
		CALL ropidgtfs_prepareTmpTable('ropidgtfs_calendar_dates');
		CALL ropidgtfs_prepareTmpTable('ropidgtfs_shapes');
		CALL ropidgtfs_prepareTmpTable('ropidgtfs_stop_times');
		CALL ropidgtfs_prepareTmpTable('ropidgtfs_stops');
		CALL ropidgtfs_prepareTmpTable('ropidgtfs_routes');		
		CALL ropidgtfs_prepareTmpTable('ropidgtfs_trips');
		CALL ropidgtfs_prepareTmpTable('ropidgtfs_cis_stops');
		CALL ropidgtfs_prepareTmpTable('ropidgtfs_cis_stop_groups');
		CALL ropidgtfs_prepareTmpTable('ropidgtfs_run_numbers');
		CALL ropidgtfs_prepareTmpTable('ropidgtfs_ois');
		alter table ropidgtfs_stop_times_tmp drop computed_dwell_time_seconds;
		alter table ropidgtfs_stop_times_tmp add computed_dwell_time_seconds int2 NOT NULL GENERATED ALWAYS AS (pid.calculate_dwell_time_seconds(arrival_time, departure_time)) stored;
	end
$procedure$
;

CREATE OR REPLACE PROCEDURE ropidgtfs_cleanoldtables()
 LANGUAGE plpgsql
 SET search_path from CURRENT
AS $procedure$
	begin
		drop table if exists "ropidgtfs_agency_old";
		drop table if exists "ropidgtfs_calendar_old";
		drop table if exists "ropidgtfs_calendar_dates_old";
		drop table if exists "ropidgtfs_shapes_old";
		drop table if exists "ropidgtfs_stop_times_old";
		drop table if exists "ropidgtfs_stops_old";
		drop table if exists "ropidgtfs_routes_old";
		drop table if exists "ropidgtfs_trips_old";
		drop table if exists "ropidgtfs_cis_stops_old";
		drop table if exists "ropidgtfs_cis_stop_groups_old";
		drop table if exists "ropidgtfs_run_numbers_old";
		drop table if exists "ropidgtfs_ois_old";
		drop table if exists "ropidgtfs_precomputed_services_calendar_old";
		drop table if exists "ropidgtfs_precomputed_minmax_stop_sequences_old";
		drop table if exists "ropidgtfs_precomputed_trip_schedule_old";
	end
$procedure$
;

CREATE OR REPLACE PROCEDURE ropidgtfs_cleantmptables()
 LANGUAGE plpgsql
 SET search_path from CURRENT
AS $procedure$
	begin		
		drop table if exists "ropidgtfs_agency_tmp";
		drop table if exists "ropidgtfs_calendar_tmp";
		drop table if exists "ropidgtfs_calendar_dates_tmp";
		drop table if exists "ropidgtfs_shapes_tmp";
		drop table if exists "ropidgtfs_stop_times_tmp";
		drop table if exists "ropidgtfs_stops_tmp";
		drop table if exists "ropidgtfs_routes_tmp";
		drop table if exists "ropidgtfs_trips_tmp";
		drop table if exists "ropidgtfs_cis_stops_tmp";
		drop table if exists "ropidgtfs_cis_stop_groups_tmp";
		drop table if exists "ropidgtfs_run_numbers_tmp";
		drop table if exists "ropidgtfs_ois_tmp";		
		drop table if exists "ropidgtfs_precomputed_services_calendar_tmp";
		drop table if exists "ropidgtfs_precomputed_minmax_stop_sequences_tmp";
		drop table if exists "ropidgtfs_precomputed_trip_schedule_tmp";
	end
$procedure$
;