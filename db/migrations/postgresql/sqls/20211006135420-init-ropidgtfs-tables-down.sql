DROP TABLE IF EXISTS ropidgtfs_agency;

DROP TABLE IF EXISTS ropidgtfs_calendar;

DROP TABLE IF EXISTS ropidgtfs_calendar_dates;

DROP TABLE IF EXISTS ropidgtfs_cis_stops;

DROP TABLE IF EXISTS ropidgtfs_cis_stop_groups;

DROP TABLE IF EXISTS ropidgtfs_metadata;
DROP SEQUENCE IF EXISTS ropidgtfs_metadata_id_seq;

DROP TABLE IF EXISTS ropidgtfs_ois;

DROP TABLE IF EXISTS ropidgtfs_routes;

DROP TABLE IF EXISTS ropidgtfs_run_numbers;

DROP TABLE IF EXISTS ropidgtfs_shapes;

DROP TABLE IF EXISTS ropidgtfs_stop_times;

DROP TABLE IF EXISTS ropidgtfs_stops;

DROP TABLE IF EXISTS ropidgtfs_trips;
