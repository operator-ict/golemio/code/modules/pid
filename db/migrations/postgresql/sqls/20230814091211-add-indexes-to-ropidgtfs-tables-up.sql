ALTER TABLE ropidgtfs_agency ADD CONSTRAINT ropidgtfs_agency_pk PRIMARY KEY (agency_id);
ALTER TABLE ropidgtfs_calendar add CONSTRAINT ropidgtfs_calendar_pk PRIMARY KEY (service_id);
ALTER TABLE ropidgtfs_calendar_dates add CONSTRAINT ropidgtfs_calendar_dates_pk PRIMARY KEY (service_id, date);
ALTER TABLE ropidgtfs_cis_stop_groups add CONSTRAINT ropidgtfs_cis_stop_groups_pk PRIMARY KEY (cis);
ALTER TABLE ropidgtfs_cis_stops add CONSTRAINT ropidgtfs_cis_stops_pk PRIMARY KEY (id);
ALTER TABLE ropidgtfs_ois add CONSTRAINT ropidgtfs_ois_pk PRIMARY KEY (ois);
ALTER TABLE ropidgtfs_routes add CONSTRAINT ropidgtfs_routes_pk PRIMARY KEY (route_id);
ALTER TABLE ropidgtfs_run_numbers add CONSTRAINT ropidgtfs_run_numbers_pk PRIMARY KEY (run_number, service_id, trip_id);
ALTER TABLE ropidgtfs_shapes add CONSTRAINT ropidgtfs_shapes_pk PRIMARY KEY (shape_id, shape_pt_sequence);
ALTER TABLE ropidgtfs_stop_times add CONSTRAINT ropidgtfs_stop_times_pk PRIMARY KEY (trip_id, stop_sequence);
ALTER TABLE ropidgtfs_stops add CONSTRAINT ropidgtfs_stops_pk PRIMARY KEY (stop_id);
ALTER TABLE ropidgtfs_trips add CONSTRAINT ropidgtfs_trips_pk PRIMARY KEY (trip_id);

CREATE UNIQUE INDEX ropidgtfs_precomputed_minmax_stop_sequences_v2_trip_id_idx ON ropidgtfs_precomputed_minmax_stop_sequences USING btree (trip_id);
CREATE UNIQUE INDEX ropidgtfs_precomputed_services_calendar_date_v2_service_id_idx ON ropidgtfs_precomputed_services_calendar USING btree (date, service_id);
CREATE INDEX ropidgtfs_precomputed_trip_schedule_origin_route_v2_id_idx ON ropidgtfs_precomputed_trip_schedule USING btree (origin_route_id);
CREATE INDEX ropidgtfs_precomputed_trip_schedule_origin_route_v2_name_idx ON ropidgtfs_precomputed_trip_schedule USING btree (origin_route_name);
CREATE INDEX ropidgtfs_precomputed_trip_schedule_start_timestamp_v2_idx ON ropidgtfs_precomputed_trip_schedule USING btree (start_timestamp);
CREATE UNIQUE INDEX ropidgtfs_precomputed_trip_schedule_unique_v2_id ON ropidgtfs_precomputed_trip_schedule USING btree (trip_id, service_id, run_number, date, origin_route_id);