-- Rename columns
ALTER TABLE ropidvymi_events
    RENAME ropid_created_at TO pa_01;

ALTER TABLE ropidvymi_events
    RENAME ropid_created_by TO pa_02;

ALTER TABLE ropidvymi_events
    RENAME ropid_updated_at TO pa_03;

ALTER TABLE ropidvymi_events
    RENAME ropid_updated_by TO pa_04;

-- Change types
ALTER TABLE ropidvymi_events
    ALTER COLUMN pa_01 TYPE VARCHAR(150)
        USING to_char(pa_01 AT TIME ZONE 'Europe/Prague', 'YYYYMMDD" "HH24:MI:SS'),
    ALTER COLUMN pa_03 TYPE VARCHAR(150)
        USING to_char(pa_03 AT TIME ZONE 'Europe/Prague', 'YYYYMMDD" "HH24:MI:SS');
