create index ropidgtfs_stop_times_drop_off_pickup_type_idx on ropidgtfs_stop_times (drop_off_type, pickup_type);
create index ropidgtfs_stop_times_drop_off_pickup_type_actual_idx on ropidgtfs_stop_times_actual (drop_off_type, pickup_type);
