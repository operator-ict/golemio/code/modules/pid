DROP TRIGGER gtfs_trigger ON ropidvymi_events_stops;
DROP TRIGGER gtfs_trigger ON ropidvymi_events_routes;

DROP FUNCTION ropidvymi_events_stops_gtfs_trigger_function;
DROP FUNCTION ropidvymi_events_routes_gtfs_trigger_function;

CREATE SEQUENCE IF NOT EXISTS ropidvymi_events_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

CREATE SEQUENCE IF NOT EXISTS ropidvymi_events_routes_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

CREATE SEQUENCE IF NOT EXISTS ropidvymi_events_stops_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER TABLE ropidvymi_events
    DROP CONSTRAINT ropidvymi_events_pkey,
    ADD COLUMN id integer DEFAULT nextval('ropidvymi_events_id_seq'::regclass) NOT NULL,
    ADD CONSTRAINT ropidvymi_events_pkey PRIMARY KEY (id);

ALTER TABLE ropidvymi_events_stops
    DROP CONSTRAINT ropidvymi_events_stops_pkey,
    ADD COLUMN id integer DEFAULT nextval('ropidvymi_events_stops_id_seq'::regclass) NOT NULL,
    ADD CONSTRAINT ropidvymi_events_stops_pkey PRIMARY KEY (event_id, vymi_id),
    ADD CONSTRAINT ropidvymi_events_stops_fkey FOREIGN KEY (event_id) REFERENCES ropidvymi_events(id)
        ON DELETE CASCADE;

ALTER TABLE ropidvymi_events_routes
    ADD COLUMN id integer DEFAULT nextval('ropidvymi_events_routes_id_seq'::regclass) NOT NULL,
    ADD CONSTRAINT ropidvymi_events_routes_fkey FOREIGN KEY (event_id) REFERENCES ropidvymi_events(id)
        ON DELETE CASCADE;

DROP TABLE ropidvymi_metadata;
