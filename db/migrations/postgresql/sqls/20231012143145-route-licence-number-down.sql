alter table ropidgtfs_run_numbers DROP COLUMN IF EXISTS route_licence_number;
alter table ropidgtfs_precomputed_trip_schedule DROP COLUMN IF EXISTS route_licence_number;

drop index if exists ropidgtfs_precomputed_trip_schedule_lookup_idx;
drop index if exists ropidgtfs_precomputed_trip_schedule_lookup_actual_idx;

create index if not exists ropidgtfs_run_numbers_lookup_idx on ropidgtfs_run_numbers (route_id, trip_number);
create index if not exists ropidgtfs_run_numbers_lookup_actual_idx on ropidgtfs_run_numbers_actual (route_id, trip_number);
