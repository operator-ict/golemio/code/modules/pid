ALTER TYPE vehicle_descriptor_state RENAME TO vehicle_descriptor_state_old;

CREATE TYPE vehicle_descriptor_state AS ENUM(
    'dilny',
    'doco',
    'nzr',
    'zar'
);

ALTER TABLE vehiclepositions_vehicle_descriptors
    ALTER COLUMN state TYPE vehicle_descriptor_state
    USING state::text::vehicle_descriptor_state;

DROP TYPE vehicle_descriptor_state_old;

