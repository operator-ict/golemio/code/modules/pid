DROP procedure vehiclepositions_data_retention(int, int);

DROP TABLE vehiclepositions_positions_history;

DROP TABLE vehiclepositions_trips_history;

CREATE INDEX IF NOT EXISTS vehiclepositions_positions_origin_time ON vehiclepositions_positions USING btree (origin_time);
