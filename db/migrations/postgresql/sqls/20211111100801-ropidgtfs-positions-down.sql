ALTER TABLE "vehiclepositions_trips"
  DROP COLUMN "gtfs_trip_short_name";

ALTER TABLE "vehiclepositions_positions" 
  DROP COLUMN "last_stop_name",
  DROP COLUMN "next_stop_name",
  DROP COLUMN "this_stop_name";