-- ropidvymi_events
CREATE SEQUENCE IF NOT EXISTS ropidvymi_events_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

CREATE TABLE IF NOT EXISTS ropidvymi_events (
    id integer DEFAULT nextval('ropidvymi_events_id_seq'::regclass) NOT NULL,
    vymi_id integer NOT NULL,
    vymi_id_dtb integer NOT NULL,
    state integer NOT NULL,
    pa_01 character varying(150) NOT NULL,
    pa_02 character varying(150) NOT NULL,
    pa_03 character varying(150) NOT NULL,
    pa_04 character varying(150) NOT NULL,
    record_type integer NOT NULL,
    event_type bigint,
    channels integer,
    title character varying(255),
    time_from timestamp with time zone NOT NULL,
    time_to_type integer NOT NULL,
    time_to timestamp with time zone,
    expiration_date timestamp with time zone,
    transportation_type integer NOT NULL,
    priority integer,
    cause character varying(1000),
    link character varying(255),
    ropid_action character varying(100000),
    dpp_action character varying(100000),
    description character varying(1000),
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),

    CONSTRAINT ropidvymi_events_pkey PRIMARY KEY (id),
    CONSTRAINT ropidvymi_events_vymi_id_uniq UNIQUE (vymi_id)
);

-- ropidvymi_events_routes
CREATE SEQUENCE IF NOT EXISTS ropidvymi_events_routes_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

CREATE TABLE IF NOT EXISTS ropidvymi_events_routes (
    id integer DEFAULT nextval('ropidvymi_events_routes_id_seq'::regclass) NOT NULL,
    event_id integer NOT NULL,
    gtfs_route_id character varying(255),
    vymi_id integer NOT NULL,
    vymi_id_dtb integer NOT NULL,
    number integer NOT NULL,
    name character varying(255) NOT NULL,
    route_type integer NOT NULL,
    valid_from timestamp with time zone,
    valid_to timestamp with time zone,
    text character varying(10000),
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),

    CONSTRAINT ropidvymi_events_routes_pkey PRIMARY KEY (event_id, vymi_id),
    CONSTRAINT ropidvymi_events_routes_fkey FOREIGN KEY (event_id) REFERENCES ropidvymi_events(id)
        ON DELETE CASCADE
);

CREATE INDEX IF NOT EXISTS ropidvymi_events_routes_idx ON ropidvymi_events_routes USING btree (event_id, gtfs_route_id);

-- ropidvymi_events_stops
CREATE SEQUENCE IF NOT EXISTS ropidvymi_events_stops_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

CREATE TABLE IF NOT EXISTS ropidvymi_events_stops (
    id integer DEFAULT nextval('ropidvymi_events_stops_id_seq'::regclass) NOT NULL,
    event_id integer NOT NULL,
    gtfs_stop_id character varying(255),
    vymi_id integer NOT NULL,
    vymi_id_dtb integer NOT NULL,
    node_number integer NOT NULL,
    stop_number integer NOT NULL,
    stop_type integer NOT NULL,
    valid_from timestamp with time zone,
    valid_to timestamp with time zone,
    text character varying(10000),
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),

    CONSTRAINT ropidvymi_events_stops_pkey PRIMARY KEY (event_id, vymi_id),
    CONSTRAINT ropidvymi_events_stops_fkey FOREIGN KEY (event_id) REFERENCES ropidvymi_events(id)
        ON DELETE CASCADE
);

CREATE INDEX IF NOT EXISTS ropidvymi_events_stops_idx ON ropidvymi_events_stops USING btree (event_id, gtfs_stop_id);
