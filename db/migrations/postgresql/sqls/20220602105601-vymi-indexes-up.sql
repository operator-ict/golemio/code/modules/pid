CREATE INDEX IF NOT EXISTS ropidvymi_events_record_type_expiration_date_idx
    ON ropidvymi_events USING btree (record_type, expiration_date);

CREATE INDEX IF NOT EXISTS ropidvymi_events_time_from_expiration_date_idx
    ON ropidvymi_events USING btree (time_from, expiration_date);

CREATE INDEX IF NOT EXISTS ropidvymi_events_stops_gtfs_stop_id_text_idx
    ON ropidvymi_events_stops USING btree (gtfs_stop_id, text);
