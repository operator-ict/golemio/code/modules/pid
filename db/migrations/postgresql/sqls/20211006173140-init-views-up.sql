-- v_ropidgtfs_trips_shapes_view
CREATE OR REPLACE VIEW v_ropidgtfs_trips_shapes_view AS
 SELECT ropidgtfs_trips.bikes_allowed,
    ropidgtfs_trips.block_id,
    ropidgtfs_trips.direction_id,
    ropidgtfs_trips.exceptional,
    ropidgtfs_trips.route_id,
    ropidgtfs_trips.service_id,
    ropidgtfs_trips.shape_id,
    ropidgtfs_trips.trip_headsign,
    ropidgtfs_trips.trip_id,
    ropidgtfs_trips.wheelchair_accessible,
    shapes.shape_dist_traveled AS shapes_shape_dist_traveled,
    shapes.shape_id AS shapes_shape_id,
    shapes.shape_pt_lat AS shapes_shape_pt_lat,
    shapes.shape_pt_lon AS shapes_shape_pt_lon,
    shapes.shape_pt_sequence AS shapes_shape_pt_sequence
   FROM (ropidgtfs_trips ropidgtfs_trips
     LEFT JOIN ropidgtfs_shapes shapes ON (((ropidgtfs_trips.shape_id)::text = (shapes.shape_id)::text)))
  ORDER BY shapes.shape_pt_sequence;

-- v_ropidgtfs_trips_stop_times_view
CREATE OR REPLACE VIEW v_ropidgtfs_trips_stop_times_view AS
 SELECT ropidgtfs_trips.bikes_allowed,
    ropidgtfs_trips.block_id,
    ropidgtfs_trips.direction_id,
    ropidgtfs_trips.exceptional,
    ropidgtfs_trips.route_id,
    ropidgtfs_trips.service_id,
    ropidgtfs_trips.shape_id,
    ropidgtfs_trips.trip_headsign,
    ropidgtfs_trips.trip_id,
    ropidgtfs_trips.wheelchair_accessible,
    stop_times.arrival_time AS stop_times_arrival_time,
    stop_times.arrival_time_seconds AS stop_times_arrival_time_seconds,
    stop_times.departure_time AS stop_times_departure_time,
    stop_times.departure_time_seconds AS stop_times_departure_time_seconds,
    stop_times.shape_dist_traveled AS stop_times_shape_dist_traveled,
    stop_times.stop_id AS stop_times_stop_id,
    stop_times.stop_sequence AS stop_times_stop_sequence,
    stop_times.stop_headsign AS stop_times_stop_headsign,
    stop_times.trip_id AS stop_times_trip_id,
    "stop_times->stop".stop_id AS stop_times_stop_stop_id,
    "stop_times->stop".stop_lat AS stop_times_stop_stop_lat,
    "stop_times->stop".stop_lon AS stop_times_stop_stop_lon
   FROM ((ropidgtfs_trips ropidgtfs_trips
     LEFT JOIN ropidgtfs_stop_times stop_times ON (((ropidgtfs_trips.trip_id)::text = (stop_times.trip_id)::text)))
     LEFT JOIN ropidgtfs_stops "stop_times->stop" ON (((stop_times.stop_id)::text = ("stop_times->stop".stop_id)::text)))
  ORDER BY stop_times.stop_sequence;

-- v_vehiclepositions_last_position
CREATE OR REPLACE VIEW v_vehiclepositions_last_position AS
 SELECT t2.created_at,
    t2.updated_at,
    t2.delay,
    t2.delay_stop_arrival,
    t2.delay_stop_departure,
    t2.next_stop_id,
    t2.shape_dist_traveled,
    t2.is_canceled,
    t2.lat,
    t2.lng,
    t2.origin_time,
    t2.origin_timestamp,
    t2.tracking,
    t2.trips_id,
    t2.create_batch_id,
    t2.created_by,
    t2.update_batch_id,
    t2.updated_by,
    t2.id,
    t2.bearing,
    t2.cis_last_stop_id,
    t2.cis_last_stop_sequence,
    t2.last_stop_id,
    t2.last_stop_sequence,
    t2.next_stop_sequence,
    t2.speed,
    t2.last_stop_arrival_time,
    t2.last_stop_departure_time,
    t2.next_stop_arrival_time,
    t2.next_stop_departure_time,
    t2.asw_last_stop_id,
    t1.gtfs_route_id,
    t1.gtfs_route_short_name,
    t1.gtfs_route_type,
    t1.gtfs_trip_id,
    t1.gtfs_trip_headsign,
    t1.vehicle_type_id,
    t1.wheelchair_accessible
   FROM (vehiclepositions_trips t1
     LEFT JOIN vehiclepositions_positions t2 ON ((t1.last_position_id = t2.id)))
  WHERE ((t2.updated_at > (now() - '00:10:00'::interval)) AND ((t2.state_position)::text = ANY (ARRAY[('on_track'::character varying)::text, ('off_track'::character varying)::text, ('at_stop'::character varying)::text, ('before_track'::character varying)::text, ('after_track'::character varying)::text, ('canceled'::character varying)::text])))
  ORDER BY t2.trips_id, t2.updated_at DESC;

-- materialized v_ropidgtfs_services_first14days
CREATE MATERIALIZED VIEW IF NOT EXISTS v_ropidgtfs_services_first14days AS
 SELECT dates.date,
    (date_part('day'::text, ((dates.date)::timestamp without time zone - (to_char(timezone('Europe/Prague'::text, now()), 'YYYY-MM-DD'::text))::timestamp without time zone)))::integer AS day_diff,
    ropidgtfs_calendar.service_id
   FROM (( SELECT (date_trunc('day'::text, dd.dd))::date AS date
           FROM generate_series(((( SELECT (CURRENT_TIMESTAMP - '1 day'::interval)))::date)::timestamp with time zone, ((( SELECT (CURRENT_TIMESTAMP + '12 days'::interval)))::date)::timestamp with time zone, '1 day'::interval) dd(dd)) dates
     JOIN ropidgtfs_calendar ON ((1 = 1)))
  WHERE (((dates.date >= (ropidgtfs_calendar.start_date)::date) AND (dates.date <= (ropidgtfs_calendar.end_date)::date) AND (((btrim(to_char((dates.date)::timestamp with time zone, 'day'::text)) = 'monday'::text) AND (ropidgtfs_calendar.monday = 1)) OR ((btrim(to_char((dates.date)::timestamp with time zone, 'day'::text)) = 'tuesday'::text) AND (ropidgtfs_calendar.tuesday = 1)) OR ((btrim(to_char((dates.date)::timestamp with time zone, 'day'::text)) = 'wednesday'::text) AND (ropidgtfs_calendar.wednesday = 1)) OR ((btrim(to_char((dates.date)::timestamp with time zone, 'day'::text)) = 'thursday'::text) AND (ropidgtfs_calendar.thursday = 1)) OR ((btrim(to_char((dates.date)::timestamp with time zone, 'day'::text)) = 'friday'::text) AND (ropidgtfs_calendar.friday = 1)) OR ((btrim(to_char((dates.date)::timestamp with time zone, 'day'::text)) = 'saturday'::text) AND (ropidgtfs_calendar.saturday = 1)) OR ((btrim(to_char((dates.date)::timestamp with time zone, 'day'::text)) = 'sunday'::text) AND (ropidgtfs_calendar.sunday = 1))) AND (NOT ((ropidgtfs_calendar.service_id)::text IN ( SELECT ropidgtfs_calendar_dates.service_id
           FROM ropidgtfs_calendar_dates
          WHERE ((ropidgtfs_calendar_dates.exception_type = 2) AND (dates.date = (ropidgtfs_calendar_dates.date)::date)))))) OR ((ropidgtfs_calendar.service_id)::text IN ( SELECT ropidgtfs_calendar_dates.service_id
           FROM ropidgtfs_calendar_dates
          WHERE ((ropidgtfs_calendar_dates.exception_type = 1) AND (dates.date = (ropidgtfs_calendar_dates.date)::date)))))
  ORDER BY dates.date;

CREATE UNIQUE INDEX IF NOT EXISTS v_ropidgtfs_services_first14days_date_service_id_idx ON v_ropidgtfs_services_first14days USING btree (date, service_id);

-- materialized v_ropidgtfs_trips_minmaxsequences
CREATE MATERIALIZED VIEW IF NOT EXISTS v_ropidgtfs_trips_minmaxsequences AS
 SELECT ropidgtfs_stop_times.trip_id,
    max(ropidgtfs_stop_times.stop_sequence) AS max_stop_sequence,
    min(ropidgtfs_stop_times.stop_sequence) AS min_stop_sequence,
    (GREATEST(max((ropidgtfs_stop_times.arrival_time)::interval), max((ropidgtfs_stop_times.departure_time)::interval)))::text AS max_stop_time,
    (LEAST(min((ropidgtfs_stop_times.arrival_time)::interval), min((ropidgtfs_stop_times.departure_time)::interval)))::text AS min_stop_time
   FROM ropidgtfs_stop_times
  GROUP BY ropidgtfs_stop_times.trip_id;

CREATE UNIQUE INDEX IF NOT EXISTS v_ropidgtfs_trips_minmaxsequences_trip_id_idx ON v_ropidgtfs_trips_minmaxsequences USING btree (trip_id);

-- materialized v_ropidgtfs_departures
CREATE MATERIALIZED VIEW IF NOT EXISTS v_ropidgtfs_departures AS
 SELECT t.stop_sequence,
    t.stop_headsign,
    t.pickup_type,
    t.drop_off_type,
    t.arrival_time,
        CASE
            WHEN (date_part('hour'::text, (t.arrival_time)::interval) > (23)::double precision) THEN make_timestamptz((date_part('year'::text, (t4.date + '1 day'::interval)))::integer, (date_part('month'::text, (t4.date + '1 day'::interval)))::integer, (date_part('day'::text, (t4.date + '1 day'::interval)))::integer, ((date_part('hour'::text, (t.arrival_time)::interval))::integer - 24), (date_part('minute'::text, (t.arrival_time)::interval))::integer, date_part('second'::text, (t.arrival_time)::interval), 'Europe/Prague'::text)
            ELSE make_timestamptz((date_part('year'::text, t4.date))::integer, (date_part('month'::text, t4.date))::integer, (date_part('day'::text, t4.date))::integer, (date_part('hour'::text, (t.arrival_time)::interval))::integer, (date_part('minute'::text, (t.arrival_time)::interval))::integer, date_part('second'::text, (t.arrival_time)::interval), 'Europe/Prague'::text)
        END AS arrival_datetime,
    t.departure_time,
        CASE
            WHEN (date_part('hour'::text, (t.departure_time)::interval) > (23)::double precision) THEN make_timestamptz((date_part('year'::text, (t4.date + '1 day'::interval)))::integer, (date_part('month'::text, (t4.date + '1 day'::interval)))::integer, (date_part('day'::text, (t4.date + '1 day'::interval)))::integer, ((date_part('hour'::text, (t.departure_time)::interval))::integer - 24), (date_part('minute'::text, (t.departure_time)::interval))::integer, date_part('second'::text, (t.departure_time)::interval), 'Europe/Prague'::text)
            ELSE make_timestamptz((date_part('year'::text, t4.date))::integer, (date_part('month'::text, t4.date))::integer, (date_part('day'::text, t4.date))::integer, (date_part('hour'::text, (t.departure_time)::interval))::integer, (date_part('minute'::text, (t.departure_time)::interval))::integer, date_part('second'::text, (t.departure_time)::interval), 'Europe/Prague'::text)
        END AS departure_datetime,
    t0.stop_id,
    t0.stop_name,
    t0.platform_code,
    t0.wheelchair_boarding,
    t1.min_stop_sequence,
    t1.max_stop_sequence,
    t2.trip_id,
    t2.trip_headsign,
    t2.trip_short_name,
    t2.wheelchair_accessible,
    t3.service_id,
    t4.date,
    t5.route_short_name,
    t5.route_type,
    t5.route_id,
    t5.is_night,
    t5.is_regional,
    t5.is_substitute_transport,
    t6.stop_sequence AS next_stop_sequence,
    t6.stop_id AS next_stop_id,
    t7.stop_sequence AS last_stop_sequence,
    t7.stop_id AS last_stop_id
   FROM ((((((((ropidgtfs_stop_times t
     LEFT JOIN ropidgtfs_stops t0 ON (((t.stop_id)::text = (t0.stop_id)::text)))
     LEFT JOIN ropidgtfs_trips t2 ON (((t.trip_id)::text = (t2.trip_id)::text)))
     JOIN v_ropidgtfs_services_first14days t4 ON (((t2.service_id)::text = (t4.service_id)::text)))
     LEFT JOIN v_ropidgtfs_trips_minmaxsequences t1 ON (((t.trip_id)::text = (t1.trip_id)::text)))
     LEFT JOIN ropidgtfs_calendar t3 ON (((t2.service_id)::text = (t3.service_id)::text)))
     LEFT JOIN ropidgtfs_routes t5 ON (((t2.route_id)::text = (t5.route_id)::text)))
     LEFT JOIN ropidgtfs_stop_times t6 ON ((((t.trip_id)::text = (t6.trip_id)::text) AND (t6.stop_sequence = (t.stop_sequence + 1)))))
     LEFT JOIN ropidgtfs_stop_times t7 ON ((((t.trip_id)::text = (t7.trip_id)::text) AND (t7.stop_sequence = (t.stop_sequence - 1)))));

CREATE INDEX IF NOT EXISTS v_ropidgtfs_departures_stop_id_idx ON v_ropidgtfs_departures USING btree (stop_id);
CREATE UNIQUE INDEX IF NOT EXISTS v_ropidgtfs_departures_unique_id ON v_ropidgtfs_departures USING btree (service_id, date, trip_id, stop_sequence);
