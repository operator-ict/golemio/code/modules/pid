ALTER TABLE vehiclepositions_trips ADD gtfs_date date NULL;
ALTER TABLE vehiclepositions_trips_history ADD gtfs_date date NULL;

update vehiclepositions_trips set gtfs_date = TO_TIMESTAMP(start_timestamp/1000)::DATE;

CREATE INDEX vehiclepositions_trips_gtfs_date_idx ON vehiclepositions_trips (gtfs_date);
