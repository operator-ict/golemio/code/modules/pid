ALTER TABLE vehiclepositions_metro_runs_messages DROP CONSTRAINT "metro_runs_messages_pkey";
ALTER TABLE vehiclepositions_metro_runs_messages ADD COLUMN "id" SERIAL;
ALTER TABLE vehiclepositions_metro_runs_messages
    ADD CONSTRAINT "metro_runs_messages_pkey" PRIMARY KEY (id)
