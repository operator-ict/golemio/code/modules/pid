ALTER TABLE ropidgtfs_run_numbers DROP COLUMN IF EXISTS trip_number;

ALTER TABLE ropidgtfs_precomputed_trip_schedule DROP COLUMN IF EXISTS trip_number;
