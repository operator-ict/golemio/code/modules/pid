-- Drop internal_run_number and internal_route_name columns
ALTER TABLE vehiclepositions_trips
    DROP COLUMN internal_run_number,
    DROP COLUMN internal_route_name;

ALTER TABLE vehiclepositions_trips_history
    DROP COLUMN internal_run_number,
    DROP COLUMN internal_route_name;

-- Rename sequence_id to run_number
ALTER TABLE vehiclepositions_trips
    RENAME run_number TO sequence_id;

ALTER TABLE vehiclepositions_trips_history
    RENAME run_number TO sequence_id;

-- Drop origin_route_name from ropidgtfs_scheduled_trips
ALTER TABLE ropidgtfs_scheduled_trips
    DROP COLUMN origin_route_name;

DROP INDEX IF EXISTS ropidgtfs_departures_start_timestamp_idx;
