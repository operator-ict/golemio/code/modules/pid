CREATE INDEX IF NOT EXISTS ropidgtfs_stops_asw_idx ON ropidgtfs_stops_actual USING btree (asw_node_id, asw_stop_id);
