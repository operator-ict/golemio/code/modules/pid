ALTER TABLE vehiclepositions_metro_runs_messages ALTER COLUMN delay_origin TYPE smallint
    USING
        CASE WHEN delay_origin <= 0 THEN greatest(delay_origin, -32768)
             ELSE least(delay_origin, 32767)
        END;
