alter table jis_infotexts
alter column severity_level type varchar(255) using severity_level::varchar(255);

alter table jis_infotexts
drop column created_timestamp;

drop type jis_infotext_severity_level;

