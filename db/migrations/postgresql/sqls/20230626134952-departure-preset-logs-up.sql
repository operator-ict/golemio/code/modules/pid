create table ropid_departures_preset_logs (
    id bigserial,
    device_alias varchar(255),
    received_at timestamp with time zone,
    is_processed boolean not null,
    request_url text not null,
    request_method varchar(10) not null,
    request_user_agent text not null,
    response_status smallint not null,
    response_time_ms int not null,

    -- audit fields
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),

    constraint ropid_departures_preset_logs_pk primary key (id)
);

create unique index ropid_departures_preset_logs_unique_idx on ropid_departures_preset_logs (device_alias, received_at);
create index ropid_departures_preset_logs_is_processed_idx on ropid_departures_preset_logs (is_processed);

alter table ropid_departures_presets add column is_testing boolean not null default false;
update ropid_departures_presets set is_testing = true where note like '%(testovací)';

create unique index ropid_departures_presets_unique_idx on ropid_departures_presets (route_name);
