DROP PROCEDURE ropidgtfs_replaceAllSavedTables(in tableNames varchar(255));
DROP PROCEDURE ropidgtfs_prepareTmpTables();
DROP PROCEDURE ropidgtfs_hotswap(IN tableName varchar(255));
DROP PROCEDURE ropidgtfs_prepareTmpTable(IN tableName varchar(255));
DROP PROCEDURE ropidgtfs_cleanTmpTables();
DROP PROCEDURE ropidgtfs_cleanOldTables();
DROP PROCEDURE ropidgtfs_preparePrecomputedTmpTables();
