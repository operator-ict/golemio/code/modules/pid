alter table vehiclepositions_vehicle_descriptors add column is_wheelchair_accessible boolean;

alter table vehiclepositions_regional_bus_runs_messages
    rename cis_route_id to cis_line_id;

alter table vehiclepositions_regional_bus_runs_messages
    rename cis_trip_id to cis_trip_number;

alter table vehiclepositions_regional_bus_runs_messages
    alter column cis_trip_number type int using cis_trip_number::int;

create index ropidgtfs_route_sub_agencies_cis_idx on ropidgtfs_route_sub_agencies (route_licence_number) where route_licence_number is not null;
create index ropidgtfs_route_sub_agencies_actual_cis_idx on ropidgtfs_route_sub_agencies_actual (route_licence_number) where route_licence_number is not null;

create index ropidgtfs_run_numbers_lookup_idx on ropidgtfs_run_numbers (route_id, trip_number);
create index ropidgtfs_run_numbers_lookup_actual_idx on ropidgtfs_run_numbers_actual (route_id, trip_number);
