-- vehiclepositions_stops
CREATE TABLE IF NOT EXISTS vehiclepositions_stops (
    arrival_time time without time zone,
    arrival_timestamp bigint,
    cis_stop_id integer,
    cis_stop_platform_code character varying(255),
    cis_stop_sequence integer NOT NULL,
    created_at timestamp with time zone,
    delay_arrival integer,
    delay_departure integer,
    delay_type integer,
    departure_time time without time zone,
    departure_timestamp bigint,
    updated_at timestamp with time zone,
    trips_id character varying(255) NOT NULL,
    create_batch_id bigint,
    created_by character varying(150),
    update_batch_id bigint,
    updated_by character varying(150),
    arrival_delay_type integer,
    asw_stop_id character varying(50),
    stop_id character varying(50),
    stop_sequence integer,
    arrival_timestamp_real bigint,
    departure_timestamp_real bigint,

    CONSTRAINT vehiclepositions_stops_pkey PRIMARY KEY (cis_stop_sequence, trips_id)
);
