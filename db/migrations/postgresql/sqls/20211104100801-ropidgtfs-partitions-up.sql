-- Table: ropidgtfs_agency
CREATE TABLE ropidgtfs_agency_actual (LIKE ropidgtfs_agency);
ALTER TABLE ropidgtfs_agency DROP CONSTRAINT ropidgtfs_agency_pkey;
ALTER TABLE ropidgtfs_agency_actual ADD CONSTRAINT ropidgtfs_agency_pkey PRIMARY KEY (agency_id);
TRUNCATE TABLE ropidgtfs_agency;
ALTER TABLE ropidgtfs_agency_actual INHERIT ropidgtfs_agency;


-- Table: ropidgtfs_calendar
CREATE TABLE ropidgtfs_calendar_actual (LIKE ropidgtfs_calendar);
ALTER TABLE ropidgtfs_calendar DROP CONSTRAINT IF EXISTS ropidgtfs_calendar_pkey1;
ALTER TABLE ropidgtfs_calendar_actual ADD CONSTRAINT ropidgtfs_calendar_pkey PRIMARY KEY (service_id);
TRUNCATE TABLE ropidgtfs_calendar;
ALTER TABLE ropidgtfs_calendar_actual INHERIT ropidgtfs_calendar;


-- Table: ropidgtfs_calendar_dates
CREATE TABLE ropidgtfs_calendar_dates_actual (LIKE ropidgtfs_calendar_dates);
ALTER TABLE ropidgtfs_calendar_dates DROP CONSTRAINT IF EXISTS ropidgtfs_calendar_dates_pkey1;
ALTER TABLE ropidgtfs_calendar_dates_actual ADD CONSTRAINT ropidgtfs_calendar_dates_pkey PRIMARY KEY (date, service_id);
TRUNCATE TABLE ropidgtfs_calendar_dates;
ALTER TABLE ropidgtfs_calendar_dates_actual INHERIT ropidgtfs_calendar_dates;


-- Table: ropidgtfs_routes
CREATE TABLE ropidgtfs_routes_actual (LIKE ropidgtfs_routes);
ALTER TABLE ropidgtfs_routes DROP CONSTRAINT IF EXISTS ropidgtfs_routes_pkey_pkey;
ALTER TABLE ropidgtfs_routes DROP CONSTRAINT IF EXISTS ropidgtfs_routes_pkey;
ALTER TABLE ropidgtfs_routes_actual ADD CONSTRAINT ropidgtfs_routes_pkey PRIMARY KEY (route_id);
TRUNCATE TABLE ropidgtfs_routes;
ALTER TABLE ropidgtfs_routes_actual INHERIT ropidgtfs_routes;


-- Table: ropidgtfs_shapes
CREATE TABLE ropidgtfs_shapes_actual (LIKE ropidgtfs_shapes);
ALTER TABLE ropidgtfs_shapes DROP CONSTRAINT IF EXISTS ropidgtfs_shapes_pkey;
ALTER TABLE ropidgtfs_shapes_actual ADD CONSTRAINT ropidgtfs_shapes_pkey PRIMARY KEY (shape_id, shape_pt_sequence);
TRUNCATE TABLE ropidgtfs_shapes;
ALTER TABLE ropidgtfs_shapes_actual INHERIT ropidgtfs_shapes;

-- Table: ropidgtfs_stop_times
CREATE TABLE ropidgtfs_stop_times_actual (LIKE ropidgtfs_stop_times);
ALTER TABLE ropidgtfs_stop_times DROP constraint IF EXISTS ropidgtfs_stop_times_pkey1;
ALTER TABLE ropidgtfs_stop_times_actual ADD CONSTRAINT ropidgtfs_stop_times_pkey PRIMARY KEY (stop_sequence, trip_id);
DROP INDEX IF EXISTS ropidgtfs_stop_times_trip_id;
CREATE INDEX ropidgtfs_stop_times_trip_id ON ropidgtfs_stop_times_actual USING btree (trip_id);
TRUNCATE TABLE ropidgtfs_stop_times;
ALTER TABLE ropidgtfs_stop_times_actual INHERIT ropidgtfs_stop_times;


-- Table: ropidgtfs_stops
CREATE TABLE ropidgtfs_stops_actual (LIKE ropidgtfs_stops);
ALTER TABLE ropidgtfs_stops DROP constraint IF EXISTS ropidgtfs_stops_pkey;
ALTER TABLE ropidgtfs_stops_actual ADD CONSTRAINT ropidgtfs_stops_pkey PRIMARY KEY (stop_id);
TRUNCATE TABLE ropidgtfs_stops;
ALTER TABLE ropidgtfs_stops_actual INHERIT ropidgtfs_stops;


-- Table: ropidgtfs_trips
CREATE TABLE ropidgtfs_trips_actual (LIKE ropidgtfs_trips);
ALTER TABLE ropidgtfs_trips DROP constraint IF EXISTS ropidgtfs_trips_pkey;
ALTER TABLE ropidgtfs_trips_actual ADD CONSTRAINT ropidgtfs_trips_pkey PRIMARY KEY (trip_id);
TRUNCATE TABLE ropidgtfs_trips;
ALTER TABLE ropidgtfs_trips_actual INHERIT ropidgtfs_trips;


-- Table: ropidgtfs_cis_stop_groups
CREATE TABLE ropidgtfs_cis_stop_groups_actual (LIKE ropidgtfs_cis_stop_groups);
ALTER TABLE ropidgtfs_cis_stop_groups DROP CONSTRAINT IF EXISTS ropidgtfs_cis_stop_groups_pkey;
ALTER TABLE ropidgtfs_cis_stop_groups_actual ADD CONSTRAINT ropidgtfs_cis_stop_groups_pkey PRIMARY KEY (cis);
TRUNCATE TABLE ropidgtfs_cis_stop_groups;
ALTER TABLE ropidgtfs_cis_stop_groups_actual INHERIT ropidgtfs_cis_stop_groups;


-- Table: ropidgtfs_cis_stops
CREATE TABLE ropidgtfs_cis_stops_actual (LIKE ropidgtfs_cis_stops);
ALTER TABLE ropidgtfs_cis_stops DROP CONSTRAINT IF EXISTS ropidgtfs_cis_stops_pkey;
ALTER TABLE ropidgtfs_cis_stops_actual ADD CONSTRAINT ropidgtfs_cis_stops_pkey PRIMARY KEY (id);
TRUNCATE TABLE ropidgtfs_cis_stops;
ALTER TABLE ropidgtfs_cis_stops_actual INHERIT ropidgtfs_cis_stops;


-- Table: ropidgtfs_run_numbers
CREATE TABLE ropidgtfs_run_numbers_actual (LIKE ropidgtfs_run_numbers);
ALTER TABLE ropidgtfs_run_numbers DROP CONSTRAINT IF EXISTS ropidgtfs_run_numbers_pkey;
ALTER TABLE ropidgtfs_run_numbers_actual ADD CONSTRAINT ropidgtfs_run_numbers_pkey PRIMARY KEY (run_number, service_id, trip_id);
TRUNCATE TABLE ropidgtfs_run_numbers;
ALTER TABLE ropidgtfs_run_numbers_actual INHERIT ropidgtfs_run_numbers;


-- Table: ropidgtfs_ois
CREATE TABLE ropidgtfs_ois_actual (LIKE ropidgtfs_ois);
ALTER TABLE ropidgtfs_ois DROP CONSTRAINT IF EXISTS ropidgtfs_ois_pkey;
ALTER TABLE ropidgtfs_ois_actual ADD CONSTRAINT ropidgtfs_ois_pkey PRIMARY KEY (ois);
TRUNCATE TABLE ropidgtfs_ois;
ALTER TABLE ropidgtfs_ois_actual INHERIT ropidgtfs_ois;


-- Table: ropidgtfs_departures
CREATE TABLE ropidgtfs_departures
AS
SELECT *
  FROM v_ropidgtfs_departures WHERE FALSE;

CREATE TABLE ropidgtfs_departures_actual (LIKE ropidgtfs_departures);
DROP INDEX IF EXISTS ropidgtfs_departures_stop_id_idx;
CREATE INDEX ropidgtfs_departures_stop_id_idx ON ropidgtfs_departures_actual USING btree (stop_id);
DROP INDEX IF EXISTS ropidgtfs_departures_unique_id;
CREATE UNIQUE INDEX ropidgtfs_departures_unique_id ON ropidgtfs_departures_actual USING btree (service_id, date, trip_id, stop_sequence);

ALTER TABLE ropidgtfs_departures_actual INHERIT ropidgtfs_departures;

DROP MATERIALIZED VIEW "v_ropidgtfs_departures";

--CREATE TABLE ropidgtfs_departures (
--	stop_sequence int4,
--	stop_headsign varchar(255),
--	pickup_type varchar(255),
--	drop_off_type varchar(255),
--	arrival_time varchar(255),
--	arrival_datetime timestamptz,
--	departure_time varchar(255),
--	departure_datetime timestamptz,
--	stop_id varchar(255),
--	stop_name varchar(255),
--	platform_code varchar(255),
--	wheelchair_boarding int4,
--	min_stop_sequence int4,
--	max_stop_sequence int4,
--	trip_id varchar(255),
--	trip_headsign varchar(255),
--	trip_short_name varchar(255),
--	wheelchair_accessible int4,
--	service_id varchar(255),
--	"date" date,
--	route_short_name varchar(255),
--	route_type varchar(255),
--	route_id varchar(255),
--	is_night varchar(255),
--	is_regional varchar(255),
--	is_substitute_transport varchar(255),
--	next_stop_sequence int4,
--	next_stop_id varchar(255),
--	last_stop_sequence int4,
--	last_stop_id varchar(255)
--);


-- Table: ropidgtfs_services_first14days
CREATE TABLE ropidgtfs_services_first14days
AS
SELECT *
  FROM v_ropidgtfs_services_first14days WHERE FALSE;

CREATE TABLE ropidgtfs_services_first14days_actual (LIKE ropidgtfs_services_first14days);
DROP INDEX IF EXISTS ropidgtfs_services_first14days_date_service_id_idx;
CREATE UNIQUE INDEX ropidgtfs_services_first14days_date_service_id_idx ON ropidgtfs_services_first14days_actual USING btree (date, service_id);

ALTER TABLE ropidgtfs_services_first14days_actual INHERIT ropidgtfs_services_first14days;

DROP MATERIALIZED VIEW "v_ropidgtfs_services_first14days";

--CREATE TABLE ropidgtfs_services_first14days {
--    "date" date,
--    day_diff integer,
--    service_id character varying(255)
--}


-- Table: ropidgtfs_trips_minmaxsequences
CREATE TABLE ropidgtfs_trips_minmaxsequences
AS
SELECT *
  FROM v_ropidgtfs_trips_minmaxsequences WHERE FALSE;

CREATE TABLE ropidgtfs_trips_minmaxsequences_actual (LIKE ropidgtfs_trips_minmaxsequences);
CREATE UNIQUE INDEX ropidgtfs_trips_minmaxsequences_trip_id_idx ON ropidgtfs_trips_minmaxsequences_actual USING btree (trip_id);
ALTER TABLE ropidgtfs_trips_minmaxsequences_actual INHERIT ropidgtfs_trips_minmaxsequences;

DROP MATERIALIZED VIEW "v_ropidgtfs_trips_minmaxsequences";

--CREATE TABLE ropidgtfs_trips_minmaxsequences {
--    trip_id	character varying(255),
--    max_stop_sequence integer,
--    min_stop_sequence integer,
--    max_stop_time text,
--    min_stop_time text,
--}
