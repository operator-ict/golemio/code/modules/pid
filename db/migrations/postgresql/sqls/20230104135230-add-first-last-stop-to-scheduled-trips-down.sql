ALTER TABLE ropidgtfs_scheduled_trips
  DROP COLUMN first_stop_id,
  DROP COLUMN last_stop_id;
