ALTER TYPE vehicle_descriptor_state
    ADD VALUE 'muz';

ALTER TYPE vehicle_descriptor_state
    ADD VALUE 'nez';

ALTER TYPE vehicle_descriptor_state
    ADD VALUE 'ods';

ALTER TYPE vehicle_descriptor_state
    ADD VALUE 'sluz';

ALTER TYPE vehicle_descriptor_state
    ADD VALUE 'prod';

ALTER TYPE vehicle_descriptor_state
    ADD VALUE 'zrus';

ALTER TYPE vehicle_descriptor_state
    ADD VALUE 'vrak';

