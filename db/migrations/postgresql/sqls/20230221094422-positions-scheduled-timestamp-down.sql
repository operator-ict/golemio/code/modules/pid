ALTER TABLE vehiclepositions_positions
    DROP COLUMN scheduled_timestamp;

ALTER TABLE vehiclepositions_positions_history
    DROP COLUMN scheduled_timestamp;

COMMENT ON COLUMN vehiclepositions_positions.origin_timestamp
    IS NULL;
