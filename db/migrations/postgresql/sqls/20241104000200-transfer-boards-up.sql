-- Companion function for get_departures, but simplified to only return transfer departures
create function get_transfers (
	stopsIds varchar,
    dateDepartureBetweenStart timestamptz,
   	dateDepartureBetweenEnd timestamptz,
 	dateCanceledDepartureBetweenStart timestamptz,
 	dateCanceledDepartureBetweenEnd timestamptz
)
	returns table (
		departure_datetime_real timestamptz,
		departure_datetime timestamptz,
		stop_headsign varchar(70),
		stop_id varchar(25),
		platform_code varchar(10),
		trip_id varchar(50),
		trip_headsign varchar(100),
		trip_short_name varchar(30),
		route_short_name varchar(50),
		route_type int2,
		route_id varchar(20),
		is_canceled bool,
        "trip.cis_stop_platform_code" varchar(15),
		is_delay_available bool
	)
	language SQL
	set search_path from current
as $$
	select
		departure_boards_detailed."computed.departure_datetime_real",
		departure_boards_detailed."ropidgtfs_precomputed_departures.departure_datetime",
		departure_boards_detailed."ropidgtfs_precomputed_departures.stop_headsign",
		departure_boards_detailed."ropidgtfs_precomputed_departures.stop_id",
		departure_boards_detailed."ropidgtfs_precomputed_departures.platform_code",
		departure_boards_detailed."ropidgtfs_precomputed_departures.trip_id",
		departure_boards_detailed."ropidgtfs_precomputed_departures.trip_headsign",
		departure_boards_detailed."ropidgtfs_precomputed_departures.trip_short_name",
		departure_boards_detailed."ropidgtfs_precomputed_departures.route_short_name",
		departure_boards_detailed."ropidgtfs_precomputed_departures.route_type",
		departure_boards_detailed."ropidgtfs_precomputed_departures.route_id",
		departure_boards_detailed."is_canceled",
        departure_boards_detailed.cis_stop_platform_code as "trip.cis_stop_platform_code",
		case when departure_boards_detailed."trip.last_position.delay" is null then false else true end as "is_delay_available"
	from (
		select
			("departure_datetime"
				+ MAKE_INTERVAL(secs => (case when "trip.last_position.delay" is null then 0 else "trip.last_position.delay" end))
				- case when (MAKE_INTERVAL(secs => (case when "trip.last_position.delay" is null then 0 else "trip.last_position.delay" end)) > MAKE_INTERVAL())
					then least (
						MAKE_INTERVAL(secs => (case when "trip.last_position.delay" is null then 0 else "trip.last_position.delay" end)),
						("departure_datetime" - "arrival_datetime")::interval
					)
					else MAKE_INTERVAL()
					end
			) "computed.departure_datetime_real",
			"arrival_datetime" + MAKE_INTERVAL(secs => (
				CASE WHEN "trip.last_position.delay" IS NULL THEN 0 ELSE "trip.last_position.delay" end
			))  "computed.arrival_datetime_real",
			"ropidgtfs_precomputed_departures"."stop_headsign" as "ropidgtfs_precomputed_departures.stop_headsign",
			"ropidgtfs_precomputed_departures"."stop_id" as "ropidgtfs_precomputed_departures.stop_id",
			"ropidgtfs_precomputed_departures"."stop_sequence" as "ropidgtfs_precomputed_departures.stop_sequence",
			"ropidgtfs_precomputed_departures"."pickup_type" as "ropidgtfs_precomputed_departures.pickup_type",
			"ropidgtfs_precomputed_departures"."drop_off_type" as "ropidgtfs_precomputed_departures.drop_off_type",
			"ropidgtfs_precomputed_departures"."departure_datetime" as "ropidgtfs_precomputed_departures.departure_datetime",
			"ropidgtfs_precomputed_departures"."platform_code" as "ropidgtfs_precomputed_departures.platform_code",
			"ropidgtfs_precomputed_departures"."max_stop_sequence" as "ropidgtfs_precomputed_departures.max_stop_sequence",
			"ropidgtfs_precomputed_departures"."trip_id" as "ropidgtfs_precomputed_departures.trip_id",
			"ropidgtfs_precomputed_departures"."trip_headsign" as "ropidgtfs_precomputed_departures.trip_headsign",
			"ropidgtfs_precomputed_departures"."trip_short_name" as "ropidgtfs_precomputed_departures.trip_short_name",
			"ropidgtfs_precomputed_departures"."route_short_name" as "ropidgtfs_precomputed_departures.route_short_name",
			"ropidgtfs_precomputed_departures"."route_type" as "ropidgtfs_precomputed_departures.route_type",
			"ropidgtfs_precomputed_departures"."route_id" as "ropidgtfs_precomputed_departures.route_id",
			x."trip.last_position.delay" as "trip.last_position.delay",
			x."trip.is_canceled" as "is_canceled",
            cis_stop.cis_stop_platform_code as "cis_stop_platform_code"
		from
			"ropidgtfs_precomputed_departures" as "ropidgtfs_precomputed_departures"
		left outer join v_vehiclepositions_trip_position_vehicle_info as x on
			"ropidgtfs_precomputed_departures"."trip_id" = x."trip.gtfs_trip_id"
        left join vehiclepositions_cis_stops as cis_stop on
            cis_stop.rt_trip_id = x."trip.id"
            and cis_stop.cis_stop_group_id = "ropidgtfs_precomputed_departures"."cis_stop_group_id"
	) departure_boards_detailed
	where
		departure_boards_detailed."ropidgtfs_precomputed_departures.stop_id" = ANY(STRING_TO_ARRAY(stopsIds,','))
		and (
			(departure_boards_detailed."computed.arrival_datetime_real" between dateDepartureBetweenStart and dateDepartureBetweenEnd)
			or (
				departure_boards_detailed."is_canceled" = true
				and
					(departure_boards_detailed."computed.arrival_datetime_real" between dateCanceledDepartureBetweenStart and dateCanceledDepartureBetweenEnd)
			)
		)
		and (
			departure_boards_detailed."ropidgtfs_precomputed_departures.pickup_type" != '1' and departure_boards_detailed."ropidgtfs_precomputed_departures.stop_sequence" != departure_boards_detailed."ropidgtfs_precomputed_departures.max_stop_sequence"
		);
$$;