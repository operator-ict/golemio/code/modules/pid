DROP INDEX IF EXISTS v_ropidgtfs_departures_stop_id_idx;
DROP INDEX IF EXISTS v_ropidgtfs_departures_unique_id;
DROP MATERIALIZED VIEW IF EXISTS v_ropidgtfs_departures;

DROP INDEX IF EXISTS v_ropidgtfs_services_first14days_date_service_id_idx;
DROP MATERIALIZED VIEW IF EXISTS v_ropidgtfs_services_first14days;

DROP INDEX IF EXISTS v_ropidgtfs_trips_minmaxsequences_trip_id_idx;
DROP MATERIALIZED VIEW IF EXISTS v_ropidgtfs_trips_minmaxsequences;

DROP VIEW IF EXISTS v_ropidgtfs_trips_shapes_view;

DROP VIEW IF EXISTS v_ropidgtfs_trips_stop_times_view;

DROP VIEW IF EXISTS v_vehiclepositions_last_position;
