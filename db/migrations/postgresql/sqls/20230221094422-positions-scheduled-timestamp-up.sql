ALTER TABLE vehiclepositions_positions
    ADD scheduled_timestamp bigint NULL;

ALTER TABLE vehiclepositions_positions_history
    ADD scheduled_timestamp bigint NULL;

COMMENT ON COLUMN vehiclepositions_positions.scheduled_timestamp
    IS 'Time at which the position was scheduled to be sent from the vehicle';

COMMENT ON COLUMN vehiclepositions_positions.origin_timestamp
    IS 'Time at which the position was sent from the vehicle';
