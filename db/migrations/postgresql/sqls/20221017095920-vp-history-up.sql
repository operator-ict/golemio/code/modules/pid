-- pid.vehiclepositions_trips definition

CREATE TABLE vehiclepositions_trips_history (
	cis_line_id varchar(50) NULL,
	cis_trip_number int4 NULL,
	sequence_id int4 NULL,
	cis_line_short_name varchar(255) NULL,
	created_at timestamptz NULL,
	gtfs_route_id varchar(255) NULL,
	gtfs_route_short_name varchar(255) NULL,
	gtfs_trip_id varchar(255) NULL,
	id varchar(255) NOT NULL,
	updated_at timestamptz NULL,
	start_cis_stop_id int4 NULL,
	start_cis_stop_platform_code varchar(255) NULL,
	start_time time NULL,
	start_timestamp int8 NULL,
	vehicle_type_id int4 NULL,
	wheelchair_accessible bool NULL,
	create_batch_id int8 NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_by varchar(150) NULL,
	agency_name_scheduled varchar(255) NULL,
	origin_route_name varchar(255) NULL,
	agency_name_real varchar(255) NULL,
	vehicle_registration_number int4 NULL,
	gtfs_trip_headsign varchar(255) NULL,
	start_asw_stop_id varchar(50) NULL,
	gtfs_route_type int4 NULL,
	gtfs_block_id varchar(255) NULL,
	last_position_id int8 NULL,
	is_canceled bool NULL,
	end_timestamp int8 NULL,
	gtfs_trip_short_name varchar(255) NULL,
	CONSTRAINT vehiclepositions_trips_history_pkey PRIMARY KEY (id)
);
CREATE INDEX vehiclepositions_trips_history_gtfs_trip_id ON vehiclepositions_trips_history USING btree (gtfs_trip_id);
CREATE INDEX vehiclepositions_trips_history_idx0 ON vehiclepositions_trips_history USING hash (last_position_id);
CREATE INDEX vehiclepositions_trips_history_start_timestamp_idx ON vehiclepositions_trips_history USING btree (start_timestamp);

-- pid.vehiclepositions_positions definition

CREATE TABLE vehiclepositions_positions_history (
	created_at timestamptz NULL,
	delay int4 NULL,
	delay_stop_arrival int4 NULL,
	delay_stop_departure int4 NULL,
	next_stop_id varchar(255) NULL,
	shape_dist_traveled numeric NULL,
	is_canceled bool NULL,
	lat numeric NULL,
	lng numeric NULL,
	origin_time time NULL,
	origin_timestamp int8 NULL,
	tracking int4 NULL,
	trips_id varchar(255) NULL,
	create_batch_id int8 NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	id bigserial NOT NULL,
	bearing int4 NULL,
	cis_last_stop_id int4 NULL,
	cis_last_stop_sequence int4 NULL,
	last_stop_id varchar(255) NULL,
	last_stop_sequence int4 NULL,
	next_stop_sequence int4 NULL,
	speed int4 NULL,
	last_stop_arrival_time int8 NULL,
	last_stop_departure_time int8 NULL,
	next_stop_arrival_time int8 NULL,
	next_stop_departure_time int8 NULL,
	asw_last_stop_id varchar(50) NULL,
	state_process varchar(50) NULL,
	state_position varchar(50) NULL,
	this_stop_id varchar(50) NULL,
	this_stop_sequence int4 NULL,
	tcp_event varchar(50) NULL,
	last_stop_headsign varchar(255) NULL,
	last_stop_name varchar(255) NULL,
	next_stop_name varchar(255) NULL,
	this_stop_name varchar(255) NULL,
	valid_to int8 NULL,
	CONSTRAINT vehiclepositions_positions_history_pkey PRIMARY KEY (id)
);
CREATE INDEX vehiclepositions_positions_history_trips_id ON vehiclepositions_positions_history USING btree (trips_id);

CREATE or replace PROCEDURE vehiclepositions_data_retention(inout numberOfRows int, in dataRetentionMinutes int)
LANGUAGE plpgsql
AS $procedure$
	declare
		idsForDelete varchar(255)[];
	begin
        select array_agg(t.id) from pid.vehiclepositions_trips t
        left join pid.vehiclepositions_positions p on
            p.id = t.last_position_id
        where
            p.valid_to/1000 < extract(epoch from (NOW() - (dataRetentionMinutes || ' minutes')::interval))
            or (gtfs_trip_id is null and p.created_at < NOW() - (dataRetentionMinutes || ' minutes')::interval) -- entries with valid to is null
		into idsForDelete;

		INSERT INTO pid.vehiclepositions_trips_history
		select * from pid.vehiclepositions_trips where id = ANY(idsForDelete)
		on conflict do nothing;

		INSERT INTO pid.vehiclepositions_positions_history
		select * from pid.vehiclepositions_positions where trips_id = ANY(idsForDelete)
		on conflict do nothing;

		delete from pid.vehiclepositions_positions where trips_id = ANY(idsForDelete);
		delete from pid.vehiclepositions_trips where id = ANY(idsForDelete);

		select array_length(idsForDelete,1) into numberOfRows;
	end;
$procedure$;

DROP INDEX vehiclepositions_positions_origin_time;
