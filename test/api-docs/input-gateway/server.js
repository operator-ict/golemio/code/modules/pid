// Load reflection lib
require("@golemio/core/dist/shared/_global");

const path = require("path");
require("ts-node").register({ project: path.resolve(process.cwd(), "tsconfig.json") });
require("tsconfig-paths").register();

const http = require("http");
const express = require("express");
const { AMQPConnector } = require("@golemio/core/dist/input-gateway/connectors");

const app = express();
const server = http.createServer(app);

const bodyParser = require("body-parser");
require("body-parser-xml")(bodyParser);

const start = async () => {
    await AMQPConnector.connect();

    app.use(
        bodyParser.xml({
            xmlParseOptions: {
                strict: false,
                explicitArray: false,
                normalize: true,
                normalizeTags: true, // portman turns example into uppercase
            },
        })
    );

    const { vehiclePositionsRouter } = require("#ig/vehicle-positions/VehiclePositionsRouter");
    const { RopidGtfsRouter } = require("#ig/ropid-gtfs/RopidGtfsRouter");

    app.use("/vehiclepositions", vehiclePositionsRouter);
    app.use("/ropidgtfs", new RopidGtfsRouter().router);

    return new Promise((resolve) => {
        server.listen(3011, () => {
            resolve();
        });
    });
};

const stop = async () => {
    await AMQPConnector.disconnect();
    server.close();
};

module.exports = {
    start,
    stop,
};
