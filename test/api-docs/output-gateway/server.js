// Load reflection lib
require("@golemio/core/dist/shared/_global");

const path = require("path");
require("ts-node").register({ project: path.resolve(process.cwd(), "tsconfig.json") });
require("tsconfig-paths").register();

const fs = require("fs");
const http = require("http");
const express = require("express");
const { ContainerToken, OutputGatewayContainer } = require("@golemio/core/dist/output-gateway/ioc");

const app = express();
const server = http.createServer(app);
const postgresConnector = OutputGatewayContainer.resolve(ContainerToken.PostgresDatabase);
const redisConnector = OutputGatewayContainer.resolve(ContainerToken.RedisConnector);

const start = async () => {
    await postgresConnector.connect();
    await redisConnector.connect();

    const { routers } = require("#og");
    const { OgPublicContainer } = require("#og/public/ioc/Di");
    const { OgModuleToken } = require("#og/public/ioc/OgModuleToken");
    const {
        GTFS_DELAY_COMPUTATION_NAMESPACE_PREFIX,
        PUBLIC_CACHE_NAMESPACE_PREFIX,
        PUBLIC_STOP_TIME_CACHE_NAMESPACE_PREFIX,
    } = require("#sch/vehicle-positions/redis/const.ts");
    const { PUBLIC_DEPARTURE_NAMESPACE_PREFIX } = require("#sch/ropid-gtfs/redis/const.ts");

    for (const router of routers) {
        app.use(router.getPath(), router.getRouter());
    }

    const redisClient = redisConnector.getConnection();

    const bufferTrips = fs.readFileSync(path.resolve(__dirname, "../../../db/example/pb/trip_updates.pb"));
    const bufferPositions = fs.readFileSync(path.resolve(__dirname, "../../../db/example/pb/vehicle_positions.pb"));
    const bufferPidFeed = fs.readFileSync(path.resolve(__dirname, "../../../db/example/pb/pid_feed.pb"));
    const bufferAlerts = fs.readFileSync(path.resolve(__dirname, "../../../db/example/pb/alerts.pb"));

    await redisClient.hset("files:gtfsRt", "trip_updates.pb", bufferTrips.toString("binary"));
    await redisClient.hset("files:gtfsRt", "vehicle_positions.pb", bufferPositions.toString("binary"));
    await redisClient.hset("files:gtfsRt", "pid_feed.pb", bufferPidFeed.toString("binary"));
    await redisClient.hset("files:gtfsRt", "alerts.pb", bufferAlerts.toString("binary"));

    await redisClient.mset(
        "files:gtfsRt:vehicle_positions_timestamp",
        "1720625070",
        "files:gtfsRt:pid_feed_timestamp",
        "1720625070",
        "files:gtfsRt:trip_updates_timestamp",
        "1720625070",
        "files:gtfsRt:alerts_timestamp",
        "1720625070"
    );

    const publicVehiclePositionsMock = require("../../../db/example/redis/publicVehiclePositionsMock.json");
    const publicDelayCompObjectMock = require("../../../db/example/redis/publicDelayCompObjectMock.json");
    const publicStopTimesMock = require("../../../db/example/redis/publicStopTimesMock.json");
    const publicDepartureBoardsMock = require("../../../db/example/redis/publicDepartureBoardsMock.json");

    // Adjust data for current time
    for (const departure of publicDepartureBoardsMock) {
        departure.departure_datetime = new Date().toISOString();
        departure.arrival_datetime = new Date().toISOString();
    }

    const pipeline = redisClient.pipeline();
    const setName = `${PUBLIC_CACHE_NAMESPACE_PREFIX}:test`;

    for (const item of publicVehiclePositionsMock) {
        pipeline.geoadd(setName, item.lng, item.lat, item.vehicle_id);
        pipeline.rpush(`${setName}:trip-${item.gtfs_trip_id}`, item.vehicle_id);
        pipeline.set(`${setName}:vehicle-${item.vehicle_id}`, JSON.stringify(item));

        if (item.gtfs_trip_id === "137_2032_240103") {
            pipeline.set(
                `${GTFS_DELAY_COMPUTATION_NAMESPACE_PREFIX}:${item.gtfs_trip_id}`,
                JSON.stringify(publicDelayCompObjectMock)
            );

            pipeline.set(`${PUBLIC_STOP_TIME_CACHE_NAMESPACE_PREFIX}:${item.gtfs_trip_id}`, JSON.stringify(publicStopTimesMock));
        }
    }

    for (const item of publicDepartureBoardsMock) {
        pipeline.del(`${PUBLIC_DEPARTURE_NAMESPACE_PREFIX}:${item.stop_id}`);
        pipeline.zadd(
            `${PUBLIC_DEPARTURE_NAMESPACE_PREFIX}:${item.stop_id}`,
            new Date(item.departure_datetime).getTime() / 1000,
            JSON.stringify(item)
        );
    }

    await pipeline.exec();
    OgPublicContainer.resolve(OgModuleToken.PublicVehiclePositionsRepository).setCurrentSetName(setName);

    return new Promise((resolve) => {
        server.listen(3011, () => {
            resolve();
        });
    });
};

const stop = async () => {
    await postgresConnector.disconnect();
    await redisConnector.disconnect();
    server.close();
};

module.exports = {
    start,
    stop,
};
