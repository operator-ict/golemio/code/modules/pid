// Load reflection lib
import "@golemio/core/dist/shared/_global";

import { RefreshPrecomputedTablesTask } from "#ie/ropid-gtfs/workers/timetables/tasks/RefreshPrecomputedTablesTask";
import { VPContainer } from "#ie/vehicle-positions/ioc/Di";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { TripsRepository } from "#ie/vehicle-positions/workers/vehicle-positions/data-access/TripsRepository";
import { PUBLIC_DEPARTURE_NAMESPACE_PREFIX } from "#sch/ropid-gtfs/redis/const";
import { PUBLIC_CACHE_NAMESPACE_PREFIX } from "#sch/vehicle-positions/redis/const";
import { ILogger } from "@golemio/core/dist/helpers";
import { IRedisConnector } from "@golemio/core/dist/helpers/data-access/redis/IRedisConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import IPostgresConnector from "@golemio/core/dist/integration-engine/connectors/interfaces/IPostgresConnector";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc";

const PostgresConnector = VPContainer.resolve<IPostgresConnector>(ContainerToken.PostgresConnector);
const RedisConnector = VPContainer.resolve<IRedisConnector>(ContainerToken.RedisConnector);
const logger = VPContainer.resolve<ILogger>(CoreToken.Logger);

const deleteCacheKeysWithPrefix = (keyPrefix: string) => {
    const connection = RedisConnector.getConnection();
    return connection.eval(
        // eslint-disable-next-line max-len
        "local keys = redis.call('keys', ARGV[1]) \n for i=1,#keys,5000 do \n redis.call('del', unpack(keys, i, math.min(i+4999, #keys))) \n end \n return #keys",
        0,
        `${keyPrefix}:*`
    );
};

(async () => {
    await PostgresConnector.connect();
    await RedisConnector.connect();

    const task = new RefreshPrecomputedTablesTask("test");
    await task.consume(null);

    const noOfVPKeys = await deleteCacheKeysWithPrefix(PUBLIC_CACHE_NAMESPACE_PREFIX);
    logger.info(`Deleted public API VP cache keys: ${noOfVPKeys}`);

    const noOfDepartureKeys = await deleteCacheKeysWithPrefix(PUBLIC_DEPARTURE_NAMESPACE_PREFIX);
    logger.info(`Deleted public API GTFS departure cache keys: ${noOfDepartureKeys}`);

    const tripRepository = VPContainer.resolve<TripsRepository>(VPContainerToken.TripRepository);
    await tripRepository.refreshGtfsTripData();

    await PostgresConnector.disconnect();
    await RedisConnector.disconnect();
})();
