export const gtfsTimestampCETToCESTTestData = [
    {
        gtfsTime: "00:01:02",
        gtfsDate: "2023-03-25",
        correctTimestamp: "2023-03-24T23:01:02.000Z",
    },
    {
        gtfsTime: "10:01:02",
        gtfsDate: "2023-03-25",
        correctTimestamp: "2023-03-25T09:01:02.000Z",
    },
    {
        gtfsTime: "00:01:02",
        gtfsDate: "2023-03-26",
        correctTimestamp: "2023-03-25T22:01:02.000Z",
    },
    {
        gtfsTime: "24:01:02",
        gtfsDate: "2023-03-25",
        correctTimestamp: "2023-03-25T23:01:02.000Z",
    },
    {
        gtfsTime: "01:01:02",
        gtfsDate: "2023-03-26",
        correctTimestamp: "2023-03-25T23:01:02.000Z",
    },
    {
        gtfsTime: "25:01:02",
        gtfsDate: "2023-03-25",
        correctTimestamp: "2023-03-26T00:01:02.000Z",
    },
    {
        gtfsTime: "02:01:02",
        gtfsDate: "2023-03-26",
        correctTimestamp: "2023-03-26T00:01:02.000Z",
    },
    {
        gtfsTime: "26:01:02",
        gtfsDate: "2023-03-25",
        correctTimestamp: "2023-03-26T01:01:02.000Z",
    },
    {
        gtfsTime: "03:01:02",
        gtfsDate: "2023-03-26",
        correctTimestamp: "2023-03-26T01:01:02.000Z",
    },
    {
        gtfsTime: "27:01:02",
        gtfsDate: "2023-03-25",
        correctTimestamp: "2023-03-26T02:01:02.000Z",
    },
    {
        gtfsTime: "04:01:02",
        gtfsDate: "2023-03-26",
        correctTimestamp: "2023-03-26T02:01:02.000Z",
    },
    {
        gtfsTime: "28:01:02",
        gtfsDate: "2023-03-25",
        correctTimestamp: "2023-03-26T03:01:02.000Z",
    },
    {
        gtfsTime: "05:01:02",
        gtfsDate: "2023-03-26",
        correctTimestamp: "2023-03-26T03:01:02.000Z",
    },
    {
        gtfsTime: "29:01:02",
        gtfsDate: "2023-03-25",
        correctTimestamp: "2023-03-26T04:01:02.000Z",
    },
    {
        gtfsTime: "06:01:02",
        gtfsDate: "2023-03-26",
        correctTimestamp: "2023-03-26T04:01:02.000Z",
    },
    {
        gtfsTime: "10:01:02",
        gtfsDate: "2023-03-26",
        correctTimestamp: "2023-03-26T08:01:02.000Z",
    },
    {
        gtfsTime: "00:01:02",
        gtfsDate: "2023-03-27",
        correctTimestamp: "2023-03-26T22:01:02.000Z",
    },
    {
        gtfsTime: "10:01:02",
        gtfsDate: "2023-03-27",
        correctTimestamp: "2023-03-27T08:01:02.000Z",
    },
];

export const gtfsTimestampCESTToCETTestData = [
    {
        gtfsTime: "00:01:02",
        gtfsDate: "2023-10-28",
        correctTimestamp: "2023-10-27T22:01:02.000Z",
    },
    {
        gtfsTime: "10:01:02",
        gtfsDate: "2023-10-28",
        correctTimestamp: "2023-10-28T08:01:02.000Z",
    },
    {
        gtfsTime: "24:01:02",
        gtfsDate: "2023-10-28",
        correctTimestamp: "2023-10-28T22:01:02.000Z",
    },
    {
        gtfsTime: "00:01:02",
        gtfsDate: "2023-10-29",
        correctTimestamp: "2023-10-28T23:01:02.000Z",
    },
    {
        gtfsTime: "25:01:02",
        gtfsDate: "2023-10-28",
        correctTimestamp: "2023-10-28T23:01:02.000Z",
    },
    {
        gtfsTime: "01:01:02",
        gtfsDate: "2023-10-29",
        correctTimestamp: "2023-10-29T00:01:02.000Z",
    },
    {
        gtfsTime: "26:01:02",
        gtfsDate: "2023-10-28",
        correctTimestamp: "2023-10-29T00:01:02.000Z",
    },
    {
        gtfsTime: "02:01:02",
        gtfsDate: "2023-10-29",
        correctTimestamp: "2023-10-29T01:01:02.000Z",
    },
    {
        gtfsTime: "27:01:02",
        gtfsDate: "2023-10-28",
        correctTimestamp: "2023-10-29T01:01:02.000Z",
    },
    {
        gtfsTime: "03:01:02",
        gtfsDate: "2023-10-29",
        correctTimestamp: "2023-10-29T02:01:02.000Z",
    },
    {
        gtfsTime: "28:01:02",
        gtfsDate: "2023-10-28",
        correctTimestamp: "2023-10-29T02:01:02.000Z",
    },
    {
        gtfsTime: "04:01:02",
        gtfsDate: "2023-10-29",
        correctTimestamp: "2023-10-29T03:01:02.000Z",
    },
    {
        gtfsTime: "29:01:02",
        gtfsDate: "2023-10-28",
        correctTimestamp: "2023-10-29T03:01:02.000Z",
    },
    {
        gtfsTime: "10:01:02",
        gtfsDate: "2023-10-29",
        correctTimestamp: "2023-10-29T09:01:02.000Z",
    },
    {
        gtfsTime: "00:01:02",
        gtfsDate: "2023-10-30",
        correctTimestamp: "2023-10-29T23:01:02.000Z",
    },
    {
        gtfsTime: "10:01:02",
        gtfsDate: "2023-10-30",
        correctTimestamp: "2023-10-30T09:01:02.000Z",
    },
];
