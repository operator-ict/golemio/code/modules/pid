import { PG_SCHEMA } from "#sch/const";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { Sequelize } from "@golemio/core/dist/shared/sequelize";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";
import { gtfsTimestampCESTToCETTestData, gtfsTimestampCETToCESTTestData } from "./data/gtfs_timestamp-dst-change";

chai.use(chaiAsPromised);

describe(`${PG_SCHEMA}.gtfs_timestamp`, () => {
    let sandbox: SinonSandbox;
    let connection: Sequelize;

    before(async () => {
        sandbox = sinon.createSandbox();
        await PostgresConnector.connect();
        connection = PostgresConnector.getConnection();
    });

    afterEach(() => {
        sandbox?.restore();
    });

    async function shouldReturnCorrectTimestamp(gtfsTime: string, gtfsDate: string, correctTimestamp: string) {
        const res = await connection.query(`SELECT ${PG_SCHEMA}.gtfs_timestamp('${gtfsTime}', '${gtfsDate}');`, {
            raw: true,
            plain: true,
        });

        expect(res).to.be.an("object").with.property("gtfs_timestamp");
        expect(res!.gtfs_timestamp).to.be.an.instanceOf(Date);
        expect((res!.gtfs_timestamp as Date).toISOString()).to.equal(correctTimestamp);
    }

    it("should return correct timestamp for time HH:MM:SS and date YYYYMMDD", async () =>
        shouldReturnCorrectTimestamp("07:08:09", "20240314", "2024-03-14T06:08:09.000Z"));

    it("should return correct timestamp for time H:MM:SS and date YYYYMMDD", async () =>
        shouldReturnCorrectTimestamp("7:08:09", "20240314", "2024-03-14T06:08:09.000Z"));

    it("should return correct timestamp for time HH:MM:SS and date YYYY-MM-DD", async () =>
        shouldReturnCorrectTimestamp("07:08:09", "2024-03-14", "2024-03-14T06:08:09.000Z"));

    it("should return correct timestamp for time H:MM:SS and date YYYY-MM-DD", async () =>
        shouldReturnCorrectTimestamp("7:08:09", "2024-03-14", "2024-03-14T06:08:09.000Z"));

    it("should return correct timestamp for time > 24:00:00", async () =>
        shouldReturnCorrectTimestamp("27:08:09", "2024-03-14", "2024-03-15T02:08:09.000Z"));

    const dstChangeTests = [
        {
            data: gtfsTimestampCETToCESTTestData,
            sessionTimezone: "UTC",
            getTitle: (sessionTimezone: string) => `Around DST change (CET -> CEST) using session timezone ${sessionTimezone}`,
        },
        {
            data: gtfsTimestampCESTToCETTestData,
            sessionTimezone: "UTC",
            getTitle: (sessionTimezone: string) => `Around DST change (CEST -> CET) using session timezone ${sessionTimezone}`,
        },
        {
            data: gtfsTimestampCETToCESTTestData,
            sessionTimezone: "Europe/Prague",
            getTitle: (sessionTimezone: string) => `Around DST change (CET -> CEST) using session timezone ${sessionTimezone}`,
        },
        {
            data: gtfsTimestampCESTToCETTestData,
            sessionTimezone: "Europe/Prague",
            getTitle: (sessionTimezone: string) => `Around DST change (CEST -> CET) using session timezone ${sessionTimezone}`,
        },
    ];

    dstChangeTests.forEach(({ data, sessionTimezone, getTitle }) => {
        describe(getTitle(sessionTimezone), () => {
            before(async () => {
                await connection.query(`SET TIME ZONE '${sessionTimezone}';`, {
                    raw: true,
                    plain: true,
                });
            });

            data.forEach(({ gtfsTime, gtfsDate, correctTimestamp }) => {
                it(`should return correct timestamp for ${gtfsDate} ${gtfsTime}`, async () => {
                    const res = await connection.query(`SHOW TIME ZONE;`, {
                        raw: true,
                        plain: true,
                    });
                    expect(res).to.be.an("object").with.property("TimeZone");
                    expect(res!.TimeZone).to.be.a("string").that.equals(sessionTimezone);

                    await shouldReturnCorrectTimestamp(gtfsTime, gtfsDate, correctTimestamp);
                });
            });
        });
    });
});
