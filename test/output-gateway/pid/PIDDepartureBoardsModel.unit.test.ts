import { PIDDepartureBoardsModel } from "#og/pid";
import { expect } from "chai";
import sinon, { SinonSandbox, SinonStub } from "sinon";
import { gtfsRunSchedule10_10, gtfsRunSchedule1_3 } from "../ropid-gtfs/fixture/GtfsRunScheduleFixture";

describe("PIDDepartureBoardsModel (unit)", () => {
    let sandbox: SinonSandbox;
    let model: PIDDepartureBoardsModel;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        model = new PIDDepartureBoardsModel();
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe("retrieveAndReturnRunTrips", () => {
        let errorStub: SinonStub;
        let getScheduleMock: SinonStub;

        beforeEach(() => {
            errorStub = sandbox.stub(model["log"], "error");
            getScheduleMock = sandbox.stub(model["runTripsRedisRepository"], "getMultipleSchedule");
        });

        it("should return empty schedule and trip ids when run tuples are empty", async () => {
            const runTuples = new Set([]);
            getScheduleMock.resolves([]);
            const { runSchedule, runTripIds } = await model["retrieveAndReturnRunTrips"](runTuples);

            expect(runSchedule.size).to.equal(0);
            expect(runTripIds.size).to.equal(0);

            expect(getScheduleMock.calledOnce).to.be.true;
            expect(errorStub.called).to.be.false;
        });

        it("should return empty run schedule and run trip ids when nulls are returned from cache", async () => {
            const runTuples = new Set(["1_1", "1_2"]);
            getScheduleMock.resolves([null, null]);
            const { runSchedule, runTripIds } = await model["retrieveAndReturnRunTrips"](runTuples);

            expect(runSchedule.size).to.equal(0);
            expect(runTripIds.size).to.equal(0);

            expect(getScheduleMock.calledOnce).to.be.true;
            expect(errorStub.called).to.be.false;
        });

        it("should return run schedule and run trip ids when cache returns valid data", async () => {
            const runTuples = new Set(["1_3", "10_10"]);
            getScheduleMock.resolves([structuredClone(gtfsRunSchedule1_3), structuredClone(gtfsRunSchedule10_10)]);
            const { runSchedule, runTripIds } = await model["retrieveAndReturnRunTrips"](runTuples);

            expect(runSchedule.size).to.equal(2);
            expect(runSchedule.get("1_3")).to.deep.equal(gtfsRunSchedule1_3.schedule);
            expect(runSchedule.get("10_10")).to.deep.equal(gtfsRunSchedule10_10.schedule);

            expect(runTripIds.size).to.equal(4);
            expect(Array.from(runTripIds)).to.deep.equal([
                "1_10344_241125",
                "1_10345_241125",
                "10_5250_250106",
                "10_13431_250106",
            ]);

            expect(getScheduleMock.calledOnce).to.be.true;
            expect(errorStub.called).to.be.false;
        });

        it("should log error and return empty run schedule and run trip ids when cache throws error", async () => {
            const runTuples = new Set(["1_3", "10_10"]);
            getScheduleMock.rejects(new Error("Cache error"));
            const { runSchedule, runTripIds } = await model["retrieveAndReturnRunTrips"](runTuples);

            expect(runSchedule.size).to.equal(0);
            expect(runTripIds.size).to.equal(0);

            expect(getScheduleMock.calledOnce).to.be.true;
            expect(errorStub.calledOnce).to.be.true;
            expect(errorStub.getCall(0).args[1]).to.equal(
                // eslint-disable-next-line max-len
                "Cannot retrieve run schedule. The API consumer will receive departures with missing vehicles even if they requested to skip them."
            );
        });
    });

    describe("retrieveAndReturnUntrackedTrips", () => {
        let errorStub: SinonStub;
        let getUntrackedMock: SinonStub;

        beforeEach(() => {
            errorStub = sandbox.stub(model["log"], "error");
            getUntrackedMock = sandbox.stub(model["positionsRedisRepository"], "getTripsWithUntrackedVehicles");
        });

        it("should return untracked trips when cache returns valid data", async () => {
            const untrackedTrips = new Set(["1_10344_241125, 1_10345_241125"]);
            getUntrackedMock.resolves(untrackedTrips);
            const result = await model["retrieveAndReturnUntrackedTrips"](untrackedTrips);

            expect(result).to.deep.equal(untrackedTrips);

            expect(getUntrackedMock.calledOnce).to.be.true;
            expect(errorStub.called).to.be.false;
        });

        it("should log error and return empty untracked trips when cache throws error", async () => {
            const untrackedTrips = new Set(["1_10344_241125, 1_10345_241125"]);
            getUntrackedMock.rejects(new Error("Cache error"));
            const result = await model["retrieveAndReturnUntrackedTrips"](untrackedTrips);

            expect(result.size).to.equal(0);

            expect(getUntrackedMock.calledOnce).to.be.true;
            expect(errorStub.calledOnce).to.be.true;
            expect(errorStub.getCall(0).args[1]).to.equal(
                // eslint-disable-next-line max-len
                "Cannot retrieve untracked trips. The API consumer will receive departures with missing vehicles even if they requested to skip them."
            );
        });
    });
});
