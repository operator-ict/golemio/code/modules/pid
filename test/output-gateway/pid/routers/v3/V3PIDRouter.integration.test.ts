import { v3PidRouter } from "#og/pid";
import { InfotextDisplayType } from "#og/pid/domain/InfotextDisplayTypeEnum";
import { InfotextPriority } from "#og/pid/domain/InfotextPriorityEnum";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import express, { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { HTTPErrorHandler } from "@golemio/core/dist/shared/golemio-errors";
import moment from "@golemio/core/dist/shared/moment-timezone";
import { expect } from "chai";
import sinon, { SinonFakeTimers, SinonSandbox } from "sinon";
import request from "supertest";

describe("V3PIDRouter (integration)", () => {
    const app = express();
    const todayYMD = moment().format("YYYY-MM-DD");
    const cisIdNarodniTrida = "62887"; // U539Z1P, U539Z2P

    let sandbox: SinonSandbox | null = null;
    let clock: SinonFakeTimers | null = null;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox?.restore();
        clock?.restore();
    });

    before(async () => {
        app.use(v3PidRouter.getPath(), v3PidRouter.getRouter());
        app.use((err: any, _req: Request, res: Response, _next: NextFunction) => {
            const error = HTTPErrorHandler.handle(err, log);
            if (error) {
                res.setHeader("Content-Type", "application/json; charset=utf-8");
                res.status(error.error_status || 500).send(error);
            }
        });
    });

    describe("/v3/pid/transferboards", () => {
        it("should return train transfers", (done) => {
            clock = sandbox!.useFakeTimers({
                now: moment.tz(`${todayYMD}T14:35:00`, "Europe/Prague").valueOf(),
                shouldAdvanceTime: true,
            });

            request(app)
                .get(`/v3/pid/transferboards?cisId=5457076`)
                .end((err, res) => {
                    expect(res.statusCode).to.be.equal(200);
                    expect(res.body).to.have.keys("departures", "infotexts");

                    const departures = res.body.departures;
                    const infotexts = res.body.infotexts;

                    expect(departures).to.have.length(1);
                    // Os 9948
                    expect(departures[0]).to.deep.equal({
                        departure_timestamp: {
                            minutes: "26",
                        },
                        route: {
                            short_name: "S7",
                            type: 2,
                        },
                        stop: {
                            platform_code: null,
                        },
                        trip: {
                            headsign: "Řevnice",
                            id: "1307_9948_210406",
                        },
                    });

                    expect(infotexts).to.have.length(1);
                    expect(infotexts[0]).to.deep.equal({
                        display_type: "inline",
                        text: "Nejede to.",
                        text_en: null,
                    });

                    done();
                });
        });

        it("should skip current trip", (done) => {
            clock = sandbox!.useFakeTimers({
                now: moment.tz(`${todayYMD}T14:35:00`, "Europe/Prague").valueOf(),
                shouldAdvanceTime: true,
            });

            request(app)
                .get(`/v3/pid/transferboards?cisId=5457076&tripNumber=9948`)
                .end((err, res) => {
                    expect(res.statusCode).to.be.equal(200);
                    expect(res.body).to.have.keys("departures", "infotexts");

                    const departures = res.body.departures;
                    const infotexts = res.body.infotexts;

                    expect(departures).to.have.length(0);
                    expect(infotexts).to.have.length(1);
                    expect(infotexts[0]).to.deep.equal({
                        display_type: "inline",
                        text: "Nejede to.",
                        text_en: null,
                    });

                    done();
                });
        });

        it("should not skip current trip (shoud not match 48 to 9948)", (done) => {
            clock = sandbox!.useFakeTimers({
                now: moment.tz(`${todayYMD}T14:35:00`, "Europe/Prague").valueOf(),
                shouldAdvanceTime: true,
            });

            request(app)
                .get(`/v3/pid/transferboards?cisId=5457076&tripNumber=48`)
                .end((err, res) => {
                    expect(res.statusCode).to.be.equal(200);
                    expect(res.body).to.have.keys("departures", "infotexts");

                    const departures = res.body.departures;
                    const infotexts = res.body.infotexts;

                    expect(departures).to.have.length(1);
                    // Os 9948
                    expect(departures[0]).to.deep.equal({
                        departure_timestamp: {
                            minutes: "26",
                        },
                        route: {
                            short_name: "S7",
                            type: 2,
                        },
                        stop: {
                            platform_code: null,
                        },
                        trip: {
                            headsign: "Řevnice",
                            id: "1307_9948_210406",
                        },
                    });

                    expect(infotexts).to.have.length(1);
                    expect(infotexts[0]).to.deep.equal({
                        display_type: "inline",
                        text: "Nejede to.",
                        text_en: null,
                    });

                    done();
                });
        });

        it("routeHeadingOnceNoGapFill - station Narodni trida 7am", (done) => {
            clock = sandbox!.useFakeTimers({
                now: moment.tz(`${todayYMD}T06:59:00`, "Europe/Prague").valueOf(),
                shouldAdvanceTime: true,
            });

            request(app)
                .get(`/v3/pid/transferboards?cisId=${cisIdNarodniTrida}&tripNumber=111`)
                .end((err: any, res: any) => {
                    expect(res.body.departures.length).to.be.greaterThan(6);
                    expect(res.body.departures[0].trip.id).to.be.equal("2_1502_220516"); //Sídliště Petřiny
                    expect(res.body.departures[1].trip.id).to.be.equal("22_7178_220509"); //Bílá Hora
                    expect(res.body.departures[2].trip.id).to.be.equal("9_4744_220502"); //Sídliště Řepy
                    expect(res.body.departures[3].trip.id).to.be.equal("18_3621_220516"); //Nádraží Podbaba
                    expect(res.body.departures[4].trip.id).to.be.equal("22_7981_220509"); //Vypich
                    expect(res.body.departures[5].trip.id).to.be.equal("9_4755_220502"); // Fill - Sídliště Řepy
                    //9_7027_220502   Sídliště Řepy     skipped same line
                    //18_3632_220516  Nádraží Podbaba   skipped same line
                    //9_4832_220502   Vozovna Motol     unique direction of line but 9 was removed before so also skipped
                    done();
                });
        });

        it("timeFrom - return only 2 transfer departures in the future", (done) => {
            clock = sandbox!.useFakeTimers({
                now: moment.tz(`${todayYMD}T06:59:00`, "Europe/Prague").valueOf(),
                shouldAdvanceTime: true,
            });

            request(app)
                .get(`/v3/pid/transferboards?cisId=${cisIdNarodniTrida}&tripNumber=111&timeFrom=${todayYMD}T07:57:00`)
                .end((err: any, res: any) => {
                    expect(res.body.departures.length).to.equal(2);
                    expect(res.body.departures[0].trip.id).to.be.equal("22_8007_220502");
                    expect(res.body.departures[1].trip.id).to.be.equal("2_1534_220516");
                    done();
                });
        });

        it("should skip departure of trip that is canceled", (done) => {
            clock = sandbox!.useFakeTimers({
                now: moment.tz(`${todayYMD}T11:35:00`, "Europe/Prague").valueOf(),
                shouldAdvanceTime: true,
            });

            request(app)
                .get(`/v3/pid/transferboards?cisId=62886&tripNumber=111`)
                .end((err: any, res: any) => {
                    expect(res.body.departures).to.be.an("array").and.to.have.length(3);
                    expect(res.body.departures[0].trip.id).to.be.equal("152_37_201230");
                    // "152_60_201230" skipped (is cancelled)
                    expect(res.body.departures[1].trip.id).to.be.equal("177_121_210201");
                    expect(res.body.departures[2].trip.id).to.be.equal("152_61_201230");
                    done();
                });
        });
    });

    describe("/v3/pid/infotexts", () => {
        it("should return active infotexts", (done) => {
            clock = sandbox!.useFakeTimers(new Date("2024-02-30T00:00:00.000Z").getTime());

            request(app)
                .get("/v3/pid/infotexts")
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(200, (_err, res) => {
                    /* eslint-disable max-len */
                    expect(res.body).to.deep.equal([
                        {
                            id: "40507cee-6468-488e-ba78-e36d9c20e67c",
                            priority: InfotextPriority.High,
                            display_type: InfotextDisplayType.Inline,
                            text: "Provoz tramvají směrem z centra na Starý Hloubětín z důvodu údržby přerušen do neděle 18:00. Zaveden autobus X-8.",
                            text_en:
                                "No tram service in direction to Starý Hloubětín until Sunday 18:00 due to track maintenance. Replacement bus X-8 instated.",
                            related_stops: [
                                {
                                    id: "U1131Z1P",
                                    name: "Pod Chodovem",
                                    platform_code: "A",
                                },
                            ],
                            valid_from: "2024-02-07T13:11:00+01:00",
                            valid_to: null,
                        },
                        {
                            id: "cdcc2d15-2cdc-4205-b82a-5cc819a79738",
                            priority: InfotextPriority.High,
                            display_type: InfotextDisplayType.Inline,
                            text: "Nejede to.",
                            text_en: null,
                            related_stops: [
                                {
                                    id: "U142Z301",
                                    name: "Praha hl.n.",
                                    platform_code: null,
                                },
                            ],
                            valid_from: "2023-03-27T00:00:00+02:00",
                            valid_to: null,
                        },
                        {
                            id: "59a0b461-df44-42a6-940f-2abf0fd4e980",
                            priority: InfotextPriority.Low,
                            display_type: InfotextDisplayType.General,
                            text: "Do pondělí 12.6. bude uzavřen tento vstup do metra. Vstup od zastávky tramvaje v ulici Plynární zůstává v provozu.",
                            text_en:
                                "This entrance to the metro will be closed until Monday 12 June. The entrance from the tram stop in Plynární Street will remain open.",
                            related_stops: [
                                {
                                    id: "U52Z4P",
                                    name: "Chodov",
                                    platform_code: "D",
                                },
                                {
                                    id: "U1341Z1P",
                                    name: "Knovízská",
                                    platform_code: "A",
                                },
                            ],
                            valid_from: "2024-02-29T23:00:00+01:00",
                            valid_to: "2024-06-12T00:00:00+02:00",
                        },
                    ]);
                    /* eslint-enable max-len */

                    done();
                });
        });
    });
});
