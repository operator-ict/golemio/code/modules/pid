import { v2PidRouter } from "#og/pid";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import express, { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { HTTPErrorHandler, IGolemioError } from "@golemio/core/dist/shared/golemio-errors";
import moment from "@golemio/core/dist/shared/moment-timezone";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon from "sinon";
import { DATA_RETENTION_IN_MINUTES, GTFS_CALENDAR_LIMIT_IN_MINUTES } from "src/const";
import request from "supertest";

chai.use(chaiAsPromised);

describe("V2PIDRouter", () => {
    // Create clean express instance
    let app: any = null;
    // Basic configuration: create a sinon sandbox for testing
    let sandbox: any = null;
    const id = "U118Z102P";
    const now = new Date();
    const todayYMD = moment(now).tz("Europe/Prague").format("YYYY-MM-DD");
    const todayTzOffset = moment(now).tz("Europe/Prague").format("Z");
    let clock: any = null;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox && sandbox.restore();
    });

    before(() => {
        clock = sinon.useFakeTimers({
            now: moment(`${todayYMD}T07:25:00`).tz("Europe/Prague").valueOf(),
            shouldAdvanceTime: true,
        });
        app = express();
        // Mount the tested router to the express instance
        app.use(v2PidRouter.getPath(), v2PidRouter.getRouter());
        app.use((err: any, req: Request, res: Response, next: NextFunction) => {
            const errObject: IGolemioError = HTTPErrorHandler.handle(err, log);
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    after(() => {
        clock.uninstall();
    });

    it("should respond with list of stop times of specific stop /v2/pid/departureboards?ids", (done) => {
        request(app)
            .get(`/v2/pid/departureboards?ids[]=${id}`)
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(200);
                expect(res.body.departures).to.be.an("array");
                expect(res.body.stops).to.be.an("array");
                expect(res.body.infotexts).to.be.an("array");
                done();
            });
    });

    it("should respond with 404 to non-existant stop /v2/pid/departureboards?ids", (done) => {
        request(app)
            .get("/v2/pid/departureboards?ids[]=kovfefe")
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(404);
                done();
            });
    });

    it("should respond with 404 to non-existant ASW stop /v2/departureboards?aswIds", (done) => {
        request(app)
            .get("/v2/pid/departureboards?aswIds[]=85_12389")
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(404);
                done();
            });
    });

    it("should respond with 400 to unspecified ids (one of ids,cisIds,aswIds must be set)", (done) => {
        request(app)
            .get("/v2/pid/departureboards")
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(400);
                done();
            });
    });

    it("should respond with 400 (invalid positive minutesAfter)", (done) => {
        request(app)
            .get(`/v2/pid/departureboards?ids[]=${id}&minutesAfter=${GTFS_CALENDAR_LIMIT_IN_MINUTES + 1}`)
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(400);
                expect(res.body.error_info).to.include("minutesAfter");
                done();
            });
    });

    it("should respond with 400 (invalid negative minutesAfter)", (done) => {
        request(app)
            .get(
                `/v2/pid/departureboards?ids[]=${id}&minutesAfter=-${
                    GTFS_CALENDAR_LIMIT_IN_MINUTES + DATA_RETENTION_IN_MINUTES + 1
                }`
            )
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(400);
                expect(res.body.error_info).to.include("minutesAfter");
                done();
            });
    });

    // FIXME test data inconsistencies
    it.skip("should respond with 22 departures", (done) => {
        request(app)
            .get("/v2/pid/departureboards?ids[]=U953Z102P&minutesBefore=1000000&minutesAfter=1000000&mode=departures")
            .end((err: any, res: any) => {
                expect(res.body.departures).to.have.lengthOf(22);
                done();
            });
    });

    // FIXME test data inconsistencies
    it.skip("should respond with 14 departures for first stop when mode arrivals", (done) => {
        request(app)
            .get("/v2/pid/departureboards?ids[]=U953Z102P&minutesBefore=1000000&minutesAfter=1000000&mode=arrivals")
            .end((err: any, res: any) => {
                expect(res.body.departures).to.have.lengthOf(14);
                done();
            });
    });

    // TODO implement
    it.skip("should respond with 14 departures for stop with dropoff_type 1 when mode arrivals", (done) => {
        request(app)
            .get("/v2/pid/departureboards?ids[]=U921Z102P&minutesBefore=1000000&minutesAfter=1000000&mode=arrivals")
            .end((err: any, res: any) => {
                expect(res.body.departures).to.have.lengthOf(14);
                done();
            });
    });

    // TODO implement
    it.skip("should respond with 22 departures for stop with dropoff_type 1 when mode departures", (done) => {
        request(app)
            .get("/v2/pid/departureboards?ids[]=U921Z102P&minutesBefore=1000000&minutesAfter=1000000&mode=departures")
            .end((err: any, res: any) => {
                expect(res.body.departures).to.have.lengthOf(22);
                done();
            });
    });

    // TODO implement
    it.skip("should respond with 14 departures for stop with pickup_type 1 when mode departures", (done) => {
        request(app)
            .get("/v2/pid/departureboards?ids[]=U118Z102P&minutesBefore=1000000&minutesAfter=1000000&mode=departures")
            .end((err: any, res: any) => {
                expect(res.body.departures).to.have.lengthOf(14);
                done();
            });
    });

    // TODO implement
    it.skip("should respond with 22 departures for stop with pickup_type 1 when mode arrivals", (done) => {
        request(app)
            .get("/v2/pid/departureboards?ids[]=U118Z102P&minutesBefore=1000000&minutesAfter=1000000&mode=arrivals")
            .end((err: any, res: any) => {
                expect(res.body.departures).to.have.lengthOf(22);
                done();
            });
    });

    // TODO implement
    it.skip("should respond with ZERO departures for LAST stop of trip when mode departures", (done) => {
        request(app)
            .get("/v2/pid/departureboards?ids[]=U306Z102P&minutesBefore=1000000&minutesAfter=1000000&mode=departures")
            .end((err: any, res: any) => {
                expect(res.body.departures).to.have.lengthOf(0);
                done();
            });
    });

    // TODO implement
    it.skip("should respond with 22 departures for LAST stop of trip when mode arrivals", (done) => {
        request(app)
            .get("/v2/pid/departureboards?ids[]=U306Z102P&minutesBefore=1000000&minutesAfter=1000000&mode=arrivals")
            .end((err: any, res: any) => {
                expect(res.body.departures).to.have.lengthOf(22);
                done();
            });
    });

    it("should respond with 1 departure with timeFrom used", (done) => {
        const timeFrom = moment.tz(`${todayYMD}T07:25:00`, "Europe/Prague").toISOString();
        request(app)
            .get(`/v2/pid/departureboards?ids[]=U713Z102P&minutesBefore=10&minutesAfter=30&timeFrom=${timeFrom}`)
            .end((err: any, res: any) => {
                expect(res.body.departures).to.have.lengthOf(1);
                done();
            });
    });

    it("should respond with no departures with timeFrom used outside of scheduled departures", (done) => {
        const timeFrom = moment.tz(`${todayYMD}T08:26:00`, "Europe/Prague").toISOString();
        request(app)
            .get(`/v2/pid/departureboards?ids[]=U713Z102P&minutesBefore=10&minutesAfter=30&timeFrom=${timeFrom}`)
            .end((err: any, res: any) => {
                expect(res.body.departures).to.have.lengthOf(0);
                done();
            });
    });

    it("should respond with 1 departure from first stop with null arrival timestamps (departures mode - default)", (done) => {
        const timeFrom = moment.tz(`${todayYMD}T07:25:00`, "Europe/Prague").toISOString();
        request(app)
            .get(`/v2/pid/departureboards?ids[]=U953Z102P&minutesBefore=10&minutesAfter=30&timeFrom=${timeFrom}`)
            .end((err: any, res: any) => {
                expect(res.body.departures).to.have.lengthOf(1);
                expect(res.body.departures[0].arrival_timestamp.predicted).to.be.equal(null);
                expect(res.body.departures[0].arrival_timestamp.scheduled).to.be.equal(null);
                expect(res.body.departures[0].departure_timestamp.predicted).to.not.be.equal(null);
                expect(res.body.departures[0].departure_timestamp.scheduled).to.not.be.equal(null);
                done();
            });
    });

    it("should respond with 1 departure from first stop with null arrival timestamps (mixed mode)", (done) => {
        const timeFrom = moment.tz(`${todayYMD}T07:25:00`, "Europe/Prague").toISOString();
        request(app)
            .get(`/v2/pid/departureboards?ids[]=U953Z102P&minutesBefore=10&minutesAfter=30&timeFrom=${timeFrom}&mode=mixed`)
            .end((err: any, res: any) => {
                expect(res.body.departures).to.have.lengthOf(1);
                expect(res.body.departures[0].arrival_timestamp.predicted).to.be.equal(null);
                expect(res.body.departures[0].arrival_timestamp.scheduled).to.be.equal(null);
                expect(res.body.departures[0].departure_timestamp.predicted).to.not.be.equal(null);
                expect(res.body.departures[0].departure_timestamp.scheduled).to.not.be.equal(null);
                done();
            });
    });

    it("should respond with 1 departure from last stop with null departure timestamps (mixed mode)", (done) => {
        const timeFrom = moment.tz(`${todayYMD}T07:25:00`, "Europe/Prague").toISOString();
        request(app)
            .get(`/v2/pid/departureboards?ids[]=U306Z102P&minutesBefore=10&minutesAfter=30&timeFrom=${timeFrom}&mode=mixed`)
            .end((err: any, res: any) => {
                expect(res.body.departures).to.have.lengthOf(1);
                expect(res.body.departures[0].arrival_timestamp.predicted).to.not.be.equal(null);
                expect(res.body.departures[0].arrival_timestamp.scheduled).to.not.be.equal(null);
                expect(res.body.departures[0].departure_timestamp.predicted).to.be.equal(null);
                expect(res.body.departures[0].departure_timestamp.scheduled).to.be.equal(null);
                done();
            });
    });

    it("should respond with 1 departure from last stop with null departure timestamps (arrivals mode)", (done) => {
        const timeFrom = moment.tz(`${todayYMD}T07:25:00`, "Europe/Prague").toISOString();
        request(app)
            .get(
                // eslint-disable-next-line max-len
                `/v2/pid/departureboards?ids[]=U306Z102P&minutesBefore=10&minutesAfter=30&timeFrom=${timeFrom}&mode=arrivals`
            )
            .end((err: any, res: any) => {
                expect(res.body.departures).to.have.lengthOf(1);
                expect(res.body.departures[0].arrival_timestamp.predicted).to.not.be.equal(null);
                expect(res.body.departures[0].arrival_timestamp.scheduled).to.not.be.equal(null);
                expect(res.body.departures[0].departure_timestamp.predicted).to.be.equal(null);
                expect(res.body.departures[0].departure_timestamp.scheduled).to.be.equal(null);
                done();
            });
    });

    it("should return 400 due to invalid preferredTimezone", (done) => {
        request(app)
            .get(`/v2/pid/departureboards?ids[]=U713Z102P&preferredTimezone=EuropePraha`)
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(400);
                done();
            });
    });

    it("should respond with departure with preferredTimezone in Europe/Prague", (done) => {
        const timeFrom = moment.tz(`${todayYMD}T07:25:00`, "Europe/Prague").toISOString();
        request(app)
            .get(
                // eslint-disable-next-line max-len
                `/v2/pid/departureboards?ids[]=U713Z102P&minutesBefore=10&minutesAfter=30&timeFrom=${timeFrom}`
            )
            .end((err: any, res: any) => {
                expect(moment.parseZone(res.body.departures[0].arrival_timestamp.predicted).utcOffset()).to.be.equal(
                    moment.tz(`${todayYMD}T05:25:00`, `Europe/Prague`).utcOffset()
                );
                done();
            });
    });

    it("should respond with departure with preferredTimezone in Europe/Prague", (done) => {
        const timeFrom = moment.tz(`${todayYMD}T07:25:00`, "Europe/Prague").toISOString();
        request(app)
            .get(
                // eslint-disable-next-line max-len
                `/v2/pid/departureboards?ids[]=U713Z102P&minutesBefore=10&minutesAfter=30&timeFrom=${timeFrom}&preferredTimezone=Europe_Prague`
            )
            .end((err: any, res: any) => {
                expect(moment.parseZone(res.body.departures[0].arrival_timestamp.predicted).utcOffset()).to.be.equal(
                    moment.tz(`${todayYMD}T05:25:00`, `Europe/Prague`).utcOffset()
                );
                done();
            });
    });

    it("should use time zone from preferredTimezone (default Europe/Prague) for timeFrom without time zone", (done) => {
        const timeFrom = moment.tz(`${todayYMD}T07:25:00`, "Europe/Prague").toISOString();
        request(app)
            .get(`/v2/pid/departureboards?ids[]=U713Z102P&minutesBefore=10&minutesAfter=30&timeFrom=${timeFrom}`)
            .end((err: any, res: any) => {
                expect(res.body.departures).to.have.lengthOf(1);
                done();
            });
    });

    it("should use time zone from preferredTimezone (Europe/Prague) for timeFrom without time zone", (done) => {
        request(app)
            .get(
                // eslint-disable-next-line max-len
                `/v2/pid/departureboards?ids[]=U713Z102P&minutesBefore=10&minutesAfter=30&timeFrom=${todayYMD}T07:25:00&preferredTimezone=Europe_Prague`
            )
            .end((err: any, res: any) => {
                expect(res.body.departures).to.have.lengthOf(1);
                done();
            });
    });

    it("should return departure of 991 (metro A) with available trip and position data", (done) => {
        request(app)
            // U400Z101P does not have a corresponding CIS stop
            .get(`/v2/pid/departureboards?aswIds[]=400_101&minutesAfter=10&timeFrom=${todayYMD}T10:20:00`)
            .end((_err, res) => {
                expect(res.body.departures).to.have.lengthOf(1);
                expect(res.body.departures[0]).to.deep.equal({
                    arrival_timestamp: {
                        predicted: `${todayYMD}T10:20:27${todayTzOffset}`,
                        scheduled: `${todayYMD}T10:20:40${todayTzOffset}`,
                    },
                    delay: { is_available: true, minutes: 0, seconds: -13 },
                    departure_timestamp: {
                        predicted: `${todayYMD}T10:20:57${todayTzOffset}`,
                        scheduled: `${todayYMD}T10:21:10${todayTzOffset}`,
                        minutes: "<1",
                    },
                    last_stop: { id: "U1072Z101P", name: "Můstek" },
                    route: {
                        short_name: "A",
                        type: 1,
                        is_night: false,
                        is_regional: false,
                        is_substitute_transport: false,
                    },
                    stop: { id: "U400Z101P", platform_code: "A1" },
                    trip: {
                        direction: null,
                        headsign: "Depo Hostivař",
                        id: "991_294_220901",
                        is_at_stop: false,
                        is_canceled: false,
                        is_wheelchair_accessible: true, // stop.wheelchair_boarding is 1 (AccessibleStation)
                        is_air_conditioned: null,
                        short_name: null,
                    },
                });

                done();
            });
    });

    it("should return departure of 991 (metro A) in Namesti Miru with available trip and stop data", (done) => {
        request(app)
            .get(`/v2/pid/departureboards?ids[]=U476Z101P&minutesAfter=10&timeFrom=${todayYMD}T10:21:00`)
            .end((_err, res) => {
                expect(res.body.departures).to.have.lengthOf(1);
                expect(res.body.departures[0].stop).to.deep.equal({
                    id: "U476Z101P",
                    platform_code: "1",
                });

                expect(res.body.departures[0].trip).to.deep.equal({
                    direction: null,
                    headsign: "Depo Hostivař",
                    id: "991_294_220901",
                    is_at_stop: false,
                    is_canceled: false,
                    is_wheelchair_accessible: false, // stop.wheelchair_boarding is 2 (InaccessibleStation)
                    is_air_conditioned: null,
                    short_name: null,
                });

                done();
            });
    });

    it("should return departure of 991 (metro A) in Jiriho z Podebrad with available trip and stop data", (done) => {
        request(app)
            .get(`/v2/pid/departureboards?ids[]=U209Z101P&minutesAfter=10&timeFrom=${todayYMD}T10:22:00`)
            .end((_err, res) => {
                expect(res.body.departures).to.have.lengthOf(1);
                expect(res.body.departures[0].stop).to.deep.equal({
                    id: "U209Z101P",
                    platform_code: "1",
                });

                expect(res.body.departures[0].trip).to.deep.equal({
                    direction: null,
                    headsign: "Depo Hostivař",
                    id: "991_294_220901",
                    is_at_stop: false,
                    is_canceled: false,
                    is_wheelchair_accessible: false, // stop.wheelchair_boarding is 0 (NoInformation)
                    is_air_conditioned: null,
                    short_name: null,
                });

                done();
            });
    });

    it("should reject invalid ASW format", (done) => {
        request(app)
            .get(`/v2/pid/departureboards?aswIds[]=test_`)
            .end((_err, res) => {
                expect(res.statusCode).to.equal(400);
                done();
            });
    });

    it("should reject invalid CIS format", (done) => {
        request(app)
            .get(`/v2/pid/departureboards?cisIds[]=test`)
            .end((_err, res) => {
                expect(res.statusCode).to.equal(400);
                done();
            });
    });

    before(() => {
        clock = sinon.useFakeTimers({
            now: moment.tz(`${todayYMD}T10:37:00`, "Europe/Prague").valueOf(),
            shouldAdvanceTime: true,
        });
    });
    it("should return departure of 115 on 1st stop with proper headsigns", (done) => {
        request(app)
            .get(
                // eslint-disable-next-line max-len
                `/v2/pid/departureboards?ids[]=U52Z4P&minutesBefore=2&minutesAfter=2&timeFrom=${todayYMD}T10:45:00&preferredTimezone=Europe_Prague`
            )
            .end((err: any, res: any) => {
                expect(res.body.departures).to.have.lengthOf(2);
                expect(res.body.departures[0].trip.headsign).to.be.equal("Městský archiv");
                done();
            });
    });
    it("should return departure of 115 on 3th stop with proper headsigns", (done) => {
        request(app)
            .get(
                // eslint-disable-next-line max-len
                `/v2/pid/departureboards?ids[]=U1341Z1P&minutesBefore=5&minutesAfter=5&timeFrom=${todayYMD}T10:49:00&preferredTimezone=Europe_Prague&limit=3`
            )
            .end((err: any, res: any) => {
                expect(res.body.departures).to.have.lengthOf(3);
                expect(res.body.departures[0].trip.headsign).to.be.equal("Městský archiv");
                expect(res.body.departures[1].trip.headsign).to.be.equal("Městský archiv");
                // Trip 2021-07-26T08:30:00Z_100115_115_1061_V2 has already departed
                expect(res.body.departures[2].trip.headsign).to.be.equal("Městský archiv");
                done();
            });
    });

    it("should return departure of 115 on 4th stop with proper headsigns", (done) => {
        request(app)
            .get(
                // eslint-disable-next-line max-len
                `/v2/pid/departureboards?ids[]=U1132Z1P&minutesBefore=5&minutesAfter=5&timeFrom=${todayYMD}T10:51:00&preferredTimezone=Europe_Prague&limit=3`
            )
            .end((err: any, res: any) => {
                expect(res.body.departures).to.have.lengthOf(3);
                expect(res.body.departures[0].trip.headsign).to.be.equal("Chodov");
                expect(res.body.departures[1].trip.headsign).to.be.equal("Chodov");
                expect(res.body.departures[2].trip.headsign).to.be.equal("Chodov");
                done();
            });
    });

    it("should return departure with top direction and null is_air_conditioned", (done) => {
        request(app)
            .get(
                // eslint-disable-next-line max-len
                `/v2/pid/departureboards?ids[]=U474Z4P&minutesBefore=120&minutesAfter=120&limit=1`
            )
            .end((err: any, res: any) => {
                expect(res.body.departures).to.have.lengthOf(1);
                expect(res.body.departures[0].trip.direction).to.be.equal("top");
                expect(res.body.departures[0].trip.is_air_conditioned).to.be.equal(null);
                done();
            });
    });

    it("should return cancelled departure ~ 2 minutes after", (done) => {
        clock = sinon.useFakeTimers({
            now: moment.tz(`${todayYMD}T08:02:00`, "Europe/Prague").valueOf(),
            shouldAdvanceTime: true,
        });
        request(app)
            .get(`/v2/pid/departureboards?ids[]=U539Z2P&minutesBefore=0&minutesAfter=10&limit=1`)
            .end((err: any, res: any) => {
                expect(res.body.departures).to.have.lengthOf(1);
                expect(res.body.departures[0].trip.is_canceled).to.be.true;
                done();
            });
    });

    it("should return cancelled departure ~ 59 minutes after (validTo 60 min after departure)", (done) => {
        // valid to is mocked to 60 mins after departure
        clock = sinon.useFakeTimers({
            now: moment.tz(`${todayYMD}T08:59:00`, "Europe/Prague").valueOf(),
            shouldAdvanceTime: true,
        });
        request(app)
            .get(`/v2/pid/departureboards?ids[]=U539Z2P&minutesBefore=60&minutesAfter=10&limit=1`)
            .end((err: any, res: any) => {
                expect(res.body.departures).to.have.lengthOf(1);
                expect(res.body.departures[0].trip.is_canceled).to.be.true;
                done();
            });
    });

    it("should return Na Knížecí stops joined with Anděl metro stops (asw_stop_id 100+)", (done) => {
        request(app)
            .get(
                // eslint-disable-next-line max-len
                `/v2/pid/departureboards?names=Na%20Kn%C3%AD%C5%BEec%C3%AD&includeMetroTrains=true`
            )
            .end((_err, res) => {
                expect(res.body.stops[0].stop_id).to.equal("U1040Z101P");
                expect(res.body.stops[0].stop_name).to.equal("Anděl");

                expect(res.body.stops[1].stop_id).to.equal("U1040Z102P");
                expect(res.body.stops[1].stop_name).to.equal("Anděl");

                for (let i = 2; i < 8; i++) {
                    res.body.stops[2].stop_name = "Na Knížecí";
                }

                done();
            });
    });

    it("should not return duplicates of Anděl metro stops with includeMetroTrains", (done) => {
        request(app)
            .get(
                // eslint-disable-next-line max-len
                `/v2/pid/departureboards?ids[]=U1040Z6P&ids[]=U1040Z101P&includeMetroTrains=true`
            )
            .end((_err, res) => {
                expect(res.body.stops.length).to.equal(3);
                expect(res.body.stops[0].stop_id).to.equal("U1040Z101P");
                expect(res.body.stops[1].stop_id).to.equal("U1040Z102P");
                expect(res.body.stops[2].stop_id).to.equal("U1040Z6P");

                done();
            });
    });

    it("should return delay during night", (done) => {
        clock = sinon.useFakeTimers({
            now: moment.tz(`${todayYMD}T01:36:00`, "Europe/Prague").valueOf(),
            shouldAdvanceTime: true,
        });
        request(app)
            .get(`/v2/pid/departureboards?ids[]=U873Z1P`)
            .end((_err, res) => {
                expect(res.body.departures.length).to.equal(1);
                expect(res.body.departures[0].delay.is_available).to.be.true;
                expect(res.body.departures[0].delay.seconds).equal(10);

                done();
            });
    });
});
