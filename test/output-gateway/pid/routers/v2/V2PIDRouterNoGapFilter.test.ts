import { v2PidRouter } from "#og/pid";
import express from "@golemio/core/dist/shared/express";
import moment from "@golemio/core/dist/shared/moment-timezone";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon from "sinon";
import request from "supertest";

chai.use(chaiAsPromised);

describe("V2PIDRouter NoGap filter", () => {
    // Create clean express instance
    let app: any = null;
    // Basic configuration: create a sinon sandbox for testing
    let sandbox: any = null;
    const idNarodniTridaA = "U539Z1P";
    const idNarodniTridaB = "U539Z2P";
    const todayYMD = moment().format("YYYY-MM-DD");
    let clock: any = null;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        clock = sinon.useFakeTimers({
            now: moment.tz(`${todayYMD}T06:59:00`, "Europe/Prague").valueOf(),
            shouldAdvanceTime: true,
        });
    });

    afterEach(() => {
        sandbox && sandbox.restore();
    });

    before(() => {
        app = express();
        // Mount the tested router to the express instance
        app.use(v2PidRouter.getPath(), v2PidRouter.getRouter());
    });

    it("routeHeadingOnceNoGap - station Narodni trida 7am", (done) => {
        request(app)
            .get(`/v2/pid/departureboards?ids[]=${idNarodniTridaA}&ids[]=${idNarodniTridaB}&total=6&filter=routeHeadingOnceNoGap`)
            .end((err: any, res: any) => {
                expect(res.body.departures.length).to.be.equal(6);
                expect(res.body.departures[0].trip.id).to.be.equal("2_1502_220516"); //Sídliště Petřiny
                expect(res.body.departures[1].trip.id).to.be.equal("22_7178_220509"); //Bílá Hora
                expect(res.body.departures[2].trip.id).to.be.equal("9_4744_220502"); //Sídliště Řepy
                expect(res.body.departures[3].trip.id).to.be.equal("18_3621_220516"); //Nádraží Podbaba
                expect(res.body.departures[4].trip.id).to.be.equal("22_7981_220509"); //Vypich
                expect(res.body.departures[5].trip.id).to.be.equal("22_7028_220516"); //Bílá Hora - new stop id
                //9_4755_220502   Sídliště Řepy     skipped same line
                //2_1556_220516   Sídliště Petřiny  skipped same line
                //9_7027_220502   Sídliště Řepy     skipped same line
                //18_3632_220516  Nádraží Podbaba   skipped same line
                //9_4832_220502   Vozovna Motol     unique direction of line but it was removed before so also skipped
                done();
            });
    });
    it("routeHeadingOnce - station Narodni trida 7am - to validate different result with NoGap filter", (done) => {
        request(app)
            .get(`/v2/pid/departureboards?ids[]=${idNarodniTridaA}&ids[]=${idNarodniTridaB}&total=6&filter=routeHeadingOnce`)
            .end((err: any, res: any) => {
                expect(res.body.departures.length).to.be.equal(6);
                expect(res.body.departures[0].trip.id).to.be.equal("2_1502_220516"); //Sídliště Petřiny
                expect(res.body.departures[1].trip.id).to.be.equal("22_7178_220509"); //Bílá Hora
                expect(res.body.departures[2].trip.id).to.be.equal("9_4744_220502"); //Sídliště Řepy
                expect(res.body.departures[3].trip.id).to.be.equal("18_3621_220516"); //Nádraží Podbaba
                expect(res.body.departures[4].trip.id).to.be.equal("22_7981_220509"); //Vypich
                //9_4755_220502   Sídliště Řepy     skipped same line
                //2_1556_220516   Sídliště Petřiny  skipped same line
                //22_7028_220516  Bílá Hora         skipped same line
                //9_7027_220502   Sídliště Řepy     skipped same line
                //18_3632_220516  Nádraží Podbaba   skipped same line
                expect(res.body.departures[5].trip.id).to.be.equal("9_4832_220502"); //Vozovna Motol

                done();
            });
    });

    it("routeHeadingOnceNoGapFill - station Narodni trida 7am", (done) => {
        request(app)
            .get(
                // eslint-disable-next-line max-len
                `/v2/pid/departureboards?ids[]=${idNarodniTridaA}&ids[]=${idNarodniTridaB}&total=7&filter=routeHeadingOnceNoGapFill`
            )
            .end((err: any, res: any) => {
                expect(res.body.departures.length).to.be.equal(7);
                expect(res.body.departures[0].trip.id).to.be.equal("2_1502_220516"); //Sídliště Petřiny
                expect(res.body.departures[1].trip.id).to.be.equal("22_7178_220509"); //Bílá Hora
                expect(res.body.departures[2].trip.id).to.be.equal("9_4744_220502"); //Sídliště Řepy
                expect(res.body.departures[3].trip.id).to.be.equal("18_3621_220516"); //Nádraží Podbaba
                expect(res.body.departures[4].trip.id).to.be.equal("22_7981_220509"); //Vypich
                expect(res.body.departures[5].trip.id).to.be.equal("9_4755_220502"); // Fill - Sídliště Řepy
                expect(res.body.departures[6].trip.id).to.be.equal("22_7028_220516"); //Bílá Hora - new stop id
                //2_1556_220516   Sídliště Petřiny  skipped same line
                //9_7027_220502   Sídliště Řepy     skipped same line
                //18_3632_220516  Nádraží Podbaba   skipped same line
                //9_4832_220502   Vozovna Motol     unique direction of line but 9 was removed before so also skipped
                done();
            });
    });
    it("routeHeadingOnceFill - station Narodni trida 7am - to validate different result with NoGap filter", (done) => {
        request(app)
            .get(`/v2/pid/departureboards?ids[]=${idNarodniTridaA}&ids[]=${idNarodniTridaB}&total=7&filter=routeHeadingOnceFill`)
            .end((err: any, res: any) => {
                expect(res.body.departures.length).to.be.equal(7);
                expect(res.body.departures[0].trip.id).to.be.equal("2_1502_220516"); //Sídliště Petřiny
                expect(res.body.departures[1].trip.id).to.be.equal("22_7178_220509"); //Bílá Hora
                expect(res.body.departures[2].trip.id).to.be.equal("9_4744_220502"); //Sídliště Řepy
                expect(res.body.departures[3].trip.id).to.be.equal("18_3621_220516"); //Nádraží Podbaba
                expect(res.body.departures[4].trip.id).to.be.equal("22_7981_220509"); //Vypich
                expect(res.body.departures[5].trip.id).to.be.equal("9_4755_220502"); // Fill Sídliště Řepy
                //2_1556_220516   Sídliště Petřiny  skipped same line
                //22_7028_220516  Bílá Hora         skipped same line
                //9_7027_220502   Sídliště Řepy     skipped same line
                //18_3632_220516  Nádraží Podbaba   skipped same line
                expect(res.body.departures[6].trip.id).to.be.equal("9_4832_220502"); //Vozovna Motol

                done();
            });
    });
});
