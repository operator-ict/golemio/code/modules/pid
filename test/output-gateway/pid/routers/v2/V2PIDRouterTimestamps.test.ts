import { IPIDDepartureOutput, v2PidRouter } from "#og/pid";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import express, { Express, NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { HTTPErrorHandler, IGolemioError } from "@golemio/core/dist/shared/golemio-errors";
import moment from "@golemio/core/dist/shared/moment-timezone";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonFakeTimers, SinonSandbox } from "sinon";
import request from "supertest";

chai.use(chaiAsPromised);

describe("V2PIDRouter Timestamps", () => {
    let app: Express;
    let sandbox: SinonSandbox;
    const todayYMD = moment().format("YYYY-MM-DD");
    let clock: SinonFakeTimers;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        clock = sinon.useFakeTimers({
            now: moment.tz(`${todayYMD}T11:35:00`, "Europe/Prague").valueOf(),
            shouldAdvanceTime: true,
        });
    });

    afterEach(() => {
        sandbox.restore();
    });

    before(() => {
        app = express();
        // Mount the tested router to the express instance
        app.use(v2PidRouter.getPath(), v2PidRouter.getRouter());
        app.use((err: any, req: Request, res: Response, next: NextFunction) => {
            const errObject: IGolemioError = HTTPErrorHandler.handle(err, log);
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    it("should predict arrival and departure time", (done) => {
        request(app)
            .get(`/v2/pid/departureboards?ids[]=U474Z4P&limit=3`)
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(200);
                expect(res.body.departures).to.be.an("array");

                const departurePredictions = [
                    // delay -20 sec (ahead of time)
                    { trip_id: "152_37_201230", predictedArrivalTime: `11:36:40`, predictedDeparturelTime: `11:36:40` },
                    // delay 30 sec, wait time 40 sec
                    { trip_id: "152_60_201230", predictedArrivalTime: `11:42:30`, predictedDeparturelTime: `11:42:40` },
                    { trip_id: "177_121_210201", predictedArrivalTime: `11:43:00`, predictedDeparturelTime: `11:43:00` },
                ];

                expect(res.body.departures).to.have.length(departurePredictions.length);

                for (let i = 0; i < departurePredictions.length; i++) {
                    const departure = departurePredictions[i];

                    expect(res.body.departures[i].trip.id).to.be.equal(departure.trip_id);
                    expect(res.body.departures[i].arrival_timestamp.predicted).to.be.equal(
                        moment.tz(`${todayYMD}T${departure.predictedArrivalTime}`, "Europe/Prague").format()
                    );

                    expect(res.body.departures[i].departure_timestamp.predicted).to.be.equal(
                        moment.tz(`${todayYMD}T${departure.predictedDeparturelTime}`, "Europe/Prague").format()
                    );
                }

                done();
            });
    });

    it("should return departures with air conditioning information", (done) => {
        request(app)
            .get(`/v2/pid/departureboards?ids[]=U474Z4P&limit=2`)
            .end((err, res) => {
                expect(res.statusCode).to.be.equal(200);

                const firstDepature = res.body.departures.find(
                    (departure: IPIDDepartureOutput) => departure.trip.id === "152_37_201230"
                );

                const secondDeparture = res.body.departures.find(
                    (departure: IPIDDepartureOutput) => departure.trip.id === "152_60_201230"
                );

                expect(firstDepature?.trip.is_air_conditioned).to.be.false;
                expect(secondDeparture?.trip.is_air_conditioned).to.be.true;
                done();
            });
    });

    it("should return departures without air conditioning information (airCondition=false)", (done) => {
        request(app)
            .get(`/v2/pid/departureboards?ids[]=U474Z4P&airCondition=false&limit=2`)
            .end((err, res) => {
                expect(res.statusCode).to.be.equal(200);

                const firstDepature = res.body.departures.find(
                    (departure: IPIDDepartureOutput) => departure.trip.id === "152_37_201230"
                );

                const secondDeparture = res.body.departures.find(
                    (departure: IPIDDepartureOutput) => departure.trip.id === "152_60_201230"
                );

                expect(firstDepature?.trip.is_air_conditioned).to.be.null;
                expect(secondDeparture?.trip.is_air_conditioned).to.be.null;
                done();
            });
    });

    describe("train arrival with known platform code", () => {
        it("should return departures with platform code", (done) => {
            clock = sinon.useFakeTimers({
                now: moment.tz(`${todayYMD}T11:35:00`, "Europe/Prague").valueOf(),
                shouldAdvanceTime: true,
            });

            request(app)
                .get(`/v2/pid/departureboards?ids[]=U142Z301&&mode=arrivals`)
                .end((err, res) => {
                    expect(res.statusCode).to.be.equal(200);

                    const departures = res.body.departures;
                    const stops = res.body.stops;

                    expect(departures).to.have.length(1);
                    expect(departures[0].trip.id).to.equal("1309_2530_231210");
                    expect(departures[0].stop).to.deep.equal({
                        id: "U142Z301",
                        platform_code: "1A/1",
                    });

                    expect(stops).to.have.length(1);
                    expect(stops[0]).to.have.keys(
                        "asw_id",
                        "level_id",
                        "location_type",
                        "parent_station",
                        "platform_code",
                        "stop_id",
                        "stop_lat",
                        "stop_lon",
                        "stop_name",
                        "wheelchair_boarding",
                        "zone_id"
                    );

                    done();
                });
        });
    });
});
