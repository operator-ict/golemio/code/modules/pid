import { v2PidRouter } from "#og/pid";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import express, { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { HTTPErrorHandler, IGolemioError } from "@golemio/core/dist/shared/golemio-errors";
import moment from "@golemio/core/dist/shared/moment-timezone";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon from "sinon";
import request from "supertest";

chai.use(chaiAsPromised);

type PartialDeparture = { trip: { id: string } };

describe("V2PIDRouter Order&Filter", () => {
    // Create clean express instance
    let app: any = null;
    // Basic configuration: create a sinon sandbox for testing
    let sandbox: any = null;
    const id = "U474Z4P";
    const todayYMD = moment().format("YYYY-MM-DD");
    let clock: any = null;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        clock = sinon.useFakeTimers({
            now: moment.tz(`${todayYMD}T11:35:00`, "Europe/Prague").valueOf(),
            shouldAdvanceTime: true,
        });
    });

    afterEach(() => {
        sandbox && sandbox.restore();
    });

    before(() => {
        app = express();
        // Mount the tested router to the express instance
        app.use(v2PidRouter.getPath(), v2PidRouter.getRouter());
        app.use((err: any, req: Request, res: Response, next: NextFunction) => {
            const errObject: IGolemioError = HTTPErrorHandler.handle(err, log);
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    it("should respond with 6 departures with default order", (done) => {
        const timeFrom = moment.tz(`${todayYMD}T20:37:00`, "Europe/Prague").toISOString();
        request(app)
            .get(`/v2/pid/departureboards?ids[]=${id}&timeFrom=${timeFrom}&minutesBefore=200&minutesAfter=200`)
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(200);
                expect(res.body.departures).to.be.an("array");

                const rightOrder = [
                    { trip_id: "177_X_1", time: "21:28:00" },
                    { trip_id: "152_X_1", time: "21:37:00" },
                    { trip_id: "152_X_2", time: "21:42:00" },
                    { trip_id: "177_X_2", time: "21:43:00" },
                    { trip_id: "152_X_3", time: "21:52:00" },
                    { trip_id: "183_X_1", time: "21:59:00" },
                ];

                expect(res.body.departures).to.have.length(rightOrder.length);

                rightOrder.forEach((departure, i) => {
                    expect(res.body.departures[i].trip.id).to.be.equal(departure.trip_id);
                    expect(res.body.departures[i].arrival_timestamp.scheduled).to.be.equal(
                        moment.tz(`${todayYMD}T${departure.time}`, "Europe/Prague").format()
                    );
                });

                done();
            });
    });

    it("should respond with 3 departures routeOnce", (done) => {
        const timeFrom = moment.tz(`${todayYMD}T20:37:00`, "Europe/Prague").toISOString();
        request(app)
            .get(
                `/v2/pid/departureboards?ids[]=${id}&filter=routeOnce` +
                    `&timeFrom=${timeFrom}&minutesBefore=200&minutesAfter=200`
            )
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(200);
                expect(res.body.departures).to.be.an("array");

                const rightOrder = [
                    { trip_id: "177_X_1", time: `21:28:00` },
                    { trip_id: "152_X_1", time: `21:37:00` },
                    // { trip_id: "152_X_2", time: `21:42:00` },
                    // { trip_id: "177_X_2", time: `21:43:00` },
                    // { trip_id: "152_X_3", time: `21:52:00` },
                    { trip_id: "183_X_1", time: `21:59:00` },
                ];

                expect(res.body.departures).to.have.length(rightOrder.length);

                rightOrder.forEach((departure, i) => {
                    expect(res.body.departures[i].trip.id).to.be.equal(departure.trip_id);
                    expect(res.body.departures[i].arrival_timestamp.scheduled).to.be.equal(
                        moment.tz(`${todayYMD}T${departure.time}`, "Europe/Prague").format()
                    );
                });

                done();
            });
    });

    it("should respond with 4 departures routeOnceFill", (done) => {
        const timeFrom = moment.tz(`${todayYMD}T20:37:00`, "Europe/Prague").toISOString();
        request(app)
            .get(
                `/v2/pid/departureboards?ids[]=${id}&filter=routeOnceFill` +
                    `&limit=4&timeFrom=${timeFrom}&minutesBefore=200&minutesAfter=200`
            )
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(200);
                expect(res.body.departures).to.be.an("array");

                const rightOrder = [
                    { trip_id: "177_X_1", time: `21:28:00` },
                    { trip_id: "152_X_1", time: `21:37:00` },
                    { trip_id: "152_X_2", time: `21:42:00` },
                    // { trip_id: "177_X_2", time: `21:43:00` },
                    // { trip_id: "152_X_3", time: `21:52:00` },
                    { trip_id: "183_X_1", time: `21:59:00` },
                ];

                expect(res.body.departures).to.have.length(rightOrder.length);

                rightOrder.forEach((departure, i) => {
                    expect(res.body.departures[i].trip.id).to.be.equal(departure.trip_id);
                    expect(res.body.departures[i].arrival_timestamp.scheduled).to.be.equal(
                        moment.tz(`${todayYMD}T${departure.time}`, "Europe/Prague").format()
                    );
                });

                done();
            });
    });

    it("should respond with 4 departures routeHeadingOnceFill", (done) => {
        const timeFrom = moment.tz(`${todayYMD}T20:37:00`, "Europe/Prague").toISOString();
        request(app)
            .get(
                `/v2/pid/departureboards?ids[]=${id}&filter=routeHeadingOnceFill` +
                    `&limit=4&timeFrom=${timeFrom}&minutesBefore=200&minutesAfter=200`
            )
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(200);
                expect(res.body.departures).to.be.an("array");

                const rightOrder = [
                    { trip_id: "177_X_1", time: `21:28:00` },
                    { trip_id: "152_X_1", time: `21:37:00` },
                    { trip_id: "152_X_2", time: `21:42:00` },
                    // { trip_id: "177_X_2", time: `21:43:00` },
                    // { trip_id: "152_X_3", time: `21:52:00` },
                    { trip_id: "183_X_1", time: `21:59:00` },
                ];

                expect(res.body.departures).to.have.length(rightOrder.length);

                rightOrder.forEach((departure, i) => {
                    expect(res.body.departures[i].trip.id).to.be.equal(departure.trip_id);
                    expect(res.body.departures[i].arrival_timestamp.scheduled).to.be.equal(
                        moment.tz(`${todayYMD}T${departure.time}`, "Europe/Prague").format()
                    );
                });

                done();
            });
    });

    it("should respond with 4 departures routeHeadingOnce", (done) => {
        const timeFrom = moment.tz(`${todayYMD}T20:37:00`, "Europe/Prague").toISOString();
        request(app)
            .get(
                `/v2/pid/departureboards?ids[]=${id}&filter=routeHeadingOnce` +
                    `&limit=5&timeFrom=${timeFrom}&minutesBefore=200&minutesAfter=200`
            )
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(200);
                expect(res.body.departures).to.be.an("array");

                const rightOrder = [
                    { trip_id: "177_X_1", time: `21:28:00` },
                    { trip_id: "152_X_1", time: `21:37:00` },
                    { trip_id: "152_X_2", time: `21:42:00` },
                    // { trip_id: "177_X_2", time: `21:43:00` },
                    // { trip_id: "152_X_3", time: `21:52:00` },
                    { trip_id: "183_X_1", time: `21:59:00` },
                ];

                expect(res.body.departures).to.have.length(rightOrder.length);

                rightOrder.forEach((departure, i) => {
                    expect(res.body.departures[i].trip.id).to.be.equal(departure.trip_id);
                    expect(res.body.departures[i].arrival_timestamp.scheduled).to.be.equal(
                        moment.tz(`${todayYMD}T${departure.time}`, "Europe/Prague").format()
                    );
                });

                done();
            });
    });

    it("should respond with 4 departures with default order", (done) => {
        request(app)
            .get(`/v2/pid/departureboards?ids[]=${id}`)
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(200);
                expect(res.body.departures).to.be.an("array");
                const rightOrder = [
                    { trip_id: "152_37_201230", time: `11:37:00` }, // already departured
                    { trip_id: "152_60_201230", time: `11:42:00` },
                    { trip_id: "177_121_210201", time: `11:43:00` },
                    { trip_id: "152_61_201230", time: `11:52:00` },
                ];

                expect(res.body.departures).to.have.length(rightOrder.length);

                rightOrder.forEach((departure, i) => {
                    expect(res.body.departures[i].trip.id).to.be.equal(departure.trip_id);
                    expect(res.body.departures[i].arrival_timestamp.scheduled).to.be.equal(
                        moment.tz(`${todayYMD}T${departure.time}`, "Europe/Prague").format()
                    );
                });

                done();
            });
    });

    it("should respond with 2 departures with ?filter=routeOnce", (done) => {
        request(app)
            .get(`/v2/pid/departureboards?ids[]=${id}&filter=routeOnce&limit=3`)
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(200);
                expect(res.body.departures).to.be.an("array");

                const rightOrder = [
                    { trip_id: "152_37_201230", time: `11:37:00` },
                    { trip_id: "177_121_210201", time: `11:43:00` },
                ];

                expect(res.body.departures).to.have.length(rightOrder.length);

                rightOrder.forEach((departure, i) => {
                    expect(res.body.departures[i].trip.id).to.be.equal(departure.trip_id);
                    expect(res.body.departures[i].arrival_timestamp.scheduled).to.be.equal(
                        moment.tz(`${todayYMD}T${departure.time}`, "Europe/Prague").format()
                    );
                });

                done();
            });
    });

    it("should respond with 3 departures with ?filter=routeHeadingOnce", (done) => {
        request(app)
            .get(`/v2/pid/departureboards?ids[]=${id}&filter=routeHeadingOnce&limit=3`)
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(200);
                expect(res.body.departures).to.be.an("array");

                const rightOrder = [
                    { trip_id: "152_37_201230", time: `11:37:00` },
                    { trip_id: "152_60_201230", time: `11:42:00` },
                    { trip_id: "177_121_210201", time: `11:43:00` },
                ];

                expect(res.body.departures).to.have.length(rightOrder.length);

                rightOrder.forEach((departure, i) => {
                    expect(res.body.departures[i].trip.id).to.be.equal(departure.trip_id);
                    expect(res.body.departures[i].arrival_timestamp.scheduled).to.be.equal(
                        moment.tz(`${todayYMD}T${departure.time}`, "Europe/Prague").format()
                    );
                });

                done();
            });
    });

    it("should respond with 3 departures with ?filter=routeOnceFill", (done) => {
        request(app)
            .get(`/v2/pid/departureboards?ids[]=${id}&filter=routeOnceFill&limit=3`)
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(200);
                expect(res.body.departures).to.be.an("array");

                const rightOrder = [
                    { trip_id: "152_37_201230", time: `11:37:00` },
                    { trip_id: "152_60_201230", time: `11:42:00` },
                    { trip_id: "177_121_210201", time: `11:43:00` },
                ];

                expect(res.body.departures).to.have.length(rightOrder.length);

                rightOrder.forEach((departure, i) => {
                    expect(res.body.departures[i].trip.id).to.be.equal(departure.trip_id);
                    expect(res.body.departures[i].arrival_timestamp.scheduled).to.be.equal(
                        moment.tz(`${todayYMD}T${departure.time}`, "Europe/Prague").format()
                    );
                });

                done();
            });
    });

    it("should respond with 3 departures with ?filter=routeHeadingOnceFill", (done) => {
        request(app)
            .get(`/v2/pid/departureboards?ids[]=${id}&filter=routeHeadingOnceFill&limit=4`)
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(200);
                expect(res.body.departures).to.be.an("array");

                const rightOrder = [
                    { trip_id: "152_37_201230", time: `11:37:00` },
                    { trip_id: "152_60_201230", time: `11:42:00` },
                    { trip_id: "177_121_210201", time: `11:43:00` },
                    { trip_id: "152_61_201230", time: `11:52:00` },
                ];

                expect(res.body.departures).to.have.length(rightOrder.length);

                rightOrder.forEach((departure, i) => {
                    expect(res.body.departures[i].trip.id).to.be.equal(departure.trip_id);
                    expect(res.body.departures[i].arrival_timestamp.scheduled).to.be.equal(
                        moment.tz(`${todayYMD}T${departure.time}`, "Europe/Prague").format()
                    );
                });

                done();
            });
    });

    it("should properly ?total&limit&offset - limit 2", (done) => {
        request(app)
            .get(`/v2/pid/departureboards?ids[]=${id}&filter=routeOnceFill&limit=2`)
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(200);
                expect(res.body.departures).to.be.an("array");

                const firstDeparture = "152_37_201230"; // 1
                const lastDeparture = "177_121_210201"; // 2
                // "152_60_201230"
                // "152_61_201230"

                expect(res.body.departures).to.have.length(2);

                expect(res.body.departures[0].trip.id).to.be.equal(firstDeparture);
                expect(res.body.departures[res.body.departures.length - 1].trip.id).to.be.equal(lastDeparture);

                done();
            });
    });

    it("should properly ?total&limit&offset - total 3, limit 2, offset 1", (done) => {
        request(app)
            .get(`/v2/pid/departureboards?ids[]=${id}&filter=routeOnceFill&total=3&limit=2&offset=1`)
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(200);
                expect(res.body.departures).to.be.an("array");

                // "152_37_201230" // 1
                const firstDeparture = "152_60_201230"; // 2
                const lastDeparture = "177_121_210201"; // 3

                expect(res.body.departures).to.have.length(2);

                expect(res.body.departures[0].trip.id).to.be.equal(firstDeparture);
                expect(res.body.departures[res.body.departures.length - 1].trip.id).to.be.equal(lastDeparture);

                done();
            });
    });

    it("should properly ?total&limit&offset - total 2, limit 4, offset 0", (done) => {
        request(app)
            .get(`/v2/pid/departureboards?ids[]=${id}&filter=routeOnceFill&total=2&limit=4&offset=0`)
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(200);
                expect(res.body.departures).to.be.an("array");

                const firstDeparture = "152_37_201230"; // 1
                const lastDeparture = "177_121_210201"; // 2

                expect(res.body.departures).to.have.length(2);

                expect(res.body.departures[0].trip.id).to.be.equal(firstDeparture);
                expect(res.body.departures[res.body.departures.length - 1].trip.id).to.be.equal(lastDeparture);

                done();
            });
    });

    it("should NOT skip departure of trip at stop", (done) => {
        request(app)
            .get(`/v2/pid/departureboards?ids[]=${id}&limit=10`)
            .end((err: any, res: any) => {
                expect(res.body.departures).to.be.an("array").and.to.have.lengthOf.lessThan(10);
                const tripIds = res.body.departures.map((departure: PartialDeparture) => departure.trip.id);
                expect(tripIds).includes("152_37_201230");
                done();
            });
    });

    it("should skip departure of trip at stop", (done) => {
        request(app)
            .get(`/v2/pid/departureboards?ids[]=${id}&skip=atStop&limit=10`)
            .end((_err, res) => {
                // "152_37_201230" skipped (is at stop)
                // "152_60_201230" skipped (is pass stop)
                expect(res.body.departures).to.be.an("array").and.to.have.length(2);
                expect(res.body.departures[0].trip.id).to.equal("177_121_210201");
                expect(res.body.departures[1].trip.id).to.equal("152_61_201230");
                done();
            });
    });

    it("should skip departure of trip at stop (positive minutesBefore)", (done) => {
        request(app)
            .get(`/v2/pid/departureboards?ids[]=${id}&minutesBefore=30&skip=atStop&limit=10`)
            .end((_err, res) => {
                // "152_37_201230" skipped (is at stop)
                // "152_60_201230" skipped (is pass stop)
                expect(res.body.departures).to.be.an("array").and.to.have.length(3);
                expect(res.body.departures[0].trip.id).to.equal("177_110_210201");
                expect(res.body.departures[1].trip.id).to.equal("177_121_210201");
                expect(res.body.departures[2].trip.id).to.equal("152_61_201230");
                done();
            });
    });

    it("should skip departure of trip at stop (negative minutesBefore)", (done) => {
        request(app)
            .get(`/v2/pid/departureboards?ids[]=${id}&minutesBefore=-5&skip=atStop&limit=10`)
            .end((_err, res) => {
                // "152_37_201230" filtered with negative minutesBefore
                // "152_60_201230" skipped (is pass stop)
                expect(res.body.departures).to.be.an("array").and.to.have.length(2);
                expect(res.body.departures[0].trip.id).to.equal("177_121_210201");
                expect(res.body.departures[1].trip.id).to.equal("152_61_201230");
                done();
            });
    });

    it("should skip departure of trip that is canceled", (done) => {
        request(app)
            .get(`/v2/pid/departureboards?ids[]=${id}&skip=canceled&limit=10`)
            .end((err: any, res: any) => {
                expect(res.body.departures).to.be.an("array").and.to.have.length(3);
                expect(res.body.departures[0].trip.id).to.be.equal("152_37_201230");
                // "152_60_201230" skipped (is cancelled)
                expect(res.body.departures[1].trip.id).to.be.equal("177_121_210201");
                expect(res.body.departures[2].trip.id).to.be.equal("152_61_201230");
                done();
            });
    });

    it("should skip departure of trips that are at stop and cancelled", (done) => {
        request(app)
            .get(`/v2/pid/departureboards?ids[]=${id}&skip[]=atStop&skip[]=canceled&limit=10`)
            .end((err: any, res: any) => {
                // "152_37_201230" skipped
                // "152_60_201230" is cancelled
                expect(res.statusCode).to.equal(200);
                expect(res.body.departures).to.be.an("array").and.to.have.length(2);
                expect(res.body.departures[0].trip.id).to.be.equal("177_121_210201");
                expect(res.body.departures[1].trip.id).to.be.equal("152_61_201230");
                done();
            });
    });

    it("should skip departure of trips that are untracked", (done) => {
        request(app)
            .get(`/v2/pid/departureboards?ids[]=${id}&total=4&skip[]=untracked&skip[]=canceled`)
            .end((err: any, res: any) => {
                expect(res.body.departures[0].trip.id).to.be.equal("152_37_201230");
                // "152_60_201230" skipped (canceled)
                expect(res.body.departures).to.have.length(1); //not 4
                // "177_121_210201" skipped (untracked)
                // "152_61_201230" skipped (untracked)
                done();
            });
    });

    it("should show canceled departure 3 minutes after departure", (done) => {
        clock = sinon.useFakeTimers({
            now: moment.tz(`${todayYMD}T11:44:59`, "Europe/Prague").valueOf(),
            shouldAdvanceTime: true,
        });
        request(app)
            .get(`/v2/pid/departureboards?ids[]=${id}&total=3`)
            .end((err: any, res: any) => {
                expect(res.body.departures[0].trip.id).to.be.equal("152_60_201230");
                expect(res.body.departures).to.have.length(2);
                // "152_61_201230" is next
                done();
            });
    });
});
