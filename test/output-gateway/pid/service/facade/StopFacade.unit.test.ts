import { OgPidToken } from "#og/pid/ioc/OgPidToken";
import { StopFacade } from "#og/pid/service/facade/StopFacade";
import { models } from "#og/ropid-gtfs";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { DependencyContainer, container as RootContainer } from "@golemio/core/dist/shared/tsyringe";
import { expect } from "chai";
import sinon, { SinonSandbox, SinonStub } from "sinon";

describe("StopFacade (unit)", () => {
    let sandbox: SinonSandbox;
    let container: DependencyContainer;

    // Mocks
    let getNodeMock: SinonStub;
    let getMultipleIdsMock: SinonStub;

    const createDependencyContainer = (): StopFacade => {
        return container
            .registerSingleton(
                OgPidToken.CisStopGroupRepository,
                class DummyCisStopGroupRepository {
                    public getNodeByCisId = getNodeMock;
                }
            )
            .registerSingleton(OgPidToken.StopFacade, StopFacade)
            .resolve(OgPidToken.StopFacade);
    };

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        getNodeMock = sandbox.stub();
        getMultipleIdsMock = sandbox.stub();
        sandbox.stub(models.GTFSStopModel, "getMultipleIdsByAswNode").callsFake(getMultipleIdsMock);

        container = RootContainer.createChildContainer();
    });

    afterEach(() => {
        sandbox.restore();
        container.clearInstances();
    });

    describe("getStopIdsForTransferBoards", () => {
        it("should return empty array if CIS stop node is not found", async () => {
            getNodeMock.resolves(null);

            const facade = createDependencyContainer();
            const result = await facade.getStopIdsForTransferBoards("123");

            expect(result).to.deep.equal([]);
        });

        it("should throw an error if CIS stop node retrieval fails", async () => {
            getNodeMock.rejects(new Error("Test error"));

            const facade = createDependencyContainer();

            try {
                await facade.getStopIdsForTransferBoards("123");
                expect.fail("Should throw an error");
            } catch (error) {
                expect(error).to.be.an.instanceOf(GeneralError);
            }
        });

        it("should throw an error if GTFS stop retrieval fails", async () => {
            getNodeMock.resolves("123");
            getMultipleIdsMock.rejects(new Error("Test error"));

            const facade = createDependencyContainer();

            try {
                await facade.getStopIdsForTransferBoards("123");
                expect.fail("Should throw an error");
            } catch (error) {
                expect(error).to.be.an.instanceOf(GeneralError);
            }
        });

        it("should return GTFS stop IDs", async () => {
            getNodeMock.resolves("123");
            getMultipleIdsMock.resolves(["123"]);

            const facade = createDependencyContainer();
            const result = await facade.getStopIdsForTransferBoards("123");

            expect(result).to.deep.equal(["123"]);
        });
    });
});
