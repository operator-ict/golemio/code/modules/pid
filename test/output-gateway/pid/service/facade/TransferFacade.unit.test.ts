import { OgPidToken } from "#og/pid/ioc/OgPidToken";
import PIDDeparturesModel from "#og/pid/models/helpers/PIDDepartureModel";
import { TransferFacade } from "#og/pid/service/facade/TransferFacade";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { DependencyContainer, container as RootContainer } from "@golemio/core/dist/shared/tsyringe";
import { expect } from "chai";
import sinon, { SinonSandbox, SinonStub } from "sinon";

describe("TransferFacade (unit)", () => {
    let sandbox: SinonSandbox;
    let container: DependencyContainer;

    // Mocks
    let getDeparturesMock: SinonStub;
    let processAndReturnTransfersMock: SinonStub;
    let transformArrayMock: SinonStub;

    const createDependencyContainer = (): TransferFacade => {
        return container
            .registerSingleton(
                OgPidToken.DeparturesRepository,
                class DummyDeparturesRepository {
                    public getTransferDepartures = getDeparturesMock;
                }
            )
            .registerSingleton(
                OgPidToken.TransferDepartureTransformation,
                class DummyTransferDepartureTransformation {
                    public transformArray = transformArrayMock;
                }
            )
            .registerSingleton(OgPidToken.TransferFacade, TransferFacade)
            .resolve(OgPidToken.TransferFacade);
    };

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        getDeparturesMock = sandbox.stub();
        transformArrayMock = sandbox.stub();
        processAndReturnTransfersMock = sandbox.stub();

        sandbox.stub(PIDDeparturesModel.prototype, "processAndReturnTransfers").callsFake(processAndReturnTransfersMock);

        container = RootContainer.createChildContainer();
    });

    afterEach(() => {
        sandbox.restore();
        container.clearInstances();
    });

    describe("getTransferDepartures", () => {
        it("should return empty array if no departures are found", async () => {
            getDeparturesMock.resolves([]);
            const transferFacade = createDependencyContainer();

            const result = await transferFacade.getTransferDepartures(["1", "2"], "123", {} as any, 0);
            expect(result).to.be.an("array").that.is.empty;
        });

        it("should throw an error if an error occurs while retrieving departures", async () => {
            getDeparturesMock.rejects(new Error("Error"));
            const transferFacade = createDependencyContainer();

            try {
                await transferFacade.getTransferDepartures(["1", "2"], "123", {} as any, 0);
                expect.fail("Should throw an error");
            } catch (error) {
                expect(error).to.be.instanceOf(GeneralError);
            }
        });

        it("should throw an error if an error occurs while processing departures", async () => {
            getDeparturesMock.resolves([{} as any]);
            processAndReturnTransfersMock.throws(new Error("Error"));
            const transferFacade = createDependencyContainer();

            try {
                await transferFacade.getTransferDepartures(["1", "2"], "123", {} as any, 0);
                expect.fail("Should throw an error");
            } catch (error) {
                expect(error).to.be.instanceOf(GeneralError);
            }
        });

        it("should return transformed departures", async () => {
            getDeparturesMock.resolves([{} as any]);
            processAndReturnTransfersMock.returns([{} as any]);
            transformArrayMock.returns([{} as any]);
            const transferFacade = createDependencyContainer();

            const result = await transferFacade.getTransferDepartures(["1", "2"], "123", {} as any, 0);
            expect(result).to.be.an("array").that.has.lengthOf(1);
        });
    });
});
