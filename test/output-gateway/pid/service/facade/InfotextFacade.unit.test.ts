import { OgPidToken } from "#og/pid/ioc/OgPidToken";
import { IGetDeparturesOptions } from "#og/pid/models/interfaces/IGetDeparturesOptions";
import { InfotextFacade } from "#og/pid/service/facade/InfotextFacade";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import moment from "@golemio/core/dist/shared/moment-timezone";
import { DependencyContainer, container as RootContainer } from "@golemio/core/dist/shared/tsyringe";
import { expect } from "chai";
import sinon, { SinonSandbox, SinonStub } from "sinon";
import { infotextsDtoFixture } from "../../fixtures/InfotextsDtoFixture";
import { infotextsForDeparturesResponseFixture } from "../../fixtures/InfotextsForDeparturesResponseFixtures";
import { infotextsForOverviewResponseFixture } from "../../fixtures/InfotextsForOverviewResponseFixtures";
import { infotextsForTransportsResponseFixture } from "../../fixtures/InfotextsForTransfersResponseFixtures";

describe("InfotextFacade (unit)", () => {
    let sandbox: SinonSandbox;
    let container: DependencyContainer;

    // Mocks
    let getInfotextsMock: SinonStub;
    let getInfotextsForOverviewMock: SinonStub;
    let filterMock: SinonStub;
    let transformElementMock: SinonStub;
    let transformArrayMock: SinonStub;

    const createDependencyContainer = (): InfotextFacade => {
        return container
            .registerSingleton(
                OgPidToken.JISInfotextRepository,
                class DummyJISInfotextRepository {
                    public findAllForDepartureBoard = getInfotextsMock;
                    public findAllForOverview = getInfotextsForOverviewMock;
                }
            )
            .registerSingleton(
                OgPidToken.JISInfotextDepartureTransformation,
                class DummyJISInfotextDepartureTransformation {
                    public transformElement = transformElementMock;
                }
            )
            .registerSingleton(
                OgPidToken.JISInfotextOverviewTransformation,
                class DummyJISInfotextOverviewTransformation {
                    public transformArray = transformArrayMock;
                }
            )
            .registerSingleton(
                OgPidToken.JISInfotextTransferTransformation,
                class DummyJISInfotextTransferTransformation {
                    public transformArray = transformArrayMock;
                }
            )
            .registerSingleton(
                OgPidToken.JISInfotextStopSuppressionFilter,
                class DummyJISInfotextStopSuppressionFilter {
                    public filterBySeverityLevel = filterMock;
                }
            )
            .registerSingleton(OgPidToken.InfotextFacade, InfotextFacade)
            .resolve(OgPidToken.InfotextFacade);
    };

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        getInfotextsMock = sandbox.stub();
        getInfotextsForOverviewMock = sandbox.stub();
        filterMock = sandbox.stub();
        transformElementMock = sandbox.stub();
        transformArrayMock = sandbox.stub();

        container = RootContainer.createChildContainer();
    });

    afterEach(() => {
        sandbox.restore();
        container.clearInstances();
    });

    describe("getInfotextsForDepartureBoards", () => {
        it("should return transformed infotexts", async () => {
            const facade = createDependencyContainer();
            getInfotextsMock.resolves(infotextsDtoFixture);
            filterMock.callsFake((infotexts) => infotexts);
            transformElementMock.returns(infotextsForDeparturesResponseFixture[0]);

            const result = await facade.getInfotextsForDepartureBoards(["123"], moment(), {
                timezone: "Europe/Prague",
            } as IGetDeparturesOptions);

            expect(result).to.be.an("array");
            expect(result).to.have.length(infotextsDtoFixture.length);
            expect(filterMock.called).to.be.true;
            expect(transformElementMock.called).to.be.true;
            expect(transformElementMock.getCall(0).args[0]).to.deep.equal({
                data: infotextsDtoFixture[0],
                timeZone: "Europe/Prague",
            });
        });

        it("should return empty array if no infotexts are found", async () => {
            const facade = createDependencyContainer();
            getInfotextsMock.resolves([]);

            const result = await facade.getInfotextsForDepartureBoards(["123"], moment(), {
                timezone: "Europe/Prague",
            } as IGetDeparturesOptions);
            expect(result).to.be.an("array");
            expect(result).to.have.length(0);
            expect(filterMock.called).to.be.false;
            expect(transformElementMock.called).to.be.false;
        });

        it("should return empty array if all infotexts are filtered out", async () => {
            const facade = createDependencyContainer();
            getInfotextsMock.resolves(infotextsDtoFixture);
            filterMock.returns([]);
            transformElementMock.returns(infotextsForDeparturesResponseFixture[0]);

            const result = await facade.getInfotextsForDepartureBoards(["123"], moment(), {
                timezone: "Europe/Prague",
            } as IGetDeparturesOptions);
            expect(result).to.be.an("array");
            expect(result).to.have.length(0);
            expect(filterMock.called).to.be.true;
            expect(transformElementMock.called).to.be.false;
        });

        it("should throw an error if infotextRepository throws an error", async () => {
            const facade = createDependencyContainer();
            getInfotextsMock.throws(new Error("Test error"));

            try {
                await facade.getInfotextsForDepartureBoards(["123"], moment(), {
                    timezone: "Europe/Prague",
                } as IGetDeparturesOptions);
                expect.fail("Should throw an error");
            } catch (error) {
                expect(error).to.be.an.instanceOf(GeneralError);
            }
        });

        it("should throw an error if transformation throws an error", async () => {
            const facade = createDependencyContainer();
            getInfotextsMock.resolves(infotextsDtoFixture);
            transformElementMock.throws(new Error("Test error"));

            try {
                await facade.getInfotextsForDepartureBoards(["123"], moment(), {
                    timezone: "Europe/Prague",
                } as IGetDeparturesOptions);
                expect.fail("Should throw an error");
            } catch (error) {
                expect(error).to.be.an.instanceOf(GeneralError);
            }
        });
    });

    describe("getInfotextsForTransferBoards", () => {
        it("should return transformed infotexts", async () => {
            const facade = createDependencyContainer();
            getInfotextsMock.resolves(infotextsDtoFixture);
            filterMock.callsFake((infotexts) => infotexts);
            transformArrayMock.returns(infotextsForTransportsResponseFixture);

            const result = await facade.getInfotextsForTransferBoards(["123"], moment(), null);
            expect(result).to.be.an("array");
            expect(result).to.have.length(infotextsDtoFixture.length);
            expect(filterMock.called).to.be.true;
            expect(transformArrayMock.called).to.be.true;
            expect(transformArrayMock.getCall(0).args[0]).to.deep.equal(infotextsDtoFixture);
        });

        it("should return empty array if no infotexts are found", async () => {
            const facade = createDependencyContainer();
            getInfotextsMock.resolves([]);

            const result = await facade.getInfotextsForTransferBoards(["123"], moment(), null);
            expect(result).to.be.an("array");
            expect(result).to.have.length(0);
            expect(filterMock.called).to.be.false;
            expect(transformArrayMock.called).to.be.false;
        });

        it("should return empty array if all infotexts are filtered out", async () => {
            const facade = createDependencyContainer();
            getInfotextsMock.resolves(infotextsDtoFixture);
            filterMock.returns([]);
            transformArrayMock.returns(infotextsForTransportsResponseFixture);

            const result = await facade.getInfotextsForTransferBoards(["123"], moment(), null);
            expect(result).to.be.an("array");
            expect(result).to.have.length(0);
            expect(filterMock.called).to.be.true;
            expect(transformArrayMock.called).to.be.false;
        });

        it("should throw an error if infotextRepository throws an error", async () => {
            const facade = createDependencyContainer();
            getInfotextsMock.throws(new Error("Test error"));

            try {
                await facade.getInfotextsForTransferBoards(["123"], moment(), null);
                expect.fail("Should throw an error");
            } catch (error) {
                expect(error).to.be.an.instanceOf(GeneralError);
            }
        });

        it("should throw an error if transformation throws an error", async () => {
            const facade = createDependencyContainer();
            getInfotextsMock.resolves(infotextsDtoFixture);
            transformArrayMock.throws(new Error("Test error"));

            try {
                await facade.getInfotextsForTransferBoards(["123"], moment(), null);
                expect.fail("Should throw an error");
            } catch (error) {
                expect(error).to.be.an.instanceOf(GeneralError);
            }
        });
    });

    describe("getInfotextsForOverview", () => {
        it("should return transformed infotexts", async () => {
            const facade = createDependencyContainer();
            getInfotextsForOverviewMock.resolves(infotextsDtoFixture);
            filterMock.callsFake((infotexts) => infotexts);
            transformArrayMock.returns(infotextsForOverviewResponseFixture);

            const result = await facade.getInfotextsForOverview();
            expect(result).to.be.an("array");
            expect(result).to.have.length(infotextsDtoFixture.length);
            expect(filterMock.called).to.be.true;
            expect(transformArrayMock.called).to.be.true;
            expect(transformArrayMock.getCall(0).args[0]).to.deep.equal(infotextsDtoFixture);
        });

        it("should return empty array if no infotexts are found", async () => {
            const facade = createDependencyContainer();
            getInfotextsForOverviewMock.resolves([]);

            const result = await facade.getInfotextsForOverview();
            expect(result).to.be.an("array");
            expect(result).to.have.length(0);
            expect(filterMock.called).to.be.false;
            expect(transformArrayMock.called).to.be.false;
        });

        it("should return empty array if all infotexts are filtered out", async () => {
            const facade = createDependencyContainer();
            getInfotextsForOverviewMock.resolves(infotextsDtoFixture);
            filterMock.returns([]);
            transformArrayMock.returns(infotextsForOverviewResponseFixture);

            const result = await facade.getInfotextsForOverview();
            expect(result).to.be.an("array");
            expect(result).to.have.length(0);
            expect(filterMock.called).to.be.true;
            expect(transformArrayMock.called).to.be.false;
        });

        it("should throw an error if infotextRepository throws an error", async () => {
            const facade = createDependencyContainer();
            getInfotextsForOverviewMock.throws(new Error("Test error"));

            try {
                await facade.getInfotextsForOverview();
                expect.fail("Should throw an error");
            } catch (error) {
                expect(error).to.be.an.instanceOf(GeneralError);
            }
        });

        it("should throw an error if transformation throws an error", async () => {
            const facade = createDependencyContainer();
            getInfotextsForOverviewMock.resolves(infotextsDtoFixture);
            transformArrayMock.throws(new Error("Test error"));

            try {
                await facade.getInfotextsForOverview();
                expect.fail("Should throw an error");
            } catch (error) {
                expect(error).to.be.an.instanceOf(GeneralError);
            }
        });
    });
});
