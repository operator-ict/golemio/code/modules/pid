import { InfotextDisplayType } from "#og/pid/domain/InfotextDisplayTypeEnum";
import { OgPidContainer } from "#og/pid/ioc/Di";
import { OgPidToken } from "#og/pid/ioc/OgPidToken";
import { JISInfotextDepartureTransformation } from "#og/pid/service/transformations/JISInfotextDepartureTransformation";
import { InfotextSeverityLevel } from "#og/shared/constants/jis/InfotextSeverityLevelEnum";
import { JISInfotextsModel } from "#sch/jis/models/JISInfotextsModel";
import { expect } from "chai";

describe("JISInfotextDepartureTransformation (integration)", () => {
    let transformation: JISInfotextDepartureTransformation;

    beforeEach(() => {
        transformation = OgPidContainer.resolve<JISInfotextDepartureTransformation>(
            OgPidToken.JISInfotextDepartureTransformation
        );
    });

    it("should transform JIS infotexts for departure", () => {
        const input = [
            {
                severity_level: InfotextSeverityLevel.Severe,
                display_type: "INLINE",
                active_period_start: new Date("2024-02-07 13:11:00.000+01"),
                active_period_end: new Date("2024-02-08 13:11:00.000+01"),
                description_text: {
                    cs: "Text 1",
                    en: "Text 2",
                },
                stops: [
                    {
                        stop_id: "U1131Z1P",
                    },
                ],
            },
        ];

        const result = transformation.transformArray(
            input.map((i) => ({ data: i as JISInfotextsModel, timeZone: "Europe/Prague" }))
        );

        expect(result).to.deep.equal([
            {
                display_type: InfotextDisplayType.Inline,
                text: "Text 1",
                text_en: "Text 2",
                related_stops: ["U1131Z1P"],
                valid_from: "2024-02-07T13:11:00+01:00",
                valid_to: "2024-02-08T13:11:00+01:00",
            },
        ]);
    });

    it("should transform JIS infotexts for departure (some values are null)", () => {
        const input = [
            {
                severity_level: InfotextSeverityLevel.Severe,
                display_type: "INLINE",
                active_period_start: new Date("2024-02-07 13:11:00.000+01"),
                active_period_end: null,
                description_text: {
                    cs: "Text 1",
                    en: null,
                },
                stops: [
                    {
                        stop_id: "U1131Z1P",
                    },
                ],
            },
        ];

        const result = transformation.transformArray(
            input.map((i) => ({ data: i as JISInfotextsModel, timeZone: "Europe/Prague" }))
        );

        expect(result).to.deep.equal([
            {
                display_type: InfotextDisplayType.Inline,
                text: "Text 1",
                text_en: null,
                related_stops: ["U1131Z1P"],
                valid_from: "2024-02-07T13:11:00+01:00",
                valid_to: null,
            },
        ]);
    });

    it("should transform JIS infotexts for departure (different timezone)", () => {
        const input = [
            {
                severity_level: "SEVERE",
                display_type: "INLINE",
                active_period_start: new Date("2024-02-07 13:11:00.000+01"),
                active_period_end: new Date("2024-02-08 13:11:00.000+01"),
                description_text: {
                    cs: "Text 1",
                    en: "Text 2",
                },
                stops: [
                    {
                        stop_id: "U1131Z1P",
                    },
                ],
            },
        ];

        const result = transformation.transformArray(
            input.map((i) => ({ data: i as JISInfotextsModel, timeZone: "Australia/Sydney" }))
        );

        expect(result).to.deep.equal([
            {
                display_type: InfotextDisplayType.Inline,
                text: "Text 1",
                text_en: "Text 2",
                related_stops: ["U1131Z1P"],
                valid_from: "2024-02-07T23:11:00+11:00",
                valid_to: "2024-02-08T23:11:00+11:00",
            },
        ]);
    });
});
