import { GTFSRouteTypeEnum } from "#helpers/RouteTypeEnums";
import { ITransferDeparture } from "#og/pid";
import { OgPidContainer } from "#og/pid/ioc/Di";
import { OgPidToken } from "#og/pid/ioc/OgPidToken";
import { TransferDepartureTransformation } from "#og/pid/service/transformations/TransferDepartureTransformation";
import { expect } from "chai";

describe("TransferDepartureTransformation (integration)", () => {
    let transformation: TransferDepartureTransformation;

    beforeEach(() => {
        transformation = OgPidContainer.resolve<TransferDepartureTransformation>(OgPidToken.TransferDepartureTransformation);
    });

    it("should transform transfer departures", () => {
        const input: ITransferDeparture[] = [
            {
                departure_datetime_real: new Date(),
                stop_headsign: "Benešov u Prahy",
                departure_datetime: new Date(),
                stop_id: "U461Z301",
                platform_code: null,
                trip_id: "1309_2547_231210",
                trip_headsign: "Benešov u Prahy",
                trip_short_name: "Os 2547",
                route_short_name: "S9",
                route_type: GTFSRouteTypeEnum.TRAIN,
                route_id: "L1309",
                is_canceled: false,
                "trip.cis_stop_platform_code": "2/3",
                is_delay_available: true,
            },
        ];
        const result = transformation.transformArray(input);
        expect(result).to.deep.equal([
            {
                departure_timestamp: {
                    minutes: "<1",
                },
                route: {
                    short_name: "S9",
                    type: GTFSRouteTypeEnum.TRAIN,
                },
                stop: {
                    platform_code: "2/3",
                },
                trip: {
                    headsign: "Benešov u Prahy",
                    id: "1309_2547_231210",
                },
            },
        ]);
    });
});
