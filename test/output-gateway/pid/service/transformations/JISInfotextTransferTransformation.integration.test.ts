import { InfotextDisplayType } from "#og/pid/domain/InfotextDisplayTypeEnum";
import { OgPidContainer } from "#og/pid/ioc/Di";
import { OgPidToken } from "#og/pid/ioc/OgPidToken";
import { JISInfotextTransferTransformation } from "#og/pid/service/transformations/JISInfotextTransferTransformation";
import { InfotextSeverityLevel } from "#og/shared/constants/jis/InfotextSeverityLevelEnum";
import { JISInfotextsModel } from "#sch/jis/models/JISInfotextsModel";
import { expect } from "chai";

describe("JISInfotextTransferTransformation (integration)", () => {
    let transformation: JISInfotextTransferTransformation;

    beforeEach(() => {
        transformation = OgPidContainer.resolve<JISInfotextTransferTransformation>(OgPidToken.JISInfotextTransferTransformation);
    });

    it("should transform JIS infotexts for transfer boards", () => {
        const input = [
            {
                severity_level: InfotextSeverityLevel.Severe,
                display_type: "INLINE",
                active_period_start: new Date("2024-02-07 13:11:00.000+01"),
                active_period_end: new Date("2024-02-08 13:11:00.000+01"),
                description_text: {
                    cs: "Text 1",
                    en: "Text 2",
                },
                stops: [
                    {
                        stop_id: "U1131Z1P",
                    },
                ],
            },
        ];

        const result = transformation.transformArray(input as JISInfotextsModel[]);
        expect(result).to.deep.equal([
            {
                display_type: InfotextDisplayType.Inline,
                text: "Text 1",
                text_en: "Text 2",
            },
        ]);
    });

    it("should transform JIS infotexts for transfer boards (some values are null)", () => {
        const input = [
            {
                severity_level: InfotextSeverityLevel.Severe,
                display_type: "INLINE",
                active_period_start: new Date("2024-02-07 13:11:00.000+01"),
                active_period_end: null,
                description_text: {
                    cs: "Text 1",
                    en: null,
                },
                stops: [
                    {
                        stop_id: "U1131Z1P",
                    },
                ],
            },
        ];

        const result = transformation.transformArray(input as JISInfotextsModel[]);
        expect(result).to.deep.equal([
            {
                display_type: InfotextDisplayType.Inline,
                text: "Text 1",
                text_en: null,
            },
        ]);
    });
});
