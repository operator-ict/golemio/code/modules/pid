import { InfotextDisplayType } from "#og/pid/domain/InfotextDisplayTypeEnum";
import { InfotextPriority } from "#og/pid/domain/InfotextPriorityEnum";
import { OgPidContainer } from "#og/pid/ioc/Di";
import { OgPidToken } from "#og/pid/ioc/OgPidToken";
import { JISInfotextOverviewTransformation } from "#og/pid/service/transformations/JISInfotextOverviewTransformation";
import { InfotextSeverityLevel } from "#og/shared/constants/jis/InfotextSeverityLevelEnum";
import { JISInfotextsModel } from "#sch/jis/models/JISInfotextsModel";
import { expect } from "chai";

describe("JISInfotextOverviewTransformation (integration)", () => {
    let transformation: JISInfotextOverviewTransformation;

    beforeEach(() => {
        transformation = OgPidContainer.resolve<JISInfotextOverviewTransformation>(OgPidToken.JISInfotextOverviewTransformation);
    });

    it("should transform JIS infotexts for overview", () => {
        const input = [
            {
                id: "40507cee-6468-488e-ba78-e36d9c20e67c",
                severity_level: InfotextSeverityLevel.Severe,
                display_type: "INLINE",
                active_period_start: new Date("2024-02-07 13:11:00.000+01"),
                active_period_end: new Date("2024-02-08 13:11:00.000+01"),
                description_text: {
                    cs: "Text 1",
                    en: "Text 2",
                },
                stops: [
                    {
                        stop_id: "U1131Z1P",
                        stop_name: "Pod Chodovem",
                        platform_code: "A",
                    },
                ],
            },
        ];
        const result = transformation.transformArray(input as JISInfotextsModel[]);
        expect(result).to.deep.equal([
            {
                id: "40507cee-6468-488e-ba78-e36d9c20e67c",
                priority: InfotextPriority.High,
                display_type: InfotextDisplayType.Inline,
                text: "Text 1",
                text_en: "Text 2",
                related_stops: [
                    {
                        id: "U1131Z1P",
                        name: "Pod Chodovem",
                        platform_code: "A",
                    },
                ],
                valid_from: "2024-02-07T13:11:00+01:00",
                valid_to: "2024-02-08T13:11:00+01:00",
            },
        ]);
    });

    it("should transform JIS infotexts for overview (some values are null)", () => {
        const input = [
            {
                id: "40507cee-6468-488e-ba78-e36d9c20e67c",
                severity_level: InfotextSeverityLevel.Severe,
                display_type: "INLINE",
                active_period_start: new Date("2024-02-07 13:11:00.000+01"),
                active_period_end: null,
                description_text: {
                    cs: "Text 1",
                    en: null,
                },
                stops: [
                    {
                        stop_id: "U1131Z1P",
                        stop_name: "Pod Chodovem",
                        platform_code: null,
                    },
                ],
            },
        ];
        const result = transformation.transformArray(input as unknown as JISInfotextsModel[]);
        expect(result).to.deep.equal([
            {
                id: "40507cee-6468-488e-ba78-e36d9c20e67c",
                priority: InfotextPriority.High,
                display_type: InfotextDisplayType.Inline,
                text: "Text 1",
                text_en: null,
                related_stops: [
                    {
                        id: "U1131Z1P",
                        name: "Pod Chodovem",
                        platform_code: null,
                    },
                ],
                valid_from: "2024-02-07T13:11:00+01:00",
                valid_to: null,
            },
        ]);
    });
});
