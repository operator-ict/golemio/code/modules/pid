import { InfotextDisplayType } from "#og/pid/domain/InfotextDisplayTypeEnum";
import { DisplayTypeMapper } from "#og/pid/service/helpers/DisplayTypeMapper";
import { expect } from "chai";

describe("DisplayTypeMapper (integration)", () => {
    it("should map INLINE input display type from DB to output display type for API", () => {
        const input = "INLINE";
        const result = DisplayTypeMapper.mapInputToOutputDisplayType(input);
        expect(result).to.equal(InfotextDisplayType.Inline);
    });

    it("should map GENERAL input display type from DB to output display type for API", () => {
        const input = "GENERAL";
        const result = DisplayTypeMapper.mapInputToOutputDisplayType(input);
        expect(result).to.equal(InfotextDisplayType.General);
    });

    it("should throw an error for unknown display type", () => {
        const input = "UNKNOWN";
        expect(() => DisplayTypeMapper.mapInputToOutputDisplayType(input)).to.throw(`Unknown display type: ${input}`);
    });
});
