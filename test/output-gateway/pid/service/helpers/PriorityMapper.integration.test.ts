import { InfotextPriority } from "#og/pid/domain/InfotextPriorityEnum";
import { PriorityMapper } from "#og/pid/service/helpers/PriorityMapper";
import { InfotextSeverityLevel } from "#og/shared/constants/jis/InfotextSeverityLevelEnum";
import { expect } from "chai";

describe("PriorityMapper (integration)", () => {
    it("should map INFO input severity level from DB to output priority for API", () => {
        const input = InfotextSeverityLevel.Info;
        const result = PriorityMapper.mapInputToOutputPriority(input);
        expect(result).to.equal(InfotextPriority.Low);
    });

    it("should map WARNING input severity level from DB to output priority for API", () => {
        const input = InfotextSeverityLevel.Warning;
        const result = PriorityMapper.mapInputToOutputPriority(input);
        expect(result).to.equal(InfotextPriority.Normal);
    });

    it("should map SEVERE input severity level from DB to output priority for API", () => {
        const input = InfotextSeverityLevel.Severe;
        const result = PriorityMapper.mapInputToOutputPriority(input);
        expect(result).to.equal(InfotextPriority.High);
    });

    it("should throw an error for unknown severity level", () => {
        const input = "UNKNOWN";
        expect(() => PriorityMapper.mapInputToOutputPriority(input as InfotextSeverityLevel)).to.throw(
            `Unknown severity level: ${input}`
        );
    });
});
