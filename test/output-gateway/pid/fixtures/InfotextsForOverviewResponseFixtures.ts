import { InfotextDisplayType } from "#og/pid/domain/InfotextDisplayTypeEnum";
import { IInfotextOverviewOutputDto } from "#og/pid/domain/InfotextInterfaces";
import { InfotextPriority } from "#og/pid/domain/InfotextPriorityEnum";

export const infotextsForOverviewResponseFixture: IInfotextOverviewOutputDto[] = [
    {
        priority: InfotextPriority.High,
        display_type: InfotextDisplayType.Inline,
        text: "Nejede to.",
        text_en: null,
        related_stops: [
            {
                id: "U1131Z1P",
                name: "Pod Chodovem",
                platform_code: "A",
            },
        ],
        valid_from: "2024-12-04T13:54:45.109+01:00",
        valid_to: null,
        id: "cdcc2d15-2cdc-4205-b82a-5cc819a79738",
    },
];
