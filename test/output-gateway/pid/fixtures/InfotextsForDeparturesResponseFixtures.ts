import { InfotextDisplayType } from "#og/pid/domain/InfotextDisplayTypeEnum";
import { IInfotextDepartureOutputDto } from "#og/pid/domain/InfotextInterfaces";

export const infotextsForDeparturesResponseFixture: IInfotextDepartureOutputDto[] = [
    {
        display_type: InfotextDisplayType.Inline,
        text: "Nejede to.",
        text_en: null,
        related_stops: ["U1131Z1P"],
        valid_from: "2024-12-04T13:54:45.109+01:00",
        valid_to: null,
    },
];
