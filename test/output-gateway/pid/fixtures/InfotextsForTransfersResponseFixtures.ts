import { InfotextDisplayType } from "#og/pid/domain/InfotextDisplayTypeEnum";
import { IInfotextTransferOutputDto } from "#og/pid/domain/InfotextInterfaces";

export const infotextsForTransportsResponseFixture: IInfotextTransferOutputDto[] = [
    {
        display_type: InfotextDisplayType.Inline,
        text: "Nejede to.",
        text_en: null,
    },
];
