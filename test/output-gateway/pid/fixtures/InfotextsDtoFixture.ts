import { InfotextSeverityLevel } from "#og/shared/constants/jis/InfotextSeverityLevelEnum";
import { IJISInfotext } from "#sch/jis/models/interfaces";

export const infotextsDtoFixture: IJISInfotext[] = [
    {
        id: "cdcc2d15-2cdc-4205-b82a-5cc819a79738",
        severity_level: InfotextSeverityLevel.Severe,
        display_type: "INLINE",
        active_period_start: new Date("2023-03-26T22:00:00.000Z"),
        active_period_end: null,
        description_text: { cs: "Nejede to." },

        created_timestamp: new Date("2024-12-04T12:54:45.109Z"),
        updated_timestamp: new Date("2024-12-04T12:54:45.109Z"),
    },
];
