import { DepartureFilter, DepartureMode, DepartureOrder, PIDDepartureBoardsModel } from "#og/pid";
import { InfotextDisplayType } from "#og/pid/domain/InfotextDisplayTypeEnum";
import moment from "@golemio/core/dist/shared/moment-timezone";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";

chai.use(chaiAsPromised);

describe("PIDDepartureBoardsModel Infotexts", async () => {
    // Basic configuration: create a sinon sandbox for testing
    let sandbox: SinonSandbox;
    let model: PIDDepartureBoardsModel;
    const todayYMD = moment().format("YYYY-MM-DD");
    let clock: any = null;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    before(() => {
        model = new PIDDepartureBoardsModel();
        clock = sinon.useFakeTimers({
            now: moment.tz(`${todayYMD}T10:37:00`, "Europe/Prague").valueOf(),
            shouldAdvanceTime: true,
        });
    });

    after(() => {
        clock.uninstall();
    });

    it("should respond with infotexts for U663Z2P", async () => {
        const results = await model.getAll({
            gtfsIds: ["U663Z2P"],
            total: 1,
            limit: 1,
            mode: DepartureMode.DEPARTURES,
            order: DepartureOrder.REAL,
            filter: DepartureFilter.NONE,
            skip: [],
            offset: 0,
            minutesBefore: 10,
            minutesAfter: 10,
            timezone: "Europe/Prague",
        });

        expect(results.infotexts.length).to.be.equal(1);
        expect(results.infotexts[0].related_stops.length).to.be.equal(1);
        expect(results.infotexts[0].display_type).to.be.equal(InfotextDisplayType.Inline);
        expect(results.infotexts[0].valid_from).to.be.equal("2024-03-05T07:30:00+01:00");
        expect(results.infotexts[0].valid_to).to.be.null;
    });

    it("should respond with infotexts for U663Z2P (different timezone)", async () => {
        const results = await model.getAll({
            gtfsIds: ["U663Z2P"],
            total: 1,
            limit: 1,
            mode: DepartureMode.DEPARTURES,
            order: DepartureOrder.REAL,
            filter: DepartureFilter.NONE,
            skip: [],
            offset: 0,
            minutesBefore: 10,
            minutesAfter: 10,
            timezone: "Australia/Sydney",
        });

        expect(results.infotexts.length).to.be.equal(1);
        expect(results.infotexts[0].valid_from).to.be.equal("2024-03-05T17:30:00+11:00");
        expect(results.infotexts[0].valid_to).to.be.equal(null);
    });

    it("should respond with infotexts for U21Z1P, U21Z2P", async () => {
        const results = await model.getAll({
            gtfsIds: ["U21Z1P", "U21Z2P"],
            total: 1,
            limit: 1,
            mode: DepartureMode.DEPARTURES,
            order: DepartureOrder.REAL,
            filter: DepartureFilter.NONE,
            skip: [],
            offset: 0,
            minutesBefore: 10,
            minutesAfter: 10,
            timezone: "Europe/Prague",
        });

        expect(results.infotexts.length).to.be.equal(2);
        expect(results.infotexts[0].related_stops.length).to.be.equal(1);
        expect(results.infotexts[0].display_type).to.be.equal(InfotextDisplayType.Inline);
        expect(results.infotexts[1].related_stops.length).to.be.equal(1);
        expect(results.infotexts[1].display_type).to.be.equal(InfotextDisplayType.Inline);
    });
});
