import { OgPidToken } from "#og/pid/ioc/OgPidToken";
import { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { DependencyContainer, container as RootContainer } from "@golemio/core/dist/shared/tsyringe";
import { expect } from "chai";
import sinon, { SinonSandbox, SinonStub } from "sinon";

type V3TransferBoardsController = import("#og/pid/controllers/v3/V3TransferBoardsController").V3TransferBoardsController;

describe("V3TransferBoardsController (unit)", () => {
    let sandbox: SinonSandbox;
    let container: DependencyContainer;

    // Mocks
    let getStopsMock: SinonStub;
    let getInfotextsMock: SinonStub;
    let getDeparturesMock: SinonStub;

    const createDependencyContainer = async (): Promise<V3TransferBoardsController> => {
        // TODO remove this after solving circular dependency in PIDDepartureBoardsModel.ts that is causing this import to fail
        // Error: Cannot register a type name as a singleton without a "to" token
        const { V3TransferBoardsController } = await import("#og/pid/controllers/v3/V3TransferBoardsController");

        return container
            .registerSingleton(
                OgPidToken.StopFacade,
                class DummyStopFacade {
                    public getStopIdsForTransferBoards = getStopsMock;
                }
            )
            .registerSingleton(
                OgPidToken.InfotextFacade,
                class DummyInfotextFacade {
                    public getInfotextsForTransferBoards = getInfotextsMock;
                }
            )
            .registerSingleton(
                OgPidToken.TransferFacade,
                class DummyTransferFacade {
                    public getTransferDepartures = getDeparturesMock;
                }
            )
            .registerSingleton(OgPidToken.V3TransferBoardsController, V3TransferBoardsController)
            .resolve(OgPidToken.V3TransferBoardsController);
    };

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        getStopsMock = sandbox.stub();
        getInfotextsMock = sandbox.stub();
        getDeparturesMock = sandbox.stub();

        container = RootContainer.createChildContainer();
    });

    afterEach(() => {
        sandbox.restore();
        container.clearInstances();
    });

    describe("getTransferDepartures", () => {
        it("should return 400 if cisId is not a valid string", async () => {
            const controller = await createDependencyContainer();
            const req = { query: { cisId: 123, tripNumber: "123" } } as unknown as Request;
            const res = { status: sandbox.stub().returnsThis(), json: sandbox.stub() } as unknown as Response;
            const nextMock = sandbox.stub();

            await controller.getTransferDepartures(req, res, nextMock as unknown as NextFunction);

            expect(nextMock.callCount).to.equal(1);
            expect(nextMock.getCall(0).args[0]).to.be.an.instanceOf(GeneralError);
            expect(nextMock.getCall(0).args[0]).to.have.property("message").that.equals("cisId is not valid");
            expect(nextMock.getCall(0).args[0]).to.have.property("status").that.equals(400);
        });

        it("should return 400 if tripNumber is not a valid string", async () => {
            const controller = await createDependencyContainer();
            const req = { query: { cisId: "123", tripNumber: 123 } } as unknown as Request;
            const res = { status: sandbox.stub().returnsThis(), json: sandbox.stub() } as unknown as Response;
            const nextMock = sandbox.stub();

            await controller.getTransferDepartures(req, res, nextMock as unknown as NextFunction);

            expect(nextMock.callCount).to.equal(1);
            expect(nextMock.getCall(0).args[0]).to.be.an.instanceOf(GeneralError);
            expect(nextMock.getCall(0).args[0]).to.have.property("message").that.equals("tripNumber is not valid");
            expect(nextMock.getCall(0).args[0]).to.have.property("status").that.equals(400);
        });

        it("should return 404 if no stops are found", async () => {
            getStopsMock.resolves([]);
            const controller = await createDependencyContainer();
            const req = { query: { cisId: "123", tripNumber: "123" } } as unknown as Request;
            const res = { status: sandbox.stub().returnsThis(), json: sandbox.stub() } as unknown as Response;

            await controller.getTransferDepartures(req, res, sandbox.stub() as unknown as NextFunction);

            expect((res.status as SinonStub).getCall(0).args[0]).to.equal(404);
            expect((res.json as SinonStub).getCall(0).args[0]).to.deep.equal({
                departures: [],
                infotexts: [],
            });
        });

        it("should call next with error if stopFacade throws an error", async () => {
            getStopsMock.rejects(new Error("test"));
            const controller = await createDependencyContainer();
            const req = { query: { cisId: "123", tripNumber: "123" } } as unknown as Request;
            const res = { status: sandbox.stub().returnsThis(), json: sandbox.stub() } as unknown as Response;
            const next = sandbox.stub();

            await controller.getTransferDepartures(req, res, next);

            expect(next.getCall(0).args[0]).to.be.an.instanceOf(Error);
        });

        it("should call next with error if transferFacade throws an error", async () => {
            getStopsMock.resolves(["123"]);
            getDeparturesMock.rejects(new Error("test"));
            const controller = await createDependencyContainer();
            const req = { query: { cisId: "123", tripNumber: "123" } } as unknown as Request;
            const res = { status: sandbox.stub().returnsThis(), json: sandbox.stub() } as unknown as Response;
            const next = sandbox.stub();

            await controller.getTransferDepartures(req, res, next);

            expect(next.getCall(0).args[0]).to.be.an.instanceOf(Error);
        });

        it("should call next with error if infotextFacade throws an error", async () => {
            getStopsMock.resolves(["123"]);
            getDeparturesMock.resolves([]);
            getInfotextsMock.rejects(new Error("test"));
            const controller = await createDependencyContainer();
            const req = { query: { cisId: "123", tripNumber: "123" } } as unknown as Request;
            const res = { status: sandbox.stub().returnsThis(), json: sandbox.stub() } as unknown as Response;
            const next = sandbox.stub();

            await controller.getTransferDepartures(req, res, next);

            expect(next.getCall(0).args[0]).to.be.an.instanceOf(Error);
        });

        it("should return 200 with departures and infotexts", async () => {
            getStopsMock.resolves(["123"]);
            getDeparturesMock.resolves([]);
            getInfotextsMock.resolves([]);
            const controller = await createDependencyContainer();
            const req = { query: { cisId: "123", tripNumber: "123" } } as unknown as Request;
            const res = { status: sandbox.stub().returnsThis(), json: sandbox.stub() } as unknown as Response;

            await controller.getTransferDepartures(req, res, sandbox.stub() as unknown as NextFunction);

            expect((res.status as SinonStub).getCall(0).args[0]).to.equal(200);
            expect((res.json as SinonStub).getCall(0).args[0]).to.deep.equal({ departures: [], infotexts: [] });
        });

        it("should return 200 with timeFrom", async () => {
            getStopsMock.resolves(["123"]);
            getDeparturesMock.resolves([]);
            getInfotextsMock.resolves([]);
            const controller = await createDependencyContainer();
            const req = { query: { cisId: "123", tripNumber: "123", timeFrom: "2024-01-01T00:00:00" } } as unknown as Request;
            const res = { status: sandbox.stub().returnsThis(), json: sandbox.stub() } as unknown as Response;

            await controller.getTransferDepartures(req, res, sandbox.stub() as unknown as NextFunction);

            expect((res.status as SinonStub).getCall(0).args[0]).to.equal(200);
            expect((res.json as SinonStub).getCall(0).args[0]).to.deep.equal({ departures: [], infotexts: [] });
        });
    });
});
