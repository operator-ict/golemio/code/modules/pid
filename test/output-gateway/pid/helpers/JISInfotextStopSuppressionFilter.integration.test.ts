import { JISInfotextStopSuppressionFilter } from "#og/pid/helpers/JISInfotextStopSuppressionFilter";
import { OgPidToken } from "#og/pid/ioc/OgPidToken";
import { InfotextSeverityLevel } from "#og/shared/constants/jis/InfotextSeverityLevelEnum";
import { JISInfotextsModel } from "#sch/jis/models/JISInfotextsModel";
import { StopDto } from "#sch/ropid-gtfs/models/StopDto";
import { container } from "@golemio/core/dist/shared/tsyringe";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";

describe("JISInfotextStopSuppressionFilter (integration)", () => {
    let sandbox: SinonSandbox;

    const createDependencyContainer = (): JISInfotextStopSuppressionFilter => {
        return container
            .registerSingleton(OgPidToken.JISInfotextStopSuppressionFilter, JISInfotextStopSuppressionFilter)
            .resolve(OgPidToken.JISInfotextStopSuppressionFilter);
    };

    const createDataValueMock = (stopId: string): StopDto => {
        return {
            getDataValue: (_key: string) => stopId,
            stop_id: stopId,
        } as unknown as StopDto;
    };

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        container.clearInstances();
        sandbox.restore();
    });

    it("should remove stops from lower severities", async () => {
        const list: Array<Partial<JISInfotextsModel>> = [
            {
                id: "1",
                severity_level: InfotextSeverityLevel.Severe,
                stops: [createDataValueMock("zas1"), createDataValueMock("zas2")],
            },
            {
                id: "2",
                severity_level: InfotextSeverityLevel.Severe,
                stops: [createDataValueMock("zas2"), createDataValueMock("zas3"), createDataValueMock("zas4")],
            },
            {
                id: "3",
                severity_level: InfotextSeverityLevel.Warning,
                stops: [createDataValueMock("zas1"), createDataValueMock("zas5")],
            },
            {
                id: "4",
                severity_level: InfotextSeverityLevel.Warning,
                stops: [createDataValueMock("zas2"), createDataValueMock("zas3")],
            },
            {
                id: "5",
                severity_level: InfotextSeverityLevel.Info,
                stops: [createDataValueMock("zas3"), createDataValueMock("zas6")],
            },
            {
                id: "6",
                severity_level: InfotextSeverityLevel.Info,
                stops: [createDataValueMock("zas1"), createDataValueMock("zas5")],
            },
            {
                id: "7",
                severity_level: InfotextSeverityLevel.Info,
                stops: [createDataValueMock("zas7")],
            },
        ];

        const infotextFilter = createDependencyContainer();
        const filteredList = infotextFilter.filterBySeverityLevel(list as JISInfotextsModel[]);
        expect(filteredList).to.have.length(5);
        expect(JSON.parse(JSON.stringify(filteredList))).to.deep.equal([
            {
                id: "1",
                severity_level: InfotextSeverityLevel.Severe,
                stops: [{ stop_id: "zas1" }, { stop_id: "zas2" }],
            },
            {
                id: "2",
                severity_level: InfotextSeverityLevel.Severe,
                stops: [{ stop_id: "zas2" }, { stop_id: "zas3" }, { stop_id: "zas4" }],
            },
            {
                id: "3",
                severity_level: InfotextSeverityLevel.Warning,
                stops: [{ stop_id: "zas5" }],
            },
            {
                id: "5",
                severity_level: InfotextSeverityLevel.Info,
                stops: [{ stop_id: "zas6" }],
            },
            {
                id: "7",
                severity_level: InfotextSeverityLevel.Info,
                stops: [{ stop_id: "zas7" }],
            },
        ]);
    });
});
