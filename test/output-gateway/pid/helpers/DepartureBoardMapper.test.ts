import DepartureBoardMapper from "#og/pid/helpers/DepartureBoardMapper";
import { expect } from "chai";
import { DateTime } from "@golemio/core/dist/shared/luxon";

describe("DepartureBoardMapper", () => {
    it('calculateDepartureMinutes ~ "<1"', () => {
        expect(DepartureBoardMapper["calculateDepartureMinutes"](DateTime.now().plus({ seconds: 25 }).toJSDate())).equal("<1");
    });
    it('calculateDepartureMinutes ~ "1"', () => {
        expect(DepartureBoardMapper["calculateDepartureMinutes"](DateTime.now().plus({ seconds: 35 }).toJSDate())).equal("1");
        expect(DepartureBoardMapper["calculateDepartureMinutes"](DateTime.now().plus({ seconds: 60 }).toJSDate())).equal("1");
        expect(DepartureBoardMapper["calculateDepartureMinutes"](DateTime.now().plus({ seconds: 85 }).toJSDate())).equal("1");
    });
    it('calculateDepartureMinutes ~ "2"', () => {
        expect(DepartureBoardMapper["calculateDepartureMinutes"](DateTime.now().plus({ seconds: 95 }).toJSDate())).equal("2");
        expect(DepartureBoardMapper["calculateDepartureMinutes"](DateTime.now().plus({ seconds: 145 }).toJSDate())).equal("2");
    });
    it('calculateDepartureMinutes ~ "null"', () => {
        expect(DepartureBoardMapper["calculateDepartureMinutes"](null)).equal(null);
    });
    it("calculateDepartureMinutes unexpected predicted format", () => {
        expect(DepartureBoardMapper["calculateDepartureMinutes"]("2022-05-23T00:00:00Z" as any)).equal(null);
    });
    it("calculateDepartureMinutes predicted time in past", () => {
        expect(DepartureBoardMapper["calculateDepartureMinutes"](DateTime.now().minus({ days: 2 }).toJSDate())).equal("<1");
    });
});
