import { GTFSRouteTypeEnum } from "#helpers/RouteTypeEnums";
import { IPIDDeparture, ITransferDeparture } from "#og/pid";
import { SkipHelper } from "#og/pid/models/helpers/SkipHelper";
import { IScheduleDto } from "#sch/vehicle-positions/redis/interfaces/IGtfsRunTripCacheDto";
import { expect } from "chai";

describe("SkipHelper (unit)", () => {
    describe("isMatchingTripNumber", () => {
        it("should return false if trip_short_name is missing", () => {
            const departure = {} as ITransferDeparture;
            const tripNumber = "1234";
            const result = SkipHelper.isMatchingTripNumber(departure, tripNumber);
            expect(result).to.be.false;
        });

        it("should return false if trip_short_name does not match the trip number (1234)", () => {
            const departure = { trip_short_name: "Os 1111" } as ITransferDeparture;
            const tripNumber = "5678";
            const result = SkipHelper.isMatchingTripNumber(departure, tripNumber);
            expect(result).to.be.false;
        });

        it("should return false if trip_short_name does not match the trip number (234)", () => {
            const departure = { trip_short_name: "Os 1111" } as ITransferDeparture;
            const tripNumber = "234";
            const result = SkipHelper.isMatchingTripNumber(departure, tripNumber);
            expect(result).to.be.false;
        });

        it("should return true if trip_short_name matches the trip number", () => {
            const departure = { trip_short_name: "Os 1111" } as ITransferDeparture;
            const tripNumber = "1111";
            const result = SkipHelper.isMatchingTripNumber(departure, tripNumber);
            expect(result).to.be.true;
        });

        it("should return true if trip_short_name (Os 1111 B) matches the trip number", () => {
            const departure = { trip_short_name: "Os 1111 B" } as ITransferDeparture;
            const tripNumber = "1111";
            const result = SkipHelper.isMatchingTripNumber(departure, tripNumber);
            expect(result).to.be.true;
        });

        it("should return true if trip_short_name (Rx 1/1111 A) matches the trip number", () => {
            const departure = { trip_short_name: "Rx 1/1111 A" } as ITransferDeparture;
            const tripNumber = "1111";
            const result = SkipHelper.isMatchingTripNumber(departure, tripNumber);
            expect(result).to.be.true;
        });
    });

    describe("hasNoDelayInfo", () => {
        it("should return true if is_delay_available is false", () => {
            const departure = { is_delay_available: false } as IPIDDeparture;
            const result = SkipHelper.hasNoDelayInfo(departure);
            expect(result).to.be.true;
        });

        it("should return false if is_delay_available is true", () => {
            const departure = { is_delay_available: true } as IPIDDeparture;
            const result = SkipHelper.hasNoDelayInfo(departure);
            expect(result).to.be.false;
        });
    });

    describe("isTripCanceled", () => {
        it("should return true if trip is canceled", () => {
            const departure = { is_canceled: true } as IPIDDeparture;
            const result = SkipHelper.isTripCanceled(departure);
            expect(result).to.be.true;
        });

        it("should return false if trip is not canceled", () => {
            const departure = { is_canceled: false } as IPIDDeparture;
            const result = SkipHelper.isTripCanceled(departure);
            expect(result).to.be.false;
        });
    });

    describe("isVehicleAtStop", () => {
        it("should return false if arrival_datetime is missing", () => {
            const departure = {} as IPIDDeparture;
            const result = SkipHelper.isVehicleAtStop(departure);
            expect(result).to.be.false;
        });

        it("should return true if the vehicle is at stop", () => {
            const departure = {
                arrival_datetime: new Date(),
                stop_sequence: 1,
                "trip.last_position.this_stop_sequence": 1,
            } as IPIDDeparture;
            const result = SkipHelper.isVehicleAtStop(departure);
            expect(result).to.be.true;
        });

        it("should return true if the vehicle is past the stop", () => {
            const departure = {
                arrival_datetime: new Date(),
                stop_sequence: 1,
                "trip.last_position.this_stop_sequence": null,
                "trip.last_position.last_stop_sequence": 1,
            } as IPIDDeparture;

            const result = SkipHelper.isVehicleAtStop(departure);
            expect(result).to.be.true;
        });

        it("should return false if the vehicle is not at stop (last_stop_sequence is null)", () => {
            const departure = {
                arrival_datetime: new Date(),
                stop_sequence: 1,
                "trip.last_position.this_stop_sequence": 2,
                "trip.last_position.last_stop_sequence": null,
            } as IPIDDeparture;

            const result = SkipHelper.isVehicleAtStop(departure);
            expect(result).to.be.false;
        });

        it("should return false if the vehicle is not at stop (stop_sequence is greater than last_stop_sequence)", () => {
            const departure = {
                arrival_datetime: new Date(),
                stop_sequence: 2,
                "trip.last_position.this_stop_sequence": 1,
                "trip.last_position.last_stop_sequence": 1,
            } as IPIDDeparture;

            const result = SkipHelper.isVehicleAtStop(departure);
            expect(result).to.be.false;
        });
    });

    describe("isVehicleMissing", () => {
        it("should return false if start_timestamp is missing", () => {
            const departure = {} as IPIDDeparture;
            const runScheduleMap = new Map();
            const untrackedTrips = new Set<string>();

            const result = SkipHelper.isVehicleMissing(departure, runScheduleMap, untrackedTrips);
            expect(result).to.be.false;
        });

        it("should return false if run_number is null", () => {
            const departure = {
                "trip.start_timestamp": "2025-01-13T04:36:00+01:00",
                run_number: null,
            } as IPIDDeparture;
            const runScheduleMap = new Map();
            const untrackedTrips = new Set<string>();

            const result = SkipHelper.isVehicleMissing(departure, runScheduleMap, untrackedTrips);
            expect(result).to.be.false;
        });

        it("should return false if origin_route_name is null", () => {
            const departure = {
                "trip.start_timestamp": "2025-01-13T04:36:00+01:00",
                run_number: 3,
                route_id: null,
            } as IPIDDeparture;
            const runScheduleMap = new Map();
            const untrackedTrips = new Set<string>();

            const result = SkipHelper.isVehicleMissing(departure, runScheduleMap, untrackedTrips);
            expect(result).to.be.false;
        });

        it("should return false if the vehicle has delay information", () => {
            const departure = {
                "trip.start_timestamp": "2025-01-13T04:36:00+01:00",
                run_number: 3,
                route_id: "L1",
                is_delay_available: true,
            } as IPIDDeparture;
            const runScheduleMap = new Map();
            const untrackedTrips = new Set<string>();

            const result = SkipHelper.isVehicleMissing(departure, runScheduleMap, untrackedTrips);
            expect(result).to.be.false;
        });

        it("should return false if departure is too far in the future", () => {
            const departure = {
                "trip.start_timestamp": "2025-01-13T04:36:00+01:00",
                run_number: 3,
                route_id: "L1",
                is_delay_available: false,
                route_type: GTFSRouteTypeEnum.TRAM,
            } as IPIDDeparture;
            const runScheduleMap = new Map();
            const untrackedTrips = new Set<string>();
            const nowDate = new Date("2025-01-13T04:00:00+01:00");

            const result = SkipHelper.isVehicleMissing(departure, runScheduleMap, untrackedTrips, nowDate);
            expect(result).to.be.false;
        });

        it("should return true if the trip should have already started", () => {
            const departure = {
                "trip.start_timestamp": "2025-01-13T04:36:00+01:00",
                run_number: 3,
                route_id: "L1",
                is_delay_available: false,
                route_type: GTFSRouteTypeEnum.TRAM,
            } as IPIDDeparture;
            const runScheduleMap = new Map();
            const untrackedTrips = new Set<string>();
            const nowDate = new Date("2025-01-13T05:00:00+01:00");

            const result = SkipHelper.isVehicleMissing(departure, runScheduleMap, untrackedTrips, nowDate);
            expect(result).to.be.true;
        });

        it("should return false if runSchedule is missing", () => {
            const departure = {
                "trip.start_timestamp": "2025-01-13T04:36:00+01:00",
                run_number: 3,
                route_id: "L1",
                is_delay_available: false,
                route_type: GTFSRouteTypeEnum.TRAM,
            } as IPIDDeparture;
            const runScheduleMap = new Map();
            const untrackedTrips = new Set<string>();
            const nowDate = new Date("2025-01-13T04:33:00+01:00");

            const result = SkipHelper.isVehicleMissing(departure, runScheduleMap, untrackedTrips, nowDate);
            expect(result).to.be.false;
        });

        it("should return false if this is the first trip in the run (missing schedule)", () => {
            const departure = {
                "trip.start_timestamp": "2025-01-13T04:36:00+01:00",
                run_number: 3,
                route_id: "L1",
                is_delay_available: false,
                route_type: GTFSRouteTypeEnum.TRAM,
                trip_id: "1",
            } as IPIDDeparture;
            const runScheduleMap = new Map<string, IScheduleDto[]>([
                ["10_3", [{ trip_id: "1", start_timestamp: "2025-01-13T04:36:00+01:00" }] as IScheduleDto[]],
            ]);
            const untrackedTrips = new Set<string>();
            const nowDate = new Date("2025-01-13T04:33:00+01:00");

            const result = SkipHelper.isVehicleMissing(departure, runScheduleMap, untrackedTrips, nowDate);
            expect(result).to.be.false;
        });

        it("should return false if this is the first trip in the run (missing run trip)", () => {
            const departure = {
                "trip.start_timestamp": "2025-01-13T04:36:00+01:00",
                run_number: 3,
                route_id: "L1",
                is_delay_available: false,
                route_type: GTFSRouteTypeEnum.TRAM,
                trip_id: "1",
            } as IPIDDeparture;
            const runScheduleMap = new Map<string, IScheduleDto[]>([
                ["1_3", [{ trip_id: "2", start_timestamp: "2025-01-13T04:36:00+01:00" }] as IScheduleDto[]],
            ]);
            const untrackedTrips = new Set<string>();
            const nowDate = new Date("2025-01-13T04:33:00+01:00");

            const result = SkipHelper.isVehicleMissing(departure, runScheduleMap, untrackedTrips, nowDate);
            expect(result).to.be.false;
        });

        it("should return false if this is the first trip in the run", () => {
            const departure = {
                "trip.start_timestamp": "2025-01-13T04:36:00+01:00",
                run_number: 3,
                route_id: "L1",
                is_delay_available: false,
                route_type: GTFSRouteTypeEnum.TRAM,
                trip_id: "1",
            } as IPIDDeparture;
            const runScheduleMap = new Map<string, IScheduleDto[]>([
                ["1_3", [{ trip_id: "1", start_timestamp: "2025-01-13T04:36:00+01:00" }] as IScheduleDto[]],
            ]);
            const untrackedTrips = new Set<string>();
            const nowDate = new Date("2025-01-13T04:33:00+01:00");

            const result = SkipHelper.isVehicleMissing(departure, runScheduleMap, untrackedTrips, nowDate);
            expect(result).to.be.false;
        });

        it("should return false if there is a significant gap after the previous trip", () => {
            const departure = {
                "trip.start_timestamp": "2025-01-13T06:40:00+01:00",
                run_number: 3,
                route_id: "L1",
                is_delay_available: false,
                route_type: GTFSRouteTypeEnum.TRAM,
                trip_id: "2",
            } as IPIDDeparture;
            const runScheduleMap = new Map<string, IScheduleDto[]>([
                [
                    "1_3",
                    [
                        {
                            trip_id: "1",
                            start_timestamp: "2025-01-13T04:36:00+01:00",
                            end_timestamp: "2025-01-13T04:38:00+01:00",
                        },
                        {
                            trip_id: "2",
                            start_timestamp: "2025-01-13T06:40:00+01:00",
                            end_timestamp: "2025-01-13T06:42:00+01:00",
                        },
                    ] as IScheduleDto[],
                ],
            ]);
            const untrackedTrips = new Set<string>();
            const nowDate = new Date("2025-01-13T06:30:00+01:00");

            const result = SkipHelper.isVehicleMissing(departure, runScheduleMap, untrackedTrips, nowDate);
            expect(result).to.be.false;
        });

        it("should return false if the previous trip has delay information available", () => {
            const departure = {
                "trip.start_timestamp": "2025-01-13T04:50:00+01:00",
                run_number: 3,
                route_id: "L1",
                is_delay_available: false,
                route_type: GTFSRouteTypeEnum.TRAM,
                trip_id: "2",
            } as IPIDDeparture;
            const runScheduleMap = new Map<string, IScheduleDto[]>([
                [
                    "1_3",
                    [
                        {
                            trip_id: "1",
                            start_timestamp: "2025-01-13T04:36:00+01:00",
                            end_timestamp: "2025-01-13T04:38:00+01:00",
                        },
                        {
                            trip_id: "2",
                            start_timestamp: "2025-01-13T04:50:00+01:00",
                            end_timestamp: "2025-01-13T04:55:00+01:00",
                        },
                    ] as IScheduleDto[],
                ],
            ]);

            const untrackedTrips = new Set<string>(["3"]);
            const nowDate = new Date("2025-01-13T04:45:00+01:00");

            const result = SkipHelper.isVehicleMissing(departure, runScheduleMap, untrackedTrips, nowDate);
            expect(result).to.be.false;
        });

        it("should return true if the previous trip should have already started", () => {
            const departure = {
                "trip.start_timestamp": "2025-01-13T04:50:00+01:00",
                run_number: 3,
                route_id: "L1",
                is_delay_available: false,
                route_type: GTFSRouteTypeEnum.TRAM,
                trip_id: "2",
            } as IPIDDeparture;
            const runScheduleMap = new Map<string, IScheduleDto[]>([
                [
                    "1_3",
                    [
                        {
                            trip_id: "1",
                            start_timestamp: "2025-01-13T04:36:00+01:00",
                            end_timestamp: "2025-01-13T04:38:00+01:00",
                        },
                        {
                            trip_id: "2",
                            start_timestamp: "2025-01-13T04:50:00+01:00",
                            end_timestamp: "2025-01-13T04:55:00+01:00",
                        },
                    ] as IScheduleDto[],
                ],
            ]);

            const untrackedTrips = new Set<string>(["1"]);
            const nowDate = new Date("2025-01-13T04:45:00+01:00");

            const result = SkipHelper.isVehicleMissing(departure, runScheduleMap, untrackedTrips, nowDate);
            expect(result).to.be.true;
        });

        it("should return false if the previous trip is the first trip in the run", () => {
            const departure = {
                "trip.start_timestamp": "2025-01-13T04:50:00+01:00",
                run_number: 3,
                route_id: "L1",
                is_delay_available: false,
                route_type: GTFSRouteTypeEnum.TRAM,
                trip_id: "2",
            } as IPIDDeparture;
            const runScheduleMap = new Map<string, IScheduleDto[]>([
                [
                    "1_3",
                    [
                        {
                            trip_id: "1",
                            start_timestamp: "2025-01-13T04:36:00+01:00",
                            end_timestamp: "2025-01-13T04:38:00+01:00",
                        },
                        {
                            trip_id: "2",
                            start_timestamp: "2025-01-13T04:50:00+01:00",
                            end_timestamp: "2025-01-13T04:55:00+01:00",
                        },
                    ] as IScheduleDto[],
                ],
            ]);

            const untrackedTrips = new Set<string>(["1"]);
            const nowDate = new Date("2025-01-13T04:35:00+01:00");

            const result = SkipHelper.isVehicleMissing(departure, runScheduleMap, untrackedTrips, nowDate);
            expect(result).to.be.false;
        });

        it("should return false if there is a significant gap after the previous previous trip", () => {
            const departure = {
                "trip.start_timestamp": "2025-01-13T04:50:00+01:00",
                run_number: 3,
                route_id: "L1",
                is_delay_available: false,
                route_type: GTFSRouteTypeEnum.TRAM,
                trip_id: "2",
            } as IPIDDeparture;
            const runScheduleMap = new Map<string, IScheduleDto[]>([
                [
                    "1_3",
                    [
                        {
                            trip_id: "0",
                            start_timestamp: "2025-01-13T01:36:00+01:00",
                            end_timestamp: "2025-01-13T01:38:00+01:00",
                        },
                        {
                            trip_id: "1",
                            start_timestamp: "2025-01-13T04:36:00+01:00",
                            end_timestamp: "2025-01-13T04:38:00+01:00",
                        },
                        {
                            trip_id: "2",
                            start_timestamp: "2025-01-13T04:50:00+01:00",
                            end_timestamp: "2025-01-13T04:55:00+01:00",
                        },
                    ] as IScheduleDto[],
                ],
            ]);

            const untrackedTrips = new Set<string>(["1"]);
            const nowDate = new Date("2025-01-13T04:35:00+01:00");

            const result = SkipHelper.isVehicleMissing(departure, runScheduleMap, untrackedTrips, nowDate);
            expect(result).to.be.false;
        });

        it("should return true if the vehicle is missing", () => {
            const departure = {
                "trip.start_timestamp": "2025-01-13T04:50:00+01:00",
                run_number: 3,
                route_id: "L1",
                is_delay_available: false,
                route_type: GTFSRouteTypeEnum.TRAM,
                trip_id: "2",
            } as IPIDDeparture;
            const runScheduleMap = new Map<string, IScheduleDto[]>([
                [
                    "1_3",
                    [
                        {
                            trip_id: "0",
                            start_timestamp: "2025-01-13T04:25:00+01:00",
                            end_timestamp: "2025-01-13T04:28:00+01:00",
                        },
                        {
                            trip_id: "1",
                            start_timestamp: "2025-01-13T04:36:00+01:00",
                            end_timestamp: "2025-01-13T04:38:00+01:00",
                        },
                        {
                            trip_id: "2",
                            start_timestamp: "2025-01-13T04:50:00+01:00",
                            end_timestamp: "2025-01-13T04:55:00+01:00",
                        },
                    ] as IScheduleDto[],
                ],
            ]);

            const untrackedTrips = new Set<string>(["1"]);
            const nowDate = new Date("2025-01-13T04:35:00+01:00");

            const result = SkipHelper.isVehicleMissing(departure, runScheduleMap, untrackedTrips, nowDate);
            expect(result).to.be.true;
        });
    });

    describe("isOutsideStartThreshold", () => {
        it("should return true if the departure is too far in the future (metro)", () => {
            const result = SkipHelper["isOutsideStartThreshold"](30, GTFSRouteTypeEnum.METRO);
            expect(result).to.be.true;
        });

        it("should return false if the departure is too far in the future (metro)", () => {
            const result = SkipHelper["isOutsideStartThreshold"](4, GTFSRouteTypeEnum.METRO);
            expect(result).to.be.false;
        });

        it("should return true if the departure is too far in the future (tram)", () => {
            const result = SkipHelper["isOutsideStartThreshold"](30, GTFSRouteTypeEnum.TRAM);
            expect(result).to.be.true;
        });

        it("should return false if the departure is too far in the future (tram)", () => {
            const result = SkipHelper["isOutsideStartThreshold"](29, GTFSRouteTypeEnum.TRAM);
            expect(result).to.be.false;
        });
    });
});
