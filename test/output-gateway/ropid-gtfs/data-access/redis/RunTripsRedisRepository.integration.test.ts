import { OgPidContainer } from "#og/pid/ioc/Di";
import { OgPidToken } from "#og/pid/ioc/OgPidToken";
import { IRunTripsRedisRepository } from "#og/ropid-gtfs/domain/repository/IRunTripsRedisRepository";
import { GTFS_RUN_SCHEDULE_NAMESPACE_PREFIX } from "#sch/ropid-gtfs/redis/const";
import { IoRedisConnector } from "@golemio/core/dist/helpers/data-access/redis/IoRedisConnector";
import { ContainerToken } from "@golemio/core/dist/output-gateway/ioc";
import { expect } from "chai";
import { gtfsRunSchedule10_10, gtfsRunSchedule1_3 } from "../../fixture/GtfsRunScheduleFixture";

describe("RunTripsRedisRepository (integration)", () => {
    const keyRouteOrigin1 = GTFS_RUN_SCHEDULE_NAMESPACE_PREFIX + ":1_3";
    const keyRouteOrigin10 = GTFS_RUN_SCHEDULE_NAMESPACE_PREFIX + ":10_10";

    before(async () => {
        const connector = OgPidContainer.resolve<IoRedisConnector>(ContainerToken.RedisConnector);
        const connection = await connector.connect();
        await connection.mset(
            keyRouteOrigin1,
            JSON.stringify(gtfsRunSchedule1_3),
            keyRouteOrigin10,
            JSON.stringify(gtfsRunSchedule10_10)
        );
    });

    after(async () => {
        const connector = OgPidContainer.resolve<IoRedisConnector>(ContainerToken.RedisConnector);
        await connector.getConnection().del(keyRouteOrigin1, keyRouteOrigin10);
    });

    it("should return empty schedule and trip ids when run tuples are empty", async () => {
        const runTuples: string[] = [];
        const repository = OgPidContainer.resolve<IRunTripsRedisRepository>(OgPidToken.RunTripsRedisRepository);

        const result = await repository.getMultipleSchedule(runTuples);
        expect(result).to.be.an("array").that.is.empty;
    });

    it("should return schedule for multiple run tuples", async () => {
        const runTuples = ["1_3", "10_10"];
        const repository = OgPidContainer.resolve<IRunTripsRedisRepository>(OgPidToken.RunTripsRedisRepository);

        const result = await repository.getMultipleSchedule(runTuples);
        expect(result).to.be.an("array").that.has.length(2);
        expect(result[0]).to.deep.equal(gtfsRunSchedule1_3);
        expect(result[1]).to.deep.equal(gtfsRunSchedule10_10);
    });
});
