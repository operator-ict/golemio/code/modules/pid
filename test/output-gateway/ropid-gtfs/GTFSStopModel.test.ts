import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon from "sinon";
import { models } from "#og/ropid-gtfs/models";
import { GTFSStopModel } from "#og/ropid-gtfs/models/GTFSStopModel";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { PG_INT_MAX } from "src/const";

chai.use(chaiAsPromised);

describe("GTFSStopModel", () => {
    const stopModel: GTFSStopModel = models.GTFSStopModel;

    // Basic configuration: create a sinon sandbox for testing
    let sandbox: any = null;
    const stopId: string = "U476Z103P";

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox && sandbox.restore();
    });

    it("should instantiate", () => {
        expect(stopModel).not.to.be.undefined;
    });

    it("should return all items", async () => {
        const result = await stopModel.GetAll({});
        expect(result.features).to.be.an.instanceOf(Array);
        expect(result.type).to.be.equal("FeatureCollection");
    });

    it("should return few items", async () => {
        const result = await stopModel.GetAll({ limit: 10, offset: 0 });
        expect(result.features).to.be.an.instanceOf(Array).and.lengthOf(10);
        expect(result.type).to.be.equal("FeatureCollection");
        expect(result.features[0]).to.have.property("geometry");
        expect(result.features[0]).to.have.property("properties");
        expect(result.features[0]).to.have.property("type", "Feature");
    });

    it("should return single item", async () => {
        const stop: any = await stopModel.GetOne(stopId);
        expect(stop).not.to.be.empty;
        expect(stop.properties).to.have.property("stop_id", stopId);
        expect(stop).to.have.property("geometry");
        expect(stop).to.have.property("properties");
        expect(stop).to.have.property("type", "Feature");
    });

    it("should return proper item for aswId[] 286_1", async () => {
        const stops: any = await stopModel.GetAll({
            aswIds: ["286_1"],
        });
        expect(stops).not.to.be.empty;
        expect(stops.features.length).to.be.equal(1);
        expect(stops.features[0].properties).to.have.property("stop_id", "U286Z1P");
    });

    it("should return proper item for aswId[] 286_101", async () => {
        const stops: any = await stopModel.GetAll({
            aswIds: ["286_101"],
        });
        expect(stops).not.to.be.empty;
        expect(stops.features.length).to.be.equal(1);
        expect(stops.features[0].properties).to.have.property("stop_id", "U286Z101P");
    });

    it("should return all 9 items for aswId[] 286", async () => {
        const stops: any = await stopModel.GetAll({
            aswIds: ["286"],
        });
        expect(stops).not.to.be.empty;
        expect(stops.features.length).to.be.equal(9);
    });

    it("should return all 9 items for aswId[] 286_", async () => {
        const stops: any = await stopModel.GetAll({
            aswIds: ["286_"],
        });
        expect(stops).not.to.be.empty;
        expect(stops.features.length).to.be.equal(9);
    });

    it("should return all 9 items for cisId[] 55083", async () => {
        const stops: any = await stopModel.GetAll({
            cisIds: ["55083"],
        });
        expect(stops).not.to.be.empty;
        expect(stops.features.length).to.be.equal(9);
    });

    it("should return 2 items for aswId[] 286_101 and 286_102", async () => {
        const stops: any = await stopModel.GetAll({
            aswIds: ["286_101", "286_102"],
        });
        expect(stops).not.to.be.empty;
        expect(stops.features.length).to.be.equal(2);
        expect(stops.features[0].properties).to.have.property("stop_id", "U286Z101P");
        expect(stops.features[1].properties).to.have.property("stop_id", "U286Z102P");
    });

    it("should return proper item for ids[] U286Z4", async () => {
        const stops: any = await stopModel.GetAll({
            gtfsIds: ["U286Z4"],
        });
        expect(stops).not.to.be.empty;
        expect(stops.features.length).to.be.equal(1);
        expect(stops.features[0].properties).to.have.property("stop_id", "U286Z4");
    });

    it("should return some wierd combination", async () => {
        const stops: any = await stopModel.GetAll({
            cisIds: ["55083"],
            gtfsIds: ["U276Z106P"],
        });
        expect(stops).not.to.be.empty;
        expect(stops.features.length).to.be.equal(10);
    });

    it("should return 9 items for names[] Háje", async () => {
        const stops: any = await stopModel.GetAll({
            names: ["Háje"],
        });
        expect(stops).not.to.be.empty;
        expect(stops.features.length).to.be.equal(9);
        for (const feature of stops.features) {
            expect(feature.properties).to.have.property("stop_name", "Háje");
        }
    });

    it("should return both origin and new stop for aswIds[] 115/101", async () => {
        const stops: any = await stopModel.GetAll({
            aswIds: ["115/101"],
        });
        expect(stops).not.to.be.empty;
        expect(stops.features.length).to.be.equal(2);
        expect(stops.features[0].properties).to.have.property("stop_id", "U115Z101P");
        expect(stops.features[1].properties).to.have.property("stop_id", "U115Z101P_900222");
    });

    it("should return only one Nemocnice Motol stop with location_type == 0", async () => {
        const stops: any = await stopModel.GetAll({
            locationType: 0,
            names: ["Nemocnice Motol"],
        });
        expect(stops).not.to.be.empty;
        expect(stops.features.length).to.be.equal(1);
        expect(stops.features[0].properties).to.have.property("stop_id", "U306Z102P");
    });

    it("should return 2 items for ?aswId[]=2543_2", async () => {
        const stops: any = await stopModel.GetAll({
            aswIds: ["2543_2"],
        });
        expect(stops).not.to.be.empty;
        expect(stops.features.length).to.be.equal(2);
    });

    it("should return only 1 item for ?aswId[]=449_3", async () => {
        const stops: any = await stopModel.GetAll({
            aswIds: ["449_3"],
        });
        expect(stops).not.to.be.empty;
        expect(stops.features.length).to.be.equal(1);
    });

    it("should not throw error for too big integer value", async () => {
        expect(() => stopModel["validateAswId"]("286_101", "286", "101")).to.not.throw();
        expect(() => stopModel["validateAswId"]("286_", "286", "")).to.not.throw();
        expect(() => stopModel["validateAswId"]("2", "2", "")).to.not.throw();
    });

    it("should throw an error for invalid ASW ID (too large)", () => {
        expect(() => stopModel["validateAswId"]("22_9999999999", "22", "9999999999")).to.throw(GeneralError);
        expect(() => stopModel["validateAswId"]("9999999999_", "9999999999")).to.throw(GeneralError);
        expect(() => stopModel["validateAswId"]("2", String(PG_INT_MAX + 1), "")).to.throw(GeneralError);
    });

    // This test will fail if PostGIS is not installed
    // it("should return all stops close to the point", async () => {
    //     const result: any = await stopModel.GetAll({
    //         lat: 50.11548,
    //         lng: 14.43732,
    //         range: 1000,
    //     });
    //     expect(result.features).to.be.an.instanceOf(Array);
    //     expect(result.type).to.be.equal("FeatureCollection");
    // });
});
