import { v2GtfsRouter } from "#og/ropid-gtfs/routers";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import express, { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { HTTPErrorHandler, IGolemioError } from "@golemio/core/dist/shared/golemio-errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon from "sinon";
import request from "supertest";

chai.use(chaiAsPromised);

describe("V2GTFSRouter", () => {
    // Create clean express instance
    const app = express();
    // Basic configuration: create a sinon sandbox for testing
    let sandbox: any = null;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });
    2;

    afterEach(() => {
        sandbox && sandbox.restore();
    });

    before(() => {
        // Mount the tested router to the express instance
        app.use(v2GtfsRouter.getPath(), v2GtfsRouter.getRouter());
        app.use((err: any, req: Request, res: Response, next: NextFunction) => {
            const errObject: IGolemioError = HTTPErrorHandler.handle(err, log);
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    it("should respond with json to GET /v2/gtfs/trips ", (done) => {
        request(app).get("/v2/gtfs/trips").set("Accept", "application/json").expect("Content-Type", /json/).expect(200, done);
    });

    it("should respond with list of trips to GET /v2/gtfs/trips", (done) => {
        request(app)
            .get("/v2/gtfs/trips")
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(200);
                expect(res.body).to.be.an("array");
                done();
            });
    });

    it("should respond with detail of trip to GET /v2/gtfs/trips/{id}", (done) => {
        request(app)
            .get("/v2/gtfs/trips/991_30_190107")
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(404);
                done();
            });
    });

    it("should respond with paginated list GET /v2/gtfs/trips?limit=20&offset=ahoj", (done) => {
        request(app)
            .get("/v2/gtfs/trips?limit=20&offset=ahoj")
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(400);
                done();
            });
    });

    it("should respond with paginated list GET /v2/gtfs/trips?limit=ahoj&offset=1", (done) => {
        request(app)
            .get("/v2/gtfs/trips?limit=ahoj&offset=1")
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(400);
                done();
            });
    });

    it("should respond with paginated list GET /v2/gtfs/trips?limit=20&offset=2", (done) => {
        request(app)
            .get("/v2/gtfs/trips?limit=20&offset=2")
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(200);
                expect(res.body).to.be.an("array");
                done();
            });
    });

    it("should respond with routes going through stop ID U953Z102P GET /v2/gtfs/trips?stopId=U953Z102P", (done) => {
        request(app)
            .get("/v2/gtfs/trips?stopId=U953Z102P")
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(200);
                expect(res.body).to.be.an("array");
                done();
            });
    });

    it("should respond with routes for specific date GET /v2/gtfs/trips?date=2019-05-27", (done) => {
        request(app)
            .get("/v2/gtfs/trips?date=2019-05-27")
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(200);
                expect(res.body).to.be.an("array");
                done();
            });
    });

    it("should respond with json to GET /v2/gtfs/stops ", (done) => {
        request(app)
            .get("/v2/gtfs/stops")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(200);
                expect(res.body).to.be.an("object");
                expect(res.body.features).to.be.an("array");
                expect(res.body.type).to.be.equal("FeatureCollection");

                const feature = res.body.features[0];
                expect(feature).to.haveOwnProperty("properties");
                expect(feature.properties).to.have.keys(
                    "level_id",
                    "location_type",
                    "parent_station",
                    "platform_code",
                    "stop_id",
                    "stop_name",
                    "wheelchair_boarding",
                    "zone_id"
                );

                done();
            });
    });

    it("should respond with 200 to GET /v2/gtfs/stops with params", (done) => {
        request(app)
            .get("/v2/gtfs/stops?ids=U1040Z102P&ids=U1116Z3P")
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(200);
                expect(res.body.features).to.have.length(2);
                done();
            });
    });

    it("should respond with 400 to GET /v2/gtfs/stops?latlng (wrong query param)", (done) => {
        request(app)
            .get("/v2/gtfs/stops?latlng=50.1154814.43732")
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(400);
                done();
            });
    });

    it("should respond with 200 to GET /v2/gtfs/stoptimes/:stop_id", (done) => {
        request(app)
            .get("/v2/gtfs/stoptimes/U118Z102P")
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(200);
                expect(res.body).to.be.instanceOf(Array);

                const stopTime = res.body[0];
                expect(stopTime).to.have.keys(
                    "arrival_time",
                    "departure_time",
                    "drop_off_type",
                    "pickup_type",
                    "shape_dist_traveled",
                    "stop_headsign",
                    "stop_id",
                    "stop_sequence",
                    "trip_id"
                );

                done();
            });
    });

    it("should respond with 400 to GET /v2/gtfs/stoptimes/:stop_id incorrect filters", (done) => {
        request(app)
            .get("/v2/gtfs/stoptimes/U118Z102P?from=13:22:11&to=12:12:12")
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(400);
                expect(res.body.error_info).to.contain("'to' cannot be later than 'from'");
                done();
            });
    });

    it("should respond with 200 to GET /v2/gtfs/routes ", (done) => {
        request(app)
            .get("/v2/gtfs/routes")
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(200);
                expect(res.body).to.be.instanceOf(Array);
                done();
            });
    });

    it("should respond with 404 to GET /v2/gtfs/routes/:routeId ", (done) => {
        request(app)
            .get("/v2/gtfs/routes/L991aaaaaaaaaa")
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(404);
                done();
            });
    });

    it("should respond with 404 to GET /v2/gtfs/shapes/:shapeId ", (done) => {
        request(app)
            .get("/v2/gtfs/shapes/asdh")
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(404);
                done();
            });
    });

    it("should respond with 200 to GET /v2/gtfs/services ", (done) => {
        request(app)
            .get("/v2/gtfs/services")
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(200);
                expect(res.body).to.be.instanceOf(Array);
                done();
            });
    });

    it("should respond with 200 to GET /v2/gtfs/services?date=2019-02-28 ", (done) => {
        request(app)
            .get("/v2/gtfs/services?date=2019-02-28")
            .end((err: any, res: any) => {
                expect(res.statusCode).to.be.equal(200);
                expect(res.body).to.be.instanceOf(Array);
                done();
            });
    });

    it("should respond with 400 to GET /v2/gtfs/trips?limit=10&date[]=2022-04-17&date[]=2022-04-17", (done) => {
        request(app)
            .get("/v2/gtfs/trips?limit=10&date[]=2022-04-17&date[]=2022-04-17")
            .end((_err, res) => {
                expect(res.statusCode).to.be.equal(400);
                done();
            });
    });
});
