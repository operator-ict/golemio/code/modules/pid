import { GTFSRouteTypeEnum } from "#helpers/RouteTypeEnums";
import { PublicVehiclePositionsTransformation } from "#og/public/service/transformations/PublicVehiclePositionsTransformation";
import { expect } from "chai";
import { StatePositionEnum } from "src/const";

describe("PublicVehiclePositionsTransformation", () => {
    let transformation: PublicVehiclePositionsTransformation;

    before(() => {
        transformation = new PublicVehiclePositionsTransformation();
    });

    it("should properly transform data (without detailed info about trip/position)", () => {
        const inputMock = {
            gtfs_trip_id: "17_001",
            route_type: GTFSRouteTypeEnum.TRAM,
            gtfs_route_short_name: "017",
            lat: 15.4444,
            lng: 50.222,
            bearing: 1,
            delay: 20,
            vehicle_id: "metro-A-32",
            state_position: StatePositionEnum.AT_STOP,
            detailed_info: {
                is_wheelchair_accessible: true,
                origin_route_name: "17",
                shape_id: null,
                run_number: 17,
                trip_headsign: "Sídliště Řepy",
                shape_dist_traveled: 7.035025,
                last_stop_sequence: 1,
                origin_timestamp: "2024-03-23T14:00:00.000Z",
                operator: null,
                registration_number: null,
                vehicle_type_id: null,
            },
        };
        const expected = {
            geometry: { coordinates: [50.222, 15.4444], type: "Point" },
            properties: {
                gtfs_trip_id: "17_001",
                route_type: "tram",
                gtfs_route_short_name: "017",
                bearing: 1,
                delay: 20,
                vehicle_id: "metro-A-32",
                state_position: StatePositionEnum.AT_STOP,
            },
            type: "Feature",
        };

        expect(transformation.transformElement(inputMock)).to.deep.equal(expected);
    });
});
