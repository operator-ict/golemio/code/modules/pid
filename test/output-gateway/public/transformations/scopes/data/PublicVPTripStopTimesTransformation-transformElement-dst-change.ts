export const transformElementCETToCESTTestData = [
    {
        time: "00:01:02",
        date: "2023-03-25",
        correctTime: "00:01:02",
    },
    {
        time: "10:01:02",
        date: "2023-03-25",
        correctTime: "10:01:02",
    },
    {
        time: "00:01:02",
        date: "2023-03-26",
        correctTime: "23:01:02",
    },
    {
        time: "24:01:02",
        date: "2023-03-25",
        correctTime: "00:01:02",
    },
    {
        time: "01:01:02",
        date: "2023-03-26",
        correctTime: "00:01:02",
    },
    {
        time: "25:01:02",
        date: "2023-03-25",
        correctTime: "01:01:02",
    },
    {
        time: "02:01:02",
        date: "2023-03-26",
        correctTime: "01:01:02",
    },
    {
        time: "26:01:02",
        date: "2023-03-25",
        correctTime: "03:01:02",
    },
    {
        time: "03:01:02",
        date: "2023-03-26",
        correctTime: "03:01:02",
    },
    {
        time: "27:01:02",
        date: "2023-03-25",
        correctTime: "04:01:02",
    },
    {
        time: "04:01:02",
        date: "2023-03-26",
        correctTime: "04:01:02",
    },
    {
        time: "28:01:02",
        date: "2023-03-25",
        correctTime: "05:01:02",
    },
    {
        time: "05:01:02",
        date: "2023-03-26",
        correctTime: "05:01:02",
    },
    {
        time: "29:01:02",
        date: "2023-03-25",
        correctTime: "06:01:02",
    },
    {
        time: "06:01:02",
        date: "2023-03-26",
        correctTime: "06:01:02",
    },
    {
        time: "10:01:02",
        date: "2023-03-26",
        correctTime: "10:01:02",
    },
    {
        time: "00:01:02",
        date: "2023-03-27",
        correctTime: "00:01:02",
    },
    {
        time: "10:01:02",
        date: "2023-03-27",
        correctTime: "10:01:02",
    },
];

export const transformElementCESTToCETTestData = [
    {
        time: "00:01:02",
        date: "2023-10-28",
        correctTime: "00:01:02",
    },
    {
        time: "10:01:02",
        date: "2023-10-28",
        correctTime: "10:01:02",
    },
    {
        time: "24:01:02",
        date: "2023-10-28",
        correctTime: "00:01:02",
    },
    {
        time: "00:01:02",
        date: "2023-10-29",
        correctTime: "01:01:02",
    },
    {
        time: "25:01:02",
        date: "2023-10-28",
        correctTime: "01:01:02",
    },
    {
        time: "01:01:02",
        date: "2023-10-29",
        correctTime: "02:01:02",
    },
    {
        time: "26:01:02",
        date: "2023-10-28",
        correctTime: "02:01:02",
    },
    {
        time: "02:01:02",
        date: "2023-10-29",
        correctTime: "02:01:02",
    },
    {
        time: "27:01:02",
        date: "2023-10-28",
        correctTime: "02:01:02",
    },
    {
        time: "03:01:02",
        date: "2023-10-29",
        correctTime: "03:01:02",
    },
    {
        time: "28:01:02",
        date: "2023-10-28",
        correctTime: "03:01:02",
    },
    {
        time: "04:01:02",
        date: "2023-10-29",
        correctTime: "04:01:02",
    },
    {
        time: "29:01:02",
        date: "2023-10-28",
        correctTime: "04:01:02",
    },
    {
        time: "10:01:02",
        date: "2023-10-29",
        correctTime: "10:01:02",
    },
    {
        time: "00:01:02",
        date: "2023-10-30",
        correctTime: "00:01:02",
    },
    {
        time: "10:01:02",
        date: "2023-10-30",
        correctTime: "10:01:02",
    },
];
