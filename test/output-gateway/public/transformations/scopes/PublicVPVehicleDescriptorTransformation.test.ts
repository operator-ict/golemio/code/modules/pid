import { PublicVPVehicleDescriptorTransformation } from "#og/public/service/transformations/scopes/PublicVPVehicleDescriptorTransformation";
import { IPublicApiCacheDto } from "#sch/vehicle-positions/redis/interfaces/IPublicApiCacheDto";
import { expect } from "chai";

describe("PublicVPVehicleDescriptorTransformation", () => {
    let transformation: PublicVPVehicleDescriptorTransformation;

    before(() => {
        transformation = new PublicVPVehicleDescriptorTransformation();
    });

    it("should properly transform data (vehicle descriptor)", () => {
        const inputMock: Partial<IPublicApiCacheDto> = {
            detailed_info: {
                is_wheelchair_accessible: true,
                origin_route_name: "137",
                shape_id: "L137V2",
                run_number: 1,
                trip_headsign: "U Waltrovky",
                shape_dist_traveled: 2.316,
                last_stop_sequence: 6,
                origin_timestamp: "2024-02-01T16:02:29.000Z",
                registration_number: 3927,
                operator: "DP PRAHA",
            },
        };

        const expected = {
            operator: "DP PRAHA",
            vehicle_type: null,
            vehicle_registration_number: "3927",
            is_wheelchair_accessible: true,
            is_air_conditioned: null,
            has_usb_chargers: null,
        };

        expect(transformation.transformElement(inputMock as IPublicApiCacheDto)).to.deep.equal(expected);
    });
});
