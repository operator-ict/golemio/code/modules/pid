import { PublicVPTripShapesTransformation } from "#og/public/service/transformations/scopes/PublicVPTripShapesTransformation";
import { IComputationTripShape } from "#sch/ropid-gtfs/redis/interfaces/IDelayComputationDto";
import { expect } from "chai";

describe("PublicVPTripShapesTransformation", () => {
    let transformation: PublicVPTripShapesTransformation;

    before(() => {
        transformation = new PublicVPTripShapesTransformation();
    });

    it("should properly transform data (shapes)", () => {
        const inputMock: IComputationTripShape = {
            dist: 7.035025,
            coords: [14.404205, 50.068688],
        };

        const expected = {
            geometry: { coordinates: [14.404205, 50.068688], type: "Point" },
            properties: {
                shape_dist_traveled: 7.035025,
            },
            type: "Feature",
        };

        expect(transformation.transformElement(inputMock)).to.deep.equal(expected);
    });
});
