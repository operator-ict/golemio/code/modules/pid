import { PublicVPTripStopTimesTransformation } from "#og/public/service/transformations/scopes/PublicVPTripStopTimesTransformation";
import { IStopTime } from "#sch/ropid-gtfs/redis/interfaces/IDelayComputationDto";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";
import {
    transformElementCESTToCETTestData,
    transformElementCETToCESTTestData,
} from "./data/PublicVPTripStopTimesTransformation-transformElement-dst-change";

describe("PublicVPTripStopTimesTransformation", () => {
    let sandbox: SinonSandbox;
    let transformation: PublicVPTripStopTimesTransformation;

    before(() => {
        transformation = new PublicVPTripStopTimesTransformation();
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should properly transform data (stop times)", () => {
        const inputMock: IStopTime = {
            arrival_time: "17:01:00",
            departure_time: "17:01:00",
            shape_dist_traveled: 2.315504,
            stop_headsign: null,
            stop_id: "U116Z1P",
            stop_sequence: 6,
            arrival_time_seconds: 61260,
            departure_time_seconds: 61260,
            is_no_stop_waypoint: false,
            stop: {
                stop_id: "U116Z1P",
                stop_lat: 50.060677,
                stop_lon: 14.381,
                stop_name: "Farkáň",
                zone_id: "P",
                wheelchair_boarding: 0,
            },
        };

        const expected = {
            geometry: {
                coordinates: [14.381, 50.060677],
                type: "Point",
            },
            properties: {
                is_wheelchair_accessible: null,
                stop_name: "Farkáň",
                stop_sequence: 6,
                zone_id: "P",
                shape_dist_traveled: 2.315504,
                arrival_time: "17:01:00",
                departure_time: "17:01:00",
                realtime_arrival_time: null,
                realtime_departure_time: null,
            },
            type: "Feature",
        };

        expect(transformation.transformElement(inputMock)).to.deep.equal(expected);
    });

    const dstChangeTests = [
        {
            data: transformElementCETToCESTTestData,
            title: "Around DST change (CET -> CEST)",
        },
        {
            data: transformElementCESTToCETTestData,
            title: "Around DST change (CEST -> CET)",
        },
    ];

    dstChangeTests.forEach(({ data, title }) => {
        describe(title, () => {
            data.forEach(({ date, time, correctTime }) => {
                it(
                    `should properly transform data (stop times) on the day of ${date} with arrival and departure time` +
                        ` ${time}`,
                    () => {
                        const timeInSeconds = time
                            .split(":")
                            .reduce((acc, val, i) => acc + parseInt(val, 10) * Math.pow(60, 2 - i), 0);
                        sandbox.useFakeTimers({
                            now: new Date(date),
                        });
                        const inputMock: IStopTime = {
                            arrival_time: time,
                            departure_time: time,
                            shape_dist_traveled: 2.315504,
                            stop_headsign: null,
                            stop_id: "U116Z1P",
                            stop_sequence: 6,
                            arrival_time_seconds: timeInSeconds,
                            departure_time_seconds: timeInSeconds,
                            is_no_stop_waypoint: false,
                            stop: {
                                stop_id: "U116Z1P",
                                stop_lat: 50.060677,
                                stop_lon: 14.381,
                                stop_name: "Farkáň",
                                zone_id: "P",
                                wheelchair_boarding: 0,
                            },
                        };
                        const expected = {
                            geometry: {
                                coordinates: [14.381, 50.060677],
                                type: "Point",
                            },
                            properties: {
                                is_wheelchair_accessible: null,
                                stop_name: "Farkáň",
                                stop_sequence: 6,
                                zone_id: "P",
                                shape_dist_traveled: 2.315504,
                                arrival_time: correctTime,
                                departure_time: correctTime,
                                realtime_arrival_time: null,
                                realtime_departure_time: null,
                            },
                            type: "Feature",
                        };

                        expect(transformation.transformElement(inputMock)).to.deep.equal(expected);
                    }
                );
            });
        });
    });
});
