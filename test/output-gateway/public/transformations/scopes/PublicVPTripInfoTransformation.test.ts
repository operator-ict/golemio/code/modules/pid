import { PublicVPTripInfoTransformation } from "#og/public/service/transformations/scopes/PublicVPTripInfoTransformation";
import { IPublicApiCacheDto } from "#sch/vehicle-positions/redis/interfaces/IPublicApiCacheDto";
import { expect } from "chai";
import { StatePositionEnum } from "src/const";

describe("PublicVPTripInfoTransformation", () => {
    let transformation: PublicVPTripInfoTransformation;

    before(() => {
        transformation = new PublicVPTripInfoTransformation();
    });

    it("should properly transform data (detailed trip info)", () => {
        const inputMock: IPublicApiCacheDto = {
            gtfs_trip_id: "137_2032_240103",
            route_type: 3,
            gtfs_route_short_name: "137",
            lat: 50.06061,
            lng: 14.38097,
            bearing: 229,
            delay: 89,
            state_position: StatePositionEnum.ON_TRACK,
            vehicle_id: "service-3-3927",
            detailed_info: {
                is_wheelchair_accessible: true,
                origin_route_name: "137",
                shape_id: "L137V2",
                run_number: 1,
                trip_headsign: "U Waltrovky",
                shape_dist_traveled: 2.316,
                last_stop_sequence: 6,
                origin_timestamp: "2024-02-01T16:02:29.000Z",
                registration_number: 3927,
                operator: "DP PRAHA",
            },
        };

        const expected = {
            gtfs_trip_id: "137_2032_240103",
            route_type: "bus",
            route_short_name: "137",
            shape_id: "L137V2",
            origin_route_name: "137",
            run_number: 1,
            trip_headsign: "U Waltrovky",
            geometry: { type: "Point", coordinates: [14.38097, 50.06061] },
            shape_dist_traveled: 2.316,
            bearing: 229,
            delay: 89,
            state_position: "on_track",
            last_stop_sequence: 6,
            origin_timestamp: "2024-02-01T17:02:29+01:00",
        };

        expect(transformation.transformElement(inputMock)).to.deep.equal(expected);
    });
});
