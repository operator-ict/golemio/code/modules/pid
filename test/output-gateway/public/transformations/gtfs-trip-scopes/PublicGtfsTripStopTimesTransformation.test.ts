import { PublicGtfsTripStopTimesTransformation } from "#og/public/service/transformations/gtfs-trip-scopes/PublicTripStopTimesTransformation";
import { IStopTimeWithStopDto } from "#sch/ropid-gtfs/interfaces/ITripWithOptionalAssociationsDto";
import { expect } from "chai";

describe("PublicGtfsTripStopTimesTransformation", () => {
    let transformation: PublicGtfsTripStopTimesTransformation;

    before(() => {
        transformation = new PublicGtfsTripStopTimesTransformation();
    });

    it("should properly transform data (stop times)", () => {
        const inputMock: Partial<IStopTimeWithStopDto> = {
            stop_sequence: 6,
            shape_dist_traveled: 2.315504,
            arrival_time_seconds: 61260,
            departure_time_seconds: 61260,
            stop: {
                stop_name: "Farkáň",
                zone_id: "P",
                wheelchair_boarding: 0,
                stop_lon: 14.381,
                stop_lat: 50.060677,
            } as any,
        };

        const expected = {
            geometry: {
                coordinates: [14.381, 50.060677],
                type: "Point",
            },
            properties: {
                arrival_time: "17:01:00",
                departure_time: "17:01:00",
                is_wheelchair_accessible: null,
                shape_dist_traveled: 2.315504,
                stop_name: "Farkáň",
                stop_sequence: 6,
                zone_id: "P",
            },
            type: "Feature",
        };

        expect(transformation.transformElement(inputMock as IStopTimeWithStopDto)).to.deep.equal(expected);
    });
});
