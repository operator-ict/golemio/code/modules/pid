import { GtfsTripWheelchairAccessEnum } from "#helpers/AccessibilityEnums";
import { PublicGtfsVehicleDescriptorTransformation } from "#og/public/service/transformations/gtfs-trip-scopes/PublicGtfsVehicleDescriptorTransformation";
import { ITripWithOptionalAssociationsDto } from "#sch/ropid-gtfs/interfaces/ITripWithOptionalAssociationsDto";
import { expect } from "chai";

describe("PublicGtfsVehicleDescriptorTransformation", () => {
    let transformation: PublicGtfsVehicleDescriptorTransformation;

    before(() => {
        transformation = new PublicGtfsVehicleDescriptorTransformation();
    });

    it("should properly transform data (vehicle descriptor)", () => {
        const inputMock: Partial<ITripWithOptionalAssociationsDto> = {
            wheelchair_accessible: GtfsTripWheelchairAccessEnum.AccessibleVehicle,
        };

        const expected = {
            is_wheelchair_accessible: true,
        };

        expect(transformation.transformElement(inputMock as ITripWithOptionalAssociationsDto)).to.deep.equal(expected);
    });
});
