import { PublicGtfsTripInfoTransformation } from "#og/public/service/transformations/gtfs-trip-scopes/PublicGtfsTripInfoTransformation";
import { ITripWithDefinedRouteDto } from "#sch/ropid-gtfs/interfaces/ITripWithOptionalAssociationsDto";
import { expect } from "chai";

describe("PublicGtfsTripInfoTransformation", () => {
    let transformation: PublicGtfsTripInfoTransformation;

    before(() => {
        transformation = new PublicGtfsTripInfoTransformation();
    });

    it("should properly transform data (GTFS trip info)", () => {
        const inputMock: Partial<ITripWithDefinedRouteDto> = {
            trip_id: "137_2032_240103",
            shape_id: "L137V2",
            trip_headsign: "U Waltrovky",
            route: {
                route_type: 3,
                route_short_name: "137",
                route_id: "L137",
            } as any,
            schedule: {
                run_number: 1,
            } as any,
        };

        const expected = {
            gtfs_trip_id: "137_2032_240103",
            route_type: "bus",
            route_short_name: "137",
            shape_id: "L137V2",
            origin_route_name: "137",
            run_number: 1,
            trip_headsign: "U Waltrovky",
        };

        expect(transformation.transformElement(inputMock as ITripWithDefinedRouteDto)).to.deep.equal(expected);
    });
});
