import { PublicGtfsTripShapesTransformation } from "#og/public/service/transformations/gtfs-trip-scopes/PublicGtfsTripShapesTransformation";
import { IShapeDto } from "#sch/ropid-gtfs/interfaces/IShapeDto";
import { expect } from "chai";

describe("PublicGtfsTripShapesTransformation", () => {
    let transformation: PublicGtfsTripShapesTransformation;

    before(() => {
        transformation = new PublicGtfsTripShapesTransformation();
    });

    it("should properly transform data (shapes)", () => {
        const inputMock: Partial<IShapeDto> = {
            shape_dist_traveled: 7.035025,
            shape_pt_lat: 50.068688,
            shape_pt_lon: 14.404205,
        };

        const expected = {
            geometry: { coordinates: [14.404205, 50.068688], type: "Point" },
            properties: {
                shape_dist_traveled: 7.035025,
            },
            type: "Feature",
        };

        expect(transformation.transformElement(inputMock as IShapeDto)).to.deep.equal(expected);
    });
});
