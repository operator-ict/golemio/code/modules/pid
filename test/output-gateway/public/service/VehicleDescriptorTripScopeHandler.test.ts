import { OgModuleToken } from "#og/public/ioc/OgModuleToken";
import { VehicleDescriptorTripScopeHandler } from "#og/public/service/helpers/trip-scope/strategy/VehicleDescriptorTripScopeHandler";
import { IPublicApiCacheDto } from "#sch/vehicle-positions/redis/interfaces/IPublicApiCacheDto";
import { container } from "@golemio/core/dist/shared/tsyringe";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";

describe("VehicleDescriptorTripScopeHandler", () => {
    let sandbox: SinonSandbox;

    const createContainer = () =>
        container
            .createChildContainer()
            .register(
                OgModuleToken.VehicleDescriptorCachedRepository,
                class VehicleDescriptorCachedRepository {
                    getOneByRegNumber = sinon.stub().resolves({
                        is_air_conditioned: true,
                        has_usb_chargers: false,
                        manufacturer: "Solaris",
                        type: "Urbino 8.9 LE",
                    });
                }
            )
            .register(OgModuleToken.VehicleDescriptorTripScopeHandler, VehicleDescriptorTripScopeHandler)
            .resolve<VehicleDescriptorTripScopeHandler>(OgModuleToken.VehicleDescriptorTripScopeHandler);

    before(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        container.clearInstances();
        sinon.reset();
        sandbox.restore();
    });

    after(() => {
        sinon.restore();
    });

    it("should return correct output", async () => {
        const handler = createContainer();
        const output = await handler.handle({}, {
            detailed_info: {
                is_wheelchair_accessible: true,
                registration_number: 3927,
                operator: "DP PRAHA",
            } as Partial<IPublicApiCacheDto["detailed_info"]>,
        } as Partial<IPublicApiCacheDto> as IPublicApiCacheDto);

        expect(output).to.deep.equal({
            vehicle_descriptor: {
                operator: "DP PRAHA",
                vehicle_type: "Solaris Urbino 8.9 LE",
                vehicle_registration_number: "3927",
                is_wheelchair_accessible: true,
                is_air_conditioned: true,
                has_usb_chargers: false,
            },
        });
    });
});
