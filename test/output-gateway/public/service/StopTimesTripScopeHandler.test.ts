import { OgModuleToken } from "#og/public/ioc/OgModuleToken";
import { StopTimesTripScopeHandler } from "#og/public/service/helpers/trip-scope/strategy/StopTimesTripScopeHandler";
import { container } from "@golemio/core/dist/shared/tsyringe";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";
import { StatePositionEnum } from "src/const";

describe("StopTimesTripScopeHandler", () => {
    let sandbox: SinonSandbox;

    const compCache = require("../../../../db/example/redis/publicDelayCompObjectMock.json");
    const stopTimesCache = require("../../../../db/example/redis/publicStopTimesMock.json");
    const stopTimesCacheBeforeTrack = require("../../../../db/example/redis/publicStopTimesMockBeforeTrack.json");
    const stopTimesCacheWithWrongDelays = require("../../../../db/example/redis/publicStopTimesMockWithWrongDelays.json");

    const createContainer = () =>
        container
            .createChildContainer()
            .register(
                OgModuleToken.DelayComputationRepository,
                class DelayComputationRepository {
                    getDelayComputationCache = sinon.stub().resolves(compCache);
                }
            )
            .register(
                OgModuleToken.PublicStopTimeRepository,
                class PublicStopTimeRepository {
                    getPublicStopTimeCache = (vehicleId: string, tripId: string) => {
                        switch (tripId) {
                            case "test":
                                return stopTimesCache;
                            case "test3":
                                return stopTimesCacheWithWrongDelays;
                            default:
                                return stopTimesCacheBeforeTrack;
                        }
                    };
                }
            )
            .register(OgModuleToken.StopTimesTripScopeHandler, StopTimesTripScopeHandler)
            .resolve<StopTimesTripScopeHandler>(OgModuleToken.StopTimesTripScopeHandler);

    before(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        container.clearInstances();
        sinon.reset();
        sandbox.restore();
    });

    after(() => {
        sinon.restore();
    });

    it("should return correct output", async () => {
        const handler = createContainer();
        const output = await handler.handle({}, { gtfs_trip_id: "test" } as any);

        expect(output).to.have.property("stop_times");
        expect(output.stop_times).to.be.an("object");
        expect(output.stop_times!.features).to.have.length(7);

        const realtimeArrivals = output.stop_times!.features.map((f: any) => f.properties.realtime_arrival_time);
        expect(realtimeArrivals).to.deep.equal(["16:52:48", null, "16:58:23", "16:59:35", "17:01:20", "17:02:20", "17:03:20"]);

        const realtimeDepartures = output.stop_times!.features.map((f: any) => f.properties.realtime_departure_time);
        expect(realtimeDepartures).to.deep.equal([
            "16:53:17",
            "16:57:13",
            "16:58:53",
            "17:00:20",
            "17:01:20",
            "17:02:20",
            "17:03:20",
        ]);
    });

    it("should fallback to schedule time if vehicle is before the track (delay 0)", async () => {
        const handler = createContainer();
        const output = await handler.handle({}, {
            gtfs_trip_id: "test2",
            delay: 0,
            state_position: StatePositionEnum.BEFORE_TRACK,
        } as any);

        expect(output).to.have.property("stop_times");
        expect(output.stop_times).to.be.an("object");
        expect(output.stop_times!.features).to.have.length(7);

        const realtimeArrivals = output.stop_times!.features.map((f: any) => f.properties.realtime_arrival_time);
        expect(realtimeArrivals).to.deep.equal([
            "16:53:00",
            "16:55:00",
            "16:57:00",
            "16:59:00",
            "17:00:00",
            "17:01:00",
            "17:02:00",
        ]);

        const realtimeDepartures = output.stop_times!.features.map((f: any) => f.properties.realtime_departure_time);
        expect(realtimeDepartures).to.deep.equal([
            "16:53:00",
            "16:55:00",
            "16:57:00",
            "16:59:00",
            "17:00:00",
            "17:01:00",
            "17:02:00",
        ]);
    });

    // TODO - this test may not be needed. Check back after confirmation from ROPID
    it.skip("should fallback to schedule time if vehicle is before the track (delay null)", async () => {
        const handler = createContainer();
        const output = await handler.handle({}, {
            gtfs_trip_id: "test2",
            delay: null,
            state_position: StatePositionEnum.BEFORE_TRACK,
        } as any);

        expect(output).to.have.property("stop_times");
        expect(output.stop_times).to.be.an("object");
        expect(output.stop_times!.features).to.have.length(7);

        const realtimeArrivals = output.stop_times!.features.map((f: any) => f.properties.realtime_arrival_time);
        expect(realtimeArrivals).to.deep.equal([
            "16:53:00",
            "16:55:00",
            "16:57:00",
            "16:59:00",
            "17:00:00",
            "17:01:00",
            "17:02:00",
        ]);

        const realtimeDepartures = output.stop_times!.features.map((f: any) => f.properties.realtime_departure_time);
        expect(realtimeDepartures).to.deep.equal([
            "16:53:00",
            "16:55:00",
            "16:57:00",
            "16:59:00",
            "17:00:00",
            "17:01:00",
            "17:02:00",
        ]);
    });

    it("should check time sequence", async () => {
        const handler = createContainer();
        const spy = sandbox.spy(handler, <any>"checkStopTimesSequence");

        const output = await handler.handle({}, {
            gtfs_trip_id: "test3",
            delay: null,
            state_position: StatePositionEnum.ON_TRACK,
        } as any);

        expect(spy.callCount).to.be.equal(7);
        expect(output).to.have.property("stop_times");
        expect(output.stop_times).to.be.an("object");
        expect(output.stop_times!.features).to.have.length(7);

        const realtimeArrivals = output.stop_times!.features.map((f: any) => f.properties.realtime_arrival_time);
        expect(realtimeArrivals).to.deep.equal(["16:53:00", "16:57:10", null, "17:01:00", null, "17:02:20", "17:03:20"]);

        const realtimeDepartures = output.stop_times!.features.map((f: any) => f.properties.realtime_departure_time);
        expect(realtimeDepartures).to.deep.equal(["16:53:00", null, null, "17:01:00", null, "17:02:20", null]);
    });
});
