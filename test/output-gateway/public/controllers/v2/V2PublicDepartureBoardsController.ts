import { V2PublicDepartureBoardsController } from "#og/public/controllers/v2/V2PublicDepartureBoardsController";
import { expect } from "chai";

describe("V2PublicDepartureBoardsController", () => {
    let controller: V2PublicDepartureBoardsController;

    before(() => {
        controller = new V2PublicDepartureBoardsController();
    });

    it("should properly parse input departures params", () => {
        const params = controller["parseDepartureParams"]({
            stopIds: ['{"0": ["test1", "test1", "test2"]}', '{"1": ["test3"], "2": ["test4"]}'],
            routeShortNames: ["269"],
            limit: "5",
            minutesAfter: "60",
        });

        expect(params).to.deep.equal({
            stopIds: new Set([
                {
                    priority: 0,
                    stopIds: ["test1", "test2"],
                },
                {
                    priority: 1,
                    stopIds: ["test3"],
                },
            ]),
            routeShortNames: ["269"],
            limit: 5,
            minutesAfter: 60,
        });
    });
});
