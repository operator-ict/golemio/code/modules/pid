import { V2PublicGtfsController } from "#og/public/controllers/v2/V2PublicGtfsController";
import { DetailedTripScope } from "#og/public/routers/v2/helpers/DetailedTripScopeEnum";
import { expect } from "chai";

describe("V2PublicGtfsController", () => {
    let controller: V2PublicGtfsController;

    before(() => {
        controller = new V2PublicGtfsController();
    });

    it("should properly parse input GTFS trip params", () => {
        expect(
            controller["parseGtfsTripLookupParams"]({
                params: {
                    gtfsTripId: "test",
                },
                query: {
                    scopes: DetailedTripScope.Info,
                },
            } as any)
        ).to.deep.equal({
            gtfsTripId: "test",
            scopes: [DetailedTripScope.Info],
        });
    });

    it("should properly parse input GTFS trip params (multiple scopes with duplicates)", () => {
        expect(
            controller["parseGtfsTripLookupParams"]({
                params: {
                    gtfsTripId: "test",
                },
                query: {
                    scopes: [
                        DetailedTripScope.Info,
                        DetailedTripScope.Info,
                        DetailedTripScope.Shapes,
                        DetailedTripScope.StopTimes,
                        DetailedTripScope.VehicleDescriptor,
                    ],
                },
            } as any)
        ).to.deep.equal({
            gtfsTripId: "test",
            scopes: [
                DetailedTripScope.Info,
                DetailedTripScope.Shapes,
                DetailedTripScope.StopTimes,
                DetailedTripScope.VehicleDescriptor,
            ],
        });
    });
});
