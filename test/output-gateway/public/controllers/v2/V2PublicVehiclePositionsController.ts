import { GTFSRouteTypeEnum } from "#helpers/RouteTypeEnums";
import { V2PublicVehiclePositionsController } from "#og/public/controllers/v2/V2PublicVehiclePositionsController";
import { DetailedTripScope } from "#og/public/routers/v2/helpers/DetailedTripScopeEnum";
import { expect } from "chai";

describe("V2PublicVehiclePositionsController", () => {
    let controller: V2PublicVehiclePositionsController;

    before(() => {
        controller = new V2PublicVehiclePositionsController();
    });

    it("should properly parse input positions params", () => {
        expect(
            controller["parsePositionsParams"]({
                boundingBox: "50.123,14.243,50.017,14.573",
                routeShortName: "17",
                routeType: "metro",
            })
        ).to.deep.equal({
            boundingBox: {
                topLeftLongitude: 14.243,
                topLeftLatitude: 50.123,
                bottomRightLongitude: 14.573,
                bottomRightLatitude: 50.017,
            },
            routeShortName: "17",
            type: [GTFSRouteTypeEnum.METRO],
        });
    });

    it("should properly parse input detailed trip params", () => {
        expect(
            controller["parseDetailedParams"]({
                params: {
                    vehicleId: "test",
                },
                query: {
                    scopes: DetailedTripScope.Info,
                },
            } as any)
        ).to.deep.equal({
            vehicleId: "test",
            tripId: undefined,
            scopes: [DetailedTripScope.Info],
        });
    });

    it("should properly parse input detailed trip params (multiple scopes with duplicates)", () => {
        expect(
            controller["parseDetailedParams"]({
                params: {
                    vehicleId: "test",
                    gtfsTripId: "testTrip",
                },
                query: {
                    scopes: [
                        DetailedTripScope.Info,
                        DetailedTripScope.Info,
                        DetailedTripScope.Shapes,
                        DetailedTripScope.StopTimes,
                        DetailedTripScope.VehicleDescriptor,
                    ],
                },
            } as any)
        ).to.deep.equal({
            vehicleId: "test",
            tripId: "testTrip",
            scopes: [
                DetailedTripScope.Info,
                DetailedTripScope.Shapes,
                DetailedTripScope.StopTimes,
                DetailedTripScope.VehicleDescriptor,
            ],
        });
    });
});
