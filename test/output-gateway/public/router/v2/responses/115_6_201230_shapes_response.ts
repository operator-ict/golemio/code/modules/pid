import { IPublicApiGtfsTripScopeShapes } from "#og/public/domain/PublicApiGtfsTripLookupInterfaces";

export const shapesResponse: IPublicApiGtfsTripScopeShapes = {
    features: [
        {
            geometry: { coordinates: [14.491693, 50.03086], type: "Point" },
            properties: { shape_dist_traveled: 0 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.491434, 50.031104], type: "Point" },
            properties: { shape_dist_traveled: 0.032964 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.491026, 50.031368], type: "Point" },
            properties: { shape_dist_traveled: 0.074365 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.490552, 50.031608], type: "Point" },
            properties: { shape_dist_traveled: 0.117504 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.490206, 50.031777], type: "Point" },
            properties: { shape_dist_traveled: 0.148617 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.489322, 50.032225], type: "Point" },
            properties: { shape_dist_traveled: 0.229239 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.489122, 50.032371], type: "Point" },
            properties: { shape_dist_traveled: 0.250872 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.489035, 50.032518], type: "Point" },
            properties: { shape_dist_traveled: 0.268337 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.489086, 50.03274], type: "Point" },
            properties: { shape_dist_traveled: 0.293337 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.48959, 50.03302], type: "Point" },
            properties: { shape_dist_traveled: 0.341044 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.490104, 50.033256], type: "Point" },
            properties: { shape_dist_traveled: 0.386222 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.490272, 50.033325], type: "Point" },
            properties: { shape_dist_traveled: 0.400539 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.49089, 50.03367], type: "Point" },
            properties: { shape_dist_traveled: 0.459063 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.491028, 50.033746], type: "Point" },
            properties: { shape_dist_traveled: 0.472101 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.491292, 50.033896], type: "Point" },
            properties: { shape_dist_traveled: 0.49734 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.491723, 50.034125], type: "Point" },
            properties: { shape_dist_traveled: 0.537303 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.491925, 50.034233], type: "Point" },
            properties: { shape_dist_traveled: 0.556171 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.492128, 50.034342], type: "Point" },
            properties: { shape_dist_traveled: 0.575039 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.492263, 50.034435], type: "Point" },
            properties: { shape_dist_traveled: 0.589251 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.492507, 50.034611], type: "Point" },
            properties: { shape_dist_traveled: 0.6155 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.492706, 50.034737], type: "Point" },
            properties: { shape_dist_traveled: 0.6355 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.49423, 50.035639], type: "Point" },
            properties: { shape_dist_traveled: 0.783758 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.494346, 50.035707], type: "Point" },
            properties: { shape_dist_traveled: 0.795006 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.494947, 50.03606], type: "Point" },
            properties: { shape_dist_traveled: 0.853255 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.495169, 50.036079], type: "Point" },
            properties: { shape_dist_traveled: 0.869255 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.495327, 50.03613], type: "Point" },
            properties: { shape_dist_traveled: 0.881904 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.495389, 50.036235], type: "Point" },
            properties: { shape_dist_traveled: 0.894434 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.495349, 50.036358], type: "Point" },
            properties: { shape_dist_traveled: 0.90847 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.49549, 50.036489], type: "Point" },
            properties: { shape_dist_traveled: 0.926162 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.496025, 50.036826], type: "Point" },
            properties: { shape_dist_traveled: 0.979762 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.496412, 50.03706], type: "Point" },
            properties: { shape_dist_traveled: 1.017775 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.496741, 50.037234], type: "Point" },
            properties: { shape_dist_traveled: 1.048304 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.496999, 50.037347], type: "Point" },
            properties: { shape_dist_traveled: 1.070664 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.497205, 50.037438], type: "Point" },
            properties: { shape_dist_traveled: 1.088553 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.497627, 50.037575], type: "Point" },
            properties: { shape_dist_traveled: 1.122391 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.497851, 50.037649], type: "Point" },
            properties: { shape_dist_traveled: 1.140419 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.49781, 50.037709], type: "Point" },
            properties: { shape_dist_traveled: 1.147699 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.497309, 50.038409], type: "Point" },
            properties: { shape_dist_traveled: 1.233425 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.497011, 50.038819], type: "Point" },
            properties: { shape_dist_traveled: 1.283714 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.498383, 50.039393], type: "Point" },
            properties: { shape_dist_traveled: 1.400915 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.500452, 50.040265], type: "Point" },
            properties: { shape_dist_traveled: 1.578048 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.500525, 50.040389], type: "Point" },
            properties: { shape_dist_traveled: 1.592812 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.500522, 50.04047], type: "Point" },
            properties: { shape_dist_traveled: 1.601868 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.500234, 50.040763], type: "Point" },
            properties: { shape_dist_traveled: 1.640352 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.500053, 50.041019], type: "Point" },
            properties: { shape_dist_traveled: 1.671672 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.500037, 50.041226], type: "Point" },
            properties: { shape_dist_traveled: 1.694759 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.499982, 50.041341], type: "Point" },
            properties: { shape_dist_traveled: 1.708076 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.499886, 50.04154], type: "Point" },
            properties: { shape_dist_traveled: 1.731256 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.499932, 50.041589], type: "Point" },
            properties: { shape_dist_traveled: 1.737659 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.49996, 50.041655], type: "Point" },
            properties: { shape_dist_traveled: 1.745275 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.499949, 50.041709], type: "Point" },
            properties: { shape_dist_traveled: 1.751275 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.499884, 50.041748], type: "Point" },
            properties: { shape_dist_traveled: 1.757678 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.499799, 50.04175], type: "Point" },
            properties: { shape_dist_traveled: 1.763761 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.49973, 50.041744], type: "Point" },
            properties: { shape_dist_traveled: 1.768761 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.499681, 50.041712], type: "Point" },
            properties: { shape_dist_traveled: 1.773761 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.49965, 50.041655], type: "Point" },
            properties: { shape_dist_traveled: 1.780469 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.499676, 50.041603], type: "Point" },
            properties: { shape_dist_traveled: 1.786552 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.49974, 50.041563], type: "Point" },
            properties: { shape_dist_traveled: 1.792955 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.499855, 50.041555], type: "Point" },
            properties: { shape_dist_traveled: 1.801201 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.499886, 50.041476], type: "Point" },
            properties: { shape_dist_traveled: 1.810256 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.50001, 50.041224], type: "Point" },
            properties: { shape_dist_traveled: 1.839684 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.500025, 50.041017], type: "Point" },
            properties: { shape_dist_traveled: 1.862771 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.500195, 50.04075], type: "Point" },
            properties: { shape_dist_traveled: 1.894787 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.500478, 50.040476], type: "Point" },
            properties: { shape_dist_traveled: 1.931461 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.500525, 50.040389], type: "Point" },
            properties: { shape_dist_traveled: 1.941659 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.500452, 50.040265], type: "Point" },
            properties: { shape_dist_traveled: 1.956424 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.498383, 50.039393], type: "Point" },
            properties: { shape_dist_traveled: 2.133556 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.497011, 50.038819], type: "Point" },
            properties: { shape_dist_traveled: 2.250757 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.496078, 50.040098], type: "Point" },
            properties: { shape_dist_traveled: 2.407948 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.495732, 50.040467], type: "Point" },
            properties: { shape_dist_traveled: 2.455875 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.495234, 50.04107], type: "Point" },
            properties: { shape_dist_traveled: 2.531837 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.494935, 50.041431], type: "Point" },
            properties: { shape_dist_traveled: 2.577366 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.494543, 50.041887], type: "Point" },
            properties: { shape_dist_traveled: 2.635305 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.493746, 50.042787], type: "Point" },
            properties: { shape_dist_traveled: 2.750622 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.492869, 50.042556], type: "Point" },
            properties: { shape_dist_traveled: 2.818471 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.492305, 50.042407], type: "Point" },
            properties: { shape_dist_traveled: 2.862193 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.491248, 50.042141], type: "Point" },
            properties: { shape_dist_traveled: 2.943446 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.490815, 50.042058], type: "Point" },
            properties: { shape_dist_traveled: 2.975834 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.490548, 50.042052], type: "Point" },
            properties: { shape_dist_traveled: 2.994939 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.490466, 50.041909], type: "Point" },
            properties: { shape_dist_traveled: 3.011939 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.490203, 50.041686], type: "Point" },
            properties: { shape_dist_traveled: 3.043052 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.489287, 50.041025], type: "Point" },
            properties: { shape_dist_traveled: 3.141647 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.489141, 50.040912], type: "Point" },
            properties: { shape_dist_traveled: 3.157926 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.489719, 50.040582], type: "Point" },
            properties: { shape_dist_traveled: 3.213243 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.48995, 50.040357], type: "Point" },
            properties: { shape_dist_traveled: 3.24321 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.490411, 50.039644], type: "Point" },
            properties: { shape_dist_traveled: 3.329076 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.49049, 50.039547], type: "Point" },
            properties: { shape_dist_traveled: 3.341348 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.490774, 50.039195], type: "Point" },
            properties: { shape_dist_traveled: 3.385378 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.491056, 50.03893], type: "Point" },
            properties: { shape_dist_traveled: 3.421156 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.491345, 50.038629], type: "Point" },
            properties: { shape_dist_traveled: 3.460551 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.491715, 50.038344], type: "Point" },
            properties: { shape_dist_traveled: 3.501891 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.492412, 50.037851], type: "Point" },
            properties: { shape_dist_traveled: 3.575952 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.492487, 50.037695], type: "Point" },
            properties: { shape_dist_traveled: 3.5942 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.492661, 50.037538], type: "Point" },
            properties: { shape_dist_traveled: 3.615671 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.493674, 50.036883], type: "Point" },
            properties: { shape_dist_traveled: 3.718472 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.494215, 50.036522], type: "Point" },
            properties: { shape_dist_traveled: 3.774275 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.494232, 50.036442], type: "Point" },
            properties: { shape_dist_traveled: 3.783275 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.494407, 50.036285], type: "Point" },
            properties: { shape_dist_traveled: 3.804746 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.494502, 50.036166], type: "Point" },
            properties: { shape_dist_traveled: 3.819612 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.494481, 50.036001], type: "Point" },
            properties: { shape_dist_traveled: 3.838051 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.49431, 50.035877], type: "Point" },
            properties: { shape_dist_traveled: 3.85649 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.493731, 50.035514], type: "Point" },
            properties: { shape_dist_traveled: 3.914325 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.493315, 50.035254], type: "Point" },
            properties: { shape_dist_traveled: 3.955893 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.493005, 50.035054], type: "Point" },
            properties: { shape_dist_traveled: 3.987294 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.492691, 50.034872], type: "Point" },
            properties: { shape_dist_traveled: 4.017526 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.492427, 50.034722], type: "Point" },
            properties: { shape_dist_traveled: 4.042765 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.492217, 50.034585], type: "Point" },
            properties: { shape_dist_traveled: 4.064166 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.492054, 50.034489], type: "Point" },
            properties: { shape_dist_traveled: 4.079978 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.491867, 50.034373], type: "Point" },
            properties: { shape_dist_traveled: 4.098579 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.491679, 50.034266], type: "Point" },
            properties: { shape_dist_traveled: 4.116606 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.490927, 50.033828], type: "Point" },
            properties: { shape_dist_traveled: 4.189277 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.490788, 50.033752], type: "Point" },
            properties: { shape_dist_traveled: 4.202315 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.490067, 50.033362], type: "Point" },
            properties: { shape_dist_traveled: 4.269732 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.489499, 50.033121], type: "Point" },
            properties: { shape_dist_traveled: 4.318486 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.489342, 50.033062], type: "Point" },
            properties: { shape_dist_traveled: 4.331486 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.48885, 50.032991], type: "Point" },
            properties: { shape_dist_traveled: 4.367611 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.488598, 50.033114], type: "Point" },
            properties: { shape_dist_traveled: 4.390239 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.4883, 50.033124], type: "Point" },
            properties: { shape_dist_traveled: 4.411616 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.487893, 50.033052], type: "Point" },
            properties: { shape_dist_traveled: 4.441882 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.487609, 50.032864], type: "Point" },
            properties: { shape_dist_traveled: 4.471088 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.48757, 50.032715], type: "Point" },
            properties: { shape_dist_traveled: 4.487851 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.487631, 50.032494], type: "Point" },
            properties: { shape_dist_traveled: 4.512871 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.487813, 50.032301], type: "Point" },
            properties: { shape_dist_traveled: 4.537951 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.48836, 50.032177], type: "Point" },
            properties: { shape_dist_traveled: 4.579544 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.488457, 50.032185], type: "Point" },
            properties: { shape_dist_traveled: 4.586544 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.488606, 50.032217], type: "Point" },
            properties: { shape_dist_traveled: 4.597724 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.488867, 50.032312], type: "Point" },
            properties: { shape_dist_traveled: 4.619265 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.491118, 50.031131], type: "Point" },
            properties: { shape_dist_traveled: 4.827275 },
            type: "Feature",
        },
        {
            geometry: { coordinates: [14.491502, 50.030786], type: "Point" },
            properties: { shape_dist_traveled: 4.874506 },
            type: "Feature",
        },
    ],
    type: "FeatureCollection",
};
