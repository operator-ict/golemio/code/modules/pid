import { CustomStopIdGroupValidator } from "#og/public/routers/v2/helpers/CustomStopIdGroupValidator";
import { expect } from "chai";

describe("CustomStopIdGroupValidator", () => {
    const stops = [
        "U717Z5P",
        "U718Z5P",
        "U719Z5P",
        "U720Z5P",
        "U721Z5P",
        "U722Z5P",
        "U723Z5P",
        "U724Z5P",
        "U725Z5P",
        "U726Z5P",
        "U727Z5P",
    ];

    it("should validate stop id group (single group, not array)", () => {
        const stopIdGroup = '{"0": ["U717Z5P"]}';
        expect(CustomStopIdGroupValidator.validate(stopIdGroup, null as any)).to.eql(true);
    });

    it("should validate stop id group (single group, array)", () => {
        const stopIdGroup = ['{"0": ["U717Z5P"]}'];
        expect(CustomStopIdGroupValidator.validate(stopIdGroup, null as any)).to.eql(true);
    });

    it("should validate stop id group (multiple groups)", () => {
        const stopIdGroup = ['{"0": ["U717Z5P"]}', '{"1": ["U718Z5P"]}'];
        expect(CustomStopIdGroupValidator.validate(stopIdGroup, null as any)).to.eql(true);
    });

    it("should validate stop id group (single group, with 50 stops)", () => {
        const stopsString = stops.slice(0, 10).join('","') + '","';
        const stopIdGroup = `{"0": ["${stopsString.repeat(5).slice(0, -2)}]}`;

        expect(CustomStopIdGroupValidator.validate(stopIdGroup, null as any)).to.eql(true);
    });

    it("should validate stop id group (multiple groups, with 50 stops)", () => {
        const stopsString = stops.slice(0, 10).join('","') + '","';
        const stopIdGroup = `{"0": ["${stopsString.repeat(2).slice(0, -2)}]}`;
        const stopIdGroup2 = `{"1": ["${stopsString.repeat(3).slice(0, -2)}]}`;

        expect(CustomStopIdGroupValidator.validate([stopIdGroup, stopIdGroup2], null as any)).to.eql(true);
    });

    it("should validate stop id group (50 groups, with 1 stop each)", () => {
        const stopIdGroup = [];

        for (let i = 0; i < 50; i++) {
            const stop = stops[Math.floor(Math.random() * stops.length)];
            stopIdGroup.push(`{"${i}": ["${stop}"]}`);
        }

        expect(CustomStopIdGroupValidator.validate(stopIdGroup, null as any)).to.eql(true);
    });

    it("should not validate stop id group (empty array)", () => {
        const stopIdGroup: string[] = [];
        expect(CustomStopIdGroupValidator.validate(stopIdGroup, null as any)).to.eql(false);
    });

    it("should not validate stop id group (invalid priority)", () => {
        const stopIdGroup = '{"a": ["U717Z5P"]}';
        expect(CustomStopIdGroupValidator.validate(stopIdGroup, null as any)).to.eql(false);
    });

    it("should not validate stop id group (invalid stop id)", () => {
        const stopIdGroup = '{"0": ["U717Z5P", 123]}';
        expect(CustomStopIdGroupValidator.validate(stopIdGroup, null as any)).to.eql(false);
    });

    it("should not validate stop id group (multiple priorities within group)", () => {
        const stopIdGroup = '{"0": ["U717Z5P"], "1": ["U718Z5P"]}';
        expect(CustomStopIdGroupValidator.validate(stopIdGroup, null as any)).to.eql(false);
    });

    it("should not validate stop id group (stop ids not array)", () => {
        const stopIdGroup = '{"0": "U717Z5P"}';
        expect(CustomStopIdGroupValidator.validate(stopIdGroup, null as any)).to.eql(false);
    });

    it("should not validate stop id group (empty stop ids)", () => {
        const stopIdGroup = '{"0": []}';
        expect(CustomStopIdGroupValidator.validate(stopIdGroup, null as any)).to.eql(false);
    });

    it("should not validate stop id group (too many groups)", () => {
        let stopIdGroup = [];

        for (let i = 0; i < 51; i++) {
            const stop = stops[Math.floor(Math.random() * stops.length)];
            stopIdGroup.push(`{"${i}": ["${stop}"]}`);
        }

        expect(CustomStopIdGroupValidator.validate(stopIdGroup, null as any)).to.eql(false);
    });

    it("should not validate stop id group (too many stop ids within group)", () => {
        const stopsString = stops.join('","') + '","';
        const stopIdGroup = `{"0": ["${stopsString.repeat(5).slice(0, -2)}]}`;

        expect(CustomStopIdGroupValidator.validate(stopIdGroup, null as any)).to.eql(false);
    });

    it("should not validate stop id group (empty stop id)", () => {
        const stopIdGroup = '{"0": [""]}';
        expect(CustomStopIdGroupValidator.validate(stopIdGroup, null as any)).to.eql(false);
    });

    it("should not validate stop id group (too long stop id)", () => {
        const stopIdGroup = '{"0": ["U717Z5PU717Z5PU717Z5PU717Z5PUTA"]}';
        expect(CustomStopIdGroupValidator.validate(stopIdGroup, null as any)).to.eql(false);
    });
});
