import { CustomBBoxValidator } from "#og/public/routers/v2/helpers/CustomBBoxValidator";
import { expect } from "chai";

describe("CustomBBoxValidator", () => {
    const bboxes = [
        "50.073619,14.414826,50.092867,14.438086", // Prague city center
        "49.833927,14.242696,49.92817,14.339513", // Central Bohemia
        "47.65,11.91,51.98,19.36", // Central Europe
        "-19.4824,19.9415,-14.27,23.8965", // South Africa
        "-19.503608,-45.168596,-19.239317,-44.921404", // Brazil
        "60.726704,-9.367915,49.217589,2.576191", // reported as invalid previously
    ];

    const invalidBboxes = [
        "90.073619,14.414826,50.092867,14.438086", // invalid topLat (high)
        "-90.073619,14.414826,50.092867,14.438086", // invalid topLat (low)
        "50.073619,180.414826,50.092867,14.438086", // invalid topLon (high)
        "50.073619,-180.414826,50.092867,14.438086", // invalid topLon (low)
        "50.073619,14.414826,90.092867,14.438086", // invalid bottomLat (high)
        "50.073619,14.414826,-90.092867,14.438086", // invalid bottomLat (low)
        "50.073619,14.414826,50.092867,180.438086", // invalid bottomLon (high)
        "50.073619,14.414826,50.092867,-180.438086", // invalid bottomLon (low)
    ];

    it("should return true for valid bounding box", () => {
        for (const bbox of bboxes) {
            expect(CustomBBoxValidator.validate(bbox, null as any)).to.be.true;
        }
    });

    it("should return false for invalid bounding box", () => {
        for (const bbox of invalidBboxes) {
            expect(CustomBBoxValidator.validate(bbox, null as any)).to.be.false;
        }
    });

    it("should return false for invalid input (empty string)", () => {
        expect(CustomBBoxValidator.validate("", null as any)).to.be.false;
    });

    it("should return false for invalid input (white space)", () => {
        expect(CustomBBoxValidator.validate("   ", null as any)).to.be.false;
    });

    it("should return false for invalid input (invalid string)", () => {
        expect(CustomBBoxValidator.validate("invalid input", null as any)).to.be.false;
    });

    it("should return false for invalid input (array)", () => {
        expect(CustomBBoxValidator.validate(bboxes, null as any)).to.be.false;
    });

    it("should return false for invalid input (length > 80)", () => {
        expect(
            CustomBBoxValidator.validate(
                // eslint-disable-next-line max-len
                "50.0736190123012301230123012301230123,14.4148260123012301230123012301230123,50.0928670123012301230123012301230123,14.4380860123012301230123012301230123",
                null as any
            )
        ).to.be.false;
    });
});
