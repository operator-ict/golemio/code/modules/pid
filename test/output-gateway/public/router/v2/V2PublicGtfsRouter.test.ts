import { v2PublicGtfsRouter } from "#og/public";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import express, { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { HTTPErrorHandler } from "@golemio/core/dist/shared/golemio-errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";
import request from "supertest";
import { shapesResponse } from "./responses/115_6_201230_shapes_response";
import { stopTimesResponse } from "./responses/115_6_201230_stop_times_response";
import { OutputGatewayContainer } from "@golemio/core/dist/output-gateway/ioc/Di";
import { IoRedisConnector } from "@golemio/core/dist/helpers/data-access/redis/IoRedisConnector";
import { ContainerToken } from "@golemio/core/dist/output-gateway/ioc";

chai.use(chaiAsPromised);

describe("V2PublicGtfsRouter", () => {
    const app = express();

    let sandbox: SinonSandbox | null = null;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox && sandbox.restore();
    });

    before(async () => {
        const redisConnector = OutputGatewayContainer.resolve<IoRedisConnector>(ContainerToken.RedisConnector);
        await redisConnector.connect();
        app.use(v2PublicGtfsRouter.getPath(), v2PublicGtfsRouter.getRouter());
        app.use((err: any, _req: Request, res: Response, _next: NextFunction) => {
            const error = HTTPErrorHandler.handle(err, log);
            if (error) {
                res.setHeader("Content-Type", "application/json; charset=utf-8");
                res.status(error.error_status || 500).send(error);
            }
        });
    });

    it("should respond with JSON and have the correct form", (done) => {
        request(app)
            .get("/v2/public/gtfs/trips/115_6_201230")
            .query({
                "scopes[0]": "info",
                "scopes[1]": "stop_times",
                "scopes[2]": "shapes",
                "scopes[3]": "vehicle_descriptor",
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200, (_err, res) => {
                expect(res.body).to.deep.equal({
                    gtfs_trip_id: "115_6_201230",
                    route_type: "bus",
                    route_short_name: "115",
                    shape_id: "L115V1",
                    origin_route_name: "115",
                    run_number: 39,
                    trip_headsign: "Chodov",
                    stop_times: stopTimesResponse,
                    shapes: shapesResponse,
                    vehicle_descriptor: {
                        is_wheelchair_accessible: true,
                    },
                });
                done();
            });
    });

    it("should return bad request error if invalid scope is provided", (done) => {
        request(app)
            .get("/v2/public/gtfs/trips/115_6_201230")
            .query({
                scopes: "blbost",
            })
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400, (_err, res) => {
                expect(res.body).to.deep.equal({
                    error_info:
                        '{"scopes":{"type":"field","value":"blbost","msg":"Invalid value","path":"scopes","location":"query"}}',
                    error_message: "Bad request",
                    error_status: 400,
                });
                done();
            });
    });
});
