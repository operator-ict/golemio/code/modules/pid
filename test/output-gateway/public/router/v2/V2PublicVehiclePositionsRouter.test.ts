import { v2PublicVPRouter } from "#og/public";
import { OgPublicContainer } from "#og/public/ioc/Di";
import { OgModuleToken } from "#og/public/ioc/OgModuleToken";
import { GTFS_DELAY_COMPUTATION_NAMESPACE_PREFIX, PUBLIC_CACHE_NAMESPACE_PREFIX } from "#sch/vehicle-positions/redis/const";
import { IPublicApiCacheDto } from "#sch/vehicle-positions/redis/interfaces/IPublicApiCacheDto";
import { IoRedisConnector } from "@golemio/core/dist/helpers/data-access/redis/IoRedisConnector";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import { ContainerToken, OutputGatewayContainer } from "@golemio/core/dist/output-gateway/ioc";
import express, { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { HTTPErrorHandler } from "@golemio/core/dist/shared/golemio-errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";
import { StatePositionEnum } from "src/const";
import request from "supertest";

chai.use(chaiAsPromised);

describe("V2PublicVehiclePositionsRouter", () => {
    const app = express();

    const vehiclePositions: IPublicApiCacheDto[] = [
        {
            gtfs_trip_id: "94_979_230320",
            route_type: 0,
            gtfs_route_short_name: "94",
            lat: 50.03141,
            lng: 14.36749,
            bearing: 257,
            delay: 10,
            state_position: StatePositionEnum.ON_TRACK,
            vehicle_id: "service-0-8388",
            detailed_info: {
                is_wheelchair_accessible: true,
                origin_route_name: "94",
                shape_id: null,
                run_number: 1,
                trip_headsign: "Sídliště Řepy",
                shape_dist_traveled: 7.035025,
                last_stop_sequence: 1,
                origin_timestamp: "2024-03-23T14:00:00.000Z",
                operator: null,
                registration_number: null,
            },
        },
        {
            gtfs_trip_id: "9_4832_220502",
            route_type: 0,
            gtfs_route_short_name: "9",
            lat: 50.03141,
            lng: 14.36749,
            bearing: 258,
            delay: 1,
            state_position: StatePositionEnum.ON_TRACK,
            vehicle_id: "service-0-9366",
            detailed_info: {
                is_wheelchair_accessible: true,
                origin_route_name: "9",
                shape_id: null,
                run_number: 20,
                trip_headsign: "Sídliště Řepy",
                shape_dist_traveled: 7.035026,
                last_stop_sequence: 1,
                origin_timestamp: "2024-04-08T05:59:00.000Z",
                operator: null,
                registration_number: null,
            },
        },
    ];

    let sandbox: SinonSandbox | null = null;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox && sandbox.restore();
    });

    before(async () => {
        const redisConnector = OutputGatewayContainer.resolve<IoRedisConnector>(ContainerToken.RedisConnector);
        await redisConnector.connect();
        const pipeline = redisConnector.getConnection().pipeline();
        app.use(v2PublicVPRouter.getPath(), v2PublicVPRouter.getRouter());
        app.use((err: any, _req: Request, res: Response, _next: NextFunction) => {
            const error = HTTPErrorHandler.handle(err, log);
            if (error) {
                res.setHeader("Content-Type", "application/json; charset=utf-8");
                res.status(error.error_status || 500).send(error);
            }
        });

        const setName = `${PUBLIC_CACHE_NAMESPACE_PREFIX}:test`;

        for (const tripPosition of vehiclePositions) {
            pipeline.geoadd(setName, tripPosition.lng, tripPosition.lat, tripPosition.vehicle_id);
            pipeline.set(`${setName}:vehicle-${tripPosition.vehicle_id}`, JSON.stringify(tripPosition));
            pipeline.set(
                `${GTFS_DELAY_COMPUTATION_NAMESPACE_PREFIX}:${tripPosition.gtfs_trip_id}`,
                JSON.stringify({
                    stop_times: [],
                    shapes: [],
                })
            );
        }

        await pipeline.exec();
        OgPublicContainer.resolve<any>(OgModuleToken.PublicVehiclePositionsRepository).setCurrentSetName(setName);
    });

    it("should respond with JSON and have the correct form", (done) => {
        request(app)
            .get("/v2/public/vehiclepositions/")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200, (_err, res) => {
                expect(res.body).to.deep.equal({
                    features: [
                        {
                            geometry: { coordinates: [14.36749, 50.03141], type: "Point" },
                            properties: {
                                gtfs_trip_id: "94_979_230320",
                                route_type: "tram",
                                gtfs_route_short_name: "94",
                                bearing: 257,
                                delay: 10,
                                state_position: "on_track",
                                vehicle_id: "service-0-8388",
                            },
                            type: "Feature",
                        },
                        {
                            geometry: { coordinates: [14.36749, 50.03141], type: "Point" },
                            properties: {
                                gtfs_trip_id: "9_4832_220502",
                                route_type: "tram",
                                gtfs_route_short_name: "9",
                                bearing: 258,
                                delay: 1,
                                state_position: "on_track",
                                vehicle_id: "service-0-9366",
                            },
                            type: "Feature",
                        },
                    ],
                    type: "FeatureCollection",
                });
                done();
            });
    });

    it("should return state position and vehicle id", (done) => {
        request(app)
            .get("/v2/public/vehiclepositions/")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200, (_err, res) => {
                expect(res.body.features[0].properties).to.have.property("vehicle_id");
                expect(res.body.features[0].properties).to.have.property("state_position");
                done();
            });
    });

    it("should return correct result with single routeShortName value", (done) => {
        request(app)
            .get("/v2/public/vehiclepositions/?routeShortName=94")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200, (_err, res) => {
                expect(res.body).to.deep.equal({
                    features: [
                        {
                            geometry: { coordinates: [14.36749, 50.03141], type: "Point" },
                            properties: {
                                gtfs_trip_id: "94_979_230320",
                                route_type: "tram",
                                gtfs_route_short_name: "94",
                                bearing: 257,
                                delay: 10,
                                state_position: "on_track",
                                vehicle_id: "service-0-8388",
                            },
                            type: "Feature",
                        },
                    ],
                    type: "FeatureCollection",
                });
                done();
            });
    });

    it("should return correct result with multiple routeShortName values", (done) => {
        request(app)
            .get("/v2/public/vehiclepositions/?routeShortName=9&routeShortName=94")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200, (_err, res) => {
                expect(res.body).to.deep.equal({
                    features: [
                        {
                            geometry: { coordinates: [14.36749, 50.03141], type: "Point" },
                            properties: {
                                gtfs_trip_id: "94_979_230320",
                                route_type: "tram",
                                gtfs_route_short_name: "94",
                                bearing: 257,
                                delay: 10,
                                state_position: "on_track",
                                vehicle_id: "service-0-8388",
                            },
                            type: "Feature",
                        },
                        {
                            geometry: { coordinates: [14.36749, 50.03141], type: "Point" },
                            properties: {
                                gtfs_trip_id: "9_4832_220502",
                                route_type: "tram",
                                gtfs_route_short_name: "9",
                                bearing: 258,
                                delay: 1,
                                state_position: "on_track",
                                vehicle_id: "service-0-9366",
                            },
                            type: "Feature",
                        },
                    ],
                    type: "FeatureCollection",
                });
                done();
            });
    });

    describe("/v2/public/vehiclepositions/{vehicleId}", () => {
        it("should return vehicle position", (done) => {
            request(app)
                .get("/v2/public/vehiclepositions/service-0-8388?scopes=info")
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(200, (_err, res) => {
                    expect(res.body).to.deep.equal({
                        gtfs_trip_id: "94_979_230320",
                        route_type: "tram",
                        route_short_name: "94",
                        shape_id: null,
                        origin_route_name: "94",
                        run_number: 1,
                        trip_headsign: "Sídliště Řepy",
                        geometry: { type: "Point", coordinates: [14.36749, 50.03141] },
                        shape_dist_traveled: 7.035025,
                        bearing: 257,
                        delay: 10,
                        state_position: "on_track",
                        last_stop_sequence: 1,
                        origin_timestamp: "2024-03-23T15:00:00+01:00",
                    });

                    done();
                });
        });

        it("should return bad request error if invalid scope is provided", (done) => {
            request(app)
                .get("/v2/public/vehiclepositions/service-0-8388?scopes=test")
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(400, (_err, res) => {
                    expect(res.body).to.deep.equal({
                        error_info:
                            '{"scopes":{"type":"field","value":"test","msg":"Invalid value","path":"scopes","location":"query"}}',
                        error_message: "Bad request",
                        error_status: 400,
                    });
                    done();
                });
        });
    });
});
