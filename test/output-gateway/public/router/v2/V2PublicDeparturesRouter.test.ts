import { GTFSRouteTypeEnum } from "#helpers/RouteTypeEnums";
import { v2PublicDeparturesRouter } from "#og/public";
import { OgPublicContainer } from "#og/public/ioc/Di";
import { OgModuleToken } from "#og/public/ioc/OgModuleToken";
import { PUBLIC_DEPARTURE_NAMESPACE_PREFIX } from "#sch/ropid-gtfs/redis/const";
import { IPublicGtfsDepartureCacheDto } from "#sch/ropid-gtfs/redis/interfaces/IPublicGtfsDepartureCacheDto";
import {
    GTFS_DELAY_COMPUTATION_NAMESPACE_PREFIX,
    PUBLIC_CACHE_NAMESPACE_PREFIX,
    PUBLIC_STOP_TIME_CACHE_NAMESPACE_PREFIX,
} from "#sch/vehicle-positions/redis/const";
import { IPublicApiCacheDto } from "#sch/vehicle-positions/redis/interfaces/IPublicApiCacheDto";
import { IPublicStopTimeCacheDto } from "#sch/vehicle-positions/redis/interfaces/IPublicStopTimeCacheDto";
import { DatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/DatabaseConnector";
import { IoRedisConnector } from "@golemio/core/dist/helpers/data-access/redis/IoRedisConnector";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import { ContainerToken, OutputGatewayContainer } from "@golemio/core/dist/output-gateway/ioc";
import express, { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { HTTPErrorHandler } from "@golemio/core/dist/shared/golemio-errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";
import { StatePositionEnum } from "src/const";
import request from "supertest";

chai.use(chaiAsPromised);

describe("V2PublicDeparturesRouter", () => {
    const postgresConnector = OutputGatewayContainer.resolve<DatabaseConnector>(ContainerToken.PostgresDatabase);
    const redisConnector = OutputGatewayContainer.resolve<IoRedisConnector>(ContainerToken.RedisConnector);
    const setName = `${PUBLIC_CACHE_NAMESPACE_PREFIX}:departure-test`;
    const app = express();

    const vehiclePositions: IPublicApiCacheDto[] = [
        {
            gtfs_trip_id: "135_1001_240219",
            route_type: 3,
            gtfs_route_short_name: "135",
            lat: 50.07211,
            lng: 14.44531,
            bearing: 97,
            delay: 16,
            state_position: StatePositionEnum.ON_TRACK,
            vehicle_id: "service-3-6837",
            detailed_info: {
                is_wheelchair_accessible: true,
                origin_route_name: "135",
                shape_id: "L135V2",
                run_number: 10,
                trip_headsign: "Chodov",
                shape_dist_traveled: 3.248,
                last_stop_sequence: 7,
                origin_timestamp: "2024-04-17T17:34:59+02:00",
                registration_number: 6837,
                operator: "DP PRAHA",
            },
        },
        // train
        {
            gtfs_trip_id: "1309_9127_231211",
            route_type: 2,
            gtfs_route_short_name: "S9",
            lat: 50.0642624,
            lng: 14.48098,
            bearing: 103,
            delay: 13,
            state_position: StatePositionEnum.ON_TRACK,
            vehicle_id: "train-9127",
            detailed_info: {
                is_wheelchair_accessible: true,
                origin_route_name: null,
                shape_id: "L1309V10",
                run_number: null,
                trip_headsign: "Říčany",
                shape_dist_traveled: 5.229,
                last_stop_sequence: 4,
                origin_timestamp: "2024-06-21T09:39:59.000Z",
                registration_number: null,
                operator: "ČESKÉ DRÁHY",
            },
        },
        // canceled trip
        {
            gtfs_trip_id: "103_1237_241029",
            route_type: 3,
            gtfs_route_short_name: "103",
            lat: 0,
            lng: 0,
            bearing: null,
            delay: null,
            state_position: StatePositionEnum.CANCELED,
            vehicle_id: "",
            detailed_info: {
                is_wheelchair_accessible: null,
                origin_route_name: "103",
                shape_id: "L103V3",
                run_number: 6,
                trip_headsign: "Ládví",
                shape_dist_traveled: null,
                last_stop_sequence: null,
                origin_timestamp: "2024-11-18T07:33:28.000Z",
                registration_number: null,
                operator: "ČSAD SČ (Mělník)",
            },
        },
    ];

    const departures: IPublicGtfsDepartureCacheDto[] = [
        {
            stop_id: "U476Z2P",
            departure_datetime: new Date("2024-04-23T15:20:11+02:00").toISOString(),
            arrival_datetime: new Date("2024-04-23T15:20:11+02:00").toISOString(),
            route_short_name: "135",
            route_type: GTFSRouteTypeEnum.BUS,
            trip_id: "135_1001_240219",
            stop_sequence: 6,
            platform_code: "B",
            trip_headsign: "Chodov",
        },
        {
            stop_id: "U477Z2P",
            departure_datetime: new Date("2024-04-23T15:20:11+02:00").toISOString(),
            arrival_datetime: new Date("2024-04-23T15:20:11+02:00").toISOString(),
            route_short_name: "135",
            route_type: GTFSRouteTypeEnum.BUS,
            trip_id: "faulty-0",
            stop_sequence: 6,
            platform_code: "B",
            trip_headsign: "Chodov",
        },
        // Praha-Uhrineves train departure
        {
            stop_id: "U461Z301",
            departure_datetime: "2024-06-21T09:51:30.000Z",
            arrival_datetime: "2024-06-21T09:51:00.000Z",
            route_short_name: "S9",
            route_type: GTFSRouteTypeEnum.TRAIN,
            trip_id: "1309_9127_231211",
            stop_sequence: 8,
            platform_code: null,
            trip_headsign: "Říčany",
        },
        {
            stop_id: "U287Z6P",
            departure_datetime: new Date("2024-04-23T15:20:11+02:00").toISOString(),
            arrival_datetime: new Date("2024-04-23T15:20:11+02:00").toISOString(),
            route_short_name: "103",
            route_type: GTFSRouteTypeEnum.BUS,
            trip_id: "103_1237_241029",
            stop_sequence: 1,
            platform_code: "F",
            trip_headsign: "Ládví",
        },
    ];

    let sandbox: SinonSandbox | null = null;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox && sandbox.restore();
    });

    before(async () => {
        await postgresConnector.connect();
        await redisConnector.connect();

        app.use(v2PublicDeparturesRouter.getPath(), v2PublicDeparturesRouter.getRouter());
        app.use((err: any, _req: Request, res: Response, _next: NextFunction) => {
            const error = HTTPErrorHandler.handle(err, log);
            if (error) {
                res.setHeader("Content-Type", "application/json; charset=utf-8");
                res.status(error.error_status || 500).send(error);
            }
        });

        const connection = redisConnector.getConnection();

        for (const tripPosition of vehiclePositions) {
            if (tripPosition.state_position !== StatePositionEnum.CANCELED) {
                await connection.geoadd(setName, tripPosition.lng, tripPosition.lat, tripPosition.vehicle_id);
                await connection.rpush(`${setName}:trip-${tripPosition.gtfs_trip_id}`, tripPosition.vehicle_id);
                await connection.set(`${setName}:vehicle-${tripPosition.vehicle_id}`, JSON.stringify(tripPosition));
                await connection.set(
                    `${GTFS_DELAY_COMPUTATION_NAMESPACE_PREFIX}:${tripPosition.gtfs_trip_id}`,
                    JSON.stringify({
                        stop_times: [],
                        shapes: [],
                    })
                );
            } else {
                await connection.set(`${setName}:canceled-trips-${tripPosition.gtfs_trip_id}`, JSON.stringify(tripPosition));
            }
        }

        await connection.rpush(`${setName}:trip-faulty-0`, "faulty-vehicle-0");

        OgPublicContainer.resolve<any>(OgModuleToken.PublicVehiclePositionsRepository).setCurrentSetName(setName);
    });

    it("should respond with JSON and have the correct form", (done) => {
        const clock = sandbox!.useFakeTimers(new Date("2024-04-23T15:20:11+02:00").getTime());
        const connection = redisConnector.getConnection();

        connection.del(`${PUBLIC_DEPARTURE_NAMESPACE_PREFIX}:${departures[0].stop_id}`);
        connection.zadd(
            `${PUBLIC_DEPARTURE_NAMESPACE_PREFIX}:${departures[0].stop_id}`,
            new Date(departures[0].departure_datetime).getTime() / 1000,
            JSON.stringify(departures[0])
        );
        connection.del(`${PUBLIC_DEPARTURE_NAMESPACE_PREFIX}:${departures[3].stop_id}`);
        connection.zadd(
            `${PUBLIC_DEPARTURE_NAMESPACE_PREFIX}:${departures[3].stop_id}`,
            new Date(departures[3].departure_datetime).getTime() / 1000,
            JSON.stringify(departures[3])
        );

        request(app)
            .get("/v2/public/departureboards")
            .query({
                stopIds: JSON.stringify({ "0": ["U476Z2P", "U287Z6P"] }),
            })
            .expect("Content-Type", /json/)
            .expect(200, (_err, res) => {
                expect(res.body).to.deep.equal([
                    [
                        {
                            departure: {
                                timestamp_scheduled: "2024-04-23T15:20:11+02:00",
                                timestamp_predicted: "2024-04-23T15:20:11+02:00",
                                delay_seconds: null,
                                minutes: 0,
                            },
                            stop: { id: "U287Z6P", sequence: 1, platform_code: "F" },
                            route: { type: "bus", short_name: "103" },
                            trip: { id: "103_1237_241029", headsign: "Ládví", is_canceled: true },
                            vehicle: {
                                id: null,
                                is_wheelchair_accessible: null,
                                is_air_conditioned: null,
                                has_charger: null,
                            },
                        },
                        {
                            departure: {
                                timestamp_scheduled: "2024-04-23T15:20:11+02:00",
                                timestamp_predicted: "2024-04-23T15:20:27+02:00",
                                delay_seconds: 16,
                                minutes: 0,
                            },
                            stop: {
                                id: "U476Z2P",
                                sequence: 6,
                                platform_code: "B",
                            },
                            route: {
                                type: "bus",
                                short_name: "135",
                            },
                            trip: {
                                id: "135_1001_240219",
                                headsign: "Chodov",
                                is_canceled: false,
                            },
                            vehicle: {
                                id: "service-3-6837",
                                is_wheelchair_accessible: true,
                                is_air_conditioned: false,
                                has_charger: false,
                            },
                        },
                    ],
                ]);
                clock.restore();
                done();
            });
    });

    it("should respond with JSON without RT data when the vehicle data is missing", (done) => {
        const clock = sandbox!.useFakeTimers(new Date("2024-04-23T15:20:11+02:00").getTime());
        const connection = redisConnector.getConnection();

        connection.del(`${PUBLIC_DEPARTURE_NAMESPACE_PREFIX}:${departures[1].stop_id}`);
        connection.zadd(
            `${PUBLIC_DEPARTURE_NAMESPACE_PREFIX}:${departures[1].stop_id}`,
            new Date(departures[1].departure_datetime).getTime() / 1000,
            JSON.stringify(departures[1])
        );

        request(app)
            .get("/v2/public/departureboards")
            .query({
                "stopIds[0]": JSON.stringify({ "0": ["U477Z2P"] }),
            })
            .expect("Content-Type", /json/)
            .expect(200, (_err, res) => {
                expect(res.body).to.deep.equal([
                    [
                        {
                            departure: {
                                timestamp_scheduled: "2024-04-23T15:20:11+02:00",
                                timestamp_predicted: "2024-04-23T15:20:11+02:00",
                                delay_seconds: null,
                                minutes: 0,
                            },
                            stop: {
                                id: "U477Z2P",
                                sequence: 6,
                                platform_code: "B",
                            },
                            route: {
                                type: "bus",
                                short_name: "135",
                            },
                            trip: {
                                id: "faulty-0",
                                headsign: "Chodov",
                                is_canceled: false,
                            },
                            vehicle: {
                                id: null,
                                is_wheelchair_accessible: null,
                                is_air_conditioned: null,
                                has_charger: null,
                            },
                        },
                    ],
                ]);
                clock.restore();
                done();
            });
    });

    describe("Train departure", () => {
        // eslint-disable-next-line max-len
        const stopTimeRedisKey = `${PUBLIC_STOP_TIME_CACHE_NAMESPACE_PREFIX}:${vehiclePositions[1].vehicle_id}-${departures[2].trip_id}`;

        it("should respond with JSON and have the correct form (known stop time)", (done) => {
            const clock = sandbox!.useFakeTimers(new Date("2024-06-21T09:51:30.000Z").getTime());
            const connection = redisConnector.getConnection();

            connection.del(`${PUBLIC_DEPARTURE_NAMESPACE_PREFIX}:${departures[2].stop_id}`);
            connection.zadd(
                `${PUBLIC_DEPARTURE_NAMESPACE_PREFIX}:${departures[2].stop_id}`,
                new Date(departures[2].departure_datetime).getTime() / 1000,
                JSON.stringify(departures[2])
            );

            const stopTime: IPublicStopTimeCacheDto = {
                sequence: 8,
                arr_delay: 0,
                dep_delay: 0,
                cis_stop_platform_code: "2",
                platform_code: null,
                stop_id: "U461Z301",
            };

            connection.del(stopTimeRedisKey);
            connection.set(
                `${PUBLIC_STOP_TIME_CACHE_NAMESPACE_PREFIX}:${vehiclePositions[1].vehicle_id}-${departures[2].trip_id}`,
                JSON.stringify([stopTime])
            );

            request(app)
                .get("/v2/public/departureboards")
                .query({
                    "stopIds[0]": JSON.stringify({ "0": ["U461Z301"] }),
                })
                .expect("Content-Type", /json/)
                .expect(200, (_err, res) => {
                    expect(res.body).to.deep.equal([
                        [
                            {
                                departure: {
                                    timestamp_scheduled: "2024-06-21T11:51:30+02:00",
                                    timestamp_predicted: "2024-06-21T11:51:30+02:00",
                                    delay_seconds: 13,
                                    minutes: 0,
                                },
                                stop: {
                                    id: "U461Z301",
                                    sequence: 8,
                                    platform_code: "2",
                                },
                                route: {
                                    type: "train",
                                    short_name: "S9",
                                },
                                trip: {
                                    id: "1309_9127_231211",
                                    headsign: "Říčany",
                                    is_canceled: false,
                                },
                                vehicle: {
                                    id: "train-9127",
                                    is_wheelchair_accessible: true,
                                    is_air_conditioned: null,
                                    has_charger: null,
                                },
                            },
                        ],
                    ]);
                    clock.restore();
                    done();
                });
        });

        it("should respond with JSON and have the correct form (unknown stop time)", (done) => {
            const clock = sandbox!.useFakeTimers(new Date("2024-06-21T09:51:30.000Z").getTime());
            const connection = redisConnector.getConnection();

            connection.del(`${PUBLIC_DEPARTURE_NAMESPACE_PREFIX}:${departures[2].stop_id}`);
            connection.zadd(
                `${PUBLIC_DEPARTURE_NAMESPACE_PREFIX}:${departures[2].stop_id}`,
                new Date(departures[2].departure_datetime).getTime() / 1000,
                JSON.stringify(departures[2])
            );

            connection.del(stopTimeRedisKey);

            request(app)
                .get("/v2/public/departureboards")
                .query({
                    "stopIds[0]": JSON.stringify({ "0": ["U461Z301"] }),
                })
                .expect("Content-Type", /json/)
                .expect(200, (_err, res) => {
                    expect(res.body).to.deep.equal([
                        [
                            {
                                departure: {
                                    timestamp_scheduled: "2024-06-21T11:51:30+02:00",
                                    timestamp_predicted: "2024-06-21T11:51:30+02:00",
                                    delay_seconds: 13,
                                    minutes: 0,
                                },
                                stop: {
                                    id: "U461Z301",
                                    sequence: 8,
                                    platform_code: null,
                                },
                                route: {
                                    type: "train",
                                    short_name: "S9",
                                },
                                trip: {
                                    id: "1309_9127_231211",
                                    headsign: "Říčany",
                                    is_canceled: false,
                                },
                                vehicle: {
                                    id: "train-9127",
                                    is_wheelchair_accessible: true,
                                    is_air_conditioned: null,
                                    has_charger: null,
                                },
                            },
                        ],
                    ]);
                    clock.restore();
                    done();
                });
        });
    });
});
