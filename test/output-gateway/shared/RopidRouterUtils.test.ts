import sinon, { SinonSandbox } from "sinon";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { Transform } from "@golemio/core/dist/shared/class-transformer";
import moment from "@golemio/core/dist/shared/moment-timezone";
import { config } from "@golemio/core/dist/output-gateway/config";
import { RopidRouterUtils } from "#og/shared/RopidRouterUtils";

chai.use(chaiAsPromised);

describe("RopidRouterUtils", () => {
    let sandbox: SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        sandbox.stub(config.vehiclePositions, "defaultTimezone").value("Europe/Prague");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("mapObjectToDTOInstance should map plain object to custom class instance", () => {
        class Test {
            prop0: string = "default";
            prop1: string = "default";
            prop2?: string;
            @Transform(({ value }) => "_" + value)
            prop3?: string;
        }

        const inputObject = {
            prop0: "some",
            prop3: "value",
        };

        const expectedResult = {
            prop0: "some",
            prop1: "default",
            prop3: "_value",
        };

        const result = RopidRouterUtils.mapObjectToDTOInstance(Test, inputObject);
        expect(result).to.deep.eq(expectedResult);
    });

    it("formatTimestamp should return null", () => {
        expect(RopidRouterUtils.formatTimestamp(null, "Europe/Prague")).to.equal(null);
    });

    it("formatTimestamp should return a datetime", () => {
        expect(
            RopidRouterUtils.formatTimestamp(moment("2022-07-01 11:11:11.000Z").tz("Europe/Prague").toDate(), "Australia/Sydney")
        ).to.equal("2022-07-01T21:11:11+10:00");
    });

    it("getPreferredTimezone should return the default timezone", () => {
        expect(RopidRouterUtils.getPreferredTimezone(undefined)).to.equal("Europe/Prague");
    });

    it("getPreferredTimezone should return the preferred timezone (1)", () => {
        expect(RopidRouterUtils.getPreferredTimezone("Australia/Sydney")).to.equal("Australia/Sydney");
    });

    it("getPreferredTimezone should return the preferred timezone (2)", () => {
        expect(RopidRouterUtils.getPreferredTimezone("Australia_Sydney")).to.equal("Australia/Sydney");
    });
});
