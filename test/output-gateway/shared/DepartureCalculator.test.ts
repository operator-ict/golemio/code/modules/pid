import { DepartureCalculator } from "#og/shared/DepartureCalculator";
import { expect } from "chai";
import sinon, { SinonFakeTimers } from "sinon";

describe("DepartureCalculator", () => {
    let clock: SinonFakeTimers;

    before(() => {
        clock = sinon.useFakeTimers(new Date("2024-04-24T22:50:00+02:00").getTime());
    });

    after(() => {
        clock.restore();
    });

    it("should calculate predicted departure time (without arrival time)", () => {
        const departureTime = new Date("2024-04-24T22:50:00+02:00");
        const arrivalTime = null;
        const delayInSeconds = 69;

        const predictedDepartureTime = DepartureCalculator.getPredictedDepartureTime(departureTime, arrivalTime, delayInSeconds);
        expect(predictedDepartureTime).to.eql(new Date("2024-04-24T22:51:09+02:00"));
    });

    it("should calculate predicted departure time (ahead of time)", () => {
        const departureTime = new Date("2024-04-24T22:50:00+02:00");
        const arrivalTime = null;
        const delayInSeconds = -69;

        const predictedDepartureTime = DepartureCalculator.getPredictedDepartureTime(departureTime, arrivalTime, delayInSeconds);
        expect(predictedDepartureTime).to.eql(new Date("2024-04-24T22:48:51+02:00"));
    });

    it("should calculate predicted departure time (dwelling)", () => {
        const departureTime = new Date("2024-04-24T22:50:00+02:00");
        const arrivalTime = new Date("2024-04-24T22:49:00+02:00");
        const delayInSeconds = 69;

        const predictedDepartureTime = DepartureCalculator.getPredictedDepartureTime(departureTime, arrivalTime, delayInSeconds);
        expect(predictedDepartureTime).to.eql(new Date("2024-04-24T22:50:09+02:00"));
    });

    it("should calculate departure minutes", () => {
        const predictedDepartureTime = new Date("2024-04-24T22:55:00+02:00");

        const departureMinutes = DepartureCalculator.getDepartureMinutes(predictedDepartureTime);
        expect(departureMinutes).to.eql(5);
    });
});
