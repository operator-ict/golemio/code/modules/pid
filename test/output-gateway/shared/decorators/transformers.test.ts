import { expect } from "chai";
import { plainToInstance } from "@golemio/core/dist/shared/class-transformer";
import { TransformArray, TransformInteger, TransformBoolean } from "#og/shared/decorators/transformers";

describe("Decorators - Transformers", () => {
    it("TransformArray should transform string to array", () => {
        class Test {
            @TransformArray()
            prop!: string;
        }

        expect(plainToInstance(Test, { prop: "test" })).to.deep.eq({ prop: ["test"] });
    });

    it("TransformInteger should transform string to integer", () => {
        class Test {
            @TransformInteger()
            prop!: number;
        }

        expect(plainToInstance(Test, { prop: "42" })).to.deep.eq({ prop: 42 });
    });

    it("TransformBoolean should transform string to boolean", () => {
        class Test {
            @TransformBoolean()
            prop!: boolean;
        }

        expect(plainToInstance(Test, { prop: "true" })).to.deep.eq({ prop: true });
    });
});
