import { repositories } from "#og/vehicle-positions/data-access";
import moment from "@golemio/core/dist/shared/moment-timezone";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon from "sinon";
import { ArrayNotPublicRegistrationNumbers, StatePositionEnum } from "src/const";
import { MPVRouteTypesEnum } from "src/helpers/RouteTypeEnums";

chai.use(chaiAsPromised);

describe("TripWithLastPositionRepository", () => {
    const tripWithPositionRepository = repositories.tripWithLastPositionRepository;

    // Basic configuration: create a sinon sandbox for testing
    let sandbox: any = null;
    let clock: any = null;
    const todayYMD = moment().format("YYYY-MM-DD");

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox && sandbox.restore();
    });

    it("should instantiate", () => {
        expect(tripWithPositionRepository).not.to.be.undefined;
    });

    it("should return all items", async () => {
        const result = await tripWithPositionRepository.GetAll({
            preferredTimezone: "Europe/Prague",
        });
        expect(result).to.be.an.instanceOf(Object);
        expect(result.data).to.be.an.instanceOf(Object);
        expect(result.data.features).to.be.an.instanceOf(Array);
        expect(result.data.type).to.be.equal("FeatureCollection");
    });

    before(() => {
        clock = sinon.useFakeTimers({
            now: moment(`${todayYMD}T07:26:00+02:00`).valueOf(),
            shouldAdvanceTime: true,
        });
    });
    it("should return same 115 with proper headsigns", async () => {
        const result = await tripWithPositionRepository.GetAll({
            includeNotTracking: true,
            routeShortName: "115",
            preferredTimezone: "Europe/Prague",
        });
        const validResults: any = {
            "115_6_201230": {
                state_position: "before_track",
                headsign: "Městský archiv",
                last_stop_sequence: null,
                trip_short_name: null,
            },
            "115_6_201230_V1": {
                state_position: "on_track",
                headsign: "Městský archiv",
                last_stop_sequence: 3,
                trip_short_name: null,
            },
            "115_6_201230_V2": {
                state_position: "at_stop",
                headsign: "Chodov",
                last_stop_sequence: 4,
                trip_short_name: "115_short",
            },
            "115_6_201230_V3": {
                state_position: "before_track_delayed",
                headsign: "Městský archiv",
                last_stop_sequence: null,
                trip_short_name: "115_short",
            },
        };

        expect(result.data.features.length).to.be.equal(Object.keys(validResults).length);
        for (let i = 0; i < Object.keys(validResults).length; i++) {
            const thisResult = result.data.features[i];
            expect(thisResult.properties?.last_position.state_position).to.be.equal(
                validResults[thisResult.properties?.trip.gtfs.trip_id].state_position
            );
            expect(thisResult.properties?.trip.gtfs.trip_headsign).to.be.equal(
                validResults[thisResult.properties?.trip.gtfs.trip_id].headsign
            );
            expect(thisResult.properties?.last_position.last_stop.sequence).to.be.equal(
                validResults[thisResult.properties?.trip.gtfs.trip_id].last_stop_sequence
            );
            expect(thisResult.properties?.trip.gtfs.trip_short_name).to.be.equal(
                validResults[thisResult.properties?.trip.gtfs.trip_id].trip_short_name
            );
        }
    });

    it("should return not public trip", async () => {
        const result = await tripWithPositionRepository.GetAll({
            includeNotTracking: true,
            includeNotPublic: true,
            preferredTimezone: "Europe/Prague",
        });

        const notPublicTrip = result.data.features.find(
            (trip) => trip.properties?.last_position.state_position == StatePositionEnum.NOT_PUBLIC
        )?.properties!;

        expect(notPublicTrip.trip.agency_name.real).to.be.equal("DP PRAHA");
        expect(notPublicTrip.trip.vehicle_type.id).to.be.equal(MPVRouteTypesEnum.TRAM);
        expect(notPublicTrip.trip.vehicle_registration_number.toString()).to.be.equal(ArrayNotPublicRegistrationNumbers[0]);
        expect(notPublicTrip.trip.air_conditioned).to.be.equal(null);
    });
});
