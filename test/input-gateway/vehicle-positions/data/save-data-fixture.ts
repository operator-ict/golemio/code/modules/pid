export const saveDataFixture = {
    m: {
        $: {
            disp: "http://77.93.194.81:8716/api",
        },
        spoj: [
            {
                $: {
                    ka: "1",
                    kb: "0",
                    den: "2024-03-17",
                    lin: "",
                    alias: "R20",
                    spoj: "680",
                    dopr: "ČESKÉ DRÁHY",
                    t: "0",
                    np: "false",
                    skol: "false",
                    posl: "false",
                    sled: "0",
                },
                zast: [
                    {
                        $: {
                            odj: "16:45",
                            zast: "5457076",
                        },
                    },
                    {
                        $: {
                            stan: "3/-",
                            prij: "16:54",
                            odj: "16:55",
                            zast: "5457256",
                        },
                    },
                    {
                        $: {
                            prij: "16:59",
                            odj: "17:00",
                            zast: "5457027",
                        },
                    },
                    {
                        $: {
                            prij: "17:17",
                            odj: "17:18",
                            zast: "5454396",
                        },
                    },
                    {
                        $: {
                            prij: "17:35",
                            odj: "17:36",
                            zast: "5454287",
                        },
                    },
                    {
                        $: {
                            prij: "17:43",
                            odj: "17:44",
                            zast: "5454257",
                        },
                    },
                    {
                        $: {
                            prij: "17:51",
                            odj: "17:52",
                            zast: "5455909",
                        },
                    },
                    {
                        $: {
                            prij: "17:58",
                            odj: "17:59",
                            zast: "5455859",
                        },
                    },
                    {
                        $: {
                            odj: "18:15",
                            zast: "5453179",
                        },
                    },
                    {
                        $: {
                            prij: "18:33",
                            zast: "5455659",
                        },
                    },
                ],
            },
            {
                $: {
                    ka: "1",
                    kb: "0",
                    den: "2024-03-17",
                    lin: "",
                    alias: "R20",
                    spoj: "689",
                    dopr: "ČESKÉ DRÁHY",
                    t: "0",
                    np: "false",
                    skol: "false",
                    posl: "false",
                    sled: "2",
                    lat: "50.42715",
                    lng: "14.257638",
                    cpoz: "16:15:03",
                },
                zast: [
                    {
                        $: {
                            odj: "15:25",
                            zast: "5455659",
                        },
                    },
                    {
                        $: {
                            prij: "15:40",
                            odj: "15:42",
                            zast: "5453179",
                        },
                    },
                    {
                        $: {
                            prij: "15:58",
                            odj: "15:59",
                            zast: "5455859",
                        },
                    },
                    {
                        $: {
                            prij: "16:04",
                            odj: "16:05",
                            zast: "5455909",
                        },
                    },
                    {
                        $: {
                            stan: "-/3",
                            prij: "16:13",
                            odj: "16:14",
                            zast: "5454257",
                            zpoz_typ_prij: "2",
                            zpoz_typ_odj: "2",
                            zpoz_prij: "180",
                            zpoz_odj: "0",
                        },
                    },
                    {
                        $: {
                            prij: "16:20",
                            odj: "16:21",
                            zast: "5454287",
                            zpoz_typ_prij: "2",
                            zpoz_typ_odj: "2",
                            zpoz_prij: "0",
                            zpoz_odj: "0",
                        },
                    },
                    {
                        $: {
                            prij: "16:39",
                            odj: "16:40",
                            zast: "5454396",
                            zpoz_typ_prij: "2",
                            zpoz_typ_odj: "2",
                            zpoz_prij: "0",
                            zpoz_odj: "0",
                        },
                    },
                    {
                        $: {
                            odj: "16:56",
                            zast: "5457027",
                            zpoz_typ_prij: "2",
                            zpoz_typ_odj: "2",
                            zpoz_prij: "0",
                            zpoz_odj: "0",
                        },
                    },
                    {
                        $: {
                            stan: "2/-",
                            odj: "17:01",
                            zast: "5457256",
                            zpoz_typ_prij: "2",
                            zpoz_typ_odj: "2",
                            zpoz_prij: "0",
                            zpoz_odj: "0",
                        },
                    },
                    {
                        $: {
                            prij: "17:12",
                            zast: "5457076",
                            zpoz_typ_prij: "2",
                            zpoz_prij: "0",
                        },
                    },
                ],
            },
            {
                $: {
                    ka: "1",
                    kb: "0",
                    den: "2024-03-17",
                    lin: "",
                    alias: "R17",
                    spoj: "712",
                    dopr: "ČESKÉ DRÁHY",
                    t: "0",
                    np: "true",
                    skol: "false",
                    posl: "false",
                    sled: "2",
                    lat: "49.6032867",
                    lng: "14.5833349",
                    cpoz: "16:15:14",
                    zast: "5473622",
                    zpoz_prij: "60",
                    zpoz_odj: "60",
                },
                zast: [
                    {
                        $: {
                            odj: "15:10",
                            zast: "5473282",
                        },
                    },
                    {
                        $: {
                            prij: "15:37",
                            odj: "15:39",
                            zast: "5473552",
                        },
                    },
                    {
                        $: {
                            prij: "15:43",
                            odj: "15:44",
                            zast: "5473572",
                        },
                    },
                    {
                        $: {
                            prij: "15:51",
                            odj: "15:52",
                            zast: "5473602",
                        },
                    },
                    {
                        $: {
                            prij: "15:58",
                            odj: "16:00",
                            zast: "5473622",
                            zpoz_typ_prij: "3",
                            zpoz_typ_odj: "3",
                            zpoz_prij: "60",
                            zpoz_odj: "60",
                        },
                    },
                    {
                        $: {
                            stan: "-/2",
                            prij: "16:19",
                            odj: "16:20",
                            zast: "5455136",
                            zpoz_typ_prij: "2",
                            zpoz_typ_odj: "2",
                            zpoz_prij: "60",
                            zpoz_odj: "0",
                        },
                    },
                    {
                        $: {
                            stan: "-/3",
                            prij: "16:29",
                            odj: "16:31",
                            zast: "5455106",
                            zpoz_typ_prij: "2",
                            zpoz_typ_odj: "2",
                            zpoz_prij: "0",
                            zpoz_odj: "0",
                        },
                    },
                    {
                        $: {
                            stan: "-/1",
                            odj: "16:59",
                            zast: "5457279",
                            zpoz_typ_prij: "2",
                            zpoz_typ_odj: "2",
                            zpoz_prij: "0",
                            zpoz_odj: "0",
                        },
                    },
                    {
                        $: {
                            odj: "17:04",
                            zast: "5457276",
                            zpoz_typ_odj: "2",
                            zpoz_odj: "0",
                        },
                    },
                    {
                        $: {
                            prij: "17:10",
                            zast: "5457076",
                            zpoz_typ_prij: "2",
                            zpoz_prij: "0",
                        },
                    },
                ],
            },
        ],
    },
};
