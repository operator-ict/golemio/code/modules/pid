import { VehiclePositionsController } from "#ig/vehicle-positions/VehiclePositionsController";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox, SinonSpy } from "sinon";
import { saveDataFixture } from "./data/save-data-fixture";

chai.use(chaiAsPromised);

describe("VehiclePositionsController", () => {
    let sandbox: SinonSandbox;
    let controller: VehiclePositionsController;

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        controller = new VehiclePositionsController();
        controller["batchSize"] = 2;

        sandbox.stub(controller["validator"], "Validate").callsFake(() => Promise.resolve(true));
        // @ts-ignore
        sandbox.stub(controller, "sendMessageToExchange");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should has name", () => {
        expect(controller.name).not.to.be.undefined;
    });

    it("should has processData method", () => {
        expect(controller.processData).not.to.be.undefined;
    });

    it("should properly process data", async () => {
        await controller.processData(saveDataFixture);
        sandbox.assert.calledOnce(controller["validator"].Validate as SinonSpy);
        // @ts-ignore
        sandbox.assert.calledTwice(controller.sendMessageToExchange);
    });
});
