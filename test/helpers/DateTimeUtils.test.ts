import { DateTimeUtils } from "#helpers/DateTimeUtils";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";
import { formatStopTimeCESTToCETTestData, formatStopTimeCETToCESTTestData } from "./data/DateTimeUtils-formatStopTime-dst-change";
import {
    getStopDateTimeForDayStartCESTToCETTestData,
    getStopDateTimeForDayStartCETToCESTTestData,
} from "./data/DateTimeUtils-getStopDateTimeForDayStart-dst-change";
import {
    getStopDateTimeForTripOriginCESTToCETTestData,
    getStopDateTimeForTripOriginCETToCESTTestData,
} from "./data/DateTimeUtils-getStopDateTimeForTripOrigin-dst-change";

describe("DateTimeUtils", () => {
    let sandbox: SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    // =============================================================================
    // formatSQLTimestamp
    // =============================================================================
    describe("formatSQLTimestamp", () => {
        it("should format timestamp", () => {
            const result = DateTimeUtils.formatSQLTimestamp("2022-10-26 08:55:10+01");
            expect(result).to.equal("2022-10-26T09:55:10+02:00");
        });
    });

    // =============================================================================
    // parseUTCTimeFromISO
    // =============================================================================
    describe("parseUTCTimeFromISO", () => {
        it("should parse time", () => {
            const result = DateTimeUtils.parseUTCTimeFromISO("2022-10-26T08:55:10+01:00");
            expect(result).to.equal("09:55:10");
        });
    });

    describe("getStopDateTimeForTripOrigin", () => {
        it("should return correct timestamp for stop time HH::MM:SS", () => {
            const datetime = DateTimeUtils.getStopDateTimeForTripOrigin("07:08:09", "2024-03-14T06:00:00+01:00");
            expect(datetime.toISOString()).to.equal("2024-03-14T06:08:09.000Z");
        });

        it("should return correct timestamp for stop time H::MM:SS", () => {
            const datetime = DateTimeUtils.getStopDateTimeForTripOrigin("7:08:09", "2024-03-14T06:00:00+01:00");
            expect(datetime.toISOString()).to.equal("2024-03-14T06:08:09.000Z");
        });

        it("should return correct timestamp for stop time > 24:00:00 and afternoon origin", () => {
            const datetime = DateTimeUtils.getStopDateTimeForTripOrigin("27:08:09", "2024-03-14T16:00:00+01:00");
            expect(datetime.toISOString()).to.equal("2024-03-15T02:08:09.000Z");
        });

        it("should return correct timestamp for stop time > 24:00:00 and after midnight origin", () => {
            const datetime = DateTimeUtils.getStopDateTimeForTripOrigin("27:08:09", "2024-03-15T02:00:00+01:00");
            expect(datetime.toISOString()).to.equal("2024-03-15T02:08:09.000Z");
        });

        const dstChangeTests = [
            {
                data: getStopDateTimeForTripOriginCETToCESTTestData,
                getTitle: (stopTime: string, tripOriginTimestamp: string) =>
                    `should return correct timestamp for origin ${tripOriginTimestamp} and stop time ${stopTime} (around ` +
                    `CET -> CEST)`,
            },
            {
                data: getStopDateTimeForTripOriginCESTToCETTestData,
                getTitle: (stopTime: string, tripOriginTimestamp: string) =>
                    `should return correct timestamp for origin ${tripOriginTimestamp} and stop time ${stopTime} (around ` +
                    `CEST -> CET)`,
            },
        ];

        dstChangeTests.forEach(({ data, getTitle }) => {
            data.forEach(({ stopTime, tripOriginTimestamp, correctTimestamp }) => {
                it(getTitle(stopTime, tripOriginTimestamp), async () => {
                    const datetime = DateTimeUtils.getStopDateTimeForTripOrigin(stopTime, tripOriginTimestamp);
                    expect(datetime.toISOString()).to.equal(correctTimestamp);
                });
            });
        });
    });

    describe("getStopDateTimeForDayStart", () => {
        function shouldReturnCorrectTimestamp(stopTime: string, startDayTimestamp: string, correctDateTimestamp: string) {
            const stopTimeInSeconds = stopTime
                .split(":")
                .reduce((acc, val, i) => acc + parseInt(val, 10) * Math.pow(60, 2 - i), 0);
            const startDayUnixTimestamp = new Date(startDayTimestamp).getTime();

            const datetime = DateTimeUtils.getStopDateTimeForDayStart(stopTimeInSeconds, startDayUnixTimestamp);
            expect(datetime.toISOString()).to.equal(correctDateTimestamp);
        }

        it("should return correct timestamp", () =>
            shouldReturnCorrectTimestamp("07:08:09", "2024-03-14T00:00:00+01:00", "2024-03-14T06:08:09.000Z"));

        it("should return correct timestamp for stop time > 24:00:00", () =>
            shouldReturnCorrectTimestamp("27:08:09", "2024-03-14T00:00:00+01:00", "2024-03-15T02:08:09.000Z"));

        it("should return correct timestamp when using default start day", () => {
            sandbox.useFakeTimers({
                now: new Date("2024-01-02T12:00:00+01:00"),
            });
            const stopTime = "07:08:09";
            const stopTimeInSeconds = stopTime
                .split(":")
                .reduce((acc, val, i) => acc + parseInt(val, 10) * Math.pow(60, 2 - i), 0);

            const datetime = DateTimeUtils.getStopDateTimeForDayStart(stopTimeInSeconds);
            expect(datetime.toISOString()).to.equal("2024-01-02T06:08:09.000Z");
        });

        const dstChangeTests = [
            {
                data: getStopDateTimeForDayStartCETToCESTTestData,
                getTitle: (stopTime: string, startDayTimestamp: string) =>
                    `should return correct Date for stop time ${stopTime} and day starting at ${startDayTimestamp} (around ` +
                    `CET -> CEST)`,
            },
            {
                data: getStopDateTimeForDayStartCESTToCETTestData,
                getTitle: (stopTime: string, startDayTimestamp: string) =>
                    `should return correct Date for stop time ${stopTime} and day starting at ${startDayTimestamp} (around ` +
                    `CEST -> CET)`,
            },
        ];

        dstChangeTests.forEach(({ data, getTitle }) => {
            data.forEach(({ stopTime, startDayTimestamp, correctDateTimestamp }) => {
                it(getTitle(stopTime, startDayTimestamp), () =>
                    shouldReturnCorrectTimestamp(stopTime, startDayTimestamp, correctDateTimestamp)
                );
            });
        });
    });

    describe("formatStopTime", () => {
        it("should format stop time", () => {
            const result = DateTimeUtils.formatStopTime(new Date("2022-10-26T08:55:10+01:00"));
            expect(result).to.equal("09:55:10");
        });

        function shouldProperlyFormatStopTime(stopTime: Date, correctlyFormattedStopTime: string) {
            const result = DateTimeUtils.formatStopTime(stopTime);
            expect(result).to.equal(correctlyFormattedStopTime);
        }

        it("should properly format stop time past 10 AM", () =>
            shouldProperlyFormatStopTime(new Date("2024-01-01T18:29:30+01:00"), "18:29:30"));

        it("should properly format stop time before 10 AM", () =>
            shouldProperlyFormatStopTime(new Date("2024-01-01T05:06:07+01:00"), "05:06:07"));

        const dstChangeTests = [
            {
                data: formatStopTimeCETToCESTTestData,
                title: "Around DST change (CET -> CEST)",
            },
            {
                data: formatStopTimeCESTToCETTestData,
                title: "Around DST change (CEST -> CET)",
            },
        ];

        dstChangeTests.forEach(({ data, title }) => {
            describe(title, () => {
                data.forEach(({ stopTime, correctlyFormattedStopTime }) => {
                    it(`should properly format stop time ${stopTime.toISOString()}`, () =>
                        shouldProperlyFormatStopTime(stopTime, correctlyFormattedStopTime));
                });
            });
        });
    });
});
