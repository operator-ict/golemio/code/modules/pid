export const formatStopTimeCETToCESTTestData = [
    {
        stopTime: new Date("2023-03-25T00:01:02+01:00"),
        correctlyFormattedStopTime: "00:01:02",
    },
    {
        stopTime: new Date("2023-03-25T10:01:02+01:00"),
        correctlyFormattedStopTime: "10:01:02",
    },
    {
        stopTime: new Date("2023-03-26T00:01:02+01:00"),
        correctlyFormattedStopTime: "00:01:02",
    },
    {
        stopTime: new Date("2023-03-26T01:01:02+01:00"),
        correctlyFormattedStopTime: "01:01:02",
    },
    {
        stopTime: new Date("2023-03-26T03:01:02+02:00"),
        correctlyFormattedStopTime: "03:01:02",
    },
    {
        stopTime: new Date("2023-03-26T04:01:02+02:00"),
        correctlyFormattedStopTime: "04:01:02",
    },
    {
        stopTime: new Date("2023-03-26T05:01:02+02:00"),
        correctlyFormattedStopTime: "05:01:02",
    },
    {
        stopTime: new Date("2023-03-26T06:01:02+02:00"),
        correctlyFormattedStopTime: "06:01:02",
    },
    {
        stopTime: new Date("2023-03-26T10:01:02+02:00"),
        correctlyFormattedStopTime: "10:01:02",
    },
    {
        stopTime: new Date("2023-03-27T00:01:02+02:00"),
        correctlyFormattedStopTime: "00:01:02",
    },
    {
        stopTime: new Date("2023-03-27T10:01:02+02:00"),
        correctlyFormattedStopTime: "10:01:02",
    },
];

export const formatStopTimeCESTToCETTestData = [
    {
        stopTime: new Date("2023-10-28T00:01:02+02:00"),
        correctlyFormattedStopTime: "00:01:02",
    },
    {
        stopTime: new Date("2023-10-28T10:01:02+02:00"),
        correctlyFormattedStopTime: "10:01:02",
    },
    {
        stopTime: new Date("2023-10-29T00:01:02+02:00"),
        correctlyFormattedStopTime: "00:01:02",
    },
    {
        stopTime: new Date("2023-10-29T01:01:02+02:00"),
        correctlyFormattedStopTime: "01:01:02",
    },
    {
        stopTime: new Date("2023-10-29T02:01:02+02:00"),
        correctlyFormattedStopTime: "02:01:02",
    },
    {
        stopTime: new Date("2023-10-29T02:01:02+01:00"),
        correctlyFormattedStopTime: "02:01:02",
    },
    {
        stopTime: new Date("2023-10-29T03:01:02+01:00"),
        correctlyFormattedStopTime: "03:01:02",
    },
    {
        stopTime: new Date("2023-10-29T04:01:02+01:00"),
        correctlyFormattedStopTime: "04:01:02",
    },
    {
        stopTime: new Date("2023-10-29T10:01:02+01:00"),
        correctlyFormattedStopTime: "10:01:02",
    },
    {
        stopTime: new Date("2023-10-30T00:01:02+01:00"),
        correctlyFormattedStopTime: "00:01:02",
    },
    {
        stopTime: new Date("2023-10-30T10:01:02+01:00"),
        correctlyFormattedStopTime: "10:01:02",
    },
];
