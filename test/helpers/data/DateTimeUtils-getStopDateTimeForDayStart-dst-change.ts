export const getStopDateTimeForDayStartCETToCESTTestData = [
    {
        stopTime: "00:01:02",
        startDayTimestamp: "2023-03-24T23:00:00.000Z",
        correctDateTimestamp: "2023-03-24T23:01:02.000Z",
    },
    {
        stopTime: "10:01:02",
        startDayTimestamp: "2023-03-24T23:00:00.000Z",
        correctDateTimestamp: "2023-03-25T09:01:02.000Z",
    },
    {
        stopTime: "24:01:02",
        startDayTimestamp: "2023-03-24T23:00:00.000Z",
        correctDateTimestamp: "2023-03-25T23:01:02.000Z",
    },
    {
        stopTime: "01:01:02",
        startDayTimestamp: "2023-03-25T23:00:00.000Z",
        correctDateTimestamp: "2023-03-25T23:01:02.000Z",
    },
    {
        stopTime: "25:01:02",
        startDayTimestamp: "2023-03-24T23:00:00.000Z",
        correctDateTimestamp: "2023-03-26T00:01:02.000Z",
    },
    {
        stopTime: "02:01:02",
        startDayTimestamp: "2023-03-25T23:00:00.000Z",
        correctDateTimestamp: "2023-03-26T00:01:02.000Z",
    },
    {
        stopTime: "26:01:02",
        startDayTimestamp: "2023-03-24T23:00:00.000Z",
        correctDateTimestamp: "2023-03-26T01:01:02.000Z",
    },
    {
        stopTime: "03:01:02",
        startDayTimestamp: "2023-03-25T23:00:00.000Z",
        correctDateTimestamp: "2023-03-26T01:01:02.000Z",
    },
    {
        stopTime: "27:01:02",
        startDayTimestamp: "2023-03-24T23:00:00.000Z",
        correctDateTimestamp: "2023-03-26T02:01:02.000Z",
    },
    {
        stopTime: "04:01:02",
        startDayTimestamp: "2023-03-25T23:00:00.000Z",
        correctDateTimestamp: "2023-03-26T02:01:02.000Z",
    },
    {
        stopTime: "28:01:02",
        startDayTimestamp: "2023-03-24T23:00:00.000Z",
        correctDateTimestamp: "2023-03-26T03:01:02.000Z",
    },
    {
        stopTime: "05:01:02",
        startDayTimestamp: "2023-03-25T23:00:00.000Z",
        correctDateTimestamp: "2023-03-26T03:01:02.000Z",
    },
    {
        stopTime: "29:01:02",
        startDayTimestamp: "2023-03-24T23:00:00.000Z",
        correctDateTimestamp: "2023-03-26T04:01:02.000Z",
    },
    {
        stopTime: "10:01:02",
        startDayTimestamp: "2023-03-25T23:00:00.000Z",
        correctDateTimestamp: "2023-03-26T08:01:02.000Z",
    },
    {
        stopTime: "00:01:02",
        startDayTimestamp: "2023-03-26T23:00:00.000Z",
        correctDateTimestamp: "2023-03-26T22:01:02.000Z",
    },
    {
        stopTime: "10:01:02",
        startDayTimestamp: "2023-03-26T22:00:00.000Z",
        correctDateTimestamp: "2023-03-27T08:01:02.000Z",
    },
];

export const getStopDateTimeForDayStartCESTToCETTestData = [
    {
        stopTime: "00:01:02",
        startDayTimestamp: "2023-10-26T22:00:00.000Z",
        correctDateTimestamp: "2023-10-26T22:01:02.000Z",
    },
    {
        stopTime: "00:50:15",
        startDayTimestamp: "2023-10-27T22:00:00.000Z",
        correctDateTimestamp: "2023-10-27T22:50:15.000Z",
    },
    {
        stopTime: "10:01:02",
        startDayTimestamp: "2023-10-27T22:00:00.000Z",
        correctDateTimestamp: "2023-10-28T08:01:02.000Z",
    },
    {
        stopTime: "24:01:02",
        startDayTimestamp: "2023-10-27T22:00:00.000Z",
        correctDateTimestamp: "2023-10-28T22:01:02.000Z",
    },
    {
        stopTime: "25:01:02",
        startDayTimestamp: "2023-10-27T22:00:00.000Z",
        correctDateTimestamp: "2023-10-28T23:01:02.000Z",
    },
    {
        stopTime: "00:01:02",
        startDayTimestamp: "2023-10-28T22:00:00.000Z",
        correctDateTimestamp: "2023-10-28T23:01:02.000Z",
    },
    {
        stopTime: "26:01:02",
        startDayTimestamp: "2023-10-27T22:00:00.000Z",
        correctDateTimestamp: "2023-10-29T00:01:02.000Z",
    },
    {
        stopTime: "01:01:02",
        startDayTimestamp: "2023-10-28T22:00:00.000Z",
        correctDateTimestamp: "2023-10-29T00:01:02.000Z",
    },
    {
        stopTime: "27:01:02",
        startDayTimestamp: "2023-10-27T22:00:00.000Z",
        correctDateTimestamp: "2023-10-29T01:01:02.000Z",
    },
    {
        stopTime: "02:01:02",
        startDayTimestamp: "2023-10-28T22:00:00.000Z",
        correctDateTimestamp: "2023-10-29T01:01:02.000Z",
    },
    {
        stopTime: "28:01:02",
        startDayTimestamp: "2023-10-27T22:00:00.000Z",
        correctDateTimestamp: "2023-10-29T02:01:02.000Z",
    },
    {
        stopTime: "03:01:02",
        startDayTimestamp: "2023-10-28T22:00:00.000Z",
        correctDateTimestamp: "2023-10-29T02:01:02.000Z",
    },
    {
        stopTime: "29:01:02",
        startDayTimestamp: "2023-10-27T22:00:00.000Z",
        correctDateTimestamp: "2023-10-29T03:01:02.000Z",
    },
    {
        stopTime: "04:01:02",
        startDayTimestamp: "2023-10-28T22:00:00.000Z",
        correctDateTimestamp: "2023-10-29T03:01:02.000Z",
    },
    {
        stopTime: "10:01:02",
        startDayTimestamp: "2023-10-28T22:00:00.000Z",
        correctDateTimestamp: "2023-10-29T09:01:02.000Z",
    },
    {
        stopTime: "00:01:02",
        startDayTimestamp: "2023-10-29T23:00:00.000Z",
        correctDateTimestamp: "2023-10-29T23:01:02.000Z",
    },
    {
        stopTime: "10:01:02",
        startDayTimestamp: "2023-10-29T23:00:00.000Z",
        correctDateTimestamp: "2023-10-30T09:01:02.000Z",
    },
];
