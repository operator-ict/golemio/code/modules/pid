import { GTFSRouteTypeEnum, getGtfsRouteType } from "#helpers/RouteTypeEnums";
import { expect } from "chai";

describe("RouteTypeEnum", () => {
    it("should properly convert string to enum", () => {
        expect(getGtfsRouteType("TRAM")).equal(GTFSRouteTypeEnum.TRAM);
        expect(getGtfsRouteType("metro")).equal(GTFSRouteTypeEnum.METRO);
        expect(getGtfsRouteType("TRAIN")).equal(GTFSRouteTypeEnum.TRAIN);
        expect(getGtfsRouteType("bus")).equal(GTFSRouteTypeEnum.BUS);
        expect(getGtfsRouteType("feRRy")).equal(GTFSRouteTypeEnum.FERRY);
        expect(getGtfsRouteType("FUNICULAR")).equal(GTFSRouteTypeEnum.FUNICULAR);
        expect(getGtfsRouteType("TROLLEYBUS")).equal(GTFSRouteTypeEnum.TROLLEYBUS);
        expect(getGtfsRouteType("other")).equal(GTFSRouteTypeEnum.EXT_MISCELLANEOUS);
    });
});
