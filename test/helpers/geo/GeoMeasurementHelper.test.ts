import { GeoMeasurementHelper } from "#helpers/geo/GeoMeasurementHelper";
import { Position } from "@golemio/core/dist/shared/geojson";
import { container } from "@golemio/core/dist/shared/tsyringe";
import { expect } from "chai";

describe("GeoMeasurementHelper", () => {
    const geoHelper = container.createChildContainer().registerSingleton(GeoMeasurementHelper).resolve(GeoMeasurementHelper);

    const validPointA: Position = [14.5, 50.4];
    const validPointB: Position = [14.5, 50.2];
    const invalidPoint: Position = [14.6];

    describe("getDistanceInKilometers", () => {
        it("should return distance in kilometers", () => {
            const distance = geoHelper.getDistanceInKilometers(validPointA, validPointB);
            expect(distance).to.equal(22.245812797712002);
        });

        it("should throw error when invalid point is passed", () => {
            expect(() => geoHelper.getDistanceInKilometers(validPointA, invalidPoint)).to.throw(
                "getValidPoint: invalid point [14.6]"
            );
        });
    });

    describe("getDistanceInMeters", () => {
        it("should return distance in meters", () => {
            const distance = geoHelper.getDistanceInMeters(validPointA, validPointB);
            expect(distance).to.equal(22245.81279771201);
        });

        it("should throw error when invalid point is passed", () => {
            expect(() => geoHelper.getDistanceInMeters(validPointA, invalidPoint)).to.throw(
                "getValidPoint: invalid point [14.6]"
            );
        });
    });

    describe("getBufferedBBoxInKilometers", () => {
        it("should return buffered bounding box in kilometers", () => {
            const bbox = geoHelper.getBufferedBBoxInKilometers(validPointA, 1);
            expect(bbox).to.deep.equal([14.486052172554654, 50.391009544051336, 14.513947827445346, 50.40899045594866]);
        });

        it("should throw error when invalid point is passed", () => {
            expect(() => geoHelper.getBufferedBBoxInKilometers(invalidPoint, 1)).to.throw("getValidPoint: invalid point [14.6]");
        });
    });

    describe("isPointInBBox", () => {
        it("should return true when point is inside bbox", () => {
            const bbox = geoHelper.getBufferedBBoxInKilometers(validPointA, 1);
            const isPointInBBox = geoHelper.isPointInBBox(validPointA, bbox);
            expect(isPointInBBox).to.equal(true);
        });

        it("should return false when point is outside bbox", () => {
            const bbox = geoHelper.getBufferedBBoxInKilometers(validPointA, 1);
            const isPointInBBox = geoHelper.isPointInBBox(validPointB, bbox);
            expect(isPointInBBox).to.equal(false);
        });

        it("should throw error when invalid point is passed", () => {
            const bbox = geoHelper.getBufferedBBoxInKilometers(validPointA, 1);
            expect(() => geoHelper.isPointInBBox(invalidPoint, bbox)).to.throw("getValidPoint: invalid point [14.6]");
        });
    });
});
