import { ICommonRunsInput } from "#ie/vehicle-positions/workers/runs/interfaces/CommonRunsMessageInterfaces";
import { SaveTramRunsToDBTask } from "#ie/vehicle-positions/workers/runs/tasks/SaveTramRunsToDBTask";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { QueueManager } from "@golemio/core/dist/integration-engine/queueprocessors";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";
import { TCPEventEnum } from "src/const";

chai.use(chaiAsPromised);

describe("RunsWorker - SaveTramRunsToDBTask", () => {
    let sandbox: SinonSandbox;
    let task: SaveTramRunsToDBTask;

    const testParamsValid: ICommonRunsInput = {
        M: {
            V: {
                $: {
                    turnus: "15/1",
                    line: "15",
                    evc: "9273",
                    np: "ano",
                    lat: "50.08885",
                    lng: "14.42999",
                    akt: "04800002",
                    takt: "2021-06-28T13:04:38",
                    konc: "10190002",
                    tjr: "2021-06-28T13:04:00",
                    pkt: "2918063",
                    tm: "2021-06-28T13:04:38",
                    events: TCPEventEnum.ARRIVAL_ANNOUNCED,
                },
            },
        },
    };

    const testParamsInvalid = {
        M: {
            V: {
                $: {
                    turnus: "15/1",
                    line: "15",
                    evc: "9273",
                    np: "ano",
                    lat: "50.08885",
                    lng: "14.42999",
                    akt: "04800002",
                    takt: "2021-06-28T13:04:38",
                    konc: "10190002",
                    tjr: "2021-06-28T13:04:00",
                    pkt: "2918063",
                    tm: "2021-06-28T13:04:38",
                    events: "TT",
                },
            },
        },
    };

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox
                    .stub()
                    .returns({ removeAttribute: sandbox.stub(), belongsTo: sandbox.stub(), hasMany: sandbox.stub() }),
            })
        );
        sandbox.stub(QueueManager, "sendMessageToExchange").resolves();

        task = new SaveTramRunsToDBTask();
    });

    afterEach(() => {
        sandbox?.restore();
    });

    it("should validate and execute", async () => {
        const transformSpy = sinon.spy(task["messagesTransformation"], "transform");
        const processStub = sinon.stub(task["messageProcessor"], "processTransformedRun").resolves();
        const promise = task.consume({
            content: Buffer.from(JSON.stringify(testParamsValid)),
            properties: { timestamp: new Date("2021-06-28T13:04:38").getTime() },
        } as any);

        await expect(promise).to.be.fulfilled;
        expect(transformSpy.callCount).to.equal(1);
        expect(processStub.callCount).to.equal(1);
    });

    it("should reject (missing timestamp)", async () => {
        const transformSpy = sinon.spy(task["messagesTransformation"], "transform");
        const promise = task.consume({
            content: Buffer.from(JSON.stringify(testParamsValid)),
        } as any);

        await expect(promise).to.be.rejectedWith(
            GeneralError,
            // eslint-disable-next-line max-len
            'Missing tram run message timestamp: {"M":{"V":{"$":{"turnus":"15/1","line":"15","evc":"9273","np":"ano","lat":"50.08885","lng":"14.42999","akt":"04800002","takt":"2021-06-28T13:04:38","konc":"10190002","tjr":"2021-06-28T13:04:00","pkt":"2918063","tm":"2021-06-28T13:04:38","events":"P"}}}}'
        );

        expect(transformSpy.callCount).to.equal(0);
    });

    it("should not validate (missing route_name and delay_origin)", async () => {
        const transformSpy = sinon.spy(task["messagesTransformation"], "transform");
        const promise = task.consume({ content: Buffer.from(JSON.stringify(testParamsInvalid)) } as any);

        await expect(promise).to.be.rejectedWith(
            GeneralError,
            // eslint-disable-next-line max-len
            "[Queue VehiclePositionsRuns.saveTramRunsToDB] Message validation failed: [events must be one of the following values: P, O, V, T]"
        );

        expect(transformSpy.callCount).to.equal(0);
    });
});
