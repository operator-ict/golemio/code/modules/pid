import { CommonRunHelper } from "#ie/vehicle-positions/workers/runs/helpers/CommonRunHelper";
import { ICommonRunsMessageProperties } from "#ie/vehicle-positions/workers/runs/interfaces/CommonRunsMessageInterfaces";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";

describe("CommonRunHelper", () => {
    let sandbox: SinonSandbox;
    let helper: CommonRunHelper;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        helper = new CommonRunHelper();
    });

    afterEach(() => {
        sandbox?.restore();
    });

    // =============================================================================
    // getRunIdentifiers
    // =============================================================================
    describe("getRunIdentifiers", () => {
        it("should return valid identifiers", () => {
            const properties: Partial<ICommonRunsMessageProperties> = {
                turnus: "X/123",
                evc: "123",
            };
            const msgTimestamp = "2020-01-01T00:00:00";
            const result = helper.getRunIdentifiers(properties as ICommonRunsMessageProperties, msgTimestamp);
            expect(result).to.eql({
                runId: "X_123_123_2020-01-01T00:00:00",
                routeId: "X",
                runNumber: "123",
            });
        });

        it("should return null", () => {
            const properties: Partial<ICommonRunsMessageProperties> = {
                turnus: "X",
                evc: "123",
            };
            const msgTimestamp = "2020-01-01T00:00:00";
            const result = helper.getRunIdentifiers(properties as ICommonRunsMessageProperties, msgTimestamp);
            expect(result).to.be.null;
        });
    });

    // =============================================================================
    // parseDateFromRunInput
    // =============================================================================
    describe("parseDateFromRunInput", () => {
        it("should return valid date", () => {
            const utcTimestamp = "2020-01-01T00:00:00";
            const result = helper.parseDateFromRunInput(utcTimestamp);
            expect(result).to.be.instanceOf(Date);
            expect(result?.toISOString()).to.equal("2020-01-01T00:00:00.000Z");
        });

        it("should return null", () => {
            const utcTimestamp = "2020-01-01T00:00:00Z";
            const result = helper.parseDateFromRunInput(utcTimestamp);
            expect(result).to.be.null;
        });
    });
});
