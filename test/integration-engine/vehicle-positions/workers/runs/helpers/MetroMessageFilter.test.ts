import { MetroMessageFilter } from "#ie/vehicle-positions/workers/runs/helpers/MetroMessageFilter";
import { IProcessMetroRunsMessage } from "#ie/vehicle-positions/workers/runs/interfaces/IProcessMetroRunsMessagesInput";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";

describe("MetroMessageFilter", () => {
    let sandbox: SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox?.restore();
    });

    // =============================================================================
    // getValidMessagesWithTrackIds
    // =============================================================================
    describe("getValidMessagesWithTrackIds", () => {
        it("should return valid messages with track ids", () => {
            const logWarnStub = sandbox.stub(log, "warn");
            const messages: IProcessMetroRunsMessage[] = [
                {
                    route_name: "A",
                    message_timestamp: "2022-07-20T13:45:34Z",
                    train_set_number_scheduled: "3",
                    train_set_number_real: "3",
                    train_number: "279",
                    track_id: "1809",
                    delay_origin: 25,
                    actual_position_timestamp_scheduled: "2022-07-20T13:45:59Z",
                },
                {
                    route_name: "A",
                    message_timestamp: "2022-07-20T13:45:34Z",
                    train_set_number_scheduled: "10",
                    train_set_number_real: "10",
                    train_number: "280",
                    track_id: "1405",
                    delay_origin: -4 * 60 * 60 - 1,
                    actual_position_timestamp_scheduled: "2022-07-20T09:45:33Z",
                },
                {
                    route_name: "A",
                    message_timestamp: "2022-07-20T13:45:34Z",
                    train_set_number_scheduled: "13",
                    train_set_number_real: "13",
                    train_number: "281",
                    track_id: "1107",
                    delay_origin: 1 * 60 * 60 + 1,
                    actual_position_timestamp_scheduled: "2022-07-20T14:45:35Z",
                },
            ];

            const result = MetroMessageFilter.getValidMessagesWithTrackIds(messages);

            expect(result.filteredMessages).to.deep.equal([messages[0]]);
            expect(result.trackIds).to.deep.equal(["1809"]);
            expect(logWarnStub.getCall(0).args[0]).to.equal(
                "getValidMessagesWithTrackIds: delay origin -14401 is out of range [-14400, 3600]"
            );
            expect(logWarnStub.getCall(1).args[0]).to.equal(
                "getValidMessagesWithTrackIds: delay origin 3601 is out of range [-14400, 3600]"
            );
        });
    });
});
