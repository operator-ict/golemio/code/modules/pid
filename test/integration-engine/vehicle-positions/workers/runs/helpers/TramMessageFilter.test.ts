import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { TimestampValidator } from "#ie/vehicle-positions/workers/runs/helpers/TimestampValidator";
import { TramMessageFilter } from "#ie/vehicle-positions/workers/runs/helpers/TramMessageFilter";
import { ICommonRunWithMessageDto } from "#sch/vehicle-positions/models/interfaces/ICommonRunWithMessageDto";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { container as GlobalContainer } from "@golemio/core/dist/shared/tsyringe";
import { expect } from "chai";
import sinon, { SinonSandbox, SinonStub } from "sinon";
import { ArrayNotPublicRegistrationNumbers, TCPEventEnum } from "src/const";

describe("TramMessageFilter", () => {
    let sandbox: SinonSandbox;
    const container = GlobalContainer.createChildContainer();
    const date = new Date();
    beforeEach(() => {
        sandbox = sinon.createSandbox();
        container.register(
            CoreToken.Logger,
            class DummyLogger {
                info = sinon.stub();
                debug = sinon.stub();
                error = sinon.stub();
            }
        );
        container.registerSingleton(
            CoreToken.SimpleConfig,
            class SimpleConfig {
                getValue = sinon.stub().callsFake((_: string, def: number) => {
                    return def;
                });
            }
        );
        container.registerSingleton(VPContainerToken.TramMessageFilter, TramMessageFilter);
        container.registerSingleton(VPContainerToken.TimestampValidator, TimestampValidator);
    });

    afterEach(() => {
        sandbox?.restore();
        container.clearInstances();
    });

    // =============================================================================
    // yieldFilteredMessages
    // =============================================================================
    describe("yieldFilteredMessages", () => {
        it("should yield messages that are valid for processing", () => {
            const messages = [
                {
                    run: {
                        route_id: "1",
                        run_number: 1,
                    },
                    run_message: {
                        actual_stop_timestamp_real: date,
                        events: TCPEventEnum.TIME,
                        lat: 123,
                        lng: 321,
                        msg_timestamp: date,
                        packet_number: "123",
                    },
                },
                {
                    run: {
                        route_id: "49",
                        run_number: 79,
                    },
                    run_message: {
                        actual_stop_timestamp_real: date,
                        events: TCPEventEnum.TIME,
                        lat: 123,
                        lng: 321,
                        msg_timestamp: date,
                        packet_number: "123",
                    },
                },
                {
                    run: {
                        route_id: "69",
                        run_number: 1,
                    },
                    run_message: {
                        actual_stop_timestamp_real: date,
                        events: TCPEventEnum.TIME,
                        lat: 123,
                        lng: 321,
                        msg_timestamp: date,
                        packet_number: "123",
                    },
                },
                {
                    run: {
                        route_id: "49",
                        run_number: 90,
                    },
                    run_message: {
                        actual_stop_timestamp_real: date,
                        events: TCPEventEnum.TIME,
                        lat: 123,
                        lng: 321,
                        msg_timestamp: date,
                        packet_number: "123",
                    },
                },
                {
                    run: {
                        route_id: "49",
                        run_number: 79,
                    },
                    // wrong actual_stop_timestamp_real
                    run_message: {
                        actual_stop_timestamp_real: new Date(new Date().setMinutes(date.getMinutes() + 10)),
                        events: TCPEventEnum.TIME,
                        lat: 123,
                        lng: 321,
                        msg_timestamp: new Date().setHours(0),
                        packet_number: "123",
                    },
                },
                {
                    run: {
                        route_id: "49",
                        run_number: 79,
                    },
                    // wrong actual_stop_timestamp_real
                    run_message: {
                        actual_stop_timestamp_real: new Date(new Date().setHours(date.getHours() - 3)),
                        events: TCPEventEnum.TIME,
                        lat: 123,
                        lng: 321,
                        msg_timestamp: new Date().setHours(0),
                        packet_number: "123",
                    },
                },
                {
                    // night tram
                    run: {
                        route_id: "95",
                        run_number: 10,
                    },
                    run_message: {
                        actual_stop_timestamp_real: date,
                        events: TCPEventEnum.TIME,
                        lat: 123,
                        lng: 321,
                        msg_timestamp: date,
                        packet_number: "123",
                    },
                },
                {
                    // mazačka (special maintenance tram)
                    run: {
                        route_id: "85",
                        run_number: 99,
                        registration_number: ArrayNotPublicRegistrationNumbers[0],
                    },
                    run_message: {
                        actual_stop_timestamp_real: date,
                        events: TCPEventEnum.TIME,
                        lat: 123,
                        lng: 321,
                        msg_timestamp: date,
                        packet_number: "123",
                    },
                },
            ] as ICommonRunWithMessageDto[];

            const filter = container.resolve<TramMessageFilter>(VPContainerToken.TramMessageFilter);
            const result = Array.from(filter.yieldFilteredMessages(messages, Date.now()));
            expect(result).to.have.lengthOf(4);
            expect(result[0]).to.deep.equal(messages[0]);
            expect(result[1]).to.deep.equal(messages[1]);
            expect(result[2]).to.deep.equal(messages[6]);
            expect(result[3]).to.deep.equal(messages[7]);
            expect((filter["logger"].info as SinonStub).callCount).to.equal(2);
        });
    });
});
