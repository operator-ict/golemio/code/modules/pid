import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { BusMessageFilter } from "#ie/vehicle-positions/workers/runs/helpers/BusMessageFilter";
import { TimestampValidator } from "#ie/vehicle-positions/workers/runs/helpers/TimestampValidator";
import { ICommonRunWithMessageDto } from "#sch/vehicle-positions/models/interfaces/ICommonRunWithMessageDto";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { container as GlobalContainer } from "@golemio/core/dist/shared/tsyringe";
import { expect } from "chai";
import sinon, { SinonSandbox, SinonStub } from "sinon";

describe("BusMessageFilter", () => {
    let sandbox: SinonSandbox;
    const container = GlobalContainer.createChildContainer();
    const date = new Date();

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        container.register(
            CoreToken.Logger,
            class DummyLogger {
                info = sinon.stub();
                debug = sinon.stub();
                error = sinon.stub();
            }
        );

        container.registerSingleton(
            CoreToken.SimpleConfig,
            class SimpleConfig {
                getValue = sinon.stub().callsFake((_: string, def: number) => {
                    return def;
                });
            }
        );
        container.registerSingleton(VPContainerToken.TimestampValidator, TimestampValidator);
        container.registerSingleton(VPContainerToken.BusMessageFilter, BusMessageFilter);
    });

    afterEach(() => {
        sandbox?.restore();
        container.clearInstances();
    });

    // =============================================================================
    // yieldFilteredMessages
    // =============================================================================
    describe("yieldFilteredMessages", () => {
        it("should yield messages that are valid for processing", () => {
            const messages = [
                {
                    run: {
                        route_id: "X",
                    },
                    run_message: {
                        actual_stop_timestamp_real: date,
                    },
                },
                {
                    run: {
                        route_id: "1",
                    },
                    run_message: {
                        actual_stop_timestamp_real: date,
                    },
                },
                {
                    run: {
                        route_id: "50",
                    },
                    run_message: {
                        actual_stop_timestamp_real: date,
                    },
                },
                {
                    run: {
                        route_id: "69",
                    },
                    run_message: {
                        actual_stop_timestamp_real: date,
                    },
                },
                {
                    run: {
                        route_id: "75",
                    },
                    run_message: {
                        actual_stop_timestamp_real: date,
                    },
                },
                {
                    run: {
                        route_id: "150",
                    },
                    run_message: {
                        actual_stop_timestamp_real: date,
                    },
                },
                {
                    run: {
                        route_id: "151",
                    },
                    run_message: {
                        actual_stop_timestamp_real: new Date(new Date().setMinutes(date.getMinutes() + 10)),
                    },
                },
                {
                    run: {
                        route_id: "152",
                    },
                    run_message: {
                        actual_stop_timestamp_real: new Date(new Date().setHours(date.getHours() - 3)),
                    },
                },
            ] as ICommonRunWithMessageDto[];

            const filter = container.resolve<BusMessageFilter>(VPContainerToken.BusMessageFilter);
            const result = Array.from(filter.yieldFilteredMessages(messages, date.getTime()));
            expect(result).to.have.lengthOf(3);
            expect(result[0]).to.deep.equal(messages[2]);
            expect(result[1]).to.deep.equal(messages[3]);
            expect(result[2]).to.deep.equal(messages[5]);
            expect((filter["logger"].info as SinonStub).callCount).to.equal(3);
        });
    });
});
