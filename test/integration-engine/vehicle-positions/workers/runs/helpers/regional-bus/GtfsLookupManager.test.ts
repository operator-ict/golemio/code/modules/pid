import { VPContainer } from "#ie/vehicle-positions/ioc/Di";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { GtfsLookupManager } from "#ie/vehicle-positions/workers/runs/helpers/regional-bus/GtfsLookupManager";
import { IExtendedScheduleWithExternalId } from "#ie/vehicle-positions/workers/runs/helpers/regional-bus/interfaces/IExtendedSchedule";
import { RedisConnector } from "@golemio/core/dist/integration-engine/connectors";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";

describe("GtfsLookupManager", () => {
    let sandbox: SinonSandbox;
    let manager: GtfsLookupManager;

    const input: IExtendedScheduleWithExternalId[] = [
        {
            cache_lookup_key: "100341_1500",
            route_id: "341",
            run_number: 123,
            msg_last_timestamp: "2023-09-26T05:46:00+02:00",
            gtfs_trip_id: "341_371_230904",
            agency_name: "ARRIVA City",
            is_wheelchair_accessible: true,
        },
    ];

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        sandbox.stub(RedisConnector, "getConnection");
        manager = VPContainer.resolve(VPContainerToken.GtfsLookupManager);
    });

    afterEach(() => {
        sandbox?.restore();
    });

    // =============================================================================
    // getScheduleInputData
    // =============================================================================
    describe("getScheduleInputData", () => {
        it("should get schedule data from cache", async () => {
            const mgetStub = sandbox.stub(manager["gtfsLookupCacheRepository"], "mget").resolves([
                {
                    gtfs_trip_id: input[0].gtfs_trip_id!,
                    route_name: input[0].route_id,
                    run_number: input[0].run_number,
                    agency_name: input[0].agency_name,
                    is_wheelchair_accessible: input[0].is_wheelchair_accessible,
                },
            ]);

            const result = await manager.getScheduleInputData([
                {
                    external_trip_id: "867377023797370",
                    cis_line_id: "100341",
                    cis_trip_number: 1500,
                    events: "R",
                    coordinates: {
                        type: "Point",
                        coordinates: [14.4203, 50.1263],
                    },
                    vehicle_timestamp: "2023-09-26T05:50:00+02:00",
                    registration_number: 1234,
                    speed_kmh: 0,
                    bearing: 0,
                    is_terminated: false,
                },
            ]);

            expect(mgetStub.callCount).to.equal(1);
            expect(mgetStub.firstCall.args).to.deep.equal([["100341_1500"]]);
            expect(result).to.deep.equal([
                {
                    agency_name: "ARRIVA City",
                    gtfs_trip_id: "341_371_230904",
                    is_wheelchair_accessible: true,
                    msg_last_timestamp: "2023-09-26T05:50:00+02:00",
                    route_id: "341",
                    run_number: 123,
                },
            ]);
        });
    });

    // =============================================================================
    // setGtfsScheduleLookup
    // =============================================================================
    describe("setGtfsScheduleLookup", () => {
        it("should set new schedule", async () => {
            const setStub = sandbox.stub(manager["gtfsLookupCacheRepository"], "set").resolves();

            await manager.setGtfsScheduleLookup(input);
            expect(setStub.callCount).to.equal(1);
            expect(setStub.firstCall.args).to.deep.equal([
                "100341_1500",
                {
                    gtfs_trip_id: input[0].gtfs_trip_id!,
                    route_name: input[0].route_id,
                    run_number: input[0].run_number,
                    agency_name: input[0].agency_name,
                    is_wheelchair_accessible: input[0].is_wheelchair_accessible,
                },
            ]);
        });
    });
});
