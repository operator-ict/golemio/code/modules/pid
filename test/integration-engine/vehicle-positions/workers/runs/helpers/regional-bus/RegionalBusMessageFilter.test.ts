import { VPContainer } from "#ie/vehicle-positions/ioc/Di";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { RegionalBusMessageFilter } from "#ie/vehicle-positions/workers/runs/helpers/regional-bus/RegionalBusMessageFilter";
import { IRegionalBusRunsMessagesModel } from "#sch/vehicle-positions/models/interfaces/IRegionalBusRunsMessagesModel";
import { ILogger } from "@golemio/core/dist/helpers";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";

describe("RegionalBusMessageFilter", () => {
    let sandbox: SinonSandbox;
    let filter: RegionalBusMessageFilter;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        filter = VPContainer.resolve(VPContainerToken.RegionalBusMessageFilter);
    });

    afterEach(() => {
        sandbox?.restore();
    });

    // =============================================================================
    // getFilteredMessages
    // =============================================================================
    describe("getFilteredMessages", () => {
        it("should return messages that are valid for processing", () => {
            filter["logger"] = {
                info: sandbox.stub(),
                verbose: sandbox.stub(),
                error: sandbox.stub(),
            } as Partial<ILogger> as ILogger;

            const messages: IRegionalBusRunsMessagesModel[] = [
                {
                    external_trip_id: "862506042070402",
                    cis_line_id: null,
                    cis_trip_number: null,
                    events: "R",
                    coordinates: {
                        type: "Point",
                        coordinates: [14.60036, 50.1105],
                    },
                    vehicle_timestamp: new Date("2023-08-09T05:53:26Z"),
                    registration_number: null,
                    speed_kmh: 11,
                    bearing: 185,
                    is_terminated: false,
                },
                {
                    external_trip_id: "867377023799426",
                    cis_line_id: "100326",
                    cis_trip_number: 1023,
                    events: "P",
                    coordinates: {
                        type: "Point",
                        coordinates: [14.5143, 49.98615],
                    },
                    vehicle_timestamp: new Date("2023-08-09T05:53:26Z"),
                    registration_number: 1981,
                    speed_kmh: 34,
                    bearing: 266,
                    is_terminated: false,
                    timestamp: "2023-08-09T05:56:42Z",
                },
                {
                    external_trip_id: "860425042364297",
                    cis_line_id: "100224",
                    cis_trip_number: 1023,
                    events: "",
                    coordinates: {
                        type: "Point",
                        coordinates: [14.54071, 50.0987],
                    },
                    vehicle_timestamp: new Date("2023-08-09T05:56:42Z"),
                    registration_number: 9044,
                    speed_kmh: 0,
                    bearing: 60,
                    is_terminated: false,
                },
                {
                    external_trip_id: "860425042364297",
                    cis_line_id: "100224",
                    cis_trip_number: 1023,
                    events: "",
                    coordinates: {
                        type: "Point",
                        coordinates: [14.54071, 50.0987],
                    },
                    vehicle_timestamp: new Date("2023-08-09T05:56:42Z"),
                    registration_number: 9044,
                    speed_kmh: 0,
                    bearing: 60,
                    is_terminated: false,
                    timestamp: new Date("2023-07-09T05:56:42Z").toString(),
                },
                {
                    external_trip_id: "860425042364297",
                    cis_line_id: "100224",
                    cis_trip_number: 1023,
                    events: "",
                    coordinates: {
                        type: "Point",
                        coordinates: [14.54071, 50.0987],
                    },
                    vehicle_timestamp: new Date("2023-08-09T05:56:42Z"),
                    registration_number: 9044,
                    speed_kmh: 0,
                    bearing: 60,
                    is_terminated: false,
                    timestamp: new Date("2023-07-08T05:59:42Z").toString(),
                },
            ];

            const result = filter.getFilteredMessages(messages, new Date("2023-08-09T05:56:42Z").getTime());
            expect(result).to.have.lengthOf(1);
            expect(result[0]).to.deep.equal(messages[1]);
        });
    });
});
