import { VPContainer } from "#ie/vehicle-positions/ioc/Di";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { CisLookupManager } from "#ie/vehicle-positions/workers/runs/helpers/regional-bus/CisLookupManager";
import { IRegionalBusRunsMessagesModel } from "#sch/vehicle-positions/models/interfaces/IRegionalBusRunsMessagesModel";
import { RedisConnector } from "@golemio/core/dist/integration-engine/connectors";
import { DependencyContainer, container as RootContainer } from "@golemio/core/dist/shared/tsyringe";
import { expect } from "chai";
import sinon, { SinonSandbox, SinonStub } from "sinon";

describe("CisLookupManager", () => {
    let sandbox: SinonSandbox;
    let manager: CisLookupManager;
    let TestContainer: DependencyContainer;
    let setStub: SinonStub;
    let mgetStub: SinonStub;

    before(() => {
        setStub = sinon.stub().resolves();
        mgetStub = sinon.stub().resolves([
            {
                cis_line_id: "100204",
                cis_trip_number: 1013,
                registration_number: 9145,
            },
            null,
        ]);

        TestContainer = RootContainer.createChildContainer()
            .register(
                VPContainerToken.RegionalBusCisCacheRepository,
                class DummyRepository {
                    set = setStub;
                    mget = mgetStub;
                }
            )
            .registerSingleton(VPContainerToken.CisLookupManager, CisLookupManager);
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        sandbox.stub(RedisConnector, "getConnection");
        manager = TestContainer.resolve(VPContainerToken.CisLookupManager);
    });

    afterEach(() => {
        sandbox?.restore();
    });

    after(() => {
        VPContainer.clearInstances();
    });

    // =============================================================================
    // enrichMessagesWithCisData
    // =============================================================================
    describe("enrichMessagesWithCisData", () => {
        it("should enrich messages with missing data if available", async () => {
            const messages: IRegionalBusRunsMessagesModel[] = [
                {
                    external_trip_id: "862506042070402",
                    cis_line_id: null,
                    cis_trip_number: null,
                    events: "R",
                    coordinates: {
                        type: "Point",
                        coordinates: [14.60036, 50.1105],
                    },
                    vehicle_timestamp: new Date("2023-08-09T05:53:26Z"),
                    registration_number: null,
                    speed_kmh: 11,
                    bearing: 185,
                    is_terminated: false,
                },
                {
                    external_trip_id: "867377023799426",
                    cis_line_id: "100326",
                    cis_trip_number: 1023,
                    events: "P",
                    coordinates: {
                        type: "Point",
                        coordinates: [14.5143, 49.98615],
                    },
                    vehicle_timestamp: new Date("2023-08-09T05:53:26Z"),
                    registration_number: 1981,
                    speed_kmh: 34,
                    bearing: 266,
                    is_terminated: false,
                },
            ];

            const result = await manager.enrichMessagesWithCisData(messages);
            expect(result).to.have.lengthOf(2);
            expect(result[0]).to.deep.equal({
                ...messages[0],
                cis_line_id: "100204",
                cis_trip_number: 1013,
                registration_number: 9145,
            });
            expect(result[1]).to.deep.equal(messages[1]);
            expect(mgetStub.callCount).to.equal(1);
            expect(setStub.callCount).to.equal(1);
        });
    });
});
