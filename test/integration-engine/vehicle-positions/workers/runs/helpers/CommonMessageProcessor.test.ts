import { CommonMessageProcessor } from "#ie/vehicle-positions/workers/runs/helpers/CommonMessageProcessor";
import { QueueManager } from "@golemio/core/dist/integration-engine/queueprocessors";
import { DateTime } from "@golemio/core/dist/shared/luxon";
import sinon, { SinonSandbox, SinonStub } from "sinon";

describe("CommonMessageProcessor", () => {
    let sandbox: SinonSandbox;
    let processor: CommonMessageProcessor;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        sandbox.stub(QueueManager, "sendMessageToExchange").resolves();
        processor = new CommonMessageProcessor({
            getRunRecordForUpdate: sinon.stub(),
            createAndAssociate: sinon.stub(),
            updateAndAssociate: sinon.stub(),
            runsMessagesRepository: { getLastMessage: sinon.stub() },
        } as any);
        processor["logger"] = { error: sinon.stub() } as any;
    });

    afterEach(() => {
        sandbox?.restore();
    });

    it("should call the correct methods (new record)", async () => {
        await processor.processTransformedRun({
            run: { msg_last_timestamp: "2021-06-10T17:00:00+02:00" },
            run_message: {},
        } as any);

        sandbox.assert.calledOnce(processor["runsRepository"].getRunRecordForUpdate as SinonStub);
        sandbox.assert.calledOnce(processor["runsRepository"].createAndAssociate as SinonStub);
        sandbox.assert.calledOnce(QueueManager.sendMessageToExchange as SinonStub);
    });

    it("should call the correct methods (record update)", async () => {
        (processor["runsRepository"].getRunRecordForUpdate as SinonStub).callsFake(() =>
            Promise.resolve({
                id: "91_71_8257_2021-06-10T16:21:00+02:00",
                route_id: "91",
                run_number: 71,
                line_short_name: "2",
                registration_number: 8257,
                msg_start_timestamp: "2021-06-10T16:21:00+02:00",
                msg_last_timestamp: "2021-06-10T16:33:12+02:00",
            })
        );

        (processor["runsRepository"]["runsMessagesRepository"].getLastMessage as SinonStub).callsFake(() =>
            Promise.resolve({
                actual_stop_timestamp_scheduled: 123456789,
            })
        );

        await processor.processTransformedRun({
            run: { msg_last_timestamp: "2021-06-10T17:00:00+02:00" },
            run_message: {},
        } as any);

        sandbox.assert.calledOnce(processor["runsRepository"].getRunRecordForUpdate as SinonStub);
        sandbox.assert.calledOnce(processor["runsRepository"].updateAndAssociate as SinonStub);
        sandbox.assert.calledWith(
            processor["runsRepository"].updateAndAssociate as SinonStub,
            {
                run: { msg_last_timestamp: "2021-06-10T17:00:00+02:00" },
                run_message: { actual_stop_timestamp_scheduled: 123456789 },
            },
            "91_71_8257_2021-06-10T16:21:00+02:00"
        );
        sandbox.assert.calledOnce(QueueManager.sendMessageToExchange as SinonStub);
    });

    it("should log error when message timestamp 'tjr' is 22 hrs ahead", async () => {
        const now = DateTime.now();
        const clock = sinon.useFakeTimers({
            now: now.toJSDate(),
        });

        await processor.processTransformedRun({
            run: {},
            run_message: { actual_stop_timestamp_scheduled: now.plus({ hours: 22 }).toJSDate() },
        } as any);

        sandbox.assert.calledOnce(processor["logger"].error as unknown as SinonStub);

        clock.restore();
    });

    it("should log error when message timestamp 'tjr' is >22 hrs ahead", async () => {
        const now = DateTime.now();
        const clock = sinon.useFakeTimers({
            now: now.toJSDate(),
        });

        await processor.processTransformedRun({
            run: {},
            run_message: { actual_stop_timestamp_scheduled: now.plus({ hours: 25 }).toJSDate() },
        } as any);

        sandbox.assert.calledOnce(processor["logger"].error as unknown as SinonStub);

        clock.restore();
    });

    it("should not log error when message timestamp 'tjr' is <22 hrs ahead", async () => {
        const now = DateTime.now();
        const clock = sinon.useFakeTimers({
            now: now.toJSDate(),
        });

        await processor.processTransformedRun({
            run: {},
            run_message: { actual_stop_timestamp_scheduled: now.plus({ hours: 21, minutes: 59 }).toJSDate() },
        } as any);

        sandbox.assert.notCalled(processor["logger"].error as unknown as SinonStub);

        clock.restore();
    });

    it("should not log error when message timestamp 'tjr' is >22 hrs behind", async () => {
        const now = DateTime.now();
        const clock = sinon.useFakeTimers({
            now: now.toJSDate(),
        });

        await processor.processTransformedRun({
            run: {},
            run_message: { actual_stop_timestamp_scheduled: now.minus({ hours: 23 }).toJSDate() },
        } as any);

        sandbox.assert.notCalled(processor["logger"].error as unknown as SinonStub);

        clock.restore();
    });
});
