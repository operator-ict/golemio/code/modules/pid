import { VPContainer } from "#ie/vehicle-positions/ioc/Di";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { TimestampValidator } from "#ie/vehicle-positions/workers/runs/helpers/TimestampValidator";

describe("TimestampValidator", () => {
    const timestampValidator = VPContainer.resolve<TimestampValidator>(VPContainerToken.TimestampValidator);

    it("should return false -> time is more then 2 hours behind timestamp", () => {
        timestampValidator.isTimestampValid(1714728582294, "2024-05-03T09:30:43+00:00");
    });
    it("should return false -> time is more then 1 minutes ahead of timestamp", () => {
        timestampValidator.isTimestampValid(1714728582294, "2024-05-03T11:40:43+00:00");
    });
    it("should return true", () => {
        timestampValidator.isTimestampValid(1714728582294, "2024-05-03T09:30:43+00:00");
    });
});
