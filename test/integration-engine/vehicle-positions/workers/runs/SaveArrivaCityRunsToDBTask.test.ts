import { VPContainer } from "#ie/vehicle-positions/ioc/Di";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { RegionalBusRunsMessagesRepository } from "#ie/vehicle-positions/workers/runs/data-access/RegionalBusRunsMessagesRepository";
import { RegionalBusCisCacheRepository } from "#ie/vehicle-positions/workers/runs/data-access/cache/RegionalBusCisCacheRepository";
import {
    IRegionalBusRunsInput,
    IRegionalBusRunsMessagePropertiesWrapper,
} from "#ie/vehicle-positions/workers/runs/interfaces/RegionalBusRunsMessageInterfaces";
import { SaveArrivaCityRunsToDBTask } from "#ie/vehicle-positions/workers/runs/tasks/SaveArrivaCityRunsToDBTask";
import { PostgresConnector, RedisConnector } from "@golemio/core/dist/integration-engine/connectors";
import { QueueManager } from "@golemio/core/dist/integration-engine/queueprocessors";
import { Op } from "@golemio/core/dist/shared/sequelize";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox, SinonStub } from "sinon";

chai.use(chaiAsPromised);

describe("RunsWorker - SaveArrivaCityRunsToDBTask", () => {
    let sandbox: SinonSandbox;
    let sendMessageStub: SinonStub;
    let task: SaveArrivaCityRunsToDBTask;

    const testParamsValid: IRegionalBusRunsInput = {
        M: {
            V: [
                {
                    $: {
                        imei: "862506042070402",
                        events: "R",
                        lat: "50.1105",
                        lng: "14.60036",
                        tm: "2024-04-25T11:55:38",
                        rych: "11",
                        smer: "185",
                    },
                },
                {
                    $: {
                        imei: "867377023799426",
                        line: "100326",
                        conn: "1023",
                        events: "P",
                        lat: "49.98615",
                        lng: "14.5143",
                        tm: "2024-04-25T11:55:38",
                        evc: "1981",
                        rych: "34",
                        smer: "266",
                    },
                },
            ],
        },
    };

    before(async () => {
        await PostgresConnector.connect();
        await RedisConnector.connect();
    });

    beforeEach(async () => {
        sandbox = sinon.createSandbox();
        sendMessageStub = sandbox.stub(QueueManager, "sendMessageToExchange").resolves();

        task = VPContainer.resolve<SaveArrivaCityRunsToDBTask>(VPContainerToken.SaveArrivaCityRunsToDBTask);
    });

    afterEach(() => {
        sandbox?.restore();
    });

    it("should validate and execute", async () => {
        const messageRepository = new RegionalBusRunsMessagesRepository();
        const cacheRepository = new RegionalBusCisCacheRepository();
        await cacheRepository.truncate(true);

        const promise = task.consume({
            content: Buffer.from(JSON.stringify(testParamsValid)),
            properties: { timestamp: 1714046138000 },
        } as any);

        await expect(promise).to.be.fulfilled;

        const messages = await messageRepository.find({
            where: {
                external_trip_id: {
                    [Op.in]: (testParamsValid.M.V as IRegionalBusRunsMessagePropertiesWrapper[]).map((v) => v.$.imei),
                },
            },
        });

        const cisCacheData = await cacheRepository.mget<any>(
            (testParamsValid.M.V as IRegionalBusRunsMessagePropertiesWrapper[]).map((v) => v.$.imei)
        );

        expect(messages).to.have.lengthOf(1);
        expect(messages[0].get("external_trip_id")).to.equal("867377023799426");

        expect(cisCacheData).to.have.lengthOf(2);
        expect(cisCacheData[0]).to.be.null;
        expect(cisCacheData[1]).to.deep.equal({
            cis_line_id: "100326",
            cis_trip_number: 1023,
            registration_number: 1981,
        });

        expect(sendMessageStub.callCount).to.equal(1);
    });
});
