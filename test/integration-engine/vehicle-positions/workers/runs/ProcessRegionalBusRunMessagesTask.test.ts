import { VPContainer } from "#ie/vehicle-positions/ioc/Di";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { IExtendedSchedule } from "#ie/vehicle-positions/workers/runs/helpers/regional-bus/interfaces/IExtendedSchedule";
import { IProcessRegionalBusRunMessagesInput } from "#ie/vehicle-positions/workers/runs/interfaces/IProcessRegionalBusRunMessagesInput";
import { ProcessRegionalBusRunMessagesTask } from "#ie/vehicle-positions/workers/runs/tasks/ProcessRegionalBusRunMessagesTask";
import { PostgresConnector, RedisConnector } from "@golemio/core/dist/integration-engine/connectors";
import { QueueManager } from "@golemio/core/dist/integration-engine/queueprocessors";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox, SinonStub } from "sinon";

chai.use(chaiAsPromised);

describe("RunsWorker - ProcessRegionalBusRunMessagesTask", () => {
    let sandbox: SinonSandbox;
    let sendMessageToExchangeStub: SinonStub;
    let task: ProcessRegionalBusRunMessagesTask;

    const testParamsValid: IProcessRegionalBusRunMessagesInput = {
        messages: [
            {
                external_trip_id: "867377023799426",
                cis_line_id: "100326",
                cis_trip_number: 1023,
                events: "P",
                coordinates: {
                    type: "Point",
                    coordinates: [14.5143, 49.98615],
                },
                vehicle_timestamp: new Date("2023-08-09T05:53:26Z"),
                registration_number: 1981,
                speed_kmh: 34,
                bearing: 266,
                is_terminated: false,
                timestamp: new Date().toString(),
            },
        ],
    };

    const testParamInvalid = {
        messages: {},
    };

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox
                    .stub()
                    .returns({ hasMany: sandbox.stub(), belongsTo: sandbox.stub(), removeAttribute: sandbox.stub() }),
            })
        );
        sandbox.stub(RedisConnector, "getConnection");
        sendMessageToExchangeStub = sandbox.stub(QueueManager, "sendMessageToExchange").resolves();

        task = VPContainer.resolve(VPContainerToken.ProcessRegionalBusRunMessagesTask);
    });

    afterEach(() => {
        sandbox?.restore();
    });

    it("should validate and execute", async () => {
        sandbox.stub(task["gtfsTripRunManager"], "setAndReturnScheduledTrips").resolves(["defined"] as any);
        sandbox.stub(task["gtfsTripRunManager"], "generateDelayMsg").resolves({ updatedTrips: ["defined"] } as any);

        const tripCacheStub = sandbox
            .stub(task, "getGtfsTripsFromCache" as any)
            .resolves(new Map<string, IExtendedSchedule>([["867377023799426", {} as any]]));

        const promise = task.consume({ content: Buffer.from(JSON.stringify(testParamsValid)) } as any);

        await expect(promise).to.be.fulfilled;
        expect(tripCacheStub.callCount).to.equal(1);
        expect(sendMessageToExchangeStub.callCount).to.equal(1);
    });

    it("should not validate (invalid messages)", async () => {
        const tripCacheStub = sandbox.stub(task, "getGtfsTripsFromCache" as any);
        const promise = task.consume({ content: Buffer.from(JSON.stringify(testParamInvalid)) } as any);
        await expect(promise).to.be.rejectedWith(
            GeneralError,
            "[Queue VehiclePositionsRuns.processRegionalBusRunMessages] Message validation failed: [messages must be an array]"
        );

        expect(tripCacheStub.callCount).to.equal(0);
    });
});
