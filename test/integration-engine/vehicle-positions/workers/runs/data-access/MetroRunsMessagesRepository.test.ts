import { MetroRunsMessagesRepository } from "#ie/vehicle-positions/workers/runs/data-access/MetroRunsMessagesRepository";
import { IMetroRunsMessagesModel } from "#sch/vehicle-positions/models/interfaces/IMetroRunsMessagesModel";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";

chai.use(chaiAsPromised);

describe("MetroRunsMessagesRepository", () => {
    let repository: MetroRunsMessagesRepository;

    before(() => {
        return PostgresConnector.connect();
    });

    beforeEach(() => {
        repository = new MetroRunsMessagesRepository();
    });

    it("should save data", async () => {
        const auditTimestamp = new Date().getTime() - 49 * 60 * 60 * 1000;
        const data: Array<IMetroRunsMessagesModel & { created_at: number; updated_at: number }> = [
            {
                route_name: "A",
                message_timestamp: new Date("2022-07-21T13:45:34Z"),
                train_set_number_scheduled: "3",
                train_set_number_real: "3",
                train_number: "280",
                track_id: "1810",
                delay_origin: -20,
                actual_position_timestamp_scheduled: new Date("2022-07-21T13:45:14Z"),

                // Inject timestamps so we can delete the new row afterwards
                created_at: auditTimestamp,
                updated_at: auditTimestamp,
            },
        ];

        const result = await repository.bulkSave(data as any);
        expect(result[0].toJSON()).to.deep.equal({
            route_name: "A",
            message_timestamp: new Date("2022-07-21T13:45:34.000Z"),
            train_set_number_scheduled: "3",
            train_set_number_real: "3",
            train_number: "280",
            track_id: "1810",
            delay_origin: -20,
            actual_position_timestamp_scheduled: new Date("2022-07-21T13:45:14.000Z"),
            created_at: new Date(auditTimestamp),
            updated_at: new Date(auditTimestamp),
        });
    });

    it("deleteNHoursOldData should delete older rows", async () => {
        const result = await repository.deleteNHoursOldData(48, "message_timestamp");
        expect(result).to.equal(1);
    });
});
