import { ICommonRunWithMessageDto } from "#sch/vehicle-positions/models/interfaces/ICommonRunWithMessageDto";
import { TCPEventEnum } from "src/const";

export const sourceDataNestedTransformed: ICommonRunWithMessageDto[] = [
    {
        run: {
            id: "2_2_8530_2021-06-10T16:22:07+02:00",
            route_id: "2",
            run_number: 2,
            line_short_name: "2",
            registration_number: "8530",
            msg_start_timestamp: "2021-06-10T16:22:07+02:00",
            msg_last_timestamp: "2021-06-10T16:22:07+02:00",
            wheelchair_accessible: false,
        },
        run_message: {
            lat: 50.09682,
            lng: 14.35231,
            actual_stop_asw_id: "00730002",
            actual_stop_timestamp_real: new Date(1623334927000),
            actual_stop_timestamp_scheduled: new Date(1623189600000),
            last_stop_asw_id: "05080002",
            packet_number: "237605",
            msg_timestamp: new Date("2021-06-08T23:59:56+02:00"),
            events: TCPEventEnum.DEPARTURED,
        },
    },
    {
        run: {
            id: "95_52_8509_2021-06-10T16:22:07+02:00",
            route_id: "95",
            run_number: 52,
            line_short_name: "95",
            registration_number: "8509",
            msg_start_timestamp: "2021-06-10T16:22:07+02:00",
            msg_last_timestamp: "2021-06-10T16:22:07+02:00",
            wheelchair_accessible: false,
        },
        run_message: {
            lat: 50.09117,
            lng: 14.43878,
            actual_stop_asw_id: "06890001",
            actual_stop_timestamp_real: new Date(1623334927000),
            actual_stop_timestamp_scheduled: new Date(1623189540000),
            last_stop_asw_id: "05080002",
            packet_number: "237606",
            msg_timestamp: new Date("2021-06-08T23:59:56+02:00"),
            events: TCPEventEnum.DEPARTURED,
        },
    },
];
