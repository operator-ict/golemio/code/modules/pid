import { ICommonRunsInput } from "#ie/vehicle-positions/workers/runs/interfaces/CommonRunsMessageInterfaces";
import { SaveBusRunsToDBTask } from "#ie/vehicle-positions/workers/runs/tasks/SaveBusRunsToDBTask";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { QueueManager } from "@golemio/core/dist/integration-engine/queueprocessors";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";
import { TCPEventEnum } from "src/const";

chai.use(chaiAsPromised);

describe("RunsWorker - SaveBusRunsToDBTask", () => {
    let sandbox: SinonSandbox;
    let task: SaveBusRunsToDBTask;

    const testParamsValidBus: ICommonRunsInput = {
        M: {
            V: {
                $: {
                    turnus: "191/8",
                    line: "191",
                    evc: "3603",
                    np: "ano",
                    lat: "50.07068",
                    lng: "14.38682",
                    akt: "02540005",
                    takt: "2023-01-22T22:11:23",
                    konc: "10400016",
                    tjr: "2023-01-22T22:08:00",
                    pkt: "13130291",
                    tm: "2023-01-22T22:11:25",
                    events: TCPEventEnum.TIME,
                },
            },
        },
    };
    const testParamsInternalBusLine: ICommonRunsInput = {
        M: {
            V: {
                $: {
                    turnus: "90/8",
                    line: "90",
                    evc: "3603",
                    np: "ano",
                    lat: "50.07068",
                    lng: "14.38682",
                    akt: "02540005",
                    takt: "2023-01-22T22:11:23",
                    konc: "10400016",
                    tjr: "2023-01-22T22:08:00",
                    pkt: "13130291",
                    tm: "2023-01-22T22:11:25",
                    events: TCPEventEnum.TIME,
                },
            },
        },
    };

    const testParamsValidTrolley: ICommonRunsInput = {
        M: {
            V: {
                $: {
                    turnus: "58/1",
                    line: "58",
                    evc: "0509",
                    lat: "50.11614",
                    lng: "14.49470",
                    akt: "07540001",
                    takt: "2023-01-22T22:11:23",
                    konc: "03840001",
                    tjr: "2023-06-03T08:16:00",
                    pkt: "733775",
                    tm: "2023-06-03T08:16:18",
                    events: TCPEventEnum.DEPARTURED,
                },
            },
        },
    };

    const testParamsInvalid = {
        M: {
            V: {
                $: {
                    turnus: "191/8",
                    line: "191",
                    evc: "3603",
                    np: "ano",
                    lat: "50.07068",
                    lng: "14.38682",
                    akt: "02540005",
                    takt: "2023-01-22T22:11:23",
                    konc: "10400016",
                    tjr: "2023-01-22T22:08:00",
                    pkt: "13130291",
                    tm: "2023-01-22T22:11:25",
                    events: "TT",
                },
            },
        },
    };

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox
                    .stub()
                    .returns({ removeAttribute: sandbox.stub(), belongsTo: sandbox.stub(), hasMany: sandbox.stub() }),
            })
        );
        sandbox.stub(QueueManager, "sendMessageToExchange").resolves();

        task = new SaveBusRunsToDBTask();
    });

    afterEach(() => {
        sandbox?.restore();
    });

    it("should validate and execute", async () => {
        const transformSpy = sinon.spy(task["messagesTransformation"], "transform");
        const processStub = sinon.stub(task["messageProcessor"], "processTransformedRun").resolves();
        const promise = task.consume({
            content: Buffer.from(JSON.stringify(testParamsValidBus)),
            properties: { timestamp: new Date("2023-01-22T22:11:23").getTime() },
        } as any);

        await expect(promise).to.be.fulfilled;
        expect(transformSpy.callCount).to.equal(1);
        expect(processStub.callCount).to.equal(1);
    });

    it("should validate and execute (trolleybus line)", async () => {
        const transformSpy = sinon.spy(task["messagesTransformation"], "transform");
        const processStub = sinon.stub(task["messageProcessor"], "processTransformedRun").resolves();
        const promise = task.consume({
            content: Buffer.from(JSON.stringify(testParamsValidTrolley)),
            properties: { timestamp: new Date("2023-01-22T22:11:23").getTime() },
        } as any);

        await expect(promise).to.be.fulfilled;
        expect(transformSpy.callCount).to.equal(1);
        expect(processStub.callCount).to.equal(1);
    });

    it("should not process (skip internal bus line)", async () => {
        const transformSpy = sinon.spy(task["messagesTransformation"], "transform");
        const processStub = sinon.stub(task["messageProcessor"], "processTransformedRun").resolves();
        const promise = task.consume({
            content: Buffer.from(JSON.stringify(testParamsInternalBusLine)),
            properties: { timestamp: new Date("2023-01-22T22:11:23").getTime() },
        } as any);

        await expect(promise).to.be.fulfilled;
        expect(transformSpy.callCount).to.equal(1);
        expect(processStub.callCount).to.equal(0);
    });

    it("should reject (missing timestamp)", async () => {
        const transformSpy = sinon.spy(task["messagesTransformation"], "transform");
        const promise = task.consume({
            content: Buffer.from(JSON.stringify(testParamsValidBus)),
        } as any);

        await expect(promise).to.be.rejectedWith(
            GeneralError,
            // eslint-disable-next-line max-len
            `Missing bus run message timestamp: {"M":{"V":{"$":{"turnus":"191/8","line":"191","evc":"3603","np":"ano","lat":"50.07068","lng":"14.38682","akt":"02540005","takt":"2023-01-22T22:11:23","konc":"10400016","tjr":"2023-01-22T22:08:00","pkt":"13130291","tm":"2023-01-22T22:11:25","events":"T"}}}}`
        );

        expect(transformSpy.callCount).to.equal(0);
    });

    it("should not validate (missing route_name and delay_origin)", async () => {
        const transformSpy = sinon.spy(task["messagesTransformation"], "transform");
        const promise = task.consume({ content: Buffer.from(JSON.stringify(testParamsInvalid)) } as any);

        await expect(promise).to.be.rejectedWith(
            GeneralError,
            // eslint-disable-next-line max-len
            "[Queue VehiclePositionsRuns.saveBusRunsToDB] Message validation failed: [events must be one of the following values: P, O, V, T]"
        );

        expect(transformSpy.callCount).to.equal(0);
    });
});
