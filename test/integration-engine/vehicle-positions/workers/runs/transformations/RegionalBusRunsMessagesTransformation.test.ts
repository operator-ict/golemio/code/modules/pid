import { VPContainer } from "#ie/vehicle-positions/ioc/Di";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { IRegionalBusRunsMessagePropertiesWrapper } from "#ie/vehicle-positions/workers/runs/interfaces/RegionalBusRunsMessageInterfaces";
import { RegionalBusRunsMessagesTransformation } from "#ie/vehicle-positions/workers/runs/transformations/RegionalBusRunsMessagesTransformation";
import { expect } from "chai";
import moment from "moment";
import sinon from "sinon";
const testData: IRegionalBusRunsMessagePropertiesWrapper[] = [
    {
        $: {
            imei: "867377023929577",
            lat: "49.99692",
            lng: "14.58208",
            tm: "2024-04-25T13:55:35",
            events: "T",
            rych: "0",
            smer: "125",
        },
    },
    {
        $: {
            imei: "862506047089852",
            lat: "50.11449",
            lng: "14.70045",
            tm: "2024-04-25T13:55:38",
            events: "L",
            rych: "25",
            smer: "297",
        },
    },
];

describe("RegionalBusRunsMessagesTransformation", () => {
    const transformation = VPContainer.resolve<RegionalBusRunsMessagesTransformation>(
        VPContainerToken.RegionalBusRunsMessagesTransformation
    );

    it("transform should transform array", () => {
        const clock = sinon.useFakeTimers({
            now: moment("2024-04-25T13:55:38").toDate(),
            shouldAdvanceTime: true,
        });

        const res = transformation.transformArray(testData);

        expect(res).to.have.length(2);
        expect(res).to.deep.eq([
            {
                external_trip_id: "867377023929577",
                cis_line_id: null,
                cis_trip_number: null,
                events: "T",
                coordinates: { type: "Point", coordinates: [14.58208, 49.99692] },
                vehicle_timestamp: new Date("2024-04-25T13:55:35.000Z"),
                timestamp: "2024-04-25T13:55:35",
                registration_number: null,
                speed_kmh: 0,
                bearing: 125,
                is_terminated: false,
            },
            {
                external_trip_id: "862506047089852",
                cis_line_id: null,
                cis_trip_number: null,
                events: "L",
                coordinates: { type: "Point", coordinates: [14.70045, 50.11449] },
                vehicle_timestamp: new Date("2024-04-25T13:55:38.000Z"),
                timestamp: "2024-04-25T13:55:38",
                registration_number: null,
                speed_kmh: 25,
                bearing: 297,
                is_terminated: false,
            },
        ]);

        clock.reset();
    });
});
