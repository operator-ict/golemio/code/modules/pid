import { VPContainer } from "#ie/vehicle-positions/ioc/Di";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { IMetroRunsInput } from "#ie/vehicle-positions/workers/runs/interfaces/MetroRunsMessageInterfaces";
import { MetroRunsMessagesTransformation } from "#ie/vehicle-positions/workers/runs/transformations/MetroRunsMessagesTransformation";
import { expect } from "chai";
import moment from "moment";
import sinon, { SinonSandbox } from "sinon";

describe("MetroRunsMessagesTransformation", () => {
    let transformation: MetroRunsMessagesTransformation;
    let sandbox: SinonSandbox;
    beforeEach(() => {
        sandbox = sinon.createSandbox();
        transformation = VPContainer.resolve<MetroRunsMessagesTransformation>(VPContainerToken.MetroRunsMessagesTransformation);
    });

    afterEach(() => {
        afterEach(() => {
            sandbox?.restore();
        });
    });

    const testDataObject: IMetroRunsInput = {
        m: {
            $: { linka: "A", tm: "2022-07-20T13:45:34Z", gvd: "GD20a" },
            vlak: { $: { csp: " 3", csr: " 3", cv: "279", ko: "1809", odch: "25" } },
        },
    };

    const testDataArray: IMetroRunsInput = {
        m: {
            $: { linka: "A", tm: "2022-07-21T13:45:34Z", gvd: "GD20a" },
            vlak: [
                { $: { csp: " 3", csr: " 3", cv: "279", ko: "1809", odch: "25" } },
                { $: { csp: " 3", csr: " 3", cv: "280", ko: "1810", odch: "-20" } },
            ],
        },
    };

    it("transform should transform object", async () => {
        const clock = sinon.useFakeTimers({
            now: moment("2022-07-20T13:45:34").toDate(),
            shouldAdvanceTime: true,
        });
        const result = await transformation.transform({ data: testDataObject });

        expect(result[0]).to.deep.equal({
            route_name: "A",
            message_timestamp: new Date("2022-07-20T13:45:34Z"),
            train_set_number_scheduled: "3",
            train_set_number_real: "3",
            train_number: "279",
            track_id: "1809",
            delay_origin: 25,
            actual_position_timestamp_scheduled: new Date("2022-07-20T13:45:59Z"),
        });
        clock.restore();
    });

    it("transform should transform array", async () => {
        const clock = sinon.useFakeTimers({
            now: moment("2022-07-21T14:45:34").toDate(),
            shouldAdvanceTime: true,
        });
        const result = await transformation.transform({ data: testDataArray });

        expect(result[0]).to.deep.equal({
            route_name: "A",
            message_timestamp: new Date("2022-07-21T13:45:34Z"),
            train_set_number_scheduled: "3",
            train_set_number_real: "3",
            train_number: "279",
            track_id: "1809",
            delay_origin: 25,
            actual_position_timestamp_scheduled: new Date("2022-07-21T13:45:59Z"),
        });

        expect(result[1]).to.deep.equal({
            route_name: "A",
            message_timestamp: new Date("2022-07-21T13:45:34Z"),
            train_set_number_scheduled: "3",
            train_set_number_real: "3",
            train_number: "280",
            track_id: "1810",
            delay_origin: -20,
            actual_position_timestamp_scheduled: new Date("2022-07-21T13:45:14Z"),
        });
        clock.restore();
    });
});
