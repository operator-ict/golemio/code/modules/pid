import { RopidGtfsContainer } from "#ie/ropid-gtfs/ioc/Di";
import { RopidGtfsContainerToken } from "#ie/ropid-gtfs/ioc/RopidGtfsContainerToken";
import { PositionsRepository } from "#ie/vehicle-positions/workers/vehicle-positions/data-access/PositionsRepository";
import { PropagateTrainDelayTask } from "#ie/vehicle-positions/workers/vehicle-positions/tasks/PropagateTrainDelayTask";
import { PostgresConnector } from "@golemio/core/dist/integration-engine";
import { FatalError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import sinon, { SinonFakeTimers, SinonSandbox, SinonSpy, SinonStub } from "sinon";

chai.use(chaiAsPromised);

describe("VPWorker - PropagateDelayTrainTask", () => {
    let sandbox: SinonSandbox;
    let clock: SinonFakeTimers;

    const redisBlockDataMock = {
        "1302_2525_231211": {
            trip_stops: [
                {
                    stop_id: "U2816Z301",
                    stop_sequence: 1,
                    cis: 5453414,
                },
                {
                    stop_id: "U2973Z301",
                    stop_sequence: 13,
                    cis: 5453214,
                },
            ],
        },
        "1309_2525_231211": {
            trip_stops: [
                {
                    stop_id: "U2973Z302",
                    stop_sequence: 1,
                    cis: 5453214,
                },
            ],
        },
        "1320_2525_231211": {
            x: "y",
        },
    };

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        clock = sinon.useFakeTimers({
            now: new Date(2024, 1, 8, 8, 40, 58),
            shouldAdvanceTime: true,
        });
        PostgresConnector.connect();
        RopidGtfsContainer.registerInstance(RopidGtfsContainerToken.BlockStopsRedisRepository, {
            get: (test: string) => Promise.resolve(redisBlockDataMock),
        });
    });

    afterEach(() => {
        sandbox?.restore();
        clock?.restore();
    });

    it("propagate delay for train position id 2002", async () => {
        const repo = new PositionsRepository();

        const position = await repo["sequelizeModel"].findByPk(2002);

        expect(position.delay).to.be.equal(null);

        const task = new PropagateTrainDelayTask("test");
        const inputData = JSON.parse(
            fs.readFileSync(__dirname + "/data/vehiclepositions-input-propagateTrainDelay.json", "utf8")
        );
        await task["execute"](inputData);

        const position2 = await repo["sequelizeModel"].findByPk(2002);
        expect(position2.delay).to.be.equal(6);
    });

    it("propagate delay for last train in the block", async () => {
        const task = new PropagateTrainDelayTask("test");
        task["propagateDelayForGtfsTrip"] = sandbox.stub().resolves();

        const inputData = JSON.parse(
            fs.readFileSync(__dirname + "/data/vehiclepositions-input-propagateTrainDelay.json", "utf8")
        );
        inputData.data[0].trip.gtfs_trip_id = "1309_2525_231211";
        inputData.data[0].trip.start_timestamp = "2024-01-08T09:40:00.000+01:00";
        await task["execute"](inputData);
        sandbox.assert.notCalled(task["propagateDelayForGtfsTrip"] as SinonSpy);
    });

    it("unexpected input", async () => {
        const task = new PropagateTrainDelayTask("test");
        await expect(task["validateAndExecute"](undefined as any)).to.be.rejectedWith(
            GeneralError,
            "Message validation failed: [each value in data must be an object, data must be an array]"
        );
    });
});
