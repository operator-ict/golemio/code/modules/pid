import { PositionsRepository } from "#ie/vehicle-positions/workers/vehicle-positions/data-access/PositionsRepository";
import { ITripPositions } from "#ie/vehicle-positions/workers/vehicle-positions/data-access/interfaces/PositionRepositoryInterfaces";
import { IPositionToUpdate } from "#ie/vehicle-positions/workers/vehicle-positions/interfaces/VPInterfaces";
import { UpdateDelayTask } from "#ie/vehicle-positions/workers/vehicle-positions/tasks/UpdateDelayTask";
import { IVPTripsPositionAttributes } from "#sch/vehicle-positions/models/interfaces/IVPTripsPositionAttributes";
import { PostgresConnector, RedisConnector } from "@golemio/core/dist/integration-engine/connectors";
import { QueueManager } from "@golemio/core/dist/integration-engine/queueprocessors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonFakeTimers, SinonSandbox, SinonStub } from "sinon";
import { vpUpdateDelayTask1 } from "./data/vp-updatedelaytask-1";

chai.use(chaiAsPromised);

describe("VPWorker - UpdateDelayTask", () => {
    let sandbox: SinonSandbox;
    let clock: SinonFakeTimers;
    let task: UpdateDelayTask;
    let positionsRepository: PositionsRepository;

    beforeEach(async () => {
        sandbox = sinon.createSandbox();
        clock = sinon.useFakeTimers({
            now: new Date(2021, 12, 9, 10, 0),
            shouldAdvanceTime: true,
        });

        await PostgresConnector.connect();
        await RedisConnector.connect();

        sandbox.stub(QueueManager, "sendMessageToExchange").resolves();

        task = new UpdateDelayTask("test.test");

        positionsRepository = new PositionsRepository();
    });

    afterEach(() => {
        sandbox?.restore();
        clock?.restore();
    });

    it("should call the correct methods by updateDelay method", async () => {
        sandbox
            .stub(task["positionsRepository"], "getPositionsForUpdateDelay")
            .callsFake(() => [{ gtfs_trip_id: "0000", positions: [{ delay: null }] }] as any);
        sandbox.stub(task["delayComputationRedisRepository"], "mget").callsFake(() => Object.assign([{ shape_points: [] }]));
        sandbox
            .stub(task["positionsManager"], "computePositions")
            .callsFake(() => ({ context: { tripId: "TRIP1" }, positions: [{ delay: 10, id: "12321" }] } as any));
        sandbox.stub(task["positionsRepository"], "bulkSave");
        sandbox.stub(task["tripsRepository"], "bulkSave");

        await task["execute"]({
            positions: [],
            updatedTrips: ["TRIP_ID"],
        } as any);

        sandbox.assert.calledOnce(task["positionsRepository"].getPositionsForUpdateDelay as SinonStub);
        sandbox.assert.calledOnce(task["delayComputationRedisRepository"].mget as SinonStub);
        sandbox.assert.calledOnce(task["positionsManager"]["computePositions"] as SinonStub);
        sandbox.assert.calledOnce(task["positionsRepository"].bulkSave as SinonStub);
        sandbox.assert.calledOnce(task["tripsRepository"].bulkSave as SinonStub);
        sandbox.assert.calledOnce(QueueManager.sendMessageToExchange as SinonStub);
    });

    it("should assign the correct values to the 'valid_to' property", async () => {
        const input = {
            updatedTrips: [
                {
                    cis_line_id: "",
                    cis_trip_number: "",
                    run_number: 51,
                    cis_line_short_name: "",
                    created_at: "2022-03-17 01:50:30",
                    gtfs_route_id: "L91",
                    gtfs_route_short_name: 91,
                    gtfs_trip_id: "91_158_211029",
                    id: "2022-03-17T02:51:00+01:00_91_158_211029_8338",
                    updated_at: "2022-03-17 03:54:33",
                    start_cis_stop_id: "",
                    start_cis_stop_platform_code: "",
                    start_time: "02:51:00",
                    start_timestamp: new Date(1647481860000),
                    vehicle_type_id: 6,
                    wheelchair_accessible: false,
                    agency_name_scheduled: "DP PRAHA",
                    origin_route_name: 91,
                    agency_name_real: "DP PRAHA",
                    vehicle_registration_number: 8338,
                    gtfs_trip_headsign: "Staré Strašnice",
                    start_asw_stop_id: "",
                    gtfs_route_type: 0,
                    gtfs_block_id: "",
                    last_position_id: 317041616,
                    is_canceled: "",
                    end_timestamp: new Date(1647485640000),
                    gtfs_trip_short_name: "",
                },
            ],
            positions: [],
        };

        const updateComputedPositionsAndTripsStub = sandbox.stub(task, "updateComputedPositionsAndTrips" as any);

        await task.consume({ content: Buffer.from(JSON.stringify(input)) } as any);

        expect(updateComputedPositionsAndTripsStub.callCount).to.be.equal(1);

        const processedPositions = updateComputedPositionsAndTripsStub.getCall(0).args[0] as any[];
        for (let position of processedPositions[0].positions) {
            switch (position.id) {
                case "317031566":
                    expect(position.valid_to).to.deep.equal(new Date(1647484111000));
                    break;
                case "317031710":
                    expect(position.valid_to).to.deep.equal(new Date(1647484450000));
                    break;
                case "317041616":
                    expect(position.valid_to).to.deep.equal(new Date(1647485932000));
                    break;
            }
        }
    });

    it("should update only defined position attributes", async () => {
        const { trips_id, input, gtfsTripDelayComputation } = structuredClone(vpUpdateDelayTask1);
        const positionsRepositoryBulkSaveStub = sandbox.stub(task["positionsRepository"], "bulkSave");
        positionsRepositoryBulkSaveStub.callsFake((...args) => {
            for (let i = 0; i < args[0].length; i++) {
                (args[0][i] as IPositionToUpdate).id ??= `20231011${i}`;
            }
            return positionsRepository.bulkSave(...args);
        });
        const positionsProxy = input.positions.map(
            (position) =>
                new Proxy(position, {
                    get: (target, prop, ...args) => (prop in target ? Reflect.get(target, prop, ...args) : null),
                })
        ) as unknown as IVPTripsPositionAttributes[];
        const tripPositionsProxy = new Proxy(
            { positions: positionsProxy, ...input.updatedTrips[0] },
            { get: (target, prop, ...args) => (prop in target ? Reflect.get(target, prop, ...args) : null) }
        ) as unknown as ITripPositions;
        tripPositionsProxy.end_timestamp = new Date(tripPositionsProxy.end_timestamp);
        tripPositionsProxy.start_timestamp = new Date(tripPositionsProxy.start_timestamp);
        const tripsPositionsToUpdate: ITripPositions[] = [tripPositionsProxy];
        const getPositionsForUpdateDelayStub = sandbox.stub(task["positionsRepository"], "getPositionsForUpdateDelay");
        getPositionsForUpdateDelayStub.onFirstCall().returns(Promise.resolve(tripsPositionsToUpdate));
        sandbox.stub(task["delayComputationManager"], "getComputationObject").returns(Promise.resolve(gtfsTripDelayComputation));
        sandbox.stub(task["delayComputationRedisRepository"], "set").returns(Promise.resolve());
        const computePositionsStub = sandbox.stub(task["positionsManager"], "computePositions");
        computePositionsStub.onFirstCall().callsFake((...args) => {
            const value = (task["positionsManager"].computePositions as any).wrappedMethod.apply(this, args);
            for (const position of value?.positions ?? []) {
                position.delay = 10;
            }
            return value;
        });

        await task.consume({ content: Buffer.from(JSON.stringify(input)) } as any);

        computePositionsStub.onSecondCall().callsFake((...args) => {
            const value = (task["positionsManager"].computePositions as any).wrappedMethod.apply(this, args);
            if (value) {
                value.positions[0].delay = 20;
                delete value.positions[1].delay;
            }
            return value;
        });
        for (const position of tripsPositionsToUpdate[0].positions) {
            position.delay = 10;
        }
        getPositionsForUpdateDelayStub.onSecondCall().returns(Promise.resolve(tripsPositionsToUpdate));

        await task.consume({ content: Buffer.from(JSON.stringify(input)) } as any);
        const { rows, count } = await positionsRepository.findAndCountAll({ where: { trips_id } });

        expect(count).to.equal(2);
        expect(rows.map((row: { delay: number }) => row.delay)).to.deep.equal([20, 10]);
    });

    describe("invalidateBacktrackedPositions", () => {
        it("should invalidate backtracked positions", async () => {
            const invalidateStub = sandbox.stub(task["positionsRepository"], "invalidatePositionsAfterStopSequence").resolves(2);
            const loggerStub = sandbox.stub(task["logger"], "info");

            await task["invalidateBacktrackedPositions"]([
                {
                    context: {
                        tripId: "TRIP1",
                    },
                    positions: [
                        {
                            last_stop_sequence: 1,
                        },
                    ],
                    isBacktrackingDetected: false,
                },
                {
                    context: {
                        tripId: "TRIP2",
                    },
                    positions: [
                        {
                            last_stop_sequence: 2,
                        },
                    ],
                    isBacktrackingDetected: true,
                },
                {
                    context: {
                        tripId: "TRIP3",
                    },
                    positions: [
                        {
                            last_stop_sequence: 5,
                        },
                        {
                            last_stop_sequence: 8,
                        },
                    ],
                    isBacktrackingDetected: true,
                },
            ] as any);

            expect(loggerStub.firstCall.args).to.deep.equal(["Invalidated 2 backtracked positions"]);
            expect(invalidateStub.firstCall.args).to.deep.equal([
                [
                    ["TRIP2", 2],
                    ["TRIP3", 8],
                ],
            ]);
        });

        it("should not invalidate any positions", async () => {
            const invalidateStub = sandbox.stub(task["positionsRepository"], "invalidatePositionsAfterStopSequence").resolves(2);
            const loggerStub = sandbox.stub(task["logger"], "info");

            await task["invalidateBacktrackedPositions"]([
                {
                    context: {
                        tripId: "TRIP1",
                    },
                    positions: [
                        {
                            last_stop_sequence: 1,
                        },
                    ],
                    isBacktrackingDetected: false,
                },
                {
                    context: {
                        tripId: "TRIP2",
                    },
                    positions: [
                        {
                            last_stop_sequence: 2,
                        },
                    ],
                    isBacktrackingDetected: false,
                },
            ] as any);

            expect(loggerStub.notCalled).to.be.true;
            expect(invalidateStub.notCalled).to.be.true;
        });

        it("should be rejected (invalid last_stop_sequence)", async () => {
            const invalidateStub = sandbox.stub(task["positionsRepository"], "invalidatePositionsAfterStopSequence").resolves(2);
            const loggerStub = sandbox.stub(task["logger"], "info");

            const promise = task["invalidateBacktrackedPositions"]([
                {
                    context: {
                        tripId: "TRIP1",
                    },
                    positions: [
                        {
                            last_stop_sequence: 0,
                        },
                    ],
                    isBacktrackingDetected: true,
                },
            ] as any);

            await expect(promise).to.be.rejectedWith("Max stop sequence must be greater than 0");
            expect(loggerStub.notCalled).to.be.true;
            expect(invalidateStub.notCalled).to.be.true;
        });

        it("should be rejected (unknown last_stop_sequence)", async () => {
            const invalidateStub = sandbox.stub(task["positionsRepository"], "invalidatePositionsAfterStopSequence").resolves(2);
            const loggerStub = sandbox.stub(task["logger"], "info");

            const promise = task["invalidateBacktrackedPositions"]([
                {
                    context: {
                        tripId: "TRIP1",
                    },
                    positions: [
                        {
                            last_stop_sequence: null,
                        },
                    ],
                    isBacktrackingDetected: true,
                },
            ] as any);

            await expect(promise).to.be.rejectedWith("Max stop sequence must be greater than 0");
            expect(loggerStub.notCalled).to.be.true;
            expect(invalidateStub.notCalled).to.be.true;
        });
    });
});
