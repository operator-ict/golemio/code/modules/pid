import { IEnrichedTripForPublicApi } from "#ie/vehicle-positions/workers/vehicle-positions/data-access/interfaces/TripRepositoryInterfaces";
import { PublicApiTripTransformation } from "#ie/vehicle-positions/workers/vehicle-positions/transformations/PublicApiTripTransformation";
import { IPublicApiCacheDto } from "#sch/vehicle-positions/redis/interfaces/IPublicApiCacheDto";
import { expect } from "chai";
import { StatePositionEnum } from "src/const";
import { ProviderSourceTypeEnum } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/ProviderSourceTypeEnum";

describe("PublicApiTripTransformation", () => {
    let transformation: PublicApiTripTransformation;

    const inputMock: IEnrichedTripForPublicApi = {
        id: "2024-03-23T14:00:00.000Z_115_107_180501_39_40",
        gtfs_trip_id: "115_107_180501",
        provider_source_type: ProviderSourceTypeEnum.TcpCommon,
        cis_trip_number: 95,
        gtfs_route_type: 1,
        gtfs_route_short_name: "115",
        run_number: 39,
        internal_run_number: 40,
        vehicle_registration_number: 8581,
        wheelchair_accessible: true,
        lat: "50.13257",
        lng: "14.47052",
        bearing: null,
        delay: 163,
        state_position: StatePositionEnum.ON_TRACK,
        created_at: new Date("2024-01-23T10:10:10Z"),
        origin_route_name: "115",
        gtfs_shape_id: null,
        last_stop_headsign: "Jiná zastávka",
        gtfs_trip_headsign: "Dejvická",
        shape_dist_traveled: "0",
        last_stop_sequence: 1,
        origin_timestamp: new Date("2024-03-23T14:00:00.000Z"),
        agency_name_scheduled: "DP PRAHA",
        agency_name_real: null,
    };

    before(() => {
        transformation = new PublicApiTripTransformation();
    });

    it("should properly transform data (last stop headsign)", () => {
        const expected: IPublicApiCacheDto = {
            gtfs_trip_id: "115_107_180501",
            route_type: 1,
            gtfs_route_short_name: "115",
            lat: 50.13257,
            lng: 14.47052,
            bearing: null,
            delay: 163,
            state_position: StatePositionEnum.ON_TRACK,
            vehicle_id: "metro-115-40-39",
            detailed_info: {
                is_wheelchair_accessible: true,
                origin_route_name: "115",
                shape_id: null,
                run_number: 39,
                trip_headsign: "Jiná zastávka",
                shape_dist_traveled: 0,
                last_stop_sequence: 1,
                origin_timestamp: "2024-03-23T14:00:00.000Z",
                operator: "DP PRAHA",
                registration_number: 8581,
            },
        };

        expect(transformation.transformElement(inputMock)).to.deep.equal(expected);
    });

    it("should properly transform data (gtfs trip headsign)", () => {
        const expected: IPublicApiCacheDto = {
            gtfs_trip_id: "115_107_180501",
            route_type: 1,
            gtfs_route_short_name: "115",
            lat: 50.13257,
            lng: 14.47052,
            bearing: null,
            delay: 163,
            state_position: StatePositionEnum.ON_TRACK,
            vehicle_id: "metro-115-40-39",
            detailed_info: {
                is_wheelchair_accessible: true,
                origin_route_name: "115",
                shape_id: null,
                run_number: 39,
                trip_headsign: "Dejvická",
                shape_dist_traveled: 0,
                last_stop_sequence: 1,
                origin_timestamp: "2024-03-23T14:00:00.000Z",
                operator: "DP PRAHA",
                registration_number: 8581,
            },
        };

        expect(
            transformation.transformElement({
                ...inputMock,
                last_stop_headsign: null,
            })
        ).to.deep.equal(expected);
    });
});
