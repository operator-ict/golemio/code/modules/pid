import { DataRetentionTask } from "#ie/vehicle-positions/workers/vehicle-positions/tasks/DataRetentionTask";
import { PostgresConnector, RedisConnector } from "@golemio/core/dist/integration-engine/connectors";
import { Mutex } from "@golemio/core/dist/shared/redis-semaphore";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox, SinonSpy } from "sinon";

chai.use(chaiAsPromised);

describe("VPWorker - DataRetentionTask", () => {
    let sandbox: SinonSandbox;
    let task: DataRetentionTask;
    let fakeSequelize: any;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        fakeSequelize = Object.assign({
            query: sandbox.stub().returns({ numberofrows: 2 }),
        });

        sandbox.stub(PostgresConnector, "getConnection").callsFake(() => fakeSequelize);

        RedisConnector.connect();
        task = new DataRetentionTask("test.test");
    });

    afterEach(() => {
        sandbox?.restore();
    });

    it("should execute task and all mainteannce commands", async () => {
        task["maintenanceEnabled"] = true;
        await task["execute"]();
        sandbox.assert.callCount(fakeSequelize.query as SinonSpy, 3);
    });

    it("should execute task without mainteannce commands", async () => {
        task["maintenanceEnabled"] = false;
        await task["execute"]();
        sandbox.assert.callCount(fakeSequelize.query as SinonSpy, 1);
    });

    it("lock already exists task is not executed", async () => {
        const mutex = task["createMutex"]();
        const lockAcquired = await mutex.tryAcquire();
        try {
            expect(lockAcquired).to.equal(true);
            await task["execute"]();
            sandbox.assert.callCount(fakeSequelize.query as SinonSpy, 0);
        } finally {
            mutex.release();
        }
    });

    it("lock expired maintenance commands are skipped", async () => {
        let mutex: Mutex | undefined = undefined;

        const fakeSequelizeExpiredLock = Object.assign({
            query: sandbox.stub().callsFake(async () => {
                await new Promise((resolve) => setTimeout(resolve, 300));
                mutex = task["createMutex"](); // other process aquires lock in the meantime
                const lockAcquired = await mutex.tryAcquire();
                await new Promise((resolve) => setTimeout(resolve, 200));
                expect(lockAcquired).to.equal(true);
                return { numberofrows: 2 };
            }),
        });
        (PostgresConnector["getConnection"] as any).restore();
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() => fakeSequelizeExpiredLock);
        try {
            task["lockTimeout"] = 2 * 100;
            task["refreshInterval"] = 4 * 100;
            task["maintenanceEnabled"] = true;
            await task["execute"]();
            sandbox.assert.callCount(fakeSequelizeExpiredLock.query as SinonSpy, 1);
        } finally {
            if (mutex) {
                (mutex as any).release();
            }
        }
    });
});
