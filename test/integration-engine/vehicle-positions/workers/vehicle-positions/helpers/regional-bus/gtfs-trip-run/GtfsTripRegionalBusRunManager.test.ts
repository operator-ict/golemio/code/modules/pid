import { IRegionalBusRunInputWithMetadata } from "#ie/vehicle-positions/workers/runs/interfaces/IRegionalBusRunInputWithMetadata";
import { PositionsRepository } from "#ie/vehicle-positions/workers/vehicle-positions/data-access/PositionsRepository";
import { TripsRepository } from "#ie/vehicle-positions/workers/vehicle-positions/data-access/TripsRepository";
import { GtfsTripRegionalBusRunManager } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/regional-bus/gtfs-trip-run/GtfsTripRegionalBusRunManager";
import { IScheduleDto } from "#sch/vehicle-positions/redis/interfaces/IGtfsRunTripCacheDto";
import { PostgresConnector, RedisConnector } from "@golemio/core/dist/integration-engine/connectors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox, SinonStub } from "sinon";
import { RegionalBusEventEnum, StatePositionEnum, StateProcessEnum } from "src/const";

chai.use(chaiAsPromised);

describe("GtfsTripRegionalBusRunManager", () => {
    let sandbox: SinonSandbox;
    let tripsRepository: TripsRepository;
    let positionsRepository: PositionsRepository;
    let manager: GtfsTripRegionalBusRunManager;
    const schedule: IScheduleDto[] = [
        {
            // origin_route_id: "L341",
            trip_id: "341_371_230904",
            service_id: "1111100-2",
            direction_id: 0,
            shape_id: null,
            date: "2023-09-26",
            route_id: "L341",
            route_type: 3,
            route_short_name: "341",
            is_regional: "1",
            is_substitute_transport: "0",
            is_night: "0",
            trip_headsign: "Praha,Obchodní náměstí",
            trip_short_name: null,
            block_id: null,
            run_number: 371,
            exceptional: 0,
            min_stop_time: {
                hours: 5,
                minutes: 46,
            },
            max_stop_time: {
                hours: 6,
                minutes: 6,
            },
            first_stop_id: "U1503Z1",
            last_stop_id: "U1158Z82",
            origin_route_name: "341",
            trip_number: 1008,
            route_licence_number: 100341,
            start_timestamp: "2023-09-26T05:46:00+02:00",
            end_timestamp: "2023-09-26T06:06:00+02:00",
            requiredTurnaroundSeconds: 0,
        },
    ];

    const runInput: IRegionalBusRunInputWithMetadata = {
        runMessage: {
            external_trip_id: "867377023797370",
            cis_line_id: "100341",
            cis_trip_number: 1500,
            events: "R",
            coordinates: {
                type: "Point",
                coordinates: [14.4203, 50.1263],
            },
            vehicle_timestamp: "2023-09-26T05:50:00+02:00",
            registration_number: 1234,
            speed_kmh: 0,
            bearing: 0,
            is_terminated: false,
        },
        gtfsTripId: "341_371_230904",
        isWheelchairAccessible: true,
        agencyName: "ARRIVA City",
    };

    beforeEach(async () => {
        sandbox = sinon.createSandbox();
        tripsRepository = Object.assign({
            upsertEntity: sandbox.stub().callsFake((tripDto: IRegionalBusRunInputWithMetadata) => {
                return tripDto;
            }),
        });
        positionsRepository = Object.assign({
            createEntity: sandbox.stub().resolves(),
        });
        await PostgresConnector.connect();
        await RedisConnector.connect();
        manager = new GtfsTripRegionalBusRunManager(true);
        sandbox.stub(manager, "tripsRepository" as any).value(tripsRepository);
        sandbox.stub(manager, "positionsRepository" as any).value(positionsRepository);
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("generateDelayMsg should return 1x trip", async () => {
        const result = await manager.generateDelayMsg(schedule, runInput);

        expect(result.updatedTrips).to.have.lengthOf(1);
        expect(result.updatedTrips[0]).to.deep.equal({
            agency_name_real: "ARRIVA City",
            agency_name_scheduled: "ARRIVA City",
            gtfs_block_id: null,
            gtfs_route_id: "L341",
            gtfs_route_short_name: "341",
            gtfs_route_type: 3,
            gtfs_trip_headsign: "Praha,Obchodní náměstí",
            gtfs_trip_id: "341_371_230904",
            gtfs_date: "2023-09-26",
            gtfs_direction_id: 0,
            gtfs_shape_id: null,
            start_time: "05:46:00",
            start_timestamp: new Date("2023-09-26T03:46:00.000Z"),
            end_timestamp: new Date("2023-09-26T04:06:00.000Z"),
            id: "2023-09-26T05:46:00+02:00_341_371_230904_867377023797370",
            cis_line_id: "100341",
            cis_trip_number: 1500,
            origin_route_name: "341",
            internal_route_name: "341",
            run_number: 371,
            vehicle_registration_number: 1234,
            vehicle_type_id: 4,
            wheelchair_accessible: true,
            provider_source_type: "4",
        });

        expect((positionsRepository.createEntity as SinonStub).callCount).to.equal(1);
        expect((positionsRepository.createEntity as SinonStub).getCall(0).args[0]).to.deep.equal({
            trips_id: "2023-09-26T05:46:00+02:00_341_371_230904_867377023797370",
            bearing: 0,
            lat: 50.1263,
            lng: 14.4203,
            origin_time: "05:50:00",
            origin_timestamp: new Date("2023-09-26T03:50:00.000Z"),
            state_position: StatePositionEnum.UNKNOWN,
            state_process: StateProcessEnum.TCP_INPUT,
            speed: 0,
            is_tracked: true,
            tcp_event: RegionalBusEventEnum.DEPARTURED,
        });
    });

    it("generateDelayMsg should create new untracked position", async () => {
        const runInputTerminated = structuredClone(runInput);
        runInputTerminated.runMessage.is_terminated = true;

        const result = await manager.generateDelayMsg(schedule, runInputTerminated);
        expect((positionsRepository.createEntity as SinonStub).callCount).to.equal(1);
        expect((positionsRepository.createEntity as SinonStub).getCall(0).args[0]).to.deep.equal({
            trips_id: "2023-09-26T05:46:00+02:00_341_371_230904_867377023797370",
            bearing: 0,
            lat: 50.1263,
            lng: 14.4203,
            origin_time: "05:50:00",
            origin_timestamp: new Date("2023-09-26T03:50:00.000Z"),
            state_position: StatePositionEnum.UNKNOWN,
            state_process: StateProcessEnum.TCP_INPUT,
            speed: 0,
            is_tracked: false,
            tcp_event: RegionalBusEventEnum.DEPARTURED,
        });
    });
});
