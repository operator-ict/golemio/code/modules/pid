import { GTFSRouteTypeEnum } from "#helpers/RouteTypeEnums";
import { ITripPositionsWithGTFS } from "#ie/vehicle-positions/workers/vehicle-positions/interfaces/VPInterfaces";
import { RegionalBusEventEnum, StatePositionEnum, StateProcessEnum } from "src/const";

export const BeforeTrackBusTripWithPositionFixture: Partial<ITripPositionsWithGTFS> = {
    id: "2023-09-26T05:46:00+02:00_341_371_230904_867377023797370",
    gtfs_trip_id: "341_371_230904",
    gtfs_route_type: GTFSRouteTypeEnum.BUS,
    start_timestamp: new Date("2023-09-26T03:46:00.000Z"),
    end_timestamp: new Date("2023-09-26T04:06:00.000Z"),
    agency_name_scheduled: "ARRIVA City",
    start_cis_stop_id: null,
    positions: [
        {
            id: "20",
            bearing: 0,
            lat: 50.1263,
            lng: 14.4203,
            origin_time: "05:50:00",
            origin_timestamp: new Date("2023-09-26T03:50:00.000Z"),
            state_position: StatePositionEnum.UNKNOWN,
            state_process: StateProcessEnum.TCP_INPUT,
            is_tracked: false,
            tcp_event: RegionalBusEventEnum.DEPARTURED,
        } as any,
    ],
    gtfsData: {
        trip_id: "341_371_230904",
        stop_times: [
            {
                arrival_time_seconds: 0,
                departure_time_seconds: 0,
                stop_sequence: 1,
                stop_id: "test_id",
                shape_dist_traveled: 0,
                is_no_stop_waypoint: true,
                stop: {
                    stop_id: "test_id",
                    stop_name: "test_name",
                    stop_lat: 0,
                    stop_lon: 0,
                    zone_id: "test_zone_id",
                    wheelchair_boarding: 0,
                },
                stop_headsign: "Praha,Obchodní náměstí",
            },
        ],
        shapes_anchor_points: [
            {
                time_scheduled_seconds: 0,
            } as any,
        ],
        shapes: [],
    },
};
