import { VPContainer } from "#ie/vehicle-positions/ioc/Di";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { ValidToCalculator } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/ValidToCalculator";
import { RegionalBusPositionsManager } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/regional-bus/RegionalBusPositionsManager";
import { ITripPositionsWithGTFS } from "#ie/vehicle-positions/workers/vehicle-positions/interfaces/VPInterfaces";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";
import { StatePositionEnum, StateProcessEnum } from "src/const";
import { BeforeTrackBusTripWithPositionFixture } from "./fixtures/BeforeTrackBusTripWithPositionFixture";

chai.use(chaiAsPromised);

describe("RegionalBusPositionsManager", () => {
    const validToCalculator = VPContainer.resolve<ValidToCalculator>(VPContainerToken.ValidToCalculator);
    const regionalBusPositionsManager = new RegionalBusPositionsManager(validToCalculator);

    let sandbox: SinonSandbox;

    beforeEach(async () => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("computePositions should return before_track position with delay=0", () => {
        const tripPositions = regionalBusPositionsManager.computePositions(
            BeforeTrackBusTripWithPositionFixture as ITripPositionsWithGTFS
        );

        expect(tripPositions.positions.length).to.be.equal(1);
        expect(tripPositions.positions[0]).to.be.deep.equal({
            id: "20",
            next_stop_arrival_time: new Date("2023-09-25T22:00:00.000Z"),
            next_stop_departure_time: new Date("2023-09-25T22:00:00.000Z"),
            next_stop_id: "test_id",
            next_stop_sequence: 1,
            next_stop_name: "test_name",
            shape_dist_traveled: 0,
            state_position: StatePositionEnum.BEFORE_TRACK,
            state_process: StateProcessEnum.PROCESSED,
            last_stop_headsign: "Praha,Obchodní náměstí",
            delay: 0,
            is_tracked: false,
            valid_to: new Date("2023-09-26T03:55:00.000Z"),
        });
    });
});
