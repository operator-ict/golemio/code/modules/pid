import { RegionalBusComputeDelayHelper } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/regional-bus/compute-positions/RegionalBusComputeDelayHelper";
import { IPositionToUpdate } from "#ie/vehicle-positions/workers/vehicle-positions/interfaces/VPInterfaces";
import { IVPTripsPositionAttributes } from "#sch/vehicle-positions/models/interfaces/IVPTripsPositionAttributes";
import { IVPTripsLastPositionContext } from "#sch/vehicle-positions/models/interfaces/VPTripsLastPositionInterfaces";
import { expect } from "chai";
import { RegionalBusEventEnum, StatePositionEnum } from "src/const";
import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";

describe("RegionalBusComputeDelayHelper - Strategy BusDelayAtStop", () => {
    const routeType = GTFSRouteTypeEnum.BUS;

    it("should return updated context", () => {
        const positiontToUpdate: Partial<IPositionToUpdate> = {
            state_position: StatePositionEnum.ON_TRACK,
            last_stop_sequence: 3,
            delay: 40,
            delay_stop_arrival: 30,
            delay_stop_departure: 50,
        };

        const context: Partial<IVPTripsLastPositionContext> = {
            tripId: "2023-09-26T05:46:00+02:00_341_371_230904_867377023797370",
            lastPositionLastStop: {
                id: "U557Z2P",
                sequence: null,
                arrival_time: null,
                arrival_delay: null,
                departure_time: null,
                departure_delay: null,
            },
        };

        const resultContext = RegionalBusComputeDelayHelper.updateContext(
            context as IVPTripsLastPositionContext,
            positiontToUpdate as IPositionToUpdate,
            { is_tracked: true } as IVPTripsPositionAttributes,
            routeType
        );

        expect(resultContext).to.deep.equal({
            tripId: "2023-09-26T05:46:00+02:00_341_371_230904_867377023797370",
            lastPositionDelay: 40,
            lastPositionLastStop: {
                id: "U557Z2P",
                sequence: 3,
                arrival_time: null,
                arrival_delay: 30,
                departure_time: null,
                departure_delay: 50,
            },
        });
    });

    it("should return updated context (AFTER_TRACK_DELAYED)", () => {
        const positiontToUpdate: Partial<IPositionToUpdate> = {
            state_position: StatePositionEnum.AFTER_TRACK_DELAYED,
            last_stop_sequence: 3,
            delay: null,
            delay_stop_arrival: 30,
            delay_stop_departure: null,
        };

        const context: Partial<IVPTripsLastPositionContext> = {
            tripId: "2023-09-26T05:46:00+02:00_341_371_230904_867377023797370",
            lastPositionLastStop: {
                id: "U557Z2P",
                sequence: 2,
                arrival_time: null,
                arrival_delay: null,
                departure_time: null,
                departure_delay: null,
            },
        };

        const resultContext = RegionalBusComputeDelayHelper.updateContext(
            context as IVPTripsLastPositionContext,
            positiontToUpdate as IPositionToUpdate,
            { is_tracked: false } as IVPTripsPositionAttributes,
            routeType
        );

        expect(resultContext).to.deep.equal({
            tripId: "2023-09-26T05:46:00+02:00_341_371_230904_867377023797370",
            lastPositionDelay: null,
            lastPositionLastStop: {
                id: "U557Z2P",
                sequence: 3,
                arrival_time: null,
                arrival_delay: 30,
                departure_time: null,
                departure_delay: null,
            },
        });
    });

    it("should return updated context (AFTER_TRACK)", () => {
        const positiontToUpdate: Partial<IPositionToUpdate> = {
            state_position: StatePositionEnum.AFTER_TRACK,
            last_stop_sequence: 3,
            delay: null,
            delay_stop_arrival: 30,
            delay_stop_departure: null,
        };

        const context: Partial<IVPTripsLastPositionContext> = {
            tripId: "2023-09-26T05:46:00+02:00_341_371_230904_867377023797370",
            lastPositionLastStop: {
                id: "U557Z2P",
                sequence: 2,
                arrival_time: null,
                arrival_delay: null,
                departure_time: null,
                departure_delay: null,
            },
        };

        const resultContext = RegionalBusComputeDelayHelper.updateContext(
            context as IVPTripsLastPositionContext,
            positiontToUpdate as IPositionToUpdate,
            { is_tracked: false } as IVPTripsPositionAttributes,
            routeType
        );

        expect(resultContext).to.deep.equal({
            tripId: "2023-09-26T05:46:00+02:00_341_371_230904_867377023797370",
            lastPositionDelay: null,
            lastPositionLastStop: {
                id: "U557Z2P",
                sequence: 3,
                arrival_time: null,
                arrival_delay: 30,
                departure_time: null,
                departure_delay: null,
            },
        });
    });

    describe("updatePositionToUpdate", () => {
        it("should return updated position (AT_STOP arrival)", () => {
            const positionToUpdate: Partial<IPositionToUpdate> = {
                state_position: StatePositionEnum.AT_STOP,
                is_tracked: true,
                last_stop_sequence: 3,
                last_stop_departure_time: new Date(1675809281000),
                delay: 95,
            };

            const position: Partial<IVPTripsPositionAttributes> = {
                state_position: StatePositionEnum.AT_STOP,
                is_tracked: true,
                last_stop_sequence: 3,
                origin_timestamp: new Date(1675809165000 + positionToUpdate.delay! * 1000),
                scheduled_timestamp: new Date(1675809165000),
            };

            const context: Partial<IVPTripsLastPositionContext> = {
                tripId: "2023-09-26T05:46:00+02:00_341_371_230904_867377023797370",
                lastPositionDelay: null,
                lastPositionLastStop: {
                    id: "", // Arbitrary value
                    sequence: 2,
                    arrival_time: null,
                    arrival_delay: null,
                    departure_time: null,
                    departure_delay: null,
                },
                atStopStreak: {
                    firstPositionDelay: null,
                    firstPositionTimestamp: null,
                    stop_sequence: null,
                },
            };

            const resultPositionToUpdate = RegionalBusComputeDelayHelper.updatePositionToUpdate(
                context as IVPTripsLastPositionContext,
                positionToUpdate as IPositionToUpdate,
                position as IVPTripsPositionAttributes,
                routeType
            );

            expect(resultPositionToUpdate).to.deep.equal({
                state_position: StatePositionEnum.AT_STOP,
                is_tracked: true,
                last_stop_sequence: 3,
                last_stop_departure_time: new Date(1675809281000),
                delay_stop_arrival: 95,
                delay_stop_departure: null,
                delay: 95,
            });
        });

        it("should return updated position (AT_STOP streak)", () => {
            const positionToUpdate: Partial<IPositionToUpdate> = {
                state_position: StatePositionEnum.AT_STOP,
                is_tracked: true,
                last_stop_sequence: 3,
                last_stop_departure_time: new Date(1675809281000),
                delay: 70,
            };

            const position: Partial<IVPTripsPositionAttributes> = {
                state_position: StatePositionEnum.AT_STOP,
                is_tracked: true,
                last_stop_sequence: 3,
                scheduled_timestamp: null,
            };

            const context: Partial<IVPTripsLastPositionContext> = {
                tripId: "2023-09-26T05:46:00+02:00_341_371_230904_867377023797370",
                lastPositionDelay: null,
                lastPositionLastStop: {
                    id: "U557Z2P",
                    sequence: 3,
                    arrival_time: null,
                    arrival_delay: 95,
                    departure_time: null,
                    departure_delay: null,
                },
                atStopStreak: {
                    firstPositionDelay: null,
                    firstPositionTimestamp: null,
                    stop_sequence: 3,
                },
            };

            const resultPositionToUpdate = RegionalBusComputeDelayHelper.updatePositionToUpdate(
                context as IVPTripsLastPositionContext,
                positionToUpdate as IPositionToUpdate,
                position as IVPTripsPositionAttributes,
                routeType
            );

            expect(resultPositionToUpdate).to.deep.equal({
                state_position: StatePositionEnum.AT_STOP,
                is_tracked: true,
                last_stop_sequence: 3,
                last_stop_departure_time: new Date(1675809281000),
                delay_stop_arrival: 95,
                delay_stop_departure: null,
                delay: 70,
            });
        });

        it("should return updated position (AT_STOP departure)", () => {
            const positionToUpdate: Partial<IPositionToUpdate> = {
                state_position: StatePositionEnum.AT_STOP,
                is_tracked: true,
                last_stop_sequence: 3,
                last_stop_departure_time: new Date(1675809281000),
                delay: 65,
            };

            const position: Partial<IVPTripsPositionAttributes> = {
                state_position: StatePositionEnum.AT_STOP,
                is_tracked: true,
                tcp_event: RegionalBusEventEnum.DEPARTURED,
                last_stop_sequence: 3,
                origin_timestamp: new Date(1675809281000 + positionToUpdate.delay! * 1000),
                scheduled_timestamp: new Date(1675809281000),
            };

            const context: Partial<IVPTripsLastPositionContext> = {
                tripId: "2023-09-26T05:46:00+02:00_341_371_230904_867377023797370",
                lastPositionDelay: null,
                lastPositionLastStop: {
                    id: "U557Z2P",
                    sequence: 3,
                    arrival_time: null,
                    arrival_delay: 70,
                    departure_time: null,
                    departure_delay: null,
                },
                atStopStreak: {
                    firstPositionDelay: null,
                    firstPositionTimestamp: null,
                    stop_sequence: 3,
                },
            };

            const resultPositionToUpdate = RegionalBusComputeDelayHelper.updatePositionToUpdate(
                context as IVPTripsLastPositionContext,
                positionToUpdate as IPositionToUpdate,
                position as IVPTripsPositionAttributes,
                routeType
            );

            expect(resultPositionToUpdate).to.deep.equal({
                state_position: StatePositionEnum.AT_STOP,
                is_tracked: true,
                last_stop_sequence: 3,
                last_stop_departure_time: new Date(1675809281000),
                delay_stop_arrival: 70,
                delay_stop_departure: 65,
                delay: 65,
            });
        });

        it("should return updated position (ON_TRACK streak)", () => {
            const positionToUpdate: Partial<IPositionToUpdate> = {
                state_position: StatePositionEnum.ON_TRACK,
                is_tracked: true,
                last_stop_sequence: 3,
                last_stop_departure_time: new Date(1675809281000),
                delay: 60,
            };

            const position: Partial<IVPTripsPositionAttributes> = {
                state_position: StatePositionEnum.ON_TRACK,
                is_tracked: true,
                tcp_event: RegionalBusEventEnum.DEPARTURED,
                last_stop_sequence: 3,
                scheduled_timestamp: null,
            };

            const context: Partial<IVPTripsLastPositionContext> = {
                tripId: "2023-09-26T05:46:00+02:00_341_371_230904_867377023797370",
                lastPositionDelay: null,
                lastPositionLastStop: {
                    id: "U557Z2P",
                    sequence: 3,
                    arrival_time: null,
                    arrival_delay: 115,
                    departure_time: null,
                    departure_delay: 65,
                },
                atStopStreak: {
                    firstPositionDelay: null,
                    firstPositionTimestamp: null,
                    stop_sequence: null,
                },
            };

            const resultPositionToUpdate = RegionalBusComputeDelayHelper.updatePositionToUpdate(
                context as IVPTripsLastPositionContext,
                positionToUpdate as IPositionToUpdate,
                position as IVPTripsPositionAttributes,
                routeType
            );

            expect(resultPositionToUpdate).to.deep.equal({
                state_position: StatePositionEnum.ON_TRACK,
                is_tracked: true,
                last_stop_sequence: 3,
                last_stop_departure_time: new Date(1675809281000),
                delay_stop_arrival: 115,
                delay_stop_departure: 65,
                delay: 60,
            });
        });

        it("should return updated position (AFTER_TRACK_DELAYED arrival)", () => {
            const positionToUpdate: Partial<IPositionToUpdate> = {
                state_position: StatePositionEnum.AFTER_TRACK_DELAYED,
                last_stop_sequence: 3,
                last_stop_arrival_time: new Date(1675809281000),
                last_stop_departure_time: null,
                delay: 195,
            };

            const position: Partial<IVPTripsPositionAttributes> = {
                is_tracked: false,
                origin_timestamp: new Date(1675809281000 + 60 * 1000),
                scheduled_timestamp: new Date(1675809281000),
            };

            const context: Partial<IVPTripsLastPositionContext> = {
                tripId: "2023-09-26T05:46:00+02:00_341_371_230904_867377023797370",
                lastPositionDelay: null,
                lastPositionLastStop: {
                    id: "U557Z2P",
                    sequence: 2,
                    arrival_time: null,
                    arrival_delay: null,
                    departure_time: null,
                    departure_delay: null,
                },
                atStopStreak: {
                    firstPositionDelay: null,
                    firstPositionTimestamp: null,
                    stop_sequence: null,
                },
            };

            const resultPositionToUpdate = RegionalBusComputeDelayHelper.updatePositionToUpdate(
                context as IVPTripsLastPositionContext,
                positionToUpdate as IPositionToUpdate,
                position as IVPTripsPositionAttributes,
                routeType
            );

            expect(resultPositionToUpdate).to.deep.equal({
                state_position: StatePositionEnum.AFTER_TRACK_DELAYED,
                last_stop_sequence: 3,
                last_stop_arrival_time: new Date(1675809281000),
                last_stop_departure_time: null,
                delay_stop_arrival: 60,
                delay_stop_departure: null,
                delay: 195,
            });
        });

        it("should return updated position (AFTER_TRACK_DELAYED subsequent)", () => {
            const positionToUpdate: Partial<IPositionToUpdate> = {
                state_position: StatePositionEnum.AFTER_TRACK_DELAYED,
                last_stop_sequence: 3,
                last_stop_arrival_time: new Date(1675809281000),
                last_stop_departure_time: null,
                delay: 195,
            };

            const position: Partial<IVPTripsPositionAttributes> = {
                is_tracked: false,
                origin_timestamp: new Date(1675809281000 + 60 * 1000),
                scheduled_timestamp: new Date(1675809281000),
            };

            const context: Partial<IVPTripsLastPositionContext> = {
                tripId: "2023-09-26T05:46:00+02:00_341_371_230904_867377023797370",
                lastPositionDelay: null,
                lastPositionLastStop: {
                    id: "U557Z2P",
                    sequence: 3,
                    arrival_time: null,
                    arrival_delay: 100,
                    departure_time: null,
                    departure_delay: null,
                },
                atStopStreak: {
                    firstPositionDelay: null,
                    firstPositionTimestamp: null,
                    stop_sequence: null,
                },
            };

            const resultPositionToUpdate = RegionalBusComputeDelayHelper.updatePositionToUpdate(
                context as IVPTripsLastPositionContext,
                positionToUpdate as IPositionToUpdate,
                position as IVPTripsPositionAttributes,
                routeType
            );

            expect(resultPositionToUpdate).to.deep.equal({
                state_position: StatePositionEnum.AFTER_TRACK_DELAYED,
                last_stop_sequence: 3,
                last_stop_arrival_time: new Date(1675809281000),
                last_stop_departure_time: null,
                delay_stop_arrival: 100,
                delay_stop_departure: null,
                delay: 195,
            });
        });

        it("should return updated position (AFTER_TRACK)", () => {
            const positionToUpdate: Partial<IPositionToUpdate> = {
                state_position: StatePositionEnum.AFTER_TRACK_DELAYED,
                last_stop_sequence: 3,
                last_stop_arrival_time: new Date(1675809281000),
                last_stop_departure_time: null,
                delay: null,
            };

            const position: Partial<IVPTripsPositionAttributes> = {
                is_tracked: false,
                origin_timestamp: new Date(1675809281000 + 60 * 1000),
                scheduled_timestamp: new Date(1675809281000),
            };

            const context: Partial<IVPTripsLastPositionContext> = {
                tripId: "2023-09-26T05:46:00+02:00_341_371_230904_867377023797370",
                lastPositionDelay: null,
                lastPositionLastStop: {
                    id: "U557Z2P",
                    sequence: 3,
                    arrival_time: null,
                    arrival_delay: 100,
                    departure_time: null,
                    departure_delay: null,
                },
            };

            const resultPositionToUpdate = RegionalBusComputeDelayHelper.updatePositionToUpdate(
                context as IVPTripsLastPositionContext,
                positionToUpdate as IPositionToUpdate,
                position as IVPTripsPositionAttributes,
                routeType
            );

            expect(resultPositionToUpdate).to.deep.equal({
                state_position: StatePositionEnum.AFTER_TRACK_DELAYED,
                last_stop_sequence: 3,
                last_stop_arrival_time: new Date(1675809281000),
                last_stop_departure_time: null,
                delay_stop_arrival: 100,
                delay_stop_departure: null,
                delay: null,
            });
        });
    });
});
