import { VPContainer } from "#ie/vehicle-positions/ioc/Di";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { PositionsManager } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/PositionsManager";
import { ValidToCalculator } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/ValidToCalculator";
import {
    IPositionToUpdate,
    ITripPositionsWithGTFS,
} from "#ie/vehicle-positions/workers/vehicle-positions/interfaces/VPInterfaces";
import { IScheduleDto } from "#sch/vehicle-positions/redis/interfaces/IGtfsRunTripCacheDto";
import { PostgresConnector, RedisConnector } from "@golemio/core/dist/integration-engine/connectors";
import { expect } from "chai";
import fs from "fs";
import sinon, { SinonFakeTimers, SinonSandbox, SinonStub } from "sinon";
import { StatePositionEnum, StateProcessEnum } from "src/const";
import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";
import {
    vpComputePositionsBeforeTrackObject,
    vpComputePositionsGhostObject,
    vpComputePositionsObject,
} from "../data/vp-computepositions-object";
import { vpComputePositionsObjectDwelling } from "../data/vp-computepositions-object-dwelling";
import { vpComputePositionsObjectForBeforeTrack } from "../data/vp-computepositions-object-for-beforetrack";
import { vpComputePositionsObjectPropagateDelay } from "../data/vp-computepositions-object-propagateDelay";
import { vpComputePositionsSchedule } from "../data/vp-computepositions-schedule-4";
import { vpComputePositionsTripPositionsVarA } from "../data/vp-computepositions-tripPositions-4";
import { vpComputePositionsTripPositionsVarB } from "../data/vp-computepositions-tripPositions-4b";
import { vpComputePositionsTripPositionsVarC } from "../data/vp-computepositions-tripPositions-4c";
import { vpComputePositionsTripPositionsVarD } from "../data/vp-computepositions-tripPositions-4d";

describe("PositionsManager - ComputePositions", () => {
    const validToCalculator = VPContainer.resolve<ValidToCalculator>(VPContainerToken.ValidToCalculator);
    const positionsManager = new PositionsManager(validToCalculator);

    let sequelizeModelStub: Record<string, SinonStub>;
    let sandbox: SinonSandbox;
    let clock: SinonFakeTimers;
    let tripDPPToUpdate: any;
    let tripTwoDPPToUpdate: any;
    let tripThreeDPPToUpdate: any;
    let tripPropagateDelay: any;
    let tripBeforeTrack: any;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        clock = sinon.useFakeTimers({
            now: new Date(2021, 2, 17, 10, 0),
            shouldAdvanceTime: true,
        });
        sequelizeModelStub = Object.assign({
            belongsTo: sandbox.stub(),
            hasMany: sandbox.stub(),
            hasOne: sandbox.stub(),
            removeAttribute: sandbox.stub(),
        });
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().callsFake(() => sequelizeModelStub),
                query: sandbox.stub().callsFake(() => [true]),
                transaction: sandbox.stub().callsFake(() => Object.assign({ commit: sandbox.stub() })),
            })
        );
        sandbox.stub(RedisConnector, "getConnection");

        tripDPPToUpdate = vpComputePositionsObject;
        tripTwoDPPToUpdate = vpComputePositionsObject;
        tripThreeDPPToUpdate = vpComputePositionsObjectDwelling;
        tripPropagateDelay = vpComputePositionsObjectPropagateDelay;
        tripBeforeTrack = vpComputePositionsObjectForBeforeTrack;
    });

    afterEach(() => {
        sandbox.restore();
        clock.restore();
    });

    it("computePositions should filter DPP and set proper next stop proper for before_track", async () => {
        const computedPositions = await positionsManager["computePositions"](tripDPPToUpdate, [
            {
                date: "2021-05-13",
                start_timestamp: "2021-05-13T09:41:00+02:00",
            },
        ] as any);

        expect(computedPositions.positions[0]?.state_position).to.equal(StatePositionEnum.INVISIBLE);
        expect(computedPositions.positions[1]?.state_position).to.equal(StatePositionEnum.BEFORE_TRACK);

        expect(computedPositions.positions[1]?.last_stop_sequence).to.equal(undefined);
        expect(computedPositions.positions[1]?.next_stop_sequence).to.equal(1);
        expect(computedPositions.positions[1]?.next_stop_id).to.equal("U1102Z2P");
        expect(computedPositions.positions[1]?.next_stop_name).to.equal("První");
    });

    it("computePositions proper on_track position", async () => {
        const computedPositions = await positionsManager["computePositions"](tripDPPToUpdate, undefined);

        expect(computedPositions.positions[2]?.state_position).to.equal(StatePositionEnum.ON_TRACK);
        expect(computedPositions.positions[2]?.shape_dist_traveled).to.equal(13.5);
        expect(computedPositions.positions[2]?.bearing).to.equal(245);
        expect(computedPositions.positions[2]?.delay).to.equal(181);
    });

    it("computePositions for on_track position should respect the original bearing when available", async () => {
        const tripPositions: any = structuredClone(tripDPPToUpdate);
        tripPositions.positions[2].bearing = 198;
        const computedPositions = await positionsManager["computePositions"](tripPositions, undefined);

        expect(computedPositions.positions[2]?.state_position).to.equal(StatePositionEnum.ON_TRACK);
        expect(computedPositions.positions[2]?.shape_dist_traveled).to.equal(13.5);
        expect(computedPositions.positions[2]?.bearing).to.equal(198);
        expect(computedPositions.positions[2]?.delay).to.equal(181);
    });

    it("computePositions proper off_track position while preserving last stop information", async () => {
        const computedPositions = await positionsManager["computePositions"](tripDPPToUpdate, undefined);
        expect(computedPositions.positions[3]?.state_position).to.equal(StatePositionEnum.OFF_TRACK);
        expect(computedPositions.positions[3]?.last_stop_id).to.equal("U69Z2P");
        expect(computedPositions.positions[3]?.last_stop_sequence).to.equal(22);
        expect(computedPositions.positions[3]?.last_stop_arrival_time).to.deep.equal(new Date(1620893640000));
        expect(computedPositions.positions[3]?.last_stop_departure_time).to.deep.equal(new Date(1620893640000));
    });

    it("computePositions detect tracking 0 duplicate", async () => {
        const computedPositions = await positionsManager["computePositions"](tripDPPToUpdate, undefined);

        expect(computedPositions.positions[4]?.state_position).to.equal(StatePositionEnum.DUPLICATE);
    });

    it("computePositions proper on_track even at stop due to tcp_event position", async () => {
        const computedPositions = await positionsManager["computePositions"](tripDPPToUpdate, undefined);

        expect(computedPositions.positions[5]?.state_position).to.equal(StatePositionEnum.ON_TRACK);
    });

    it("computePositions proper after_track_delayed position", async () => {
        const computedPositions = await positionsManager["computePositions"](tripDPPToUpdate, undefined);

        expect(computedPositions.positions[6]?.state_position).to.equal(StatePositionEnum.AFTER_TRACK_DELAYED);
    });

    it("computePositions proper invisible position, should not be assigned as last position to trip", async () => {
        const tripPositions = {
            ...tripDPPToUpdate,
            positions: tripDPPToUpdate.positions.slice(0, 8),
        };
        tripPositions.positions[6].is_tracked = true;
        //take only 8 positions, last should be invisible
        const computedPositions = await positionsManager["computePositions"](tripPositions, [
            {
                start_timestamp: "2021-05-13T07:41:00Z",
                date: "2021-05-13",
                trip_id: "136_860_210418",
            } as any,
            {
                start_timestamp: "2021-05-13T20:41:00Z",
                date: "2021-05-13",
                trip_id: "136_860_210419",
            } as any,
        ]);

        expect(computedPositions.positions[7]?.state_position).to.equal(StatePositionEnum.INVISIBLE);
        expect(computedPositions.context.lastPositionId).to.equal(computedPositions.positions[6]?.id);
    });

    // eslint-disable-next-line max-len
    it("computePositions should move position to the next stop, while keeping the original shape_dist_traveled and bearing", async () => {
        const computedPositions = await positionsManager["computePositions"](tripDPPToUpdate, undefined);

        expect(computedPositions.positions[8]?.state_position).to.equal(StatePositionEnum.AT_STOP);
        expect(computedPositions.positions[8]?.this_stop_sequence).to.equal(14);
        expect(computedPositions.positions[8]?.shape_dist_traveled).to.equal(6.6);
        expect(computedPositions.positions[8]?.bearing).to.equal(231);
    });

    it("computePositions should move position to AFTER_TRACK_DELAYED", async () => {
        const computedPositions = await positionsManager["computePositions"](tripDPPToUpdate, undefined);

        expect(computedPositions.positions[9]?.state_position).to.equal(StatePositionEnum.AFTER_TRACK_DELAYED);
    });

    it("computePositions should return last position as canceled", async () => {
        const computedPositions = await positionsManager["computePositions"](tripDPPToUpdate, undefined);

        expect(computedPositions.context.lastPositionCanceled).to.be.true;
    });

    it("computePositions should process canceled position without coords", async () => {
        const tripData = structuredClone(tripDPPToUpdate);
        const canceledPosition = tripData.positions.at(-2);
        expect(canceledPosition).to.not.be.undefined;
        expect(canceledPosition.is_canceled).to.be.true;
        canceledPosition.lat = null;
        canceledPosition.lng = null;

        const computedPositions = await positionsManager["computePositions"](tripData, undefined);

        expect(computedPositions.context.lastPositionCanceled).to.be.true;
        const computedCanceledPosition = computedPositions.positions.at(-2);
        expect(computedCanceledPosition).to.not.be.undefined;
        expect(computedCanceledPosition!.state_position).to.equal(StatePositionEnum.CANCELED);
        expect(computedCanceledPosition!.state_process).to.equal(StateProcessEnum.PROCESSED);
        expect(computedCanceledPosition!.lat).to.be.a("number").and.to.equal(tripData.positions.at(-3).lat);
        expect(computedCanceledPosition!.lng).to.be.a("number").and.to.equal(tripData.positions.at(-3).lng);
    });

    it("computePositions should set canceled position's coords to the null island", async () => {
        const tripData = structuredClone(tripDPPToUpdate);
        const canceledPosition = tripData.positions.at(-2);
        expect(canceledPosition).to.not.be.undefined;
        expect(canceledPosition.is_canceled).to.be.true;
        canceledPosition.lat = null;
        canceledPosition.lng = null;
        tripData.positions = [canceledPosition];

        const computedPositions = await positionsManager["computePositions"](tripData, undefined);

        expect(computedPositions.positions)
            .to.be.an("array")
            .that.deep.equal([
                {
                    id: 1425200451,
                    lat: 0,
                    lng: 0,
                    state_position: "canceled",
                    state_process: "processed",
                    is_tracked: false,
                    valid_to: new Date("2021-05-13T14:18:38.000Z"),
                },
            ]);
    });

    it("computePositions should not set reference to duplicated position", async () => {
        const computedPositions = await positionsManager["computePositions"](tripDPPToUpdate, undefined);

        expect(computedPositions.positions[11]?.state_position).to.equal(StatePositionEnum.DUPLICATE);
        expect(computedPositions.context.lastPositionId).to.equal(computedPositions.positions[10]?.id);
    });

    it("computePositions should handle trip DELAYED at stop", async () => {
        tripTwoDPPToUpdate.positions = [
            {
                lng: 14.5137701,
                lat: 50.0250101,
                origin_time: "10:49:10",
                origin_timestamp: new Date(1620895750000),
                delay: null,
                is_tracked: true,
                id: 1,
                shape_dist_traveled: null,
                is_canceled: false,
            },
            {
                lng: 14.5151301,
                lat: 50.0252101,
                origin_time: "10:49:15",
                origin_timestamp: new Date(1620895755000),
                delay: null,
                is_tracked: true,
                id: 2,
                shape_dist_traveled: null,
                is_canceled: false,
            },
            {
                lng: 14.5151302,
                lat: 50.0252102,
                origin_time: "10:50:15",
                origin_timestamp: new Date(1620895815000),
                delay: null,
                is_tracked: true,
                id: 3,
                shape_dist_traveled: null,
                is_canceled: false,
            },
        ];
        const computedPositions = await positionsManager["computePositions"](tripTwoDPPToUpdate, undefined);
        expect(computedPositions.positions[0]?.delay).to.equal(31);
        expect(computedPositions.positions[1]?.delay).to.equal(0);
        expect(computedPositions.positions[2]?.delay).to.equal(35);
    });

    it("computePositions should handle trip AHEAD at stop", async () => {
        tripTwoDPPToUpdate.positions = [
            {
                lng: 14.5137701,
                lat: 50.0250101,
                origin_time: "10:38:09",
                origin_timestamp: new Date(1620895089000),
                delay: null,
                is_tracked: true,
                id: 1,
                shape_dist_traveled: null,
                is_canceled: false,
            },
            {
                lng: 14.5151301,
                lat: 50.0252101,
                origin_time: "10:39:00",
                origin_timestamp: new Date(1620895140000),
                delay: null,
                is_tracked: true,
                id: 2,
                shape_dist_traveled: null,
                is_canceled: false,
            },
            {
                lng: 14.5151302,
                lat: 50.0252102,
                origin_time: "10:40:40",
                origin_timestamp: new Date(1620895240000),
                delay: null,
                is_tracked: true,
                id: 3,
                shape_dist_traveled: null,
                is_canceled: false,
            },
        ];
        const computedPositions = await positionsManager["computePositions"](tripTwoDPPToUpdate, undefined);
        expect(computedPositions.positions[0]?.delay).to.equal(-630);
        expect(computedPositions.positions[1]?.delay).to.equal(-600);
        expect(computedPositions.positions[2]?.delay).to.equal(-540);
    });

    it("computePositions should handle trip AHEAD dwelling longer but still AHEAD", async () => {
        tripThreeDPPToUpdate.positions = [
            {
                lng: 14.5225201,
                lat: 50.1563601,
                origin_time: "09:31:42",
                origin_timestamp: new Date(1620891102000),
                delay: null,
                is_tracked: true,
                id: 1,
                shape_dist_traveled: null,
                is_canceled: false,
            },
            {
                lng: 14.5224001,
                lat: 50.1554601,
                origin_time: "09:32:00",
                origin_timestamp: new Date(1620891120000),
                delay: null,
                is_tracked: true,
                id: 2,
                shape_dist_traveled: null,
                is_canceled: false,
            },
            {
                lng: 14.5224002,
                lat: 50.1554602,
                origin_time: "09:40:00",
                origin_timestamp: new Date(1620891600000),
                delay: null,
                is_tracked: true,
                id: 3,
                shape_dist_traveled: null,
                is_canceled: false,
            },
            {
                lng: 14.5224003,
                lat: 50.1554603,
                origin_time: "09:43:00",
                origin_timestamp: new Date(1620891780000),
                delay: null,
                is_tracked: true,
                id: 4,
                shape_dist_traveled: null,
                is_canceled: false,
            },
        ];
        const computedPositions = await positionsManager["computePositions"](tripThreeDPPToUpdate, undefined);
        expect(computedPositions.positions[0]?.delay).to.equal(-600);
        expect(computedPositions.positions[1]?.delay).to.equal(-600);
        expect(computedPositions.positions[2]?.delay).to.equal(-600);
        expect(computedPositions.positions[3]?.delay).to.equal(-540);
    });

    it("computePositions should handle trip AHEAD dwelling too much to be DELAYED", async () => {
        tripThreeDPPToUpdate.positions = [
            {
                lng: 14.5225201,
                lat: 50.1563601,
                origin_time: "09:40:42",
                origin_timestamp: new Date(1620891642000),
                delay: null,
                is_tracked: true,
                id: 1,
                shape_dist_traveled: null,
                is_canceled: false,
            },
            {
                lng: 14.5224001,
                lat: 50.1554601,
                origin_time: "09:41:00",
                origin_timestamp: new Date(1620891660000),
                delay: null,
                is_tracked: true,
                id: 2,
                shape_dist_traveled: null,
                is_canceled: false,
            },
            {
                lng: 14.5224002,
                lat: 50.1554602,
                origin_time: "09:51:00",
                origin_timestamp: new Date(1620892260000),
                delay: null,
                is_tracked: true,
                id: 3,
                shape_dist_traveled: null,
                is_canceled: false,
            },
            {
                lng: 14.5224003,
                lat: 50.1554603,
                origin_time: "09:52:00",
                origin_timestamp: new Date(1620892320000),
                delay: null,
                is_tracked: true,
                id: 4,
                shape_dist_traveled: null,
                is_canceled: false,
            },
            {
                lng: 14.5224004,
                lat: 50.1554604,
                origin_time: "09:53:00",
                origin_timestamp: new Date(1620892380000),
                delay: null,
                is_tracked: true,
                id: 5,
                shape_dist_traveled: null,
                is_canceled: false,
            },
        ];
        const computedPositions = await positionsManager["computePositions"](tripThreeDPPToUpdate, undefined);
        expect(computedPositions.positions[0]?.delay).to.equal(-60);
        expect(computedPositions.positions[1]?.delay).to.equal(-60);
        expect(computedPositions.positions[2]?.delay).to.equal(-60);
        expect(computedPositions.positions[3]?.delay).to.equal(0);
        expect(computedPositions.positions[4]?.delay).to.equal(60);
    });

    it("computePositions should handle trip DELAYED properly at stop", async () => {
        tripThreeDPPToUpdate.positions = [
            {
                lng: 14.5225201,
                lat: 50.1563601,
                origin_time: "09:42:42",
                origin_timestamp: new Date(1620891762000),
                delay: null,
                is_tracked: true,
                id: 1,
                shape_dist_traveled: null,
                is_canceled: false,
            },
            {
                lng: 14.5224001,
                lat: 50.1554601,
                origin_time: "09:43:00",
                origin_timestamp: new Date(1620891780000),
                delay: null,
                is_tracked: true,
                id: 2,
                shape_dist_traveled: null,
                is_canceled: false,
            },
            {
                lng: 14.5224002,
                lat: 50.1554602,
                origin_time: "09:51:00",
                origin_timestamp: new Date(1620892260000),
                delay: null,
                is_tracked: true,
                id: 3,
                shape_dist_traveled: null,
                is_canceled: false,
            },
            {
                lng: 14.5224003,
                lat: 50.1554603,
                origin_time: "09:53:00",
                origin_timestamp: new Date(1620892380000),
                delay: null,
                is_tracked: true,
                id: 4,
                shape_dist_traveled: null,
                is_canceled: false,
            },
        ];
        const computedPositions = await positionsManager["computePositions"](tripThreeDPPToUpdate, undefined);
        expect(computedPositions.positions[0]?.delay).to.equal(+60);
        expect(computedPositions.positions[1]?.delay).to.equal(+0);
        expect(computedPositions.positions[2]?.delay).to.equal(+0);
        expect(computedPositions.positions[3]?.delay).to.equal(+60);
    });

    it("computePositions should handle trip AHEAD dwelling too much to be DELAYED, comptedBefore!", async () => {
        tripThreeDPPToUpdate.positions = [
            {
                lng: 14.5225201,
                lat: 50.1563601,
                origin_time: "09:40:42",
                origin_timestamp: new Date(1620891642000),
                is_tracked: true,
                id: 1,
                shape_dist_traveled: null,
                is_canceled: false,
                //computed
                state_position: StatePositionEnum.ON_TRACK,
                state_process: StateProcessEnum.PROCESSED,
                this_stop_sequence: null,
                delay: -60,
            },
            {
                lng: 14.5224001,
                lat: 50.1554601,
                origin_time: "09:41:00",
                origin_timestamp: new Date(1620891660000),
                is_tracked: true,
                id: 2,
                shape_dist_traveled: null,
                is_canceled: false,
                //computed
                state_position: StatePositionEnum.AT_STOP,
                state_process: StateProcessEnum.PROCESSED,
                this_stop_sequence: 2,
                delay: -60,
            },
            {
                lng: 14.5224002,
                lat: 50.1554602,
                origin_time: "09:51:00",
                origin_timestamp: new Date(1620892260000),
                delay: null,
                is_tracked: true,
                id: 3,
                shape_dist_traveled: null,
                is_canceled: false,
            },
            {
                lng: 14.5224003,
                lat: 50.1554603,
                origin_time: "09:52:00",
                origin_timestamp: new Date(1620892320000),
                delay: null,
                is_tracked: true,
                id: 4,
                shape_dist_traveled: null,
                is_canceled: false,
            },
            {
                lng: 14.5224004,
                lat: 50.1554604,
                origin_time: "09:53:00",
                origin_timestamp: new Date(1620892380000),
                delay: null,
                is_tracked: true,
                id: 5,
                shape_dist_traveled: null,
                is_canceled: false,
            },
        ];
        const computedPositions = await positionsManager["computePositions"](tripThreeDPPToUpdate, undefined);
        expect(computedPositions.positions.length).to.equal(5);
        expect(computedPositions.positions[0]).to.equal(null);
        expect(computedPositions.positions[1]).to.equal(null);
        expect(computedPositions.positions[2]?.delay).to.equal(-60);
        expect(computedPositions.positions[3]?.delay).to.equal(0);
        expect(computedPositions.positions[4]?.delay).to.equal(60);
    });

    it("updatePositionsIterator ~ should propagate delay", () => {
        const tripPositions = tripPropagateDelay as any;
        const gtfsType = tripPositions.gtfs_route_type as GTFSRouteTypeEnum;
        const startTimestamp = tripPropagateDelay.start_timestamp.getTime();
        const startDayTimestamp = positionsManager["getStartDayTimestamp"](
            startTimestamp,
            tripPropagateDelay.gtfsData.shapes_anchor_points[0].time_scheduled_seconds
        );
        const context = {
            atStopStreak: {},
            lastPositionLastStop: {},
            lastPositionDelay: null,
            lastPositionId: null,
            lastPositionOriginTimestamp: null,
            lastPositionTracking: null,
            lastPositionCanceled: null,
            lastPositionBeforeTrackDelayed: {
                delay: 1000,
                origin_timestamp: 1620883170000,
            },
            lastPositionState: null,
            tripId: tripPropagateDelay.id,
        };
        const computedPositions: IPositionToUpdate[] = [];

        const result = positionsManager.updatePositions(
            {
                tripPositions,
                startTimestamp,
                startDayTimestamp,
                endTimestamp: null,
                context,
                computedPositions,
                gtfsType,
            } as any,
            {
                date: "2021-05-13",
                start_timestamp: "2021-05-13T09:41:00+02:00",
            } as any
        );
        expect(result.positions.length).to.equal(2);
        expect(result.positions.map((p) => p?.delay)).to.satisfy((delays: number[]) =>
            delays.every((d) => typeof d === "number")
        );
    });

    it("updatePositionsIterator ~ should propagate delay to before_track (null context)", () => {
        const tripPositions = tripBeforeTrack;
        const gtfsType = tripPositions.gtfs_route_type as GTFSRouteTypeEnum;
        const startTimestamp = tripPositions.start_timestamp.getTime();
        const startDayTimestamp = positionsManager["getStartDayTimestamp"](
            startTimestamp,
            tripPositions.gtfsData.shapes_anchor_points[0].time_scheduled_seconds
        );
        const context = {
            atStopStreak: {},
            lastPositionLastStop: {},
            lastPositionDelay: null,
            lastPositionId: null,
            lastPositionOriginTimestamp: null,
            lastPositionTracking: null,
            lastPositionCanceled: null,
            lastPositionBeforeTrackDelayed: null,
            lastPositionState: null,
            tripId: tripPositions.id,
        };
        const computedPositions: IPositionToUpdate[] = [];

        const result = positionsManager.updatePositions(
            {
                tripPositions,
                startTimestamp,
                startDayTimestamp,
                endTimestamp: null,
                context,
                computedPositions,
                gtfsType,
            } as any,
            undefined
        );

        const beforeTrackPositions = result.positions.filter((p) => p?.state_position === StatePositionEnum.BEFORE_TRACK);
        const afterTrackDelayedPositions = result.positions.filter(
            (p) => p?.state_position === StatePositionEnum.AFTER_TRACK_DELAYED
        );

        expect(beforeTrackPositions.length).to.equal(2);
        expect(afterTrackDelayedPositions.length).to.equal(1);

        // delay
        expect(beforeTrackPositions.map((p) => p?.delay)).to.satisfy((delays: number[]) =>
            delays.every((d) => typeof d === "number")
        );
        expect(afterTrackDelayedPositions.map((p) => p?.delay)).to.satisfy((delays: number[]) =>
            delays.every((d) => typeof d === "number")
        );

        // next stop id
        expect(beforeTrackPositions.map((p) => p?.next_stop_id)).to.satisfy((seqArr: number[]) =>
            seqArr.every((d) => typeof d === "string")
        );
        expect(afterTrackDelayedPositions.map((p) => p?.next_stop_id)).to.satisfy((seqArr: number[]) =>
            seqArr.every((d) => d === null)
        );

        // next stop sequence
        expect(beforeTrackPositions.map((p) => p?.next_stop_sequence)).to.satisfy((seqArr: number[]) =>
            seqArr.every((d) => typeof d === "number")
        );
        expect(afterTrackDelayedPositions.map((p) => p?.next_stop_sequence)).to.satisfy((seqArr: number[]) =>
            seqArr.every((d) => d === null)
        );

        // next stop id
        expect(beforeTrackPositions.map((p) => p?.next_stop_name)).to.satisfy((names: number[]) =>
            names.every((d) => typeof d === "string")
        );
        expect(afterTrackDelayedPositions.map((p) => p?.next_stop_name)).to.satisfy((names: number[]) =>
            names.every((d) => d === null)
        );
    });

    it("updatePositionsIterator ~ should propagate delay to before_track (null tripPositions delay)", () => {
        const tripPositions = structuredClone(tripBeforeTrack);
        for (const position of tripPositions.positions) {
            position.delay = null;
        }
        const gtfsType = tripPositions.gtfs_route_type as GTFSRouteTypeEnum;
        const startTimestamp = tripPositions.start_timestamp.getTime();
        const startDayTimestamp = positionsManager["getStartDayTimestamp"](
            startTimestamp,
            tripPositions.gtfsData.shapes_anchor_points[0].time_scheduled_seconds
        );
        const context = {
            atStopStreak: {},
            lastPositionLastStop: {},
            lastPositionDelay: null,
            lastPositionId: null,
            lastPositionOriginTimestamp: null,
            lastPositionTracking: null,
            lastPositionCanceled: null,
            lastPositionBeforeTrackDelayed: {
                delay: 87,
                origin_timestamp: "2023-10-04T09:34:27.000Z",
            },
            lastPositionState: null,
            tripId: tripPositions.id,
        };
        const computedPositions: IPositionToUpdate[] = [];

        const result = positionsManager.updatePositions(
            {
                tripPositions,
                startTimestamp,
                startDayTimestamp,
                endTimestamp: null,
                context,
                computedPositions,
                gtfsType,
            } as any,
            undefined
        );

        const beforeTrackPositions = result.positions.filter((p) => p?.state_position === StatePositionEnum.BEFORE_TRACK);
        const afterTrackDelayedPositions = result.positions.filter(
            (p) => p?.state_position === StatePositionEnum.AFTER_TRACK_DELAYED
        );

        expect(beforeTrackPositions.length).to.equal(2);
        expect(afterTrackDelayedPositions.length).to.equal(1);

        // delay
        expect(beforeTrackPositions.map((p) => p?.delay)).to.satisfy((delays: number[]) =>
            delays.every((d) => typeof d === "number")
        );
        expect(afterTrackDelayedPositions.map((p) => p?.delay)).to.satisfy((delays: number[]) =>
            delays.every((d) => typeof d === "number")
        );

        // next stop id
        expect(beforeTrackPositions.map((p) => p?.next_stop_id)).to.satisfy((seqArr: number[]) =>
            seqArr.every((d) => typeof d === "string")
        );
        expect(afterTrackDelayedPositions.map((p) => p?.next_stop_id)).to.satisfy((seqArr: number[]) =>
            seqArr.every((d) => d === null)
        );

        // next stop sequence
        expect(beforeTrackPositions.map((p) => p?.next_stop_sequence)).to.satisfy((seqArr: number[]) =>
            seqArr.every((d) => typeof d === "number")
        );
        expect(afterTrackDelayedPositions.map((p) => p?.next_stop_sequence)).to.satisfy((seqArr: number[]) =>
            seqArr.every((d) => d === null)
        );

        // next stop id
        expect(beforeTrackPositions.map((p) => p?.next_stop_name)).to.satisfy((names: number[]) =>
            names.every((d) => typeof d === "string")
        );
        expect(afterTrackDelayedPositions.map((p) => p?.next_stop_name)).to.satisfy((names: number[]) =>
            names.every((d) => d === null)
        );
    });

    it("computePositions should add attributes delay attributes ~ TRAM", async () => {
        const schedule = vpComputePositionsSchedule as IScheduleDto[];
        const tripPositions = vpComputePositionsTripPositionsVarA as unknown as ITripPositionsWithGTFS;

        const result = await positionsManager.computePositions(tripPositions, schedule);
        const mappedPositions = result.positions.map((el, index) => {
            return {
                state: el?.state_position,
                delayStopArrival: el?.delay_stop_arrival,
                delayStopDeparture: el?.delay_stop_departure,
                nextStopId: el?.next_stop_id,
                tcpEvent: tripPositions.positions[index].tcp_event,
                lastStopSequence: el?.last_stop_sequence,
                statePosition: el?.state_position,
            };
        });

        expect(mappedPositions[0].statePosition).equal(StatePositionEnum.BEFORE_TRACK);

        expect(mappedPositions[2].delayStopArrival).equal(-10);
        expect(mappedPositions[2].delayStopDeparture).equal(undefined);
        expect(mappedPositions[2].lastStopSequence).equal(3);
        expect(mappedPositions[2].statePosition).equal(StatePositionEnum.AT_STOP);

        const departureU34Z1P = mappedPositions[3];
        expect(departureU34Z1P.delayStopArrival).equal(-10);
        expect(departureU34Z1P.delayStopDeparture).equal(19);
        const departureU1019Z2P = mappedPositions[55];
        expect(departureU1019Z2P.delayStopArrival).equal(67);
        expect(departureU1019Z2P.delayStopDeparture).equal(114);
        const departureU49Z4P = mappedPositions[46];
        const departureU49Z4Pb = mappedPositions[47];
        expect(departureU49Z4P.delayStopArrival).equal(departureU49Z4Pb.delayStopArrival);
        expect(departureU49Z4P.delayStopDeparture).equal(departureU49Z4Pb.delayStopDeparture);
        expect(mappedPositions[56].delayStopArrival).to.be.equal(90);
        expect(mappedPositions.slice(57, 61).every((el) => el.delayStopArrival === 90)).to.be.true;
        expect(mappedPositions.slice(57, 61).every((el) => el.delayStopDeparture === undefined)).to.be.true;
    });

    it("computePositions should add attributes delay attributes ~ TRAM B", async () => {
        const schedule = vpComputePositionsSchedule as IScheduleDto[];
        const tripPositions = vpComputePositionsTripPositionsVarB as unknown as ITripPositionsWithGTFS;

        const result = await positionsManager.computePositions(tripPositions, schedule);
        const mappedPositions = result.positions
            .filter((position) => position !== null)
            .map((el, index) => {
                return {
                    state: el?.state_position,
                    delayStopArrival: el?.delay_stop_arrival,
                    delayStopDeparture: el?.delay_stop_departure,
                    nextStopId: el?.next_stop_id,
                    tcpEvent: tripPositions.positions[index].tcp_event,
                };
            });
        expect(mappedPositions[0].delayStopArrival).equal(67);
    });

    it("computePositions should add attributes delay attributes ~ TRAM C", async () => {
        const schedule = vpComputePositionsSchedule as IScheduleDto[];
        const tripPositions = vpComputePositionsTripPositionsVarC as unknown as ITripPositionsWithGTFS;

        const result = await positionsManager.computePositions(tripPositions, schedule);
        const mappedPositions = result.positions
            .filter((position) => position !== null)
            .map((el, index) => {
                return {
                    state: el?.state_position,
                    delayStopArrival: el?.delay_stop_arrival,
                    delayStopDeparture: el?.delay_stop_departure,
                    nextStopId: el?.next_stop_id,
                    tcpEvent: tripPositions.positions[index].tcp_event,
                };
            });
        expect(mappedPositions[0].delayStopArrival).to.be.undefined; // after track
    });

    // TODO Temporarily skip tests due to test data containg mixture of unprocessed and processed positions
    //  making it impossible to test the actual processing right now
    it.skip("computePositions should add attributes delay attributes ~ TRAM D", async () => {
        const schedule = vpComputePositionsSchedule as IScheduleDto[];
        const tripPositions = vpComputePositionsTripPositionsVarD as unknown as ITripPositionsWithGTFS;

        const result = await positionsManager.computePositions(tripPositions, schedule);
        const mappedPositions = result.positions
            .filter((position) => position !== null)
            .map((el, index) => {
                return {
                    id: el?.id,
                    state: el?.state_position,
                    delayStopArrival: el?.delay_stop_arrival,
                    delayStopDeparture: el?.delay_stop_departure,
                    nextStopId: el?.next_stop_id,
                    tcpEvent: tripPositions.positions[index].tcp_event,
                };
            });
        expect(mappedPositions[0].delayStopArrival).equal(74);
        expect(mappedPositions[0].delayStopDeparture).equal(95);
    });

    // TODO Temporarily skip tests for delay at stop calculations for busses
    // https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/168#note_1115019230
    it.skip("computePositions should add attributes delay attributes ~ BUS", async () => {
        const schedule: IScheduleDto[] = JSON.parse(
            fs.readFileSync(__dirname + "/../data/vp-computepositions-schedule-188.json").toString()
        );
        const tripPositions: ITripPositionsWithGTFS = JSON.parse(
            fs.readFileSync(__dirname + "/../data/vp-computepositions-tripPositions-188.json").toString()
        );

        const result = await positionsManager.computePositions(tripPositions, schedule);
        const mappedPositions = result.positions.map((el) => {
            return {
                state: el?.state_position,
                delayStopArrival: el?.delay_stop_arrival,
                delayStopDeparture: el?.delay_stop_departure,
                nextStopId: el?.next_stop_id,
            };
        });

        // this is artificially added situation when BUS sends twice position from same stop
        expect(mappedPositions[5].delayStopArrival).to.equal(73);
        expect(mappedPositions[5].delayStopDeparture).to.equal(undefined);
        expect(mappedPositions[6].delayStopArrival).to.equal(73);
        expect(mappedPositions[6].delayStopDeparture).to.equal(undefined);
        const afterDepartureU418Z1P = mappedPositions[7];
        expect(afterDepartureU418Z1P.delayStopArrival).to.equal(73);
        expect(afterDepartureU418Z1P.delayStopDeparture).to.equal(79);
        const afterDepartureU201Z1P = mappedPositions[34];
        expect(afterDepartureU201Z1P.delayStopArrival).to.equal(221);
        expect(afterDepartureU201Z1P.delayStopDeparture).to.equal(221);
        expect(mappedPositions.slice(38, 43).every((el) => el.delayStopArrival === 371)).to.be.true;
        expect(mappedPositions.slice(38, 43).every((el) => el.delayStopDeparture === undefined)).to.be.true;
    });

    it.skip("computePositions should add attributes delay attributes ~ BUS B", async () => {
        const schedule: IScheduleDto[] = JSON.parse(
            fs.readFileSync(__dirname + "/../data/vp-computepositions-schedule-188.json").toString()
        );
        const tripPositions: ITripPositionsWithGTFS = JSON.parse(
            fs.readFileSync(__dirname + "/../data/vp-computepositions-tripPositions-188b.json").toString()
        );

        const result = await positionsManager.computePositions(tripPositions, schedule);
        const mappedPositions = result.positions.map((el) => {
            return {
                state: el?.state_position,
                delayStopArrival: el?.delay_stop_arrival,
                delayStopDeparture: el?.delay_stop_departure,
                nextStopId: el?.next_stop_id,
            };
        });

        expect(mappedPositions[0].delayStopArrival).to.equal(221);
    });

    it.skip("computePositions should add attributes delay attributes ~ BUS C", async () => {
        const schedule: IScheduleDto[] = JSON.parse(
            fs.readFileSync(__dirname + "/../data/vp-computepositions-schedule-188.json").toString()
        );
        const tripPositionsc: ITripPositionsWithGTFS = JSON.parse(
            fs.readFileSync(__dirname + "/../data/vp-computepositions-tripPositions-188c.json").toString()
        );

        const result = await positionsManager.computePositions(tripPositionsc, schedule);
        const mappedPositions = result.positions.map((el) => {
            return {
                state: el?.state_position,
                delayStopArrival: el?.delay_stop_arrival,
                delayStopDeparture: el?.delay_stop_departure,
                nextStopId: el?.next_stop_id,
            };
        });

        expect(mappedPositions[0].delayStopArrival).to.equal(371);
    });

    it("computePositions should handle positions in garage as after track even after archiving", async () => {
        const computedPositions = await positionsManager.computePositions(vpComputePositionsGhostObject as any, undefined);

        expect(computedPositions.positions[0]?.state_position).to.equal(StatePositionEnum.AFTER_TRACK);
    });

    it("computePositions should handle positions with scheduled timestamp as before track", async () => {
        const computedPositions = await positionsManager.computePositions(vpComputePositionsBeforeTrackObject as any, undefined);

        expect(computedPositions.positions[0]?.state_position).to.equal(StatePositionEnum.BEFORE_TRACK);
    });
});
