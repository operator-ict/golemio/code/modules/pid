import { GTFSRouteTypeEnum } from "#helpers/RouteTypeEnums";
import { VPContainer } from "#ie/vehicle-positions/ioc/Di";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { PositionsManager } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/PositionsManager";
import { ValidToCalculator } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/ValidToCalculator";
import { ITripPositionsWithGTFS } from "#ie/vehicle-positions/workers/vehicle-positions/interfaces/VPInterfaces";
import { IVPTripsPositionAttributes } from "#sch/vehicle-positions/models/interfaces/IVPTripsPositionAttributes";
import { expect } from "chai";
import { DelayComputationFixture } from "./fixtures/bus-315_313_230904-switchback/DelayComputationObjectFixture";
import { PositionsFixture } from "./fixtures/bus-315_313_230904-switchback/PositionsFixture";

describe("positionsManager - Switchbacks/ZigZags (zavleky/uvrate)", () => {
    const validToCalculator = VPContainer.resolve<ValidToCalculator>(VPContainerToken.ValidToCalculator);
    const positionsManager = new PositionsManager(validToCalculator);

    describe("Bus 315 -> Praha,Cerny Most", () => {
        const MLADA_BOLESLAV_AUT_ST_STOP_SEQUENCE = 16;

        it("should compute positions going in and out of Mlada Boleslav,aut.st.", async () => {
            const tripPositions: ITripPositionsWithGTFS = {
                gtfsData: DelayComputationFixture as any,
                id: "2024-01-04T12:30:00Z_100315_315_1046",
                gtfs_trip_id: "315_313_230904",
                gtfs_route_type: GTFSRouteTypeEnum.BUS,
                start_timestamp: new Date("2024-01-04T12:30:00.000Z"),
                end_timestamp: null,
                agency_name_scheduled: "ARRIVA SČ (Mladá Boleslav)",
                start_cis_stop_id: 21573,
                last_position_context: null,
                positions: PositionsFixture as IVPTripsPositionAttributes[],
            };

            const { positions } = await positionsManager.computePositions(tripPositions, undefined);
            const targetPositions = positions.filter(
                (p) =>
                    p?.last_stop_sequence === MLADA_BOLESLAV_AUT_ST_STOP_SEQUENCE ||
                    p?.next_stop_sequence === MLADA_BOLESLAV_AUT_ST_STOP_SEQUENCE ||
                    p?.this_stop_sequence === MLADA_BOLESLAV_AUT_ST_STOP_SEQUENCE
            );

            const thisStopIndex = targetPositions.findIndex((p) => p?.this_stop_sequence === MLADA_BOLESLAV_AUT_ST_STOP_SEQUENCE);
            const lastStopIndex = targetPositions.findIndex((p) => p?.last_stop_sequence === MLADA_BOLESLAV_AUT_ST_STOP_SEQUENCE);

            // All positions after arriving at Mlada Boleslav,aut.st. should have last_stop_sequence set to Mlada Boleslav,aut.st.
            for (let i = thisStopIndex; i < targetPositions.length; i++) {
                expect(targetPositions[i]?.this_stop_sequence).to.be.oneOf([MLADA_BOLESLAV_AUT_ST_STOP_SEQUENCE, undefined]);
                expect(targetPositions[i]?.last_stop_sequence).to.be.equal(MLADA_BOLESLAV_AUT_ST_STOP_SEQUENCE);
            }

            // All positions after departing Mlada Boleslav,aut.st. should have last_stop_sequence set to Mlada Boleslav,aut.st.
            for (let i = lastStopIndex; i < targetPositions.length; i++) {
                expect(targetPositions[i]?.last_stop_sequence).to.be.equal(MLADA_BOLESLAV_AUT_ST_STOP_SEQUENCE);
            }
        });
    });
});
