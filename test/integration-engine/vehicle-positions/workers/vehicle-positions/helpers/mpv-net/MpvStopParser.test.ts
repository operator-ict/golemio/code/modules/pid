import { MpvStopParser } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/mpv-net/MpvStopParser";
import { expect } from "chai";

describe("MpvStopParser", () => {
    it("should parse platform code (1/3)", () => {
        const platformCode = MpvStopParser.parsePlatformCode({ $: { stan: "1/3" } });
        expect(platformCode).to.be.equal("1/3");
    });

    it("should parse platform code (-/3)", () => {
        const platformCode = MpvStopParser.parsePlatformCode({ $: { stan: "-/3" } });
        expect(platformCode).to.be.equal("3");
    });

    it("should parse platform code (1/-)", () => {
        const platformCode = MpvStopParser.parsePlatformCode({ $: { stan: "1/-" } });
        expect(platformCode).to.be.equal("1");
    });

    it("should parse platform code (-/BUS)", () => {
        const platformCode = MpvStopParser.parsePlatformCode({ $: { stan: "-/BUS" } });
        expect(platformCode).to.be.equal("BUS");
    });

    it("should parse platform code (2/100V)", () => {
        const platformCode = MpvStopParser.parsePlatformCode({ $: { stan: "2/100V" } });
        expect(platformCode).to.be.equal("2/100V");
    });

    it("should parse platform code (-/5 C-F)", () => {
        const platformCode = MpvStopParser.parsePlatformCode({ $: { stan: "-/5 C-F" } });
        expect(platformCode).to.be.equal("5");
    });

    it("should parse platform code (2/5 A)", () => {
        const platformCode = MpvStopParser.parsePlatformCode({ $: { stan: "2/5 A" } });
        expect(platformCode).to.be.equal("2/5");
    });

    it("should parse platform code (5A/- A C)", () => {
        const platformCode = MpvStopParser.parsePlatformCode({ $: { stan: "5A/- A C" } });
        expect(platformCode).to.be.equal("5A");
    });

    it("should return null for empty platform code", () => {
        const platformCode = MpvStopParser.parsePlatformCode({ $: { stan: undefined } });
        expect(platformCode).to.be.null;
    });
});
