import { VPContainer } from "#ie/vehicle-positions/ioc/Di";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { PositionsManager } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/PositionsManager";
import { ValidToCalculator } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/ValidToCalculator";
import { ICurrentPositionProperties } from "#ie/vehicle-positions/workers/vehicle-positions/interfaces/VPInterfaces";
import { IVPTripsLastPositionContext } from "#sch/vehicle-positions/models/interfaces/VPTripsLastPositionInterfaces";
import { PostgresConnector, RedisConnector } from "@golemio/core/dist/integration-engine/connectors";
import * as turf from "@turf/turf";
import { expect } from "chai";
import sinon, { SinonSandbox, SinonStub } from "sinon";

describe("positionsManager - CorrectedDelay", () => {
    const validToCalculator = VPContainer.resolve<ValidToCalculator>(VPContainerToken.ValidToCalculator);
    const positionsManager = new PositionsManager(validToCalculator);

    let sequelizeModelStub: Record<string, SinonStub>;
    let sandbox: SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        sequelizeModelStub = Object.assign({
            belongsTo: sandbox.stub(),
            hasMany: sandbox.stub(),
            hasOne: sandbox.stub(),
            removeAttribute: sandbox.stub(),
        });
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().callsFake(() => sequelizeModelStub),
                query: sandbox.stub().callsFake(() => [true]),
                transaction: sandbox.stub().callsFake(() => Object.assign({ commit: sandbox.stub() })),
            })
        );
        sandbox.stub(RedisConnector, "getConnection");
    });
    afterEach(() => {
        sandbox.restore();
    });

    it("getCorrectedTimeDelay AHEAD #1", async () => {
        const correctedTimeDelay = await positionsManager["getCorrectedTimeDelay"](
            // initially computed delay = time is "14:17", scheduled arriival "14:20"
            -180,
            // context
            {
                atStopStreak: {
                    // "14:20"
                    // firstPositionTimestamp: 1644326400000,
                    // firstPositionDelay: 200,
                    stop_sequence: null,
                },
            } as IVPTripsLastPositionContext,
            // currentPosition
            turf.point([50, 14], {
                id: "1",
                origin_time: "14:17:00",
                origin_timestamp: new Date(1644326220000),
                tcp_event: null,
            } as ICurrentPositionProperties),
            // thisStopSequence
            10,
            // stopTimes
            {
                // "14:20"
                arrivalTime: 51600,
                // "14:25"
                departureTime: 51900,
            }
        );

        expect(correctedTimeDelay).to.equal(-180);
    });

    it("getCorrectedTimeDelay AHEAD #2", async () => {
        const correctedTimeDelay = await positionsManager["getCorrectedTimeDelay"](
            // initially computed delay = time is "14:20", scheduled arriival "14:20"
            0,
            // context
            {
                atStopStreak: {
                    // "14:20"
                    firstPositionTimestamp: 1644326220000,
                    firstPositionDelay: -180,
                    stop_sequence: 10,
                },
            } as IVPTripsLastPositionContext,
            // currentPosition
            turf.point([50, 14], {
                id: "1",
                origin_time: "14:20:00",
                origin_timestamp: new Date(1644326400000),
                tcp_event: null,
            } as ICurrentPositionProperties),
            // thisStopSequence
            10,
            // stopTimes
            {
                // "14:20"
                arrivalTime: 51600,
                // "14:25"
                departureTime: 51900,
            }
        );

        expect(correctedTimeDelay).to.equal(-180);
    });

    it("getCorrectedTimeDelay AHEAD #3", async () => {
        const correctedTimeDelay = await positionsManager["getCorrectedTimeDelay"](
            // initially computed delay = time is "14:24", scheduled arriival "14:20"
            240,
            // lastPosition
            {
                atStopStreak: {
                    // "14:17"
                    firstPositionTimestamp: 1644326220000,
                    firstPositionDelay: -180,
                    stop_sequence: 10,
                },
            } as IVPTripsLastPositionContext,
            // currentPosition
            turf.point([50, 14], {
                id: "1",
                origin_time: "14:24:00",
                origin_timestamp: new Date(1644326640000),
                tcp_event: null,
            } as ICurrentPositionProperties),
            // thisStopSequence
            10,
            // stopTimes
            {
                // "14:20"
                arrivalTime: 51600,
                // "14:25"
                departureTime: 51900,
            }
        );

        expect(correctedTimeDelay).to.equal(-60);
    });

    it("getCorrectedTimeDelay DELAYED #1", async () => {
        const correctedTimeDelay = await positionsManager["getCorrectedTimeDelay"](
            // initially computed delay = time is "14:23", scheduled arriival "14:20"
            180,
            // lastPosition
            {
                atStopStreak: {
                    // "14:20"
                    // firstPositionTimestamp: 1644326400000,
                    // firstPositionDelay: 200,
                    stop_sequence: null,
                },
            } as IVPTripsLastPositionContext,
            // currentPosition
            turf.point([50, 14], {
                id: "1",
                origin_time: "14:23:00",
                origin_timestamp: new Date(1644326580000),
                tcp_event: null,
            } as ICurrentPositionProperties),
            // thisStopSequence
            10,
            // stopTimes
            {
                // "14:20"
                arrivalTime: 51600,
                // "14:25"
                departureTime: 51900,
            }
        );

        expect(correctedTimeDelay).to.equal(0);
    });

    it("getCorrectedTimeDelay DELAYED #2", async () => {
        const correctedTimeDelay = await positionsManager["getCorrectedTimeDelay"](
            // initially computed delay = time is "14:24", scheduled arriival "14:20"
            240,
            // lastPosition
            {
                atStopStreak: {
                    // "14:23"
                    firstPositionTimestamp: 1644326580000,
                    firstPositionDelay: 0,
                    stop_sequence: 10,
                },
            } as IVPTripsLastPositionContext,
            // currentPosition
            turf.point([50, 14], {
                id: "1",
                origin_time: "14:24:00",
                origin_timestamp: new Date(1644326640000),
                tcp_event: null,
            } as ICurrentPositionProperties),
            // thisStopSequence
            10,
            // stopTimes
            {
                // "14:20"
                arrivalTime: 51600,
                // "14:25"
                departureTime: 51900,
            }
        );

        expect(correctedTimeDelay).to.equal(0);
    });

    it("getCorrectedTimeDelay DELAYED #3", async () => {
        const correctedTimeDelay = await positionsManager["getCorrectedTimeDelay"](
            // initially computed delay = time is "14:26", scheduled arriival "14:20"
            360,
            // lastPosition
            {
                atStopStreak: {
                    // "14:23"
                    firstPositionTimestamp: 1644326580000,
                    firstPositionDelay: 0,
                    stop_sequence: 10,
                },
            } as IVPTripsLastPositionContext,
            // currentPosition
            turf.point([50, 14], {
                id: "1",
                origin_time: "14:26:00",
                origin_timestamp: new Date(1644326760000),
                tcp_event: null,
            } as ICurrentPositionProperties),
            // thisStopSequence
            10,
            // stopTimes
            {
                // "14:20"
                arrivalTime: 51600,
                // "14:25"
                departureTime: 51900,
            }
        );

        expect(correctedTimeDelay).to.equal(60);
    });
});
