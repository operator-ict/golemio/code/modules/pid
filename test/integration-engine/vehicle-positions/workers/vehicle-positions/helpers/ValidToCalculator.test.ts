import { IGeoMeasurementHelper } from "#helpers/geo/interfaces/IGeoMeasurementHelper";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { VPContainer } from "#ie/vehicle-positions/ioc/Di";
import { ValidToCalculator } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/ValidToCalculator";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { expect } from "chai";
import { StatePositionEnum } from "src/const";
import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";

describe("ValidToCalculator", () => {
    const simpleConfig = VPContainer.resolve<ISimpleConfig>(CoreToken.SimpleConfig);
    const geoHelper = VPContainer.resolve<IGeoMeasurementHelper>(ModuleContainerToken.GeoMeasurementHelper);
    const validToCalculator = new ValidToCalculator(simpleConfig, geoHelper);

    const beforeTrackMockupInput = {
        positions: [
            {
                state_position: StatePositionEnum.BEFORE_TRACK,
                lng: 14.29806,
                lat: 50.06547,
                origin_timestamp: new Date(1656477708000), // středa 29. červen 2022 6:41:48
            } as any,
        ],
        gtfsData: {
            stop_times: [
                {
                    stop: {
                        stop_lon: 14.298032,
                        stop_lat: 50.065437,
                    },
                },
            ],
        },
        start_timestamp: 1656477720000, //středa 29. červen 2022 6:42:00
    } as any;

    it("should assign 'valid_to' ~ Tram, before_track", () => {
        const expectedValidTo = new Date(1656478020000); // středa 29. červen 2022 6:47:00
        const calculatedValidTo = validToCalculator.getValidToAttribute(
            beforeTrackMockupInput.positions[0],
            beforeTrackMockupInput.positions[0],
            GTFSRouteTypeEnum.TRAM,
            beforeTrackMockupInput.gtfsData,
            beforeTrackMockupInput.start_timestamp
        );
        expect(calculatedValidTo).to.deep.equal(expectedValidTo);
    });

    it("should assign 'valid_to' ~ Bus, before_track", () => {
        const expectedValidTo = new Date(1656478008000); // středa 29. červen 2022 6:46:48
        const calculatedValidTo = validToCalculator.getValidToAttribute(
            beforeTrackMockupInput.positions[0],
            beforeTrackMockupInput.positions[0],
            GTFSRouteTypeEnum.BUS,
            beforeTrackMockupInput.gtfsData,
            beforeTrackMockupInput.start_timestamp
        );
        expect(calculatedValidTo).to.deep.equal(expectedValidTo);
    });

    it("should assign 'valid_to' ~ empty positionToUpdate, before_track", () => {
        const expectedValidTo = new Date(1656478008000); // středa 29. červen 2022 6:46:48
        const calculatedValidTo = validToCalculator.getValidToAttribute(
            {} as any,
            beforeTrackMockupInput.positions[0],
            GTFSRouteTypeEnum.BUS,
            beforeTrackMockupInput.gtfsData,
            beforeTrackMockupInput.start_timestamp
        );
        expect(calculatedValidTo).to.deep.equal(expectedValidTo);
    });

    it("should assign 'valid_to' ~ cancelled line", () => {
        const expectedValidTo = new Date(1658406600000); //Thu Jul 21 2022 14:30:00 GMT+0200 (Středoevropský letní čas)
        const calculatedValidTo = validToCalculator.getValidToAttribute(
            {
                state_position: StatePositionEnum.CANCELED,
            } as any,
            {
                origin_timestamp: new Date(1658385000000),
            } as any,
            GTFSRouteTypeEnum.BUS,
            {
                stop_times: [
                    {
                        departure_time_seconds: 36000, //10:00
                    },
                ],
            } as any,
            1658354400000 //Thu Jul 21 2022 00:00:00 GMT+0200 (Středoevropský letní čas)
        );
        expect(calculatedValidTo).to.deep.equal(expectedValidTo);
    });
});
