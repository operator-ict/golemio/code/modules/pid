import { IEnrichedTripForPublicApi } from "#ie/vehicle-positions/workers/vehicle-positions/data-access/interfaces/TripRepositoryInterfaces";
import { GTFSRouteTypeEnum } from "#helpers/RouteTypeEnums";
import { StatePositionEnum } from "src/const";
import { expect } from "chai";
import { PublicCacheTripsFilter } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/public-api/PublicCacheTripsFilter";

describe("TripFilter", () => {
    it("filters trips correctly", () => {
        const unfilteredTrips: Array<Partial<IEnrichedTripForPublicApi>> = [
            {
                gtfs_trip_id: "115_6_201230",
                gtfs_route_type: GTFSRouteTypeEnum.BUS,
                gtfs_route_short_name: "115",
                run_number: 39,
                cis_trip_number: 1061,
                vehicle_registration_number: 1861,
                lat: "50.02",
                lng: "14.48",
                bearing: null,
                delay: null,
                state_position: StatePositionEnum.BEFORE_TRACK,
                created_at: new Date("2021-07-26 10:30:15.582 +0200"),
            },
            {
                gtfs_trip_id: "115_6_201239",
                gtfs_route_type: GTFSRouteTypeEnum.BUS,
                gtfs_route_short_name: "115",
                run_number: 39,
                cis_trip_number: 1061,
                vehicle_registration_number: 1861,
                lat: "59",
                lng: "17",
                bearing: null,
                delay: null,
                state_position: StatePositionEnum.CANCELED,
                created_at: new Date("2021-07-26 9:30:15.582 +0200"),
            },
            {
                gtfs_trip_id: "115_6_201230",
                gtfs_route_type: GTFSRouteTypeEnum.BUS,
                gtfs_route_short_name: "115",
                run_number: 39,
                cis_trip_number: 1061,
                vehicle_registration_number: 1861,
                lat: "50.03082",
                lng: "14.49163",
                bearing: null,
                delay: null,
                state_position: StatePositionEnum.ON_TRACK,
                created_at: new Date("2021-07-26 10:28:15.582 +0200"),
            },
        ];
        const { filteredTrips, futureTrips } = PublicCacheTripsFilter.getDistinctLatestTrips(
            unfilteredTrips as IEnrichedTripForPublicApi[]
        );
        expect(filteredTrips).length(1);
        expect(filteredTrips[0].state_position).to.eql(StatePositionEnum.ON_TRACK);
    });
});
