import { AnchorPointSegmenter } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/anchor-points/AnchorPointSegmenter";
import { ICurrentPositionProperties } from "#ie/vehicle-positions/workers/vehicle-positions/interfaces/VPInterfaces";
import * as turf from "@turf/turf";
import { expect } from "chai";
import { AnchorPointsFixture } from "./fixtures/AnchorPointsFixture";

describe("AnchorPointSegmenter", () => {
    describe("computeAnchorPointSegments", () => {
        it("should divide anchor points into segments", () => {
            const currentPosition = turf.point([14.34679, 50.09539], {} as ICurrentPositionProperties);
            const segmenter = new AnchorPointSegmenter(AnchorPointsFixture, currentPosition);
            const segments = segmenter["computeAnchorPointSegments"](AnchorPointsFixture);

            expect(segments.size).equal(3);

            expect(segments.get(1)?.length).to.equal(5);
            expect(segments.get(1)?.map((p) => p.index)).to.deep.equal([0, 1, 2, 3, 4]);

            expect(segments.get(2)?.length).to.equal(2);
            expect(segments.get(2)?.map((p) => p.index)).to.deep.equal([5, 6]);

            expect(segments.get(3)?.length).to.equal(3);
            expect(segments.get(3)?.map((p) => p.index)).to.deep.equal([5, 6, 7]);
        });
    });
});
