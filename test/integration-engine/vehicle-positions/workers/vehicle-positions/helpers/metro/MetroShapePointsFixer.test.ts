import { VPContainer } from "#ie/vehicle-positions/ioc/Di";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { MetroShapePointsFixer } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/metro/MetroShapePointsFixer";
import { IComputationTrip } from "#ie/vehicle-positions/workers/vehicle-positions/interfaces/VPInterfaces";
import { PostgresConnector } from "@golemio/core/dist/integration-engine";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";

chai.use(chaiAsPromised);

describe("MetroShapePointsFixer", () => {
    const mockData: IComputationTrip = JSON.parse(
        fs.readFileSync(__dirname + "/../../data/vehiclepositions-metroShapePointFix.json").toString()
    );
    const expectedData: IComputationTrip = JSON.parse(
        fs.readFileSync(__dirname + "/../../data/vehiclepositions-metroShapePointFix-expected.json").toString()
    );

    before(() => {
        PostgresConnector.connect();
    });

    it("test fixer on mocked data", async () => {
        const metroShapePointsFixer = VPContainer.resolve<MetroShapePointsFixer>(VPContainerToken.MetroShapePointsFixer);
        const fixedData = await metroShapePointsFixer.fix(mockData);

        expect(fixedData).to.deep.equal(expectedData);
    });
});
