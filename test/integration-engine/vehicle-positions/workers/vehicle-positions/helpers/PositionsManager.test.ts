import { VPContainer } from "#ie/vehicle-positions/ioc/Di";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { PositionsManager } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/PositionsManager";
import { ValidToCalculator } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/ValidToCalculator";
import {
    ICurrentPositionProperties,
    IShapeAnchorPoint,
} from "#ie/vehicle-positions/workers/vehicle-positions/interfaces/VPInterfaces";
import { IVPTripsLastPositionContext } from "#sch/vehicle-positions/models/interfaces/VPTripsLastPositionInterfaces";
import { DateTime } from "@golemio/core/dist/shared/luxon";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonFakeTimers } from "sinon";
import { StatePositionEnum, StateProcessEnum, TCPEventEnum } from "src/const";
import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";
import {
    getDelayBeforeTrackCESTToCETTestData,
    getDelayBeforeTrackCETToCESTTestData,
} from "./data/PositionsManager-getDelayBeforeTrack-dst-change";

const ONE_MINUTE_IN_SECONDS = 60;
const ONE_HOUR_IN_SECONDS = 60 * ONE_MINUTE_IN_SECONDS;

chai.use(chaiAsPromised);

describe("PositionsManager", () => {
    const validToCalculator = VPContainer.resolve<ValidToCalculator>(VPContainerToken.ValidToCalculator);
    const positionsManager = new PositionsManager(validToCalculator);

    const defaultContext: IVPTripsLastPositionContext = {
        atStopStreak: {
            stop_sequence: null,
            firstPositionTimestamp: null,
            firstPositionDelay: null,
        },
        lastPositionLastStop: {
            id: null,
            sequence: null,
            arrival_time: null,
            arrival_delay: null,
            departure_time: null,
            departure_delay: null,
        },
        lastPositionDelay: null,
        lastPositionId: null,
        lastPositionLng: null,
        lastPositionLat: null,
        lastPositionOriginTimestamp: null,
        lastPositionTracking: null,
        lastStopSequence: null,
        lastPositionCanceled: null,
        lastPositionBeforeTrackDelayed: null,
        lastPositionState: null,
        lastPositionStateChange: null,
        tripId: "2022-12-03T23:11:00Z_100375_375_4077",
    };

    const currentContext: IVPTripsLastPositionContext = {
        tripId: "2022-12-03T23:11:00Z_100375_375_4077",
        atStopStreak: {
            stop_sequence: null,
            firstPositionDelay: null,
            firstPositionTimestamp: null,
        },
        lastPositionId: "33",
        lastPositionDelay: 40,
        lastPositionCanceled: false,
        lastPositionLastStop: {
            id: null,
            sequence: null,
            arrival_time: null,
            arrival_delay: null,
            departure_time: null,
            departure_delay: null,
        },
        lastPositionTracking: {
            type: "Feature",
            geometry: { type: "Point", coordinates: [14.53414, 50.12516] },
            properties: {
                id: "33",
                lat: 50.12516,
                lng: 14.53414,
                delay: 40,
                bearing: 58,
                is_tracked: true,
                valid_to: new Date(1670110340000),
                tcp_event: null,
                is_canceled: false,
                origin_time: "00:27:20",
                this_stop_id: null,
                last_stop_id: "U109Z1",
                next_stop_id: "U171Z1",
                state_process: StateProcessEnum.PROCESSED,
                last_stop_headsign: null,
                last_stop_name: "Důstojnické domy",
                next_stop_name: "Letecké muzeum",
                state_position: StatePositionEnum.ON_TRACK,
                origin_timestamp: new Date(11670110040000),
                scheduled_timestamp: null,
                this_stop_sequence: null,
                last_stop_sequence: 8,
                next_stop_sequence: 9,
                shape_dist_traveled: 6.527,
                last_stop_arrival_time: new Date(11670109960000),
                next_stop_arrival_time: new Date(11670110020000),
                last_stop_departure_time: new Date(11670109960000),
                next_stop_departure_time: new Date(11670110020000),
            },
        },
        lastPositionState: null,
        lastPositionStateChange: null,
        lastPositionOriginTimestamp: 1670110040000,
        lastPositionBeforeTrackDelayed: null,
    };

    // =============================================================================
    // getCurrentContext
    // =============================================================================
    describe("getCurrentContext", () => {
        it("should return current context", () => {
            const context = positionsManager["getCurrentContext"]({
                id: "2022-12-03T23:11:00Z_100375_375_4077",
                last_position_context: currentContext,
            } as any);

            expect(context).to.deep.equal(currentContext);
        });

        it("should return default context", () => {
            const context = positionsManager["getCurrentContext"]({
                id: "2022-12-03T23:11:00Z_100375_375_4077",
            } as any);

            expect(context).to.deep.equal(defaultContext);
        });
    });

    // =============================================================================
    // getDelayBeforeTrack
    // =============================================================================
    describe("getDelayBeforeTrack", () => {
        it("should propagate last position's delay", () => {
            const startOfDay = DateTime.fromISO("2024-03-18T23:00:00.000Z").setZone("Europe/Prague");
            const firstStopDepartureTime = startOfDay.plus({ seconds: 50 }).toSeconds() - startOfDay.toSeconds();
            const lastPositionsDelay = 42;
            const positionOriginUnixTimestamp = startOfDay.plus({ seconds: 60 }).toMillis();
            const args: Parameters<(typeof positionsManager)["getDelayBeforeTrack"]> = [
                lastPositionsDelay,
                firstStopDepartureTime,
                positionOriginUnixTimestamp,
                startOfDay.toMillis(),
            ];
            const delay = positionsManager["getDelayBeforeTrack"](...args);
            expect(delay).to.equal(lastPositionsDelay);
        });

        it("should return corrected delay from attributes", () => {
            const startOfDay = DateTime.fromISO("2024-03-18T23:00:00.000Z").setZone("Europe/Prague");
            const firstStopDepartureTime = startOfDay.plus({ seconds: 50 }).toSeconds() - startOfDay.toSeconds();
            const lastPositionsDelay = 5;
            const positionOriginUnixTimestamp = startOfDay.plus({ seconds: 90 }).toMillis();
            const args: Parameters<(typeof positionsManager)["getDelayBeforeTrack"]> = [
                lastPositionsDelay,
                firstStopDepartureTime,
                positionOriginUnixTimestamp,
                startOfDay.toMillis(),
            ];
            const delay = positionsManager["getDelayBeforeTrack"](...args);
            expect(delay).to.equal(40);
        });

        const dstChangeTests = [
            {
                data: getDelayBeforeTrackCETToCESTTestData,
                getTitle: (tripStartTimestamp: string, firstStopDepartureTime: string, positionOriginTimestamp: string) =>
                    `should return corrected delay from attrs for trip start ${tripStartTimestamp}, origin ` +
                    `${positionOriginTimestamp}, and 1st stop dept. ${firstStopDepartureTime} (around CET -> CEST)`,
            },
            {
                data: getDelayBeforeTrackCESTToCETTestData,
                getTitle: (tripStartTimestamp: string, firstStopDepartureTime: string, positionOriginTimestamp: string) =>
                    `should return corrected delay from attrs for trip start ${tripStartTimestamp}, origin ` +
                    `${positionOriginTimestamp}, and 1st stop dept. ${firstStopDepartureTime} (around CEST -> CET)`,
            },
        ];

        dstChangeTests.forEach(({ data, getTitle }) => {
            data.forEach(({ tripStartTimestamp, firstStopDepartureTime, positionOriginTimestamp, correctDelay }) => {
                it(getTitle(tripStartTimestamp, firstStopDepartureTime, positionOriginTimestamp), async () => {
                    const [hours, minutes, seconds] = firstStopDepartureTime.split(":").map(Number);
                    const firstStopDepartureTimeInSeconds =
                        hours * ONE_HOUR_IN_SECONDS + minutes * ONE_MINUTE_IN_SECONDS + seconds;
                    const tripStartUnixTimestamp = new Date(tripStartTimestamp).valueOf();
                    const positionOriginUnixTimestamp = new Date(positionOriginTimestamp).valueOf();
                    const lastPositionsDelay = 5;

                    const startDayUnixTimestamp = positionsManager["getStartDayTimestamp"](
                        tripStartUnixTimestamp,
                        firstStopDepartureTimeInSeconds
                    );
                    const delay = positionsManager["getDelayBeforeTrack"](
                        lastPositionsDelay,
                        firstStopDepartureTimeInSeconds,
                        positionOriginUnixTimestamp,
                        startDayUnixTimestamp
                    );

                    expect(delay).to.equal(correctDelay);
                });
            });
        });

        it("should return null", () => {
            const startOfDay = DateTime.fromISO("2024-03-18T23:00:00.000Z").setZone("Europe/Prague");
            const firstStopDepartureTime = startOfDay.plus({ seconds: 50 }).toSeconds() - startOfDay.toSeconds();
            const lastPositionsDelay = undefined;
            const positionOriginUnixTimestamp = startOfDay.plus({ seconds: 60 }).toMillis();
            const args: Parameters<(typeof positionsManager)["getDelayBeforeTrack"]> = [
                lastPositionsDelay,
                firstStopDepartureTime,
                positionOriginUnixTimestamp,
                startOfDay.toMillis(),
            ];
            const delay = positionsManager["getDelayBeforeTrack"](...args);
            expect(delay).to.equal(null);
        });
    });

    // =============================================================================
    // getStateAndStopSequences
    // =============================================================================
    describe("getStateAndStopSequences", () => {
        const metroStopTimes = require("./fixtures/metro_992_1291_230213_stop_times.json");

        it("should return data (TRAM)", () => {
            const stopTimes = [
                {
                    arrival_time: "16:58:00",
                    departure_time: "16:58:00",
                    shape_dist_traveled: 0,
                    stop_headsign: null,
                    stop_id: "U945Z1P",
                    stop_sequence: 1,
                    arrival_time_seconds: 61080,
                    departure_time_seconds: 61080,
                    stop: {
                        stop_id: "U945Z1P",
                        stop_lat: 50.005123,
                        stop_lon: 14.436082,
                        stop_name: "Levského",
                    },
                },
                {
                    arrival_time: "16:58:00",
                    departure_time: "16:58:00",
                    shape_dist_traveled: 0.258011,
                    stop_headsign: null,
                    stop_id: "U946Z1P",
                    stop_sequence: 2,
                    arrival_time_seconds: 61080,
                    departure_time_seconds: 61080,
                    stop: {
                        stop_id: "U946Z1P",
                        stop_lat: 50.00523,
                        stop_lon: 14.432528,
                        stop_name: "Sídliště Modřany",
                    },
                },
                {
                    arrival_time: "16:59:00",
                    departure_time: "16:59:00",
                    shape_dist_traveled: 0.54451,
                    stop_headsign: null,
                    stop_id: "U947Z1P",
                    stop_sequence: 3,
                    arrival_time_seconds: 61140,
                    departure_time_seconds: 61140,
                    stop: {
                        stop_id: "U947Z1P",
                        stop_lat: 50.006416,
                        stop_lon: 14.429029,
                        stop_name: "Modřanská rokle",
                    },
                },
            ];

            const point: IShapeAnchorPoint = {
                index: Infinity, // Arbitrary value
                at_stop: true,
                bearing: 0,
                coordinates: [14.432528, 50.00523],
                distance_from_last_stop: 0,
                last_stop_sequence: 2,
                next_stop_sequence: 3,
                shape_dist_traveled: 0,
                this_stop_sequence: 2,
                time_scheduled_seconds: 0,
            };

            const data = positionsManager["getStateAndStopSequences"](
                GTFSRouteTypeEnum.TRAM,
                point as any,
                { this_stop_id: null } as ICurrentPositionProperties,
                null
            );

            expect(data.statePosition).to.equal(StatePositionEnum.AT_STOP);
            expect(data.thisStopSequence).to.equal(2);
            expect(data.lastStopSequence).to.equal(2);
            expect(data.nextStopSequence).to.equal(3);
        });

        it("should return data (METRO ON_TRACK)", () => {
            const point: IShapeAnchorPoint = {
                index: 13,
                at_stop: false,
                bearing: 256,
                coordinates: [14.55949, 50.10669],
                distance_from_last_stop: 0.023,
                last_stop_sequence: 1,
                next_stop_sequence: 2,
                shape_dist_traveled: 1.216,
                this_stop_sequence: null,
                time_scheduled_seconds: 57235,
            };

            const data = positionsManager["getStateAndStopSequences"](
                GTFSRouteTypeEnum.METRO,
                point as any,
                { this_stop_id: null } as ICurrentPositionProperties,
                null
            );

            expect(data.statePosition).to.equal(StatePositionEnum.ON_TRACK);
            expect(data.thisStopSequence).to.equal(null);
            expect(data.lastStopSequence).to.equal(1);
            expect(data.nextStopSequence).to.equal(2);
        });

        it("should return data (mismatched TRAM event T)", () => {
            const point: IShapeAnchorPoint = {
                index: 21,
                at_stop: false,
                bearing: 264,
                coordinates: [14.44979, 50.12201],
                distance_from_last_stop: 0.103,
                last_stop_sequence: 5,
                next_stop_sequence: 6,
                shape_dist_traveled: 1.72,
                this_stop_sequence: null,
                time_scheduled_seconds: 56720,
            };

            const context = {
                lastPositionTracking: {
                    properties: {
                        tcp_event: TCPEventEnum.ARRIVAL_ANNOUNCED,
                        last_stop_sequence: 6,
                        next_stop_sequence: 7,
                    },
                },
            };

            const data = positionsManager["getStateAndStopSequences"](
                GTFSRouteTypeEnum.TRAM,
                point,
                { tcp_event: TCPEventEnum.TIME } as ICurrentPositionProperties,
                context as IVPTripsLastPositionContext
            );

            expect(data.statePosition).to.equal(StatePositionEnum.MISMATCHED);
            expect(data.thisStopSequence).to.equal(null);
            expect(data.lastStopSequence).to.equal(5);
            expect(data.nextStopSequence).to.equal(6);
        });

        it("should return data (on_track BUS event T, close to the previous station)", () => {
            const point: IShapeAnchorPoint = {
                index: 282,
                at_stop: true,
                bearing: 351,
                coordinates: [14.48836, 50.02348],
                distance_from_last_stop: 0.024,
                last_stop_sequence: 44,
                next_stop_sequence: 45,
                shape_dist_traveled: 26.143,
                this_stop_sequence: 44,
                time_scheduled_seconds: 61200,
            };

            const context = {
                lastPositionTracking: {
                    properties: {
                        tcp_event: TCPEventEnum.DEPARTURED,
                        last_stop_sequence: 44,
                        next_stop_sequence: 45,
                    },
                },
            };

            const data = positionsManager["getStateAndStopSequences"](
                GTFSRouteTypeEnum.BUS,
                point,
                { tcp_event: TCPEventEnum.TIME } as ICurrentPositionProperties,
                context as IVPTripsLastPositionContext
            );

            expect(data.statePosition).to.equal(StatePositionEnum.ON_TRACK);
            expect(data.thisStopSequence).to.equal(null);
            expect(data.lastStopSequence).to.equal(44);
            expect(data.nextStopSequence).to.equal(45);
        });

        it("should return data (mismatched BUS event T)", () => {
            const point: IShapeAnchorPoint = {
                index: 282,
                at_stop: true,
                bearing: 351,
                coordinates: [14.48836, 50.02348],
                distance_from_last_stop: 0.024,
                last_stop_sequence: 44,
                next_stop_sequence: 45,
                shape_dist_traveled: 26.143,
                this_stop_sequence: 44,
                time_scheduled_seconds: 61200,
            };

            const context = {
                lastPositionTracking: {
                    properties: {
                        tcp_event: TCPEventEnum.DEPARTURED,
                        last_stop_sequence: 45,
                        next_stop_sequence: 46,
                    },
                },
            };

            const data = positionsManager["getStateAndStopSequences"](
                GTFSRouteTypeEnum.BUS,
                point,
                { tcp_event: TCPEventEnum.TIME } as ICurrentPositionProperties,
                context as IVPTripsLastPositionContext
            );

            expect(data.statePosition).to.equal(StatePositionEnum.MISMATCHED);
            expect(data.thisStopSequence).to.equal(null);
            expect(data.lastStopSequence).to.equal(44);
            expect(data.nextStopSequence).to.equal(45);
        });
    });

    // =============================================================================
    // isAfterTrack
    // =============================================================================
    describe("isAfterTrack", () => {
        const closestPoint: Pick<IShapeAnchorPoint, "last_stop_sequence"> = { last_stop_sequence: 9 };
        const lastTripPoint: Pick<IShapeAnchorPoint, "last_stop_sequence"> = { last_stop_sequence: 10 };
        let clock: SinonFakeTimers;

        beforeEach(() => {
            clock = sinon.useFakeTimers({
                now: new Date(2021, 2, 17, 10, 0),
                shouldAdvanceTime: true,
            });
        });

        afterEach(() => {
            clock.restore();
        });

        it("should return false (unknown TCP event)", () => {
            expect(positionsManager["isAfterTrack"](closestPoint.last_stop_sequence, lastTripPoint as IShapeAnchorPoint)).to.be
                .false;
        });

        it("should return true (close to/in the terminus)", () => {
            const closestPoint: Pick<IShapeAnchorPoint, "last_stop_sequence"> = lastTripPoint;
            expect(positionsManager["isAfterTrack"](closestPoint.last_stop_sequence, lastTripPoint as IShapeAnchorPoint)).to.be
                .true;
        });

        it("should return false (unknown scheduled timestamp)", () => {
            expect(positionsManager["isAfterTrack"](closestPoint.last_stop_sequence, lastTripPoint as IShapeAnchorPoint)).to.be
                .false;
        });

        it("should return false (still on track)", () => {
            expect(positionsManager["isAfterTrack"](closestPoint.last_stop_sequence, lastTripPoint as IShapeAnchorPoint)).to.be
                .false;
        });
    });
});
