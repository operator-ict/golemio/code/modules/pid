import {
    IExtendedMetroRunInputForProcessing,
    IMetroRunInputForProcessing,
} from "#ie/vehicle-positions/workers/runs/interfaces/IMetroRunInputForProcessing";
import PositionsMapper from "#ie/vehicle-positions/workers/vehicle-positions/data-access/helpers/PositionsMapper";
import TripsMapper from "#ie/vehicle-positions/workers/vehicle-positions/data-access/helpers/TripsMapper";
import { PositionsRepository } from "#ie/vehicle-positions/workers/vehicle-positions/data-access/PositionsRepository";
import { TripsRepository } from "#ie/vehicle-positions/workers/vehicle-positions/data-access/TripsRepository";
import { AbstractGTFSTripRunManager } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/gtfs-trip-run/AbstractGTFSTripRunManager";
import {
    GTFSTripRunManagerFactory,
    GTFSTripRunType,
} from "#ie/vehicle-positions/workers/vehicle-positions/helpers/gtfs-trip-run/GTFSTripRunManagerFactory";
import { IScheduleDto } from "#sch/vehicle-positions/redis/interfaces/IGtfsRunTripCacheDto";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";

chai.use(chaiAsPromised);

describe("GTFSTripMetroRunManager", () => {
    let sandbox: SinonSandbox;
    let tripsRepository: TripsRepository;
    let positionsRepository: PositionsRepository;
    let manager: AbstractGTFSTripRunManager;
    const schedule: IScheduleDto[] = [
        {
            // origin_route_id: "L991",
            trip_id: "991_77_220901",
            service_id: "1111100-1",
            direction_id: 0,
            shape_id: null,
            date: "2022-10-24",
            route_id: "L991",
            route_type: 1,
            route_short_name: "A",
            origin_route_name: "991",
            run_number: 77,
            is_regional: "0",
            is_substitute_transport: "0",
            is_night: "0",
            trip_headsign: "Nemocnice Motol",
            trip_short_name: null,
            block_id: null,
            exceptional: 0,
            min_stop_time: {
                hours: 9,
                minutes: 55,
            },
            max_stop_time: {
                hours: 10,
                minutes: 26,
            },
            start_timestamp: "2022-10-24T09:55:10+02:00",
            end_timestamp: "2022-10-24T10:26:20+02:00",
            first_stop_id: "U1071Z102P",
            last_stop_id: "U306Z102P",
            trip_number: 182,
            route_licence_number: null,
        },
        {
            // origin_route_id: "L991",
            trip_id: "991_78_220321",
            service_id: "1111100-1",
            direction_id: 0,
            shape_id: null,
            date: "2022-10-24",
            route_id: "L991",
            route_type: 1,
            route_short_name: "A",
            origin_route_name: "991",
            run_number: 78,
            is_regional: "0",
            is_substitute_transport: "0",
            is_night: "0",
            trip_headsign: "Skalka",
            trip_short_name: null,
            block_id: null,
            exceptional: 0,
            min_stop_time: {
                hours: 10,
                minutes: 0,
            },
            max_stop_time: {
                hours: 11,
                minutes: 0,
            },
            start_timestamp: "2022-10-24T10:32:35+02:00",
            end_timestamp: "2022-10-24T11:00:40+02:00",
            first_stop_id: "U306Z101P",
            last_stop_id: "U953Z101P",
            trip_number: null,
            route_licence_number: null,
        },
        {
            // origin_route_id: "L991",
            trip_id: "991_124_201230",
            service_id: "1111100-1",
            direction_id: 0,
            shape_id: null,
            date: "2022-10-24",
            route_id: "L991",
            route_type: 1,
            route_short_name: "A",
            origin_route_name: "991",
            run_number: 124,
            is_regional: "0",
            is_substitute_transport: "0",
            is_night: "0",
            trip_headsign: "Nemocnice Motol",
            trip_short_name: null,
            block_id: null,
            exceptional: 0,
            min_stop_time: {
                hours: 11,
                minutes: 2,
            },
            max_stop_time: {
                hours: 11,
                minutes: 31,
            },
            start_timestamp: "2022-10-24T11:02:40+02:00",
            end_timestamp: "2022-10-24T11:31:20+02:00",
            first_stop_id: "U953Z102P",
            last_stop_id: "U306Z102P",
            trip_number: null,
            route_licence_number: null,
        },
    ];

    const runInput: IExtendedMetroRunInputForProcessing = {
        messageTimestamp: "2022-10-24T08:25:34Z",
        timestampScheduled: new Date("2022-10-24T08:25:34Z").toISOString(),
        runNumber: 77,
        internalRunNumber: 77,
        routeId: "991",
        trainSetNumberScheduled: "5",
        trainSetNumberReal: "5",
        trainNumber: "182",
        coordinates: {
            lon: 14.36283,
            lat: 50.09831,
            gtfs_stop_id: "U157Z101P",
        },
        tripId: "991_77_220901",
    };

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        tripsRepository = Object.assign({
            upsertMetroRunTrip: sandbox
                .stub()
                .callsFake((gtfsTrip: IScheduleDto, runInput: IExtendedMetroRunInputForProcessing) => {
                    return TripsMapper.mapMetroRunToDto(gtfsTrip, runInput);
                }),
        });
        positionsRepository = Object.assign({
            upsertMetroRunPosition: sandbox
                .stub()
                .callsFake((runInput: IMetroRunInputForProcessing, gtfsTrip: IScheduleDto, isCurrent = true) => {
                    const result = PositionsMapper.mapMetroRunToDto(isCurrent, gtfsTrip, runInput);
                    return result;
                }),
        });
        manager = GTFSTripRunManagerFactory.create(
            GTFSTripRunType.Metro,
            positionsRepository,
            tripsRepository,
            sandbox.stub() as any,
            sandbox.stub() as any,
            true
        );
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("generateDelayMsg should return 1x tracking, 1x untracked", async () => {
        const clock = sinon.useFakeTimers(new Date("2022-10-24T08:24:00Z").getTime());
        const result = await manager.generateDelayMsg(schedule, runInput);
        const tracking = result.positions.filter((element) => !!element.is_tracked);
        const untracked = result.positions.filter((element) => !element.is_tracked);

        expect(tracking.length).to.equal(1);
        expect(untracked.length).to.equal(1);
        clock.restore();
    });

    it("generateDelayMsg should return empty arrays (future trip)", async () => {
        const clock = sinon.useFakeTimers(new Date("2022-10-24T04:24:00Z").getTime());
        const input = Object.assign({}, runInput, { tripId: "991_124_201230" });
        schedule[2].start_timestamp = new Date("2022-10-24T08:25:00Z").toISOString();

        const { updatedTrips } = await manager.generateDelayMsg(schedule, input);
        expect(updatedTrips).to.have.lengthOf(0);

        // change clock time to 7:30:00
        clock.tick(6 * 60 * 1000);

        const { updatedTrips: updatedTrips2 } = await manager.generateDelayMsg(schedule, input);
        expect(updatedTrips2).to.have.length.that.is.greaterThan(0);

        schedule[2].start_timestamp = "2022-10-24T11:02:40+02:00";
        clock.restore();
    });
});
