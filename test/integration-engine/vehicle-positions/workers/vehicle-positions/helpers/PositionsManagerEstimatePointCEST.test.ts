import { RopidGTFSTripsModel } from "#ie/ropid-gtfs/RopidGTFSTripsModel";
import { VPContainer } from "#ie/vehicle-positions/ioc/Di";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { DelayComputationManager } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/DelayComputationManager";
import { PositionsManager } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/PositionsManager";
import { ValidToCalculator } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/ValidToCalculator";
import {
    IComputationTrip,
    ICurrentPositionProperties,
} from "#ie/vehicle-positions/workers/vehicle-positions/interfaces/VPInterfaces";
import { config } from "@golemio/core/dist/integration-engine/config";
import { PostgresConnector, RedisConnector } from "@golemio/core/dist/integration-engine/connectors";
import moment from "@golemio/core/dist/shared/moment-timezone";
import * as turf from "@turf/turf";
import { expect } from "chai";
import sinon, { SinonFakeTimers, SinonSandbox } from "sinon";

describe("positionsManager - EstimatePointCEST", () => {
    const validToCalculator = VPContainer.resolve<ValidToCalculator>(VPContainerToken.ValidToCalculator);
    const positionsManager = new PositionsManager(validToCalculator);

    let sandbox: SinonSandbox;
    let clock: SinonFakeTimers;
    let manager: DelayComputationManager;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        clock = sinon.useFakeTimers({
            now: new Date(2021, 2, 17, 10, 0),
            shouldAdvanceTime: true,
        });
        PostgresConnector.connect();
        sandbox.stub(RedisConnector, "getConnection");

        const step = 0.05;
        sandbox.stub(config.vehiclePositions, "stepBetweenPoints").value(step);

        manager = new DelayComputationManager(new RopidGTFSTripsModel());
    });

    afterEach(() => {
        sandbox.restore();
        clock.restore();
    });

    it("getEstimatedPoint should return proper delay (70s delayed)", async () => {
        // position on track
        const currentPosition = {
            geometry: {
                coordinates: [14.4747, 50.1173],
                type: "Point",
            },
            properties: {
                origin_time: "10:01:41",
                origin_timestamp: new Date("2021-03-29T10:01:41+02:00"),
            },
            type: "Feature",
        };
        const lastPosition = null;
        const startTimestamp = new Date("2021-03-29T10:00:00+02:00").getTime();

        const tripGtfsData = (await manager.getComputationObject("TRIP_1")) as IComputationTrip;

        const startDayTimestamp = positionsManager["getStartDayTimestamp"](
            startTimestamp,
            tripGtfsData.shapes_anchor_points[0].time_scheduled_seconds
        );

        const estimatedPosition = await positionsManager["getEstimatedPoint"](
            tripGtfsData,
            currentPosition as any,
            lastPosition,
            startDayTimestamp,
            undefined as any
        );

        expect(estimatedPosition.delay).to.equal(70);
    });

    // eslint-disable-next-line max-len
    it("getEstimatedPoint should return proper delay (trip started day before, 70s delayed)", async () => {
        // position on track
        const currentPosition = turf.point([14.4747, 50.1173], {
            id: "1231241",
            origin_time: "04:01:41",
            origin_timestamp: moment("2021-03-27T04:01:41+01:00").toDate(), //1615971610000
        } as ICurrentPositionProperties);
        const lastPosition = null;
        const startTimestamp = new Date("2021-03-27T04:00:00+01:00").getTime();

        const tripGtfsData = (await manager.getComputationObject("TRIP_2")) as IComputationTrip;

        const startDayTimestamp = positionsManager["getStartDayTimestamp"](
            startTimestamp,
            tripGtfsData.shapes_anchor_points[0].time_scheduled_seconds
        );

        const estimatedPosition = await positionsManager["getEstimatedPoint"](
            tripGtfsData,
            currentPosition,
            lastPosition,
            startDayTimestamp,
            undefined as any
        );

        expect(estimatedPosition.delay).to.equal(70);
    });

    it("getEstimatedPoint should return proper delay (70s delayed, even this day was shift from CET to CEST)", async () => {
        // position on track
        const currentPosition = turf.point([14.4747, 50.1173], {
            id: "1231241",
            origin_time: "10:01:41",
            origin_timestamp: moment("2021-03-28T10:01:41+02:00").toDate(),
        } as ICurrentPositionProperties);
        const lastPosition = null;
        const startTimestamp = new Date("2021-03-28T10:00:00+02:00").getTime();

        const tripGtfsData = (await manager.getComputationObject("TRIP_1")) as IComputationTrip;

        const startDayTimestamp = positionsManager["getStartDayTimestamp"](
            startTimestamp,
            tripGtfsData.shapes_anchor_points[0].time_scheduled_seconds
        );

        const estimatedPosition = await positionsManager["getEstimatedPoint"](
            tripGtfsData,
            currentPosition,
            lastPosition,
            startDayTimestamp,
            undefined as any
        );

        expect(estimatedPosition.delay).to.equal(70);
    });

    // eslint-disable-next-line max-len
    it("getEstimatedPoint should return proper delay (trip started day before, 70s delayed, even this day was shift from CET to CEST)", async () => {
        // position on track
        const currentPosition = turf.point([14.4747, 50.1173], {
            id: "1231241",
            origin_time: "05:01:41",
            origin_timestamp: moment("2021-03-28T05:01:41+02:00").toDate(), //1615971610000
        } as ICurrentPositionProperties);
        const lastPosition = null;
        const startTimestamp = new Date("2021-03-28T05:00:00+02:00").getTime();

        const tripGtfsData = (await manager.getComputationObject("TRIP_2")) as IComputationTrip;

        const startDayTimestamp = positionsManager["getStartDayTimestamp"](
            startTimestamp,
            tripGtfsData.shapes_anchor_points[0].time_scheduled_seconds
        );

        const estimatedPosition = await positionsManager["getEstimatedPoint"](
            tripGtfsData,
            currentPosition,
            lastPosition,
            startDayTimestamp,
            undefined as any
        );

        expect(estimatedPosition.delay).to.equal(70);
    });

    // eslint-disable-next-line max-len
    it("getEstimatedPoint should return proper delay (trip started day before, 70s delayed, even previous day was shift from CET to CEST)", async () => {
        // position on track
        const currentPosition = turf.point([14.4747, 50.1173], {
            id: "1231241",
            origin_time: "04:01:41",
            origin_timestamp: moment("2021-03-29T04:01:41+02:00").toDate(), //1615971610000
        } as ICurrentPositionProperties);
        const lastPosition = null;
        const startTimestamp = new Date("2021-03-29T04:00:00+02:00").getTime();

        const tripGtfsData = (await manager.getComputationObject("TRIP_2")) as IComputationTrip;

        const startDayTimestamp = positionsManager["getStartDayTimestamp"](
            startTimestamp,
            tripGtfsData.shapes_anchor_points[0].time_scheduled_seconds
        );

        const estimatedPosition = await positionsManager["getEstimatedPoint"](
            tripGtfsData,
            currentPosition,
            lastPosition,
            startDayTimestamp,
            undefined as any
        );

        expect(estimatedPosition.delay).to.equal(70);
    });

    it("getEstimatedPoint should return proper delay (70s delayed, even this day was shift from CEST to CET)", async () => {
        // position on track
        const currentPosition = turf.point([14.4747, 50.1173], {
            id: "1231241",
            origin_time: "10:01:41",
            origin_timestamp: moment("2021-10-31T10:01:41+01:00").toDate(),
        } as ICurrentPositionProperties);
        const lastPosition = null;
        const startTimestamp = new Date("2021-10-31T10:00:00+02:00").getTime();

        const tripGtfsData = (await manager.getComputationObject("TRIP_1")) as IComputationTrip;

        const startDayTimestamp = positionsManager["getStartDayTimestamp"](
            startTimestamp,
            tripGtfsData.shapes_anchor_points[0].time_scheduled_seconds
        );

        const estimatedPosition = await positionsManager["getEstimatedPoint"](
            tripGtfsData,
            currentPosition,
            lastPosition,
            startDayTimestamp,
            undefined as any
        );

        expect(estimatedPosition.delay).to.equal(70);
    });

    // eslint-disable-next-line max-len
    it("getEstimatedPoint should return proper delay (trip started day before, 70s delayed, even this day was shift from CEST to CET)", async () => {
        // position on track
        const currentPosition = turf.point([14.4747, 50.1173], {
            id: "1231241",
            origin_time: "03:01:41",
            origin_timestamp: moment("2021-10-31T03:01:41+01:00").toDate(), //1615971610000
        } as ICurrentPositionProperties);
        const lastPosition = null;
        const startTimestamp = new Date("2021-10-31T03:00:00+01:00").getTime();

        const tripGtfsData = (await manager.getComputationObject("TRIP_2")) as IComputationTrip;

        const startDayTimestamp = positionsManager["getStartDayTimestamp"](
            startTimestamp,
            tripGtfsData.shapes_anchor_points[0].time_scheduled_seconds
        );

        const estimatedPosition = await positionsManager["getEstimatedPoint"](
            tripGtfsData,
            currentPosition,
            lastPosition,
            startDayTimestamp,
            undefined as any
        );

        expect(estimatedPosition.delay).to.equal(70);
    });

    // eslint-disable-next-line max-len
    it("getEstimatedPoint should return proper delay (trip started day before, 191s delayed, previous day was shift from CET to CEST)", async () => {
        // position on track
        const currentPosition = turf.point([14.4747, 50.1173], {
            id: "1231241",
            origin_time: "00:01:41",
            origin_timestamp: moment("2021-03-29T00:01:41+02:00").toDate(), //1615971610000
        } as ICurrentPositionProperties);
        const lastPosition = null;
        const startTimestamp = new Date("2021-03-28T23:58:00+02:00").getTime();

        const tripGtfsData = (await manager.getComputationObject("TRIP_3")) as IComputationTrip;

        const startDayTimestamp = positionsManager["getStartDayTimestamp"](
            startTimestamp,
            tripGtfsData.shapes_anchor_points[0].time_scheduled_seconds
        );

        const estimatedPosition = await positionsManager["getEstimatedPoint"](
            tripGtfsData,
            currentPosition,
            lastPosition,
            startDayTimestamp,
            undefined as any
        );

        expect(estimatedPosition.delay).to.equal(190);
    });
});
