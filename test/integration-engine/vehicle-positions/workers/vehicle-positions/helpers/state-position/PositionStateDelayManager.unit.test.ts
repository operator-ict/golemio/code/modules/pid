import { PositionStateDelayManager } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/state-position/PositionStateDelayManager";
import { IVPTripsLastPositionContext } from "#sch/vehicle-positions/models/interfaces/VPTripsLastPositionInterfaces";
import { expect } from "chai";
import sinon, { SinonFakeTimers, SinonSandbox } from "sinon";
import { StatePositionEnum } from "src/const";

describe("PositionStateDelayManager (unit)", () => {
    describe("shouldPropagateAfterTrackDelay", () => {
        let sandbox: SinonSandbox;
        let clock: SinonFakeTimers;

        beforeEach(() => {
            sandbox = sinon.createSandbox();
            clock = sandbox.useFakeTimers({
                now: new Date("2024-10-03T23:06:53.630Z"),
                shouldAdvanceTime: false,
            });
        });

        afterEach(() => {
            clock.restore();
            sandbox.restore();
        });

        it("should return true if state was changed less than 30 seconds ago", () => {
            const context: Partial<IVPTripsLastPositionContext> = {
                lastPositionState: StatePositionEnum.ON_TRACK,
                lastPositionStateChange: new Date("2024-10-03T23:06:43.630Z").toISOString(),
            };

            const result = PositionStateDelayManager.shouldPropagateAfterTrackDelay(context as IVPTripsLastPositionContext);
            expect(result).to.be.true;
        });

        it("should return true if state was changed more than 30 seconds ago (from ON_TRACK)", () => {
            const context: Partial<IVPTripsLastPositionContext> = {
                lastPositionState: StatePositionEnum.ON_TRACK,
                lastPositionStateChange: new Date("2024-10-03T23:06:22.630Z").toISOString(),
            };

            const result = PositionStateDelayManager.shouldPropagateAfterTrackDelay(context as IVPTripsLastPositionContext);
            expect(result).to.be.true;
        });

        it("should return false if context is null", () => {
            const context = null;
            const result = PositionStateDelayManager.shouldPropagateAfterTrackDelay(context);

            expect(result).to.be.false;
        });

        it("should return false if lastPositionState is AFTER_TRACK", () => {
            const context: Partial<IVPTripsLastPositionContext> = {
                lastPositionState: StatePositionEnum.AFTER_TRACK,
                lastPositionStateChange: new Date("2024-10-03T23:06:43.630Z").toISOString(),
            };

            const result = PositionStateDelayManager.shouldPropagateAfterTrackDelay(context as IVPTripsLastPositionContext);
            expect(result).to.be.false;
        });

        it("should return false if lastPositionStateChange is null", () => {
            const context: Partial<IVPTripsLastPositionContext> = {
                lastPositionState: StatePositionEnum.ON_TRACK,
                lastPositionStateChange: null,
            };

            const result = PositionStateDelayManager.shouldPropagateAfterTrackDelay(context as IVPTripsLastPositionContext);
            expect(result).to.be.false;
        });

        it("should return false if state was changed more than 30 seconds ago", () => {
            const context: Partial<IVPTripsLastPositionContext> = {
                lastPositionState: StatePositionEnum.AFTER_TRACK_DELAYED,
                lastPositionStateChange: new Date("2024-10-03T23:06:22.630Z").toISOString(),
            };

            const result = PositionStateDelayManager.shouldPropagateAfterTrackDelay(context as IVPTripsLastPositionContext);
            expect(result).to.be.false;
        });
    });
});
