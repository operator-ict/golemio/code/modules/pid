import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { HttpGtfsTripDataFixer } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/gtfs-trip-data/strategy/HttpGtfsTripDataFixer";
import { QueueManager } from "@golemio/core/dist/integration-engine";
import { container } from "@golemio/core/dist/shared/tsyringe";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";

describe("HttpGtfsTripDataFixer", () => {
    let sandbox: SinonSandbox;

    const createContainer = () =>
        container
            .createChildContainer()
            .register(
                VPContainerToken.TripRepository,
                class TripsRepository {
                    getTripsWithInvalidGtfsTripId = sandbox.stub().resolves([]);
                }
            )
            .register(VPContainerToken.HttpGtfsTripDataFixer, HttpGtfsTripDataFixer);

    before(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        container.clearInstances();
        sandbox.restore();
    });

    it("should return correct output (zero invalid trips)", async () => {
        const queueStub = sandbox.stub(QueueManager, "sendMessageToExchange").resolves();
        const testContainer = createContainer();
        const fixer = testContainer.resolve<HttpGtfsTripDataFixer>(VPContainerToken.HttpGtfsTripDataFixer);
        const result = await fixer.fix("testprefix");

        expect(result).to.equal(0);
        expect(queueStub.callCount).to.equal(0);
    });

    it("should return correct output (multiple invalid trips)", async () => {
        const queueStub = sandbox.stub(QueueManager, "sendMessageToExchange").resolves();
        const testContainer = createContainer();

        testContainer.register(
            VPContainerToken.TripRepository,
            class TripsRepository {
                getTripsWithInvalidGtfsTripId = sandbox.stub().resolves([
                    {
                        id: "trip1",
                    },
                    {
                        id: "trip2",
                    },
                ]);
            }
        );

        const fixer = testContainer.resolve<HttpGtfsTripDataFixer>(VPContainerToken.HttpGtfsTripDataFixer);
        const result = await fixer.fix("testprefix");

        expect(result).to.equal(2);
        expect(queueStub.callCount).to.equal(1);
    });
});
