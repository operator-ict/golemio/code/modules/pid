import ComputeDelayHelper from "#ie/vehicle-positions/workers/vehicle-positions/helpers/compute-positions/ComputeDelayHelper";
import { IPositionToUpdate } from "#ie/vehicle-positions/workers/vehicle-positions/interfaces/VPInterfaces";
import { IVPTripsPositionAttributes } from "#sch/vehicle-positions/models/interfaces/IVPTripsPositionAttributes";
import { IVPTripsLastPositionContext } from "#sch/vehicle-positions/models/interfaces/VPTripsLastPositionInterfaces";
import { expect } from "chai";
import { StatePositionEnum, TCPEventEnum } from "src/const";
import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";

describe("ComputeDelayHelper - Strategy BusDelayAtStop", () => {
    const routeType = GTFSRouteTypeEnum.BUS;

    it("should return updated context", () => {
        const position: Partial<IVPTripsPositionAttributes> = {
            tcp_event: TCPEventEnum.DEPARTURED,
            state_position: StatePositionEnum.ON_TRACK,
            is_tracked: true,
            last_stop_sequence: 3,
            origin_timestamp: new Date(1675809271000),
            delay: 90,
        };

        const positionToUpdate: Partial<IPositionToUpdate> = {
            delay: 95,
        };

        const context: Partial<IVPTripsLastPositionContext> = {
            tripId: "2023-02-07T23:33:00+01:00_234_115_220928_3906",
            lastPositionLastStop: {
                id: "U557Z2P",
                sequence: null,
                arrival_time: null,
                arrival_delay: null,
                departure_time: null,
                departure_delay: null,
            },
        };

        const resultContext = ComputeDelayHelper.updateContext(
            context as IVPTripsLastPositionContext,
            positionToUpdate as IPositionToUpdate,
            position as IVPTripsPositionAttributes,
            routeType
        );

        expect(resultContext).to.deep.equal({
            tripId: "2023-02-07T23:33:00+01:00_234_115_220928_3906",
            lastPositionDelay: 95,
            lastPositionLastStop: {
                id: "U557Z2P",
                sequence: 3,
                arrival_time: null,
                arrival_delay: null,
                departure_time: null,
                departure_delay: 95,
            },
        });
    });

    it("should return updated context (AFTER_TRACK_DELAYED)", () => {
        const positiontToUpdate: Partial<IPositionToUpdate> = {
            state_position: StatePositionEnum.AFTER_TRACK_DELAYED,
            last_stop_sequence: 3,
            delay: null,
            delay_stop_arrival: 30,
            delay_stop_departure: null,
        };

        const context: Partial<IVPTripsLastPositionContext> = {
            tripId: "2023-02-07T23:33:00+01:00_234_115_220928_3906",
            lastPositionLastStop: {
                id: "U557Z2P",
                sequence: 2,
                arrival_time: null,
                arrival_delay: null,
                departure_time: null,
                departure_delay: null,
            },
        };

        const resultContext = ComputeDelayHelper.updateContext(
            context as IVPTripsLastPositionContext,
            positiontToUpdate as IPositionToUpdate,
            { is_tracked: false } as IVPTripsPositionAttributes,
            routeType
        );

        expect(resultContext).to.deep.equal({
            tripId: "2023-02-07T23:33:00+01:00_234_115_220928_3906",
            lastPositionDelay: null,
            lastPositionLastStop: {
                id: "U557Z2P",
                sequence: 3,
                arrival_time: null,
                arrival_delay: 30,
                departure_time: null,
                departure_delay: null,
            },
        });
    });

    it("should return updated context (AFTER_TRACK)", () => {
        const positiontToUpdate: Partial<IPositionToUpdate> = {
            state_position: StatePositionEnum.AFTER_TRACK,
            last_stop_sequence: 3,
            delay: null,
            delay_stop_arrival: 30,
            delay_stop_departure: null,
        };

        const context: Partial<IVPTripsLastPositionContext> = {
            tripId: "2023-02-07T23:33:00+01:00_234_115_220928_3906",
            lastPositionLastStop: {
                id: "U557Z2P",
                sequence: 2,
                arrival_time: null,
                arrival_delay: null,
                departure_time: null,
                departure_delay: null,
            },
        };

        const resultContext = ComputeDelayHelper.updateContext(
            context as IVPTripsLastPositionContext,
            positiontToUpdate as IPositionToUpdate,
            { is_tracked: false } as IVPTripsPositionAttributes,
            routeType
        );

        expect(resultContext).to.deep.equal({
            tripId: "2023-02-07T23:33:00+01:00_234_115_220928_3906",
            lastPositionDelay: null,
            lastPositionLastStop: {
                id: "U557Z2P",
                sequence: 3,
                arrival_time: null,
                arrival_delay: 30,
                departure_time: null,
                departure_delay: null,
            },
        });
    });

    it("should return updated position (event O departured)", () => {
        const positionToUpdate: Partial<IPositionToUpdate> = {
            state_position: StatePositionEnum.ON_TRACK,
            is_tracked: true,
            last_stop_sequence: 3,
            last_stop_departure_time: new Date(1675809281000),
            delay: 95,
        };

        const position: Partial<IVPTripsPositionAttributes> = {
            tcp_event: TCPEventEnum.DEPARTURED,
            state_position: StatePositionEnum.ON_TRACK,
            is_tracked: true,
            origin_timestamp: new Date(1675809281000 + 95 * 1000),
            last_stop_sequence: 3,
        };

        const context: Partial<IVPTripsLastPositionContext> = {
            tripId: "2023-02-07T23:33:00+01:00_234_115_220928_3906",
            lastPositionDelay: null,
            lastPositionLastStop: {
                id: "U557Z2P",
                sequence: 3,
                arrival_time: null,
                arrival_delay: null,
                departure_time: null,
                departure_delay: null,
            },
        };

        const resultPositionToUpdate = ComputeDelayHelper.updatePositionToUpdate(
            context as IVPTripsLastPositionContext,
            positionToUpdate as IPositionToUpdate,
            position as IVPTripsPositionAttributes,
            routeType
        );

        expect(resultPositionToUpdate).to.deep.equal({
            state_position: StatePositionEnum.ON_TRACK,
            is_tracked: true,
            last_stop_sequence: 3,
            last_stop_departure_time: new Date(1675809281000),
            delay_stop_departure: 95,
            delay: 95,
        });
    });

    it("should just return (unknown TCP event)", () => {
        const positionToUpdate: Partial<IPositionToUpdate> = {
            state_position: StatePositionEnum.ON_TRACK,
            is_tracked: true,
            last_stop_sequence: 3,
            last_stop_departure_time: new Date(1675809281000),
        };

        const position: Partial<IVPTripsPositionAttributes> = {
            state_position: StatePositionEnum.ON_TRACK,
            is_tracked: true,
            last_stop_sequence: 3,
            origin_timestamp: new Date(1675809281000 + 95 * 1000),
        };

        const context: Partial<IVPTripsLastPositionContext> = {
            tripId: "2023-02-07T23:33:00+01:00_234_115_220928_3906",
            lastPositionDelay: 95,
            lastPositionLastStop: {
                id: "U557Z2P",
                sequence: 3,
                arrival_time: null,
                arrival_delay: null,
                departure_time: null,
                departure_delay: 95,
            },
        };

        const resultPositionToUpdate = ComputeDelayHelper.updatePositionToUpdate(
            context as IVPTripsLastPositionContext,
            positionToUpdate as IPositionToUpdate,
            position as IVPTripsPositionAttributes,
            routeType
        );

        expect(resultPositionToUpdate).to.deep.equal(positionToUpdate);
    });

    describe("Event P (arrival announced)", () => {
        const positionToUpdate: Partial<IPositionToUpdate> = {
            state_position: StatePositionEnum.AT_STOP,
            is_tracked: true,
            last_stop_sequence: 4,
            last_stop_arrival_time: new Date(1675809281000),
            delay: 90,
        };

        const position: Partial<IVPTripsPositionAttributes> = {
            tcp_event: TCPEventEnum.ARRIVAL_ANNOUNCED,
            state_position: StatePositionEnum.UNKNOWN,
            is_tracked: true,
            origin_timestamp: new Date(1675809281000 + 90 * 1000),
        };

        it("should return updated position", () => {
            const context: Partial<IVPTripsLastPositionContext> = {
                tripId: "2023-02-07T23:33:00+01:00_234_115_220928_3906",
                lastPositionDelay: 95,
                lastPositionLastStop: {
                    id: "U557Z2P",
                    sequence: 3,
                    arrival_time: null,
                    arrival_delay: 95,
                    departure_time: null,
                    departure_delay: 95,
                },
            };

            const resultPositionToUpdate = ComputeDelayHelper.updatePositionToUpdate(
                context as IVPTripsLastPositionContext,
                positionToUpdate as IPositionToUpdate,
                position as IVPTripsPositionAttributes,
                routeType
            );

            expect(resultPositionToUpdate).to.deep.equal({
                state_position: StatePositionEnum.AT_STOP,
                is_tracked: true,
                last_stop_sequence: 4,
                last_stop_arrival_time: new Date(1675809281000),
                delay: 90,
                delay_stop_arrival: 90,
            });
        });
    });

    describe("Event T (terminated)", () => {
        const positionToUpdate: Partial<IPositionToUpdate> = {
            state_position: StatePositionEnum.ON_TRACK,
            is_tracked: true,
            last_stop_sequence: 3,
            last_stop_departure_time: new Date(1675809281000),
        };

        const position: Partial<IVPTripsPositionAttributes> = {
            tcp_event: TCPEventEnum.TIME,
            state_position: StatePositionEnum.ON_TRACK,
            is_tracked: true,
            last_stop_sequence: 3,
        };

        it("should return updated position", () => {
            const context: Partial<IVPTripsLastPositionContext> = {
                tripId: "2023-02-07T23:33:00+01:00_234_115_220928_3906",
                lastPositionDelay: 95,
                lastPositionLastStop: {
                    id: "U557Z2P",
                    sequence: 3,
                    arrival_time: null,
                    arrival_delay: null,
                    departure_time: null,
                    departure_delay: 95,
                },
            };

            const resultPositionToUpdate = ComputeDelayHelper.updatePositionToUpdate(
                context as IVPTripsLastPositionContext,
                Object.assign({}, positionToUpdate) as IPositionToUpdate,
                position as IVPTripsPositionAttributes,
                routeType
            );

            expect(resultPositionToUpdate).to.deep.equal({
                state_position: StatePositionEnum.ON_TRACK,
                is_tracked: true,
                last_stop_sequence: 3,
                last_stop_departure_time: new Date(1675809281000),
                delay_stop_departure: 95,
            });
        });

        it("should return updated position (last stop delay 0)", () => {
            const context: Partial<IVPTripsLastPositionContext> = {
                tripId: "2023-02-07T23:33:00+01:00_234_115_220928_3906",
                lastPositionDelay: 95,
                lastPositionLastStop: {
                    id: "U557Z2P",
                    sequence: 3,
                    arrival_time: null,
                    arrival_delay: null,
                    departure_time: null,
                    departure_delay: 0,
                },
            };

            const resultPositionToUpdate = ComputeDelayHelper.updatePositionToUpdate(
                context as IVPTripsLastPositionContext,
                Object.assign({}, positionToUpdate) as IPositionToUpdate,
                position as IVPTripsPositionAttributes,
                routeType
            );

            expect(resultPositionToUpdate).to.deep.equal({
                state_position: StatePositionEnum.ON_TRACK,
                is_tracked: true,
                last_stop_sequence: 3,
                last_stop_departure_time: new Date(1675809281000),
                delay_stop_departure: 0,
            });
        });
    });

    describe("AFTER_TRACK and AFTER_TRACK_DELAYED", () => {
        it("should return updated position (AFTER_TRACK_DELAYED arrival)", () => {
            const positionToUpdate: Partial<IPositionToUpdate> = {
                state_position: StatePositionEnum.AFTER_TRACK_DELAYED,
                last_stop_sequence: 3,
                last_stop_arrival_time: new Date(1675809281000),
                last_stop_departure_time: null,
                delay: 195,
            };

            const position: Partial<IVPTripsPositionAttributes> = {
                tcp_event: TCPEventEnum.TERMINATED,
                is_tracked: false,
                origin_timestamp: new Date(1675809281000 + 195 * 1000),
                scheduled_timestamp: new Date(1675809281000),
            };

            const context: Partial<IVPTripsLastPositionContext> = {
                tripId: "2023-02-07T23:33:00+01:00_234_115_220928_3906",
                lastPositionDelay: null,
                lastPositionLastStop: {
                    id: "U557Z2P",
                    sequence: 2,
                    arrival_time: null,
                    arrival_delay: null,
                    departure_time: null,
                    departure_delay: null,
                },
                atStopStreak: {
                    firstPositionDelay: null,
                    firstPositionTimestamp: null,
                    stop_sequence: null,
                },
            };

            const resultPositionToUpdate = ComputeDelayHelper.updatePositionToUpdate(
                context as IVPTripsLastPositionContext,
                positionToUpdate as IPositionToUpdate,
                position as IVPTripsPositionAttributes,
                routeType
            );

            expect(resultPositionToUpdate).to.deep.equal({
                state_position: StatePositionEnum.AFTER_TRACK_DELAYED,
                last_stop_sequence: 3,
                last_stop_arrival_time: new Date(1675809281000),
                last_stop_departure_time: null,
                delay_stop_arrival: 195,
                delay: 195,
            });
        });

        it("should return updated position (AFTER_TRACK_DELAYED subsequent)", () => {
            const positionToUpdate: Partial<IPositionToUpdate> = {
                state_position: StatePositionEnum.AFTER_TRACK_DELAYED,
                last_stop_sequence: 3,
                last_stop_arrival_time: new Date(1675809281000),
                last_stop_departure_time: null,
                delay: 195,
            };

            const position: Partial<IVPTripsPositionAttributes> = {
                tcp_event: TCPEventEnum.TIME,
                is_tracked: false,
                origin_timestamp: new Date(1675809281000 + 195 * 1000),
                scheduled_timestamp: new Date(1675809281000),
            };

            const context: Partial<IVPTripsLastPositionContext> = {
                tripId: "2023-02-07T23:33:00+01:00_234_115_220928_3906",
                lastPositionDelay: null,
                lastPositionLastStop: {
                    id: "U557Z2P",
                    sequence: 3,
                    arrival_time: null,
                    arrival_delay: 29,
                    departure_time: null,
                    departure_delay: null,
                },
                atStopStreak: {
                    firstPositionDelay: null,
                    firstPositionTimestamp: null,
                    stop_sequence: null,
                },
            };

            const resultPositionToUpdate = ComputeDelayHelper.updatePositionToUpdate(
                context as IVPTripsLastPositionContext,
                positionToUpdate as IPositionToUpdate,
                position as IVPTripsPositionAttributes,
                routeType
            );

            expect(resultPositionToUpdate).to.deep.equal({
                state_position: StatePositionEnum.AFTER_TRACK_DELAYED,
                last_stop_sequence: 3,
                last_stop_arrival_time: new Date(1675809281000),
                last_stop_departure_time: null,
                delay_stop_arrival: 29,
                delay: 195,
            });
        });

        it("should return updated position (AFTER_TRACK)", () => {
            const positionToUpdate: Partial<IPositionToUpdate> = {
                state_position: StatePositionEnum.AFTER_TRACK,
                last_stop_sequence: 3,
                last_stop_arrival_time: new Date(1675809281000),
                last_stop_departure_time: null,
                delay: null,
            };

            const position: Partial<IVPTripsPositionAttributes> = {
                tcp_event: TCPEventEnum.TIME,
                is_tracked: false,
                origin_timestamp: new Date(1675809281000 + 195 * 1000),
                scheduled_timestamp: new Date(1675809281000),
            };

            const context: Partial<IVPTripsLastPositionContext> = {
                tripId: "2023-02-07T23:33:00+01:00_234_115_220928_3906",
                lastPositionDelay: null,
                lastPositionLastStop: {
                    id: "U557Z2P",
                    sequence: 3,
                    arrival_time: null,
                    arrival_delay: 29,
                    departure_time: null,
                    departure_delay: null,
                },
                atStopStreak: {
                    firstPositionDelay: null,
                    firstPositionTimestamp: null,
                    stop_sequence: null,
                },
            };

            const resultPositionToUpdate = ComputeDelayHelper.updatePositionToUpdate(
                context as IVPTripsLastPositionContext,
                positionToUpdate as IPositionToUpdate,
                position as IVPTripsPositionAttributes,
                routeType
            );

            expect(resultPositionToUpdate).to.deep.equal({
                state_position: StatePositionEnum.AFTER_TRACK,
                last_stop_sequence: 3,
                last_stop_arrival_time: new Date(1675809281000),
                last_stop_departure_time: null,
                delay_stop_arrival: 29,
                delay: null,
            });
        });
    });
});
