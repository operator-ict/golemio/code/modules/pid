import ComputeDelayHelper from "#ie/vehicle-positions/workers/vehicle-positions/helpers/compute-positions/ComputeDelayHelper";
import { IVPTripsLastPositionContext } from "#sch/vehicle-positions/models/interfaces/VPTripsLastPositionInterfaces";
import { expect } from "chai";
import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";

describe("ComputeDelayHelper - Strategy CommonDelayAtStop", () => {
    const routeType = GTFSRouteTypeEnum.TRAM;
    const context = {
        atStopStreak: { stop_sequence: null, firstPositionTimestamp: 1706012807000, firstPositionDelay: 4 },
        lastPositionLastStop: {
            id: null,
            sequence: 8,
            arrival_time: null,
            arrival_delay: -13,
            departure_time: null,
            departure_delay: 11,
        },
    } as IVPTripsLastPositionContext;

    const position = {
        bearing: 20,
        delay: 9,
        id: "597979267",
        lat: 50.04167,
        lng: 14.40428,
        origin_time: "13:29:52",
        origin_timestamp: new Date("2024-01-23T12:29:52.000Z"),
        scheduled_timestamp: "2024-01-23T12:27:00.000Z",
        shape_dist_traveled: 4.669,
        is_tracked: true,
        is_canceled: null,
        state_position: "on_track",
        state_process: "processed",
        tcp_event: "O",
        last_stop_sequence: 8,
        last_stop_id: "U49Z5P",
        last_stop_arrival_time: "2024-01-23T12:27:00.000Z",
        last_stop_departure_time: "2024-01-23T12:27:00.000Z",
        next_stop_arrival_time: "2024-01-23T12:30:00.000Z",
        next_stop_departure_time: "2024-01-23T12:30:00.000Z",
        next_stop_id: "U147Z5P",
        next_stop_sequence: 9,
        next_stop_name: "Hlubočepy",
        last_stop_name: "Geologická",
        valid_to: "2024-01-23T12:34:52.000Z",
        delay_stop_departure: 172,
        delay_stop_arrival: -13,
    } as any;

    it("should handle two O after each other", () => {
        const resultContext = ComputeDelayHelper.updatePositionToUpdate(
            context,
            {
                last_stop_departure_time: new Date("2024-01-23T12:27:00.000Z"),
            } as any,
            position,
            routeType
        );

        expect(resultContext.delay_stop_departure).to.equal(11);
    });
});
