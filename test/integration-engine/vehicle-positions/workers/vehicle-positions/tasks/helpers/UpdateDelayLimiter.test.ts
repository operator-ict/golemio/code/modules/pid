import { UpdateDelayLimiter } from "#ie/vehicle-positions/workers/vehicle-positions/tasks/helpers/UpdateDelayLimiter";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";

chai.use(chaiAsPromised);

describe("UpdateDelayLimiter", () => {
    let sandbox: SinonSandbox;
    let instance: UpdateDelayLimiter;
    let logger: any;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        logger = {
            warn: sandbox.stub(),
        };

        instance = new UpdateDelayLimiter("test", 1000, logger);
    });

    afterEach(() => {
        sandbox?.restore();
    });

    it("should have name", () => {
        expect(instance["name"]).to.equal("test");
    });

    it("should have queueTtl", () => {
        expect(instance["queueTtl"]).to.equal(1000);
    });

    it("should return true if messageProperties is undefined", () => {
        expect(instance.isActualMessage(undefined)).to.equal(true);
        expect(logger.warn.calledOnce).to.equal(true);
    });

    it("should return true if messageProperties.timestamp is undefined", () => {
        expect(instance.isActualMessage({} as any)).to.equal(true);
        expect(logger.warn.calledOnce).to.equal(true);
    });

    it("should return false if messageProperties.timestamp is older than queueTtl", () => {
        expect(instance.isActualMessage({ timestamp: Date.now() - 1001 } as any)).to.equal(false);
        expect(logger.warn.calledOnce).to.equal(false);
    });

    it("should return true if messageProperties.timestamp is newer than queueTtl", () => {
        expect(instance.isActualMessage({ timestamp: Date.now() } as any)).to.equal(true);
        expect(logger.warn.calledOnce).to.equal(false);
    });

    it("should warn if many iterations was skipped", () => {
        instance["skippedIterations"] = 100;
        expect(instance.isActualMessage({ timestamp: Date.now() - 1001 } as any)).to.equal(false);
        expect(logger.warn.calledOnce).to.equal(true);
    });
});
