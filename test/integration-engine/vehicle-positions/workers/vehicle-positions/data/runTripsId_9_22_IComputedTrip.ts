import { IPropagateDelayInput } from "#ie/vehicle-positions/workers/vehicle-positions/interfaces/IPropagateDelayInput";
import { IVPTripsPositionAttributes } from "#sch/vehicle-positions/models/interfaces/IVPTripsPositionAttributes";
import { StatePositionEnum } from "src/const";

export const runTripsId_9_22_IComputedTrip: IPropagateDelayInput = {
    processedPositions: [
        {
            context: {
                tripId: "2021-12-09T07:02:00+01:00_9_6903_211202_9381",
                lastPositionTracking: {
                    type: "Feature",
                    geometry: {
                        type: "Point",
                        coordinates: [14, 50],
                    },
                    properties: {
                        state_position: StatePositionEnum.AT_STOP,
                        origin_timestamp: new Date(1639034440000),
                        is_tracked: true,
                        delay: 2635,
                        bearing: 10,
                    } as IVPTripsPositionAttributes,
                },
                lastPositionId: "2",
                lastPositionOriginTimestamp: 1639034440000,
                lastPositionCanceled: false,
                lastPositionBeforeTrackDelayed: null,
                lastPositionDelay: 2635,
                lastPositionLastStop: {
                    id: null,
                    sequence: null,
                    arrival_time: null,
                    arrival_delay: null,
                    departure_time: null,
                    departure_delay: null,
                },
                lastPositionState: StatePositionEnum.AT_STOP,
                lastPositionStateChange: null,
                atStopStreak: {
                    firstPositionDelay: null,
                    firstPositionTimestamp: null,
                    stop_sequence: null,
                },
            },
            positions: [],
        },
    ],
    trips: [
        {
            run_number: 22,
            internal_run_number: 22,
            gtfs_trip_id: "9_6903_211202",
            id: "2021-12-09T07:02:00+01:00_9_6903_211202_9381",
            start_timestamp: new Date(1639029720000).toISOString(),
            origin_route_name: "9",
            internal_route_name: "9",
            vehicle_registration_number: 9381,
            gtfs_route_type: 0,
            gtfs_block_id: undefined,
            end_timestamp: new Date(1639032900000).toISOString(),
        },
    ],
};
