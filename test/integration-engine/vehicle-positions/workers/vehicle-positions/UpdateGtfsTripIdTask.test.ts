import {
    IUpdateDelayTripsIdsData,
    IUpdateGTFSTripIdData,
} from "#ie/vehicle-positions/workers/vehicle-positions/data-access/interfaces/TripRepositoryInterfaces";
import { IPositionTransformationResult } from "#ie/vehicle-positions/workers/vehicle-positions/interfaces/TransformationInterfaces";
import { UpdateGtfsTripIdTask } from "#ie/vehicle-positions/workers/vehicle-positions/tasks/UpdateGtfsTripIdTask";
import { BulkSaveRecords } from "@golemio/core/dist/integration-engine";
import { PostgresConnector, RedisConnector } from "@golemio/core/dist/integration-engine/connectors";
import { QueueManager } from "@golemio/core/dist/integration-engine/queueprocessors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonFakeTimers, SinonSandbox, SinonStub } from "sinon";
import { StatePositionEnum, StateProcessEnum } from "src/const";
import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";

chai.use(chaiAsPromised);

describe("VPWorker - UpdateGtfsTripIdTask", () => {
    let sandbox: SinonSandbox;
    let clock: SinonFakeTimers;
    let task: UpdateGtfsTripIdTask;

    before(() => {
        PostgresConnector.connect();
        RedisConnector.connect();
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        clock = sinon.useFakeTimers({
            now: new Date(2021, 2, 17, 10, 0),
            shouldAdvanceTime: true,
        });

        sandbox.stub(QueueManager, "sendMessageToExchange").resolves();

        task = new UpdateGtfsTripIdTask("test.test");

        sandbox.stub(task["tripsRepository"], "save");

        sandbox.stub(task["tripsRepository"], "findGTFSTripId").resolves([
            {
                id: "2022-04-04T10:32:00Z_none_S5_9818",
                gtfs_trip_id: "1345_9818_211213",
                gtfs_block_id: "1305_9818_211213",
                gtfs_route_type: GTFSRouteTypeEnum.TRAIN,
                start_timestamp: new Date().toISOString(),
                end_timestamp: new Date().toISOString(),
            },
            {
                id: "2022-04-04T10:32:00Z_none_S5_9818_gtfs_trip_id_1305_9818_211213",
                gtfs_trip_id: "1305_9818_211213",
                gtfs_block_id: "1305_9818_211213",
                gtfs_route_type: GTFSRouteTypeEnum.TRAIN,
                start_timestamp: new Date().toISOString(),
                end_timestamp: new Date().toISOString(),
            },
        ]);
        sandbox.stub(task["gtfsStopTimesRepository"], "findTripStops").resolves([
            {
                trip_id: "1345_9818_211213",
                trip_stops: [{ stop_id: "a", stop_sequence: 42, cis: 5454396 }],
            },
            {
                trip_id: "1305_9818_211213",
                trip_stops: [{ stop_id: "b", stop_sequence: 24, cis: 5454369 }],
            },
        ]);
    });

    afterEach(() => {
        sandbox?.restore();
        clock?.restore();
    });

    it("should call the correct methods", async () => {
        sandbox.stub(task["positionsRepository"], "bulkSave").resolves();
        await task.consume({
            content: Buffer.from(
                JSON.stringify({
                    trips: [
                        {
                            cis_line_short_name: "S45",
                            id: "2021-02-03T10:23:00Z_none_S45_1573",
                            start_asw_stop_id: null,
                            start_cis_stop_id: 5454396,
                            start_timestamp: 1612347780000,
                            agency_name_real: null,
                            agency_name_scheduled: "ČESKÉ DRÁHY",
                            cis_line_id: "none",
                            cis_trip_number: 1573,
                            origin_route_name: null,
                            run_number: null,
                            vehicle_registration_number: null,
                            vehicle_type_id: 0,
                            wheelchair_accessible: true,
                        },
                    ],
                    positions: [
                        {
                            asw_last_stop_id: null,
                            bearing: null,
                            cis_last_stop_id: 5457066,
                            cis_last_stop_sequence: 15,
                            delay_stop_arrival: 0,
                            delay_stop_departure: 0,
                            is_canceled: false,
                            lat: 50.238575,
                            lng: 14.3130083,
                            origin_time: "11:23:00",
                            origin_timestamp: "2021-02-03T10:23:00.000Z",
                            speed: null,
                            is_tracked: false,
                            trips_id: "2021-02-03T10:23:00Z_none_S45_1573",
                            state_position: StatePositionEnum.UNKNOWN,
                            state_process: StateProcessEnum.INPUT,
                        },
                        {
                            asw_last_stop_id: null,
                            bearing: null,
                            cis_last_stop_id: 5457066,
                            cis_last_stop_sequence: 15,
                            delay_stop_arrival: 0,
                            delay_stop_departure: 0,
                            is_canceled: false,
                            lat: 50.238575,
                            lng: 14.3130083,
                            origin_time: "11:23:00",
                            origin_timestamp: "2021-02-03T10:23:00.000Z",
                            speed: null,
                            is_tracked: false,
                            trips_id: "2021-02-03T10:23:00Z_none_S45_1573_gtfs_trip_id_1345_1573_201213",
                            state_position: StatePositionEnum.UNKNOWN,
                            state_process: StateProcessEnum.INPUT,
                        },
                    ],
                })
            ),
        } as any);

        sandbox.assert.calledOnce(task["tripsRepository"].findGTFSTripId as SinonStub);
        sandbox.assert.calledOnce(task["positionsRepository"].bulkSave as SinonStub);
        sandbox.assert.calledWith(task["positionsRepository"].bulkSave as SinonStub, [
            {
                asw_last_stop_id: null,
                bearing: null,
                cis_last_stop_id: 5457066,
                cis_last_stop_sequence: 15,
                delay_stop_arrival: 0,
                delay_stop_departure: 0,
                is_canceled: false,
                lat: 50.238575,
                lng: 14.3130083,
                origin_time: "11:23:00",
                origin_timestamp: "2021-02-03T10:23:00.000Z",
                speed: null,
                is_tracked: false,
                trips_id: "2021-02-03T10:23:00Z_none_S45_1573",
                state_position: StatePositionEnum.UNKNOWN,
                state_process: StateProcessEnum.INPUT,
            },
            {
                asw_last_stop_id: null,
                bearing: null,
                cis_last_stop_id: 5457066,
                cis_last_stop_sequence: 15,
                delay_stop_arrival: 0,
                delay_stop_departure: 0,
                is_canceled: false,
                lat: 50.238575,
                lng: 14.3130083,
                origin_time: "11:23:00",
                origin_timestamp: "2021-02-03T10:23:00.000Z",
                speed: null,
                is_tracked: false,
                trips_id: "2021-02-03T10:23:00Z_none_S45_1573_gtfs_trip_id_1345_1573_201213",
                state_position: StatePositionEnum.UNKNOWN,
                state_process: StateProcessEnum.INPUT,
            },
        ]);

        sandbox.assert.calledOnce(QueueManager.sendMessageToExchange as SinonStub);
    });

    it("should insert new position for train trip", async () => {
        const inputData = {
            trips: [
                {
                    cis_line_short_name: "none",
                    id: "2022-04-04T10:32:00Z_none_S5_9818",
                    start_asw_stop_id: null,
                    start_cis_stop_id: 5457236,
                    start_cis_stop_platform_code: null,
                    start_timestamp: "1649068320000",
                    agency_name_real: null,
                    agency_name_scheduled: "ČESKÉ DRÁHY",
                    cis_line_id: "none",
                    cis_trip_number: 9818,
                    origin_route_name: null,
                    run_number: null,
                    start_time: "12:32:00",
                    vehicle_registration_number: null,
                    vehicle_type_id: 0,
                    wheelchair_accessible: true,
                },
            ],
            positions: [
                {
                    asw_last_stop_id: null,
                    bearing: null,
                    cis_last_stop_id: 5454396,
                    cis_last_stop_sequence: 21,
                    delay_stop_arrival: 0,
                    delay_stop_departure: null,
                    is_canceled: false,
                    lat: 50.238575,
                    lng: 14.3130083,
                    origin_time: "14:07:00",
                    origin_timestamp: "2022-04-04T12:07:00.000Z",
                    speed: null,
                    state_position: "unknown",
                    state_process: "input",
                    is_tracked: false,
                    trips_id: "2022-04-04T10:32:00Z_none_S5_9818",
                },
            ],
        };

        const saveStub = sandbox.stub(task["positionsRepository"], "bulkSave").resolves();
        await task.consume({
            content: Buffer.from(JSON.stringify(inputData)),
        } as any);

        const inserted: BulkSaveRecords<any> = saveStub.getCall(0).args[0];
        expect(inserted[1].trips_id).to.equal("2022-04-04T10:32:00Z_none_S5_9818_gtfs_trip_id_1305_9818_211213");
        expect(inserted[1].is_tracked).to.equal(false);
    });

    // =============================================================================
    // generateAndSaveScheduledTrips
    // =============================================================================
    describe("generateAndSaveScheduledTrips", () => {
        it("should generate and save scheduled trips for the BB1 route", async () => {
            const tripRunManagerStub = sandbox.stub(task["gtfsTripRunManager"], "setAndReturnScheduledTrips").resolves();
            const tripIdToPositionDict: Record<string, Partial<IPositionTransformationResult>> = {
                "2023-03-09T13:07:00Z_103123_BB1_1095": {
                    origin_timestamp: new Date(1678367267000),
                },
            };

            const inputTrips: Array<Partial<IUpdateGTFSTripIdData>> = [
                {
                    id: "2023-03-09T13:07:00Z_103123_BB1_1095",
                    origin_route_name: "115",
                    run_number: 91,
                },
            ];

            await task["generateAndSaveScheduledTrips"](
                tripIdToPositionDict as Record<string, IPositionTransformationResult>,
                inputTrips as IUpdateGTFSTripIdData[],
                []
            );

            expect(tripRunManagerStub.calledOnce).to.be.true;
            expect(tripRunManagerStub.getCall(0).args[0]).to.deep.equal({
                route_id: "115",
                run_number: 91,
                msg_last_timestamp: "2023-03-09T13:07:47.000Z",
            });
        });

        it("should generate and save scheduled trips for the BB1 route (corrected route id and run number)", async () => {
            const tripRunManagerStub = sandbox.stub(task["gtfsTripRunManager"], "setAndReturnScheduledTrips").resolves();
            const tripIdToPositionDict: Record<string, Partial<IPositionTransformationResult>> = {
                "2023-03-09T13:07:00Z_103123_BB1_1095": {
                    origin_timestamp: new Date(1678367267000),
                },
            };

            const inputTrips: Array<Partial<IUpdateGTFSTripIdData>> = [
                {
                    id: "2023-03-09T13:07:00Z_103123_BB1_1095",
                    origin_route_name: "115",
                    run_number: 91,
                },
            ];

            const existingTrips: Array<Partial<IUpdateDelayTripsIdsData>> = [
                {
                    id: "2023-03-09T13:07:00Z_103123_BB1_1095",
                    internal_route_name: "1703",
                    internal_run_number: 90, // in reality, this is also 91, but we want to explicitly test the correction
                },
            ];

            await task["generateAndSaveScheduledTrips"](
                tripIdToPositionDict as Record<string, IPositionTransformationResult>,
                inputTrips as IUpdateGTFSTripIdData[],
                existingTrips as IUpdateDelayTripsIdsData[]
            );

            expect(tripRunManagerStub.calledOnce).to.be.true;
            expect(tripRunManagerStub.getCall(0).args[0]).to.deep.equal({
                route_id: "1703",
                run_number: 90,
                msg_last_timestamp: "2023-03-09T13:07:47.000Z",
            });
        });
    });
});
