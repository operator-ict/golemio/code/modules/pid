import { IMetroRunInputForProcessing } from "#ie/vehicle-positions/workers/runs/interfaces/IMetroRunInputForProcessing";
import { IProcessRegionalBusRunMessage } from "#ie/vehicle-positions/workers/runs/interfaces/IProcessRegionalBusRunMessagesInput";
import TripsIdGenerator from "#ie/vehicle-positions/workers/vehicle-positions/data-access/helpers/TripsIdGenerator";
import { ICommonRunsModel } from "#sch/vehicle-positions/models/interfaces/ICommonRunsModel";
import { IScheduleDto } from "#sch/vehicle-positions/redis/interfaces/IGtfsRunTripCacheDto";
import { expect } from "chai";

describe("TripsIdGenerator", () => {
    it("should generate id from common run", () => {
        const gtfsTrip: Partial<IScheduleDto> = {
            start_timestamp: "2022-11-04T12:31:00+01:00",
            trip_id: "10_4873_220926",
        };

        const runInput: Partial<ICommonRunsModel> = {
            registration_number: "8215",
        };

        const tripsId = TripsIdGenerator.generateFromCommonRun(gtfsTrip as IScheduleDto, runInput as ICommonRunsModel);
        expect(tripsId).to.equal("2022-11-04T12:31:00+01:00_10_4873_220926_8215");
    });

    it("should generate id from common run not public", () => {
        const runInput: Partial<ICommonRunsModel> = {
            route_id: "67",
            run_number: 1,
            msg_start_timestamp: "2021-06-28T15:05:00+02:00",
            registration_number: "5572",
        };

        const tripsId = TripsIdGenerator.generateFromCommonRunNotPublic(runInput as ICommonRunsModel);
        expect(tripsId).to.equal("NOT_PUBLIC_67_1_2021-06-28T15:05:00+02:00_5572");
    });

    it("should generate id from metro run", () => {
        const gtfsTrip: Partial<IScheduleDto> = {
            start_timestamp: "2022-11-04T12:37:35+01:00",
            trip_id: "991_416_220901",
            run_number: 12,
        };

        const runInput: Partial<IMetroRunInputForProcessing> = {
            trainSetNumberReal: "13",
        };

        const tripsId = TripsIdGenerator.generateFromMetroRun(gtfsTrip as IScheduleDto, runInput as IMetroRunInputForProcessing);

        expect(tripsId).to.equal("2022-11-04T12:37:35+01:00_991_416_220901_12_13");
    });

    it("should generate id from regional bus run", () => {
        const gtfsTrip: Partial<IScheduleDto> = {
            start_timestamp: "2023-09-26T05:46:00+02:00",
            trip_id: "341_371_230904",
        };

        const runInput: Partial<IProcessRegionalBusRunMessage> = {
            external_trip_id: "867377023797370",
        };

        const tripsId = TripsIdGenerator.generateFromRegionalBusRun(
            gtfsTrip as IScheduleDto,
            runInput as IProcessRegionalBusRunMessage
        );

        expect(tripsId).to.equal("2023-09-26T05:46:00+02:00_341_371_230904_867377023797370");
    });
});
