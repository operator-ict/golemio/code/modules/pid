import { TripsRepository } from "#ie/vehicle-positions/workers/vehicle-positions/data-access/TripsRepository";
import TripsMapper from "#ie/vehicle-positions/workers/vehicle-positions/data-access/helpers/TripsMapper";
import {
    IFoundGTFSTripData,
    IUpdateDelayTripsIdsData,
    IUpdateGTFSTripIdData,
} from "#ie/vehicle-positions/workers/vehicle-positions/data-access/interfaces/TripRepositoryInterfaces";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import moment from "@golemio/core/dist/shared/moment-timezone";
import Sequelize, { Filterable, WhereAttributeHash, WhereAttributeHashValue } from "@golemio/core/dist/shared/sequelize";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonFakeTimers, SinonSandbox, SinonStub } from "sinon";
import {
    deduceTimestampsCESTToCETTestData,
    deduceTimestampsCETToCESTTestData,
} from "../data/TripsRepository-deduceTimestamps-dst-change";

chai.use(chaiAsPromised);

describe("TripsRepository", () => {
    let repository: TripsRepository;
    let sandbox: SinonSandbox;
    let clock: SinonFakeTimers;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        clock = sinon.useFakeTimers({
            now: moment().tz("Europe/Prague").hours(10).minutes(39).toDate(),
            shouldAdvanceTime: true,
        });
        PostgresConnector.connect();
        repository = new TripsRepository();
        sandbox.stub(repository, "save");
        sandbox.stub(repository, "update");
    });

    afterEach(() => {
        sandbox.restore();
        clock.restore();
    });

    it("findGTFSTripIdBasic should return valid result", async () => {
        const data = await repository["findGTFSTripIdBasic"]({
            cis_line_short_name: "152",
            id: "2019-05-26T09:13:00Z_XX_152_60_201230",
            start_asw_stop_id: null,
            start_cis_stop_id: 47178,
            start_cis_stop_platform_code: "B",
            start_timestamp: moment().tz("Europe/Prague").hours(11).minutes(39).startOf("minute").toISOString(),
            agency_name_real: "ARRIVA City",
            agency_name_scheduled: "ARRIVA City",
            cis_line_id: "100152",
            cis_trip_number: 95,
            origin_route_name: "433",
            run_number: 39,
            start_time: "11:39:00",
            vehicle_registration_number: 8581,
            vehicle_type_id: 3,
            wheelchair_accessible: true,
        } as IUpdateGTFSTripIdData);
        expect(data[0].gtfs_trip_id).to.be.equal("152_60_201230");
        expect(data[0].min_stop_time).to.be.equal("11:39:00");
        expect(data[0].max_stop_time).to.be.equal("11:58:00");
    });

    it("findGTFSTripId should save gtfs_trip_id", async () => {
        const data = (await repository["findGTFSTripId"]({
            cis_line_short_name: "152",
            id: "2019-05-26T09:13:00Z_XX_152_60_201230",
            start_asw_stop_id: null,
            start_cis_stop_id: 47178,
            start_cis_stop_platform_code: "B",
            start_timestamp: moment().tz("Europe/Prague").hours(11).minutes(39).startOf("minute").toISOString(),
            agency_name_real: "ARRIVA City",
            agency_name_scheduled: "ARRIVA City",
            cis_line_id: "100152",
            cis_trip_number: 95,
            origin_route_name: "433",
            run_number: 39,
            start_time: "11:39:00",
            vehicle_registration_number: 8581,
            vehicle_type_id: 3,
            wheelchair_accessible: true,
        } as IUpdateGTFSTripIdData)) as IUpdateDelayTripsIdsData[];
        expect(data[0].id).to.be.equal("2019-05-26T09:13:00Z_XX_152_60_201230");
        sandbox.assert.calledOnce(repository.update as SinonStub);
    });

    it("findGTFSTripIdsTrain should find 1 gtfs_trip_id", async () => {
        const data = (await repository["findGTFSTripIdsTrain"]({
            cis_line_id: "none",
            cis_trip_number: 9948,
            run_number: null,
            cis_line_short_name: "S7",
            created_at: "2021-06-24 15:08:38.638+02",
            id: "2021-06-24T13:01:00Z_none_S7_9948",
            updated_at: "2021-06-24 15:48:41.054+02",
            start_cis_stop_id: 5457076,
            start_cis_stop_platform_code: "2J/7J",
            start_time: null,
            start_timestamp: new Date(1624539660000).toISOString(),
            vehicle_type_id: 0,
            wheelchair_accessible: true,
            agency_name_scheduled: "ČESKÉ DRÁHY",
            origin_route_name: null,
            agency_name_real: null,
            vehicle_registration_number: null,
            start_asw_stop_id: null,
            last_position_id: null,
            is_canceled: false,
            end_timestamp: null,
        } as IUpdateGTFSTripIdData)) as IFoundGTFSTripData[];
        expect(data[0].gtfs_trip_id).to.be.equal("1307_9948_210406");
    });

    it("findGTFSTripIdsTrain should find 2 gtfs_trip_id", async () => {
        const data = (await repository["findGTFSTripIdsTrain"]({
            cis_line_id: "none",
            cis_trip_number: 6910,
            run_number: null,
            cis_line_short_name: "U4",
            created_at: "2021-06-25 09:42:37.067+02",
            id: "2021-06-25T07:42:00Z_none_U4_6910",
            updated_at: "2021-06-25 11:57:23.995+02",
            start_cis_stop_id: 5457236,
            start_cis_stop_platform_code: "4P/4P",
            start_time: null,
            start_timestamp: new Date(1624606920000).toISOString(),
            vehicle_type_id: 0,
            wheelchair_accessible: true,
            agency_name_scheduled: "ČESKÉ DRÁHY",
            origin_route_name: null,
            agency_name_real: null,
            vehicle_registration_number: null,
            start_asw_stop_id: null,
            last_position_id: null,
            is_canceled: false,
            end_timestamp: null,
        } as IUpdateGTFSTripIdData)) as IFoundGTFSTripData[];
        expect(data[0].gtfs_trip_id).to.be.equal("1004_6910_201214");
        expect(data[1].gtfs_trip_id).to.be.equal("1304_6910_201214");
    });

    it("upsertCommonRunTrip should upsert and return plain record", async () => {
        const getRecordStub = sandbox.stub().returns("done");
        sandbox.stub(repository["sequelizeModel"], "upsert").resolves([{ get: getRecordStub }] as any);
        sandbox.stub(TripsMapper, "mapCommonRunToDto");

        const result = await repository.upsertCommonRunTrip({ run: "dummy1" } as any, "dummy2" as any);
        expect(result).to.equal("done");
    });

    it("upsertCommonRunTrip should throw", async () => {
        sandbox.stub(repository["sequelizeModel"], "upsert").rejects();
        sandbox.stub(TripsMapper, "mapCommonRunToDto");

        const promise = repository.upsertCommonRunTrip({ run: "dummy1" } as any, "dummy2" as any);
        await expect(promise).to.be.rejectedWith(GeneralError, "upsertCommonRunTrip: Error while saving to database.");
    });

    it("upsertMetroRunTrip should upsert and return plain record", async () => {
        const getRecordStub = sandbox.stub().returns("done");
        sandbox.stub(repository["sequelizeModel"], "upsert").resolves([{ get: getRecordStub }] as any);
        sandbox.stub(TripsMapper, "mapMetroRunToDto");

        const result = await repository.upsertMetroRunTrip("dummy1" as any, "dummy2" as any);
        expect(result).to.equal("done");
    });

    it("upsertMetroRunTrip should throw", async () => {
        sandbox.stub(repository["sequelizeModel"], "upsert").rejects();
        sandbox.stub(TripsMapper, "mapMetroRunToDto");

        const promise = repository.upsertMetroRunTrip("dummy1" as any, "dummy2" as any);
        await expect(promise).to.be.rejectedWith(GeneralError, "upsertMetroRunTrip: Error while saving to database.");
    });

    describe("deduceTimestamps", () => {
        it("normal case", async () => {
            const data = repository["deduceTimestamps"](
                "10:20:00",
                "11:34:00",
                new Date("2021-05-19 10:20:00+02:00").toISOString()
            );

            expect(data.startTimestamp.toISOString()).to.be.equal("2021-05-19T08:20:00.000Z");
            expect(data.endTimestamp.toISOString()).to.be.equal("2021-05-19T09:34:00.000Z");
        });

        it("23+ hours end case", async () => {
            const data = repository["deduceTimestamps"](
                "23:20:00",
                "24:34:00",
                new Date("2021-05-19 23:20:00+02:00").toISOString()
            );

            expect(data.startTimestamp.toISOString()).to.be.equal("2021-05-19T21:20:00.000Z");
            expect(data.endTimestamp.toISOString()).to.be.equal("2021-05-19T22:34:00.000Z");
        });

        it("23+ hours all case", async () => {
            const data = repository["deduceTimestamps"](
                "25:20:00",
                "26:34:00",
                new Date("2021-05-19 01:20:00+02:00").toISOString()
            );

            expect(moment(+data.startTimestamp).toISOString()).to.be.equal(moment("2021-05-19 01:20:00+02:00").toISOString());
            expect(moment(+data.endTimestamp).toISOString()).to.be.equal(moment("2021-05-19 02:34:00+02:00").toISOString());
        });

        it("23+ hours all case before CET->CEST day", async () => {
            const data = repository["deduceTimestamps"](
                "25:20:00",
                "29:34:00",
                new Date("2021-03-28 01:20:00+01:00").toISOString()
            );

            expect(moment(+data.startTimestamp).toISOString()).to.be.equal(moment("2021-03-28 01:20:00+01:00").toISOString());
            expect(moment(+data.endTimestamp).toISOString()).to.be.equal(moment("2021-03-28 06:34:00+02:00").toISOString());
        });

        it("23+ hours all case after CET->CEST day", async () => {
            const data = repository["deduceTimestamps"](
                "25:20:00",
                "26:34:00",
                new Date("2021-03-28 22:20:00+02:00").toISOString()
            );

            expect(moment(+data.startTimestamp).toISOString()).to.be.equal(moment("2021-03-29 01:20:00+02:00").toISOString());
            expect(moment(+data.endTimestamp).toISOString()).to.be.equal(moment("2021-03-29 02:34:00+02:00").toISOString());
        });

        const dstChangeTests = [
            {
                data: deduceTimestampsCETToCESTTestData,
                getTitle: (originStartTimestamp: string, minStopTime: string, maxStopTime: string) =>
                    `should return correct timestamps for origin ${originStartTimestamp}, ${minStopTime}-${maxStopTime}` +
                    ` min-max stop time (around CET -> CEST)`,
            },
            {
                data: deduceTimestampsCESTToCETTestData,
                getTitle: (originStartTimestamp: string, minStopTime: string, maxStopTime: string) =>
                    `should return correct timestamps for origin ${originStartTimestamp}, ${minStopTime}-${maxStopTime}` +
                    ` min-max stop time (around CEST -> CET)`,
            },
        ];

        dstChangeTests.forEach(({ data, getTitle }) => {
            data.forEach(({ minStopTime, maxStopTime, originStartTimestamp, correctStartTimestamp, correctEndTimestamp }) => {
                it(getTitle(originStartTimestamp, minStopTime, maxStopTime), async () => {
                    const { startTimestamp, endTimestamp } = repository["deduceTimestamps"](
                        minStopTime,
                        maxStopTime,
                        originStartTimestamp
                    );

                    expect(startTimestamp.toISOString()).to.equal(correctStartTimestamp);
                    expect(endTimestamp.toISOString()).to.equal(correctEndTimestamp);
                });
            });
        });
    });

    describe("findAllForPublicCache", () => {
        it("should call findAll with valid_to constraint", async () => {
            const findAllStub = sandbox.stub(repository["sequelizeModel"], "findAll").resolves([]);
            const startDateTime = new Date();
            await repository.findAllForPublicCache(startDateTime);
            sandbox.assert.calledOnce(findAllStub);

            const includes = findAllStub.args[0][0]!["include"]! as Filterable[];
            const wheres: WhereAttributeHashValue<any> = (includes[0].where as WhereAttributeHash<{ valid_to: object }>).valid_to;
            const firstValue = wheres[Sequelize.Op.or][0];
            const secondValue = wheres[Sequelize.Op.or][1][Sequelize.Op.gte];

            expect(firstValue).to.be.null;
            expect(secondValue).to.deep.equal(startDateTime);
        });
    });
});
