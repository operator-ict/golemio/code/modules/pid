import { SaveDataToDBTask } from "#ie/vehicle-positions/workers/vehicle-positions/tasks/SaveDataToDBTask";
import { PostgresConnector, RedisConnector } from "@golemio/core/dist/integration-engine/connectors";
import { QueueManager } from "@golemio/core/dist/integration-engine/queueprocessors";
import { DateTime } from "@golemio/core/dist/shared/luxon";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import sinon, { SinonFakeTimers, SinonSandbox, SinonStub } from "sinon";

chai.use(chaiAsPromised);

describe("VPWorker - SaveDataToDBTask", () => {
    let sandbox: SinonSandbox;
    let clock: SinonFakeTimers;
    let task: SaveDataToDBTask;

    beforeEach(async () => {
        sandbox = sinon.createSandbox();

        sandbox.stub(RedisConnector, "getConnection");
        sandbox.stub(QueueManager, "sendMessageToExchange").resolves();

        await PostgresConnector.connect();
        task = new SaveDataToDBTask("test.test");
    });

    afterEach(() => {
        sandbox?.restore();
        clock?.restore();
    });

    it("should call the correct methods", async () => {
        clock = sinon.useFakeTimers({
            now: new Date(2021, 2, 17, 10, 0),
            shouldAdvanceTime: true,
        });

        sandbox
            .stub(task["messageTransformation"], "transform")
            .callsFake(() => Object.assign({ positions: [], stops: [], trips: [] }));

        sandbox.stub(task["tripsRepository"], "bulkSave").resolves([
            {
                cis_short_name: "999",
                id: "999",
                start_cis_stop_id: "999",
                start_cis_stop_platform_code: "a",
                start_timestamp: null,
                get(): string {
                    return new Date().toISOString();
                },
            },
        ] as any);

        sandbox.stub(task["cisStopRepository"], "bulkSave").resolves([] as any);

        await task.consume({
            content: fs.readFileSync(__dirname + "/./data/vehiclepositions-input.json"),
        } as any);

        sandbox.assert.calledOnce(task["messageTransformation"].transform as SinonStub);
        sandbox.assert.calledOnce(task["tripsRepository"].bulkSave as SinonStub);
        sandbox.assert.calledWith(task["tripsRepository"].bulkSave as SinonStub, [], ["updated_at"], true);
        sandbox.assert.calledOnce(task["cisStopRepository"].bulkSave as SinonStub);
        sandbox.assert.calledOnce(QueueManager.sendMessageToExchange as SinonStub);
        sandbox.assert.callOrder(
            task["messageTransformation"].transform as SinonStub,
            task["tripsRepository"].bulkSave as SinonStub,
            QueueManager.sendMessageToExchange as SinonStub
        );
    });

    it("should properly transform/save trips and positions, and send them to RMQ", async () => {
        const todayYMD = new Date().toISOString().split("T")[0];
        const inputData = {
            m: {
                $: {
                    disp: "http://77.93.194.81:8716/api",
                },
                spoj: [
                    {
                        $: {
                            lin: "100110",
                            alias: "110",
                            spoj: "88",
                            dopr: "Jaroslav Štěpánek",
                            doprSkut: "Jaroslav Štěpánek",
                            kmenl: "110",
                            t: "3",
                            sled: "0",
                            np: "true",
                            zrus: "false",
                            vuzevc: "1030",
                            lat: "50.16252",
                            lng: "14.52483",
                            azimut: "-10",
                            rychl: "0",
                            cpoz: "07:00:00",
                            po: "1",
                            zast: "28010",
                            zpoz_prij: "174",
                        },
                        zast: [
                            {
                                $: {
                                    zast: "55186",
                                    stan: "C",
                                    prij: "",
                                    odj: "06:15",
                                    zpoz_typ_prij: "0",
                                    zpoz_typ: "3",
                                    zpoz_odj: "50",
                                },
                            },
                            {
                                $: {
                                    zast: "28010",
                                    stan: "C",
                                },
                            },
                            {
                                $: {
                                    zast: "28012",
                                    stan: "D",
                                },
                            },
                        ],
                    },
                ],
            },
        };

        const expectedStartTimestamp = DateTime.fromFormat(todayYMD, "yyyy-MM-dd", { zone: "Europe/Prague" })
            .set({ hour: 6, minute: 15 })
            .toJSDate();

        const expectedTripsId =
            DateTime.fromFormat(todayYMD, "yyyy-MM-dd", { zone: "Europe/Prague" })
                .set({ hour: 6, minute: 15 })
                .toJSDate()
                .toISOString()
                .split(".")[0] + "Z_100110_110_88";

        const expectedPosition = {
            asw_last_stop_id: null,
            bearing: 246,
            cis_last_stop_id: 28010,
            cis_last_stop_sequence: null,
            delay_stop_arrival: 174,
            delay_stop_departure: null,
            is_canceled: false,
            lat: 50.16252,
            lng: 14.52483,
            origin_time: "07:00:00",
            origin_timestamp: DateTime.fromFormat(todayYMD, "yyyy-MM-dd", { zone: "Europe/Prague" }).set({ hour: 7 }).toJSDate(),
            speed: 0,
            state_position: "unknown",
            state_process: "input",
            is_tracked: false,
            trips_id: expectedTripsId,
        };

        await task.consume({
            content: Buffer.from(JSON.stringify(inputData)),
        } as any);

        await task.consume({
            content: Buffer.from(JSON.stringify(inputData)),
        } as any);

        expect((QueueManager.sendMessageToExchange as SinonStub).callCount).to.equal(2);
        expect((QueueManager.sendMessageToExchange as SinonStub).getCall(0).args).to.deep.equal([
            "test.test",
            "updateGTFSTripId",
            {
                trips: [
                    {
                        cis_line_short_name: "110",
                        id: expectedTripsId,
                        start_asw_stop_id: null,
                        start_cis_stop_id: 55186,
                        start_cis_stop_platform_code: "C",
                        start_timestamp: expectedStartTimestamp.toISOString(),
                        agency_name_real: "Jaroslav Štěpánek",
                        agency_name_scheduled: "Jaroslav Štěpánek",
                        cis_line_id: "100110",
                        cis_trip_number: 88,
                        origin_route_name: "110",
                        run_number: 1,
                        start_time: "06:15:00",
                        vehicle_registration_number: 1030,
                        vehicle_type_id: 3,
                        wheelchair_accessible: true,
                    },
                ],
                positions: [expectedPosition],
            },
            {
                timestamp: undefined,
            },
        ]);

        expect((QueueManager.sendMessageToExchange as SinonStub).getCall(1).args).to.deep.equal([
            "test.test",
            "updateDelay",
            {
                positions: [expectedPosition],
                updatedTrips: [
                    {
                        id: expectedTripsId,
                        gtfs_trip_id: null,
                        gtfs_block_id: null,
                        gtfs_route_type: null,
                        start_timestamp: expectedStartTimestamp,
                        end_timestamp: null,
                        run_number: 1,
                        internal_run_number: null,
                        origin_route_name: "110",
                        internal_route_name: null,
                        vehicle_registration_number: 1030,
                    },
                ],
            },
            {
                timestamp: undefined,
            },
        ]);
    });
});
