import { VPContainer } from "#ie/vehicle-positions/ioc/Di";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { RefreshPublicStopTimeCacheTask } from "#ie/vehicle-positions/workers/vehicle-positions/tasks/RefreshPublicStopTimeCacheTask";
import { IPublicStopTimeDto } from "#sch/vehicle-positions/models/views/interfaces/IPublicStopTimeDto";
import { PUBLIC_STOP_TIME_CACHE_NAMESPACE_PREFIX } from "#sch/vehicle-positions/redis/const";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { IoRedisConnector } from "@golemio/core/dist/helpers/data-access/redis/IoRedisConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";
import { GTFSRouteTypeEnum } from "#helpers/RouteTypeEnums";
import { ProviderSourceTypeEnum } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/ProviderSourceTypeEnum";
import { RefreshPublicTripCacheTask } from "#ie/vehicle-positions/workers/vehicle-positions/tasks/RefreshPublicTripCacheTask";

describe("RefreshPublicTripCacheTask", () => {
    let sandbox: SinonSandbox;
    let task: RefreshPublicTripCacheTask;

    before(async () => {
        const postgresConnector = VPContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector);
        await postgresConnector.connect();

        const redisConnector = VPContainer.resolve<IoRedisConnector>(ContainerToken.RedisConnector);
        await redisConnector.connect();
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        task = VPContainer.resolve(VPContainerToken.RefreshPublicTripCacheTask);

        task["refreshInterval"] = 0;
        task["createMutex"] = () =>
            ({
                tryAcquire: sandbox.stub().resolves(true),
                release: sandbox.stub().resolves(),
            } as any);
    });

    afterEach(() => {
        sandbox?.restore();
    });

    it("should update public canceled trip cache", async () => {
        const trips = [
            {
                id: "2024-11-18T06:46:00Z_100103_103_1048",
                gtfs_trip_id: "103_1237_241029",
                gtfs_route_type: 3,
                gtfs_route_short_name: "103",
                cis_trip_number: 1048,
                lat: "0",
                lng: "0",
                bearing: null,
                delay: null,
                state_position: "canceled",
                created_at: new Date("2024-11-18 07:04:19.503"),
                wheelchair_accessible: false,
                run_number: 6,
                internal_run_number: 6,
                vehicle_registration_number: null,
                agency_name_scheduled: "ČSAD SČ (Mělník)",
                agency_name_real: null,
                origin_route_name: "103",
                gtfs_shape_id: "L103V3",
                gtfs_trip_headsign: "Ládví",
                provider_source_type: "1",
                shape_dist_traveled: null,
                last_stop_sequence: null,
                origin_timestamp: new Date("2024-11-18 07:33:28.000"),
                last_stop_headsign: null,
            },
        ];

        task["tripRepository"] = {
            findAllForPublicCache: sandbox.stub().resolves(trips),
        } as any;

        const geoaddSpy = sinon.spy(task["publicApiCacheRepository"], "geoadd");

        await task["execute"]();
        const newSetKey = await geoaddSpy.returnValues[0];

        const connection = VPContainer.resolve<IoRedisConnector>(ContainerToken.RedisConnector).getConnection();
        const cache = await connection.get(`${newSetKey}:canceled-trips-103_1237_241029`);

        expect(cache).not.to.be.null;
        expect(JSON.parse(cache!)).to.deep.equal({
            gtfs_trip_id: "103_1237_241029",
            route_type: 3,
            gtfs_route_short_name: "103",
            lat: 0,
            lng: 0,
            bearing: null,
            delay: null,
            state_position: "canceled",
            vehicle_id: "service-3-null",
            detailed_info: {
                is_wheelchair_accessible: false,
                origin_route_name: "103",
                shape_id: "L103V3",
                run_number: 6,
                trip_headsign: "Ládví",
                shape_dist_traveled: null,
                last_stop_sequence: null,
                origin_timestamp: "2024-11-18T07:33:28.000Z",
                registration_number: null,
                operator: "ČSAD SČ (Mělník)",
            },
        });
    });
});
