import { VPContainer } from "#ie/vehicle-positions/ioc/Di";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { RefreshPublicStopTimeCacheTask } from "#ie/vehicle-positions/workers/vehicle-positions/tasks/RefreshPublicStopTimeCacheTask";
import { IPublicStopTimeDto } from "#sch/vehicle-positions/models/views/interfaces/IPublicStopTimeDto";
import { PUBLIC_STOP_TIME_CACHE_NAMESPACE_PREFIX } from "#sch/vehicle-positions/redis/const";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { IoRedisConnector } from "@golemio/core/dist/helpers/data-access/redis/IoRedisConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";
import { GTFSRouteTypeEnum } from "#helpers/RouteTypeEnums";
import { ProviderSourceTypeEnum } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/ProviderSourceTypeEnum";

describe("RefreshPublicStopTimeCacheTask", () => {
    let sandbox: SinonSandbox;
    let task: RefreshPublicStopTimeCacheTask;

    before(async () => {
        const postgresConnector = VPContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector);
        await postgresConnector.connect();

        const redisConnector = VPContainer.resolve<IoRedisConnector>(ContainerToken.RedisConnector);
        await redisConnector.connect();
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        task = VPContainer.resolve(VPContainerToken.RefreshPublicStopTimeCacheTask);

        task["refreshInterval"] = 0;
        task["createMutex"] = () =>
            ({
                tryAcquire: sandbox.stub().resolves(true),
                release: sandbox.stub().resolves(),
            } as any);
    });

    afterEach(() => {
        sandbox?.restore();
    });

    it("should update public stop time cache", async () => {
        const stopTimes: IPublicStopTimeDto[] = [
            {
                rt_trip_id: "test_trip_id_1",
                gtfs_trip_id: "test_trip_1",
                stop_id: "test_stop_1a",
                stop_sequence: 1,
                stop_arr_delay: 0,
                stop_dep_delay: 5,
                cis_stop_platform_code: "1",
                platform_code: "2",
                gtfs_route_type: GTFSRouteTypeEnum.METRO,
                gtfs_route_short_name: "A",
                run_number: 22,
                internal_run_number: 22,
                provider_source_type: ProviderSourceTypeEnum.TcpMetro,
                cis_trip_number: null,
                vehicle_registration_number: 123,
            },
            {
                rt_trip_id: "test_trip_id_1",
                gtfs_trip_id: "test_trip_1",
                stop_id: "test_stop_1b",
                stop_sequence: 2,
                stop_arr_delay: 2,
                stop_dep_delay: 10,
                cis_stop_platform_code: null,
                platform_code: "2",
                gtfs_route_type: GTFSRouteTypeEnum.METRO,
                gtfs_route_short_name: "A",
                run_number: 22,
                internal_run_number: 22,
                provider_source_type: ProviderSourceTypeEnum.TcpMetro,
                cis_trip_number: null,
                vehicle_registration_number: 123,
            },
            {
                rt_trip_id: "test_trip_id_2",
                gtfs_trip_id: "test_trip_2",
                stop_id: "test_stop_2a",
                stop_sequence: 1,
                stop_arr_delay: 4,
                stop_dep_delay: 12,
                cis_stop_platform_code: "1",
                platform_code: null,
                gtfs_route_type: GTFSRouteTypeEnum.BUS,
                gtfs_route_short_name: "A",
                run_number: 5,
                internal_run_number: 5,
                provider_source_type: ProviderSourceTypeEnum.TcpRegionalBus,
                cis_trip_number: 1083,
                vehicle_registration_number: 123,
            },
        ];

        task["publicStopTimeRepository"] = {
            findAll: sandbox.stub().resolves(stopTimes),
        } as any;

        await task["execute"]();

        const connection = VPContainer.resolve<IoRedisConnector>(ContainerToken.RedisConnector).getConnection();
        const cache = await connection.mget(
            `${PUBLIC_STOP_TIME_CACHE_NAMESPACE_PREFIX}:metro-A-22-22-test_trip_1`,
            `${PUBLIC_STOP_TIME_CACHE_NAMESPACE_PREFIX}:service-3-2-test_trip_2`
        );

        expect(JSON.parse(cache[0] as string)).to.deep.equal([
            {
                stop_id: "test_stop_1a",
                sequence: 1,
                arr_delay: 0,
                dep_delay: 5,
                cis_stop_platform_code: "1",
                platform_code: "2",
            },
            {
                stop_id: "test_stop_1b",
                sequence: 2,
                arr_delay: 2,
                dep_delay: 10,
                cis_stop_platform_code: null,
                platform_code: "2",
            },
        ]);

        expect(JSON.parse(cache[1] as string)).to.deep.equal([
            {
                stop_id: "test_stop_2a",
                sequence: 1,
                arr_delay: 4,
                dep_delay: 12,
                cis_stop_platform_code: "1",
                platform_code: null,
            },
        ]);
    });
});
