import { IUpdateRunsGtfsTripInput } from "#ie/vehicle-positions/workers/vehicle-positions/interfaces/IUpdateRunsGtfsTripInput";
import { UpdateRunsGtfsTripIdTask } from "#ie/vehicle-positions/workers/vehicle-positions/tasks/UpdateRunsGtfsTripIdTask";
import { PositionDto } from "#sch/vehicle-positions/models/PositionDto";
import { IPositionDto } from "#sch/vehicle-positions/models/interfaces/IPositionDto";
import { IVPTripsModel } from "#sch/vehicle-positions/models/interfaces/IVPTripsModel";
import { IScheduleDto } from "#sch/vehicle-positions/redis/interfaces/IGtfsRunTripCacheDto";
import { PostgresConnector, RedisConnector } from "@golemio/core/dist/integration-engine/connectors";
import { QueueManager } from "@golemio/core/dist/integration-engine/queueprocessors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import sinon, { SinonFakeTimers, SinonSandbox, SinonStub } from "sinon";
import { StatePositionEnum, StateProcessEnum, TCPEventEnum } from "src/const";
import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";
import { sourceDataTransformed } from "../runs/data/vehiclepositions-runs-transformed";

chai.use(chaiAsPromised);

describe("VPWorker - UpdateRunsGtfsTripIdTask", () => {
    let sandbox: SinonSandbox;
    let sequelizeModelStub: Record<string, SinonStub>;
    let testData: Record<string, any>;
    let clock: SinonFakeTimers;
    let modelRunsStub: SinonStub;
    let runTripsModelGetStub: SinonStub;
    let scheduledTripsData: IScheduleDto[];
    let cachedRunsTripsData: { schedule: IScheduleDto[] };
    let modelTripsUpsertRunTripStub: SinonStub;
    let modelPositionsUpsertRunPositionStub: SinonStub;
    let schedule: any, tramSchedule: any;
    let task: UpdateRunsGtfsTripIdTask;

    beforeEach(() => {
        testData = {
            inserted: [
                {
                    cis_short_name: "999",
                    id: "999",
                    start_cis_stop_id: "999",
                    start_cis_stop_platform_code: "a",
                    start_timestamp: null,
                },
            ],
            updated: [],
        };

        schedule = [
            {
                trip_id: "177_1352_210924",
                route_type: GTFSRouteTypeEnum.BUS,
                // ...
                start_timestamp: "2021-10-07T12:07:00+02:00",
                end_timestamp: "2021-10-07T12:21:00+02:00",
            },
            {
                trip_id: "177_1352_210925",
                route_type: GTFSRouteTypeEnum.BUS,
                // ...
                start_timestamp: "2021-10-07T12:27:00+02:00",
                end_timestamp: "2021-10-07T12:41:00+02:00",
            },
        ];

        tramSchedule = [
            {
                trip_id: "17_1",
                route_type: GTFSRouteTypeEnum.TRAM,
                // ...
                start_timestamp: "2021-10-07T12:07:00+02:00",
                end_timestamp: "2021-10-07T12:21:00+02:00",
            },
            {
                trip_id: "17_2",
                route_type: GTFSRouteTypeEnum.TRAM,
                // ...
                start_timestamp: "2021-10-07T12:27:00+02:00",
                end_timestamp: "2021-10-07T12:41:00+02:00",
            },
        ];

        scheduledTripsData = JSON.parse(
            fs.readFileSync(__dirname + "/./data/vehiclepositions-runs-scheduled-trips.json").toString("utf8")
        );
        cachedRunsTripsData = JSON.parse(
            fs.readFileSync(__dirname + "/./data/vehiclepositions-runs-cache.json").toString("utf8")
        );

        sandbox = sinon.createSandbox();
        clock = sinon.useFakeTimers({
            now: new Date(2021, 2, 17, 10, 0),
            shouldAdvanceTime: true,
        });

        sequelizeModelStub = Object.assign({
            belongsTo: sandbox.stub(),
            hasMany: sandbox.stub(),
            hasOne: sandbox.stub(),
            removeAttribute: sandbox.stub(),
            upsert: sandbox.stub().resolves([{ id: 42 }, true]),
        });
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().callsFake(() => sequelizeModelStub),
                query: sandbox.stub().callsFake(() => [{ exists: true }]),
                transaction: sandbox.stub().callsFake(() => Object.assign({ commit: sandbox.stub() })),
            })
        );
        sandbox.stub(RedisConnector, "getConnection");

        sandbox.stub(QueueManager, "sendMessageToExchange").resolves();

        task = new UpdateRunsGtfsTripIdTask("test.test");

        modelRunsStub = sandbox.stub(task["commonRunsRepository"], "getRunRecordForUpdate");
        sandbox.stub(task["positionsRepository"], "bulkSave");
        sandbox.stub(task["tripsRepository"], "bulkSave").resolves(testData as any);
        sandbox.stub(task["tripsRepository"], "update");

        sandbox.stub(task["commonRunsRepository"], "updateAndAssociate");
        sandbox.stub(task["commonRunsRepository"], "createAndAssociate");
        sandbox.stub(task["commonRunsRepository"], "getScheduledTrips").callsFake(() => Promise.resolve(scheduledTripsData));
        sandbox.stub(task["positionsRepository"], "deleteNHoursOldData");
        modelTripsUpsertRunTripStub = sandbox.stub(task["tripsRepository"], "upsertCommonRunTrip");
        modelPositionsUpsertRunPositionStub = sandbox
            .stub(task["positionsRepository"], "upsertCommonRunPosition")
            .callsFake(() => Promise.resolve({ id: "id" } as any));
        runTripsModelGetStub = sandbox.stub(task["runTripsRedisRepository"], "get").callsFake(() => Promise.resolve(null));
        sandbox.stub(task["runTripsRedisRepository"], "set").callsFake(() => Promise.resolve(null));
    });

    afterEach(() => {
        sandbox?.restore();
        clock?.restore();
    });

    it("should call the correct methods by updateRunsGTFSTripId method", async () => {
        sandbox.stub(task["gtfsTripRunManager"], "turnaroundEstimateInSeconds" as any).value(30);
        modelRunsStub.callsFake(() => Promise.resolve(null));
        runTripsModelGetStub.callsFake(() => Promise.resolve(null));

        await task.consume({
            content: Buffer.from(JSON.stringify(sourceDataTransformed)),
        } as any);

        sandbox.assert.calledOnce(task["runTripsRedisRepository"].get as SinonStub);
        sandbox.assert.calledWith(task["runTripsRedisRepository"].get as SinonStub, "15_1");
        sandbox.assert.calledOnce(task["commonRunsRepository"].getScheduledTrips as SinonStub);
        sandbox.assert.calledOnce(task["runTripsRedisRepository"].set as SinonStub);
        sandbox.assert.calledWith(task["runTripsRedisRepository"].set as SinonStub, "15_1", cachedRunsTripsData);
        sandbox.assert.calledOnce(task["tripsRepository"].upsertCommonRunTrip as SinonStub);
        sandbox.assert.calledOnce(task["positionsRepository"].upsertCommonRunPosition as SinonStub);
        sandbox.assert.calledOnce(QueueManager.sendMessageToExchange as SinonStub);
    });

    it("updateRunsGTFSTripId should set tracking due to departure TCP event", async () => {
        sinon.stub(task["gtfsTripRunManager"], <any>"setAndReturnScheduledTrips").returns(Promise.resolve(schedule));

        const input = {
            run: {
                id: "177_6_6755_2021-10-07T03:58:27+02:00",
                route_id: "177",
                run_number: "6",
                line_short_name: "177",
                registration_number: "6755",
                msg_start_timestamp: "2021-10-07 03:58:27+02",
                msg_last_timestamp: "2021-10-07 13:28:36+02",
                wheelchair_accessible: true,
            },
            run_message: {
                id: 13619608,
                lat: 50.12707,
                lng: 14.41349,
                actual_stop_asw_id: "06490002",
                actual_stop_timestamp_real: 1633601100000,
                actual_stop_timestamp_scheduled: 1633601220000,
                last_stop_asw_id: "00520102",
                packet_number: null,
                msg_timestamp: "2021-10-07 12:05:23+02",
                events: TCPEventEnum.DEPARTURED,
            },
        };

        await task.consume({ content: Buffer.from(JSON.stringify(input)) } as any);

        expect(modelTripsUpsertRunTripStub.getCall(0).args).to.deep.equal([input, schedule[0]]);
        expect(modelPositionsUpsertRunPositionStub.getCall(0).args).to.deep.equal([input, schedule[0], true]);

        expect(modelTripsUpsertRunTripStub.getCall(1).args).to.deep.equal([input, schedule[1]]);
        expect(modelPositionsUpsertRunPositionStub.getCall(1).args).to.deep.equal([input, schedule[1], false]);

        expect(modelTripsUpsertRunTripStub.callCount).to.be.equal(2);
        expect(modelPositionsUpsertRunPositionStub.callCount).to.be.equal(2);
    });

    it("updateRunsGTFSTripId should set tracking due to time after first stop time", async () => {
        sinon.stub(task["gtfsTripRunManager"], <any>"setAndReturnScheduledTrips").returns(Promise.resolve(schedule));

        const input = {
            run: {
                id: "177_6_6755_2021-10-07T03:58:27+02:00",
                route_id: "177",
                run_number: "6",
                line_short_name: "177",
                registration_number: "6755",
                msg_start_timestamp: "2021-10-07 03:58:27+02",
                msg_last_timestamp: "2021-10-07 13:28:36+02",
                wheelchair_accessible: true,
            },
            run_message: {
                id: 13619608,
                lat: 50.12707,
                lng: 14.41349,
                actual_stop_asw_id: "06490002",
                actual_stop_timestamp_real: 1633601100000,
                actual_stop_timestamp_scheduled: 1633601220000,
                last_stop_asw_id: "00520102",
                packet_number: null,
                msg_timestamp: "2021-10-07 12:07:10+02",
                events: TCPEventEnum.TIME,
            },
        };

        await task.consume({ content: Buffer.from(JSON.stringify(input)) } as any);

        expect(modelTripsUpsertRunTripStub.getCall(0).args).to.deep.equal([input, schedule[0]]);
        expect(modelPositionsUpsertRunPositionStub.getCall(0).args).to.deep.equal([input, schedule[0], true]);

        expect(modelTripsUpsertRunTripStub.getCall(1).args).to.deep.equal([input, schedule[1]]);
        expect(modelPositionsUpsertRunPositionStub.getCall(1).args).to.deep.equal([input, schedule[1], false]);

        expect(modelTripsUpsertRunTripStub.callCount).to.be.equal(2);
        expect(modelPositionsUpsertRunPositionStub.callCount).to.be.equal(2);
    });

    it("updateRunsGTFSTripId should set tracking due to stop time after first stop time even before scheduled", async () => {
        sinon.stub(task["gtfsTripRunManager"], <any>"setAndReturnScheduledTrips").returns(Promise.resolve(schedule));

        const input = {
            run: {
                id: "177_6_6755_2021-10-07T03:58:27+02:00",
                route_id: "177",
                run_number: "6",
                line_short_name: "177",
                registration_number: "6755",
                msg_start_timestamp: "2021-10-07 03:58:27+02",
                msg_last_timestamp: "2021-10-07 13:28:36+02",
                wheelchair_accessible: true,
            },
            run_message: {
                id: 13619608,
                lat: 50.12707,
                lng: 14.41349,
                actual_stop_asw_id: "06490002",
                actual_stop_timestamp_real: 1633601280000,
                actual_stop_timestamp_scheduled: 1633601280000,
                last_stop_asw_id: "00520102",
                packet_number: null,
                msg_timestamp: "2021-10-07 12:06:10+02",
                events: TCPEventEnum.TIME,
            },
        };

        await task.consume({ content: Buffer.from(JSON.stringify(input)) } as any);

        expect(modelTripsUpsertRunTripStub.getCall(0).args).to.deep.equal([input, schedule[0]]);
        expect(modelPositionsUpsertRunPositionStub.getCall(0).args).to.deep.equal([input, schedule[0], true]);

        expect(modelTripsUpsertRunTripStub.getCall(1).args).to.deep.equal([input, schedule[1]]);
        expect(modelPositionsUpsertRunPositionStub.getCall(1).args).to.deep.equal([input, schedule[1], false]);

        expect(modelTripsUpsertRunTripStub.callCount).to.be.equal(2);
        expect(modelPositionsUpsertRunPositionStub.callCount).to.be.equal(2);
    });

    it("updateRunsGTFSTripId should set tracking due to time after first stop time", async () => {
        sinon.stub(task["gtfsTripRunManager"], <any>"setAndReturnScheduledTrips").returns(Promise.resolve(schedule));

        const input = {
            run: {
                id: "177_6_6755_2021-10-07T03:58:27+02:00",
                route_id: "177",
                run_number: "6",
                line_short_name: "177",
                registration_number: "6755",
                msg_start_timestamp: "2021-10-07 03:58:27+02",
                msg_last_timestamp: "2021-10-07 13:28:36+02",
                wheelchair_accessible: true,
            },
            run_message: {
                id: 13619608,
                lat: 50.12707,
                lng: 14.41349,
                actual_stop_asw_id: "06490002",
                actual_stop_timestamp_real: 1633601100000,
                actual_stop_timestamp_scheduled: 1633601220000,
                last_stop_asw_id: "00520102",
                packet_number: null,
                msg_timestamp: "2021-10-07 12:07:10+02",
                events: TCPEventEnum.TIME,
            },
        };

        await task.consume({ content: Buffer.from(JSON.stringify(input)) } as any);

        expect(modelTripsUpsertRunTripStub.getCall(0).args).to.deep.equal([input, schedule[0]]);
        expect(modelPositionsUpsertRunPositionStub.getCall(0).args).to.deep.equal([input, schedule[0], true]);

        expect(modelTripsUpsertRunTripStub.getCall(1).args).to.deep.equal([input, schedule[1]]);
        expect(modelPositionsUpsertRunPositionStub.getCall(1).args).to.deep.equal([input, schedule[1], false]);

        expect(modelTripsUpsertRunTripStub.callCount).to.be.equal(2);
        expect(modelPositionsUpsertRunPositionStub.callCount).to.be.equal(2);
    });

    it("updateRunsGTFSTripId should set tracking due to stop time after first stop time even before scheduled", async () => {
        sinon.stub(task["gtfsTripRunManager"], <any>"setAndReturnScheduledTrips").returns(Promise.resolve(schedule));

        const input = {
            run: {
                id: "177_6_6755_2021-10-07T03:58:27+02:00",
                route_id: "177",
                run_number: "6",
                line_short_name: "177",
                registration_number: "6755",
                msg_start_timestamp: "2021-10-07 03:58:27+02",
                msg_last_timestamp: "2021-10-07 13:28:36+02",
                wheelchair_accessible: true,
            },
            run_message: {
                id: 13619608,
                lat: 50.12707,
                lng: 14.41349,
                actual_stop_asw_id: "06490002",
                actual_stop_timestamp_real: 1633601280000,
                actual_stop_timestamp_scheduled: 1633601280000,
                last_stop_asw_id: "00520102",
                packet_number: null,
                msg_timestamp: "2021-10-07 12:06:10+02",
                events: TCPEventEnum.TIME,
            },
        };

        await task.consume({ content: Buffer.from(JSON.stringify(input)) } as any);

        expect(modelTripsUpsertRunTripStub.getCall(0).args).to.deep.equal([input, schedule[0]]);
        expect(modelPositionsUpsertRunPositionStub.getCall(0).args).to.deep.equal([input, schedule[0], true]);

        expect(modelTripsUpsertRunTripStub.getCall(1).args).to.deep.equal([input, schedule[1]]);
        expect(modelPositionsUpsertRunPositionStub.getCall(1).args).to.deep.equal([input, schedule[1], false]);

        expect(modelTripsUpsertRunTripStub.callCount).to.be.equal(2);
        expect(modelPositionsUpsertRunPositionStub.callCount).to.be.equal(2);
    });

    it("updateRunsGTFSTripId should get no current trip but two (one last and one future)", async () => {
        sinon.stub(task["gtfsTripRunManager"], <any>"setAndReturnScheduledTrips").returns(Promise.resolve(schedule));

        const input = {
            run: {
                id: "177_6_6755_2021-10-08T03:58:27+02:00",
                route_id: "177",
                run_number: "6",
                line_short_name: "177",
                registration_number: "6755",
                msg_start_timestamp: "2021-10-07 03:58:27+02",
                msg_last_timestamp: "2021-10-07 13:28:36+02",
                wheelchair_accessible: true,
            },
            run_message: {
                id: 13619608,
                lat: 50.12707,
                lng: 14.41349,
                actual_stop_asw_id: "06490002",
                actual_stop_timestamp_real: 1633602240000,
                actual_stop_timestamp_scheduled: 1633602240000,
                last_stop_asw_id: "00520102",
                packet_number: null,
                msg_timestamp: "2021-10-07 12:06:10+02",
                events: TCPEventEnum.TIME,
            },
        };

        await task.consume({ content: Buffer.from(JSON.stringify(input)) } as any);

        expect(modelTripsUpsertRunTripStub.getCall(0).args).to.deep.equal([input, schedule[0]]);
        expect(modelPositionsUpsertRunPositionStub.getCall(0).args).to.deep.equal([input, schedule[0], false]);

        expect(modelTripsUpsertRunTripStub.getCall(1).args).to.deep.equal([input, schedule[1]]);
        expect(modelPositionsUpsertRunPositionStub.getCall(1).args).to.deep.equal([input, schedule[1], false]);

        expect(modelTripsUpsertRunTripStub.callCount).to.be.equal(2);
        expect(modelPositionsUpsertRunPositionStub.callCount).to.be.equal(2);
    });

    it("updateRunsGTFSTripId tram should be tracking even before scheduled first stop", async () => {
        sinon.stub(task["gtfsTripRunManager"], <any>"setAndReturnScheduledTrips").returns(Promise.resolve(tramSchedule));

        const input = {
            run: {
                id: "17_1",
                route_id: "17",
                run_number: "6",
                line_short_name: "17",
                registration_number: "6755",
                msg_start_timestamp: "2021-10-07 03:58:27+02",
                msg_last_timestamp: "2021-10-07 13:28:36+02",
                wheelchair_accessible: true,
            },
            run_message: {
                id: 13619608,
                lat: 50.12707,
                lng: 14.41349,
                actual_stop_asw_id: "06490002",
                actual_stop_timestamp_real: 1633601100000,
                actual_stop_timestamp_scheduled: 1633601220000,
                last_stop_asw_id: "00520102",
                packet_number: null,
                msg_timestamp: "2021-10-07 12:06:10+02",
                events: TCPEventEnum.TIME,
            },
        };

        await task.consume({ content: Buffer.from(JSON.stringify(input)) } as any);

        expect(modelTripsUpsertRunTripStub.getCall(0).args).to.deep.equal([input, tramSchedule[0]]);
        expect(modelPositionsUpsertRunPositionStub.getCall(0).args).to.deep.equal([input, tramSchedule[0], true]);

        expect(modelTripsUpsertRunTripStub.getCall(1).args).to.deep.equal([input, tramSchedule[1]]);
        expect(modelPositionsUpsertRunPositionStub.getCall(1).args).to.deep.equal([input, tramSchedule[1], false]);

        expect(modelTripsUpsertRunTripStub.callCount).to.be.equal(2);
        expect(modelPositionsUpsertRunPositionStub.callCount).to.be.equal(2);
    });

    it("should call the correct methods for not public trip", async () => {
        const tripsId = "NOT_PUBLIC_67_1_2021-06-28T15:05:00+02:00_5572";
        const positionId = "24";
        sinon.stub(task["gtfsTripRunManager"], <any>"setAndReturnScheduledTrips").returns(Promise.resolve([]));
        const modelTripsUpsertNotPublicTripStub = sandbox.stub(task["tripsRepository"], "upsertEntity");
        const modelTripsPositionsUpsertNotPublicTripStub = sinon.stub(task["positionsRepository"], "createEntity").resolves({
            id: positionId,
        } as PositionDto);

        const input: IUpdateRunsGtfsTripInput = {
            run: {
                id: "67_1_5572_2021-06-28T15:05:00+02:00",
                route_id: "67",
                run_number: 1,
                line_short_name: "67",
                registration_number: "5572",
                msg_start_timestamp: "2021-06-28T15:05:00+02:00",
                msg_last_timestamp: "2021-06-28T15:05:00+02:00",
                wheelchair_accessible: false,
            },
            run_message: {
                lat: 50.08885,
                lng: 14.42999,
                actual_stop_asw_id: "04800002",
                actual_stop_timestamp_real: "2021-06-28T13:04:38.000Z",
                actual_stop_timestamp_scheduled: "2021-06-28T13:04:00.000Z",
                last_stop_asw_id: "10190002",
                packet_number: "2918063",
                msg_timestamp: new Date("2021-06-28T15:04:38+02:00"),
                events: TCPEventEnum.ARRIVAL_ANNOUNCED,
            },
        };

        await task.consume({
            content: Buffer.from(JSON.stringify(input)),
        } as any);

        const positionDto: Partial<IPositionDto> = {
            asw_last_stop_id: "10190002",
            lat: 50.08885,
            lng: 14.42999,
            origin_time: "15:04:38",
            origin_timestamp: new Date("2021-06-28T13:04:38.000Z"),
            state_position: StatePositionEnum.NOT_PUBLIC,
            state_process: StateProcessEnum.PROCESSED,
            is_tracked: true,
            trips_id: tripsId,
            tcp_event: TCPEventEnum.ARRIVAL_ANNOUNCED,
        };

        const tripDto: Partial<IVPTripsModel> = {
            agency_name_real: "DP PRAHA",
            agency_name_scheduled: "DP PRAHA",
            gtfs_route_type: 0,
            origin_route_name: "67",
            internal_route_name: "67",
            run_number: 1,
            internal_run_number: 1,
            vehicle_registration_number: 5572,
            vehicle_type_id: 2,
            wheelchair_accessible: false,
            provider_source_type: "2",
            id: tripsId,
            last_position_id: positionId,
        };

        expect(modelTripsPositionsUpsertNotPublicTripStub.getCall(0).args).to.deep.equal([positionDto]);
        expect(modelTripsUpsertNotPublicTripStub.getCall(0).args).to.deep.equal([tripDto]);
    });
});
