import { PropagateDelayTask } from "#ie/vehicle-positions/workers/vehicle-positions/tasks/PropagateDelayTask";
import { IVPTripsModel } from "#sch/vehicle-positions/models/interfaces/IVPTripsModel";
import { IVPTripsPositionAttributes } from "#sch/vehicle-positions/models/interfaces/IVPTripsPositionAttributes";
import { PostgresConnector, RedisConnector } from "@golemio/core/dist/integration-engine/connectors";
import { QueueManager } from "@golemio/core/dist/integration-engine/queueprocessors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import sinon, { SinonFakeTimers, SinonSandbox, SinonStub } from "sinon";
import { StatePositionEnum, StateProcessEnum } from "src/const";
import { runTripsId_9_22_IComputedTrip } from "./data/runTripsId_9_22_IComputedTrip";
import { runTripsId_9_22_getPDTWPositions } from "./data/runTripsId_9_22_getPDTWPositions";

chai.use(chaiAsPromised);

describe("VPWorker - PropagateDelayTask", () => {
    let sandbox: SinonSandbox;
    let clock: SinonFakeTimers;
    let sequelizeModelStub: Record<string, SinonStub>;
    let tripRepositoryBulkSaveStub: SinonStub;
    let positionRepositoryBulkSaveStub: SinonStub;
    let runTripsModelGetStub: SinonStub;
    let propagateDelayForGtfsTripStub: SinonStub;
    let getPropagateDelayTripsWithPositionsStub: SinonStub;
    let task: PropagateDelayTask;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        clock = sinon.useFakeTimers({
            now: new Date(2021, 12, 9, 10, 0),
            shouldAdvanceTime: true,
        });

        sequelizeModelStub = Object.assign({
            belongsTo: sandbox.stub(),
            hasMany: sandbox.stub(),
            hasOne: sandbox.stub(),
            removeAttribute: sandbox.stub(),
            upsert: sandbox.stub().resolves([{ id: 42 }, true]),
        });
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().callsFake(() => sequelizeModelStub),
                query: sandbox.stub().callsFake(() => [{ exists: true }]),
                transaction: sandbox.stub().callsFake(() => Object.assign({ commit: sandbox.stub() })),
            })
        );
        sandbox.stub(RedisConnector, "getConnection");

        sandbox.stub(QueueManager, "sendMessageToExchange").resolves();

        task = new PropagateDelayTask("test.test");
        tripRepositoryBulkSaveStub = sandbox.stub(task["tripsRepository"], "bulkSave");
        positionRepositoryBulkSaveStub = sandbox.stub(task["positionsRepository"], "bulkSave");
    });

    afterEach(() => {
        sandbox?.restore();
        clock?.restore();
    });

    it("should take second gtfs_trip_id from schedule for same start_timestamp", async () => {
        propagateDelayForGtfsTripStub = sandbox
            .stub(task["tripsRepository"], "getPropagateDelayTripsWithPositions")
            .resolves(runTripsId_9_22_getPDTWPositions as Array<IVPTripsModel & { last_position: IVPTripsPositionAttributes }>);

        runTripsModelGetStub = sandbox
            .stub(task["runTripsRedisRepository"], "get")
            .callsFake(() => JSON.parse(fs.readFileSync(__dirname + "/data/runTripsId_9_22_redis.json").toString()));

        await task.consume({
            content: JSON.stringify(runTripsId_9_22_IComputedTrip),
        } as any);

        expect(tripRepositoryBulkSaveStub.callCount).to.be.equal(1);
        expect(tripRepositoryBulkSaveStub.getCall(0).args).to.be.deep.equal([
            [
                {
                    id: "2021-12-09T08:03:00+01:00_9_4904_211202_9381",
                    last_position_id: "58895276",
                    last_position_context: {
                        lastPositionLat: 50.08505,
                        lastPositionLng: 14.44451,
                        lastPositionState: StatePositionEnum.BEFORE_TRACK_DELAYED,
                        lastPositionStateChange: "2022-01-09T10:00:00.000Z",
                        lastPositionBeforeTrackDelayed: {
                            delay: 2155,
                            origin_timestamp: new Date(1639034440000),
                        },
                    },
                },
            ],
        ]);

        expect(positionRepositoryBulkSaveStub.callCount).to.be.equal(1);
        expect(positionRepositoryBulkSaveStub.getCall(0).args).to.be.deep.equal([
            [
                {
                    id: "58895276",
                    delay: 2155,
                    state_position: StatePositionEnum.BEFORE_TRACK_DELAYED,
                    state_process: StateProcessEnum.PROCESSED,
                    origin_timestamp: new Date(1639034440000),
                    bearing: 10,
                    origin_position_id: "2",
                },
            ],
        ]);
    });

    it("should call the correct methods by propagateDelay method", async () => {
        runTripsModelGetStub = sandbox.stub(task["runTripsRedisRepository"], "get").callsFake(() => Promise.resolve(null));

        propagateDelayForGtfsTripStub = sandbox.stub(task, "propagateDelayForGtfsTrip" as any);
        runTripsModelGetStub.callsFake(() =>
            Promise.resolve({
                schedule: [
                    {
                        trip_id: "19_2383_210830",
                        start_timestamp: "2021-09-08T14:03:00+02:00",
                        end_timestamp: "2021-09-08T14:56:00+02:00",
                    },
                    {
                        trip_id: "19_2384_210830",
                        start_timestamp: "2021-09-08T15:06:00+02:00",
                        end_timestamp: "2021-09-08T15:58:00+02:00",
                    },
                ],
            })
        );

        await task["execute"]({
            processedPositions: [
                {
                    context: {
                        lastPositionTracking: {
                            // @ts-expect-error - not all properties are required in the test
                            properties: {
                                bearing: 10,
                            },
                        },
                        lastPositionOriginTimestamp: 1631102586000,
                        lastPositionDelay: 154,
                        lastPositionState: StatePositionEnum.AT_STOP,
                        lastPositionStateChange: "2022-01-09T10:00:00.000Z",
                        tripId: "2021-09-08T14:03:00+02:00_19_2383_210830_9085",
                        lastPositionId: "111111",
                    },
                    positions: [],
                },
            ],
            trips: [
                {
                    id: "2021-09-08T14:03:00+02:00_19_2383_210830_9085",
                    gtfs_trip_id: "19_2383_210830",
                    gtfs_block_id: "test",
                    start_timestamp: new Date(1631102580000).toISOString(),
                    origin_route_name: "19",
                    internal_route_name: "19",
                    run_number: 13,
                    internal_run_number: 13,
                    vehicle_registration_number: 9085,
                },
            ],
        });

        sandbox.assert.calledOnce(task["runTripsRedisRepository"].get as SinonStub);
        sandbox.assert.calledOnce(propagateDelayForGtfsTripStub);
        sandbox.assert.calledWith(propagateDelayForGtfsTripStub, {
            currentGtfsTripId: "19_2383_210830",
            currentGtfsBlockId: "test",
            currentOriginTimestamp: new Date("2021-09-08T12:03:06.000Z"),
            currentDelay: 154,
            currentRegistrationNumber: 9085,
            currentStartTime: "2021-09-08T14:03:00+02:00",
            currentEndTime: "2021-09-08T14:56:00+02:00",
            nextGtfsTrips: [{ trip_id: "19_2384_210830", requiredTurnaroundSeconds: undefined }],
            currentBearing: 10,
            originPositionId: "111111",
        });
    });

    it("should calculate and propagate delay for GTFS run trip", async () => {
        getPropagateDelayTripsWithPositionsStub = sandbox
            .stub(task["tripsRepository"], "getPropagateDelayTripsWithPositions")
            .callsFake(
                async (): Promise<any> => [
                    {
                        last_position: {
                            id: "227255",
                            delay: null,
                            origin_timestamp: new Date(1629882900000),
                            lat: 50.08505,
                            lng: 14.44451,
                        },
                        id: "trip1",
                        start_timestamp: new Date(1629884100000),
                        end_timestamp: new Date(1629885180000),
                        // ...
                    },
                    {
                        last_position: {
                            id: "227398",
                            delay: null,
                            origin_timestamp: new Date(1629882900000),
                            lat: 50.08505,
                            lng: 14.44451,
                        },
                        id: "trip2",
                        start_timestamp: new Date(1629887100000),
                        end_timestamp: new Date(1629888180000),
                        // ...
                    },
                ]
            );

        await task["propagateDelayForGtfsTrip"]({
            currentGtfsTripId: "21_2255_210809",
            currentGtfsBlockId: null,
            currentDelay: 360,
            currentRegistrationNumber: 9219,
            currentOriginTimestamp: new Date(1629882900000),
            currentStartTime: "2021-08-25T11:15:00+02:00",
            currentEndTime: "2021-08-25T11:33:00+02:00",
            nextGtfsTrips: [{ trip_id: "21_2256_210809" }, { trip_id: "21_2258_210809" }],
            currentBearing: 10,
            originPositionId: "111111",
        });

        expect(getPropagateDelayTripsWithPositionsStub.callCount).to.be.equal(1);
        expect(tripRepositoryBulkSaveStub.callCount).to.be.equal(1);
        expect(tripRepositoryBulkSaveStub.getCall(0).args).to.be.deep.equal([
            [
                {
                    id: "trip1",
                    last_position_id: "227255",
                    last_position_context: {
                        lastPositionLat: 50.08505,
                        lastPositionLng: 14.44451,
                        lastPositionState: StatePositionEnum.BEFORE_TRACK_DELAYED,
                        lastPositionStateChange: "2022-01-09T10:00:00.000Z",
                        lastPositionBeforeTrackDelayed: {
                            delay: 240,
                            origin_timestamp: new Date(1629882900000),
                        },
                    },
                },
                {
                    id: "trip2",
                    last_position_id: "227398",
                    last_position_context: {
                        lastPositionLat: 50.08505,
                        lastPositionLng: 14.44451,
                        lastPositionState: StatePositionEnum.BEFORE_TRACK_DELAYED,
                        lastPositionStateChange: "2022-01-09T10:00:00.000Z",
                        lastPositionBeforeTrackDelayed: {
                            delay: 0,
                            origin_timestamp: new Date(1629882900000),
                        },
                    },
                },
            ],
        ]);

        expect(positionRepositoryBulkSaveStub.callCount).to.be.equal(1);
        expect(positionRepositoryBulkSaveStub.getCall(0).args).to.be.deep.equal([
            [
                {
                    id: "227255",
                    delay: 240,
                    state_position: StatePositionEnum.BEFORE_TRACK_DELAYED,
                    state_process: StateProcessEnum.PROCESSED,
                    origin_timestamp: new Date(1629882900000),
                    bearing: 10,
                    origin_position_id: "111111",
                },
                {
                    id: "227398",
                    delay: 0,
                    state_position: StatePositionEnum.BEFORE_TRACK_DELAYED,
                    state_process: StateProcessEnum.PROCESSED,
                    origin_timestamp: new Date(1629882900000),
                    bearing: 10,
                    origin_position_id: "111111",
                },
            ],
        ]);
    });

    it("should calculate and propagate delay for GTFS run trip with consideration requiredTurnaroundSeconds", async () => {
        getPropagateDelayTripsWithPositionsStub = sandbox
            .stub(task["tripsRepository"], "getPropagateDelayTripsWithPositions")
            .callsFake(
                async (): Promise<any> => [
                    {
                        last_position: {
                            id: "227255",
                            delay: null,
                            origin_timestamp: new Date(1629882900000),
                            lat: 50.08505,
                            lng: 14.44451,
                        },
                        id: "trip1",
                        start_timestamp: 1629884100000,
                        end_timestamp: 1629885180000,
                        gtfs_trip_id: "21_2256_210809",
                        // ...
                    },
                    {
                        last_position: {
                            id: "227398",
                            delay: null,
                            origin_timestamp: new Date(1629882900000),
                            lat: 50.08505,
                            lng: 14.44451,
                        },
                        id: "trip2",
                        start_timestamp: 1629885420000,
                        end_timestamp: 1629888180000,
                        gtfs_trip_id: "21_2258_210809",
                        // ...
                    },
                ]
            );

        await task["propagateDelayForGtfsTrip"]({
            currentGtfsTripId: "21_2255_210809",
            currentGtfsBlockId: null,
            currentDelay: 360,
            currentRegistrationNumber: 9219,
            currentOriginTimestamp: new Date(1629882900000),
            currentStartTime: "2021-08-25T11:15:00+02:00",
            currentEndTime: "2021-08-25T11:33:00+02:00",
            nextGtfsTrips: [{ trip_id: "21_2256_210809" }, { trip_id: "21_2258_210809", requiredTurnaroundSeconds: 30 }],
            currentBearing: 10,
            originPositionId: "111111",
        });

        expect(getPropagateDelayTripsWithPositionsStub.callCount).to.be.equal(1);
        expect(tripRepositoryBulkSaveStub.callCount).to.be.equal(1);
        expect(tripRepositoryBulkSaveStub.getCall(0).args).to.be.deep.equal([
            [
                {
                    id: "trip1",
                    last_position_id: "227255",
                    last_position_context: {
                        lastPositionLat: 50.08505,
                        lastPositionLng: 14.44451,
                        lastPositionState: StatePositionEnum.BEFORE_TRACK_DELAYED,
                        lastPositionStateChange: "2022-01-09T10:00:00.000Z",
                        lastPositionBeforeTrackDelayed: {
                            delay: 240,
                            origin_timestamp: new Date(1629882900000),
                        },
                    },
                },
                {
                    id: "trip2",
                    last_position_id: "227398",
                    last_position_context: {
                        lastPositionLat: 50.08505,
                        lastPositionLng: 14.44451,
                        lastPositionState: StatePositionEnum.BEFORE_TRACK_DELAYED,
                        lastPositionStateChange: "2022-01-09T10:00:00.000Z",
                        lastPositionBeforeTrackDelayed: {
                            delay: 30,
                            origin_timestamp: new Date(1629882900000),
                        },
                    },
                },
            ],
        ]);

        expect(positionRepositoryBulkSaveStub.callCount).to.be.equal(1);
        expect(positionRepositoryBulkSaveStub.getCall(0).args).to.be.deep.equal([
            [
                {
                    id: "227255",
                    delay: 240,
                    state_position: StatePositionEnum.BEFORE_TRACK_DELAYED,
                    state_process: StateProcessEnum.PROCESSED,
                    origin_timestamp: new Date(1629882900000),
                    bearing: 10,
                    origin_position_id: "111111",
                },
                {
                    id: "227398",
                    delay: 30,
                    state_position: StatePositionEnum.BEFORE_TRACK_DELAYED,
                    state_process: StateProcessEnum.PROCESSED,
                    origin_timestamp: new Date(1629882900000),
                    bearing: 10,
                    origin_position_id: "111111",
                },
            ],
        ]);
    });

    it("should calculate and propagate delay for GTFS run trip with negative delay (defined gtfs_block_id)", async () => {
        getPropagateDelayTripsWithPositionsStub = sandbox
            .stub(task["tripsRepository"], "getPropagateDelayTripsWithPositions")
            .callsFake(
                async (): Promise<any> => [
                    {
                        last_position: {
                            id: "227255",
                            delay: null,
                            origin_timestamp: new Date(1629882900000),
                            lat: 50.08505,
                            lng: 14.44451,
                        },
                        id: "trip1",
                        start_timestamp: 1629884100000,
                        end_timestamp: 1629885180000,
                        gtfs_trip_id: "21_2256_210809",
                        gtfs_block_id: "test",
                        // ...
                    },
                    {
                        last_position: {
                            id: "227398",
                            delay: null,
                            origin_timestamp: new Date(1629882900000),
                            lat: 50.08505,
                            lng: 14.44451,
                        },
                        id: "trip2",
                        start_timestamp: 1629885420000,
                        end_timestamp: 1629888180000,
                        gtfs_trip_id: "21_2258_210809",
                        gtfs_block_id: "something",
                        // ...
                    },
                ]
            );

        await task["propagateDelayForGtfsTrip"]({
            currentGtfsTripId: "21_2255_210809",
            currentGtfsBlockId: "test",
            currentDelay: -360,
            currentRegistrationNumber: 9219,
            currentOriginTimestamp: new Date(1629882900000),
            currentStartTime: "2021-08-25T11:15:00+02:00",
            currentEndTime: "2021-08-25T11:33:00+02:00",
            nextGtfsTrips: [{ trip_id: "21_2256_210809" }, { trip_id: "21_2258_210809", requiredTurnaroundSeconds: 30 }],
            currentBearing: 10,
            originPositionId: "111111",
        });

        expect(getPropagateDelayTripsWithPositionsStub.callCount).to.be.equal(1);
        expect(tripRepositoryBulkSaveStub.callCount).to.be.equal(1);
        expect(tripRepositoryBulkSaveStub.getCall(0).args).to.be.deep.equal([
            [
                {
                    id: "trip1",
                    last_position_id: "227255",
                    last_position_context: {
                        lastPositionLat: 50.08505,
                        lastPositionLng: 14.44451,
                        lastPositionState: StatePositionEnum.BEFORE_TRACK_DELAYED,
                        lastPositionStateChange: "2022-01-09T10:00:00.000Z",
                        lastPositionBeforeTrackDelayed: {
                            delay: -360,
                            origin_timestamp: new Date(1629882900000),
                        },
                    },
                },
                {
                    id: "trip2",
                    last_position_id: "227398",
                    last_position_context: {
                        lastPositionLat: 50.08505,
                        lastPositionLng: 14.44451,
                        lastPositionState: StatePositionEnum.BEFORE_TRACK_DELAYED,
                        lastPositionStateChange: "2022-01-09T10:00:00.000Z",
                        lastPositionBeforeTrackDelayed: {
                            delay: 0,
                            origin_timestamp: new Date(1629882900000),
                        },
                    },
                },
            ],
        ]);

        expect(positionRepositoryBulkSaveStub.callCount).to.be.equal(1);
        expect(positionRepositoryBulkSaveStub.getCall(0).args).to.be.deep.equal([
            [
                {
                    id: "227255",
                    delay: -360,
                    state_position: StatePositionEnum.BEFORE_TRACK_DELAYED,
                    state_process: StateProcessEnum.PROCESSED,
                    origin_timestamp: new Date(1629882900000),
                    bearing: 10,
                    origin_position_id: "111111",
                },
                {
                    id: "227398",
                    delay: 0,
                    state_position: StatePositionEnum.BEFORE_TRACK_DELAYED,
                    state_process: StateProcessEnum.PROCESSED,
                    origin_timestamp: new Date(1629882900000),
                    bearing: 10,
                    origin_position_id: "111111",
                },
            ],
        ]);
    });

    it("should propagate delay 0 for GTFS run trip with negative delay (unknown gtfs_block_id)", async () => {
        getPropagateDelayTripsWithPositionsStub = sandbox
            .stub(task["tripsRepository"], "getPropagateDelayTripsWithPositions")
            .callsFake(
                async (): Promise<any> => [
                    {
                        last_position: {
                            id: "227255",
                            delay: null,
                            origin_timestamp: new Date(1629882900000),
                            lat: 50.08505,
                            lng: 14.44451,
                        },
                        id: "trip1",
                        start_timestamp: 1629884100000,
                        end_timestamp: 1629885180000,
                        gtfs_trip_id: "21_2256_210809",
                        // ...
                    },
                    {
                        last_position: {
                            id: "227398",
                            delay: null,
                            origin_timestamp: new Date(1629882900000),
                            lat: 50.08505,
                            lng: 14.44451,
                        },
                        id: "trip2",
                        start_timestamp: 1629885420000,
                        end_timestamp: 1629888180000,
                        gtfs_trip_id: "21_2258_210809",
                        // ...
                    },
                ]
            );

        await task["propagateDelayForGtfsTrip"]({
            currentGtfsTripId: "21_2255_210809",
            currentGtfsBlockId: null,
            currentDelay: -360,
            currentRegistrationNumber: 9219,
            currentOriginTimestamp: new Date(1629882900000),
            currentStartTime: "2021-08-25T11:15:00+02:00",
            currentEndTime: "2021-08-25T11:33:00+02:00",
            nextGtfsTrips: [{ trip_id: "21_2256_210809" }, { trip_id: "21_2258_210809", requiredTurnaroundSeconds: 30 }],
            currentBearing: 10,
            originPositionId: "111111",
        });

        expect(getPropagateDelayTripsWithPositionsStub.callCount).to.be.equal(1);
        expect(tripRepositoryBulkSaveStub.callCount).to.be.equal(1);
        expect(tripRepositoryBulkSaveStub.getCall(0).args).to.be.deep.equal([
            [
                {
                    id: "trip1",
                    last_position_id: "227255",
                    last_position_context: {
                        lastPositionLat: 50.08505,
                        lastPositionLng: 14.44451,
                        lastPositionState: StatePositionEnum.BEFORE_TRACK_DELAYED,
                        lastPositionStateChange: "2022-01-09T10:00:00.000Z",
                        lastPositionBeforeTrackDelayed: {
                            delay: 0,
                            origin_timestamp: new Date(1629882900000),
                        },
                    },
                },
                {
                    id: "trip2",
                    last_position_id: "227398",
                    last_position_context: {
                        lastPositionLat: 50.08505,
                        lastPositionLng: 14.44451,
                        lastPositionState: StatePositionEnum.BEFORE_TRACK_DELAYED,
                        lastPositionStateChange: "2022-01-09T10:00:00.000Z",
                        lastPositionBeforeTrackDelayed: {
                            delay: 0,
                            origin_timestamp: new Date(1629882900000),
                        },
                    },
                },
            ],
        ]);

        expect(positionRepositoryBulkSaveStub.callCount).to.be.equal(1);
        expect(positionRepositoryBulkSaveStub.getCall(0).args).to.be.deep.equal([
            [
                {
                    id: "227255",
                    delay: 0,
                    state_position: StatePositionEnum.BEFORE_TRACK_DELAYED,
                    state_process: StateProcessEnum.PROCESSED,
                    origin_timestamp: new Date(1629882900000),
                    bearing: 10,
                    origin_position_id: "111111",
                },
                {
                    id: "227398",
                    delay: 0,
                    state_position: StatePositionEnum.BEFORE_TRACK_DELAYED,
                    state_process: StateProcessEnum.PROCESSED,
                    origin_timestamp: new Date(1629882900000),
                    bearing: 10,
                    origin_position_id: "111111",
                },
            ],
        ]);
    });
});
