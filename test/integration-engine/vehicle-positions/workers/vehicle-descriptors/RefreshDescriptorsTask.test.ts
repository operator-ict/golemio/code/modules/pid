import { VPContainer } from "#ie/vehicle-positions/ioc/Di";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { RefreshDescriptorsTask } from "#ie/vehicle-positions/workers/vehicle-descriptors/tasks/RefreshDescriptorsTask";
import IPostgresConnector from "@golemio/core/dist/integration-engine/connectors/interfaces/IPostgresConnector";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc/ContainerToken";
import { expect } from "chai";
import sinon, { SinonSandbox, SinonStub } from "sinon";
import { InputDataFixture } from "./fixtures/InputDataFixture";
import { DescriptorRepository } from "#ie/vehicle-positions/workers/vehicle-descriptors/data-access/DescriptorRepository";

describe("DescriptorWorker - RefreshDescriptorsTask", () => {
    let sandbox: SinonSandbox;
    let task: RefreshDescriptorsTask;
    let getAllStub: SinonStub;
    let saveDataStub: SinonStub;

    const transformedFixture = {
        id: 1,
        state: "dilny",
        registration_number: 3,
        registration_number_index: 1,
        license_plate: "4SX XXXX",
        operator: "ČSAD MHD Kladno",
        manufacturer: "Scania",
        type: "Citywide LF 12M CNG",
        traction: "autobus",
        gtfs_route_type: 3,
        is_wheelchair_accessible: true,
        is_air_conditioned: true,
        has_usb_chargers: true,
        paint: "MAD Kladno, bílá s červeným pruhem",
        thumbnail_url: "https://xxxxx.jpg",
        photo_url: "https://xxxxx.jpg",
    };

    before(async () => {
        sandbox = sinon.createSandbox();
        getAllStub = sandbox.stub().resolves([InputDataFixture[0]]);

        const postgresConnector = VPContainer.resolve<IPostgresConnector>(ContainerToken.PostgresConnector);
        await postgresConnector.connect();

        VPContainer.registerSingleton(
            VPContainerToken.DescriptorDataSourceFactory,
            class DummyFactory {
                getDataSource() {
                    return {
                        getAll: getAllStub,
                    };
                }
            }
        );

        saveDataStub = sandbox.stub().resolves();

        VPContainer.registerSingleton(
            VPContainerToken.DescriptorRepository,
            class DummyRepository {
                saveData = saveDataStub;
            }
        );
    });

    beforeEach(() => {
        task = VPContainer.resolve<RefreshDescriptorsTask>(VPContainerToken.RefreshDescriptorsTask);
        saveDataStub.reset();
    });

    after(() => {
        VPContainer.clearInstances();
        VPContainer.registerSingleton(VPContainerToken.DescriptorRepository, DescriptorRepository);
        sandbox.restore();
    });

    it("should refresh data", async () => {
        await task["execute"]();
        getAllStub.resolves([{ ...InputDataFixture[0], airCondition: 0 }]);
        await task["execute"]();

        sandbox.assert.calledTwice(saveDataStub);
        expect(saveDataStub.getCall(0).args[0]).to.eql([transformedFixture]);
        expect(saveDataStub.getCall(1).args[0]).to.eql([{ ...transformedFixture, is_air_conditioned: false }]);
    });

    it("should filter out duplicates and save vehicle with correct state", async () => {
        getAllStub.resolves([
            InputDataFixture[4],
            InputDataFixture[3],
            InputDataFixture[0],
            InputDataFixture[4],
            InputDataFixture[0],
        ]);
        await task["execute"]();
        sandbox.assert.calledOnce(saveDataStub);
        expect(saveDataStub.getCall(0).args[0]).to.eql([transformedFixture]);
    });
});
