import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { DescriptorTransformation } from "#ie/vehicle-positions/workers/vehicle-descriptors/transformations/DescriptorTransformation";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";
import { InputDataFixture } from "../fixtures/InputDataFixture";
import { container } from "@golemio/core/dist/shared/tsyringe";

describe("DescriptorTransformation", () => {
    let sandbox: SinonSandbox;

    const createContainer = () =>
        container
            .createChildContainer()
            .register(VPContainerToken.DescriptorTransformation, DescriptorTransformation)
            .resolve<DescriptorTransformation>(VPContainerToken.DescriptorTransformation);

    before(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        container.clearInstances();
        sinon.reset();
        sandbox.restore();
    });

    after(() => {
        sinon.restore();
    });

    it("should transform input data", async () => {
        const transformation = createContainer();
        const result = await transformation.transform(InputDataFixture);
        expect(result).to.deep.equal([
            {
                id: 1,
                state: "dilny",
                registration_number: 3,
                registration_number_index: 1,
                license_plate: "4SX XXXX",
                operator: "ČSAD MHD Kladno",
                manufacturer: "Scania",
                type: "Citywide LF 12M CNG",
                traction: "autobus",
                gtfs_route_type: 3,
                is_wheelchair_accessible: true,
                is_air_conditioned: true,
                has_usb_chargers: true,
                paint: "MAD Kladno, bílá s červeným pruhem",
                thumbnail_url: "https://xxxxx.jpg",
                photo_url: "https://xxxxx.jpg",
            },
            {
                id: 2,
                state: "zar",
                registration_number: 4100,
                registration_number_index: 1,
                license_plate: null,
                operator: "Dopravní podnik hl. m. Prahy",
                manufacturer: "Ringhoffer",
                type: "mv",
                traction: "tramvaj",
                gtfs_route_type: 0,
                is_wheelchair_accessible: null,
                is_air_conditioned: false,
                has_usb_chargers: false,
                paint: "červeno-bílá",
                thumbnail_url: "https://xxxxx.jpg",
                photo_url: "https://xxxxx.jpg",
            },
            {
                id: 3,
                state: "zar",
                registration_number: 3001,
                registration_number_index: 5,
                license_plate: "ELX XXXX",
                operator: "Dopravní podnik hl. m. Prahy",
                manufacturer: "Škoda",
                type: "36BB E'CITY",
                traction: "elektrobus",
                gtfs_route_type: 3,
                is_wheelchair_accessible: true,
                is_air_conditioned: true,
                has_usb_chargers: false,
                paint: "schéma PID, šedá s červenými svislými pruhy",
                thumbnail_url: null,
                photo_url: null,
            },
            {
                id: 4,
                state: "dunno",
                registration_number: 9857,
                registration_number_index: 1,
                license_plate: "4SX XXXX",
                operator: "ARRIVA STŘEDNÍ ČECHY",
                manufacturer: "Setra",
                type: "S 419 UL",
                traction: "autobus",
                gtfs_route_type: 3,
                is_wheelchair_accessible: true,
                is_air_conditioned: true,
                has_usb_chargers: true,
                paint: "schéma PID, červeno-bílo-modrá",
                thumbnail_url: "https://xxxxx.jpg",
                photo_url: "https://xxxxx.jpg",
            },
            {
                id: 5,
                state: "vrak",
                registration_number: 3,
                registration_number_index: 1,
                license_plate: "4SX XXXX",
                operator: "ČSAD MHD Kladno",
                manufacturer: "Scania",
                type: "Citywide LF 12M CNG",
                traction: "autobus",
                gtfs_route_type: 3,
                is_wheelchair_accessible: true,
                is_air_conditioned: true,
                has_usb_chargers: true,
                paint: "MAD Kladno, bílá s červeným pruhem",
                thumbnail_url: "https://xxxxx.jpg",
                photo_url: "https://xxxxx.jpg",
            },
        ]);
    });
});
