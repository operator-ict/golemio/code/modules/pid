import { VehicleIdGenerator } from "#ie/vehicle-positions/workers/gtfs-rt/helpers/VehicleIdGenerator";
import { expect } from "chai";
import { ProviderSourceTypeEnum } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/ProviderSourceTypeEnum";

describe("VehicleId generator helper", () => {
    it("returns vehicle id for a train", () => {
        expect(
            VehicleIdGenerator.getVehicleId("trip_id", 2, ProviderSourceTypeEnum.Http, null, 12, null, null, null)
        ).to.be.equal("train-12");
    });

    it("returns vehicle id for metro", () => {
        expect(
            VehicleIdGenerator.getVehicleId(
                "2024-03-22T10:04:35+02:00_991_294_220901_32_31",
                1,
                ProviderSourceTypeEnum.TcpMetro,
                "A",
                null,
                null,
                32,
                33
            )
        ).to.be.equal("metro-A-33-32");
    });

    it("returns vehicle id for service", () => {
        expect(
            VehicleIdGenerator.getVehicleId("trip_id", 3, ProviderSourceTypeEnum.TcpCommon, null, null, 186, null, null)
        ).to.be.equal("service-3-186");
    });

    it("returns vehicle id for Arriva bus service", () => {
        expect(
            VehicleIdGenerator.getVehicleId(
                "2024-03-22T10:04:35+02:00_163_1047_231210_867377023797362",
                3,
                ProviderSourceTypeEnum.TcpRegionalBus,
                null,
                null,
                186,
                null,
                null
            )
        ).to.be.equal("service-3-867377023797362");
    });
});
