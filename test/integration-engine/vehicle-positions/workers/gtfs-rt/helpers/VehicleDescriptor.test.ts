import { VehicleDescriptor } from "#ie/vehicle-positions/workers/gtfs-rt/helpers/VehicleDescriptor";
import { expect } from "chai";
import { GtfsRtTripDataFixture } from "../data/GtfsRtTripData.fixture";

describe("VehicleDescriptor", () => {
    const vehicleDescriptor = new VehicleDescriptor();

    it("returns correct bus/generic descriptor", () => {
        const expected = {
            id: "service-3-12233",
            label: "12233",
            ".transit_realtime.ovapiVehicleDescriptor": {
                wheelchairAccessible: true,
                vehicleType: undefined,
            },
        };
        const actual = vehicleDescriptor.getVehicleDescriptor(GtfsRtTripDataFixture[2]);
        expect(actual).eql(expected);
    });

    it("returns correct Arriva bus descriptor", () => {
        const expected = {
            id: "service-3-867377023797362",
            label: "12233",
            ".transit_realtime.ovapiVehicleDescriptor": {
                wheelchairAccessible: true,
                vehicleType: undefined,
            },
        };
        const actual = vehicleDescriptor.getVehicleDescriptor(GtfsRtTripDataFixture[3]);
        expect(actual).eql(expected);
    });

    it("returns correct descriptor for metro", () => {
        const expected = {
            id: "metro-A-33-32",
            ".transit_realtime.ovapiVehicleDescriptor": {
                wheelchairAccessible: true,
                vehicleType: undefined,
            },
        };
        const actual = vehicleDescriptor.getVehicleDescriptor(GtfsRtTripDataFixture[0]);
        expect(actual).eql(expected);
    });

    it("returns correct id for a train", () => {
        const expected = "train-12";
        const actual = vehicleDescriptor.getVehicleDescriptor(GtfsRtTripDataFixture[1]);
        expect(actual.id).equal(expected);
    });
});
