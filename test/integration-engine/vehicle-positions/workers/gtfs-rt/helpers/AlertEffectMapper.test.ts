import { getGtfsEffectByEventType } from "#ie/vehicle-positions/workers/gtfs-rt/helpers/AlertEffectMapper";
import { GtfsAlertEffectEnum } from "#ie/vehicle-positions/workers/gtfs-rt/helpers/enums/AlertEffectEnum";
import { expect } from "chai";

describe("AlertEffectMapper", () => {
    it("maps effects by correct event types", () => {
        const eventTypesMap = {
            1: GtfsAlertEffectEnum.REDUCED_SERVICE, // PROVOZ_ZASTAVEN
            4: GtfsAlertEffectEnum.REDUCED_SERVICE, // PROVOZ_OMEZEN
            8: GtfsAlertEffectEnum.OTHER_EFFECT, // PROVOZ_OBNOVEN
            128: GtfsAlertEffectEnum.REDUCED_SERVICE, // NEODJETI_SPOJE
            2048: GtfsAlertEffectEnum.SIGNIFICANT_DELAYS, // ZPOZDENI_SPOJE
            4096: GtfsAlertEffectEnum.SIGNIFICANT_DELAYS, // ZPOZDENI_SPOJU
            4097: GtfsAlertEffectEnum.REDUCED_SERVICE, // Zpoždění spojů, Provoz zastaven
            4100: GtfsAlertEffectEnum.REDUCED_SERVICE, // Zpoždění spojů, Provoz omezen
            4232: GtfsAlertEffectEnum.REDUCED_SERVICE, // Zpoždění spojů,Neodjetí spoje,Provoz obnoven
            8192: GtfsAlertEffectEnum.NO_SERVICE, // STANICE_UZAVRENA
            16384: GtfsAlertEffectEnum.OTHER_EFFECT, // Omezení přístupu do stanice
            131072: GtfsAlertEffectEnum.ADDITIONAL_SERVICE, // POSILENI_SPOJU
            262144: GtfsAlertEffectEnum.MODIFIED_SERVICE, // Rozvázání přestupní vazby
            524288: GtfsAlertEffectEnum.OTHER_EFFECT, // OSTATNI
            1048576: GtfsAlertEffectEnum.DETOUR, // ODKLON
            2097152: GtfsAlertEffectEnum.NO_SERVICE, // ZRUSENI_ZASTAVKY
            16777216: GtfsAlertEffectEnum.REDUCED_SERVICE, // PRERUSENI_PROVOZU
            3145728: GtfsAlertEffectEnum.DETOUR, // Zrušení zastávky,Odklon
            8388608: GtfsAlertEffectEnum.MODIFIED_SERVICE, // NAHRADNI_DOPRAVA
            8388736: GtfsAlertEffectEnum.MODIFIED_SERVICE, // Náhradní doprava,Neodjetí spoje
            268435456: GtfsAlertEffectEnum.MODIFIED_SERVICE, // ZMENA_ZASTAVEK
        };

        for (const [eventType, effect] of Object.entries(eventTypesMap)) {
            expect(getGtfsEffectByEventType(Number(eventType))).to.be.equal(effect);
        }
    });
});
