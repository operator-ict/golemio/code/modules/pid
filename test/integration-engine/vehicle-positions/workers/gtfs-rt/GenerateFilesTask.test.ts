import { GTFSRouteTypeEnum } from "#helpers/RouteTypeEnums";
import { VehicleDescriptor } from "#ie/vehicle-positions/workers/gtfs-rt/helpers/VehicleDescriptor";
import { VehicleIdGenerator } from "#ie/vehicle-positions/workers/gtfs-rt/helpers/VehicleIdGenerator";
import { GenerateFilesTask } from "#ie/vehicle-positions/workers/gtfs-rt/tasks/GenerateFilesTask";
import { IPublicStopTimeCacheDto } from "#sch/vehicle-positions/redis/interfaces/IPublicStopTimeCacheDto";
import { PostgresConnector, RedisConnector } from "@golemio/core/dist/integration-engine/connectors";
import { CloudflareCachePurgeWebhook } from "@golemio/core/dist/integration-engine/workers/webhooks/CloudflareCachePurgeWebhook";
import moment from "@golemio/core/dist/shared/moment-timezone";
import { transit_realtime as gtfsRealtime } from "@golemio/ovapi-gtfs-realtime-bindings";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonFakeTimers, SinonSandbox, SinonStub } from "sinon";
import { StatePositionEnum } from "src/const";
import { getPublicStopTimeCacheFixture } from "./data/getPublicStopTimeCache.fixture";
chai.use(chaiAsPromised);

describe("GtfsRealTimeWorker - GenerateFilesTask", () => {
    let sandbox: SinonSandbox;
    let task: GenerateFilesTask;
    let clock: SinonFakeTimers;
    let stubbedRedisHset: SinonStub;
    let stubbedRedisSet: SinonStub;
    let stubbedStopTimeRepository: SinonStub;
    let vehicleDescriptor: VehicleDescriptor;
    let nowTime = moment().tz("Europe/Prague").hour(11).minute(40).second(0);
    const todayYMD = nowTime.format("YYYY-MM-DD");

    function getPublicStopTimeCacheFixtureDataForVehicle(vehicleIds: string[]) {
        return new Map(
            vehicleIds.map((vehicleId) => [
                vehicleId,
                getPublicStopTimeCacheFixture(todayYMD)
                    .filter((item) =>
                        vehicleId.includes(
                            VehicleIdGenerator.getVehicleId(
                                item.rt_trip_id,
                                item.gtfs_route_type,
                                item.provider_source_type,
                                item.gtfs_route_short_name,
                                item.cis_trip_number,
                                item.vehicle_registration_number,
                                item.run_number,
                                item.internal_run_number
                            )
                        )
                    )
                    .map(
                        ({
                            rt_trip_id,
                            gtfs_route_type,
                            provider_source_type,
                            gtfs_route_short_name,
                            cis_trip_number,
                            vehicle_registration_number,
                            run_number,
                            internal_run_number,
                            ...item
                        }) => item
                    ),
            ])
        );
    }

    beforeEach(async () => {
        sandbox = sinon.createSandbox();
        clock = sinon.useFakeTimers({
            now: new Date(nowTime.valueOf()),
            shouldAdvanceTime: true,
        });

        await PostgresConnector.connect();
        sandbox.stub(RedisConnector, "getConnection");

        task = new GenerateFilesTask("test.test");
        stubbedRedisHset = sandbox.stub(task["gtfsRtRedisRepository"], "hset");
        stubbedRedisSet = sandbox.stub(task["gtfsRtRedisRepository"], "set");
        stubbedStopTimeRepository = sandbox
            .stub(task["stopTimeRepository"], "getPublicStopTimeCache")
            .callsFake((vehicleIds) => Promise.resolve(getPublicStopTimeCacheFixtureDataForVehicle(vehicleIds)));

        vehicleDescriptor = new VehicleDescriptor();
    });

    afterEach(() => {
        sandbox?.restore();
        clock?.restore();
    });

    it("should not generate GTFS-RT feeds when no RT data available", async () => {
        const logWarnStub = sandbox.stub(task["logger"], "warn");
        sandbox.stub(task["tripsRepository"], "findAllForGTFSRt").resolves([]);

        stubbedStopTimeRepository.restore();
        stubbedStopTimeRepository = sandbox
            .stub(task["stopTimeRepository"], "getPublicStopTimeCache")
            .callsFake(async () => new Map());

        await task["execute"]();

        expect(stubbedRedisHset.called).to.be.false;
        expect(logWarnStub.called).to.be.true;
        expect(logWarnStub.getCall(0).args[0]).to.equal(
            "No stop time ids found for GTFS-RT generation. Most likely no vehicles are running."
        );
    });

    it("should generate GTFS-RT trip updates feed", async () => {
        await task["execute"]();
        const tripUpdatesBinary = stubbedRedisHset.getCall(0).args[1];
        const tripUpdates = gtfsRealtime.FeedMessage.decode(Buffer.from(tripUpdatesBinary, "binary"));
        const tripEntity = tripUpdates.entity.find((entity) => entity.id === todayYMD + "T10:04:35+02:00_991_294_220901_9_9");

        expect(tripEntity).to.be.an("object");
        expect(tripEntity!.id).to.equal(todayYMD + "T10:04:35+02:00_991_294_220901_9_9");
        expect(tripEntity!.tripUpdate?.trip.tripId).to.equal("991_294_220901");
        const ovapiStopTimeUpdate = tripEntity!.tripUpdate?.stopTimeUpdate?.find((stu) => stu.stopId === "U400Z101P")?.[
            ".transit_realtime.ovapiStopTimeUpdate"
        ];
        expect(ovapiStopTimeUpdate?.scheduledTrack).to.equal("A1");
        expect(ovapiStopTimeUpdate?.actualTrack).to.equal("A1");
        expect(tripEntity!.vehicle).to.be.null;
        expect(tripEntity!.alert).to.be.null;
    });

    it("should generate GTFS-RT trip updates feed for train trip with known platform code", async () => {
        await task["execute"]();
        const tripUpdatesBinary = stubbedRedisHset.getCall(0).args[1];
        const tripUpdates = gtfsRealtime.FeedMessage.decode(Buffer.from(tripUpdatesBinary, "binary"));

        const entity = tripUpdates.entity.find((entity) => entity.id === todayYMD + "T10:05:00Z_2530");
        expect(entity).to.be.an("object");

        const stopTimeUpdate = entity?.tripUpdate?.stopTimeUpdate;
        expect(stopTimeUpdate).to.be.an("array").of.length(1);
        expect(stopTimeUpdate?.[0][".transit_realtime.ovapiStopTimeUpdate"]).to.be.an("object");
        expect(stopTimeUpdate?.[0][".transit_realtime.ovapiStopTimeUpdate"]?.actualTrack).to.equal("1A/1");
    });

    it("should generate GTFS-RT trip updates feed including past stops and delays", async () => {
        await task["execute"]();
        const tripUpdatesBinary = stubbedRedisHset.getCall(0).args[1];
        const tripUpdates = gtfsRealtime.FeedMessage.decode(Buffer.from(tripUpdatesBinary, "binary"));
        const entity = tripUpdates.entity.find((entity) => entity.id === "2019-05-26T09:13:00Z_XX_152_37_201230");

        expect(entity).to.be.an("object").and.not.null;
        expect(entity?.tripUpdate?.stopTimeUpdate).to.be.an("array").of.length(17);
        expect(entity?.tripUpdate?.stopTimeUpdate?.every((stop) => typeof stop.arrival?.delay === "number")).to.be.true;
        expect(entity?.tripUpdate?.stopTimeUpdate?.every((stop) => typeof stop.departure?.delay === "number")).to.be.true;
    });

    it(
        "should generate GTFS-RT trip updates feed with arrival delays copied from departure when unavailable and vice" +
            " versa",
        async () => {
            stubbedStopTimeRepository.restore();
            stubbedStopTimeRepository = sandbox
                .stub(task["stopTimeRepository"], "getPublicStopTimeCache")
                .callsFake(async (vehicleIds) => {
                    const data = structuredClone(await getPublicStopTimeCacheFixtureDataForVehicle(vehicleIds));
                    const vehicleData = data.get("service-3-8582-152_37_201230")!;
                    for (let i = 0; i < vehicleData.length; i++) {
                        if (i % 2 === 0) {
                            vehicleData[i]["arr_delay"] = null;
                        } else {
                            vehicleData[i]["dep_delay"] = null;
                        }
                    }
                    return data;
                });

            await task["execute"]();
            const tripUpdatesBinary = stubbedRedisHset.getCall(0).args[1];
            const tripUpdates = gtfsRealtime.FeedMessage.decode(Buffer.from(tripUpdatesBinary, "binary"));
            const rawEntity = tripUpdates.entity.find((entity) => entity.id === "2019-05-26T09:13:00Z_XX_152_37_201230");
            const entity = JSON.parse(JSON.stringify(rawEntity)) as gtfsRealtime.IFeedEntity;

            expect(entity).to.be.an("object").and.not.null;
            expect(entity?.tripUpdate?.stopTimeUpdate).to.be.an("array").of.length(17);
            expect(entity?.tripUpdate?.stopTimeUpdate?.every((stop) => typeof stop.arrival?.delay === "number")).to.be.true;
            expect(entity?.tripUpdate?.stopTimeUpdate?.every((stop) => typeof stop.departure?.delay === "number")).to.be.true;
        }
    );

    it(
        "should generate GTFS-RT trip updates feed with NO_DATA stop time update scheduleRelationship when no delay is" +
            " available for some updates",
        async () => {
            stubbedStopTimeRepository.restore();
            stubbedStopTimeRepository = sandbox
                .stub(task["stopTimeRepository"], "getPublicStopTimeCache")
                .callsFake(async (vehicleIds) => {
                    const data = structuredClone(await getPublicStopTimeCacheFixtureDataForVehicle(vehicleIds));
                    const vehicleData = data.get("service-3-8582-152_37_201230")!;
                    for (let i = 0; i + 1 < vehicleData.length; i += 2) {
                        vehicleData[i]["arr_delay"] = null;
                        vehicleData[i]["dep_delay"] = null;
                    }
                    return data;
                });

            await task["execute"]();
            const tripUpdatesBinary = stubbedRedisHset.getCall(0).args[1];
            const tripUpdates = gtfsRealtime.FeedMessage.decode(Buffer.from(tripUpdatesBinary, "binary"));
            const rawEntity = tripUpdates.entity.find((entity) => entity.id === "2019-05-26T09:13:00Z_XX_152_37_201230");
            const entity = JSON.parse(JSON.stringify(rawEntity)) as gtfsRealtime.IFeedEntity;

            expect(entity).to.be.an("object").and.not.null;
            expect(entity?.tripUpdate?.stopTimeUpdate).to.be.an("array").of.length(17);
            for (let i = 0; i + 1 < (entity?.tripUpdate?.stopTimeUpdate?.length ?? 0); i += 2) {
                expect(entity?.tripUpdate?.stopTimeUpdate?.at(i)?.scheduleRelationship).to.equal("NO_DATA");
            }
        }
    );

    it("should generate GTFS-RT vehicle positions feed", async () => {
        await task["execute"]();
        const vehiclePositionsBinary = stubbedRedisHset.getCall(1).args[1];
        const vehiclePositions = gtfsRealtime.FeedMessage.decode(Buffer.from(vehiclePositionsBinary, "binary"));
        const vehicleEntity = vehiclePositions.entity.find(
            (entity) => entity.id === todayYMD + "T10:04:35+02:00_991_294_220901_9_9"
        );

        expect(vehicleEntity!.id).to.equal(todayYMD + "T10:04:35+02:00_991_294_220901_9_9");
        expect(vehicleEntity!.vehicle?.trip?.tripId).to.equal("991_294_220901");
        expect(vehicleEntity!.tripUpdate).to.be.null;
        expect(vehicleEntity!.alert).to.be.null;
    });

    it("should generate GTFS-RT vehicle positions feed without duplicate VehicleDescriptor IDs", async () => {
        const vehicleId = "service-3-1861";
        const tripsRepositoryDindAllForGTFSRtStub = sandbox.stub(task["tripsRepository"], "findAllForGTFSRt");
        tripsRepositoryDindAllForGTFSRtStub.callsFake(async () => {
            const data = await tripsRepositoryDindAllForGTFSRtStub.wrappedMethod(new Date());
            const duplicates = data.filter((datum) => vehicleDescriptor.getVehicleDescriptor(datum).id === vehicleId);
            duplicates[1].start_timestamp = "2023-11-14T09:50:00.000Z";
            duplicates[1].start_time = "10:50";
            duplicates[2].start_timestamp = "2023-11-14T09:30:00.000Z";
            duplicates[2].start_time = "10:30";
            return data;
        });

        await task["execute"]();
        const vehiclePositionsBinary = stubbedRedisHset.getCall(1).args[1];
        const vehiclePositions = gtfsRealtime.FeedMessage.decode(Buffer.from(vehiclePositionsBinary, "binary"));

        const selectEntities = vehiclePositions.entity.filter((entity) => entity.vehicle?.vehicle?.id === vehicleId);
        expect(selectEntities).to.be.an("array").of.length(1);
        expect(selectEntities[0].vehicle?.trip?.startTime).to.equal("10:30");
    });

    it("should generate GTFS-RT vehicle positions feed without duplicate VehicleDescriptor IDs #2", async () => {
        const vehicleId = "service-3-1861";
        const tripsRepositoryDindAllForGTFSRtStub = sandbox.stub(task["tripsRepository"], "findAllForGTFSRt");
        tripsRepositoryDindAllForGTFSRtStub.callsFake(async () => {
            const data = await tripsRepositoryDindAllForGTFSRtStub.wrappedMethod(new Date());
            const duplicates = data.filter((datum) => vehicleDescriptor.getVehicleDescriptor(datum).id === vehicleId);
            duplicates[1].start_timestamp = "2023-11-14T09:35:00.000Z";
            duplicates[1].start_time = "10:35";
            duplicates[2].start_timestamp = "2023-11-14T09:30:00.000Z";
            duplicates[2].start_time = "10:30";
            duplicates[2].last_position.is_canceled = true;
            duplicates[2].last_position.lat = 0;
            duplicates[2].last_position.lng = 0;

            return duplicates;
        });

        await task["execute"]();
        const vehiclePositionsBinary = stubbedRedisHset.getCall(1).args[1];
        const vehiclePositions = gtfsRealtime.FeedMessage.decode(Buffer.from(vehiclePositionsBinary, "binary"));

        const selectEntities = vehiclePositions.entity.filter((entity) => entity.vehicle?.vehicle?.id === vehicleId);
        expect(selectEntities).to.be.an("array").of.length(1);
        expect(selectEntities[0].vehicle?.trip?.startTime).to.equal("10:35");
    });
    it("Should include canceled trips in tripUpdates", async () => {
        const tripsRepositoryDindAllForGTFSRtStub = sandbox.stub(task["tripsRepository"], "findAllForGTFSRt");
        tripsRepositoryDindAllForGTFSRtStub.callsFake(async () => {
            const data = await tripsRepositoryDindAllForGTFSRtStub.wrappedMethod(new Date());
            const toBeChanged = data.filter((entity) => entity.id === `${todayYMD}T10:05:00Z_2530`)!;
            toBeChanged[0].last_position.lat = 0;
            toBeChanged[0].last_position.lng = 0;
            toBeChanged[0].last_position.is_canceled = true;
            return toBeChanged;
        });
        await task["execute"]();
        const tripUpdatesBinary = stubbedRedisHset.getCall(0).args[1];
        const tripUpdates = gtfsRealtime.FeedMessage.decode(Buffer.from(tripUpdatesBinary, "binary"));
        const entity = tripUpdates.entity.find((entity) => entity.id === `${todayYMD}T10:05:00Z_2530`);
        expect(entity).to.be.an("object").and.not.null;
        expect(entity?.tripUpdate?.stopTimeUpdate).to.be.an("array").of.length(0);
        expect(entity?.tripUpdate?.trip.scheduleRelationship).to.be.eq(3);
    });

    it("should generate GTFS-RT pid feed", async () => {
        await task["execute"]();
        const pidFeedBinary = stubbedRedisHset.getCall(2).args[1];
        const pidFeed = gtfsRealtime.FeedMessage.decode(Buffer.from(pidFeedBinary, "binary"));
        const pidEntity = pidFeed.entity.find((entity) => entity.id === todayYMD + "T10:04:35+02:00_991_294_220901_9_9");

        expect(pidEntity!.id).to.equal(todayYMD + "T10:04:35+02:00_991_294_220901_9_9");
        expect(pidEntity!.tripUpdate?.trip.tripId).to.equal("991_294_220901");
        expect(pidEntity!.vehicle?.trip?.tripId).to.equal("991_294_220901");
        expect(pidEntity!.alert).to.be.null;
    });

    it("should generate GTFS-RT pid feed without duplicate VehicleDescriptor IDs", async () => {
        const vehicleId = "service-3-1861";
        const tripsRepositoryFindAllForGTFSRtStub = sandbox.stub(task["tripsRepository"], "findAllForGTFSRt");
        let duplicatesTripsIds: string[] = [];
        tripsRepositoryFindAllForGTFSRtStub.callsFake(async () => {
            const data = await tripsRepositoryFindAllForGTFSRtStub.wrappedMethod(new Date());
            const duplicates = data.filter((datum) => vehicleDescriptor.getVehicleDescriptor(datum).id === vehicleId);
            duplicatesTripsIds = duplicates.map(({ id }) => id);
            duplicates[1].start_timestamp = "2023-11-14T09:50:00.000Z";
            duplicates[1].start_time = "10:50";
            duplicates[2].start_timestamp = "2023-11-14T09:30:00.000Z";
            duplicates[2].start_time = "10:30";
            return data;
        });

        await task["execute"]();
        const pidFeedBinary = stubbedRedisHset.getCall(2).args[1];
        const pidFeed = gtfsRealtime.FeedMessage.decode(Buffer.from(pidFeedBinary, "binary"));

        const selectEntities = pidFeed.entity.filter((entity) => entity.vehicle?.vehicle?.id === vehicleId);
        expect(selectEntities).to.be.an("array").of.length(1);
        expect(selectEntities[0].vehicle?.trip?.startTime).to.equal("10:30");
        const selectEntities2 = pidFeed.entity.filter((entity) => entity.tripUpdate?.vehicle?.id === vehicleId);
        expect(selectEntities2).to.be.an("array").of.length(4);
        expect(duplicatesTripsIds).to.include.members(selectEntities2.map((entity) => entity.id));
        expect(pidFeed.entity.filter((entity) => duplicatesTripsIds.includes(entity.id))).to.be.of.length(4);
    });

    it("should generate GTFS-RT pid feed without duplicate VehicleDescriptor IDs #2", async () => {
        const vehicleId = "service-3-1861";
        const tripsRepositoryFindAllForGTFSRtStub = sandbox.stub(task["tripsRepository"], "findAllForGTFSRt");
        let duplicatesTripsIds: string[] = [];
        tripsRepositoryFindAllForGTFSRtStub.callsFake(async () => {
            const data = await tripsRepositoryFindAllForGTFSRtStub.wrappedMethod(new Date());
            const duplicates = data.filter((datum) => vehicleDescriptor.getVehicleDescriptor(datum).id === vehicleId);
            duplicatesTripsIds = duplicates.map(({ id }) => id);
            duplicates[1].start_timestamp = "2023-11-14T09:35:00.000Z";
            duplicates[1].start_time = "10:35";
            duplicates[2].start_timestamp = "2023-11-14T09:30:00.000Z";
            duplicates[2].start_time = "10:30";
            duplicates[2].last_position.is_canceled = true;
            return data;
        });

        await task["execute"]();
        const pidFeedBinary = stubbedRedisHset.getCall(2).args[1];
        const pidFeed = gtfsRealtime.FeedMessage.decode(Buffer.from(pidFeedBinary, "binary"));

        const selectEntities = pidFeed.entity.filter((entity) => entity.vehicle?.vehicle?.id === vehicleId);
        expect(selectEntities).to.be.an("array").of.length(1);
        expect(selectEntities[0].vehicle?.trip?.startTime).to.equal("10:35");
        const selectEntities2 = pidFeed.entity.filter((entity) => entity.tripUpdate?.vehicle?.id === vehicleId);
        expect(selectEntities2).to.be.an("array").of.length(3);
        expect(duplicatesTripsIds).to.include.members(selectEntities2.map((entity) => entity.id));
        expect(pidFeed.entity.filter((entity) => duplicatesTripsIds.includes(entity.id))).to.be.of.length(4);
    });

    it("should generate GTFS-RT pid feed including past stops and delays", async () => {
        await task["execute"]();
        const pidFeedBinary = stubbedRedisHset.getCall(2).args[1];
        const pidFeed = gtfsRealtime.FeedMessage.decode(Buffer.from(pidFeedBinary, "binary"));
        const pidEntity = pidFeed.entity.find((entity) => entity.id === "2019-05-26T09:13:00Z_XX_152_37_201230");

        expect(pidEntity).to.be.an("object").and.not.null;
        expect(pidEntity?.tripUpdate?.stopTimeUpdate).to.be.an("array").of.length(17);
        expect(pidEntity?.tripUpdate?.stopTimeUpdate?.every((stop) => typeof stop.arrival?.delay === "number")).to.be.true;
        expect(pidEntity?.tripUpdate?.stopTimeUpdate?.every((stop) => typeof stop.departure?.delay === "number")).to.be.true;
    });

    it("should generate GTFS-RT pid feed with arrival delays copied from departure when unavailable and vice versa", async () => {
        stubbedStopTimeRepository.restore();
        stubbedStopTimeRepository = sandbox
            .stub(task["stopTimeRepository"], "getPublicStopTimeCache")
            .callsFake(async (vehicleIds) => {
                const data = structuredClone(await getPublicStopTimeCacheFixtureDataForVehicle(vehicleIds));
                const vehicleData = data.get("service-3-8582-152_37_201230")!;
                for (let i = 0; i < vehicleData.length; i++) {
                    if (i % 2 === 0) {
                        vehicleData[i]["arr_delay"] = null;
                    } else {
                        vehicleData[i]["dep_delay"] = null;
                    }
                }
                return data;
            });

        await task["execute"]();
        const pidFeedBinary = stubbedRedisHset.getCall(2).args[1];
        const pidFeed = gtfsRealtime.FeedMessage.decode(Buffer.from(pidFeedBinary, "binary"));
        const rawEntity = pidFeed.entity.find((entity) => entity.id === "2019-05-26T09:13:00Z_XX_152_37_201230");
        const entity = JSON.parse(JSON.stringify(rawEntity)) as gtfsRealtime.IFeedEntity;

        expect(entity).to.be.an("object").and.not.null;
        expect(entity?.tripUpdate?.stopTimeUpdate).to.be.an("array").of.length(17);
        expect(entity?.tripUpdate?.stopTimeUpdate?.every((stop) => typeof stop.arrival?.delay === "number")).to.be.true;
        expect(entity?.tripUpdate?.stopTimeUpdate?.every((stop) => typeof stop.departure?.delay === "number")).to.be.true;
    });

    it(
        "should generate GTFS-RT pid feed with NO_DATA stop time update scheduleRelationship when no delay is available for " +
            "some updates",
        async () => {
            stubbedStopTimeRepository.restore();
            stubbedStopTimeRepository = sandbox
                .stub(task["stopTimeRepository"], "getPublicStopTimeCache")
                .callsFake(async (vehicleIds) => {
                    const data = structuredClone(await getPublicStopTimeCacheFixtureDataForVehicle(vehicleIds));
                    const vehicleData = data.get("service-3-8582-152_37_201230")!;
                    for (let i = 0; i + 1 < vehicleData.length; i += 2) {
                        vehicleData[i]["arr_delay"] = null;
                        vehicleData[i]["dep_delay"] = null;
                    }
                    return data;
                });

            await task["execute"]();
            const pidFeedBinary = stubbedRedisHset.getCall(2).args[1];
            const pidFeed = gtfsRealtime.FeedMessage.decode(Buffer.from(pidFeedBinary, "binary"));
            const rawEntity = pidFeed.entity.find((entity) => entity.id === "2019-05-26T09:13:00Z_XX_152_37_201230");
            const entity = JSON.parse(JSON.stringify(rawEntity)) as gtfsRealtime.IFeedEntity;

            expect(entity).to.be.an("object").and.not.null;
            expect(entity?.tripUpdate?.stopTimeUpdate).to.be.an("array").of.length(17);
            for (let i = 0; i + 1 < (entity?.tripUpdate?.stopTimeUpdate?.length ?? 0); i += 2) {
                expect(entity?.tripUpdate?.stopTimeUpdate?.at(i)?.scheduleRelationship).to.equal("NO_DATA");
            }
        }
    );

    it("should generate GTFS-RT alerts feed", async () => {
        await task["execute"]();
        const alertsBinary = stubbedRedisHset.getCall(3).args[1];
        const alerts = gtfsRealtime.FeedMessage.decode(Buffer.from(alertsBinary, "binary"));

        expect(alerts.entity[0].id).to.equal("alert-83-3");
        expect(alerts.entity[0].alert?.headerText?.translation?.[0].text).to.equal(
            "Sídliště Pankrác: trvalé přemístění zastávky"
        );
        expect(alerts.entity[0].tripUpdate).to.be.null;
        expect(alerts.entity[0].vehicle).to.be.null;
    });

    it("should execute CachePurgeWebhook after task execute", async () => {
        const cloudflareCachePurgeWebhookExecuteStubPromise = new Promise<void>((resolve) => resolve());
        sandbox.stub(CloudflareCachePurgeWebhook.prototype, "execute").returns(cloudflareCachePurgeWebhookExecuteStubPromise);
        await task["execute"]();
        expect(await cloudflareCachePurgeWebhookExecuteStubPromise).to.be.undefined;
    });

    describe("generateStopTimeUpdate", () => {
        const stopTimeCache: IPublicStopTimeCacheDto[] = [
            {
                arr_delay: 60,
                dep_delay: 60,
                sequence: 1,
                cis_stop_platform_code: null,
                platform_code: null,
                stop_id: "U400Z101P",
            },
            {
                arr_delay: -70,
                dep_delay: 10,
                sequence: 2,
                cis_stop_platform_code: null,
                platform_code: null,
                stop_id: "U401Z101P",
            },
        ];

        it("should generate stop time update with delay at stop", async () => {
            const routeType = GTFSRouteTypeEnum.TRAM;
            const stopTimeUpdate = task["generateStopTimeUpdate"](routeType, stopTimeCache, {
                delay: 25,
                state_position: StatePositionEnum.BEFORE_TRACK,
            } as any);

            expect(stopTimeUpdate[0].arrival?.delay).to.equal(60);
            expect(stopTimeUpdate[0].departure?.delay).to.equal(60);
            expect(stopTimeUpdate[1].arrival?.delay).to.equal(-70);
            expect(stopTimeUpdate[1].departure?.delay).to.equal(10);
        });

        it("should generate stop time update with fallback delay at stop (delay 0)", async () => {
            const routeType = GTFSRouteTypeEnum.TRAM;
            const stopTimeUpdate = task["generateStopTimeUpdate"](routeType, stopTimeCache, {
                delay: 0,
                state_position: StatePositionEnum.BEFORE_TRACK,
            } as any);

            expect(stopTimeUpdate.every((stu) => stu.arrival?.delay === 0)).to.be.true;
            expect(stopTimeUpdate.every((stu) => stu.departure?.delay === 0)).to.be.true;
        });

        it("should generate stop time update with fallback delay at stop (delay null)", async () => {
            const routeType = GTFSRouteTypeEnum.TRAM;
            const stopTimeUpdate = task["generateStopTimeUpdate"](routeType, stopTimeCache, {
                delay: null,
                state_position: StatePositionEnum.BEFORE_TRACK,
            } as any);

            expect(stopTimeUpdate.every((stu) => stu.arrival?.delay === 0)).to.be.true;
            expect(stopTimeUpdate.every((stu) => stu.departure?.delay === 0)).to.be.true;
        });
    });
});
