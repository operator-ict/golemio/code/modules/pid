import { RopidVYMIEventsRoutesModel } from "#ie/ropid-vymi";
import { AlertsTransformation } from "#ie/vehicle-positions/workers/gtfs-rt/transformations/AlertsTransformation";
import { PostgresConnector, RedisConnector } from "@golemio/core/dist/integration-engine/connectors";
import { expect } from "chai";
import { gtfsRtAlertFixture } from "../data/gtfsRtAlert.fixture";
import { ropidVYMIEventOutputFixture } from "../data/RopidVYMIEventOutput.fixture";

describe("AlertsTransformation", async () => {
    it("transforms valid data correctly", async () => {
        await PostgresConnector.connect();
        await RedisConnector.connect();

        const transformer = new AlertsTransformation(new RopidVYMIEventsRoutesModel());
        const transformedResult = await transformer.transform(ropidVYMIEventOutputFixture);

        expect(transformedResult).to.deep.equal(gtfsRtAlertFixture);
    });
});
