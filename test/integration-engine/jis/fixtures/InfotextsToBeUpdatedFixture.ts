import { InfotextSeverityLevel } from "#og/shared/constants/jis/InfotextSeverityLevelEnum";
import { IJISInfotext } from "#sch/jis/models/interfaces";

/* eslint-disable max-len */
export const dataInDb: IJISInfotext[] = [
    {
        id: "40507cee-6468-488e-ba78-e36d9c20e67c",
        severity_level: InfotextSeverityLevel.Severe,
        display_type: "INLINE",
        active_period_start: new Date("2024-02-07T12:11:00.000Z"),
        active_period_end: null,
        description_text: {
            cs: "Provoz tramvají směrem z centra na Starý Hloubětín z důvodu údržby přerušen do neděle 18:00. Zaveden autobus X-8.",
            en: "No tram service in direction to Starý Hloubětín until Sunday 18:00 due to track maintenance. Replacement bus X-8 instated.",
        },
        created_timestamp: new Date("2024-12-04T12:54:45.109Z"),
        updated_timestamp: new Date("2024-12-04T12:54:45.109Z"),
    },
    {
        id: "6bdc3fdb-dd18-438b-878a-7972bddb0d92",
        severity_level: InfotextSeverityLevel.Severe,
        display_type: "INLINE",
        active_period_start: new Date("2024-03-05T06:30:00.000Z"),
        active_period_end: null,
        description_text: {
            cs: "cca 8:30 - 9:15: VŠECHNY LINKY ODKLON do zast. BUDĚJOVICKÁ, stanice Kačerov uzavřena.",
            en: "approx. 8:30 - 9:15: ALL LINES DEPARTURE to stop. BUDĚJOVICKÁ, station Kačerov closed.",
        },
        created_timestamp: new Date("2024-12-04T12:54:45.109Z"),
        updated_timestamp: new Date("2024-12-04T12:54:45.109Z"),
    },
    {
        id: "eba39046-86d2-4f8c-aa39-932dfcfd6d0f",
        severity_level: InfotextSeverityLevel.Severe,
        display_type: "INLINE",
        active_period_start: new Date("2024-03-04T11:11:00.000Z"),
        active_period_end: null,
        description_text: {
            cs: "Cizí dopravní nehoda ve Psárském lese. Linka 332 je odkloněna přes Libeř, přičemž neobsluhuje zastávku Psáry,Domov Laguna.",
            en: "Traffic accident in the Psar Forest. Line 332 is diverted via Libeř, while it does not serve the stop Psáry,Domov Laguna..",
        },

        created_timestamp: new Date("2024-12-04T12:54:45.109Z"),
        updated_timestamp: new Date("2024-12-04T12:54:45.109Z"),
    },
    {
        id: "8556cf18-786b-453a-a516-85c8fe2b9878",
        severity_level: InfotextSeverityLevel.Severe,
        display_type: "INLINE",
        active_period_start: new Date("2024-03-02T08:00:00.000Z"),
        active_period_end: null,
        description_text: {
            cs: "Dnes v noci tramvaje mimo provoz. Zastávky náhradních autobusů X8 a X90: směr Lehovec - vlevo, u Lidlu; směr Palmovka - na chodníku u původní tramvajové zastávky.",
            en: "Trams out of service tonight. Stopping points for replacement buses X8 and X90: direction Lehovec - left, near Lidl; direction Palmovka - on the pavement near the original tram stop.",
        },

        created_timestamp: new Date("2024-12-04T12:54:45.109Z"),
        updated_timestamp: new Date("2024-12-04T12:54:45.109Z"),
    },
    {
        id: "9923c199-d61c-47fa-9f7e-14d01473eb18",
        severity_level: InfotextSeverityLevel.Severe,
        display_type: "GENERAL",
        active_period_start: new Date("2024-03-18T14:00:00.000Z"),
        active_period_end: null,
        description_text: {
            cs: "Dnes v sobotu 8. konání půl maratonu. Od 16h do 23h změna trasy linek 392, 395, 500, 501, 502, 503, 506, 517, 531, 754. Podrobnosti na zastávkách a pid.cz/zmeny",
            en: "Today on Saturday the 8th holding of the half marathon. From 16h to 23h change of route of lines 392, 395, 500, 501, 502, 503, 506, 517, 531, 754. Details at the stops and pid.cz/changes",
        },

        created_timestamp: new Date("2024-12-04T12:54:45.109Z"),
        updated_timestamp: new Date("2024-12-04T12:54:45.109Z"),
    },
    {
        id: "0a3c97d0-43d4-4882-8a1d-27079044f51b",
        severity_level: InfotextSeverityLevel.Severe,
        display_type: "INLINE",
        active_period_start: new Date("2024-03-25T08:00:00.000Z"),
        active_period_end: null,
        description_text: {
            cs: "Do 15:00 je zastávka mimo provoz. Linky 1, 12 a 25 odjíždí ze stanoviště D, od nábřeží. Linky 8 jede ze stanoviště E od Výstaviště, linka 26 zde neprojíždí.",
            en: "The stop is out of service until 15:00. Lines 1, 12 and 25 depart from Station D, from the waterfront. Line 8 departs from station E from the Exhibition Grounds, line 26 does not run here.",
        },

        created_timestamp: new Date("2024-12-04T12:54:45.109Z"),
        updated_timestamp: new Date("2024-12-04T12:54:45.109Z"),
    },
    {
        id: "ba52df69-5db3-4c60-94be-8f4b5442c1e0",
        severity_level: InfotextSeverityLevel.Severe,
        display_type: "INLINE",
        active_period_start: new Date("2024-03-11T11:11:00.000Z"),
        active_period_end: null,
        description_text: {
            cs: "Do 16:00 zde nejezdí tramvaje z důvodu údržby tratě, mezi Palmovkou a Lehovcem využijte náhradní autobus X-8.",
            en: "Trams do not run here until 16:00 due to track maintenance, use the alternative bus X-8 between Palmovka and Lehovec.",
        },

        created_timestamp: new Date("2024-12-04T12:54:45.109Z"),
        updated_timestamp: new Date("2024-12-04T12:54:45.109Z"),
    },
    {
        id: "ee330c7f-cf53-4762-a231-08af9f22442f",
        severity_level: InfotextSeverityLevel.Severe,
        display_type: "INLINE",
        active_period_start: new Date("2024-03-04T08:00:00.000Z"),
        active_period_end: new Date("2024-08-10T16:00:00.000Z"),
        description_text: {
            cs: "Do 18:00 údržba tramvajové trati Hlubočepy – Slivenec. Tram 12 a 20 zkráceny do zastávky Hlubočepy.",
            en: "Until 18:00 maintenance of the tram line Hlubočepy - Slivenec. Tram 12 and 20 shortened to the Hlubočepy stop.",
        },

        created_timestamp: new Date("2024-12-04T12:54:45.109Z"),
        updated_timestamp: new Date("2024-12-04T12:54:45.109Z"),
    },
    {
        id: "265cdc5b-d6da-48e0-a114-6901200c4d46",
        severity_level: InfotextSeverityLevel.Severe,
        display_type: "GENERAL",
        active_period_start: new Date("2024-03-27T08:00:00.000Z"),
        active_period_end: new Date("2024-09-10T22:00:00.000Z"),
        description_text: {
            cs: "Do 10.9. 24:00 nepojedou tramvaje mezi Výstavištěm a Strossmayerovým náměstím. Pro cestu na Strossmayerovo náměstí použijte linky 12 nebo 17.",
            en: "Trams will not run between Výstaviště and Strossmayer Square until 24:00 on 24 April. Use lines 12 or 17 to get to Strossmayer Square.",
        },

        created_timestamp: new Date("2024-12-04T12:54:45.109Z"),
        updated_timestamp: new Date("2024-12-04T12:54:45.109Z"),
    },
    {
        id: "adfd2cef-9c2c-4ed0-bd0d-be0b3216faad",
        severity_level: InfotextSeverityLevel.Severe,
        display_type: "INLINE",
        active_period_start: new Date("2024-03-28T07:00:00.000Z"),
        active_period_end: new Date("2024-09-21T22:00:00.000Z"),
        description_text: {
            cs: "Do nedělní půlnoci přerušen provoz odsud na Výstaviště. Linka 6 jede přes Pražskou tržnici. Polovina spojů 17 odkloněna přes Pražskou tržnici a Nádraží Holešovice. Druhá polovina pokračuje jako 27 přes Letnou na Špejchar. Využijte náhradní autobus X6.",
            en: "Until Sunday midnight no tram service from here to Výstaviště. Tram 6 diverted via Pražská tržnice. Every other tram 17 diverted via Pražská tržnice and Nádraží Holešovice. The rest of 17 turns left to end at Špejchar. Use substitute bus X6.",
        },

        created_timestamp: new Date("2024-12-04T12:54:45.109Z"),
        updated_timestamp: new Date("2024-12-04T12:54:45.109Z"),
    },
    {
        id: "15bd72da-a3da-484b-b592-742deab84b55",
        severity_level: InfotextSeverityLevel.Warning,
        display_type: "INLINE",
        active_period_start: new Date("2024-03-22T11:11:00.000Z"),
        active_period_end: new Date("2024-10-06T11:11:00.000Z"),
        description_text: {
            cs: "Do pondělní půlnoci probíhá výluka metra C mezi I. P. Pavlova a Nádraží Holešovice. Náhradní autobus X-C navazuje ze zastávky I. P. Pavlova.",
            en: "Until Monday midnight, the C metro station is closed between I. P. Pavlova and Nádraží Holešovice. A replacement bus X-C will connect from the stop I. P. Pavlova.",
        },

        created_timestamp: new Date("2024-12-04T12:54:45.109Z"),
        updated_timestamp: new Date("2024-12-04T12:54:45.109Z"),
    },
    {
        id: "a7446544-a907-4b9f-877b-b29458ad9c82",
        severity_level: InfotextSeverityLevel.Severe,
        display_type: "INLINE",
        active_period_start: new Date("2024-03-16T08:00:00.000Z"),
        active_period_end: new Date("2024-05-14T22:00:00.000Z"),
        description_text: {
            cs: "Do pondělní půlnoci probíhá výluka metra C mezi Nádražím Holešovice a I. P. Pavlova. Náhradní autobus X-C odjíždí z nástupiště C (jako bus 201).",
            en: "Until Monday midnight, the C metro station is closed between Nádraž Holešovice and I. P. Pavlova. The replacement bus X-C departs from platform C (as bus 201).",
        },

        created_timestamp: new Date("2024-12-04T12:54:45.109Z"),
        updated_timestamp: new Date("2024-12-04T12:54:45.109Z"),
    },
    {
        id: "07a8449a-52b2-4266-a611-89d2b8758637",
        severity_level: InfotextSeverityLevel.Severe,
        display_type: "INLINE",
        active_period_start: new Date("2024-09-10T15:00:00.000Z"),
        active_period_end: new Date("2024-11-19T22:00:00.000Z"),
        description_text: {
            cs: "Do pondělní půlnoci probíhá výluka metra C mezi Nádražím Holešovice a I. P. Pavlova. Zastávky náhradního autobusu X-C jsou na Florenci nahoře na magistrále.",
            en: "Until Monday midnight, the C metro station is closed between Nádraž Holešovice and I. P. Pavlova. The stops of the replacement bus X-C are at Florence at the top of the main line.",
        },

        created_timestamp: new Date("2024-12-04T12:54:45.109Z"),
        updated_timestamp: new Date("2024-12-04T12:54:45.109Z"),
    },
    {
        id: "59a0b461-df44-42a6-940f-2abf0fd4e980",
        severity_level: InfotextSeverityLevel.Info,
        display_type: "GENERAL",
        active_period_start: new Date("2024-02-29T22:00:00.000Z"),
        active_period_end: new Date("2024-06-11T22:00:00.000Z"),
        description_text: {
            cs: "Do pondělí 12.6. bude uzavřen tento vstup do metra. Vstup od zastávky tramvaje v ulici Plynární zůstává v provozu.",
            en: "This entrance to the metro will be closed until Monday 12 June. The entrance from the tram stop in Plynární Street will remain open.",
        },

        created_timestamp: new Date("2024-12-04T12:54:45.109Z"),
        updated_timestamp: new Date("2024-12-04T12:54:45.109Z"),
    },
    {
        id: "e9a15133-e606-406b-b492-9af05c643e7a",
        severity_level: InfotextSeverityLevel.Severe,
        display_type: "INLINE",
        active_period_start: new Date("2024-03-26T22:00:00.000Z"),
        active_period_end: new Date("2024-09-21T22:00:00.000Z"),
        description_text: {
            cs: "Do pondělí probíhá výluka metra C mezi I. P. Pavlova a Nádraží Holešovice. Náhradní autobus X-C navazuje ze zastávky I. P. Pavlova.",
            en: "Until Monday, the C metro station is closed between I. P. Pavlova and Nádraží Holešovice. The replacement bus X-C will connect from the stop I. P. Pavlova.",
        },

        created_timestamp: new Date("2024-12-04T12:54:45.109Z"),
        updated_timestamp: new Date("2024-12-04T12:54:45.109Z"),
    },
    {
        id: "cdcc2d15-2cdc-4205-b82a-5cc819a79738",
        severity_level: InfotextSeverityLevel.Severe,
        display_type: "INLINE",
        active_period_start: new Date("2023-03-26T22:00:00.000Z"),
        active_period_end: null,
        description_text: { cs: "Nejede to." },

        created_timestamp: new Date("2024-12-04T12:54:45.109Z"),
        updated_timestamp: new Date("2024-12-04T12:54:45.109Z"),
    },
];
