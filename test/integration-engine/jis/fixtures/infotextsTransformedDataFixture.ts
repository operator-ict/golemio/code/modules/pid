import { InfotextSeverityLevel } from "#og/shared/constants/jis/InfotextSeverityLevelEnum";
import { IJISInfotext, IJISInfotextsRopidGTFSStops } from "#sch/jis/models/interfaces";

/* eslint-disable max-len */
export const infotextsTransformedDataFixture: Array<{
    infotext: IJISInfotext;
    infotextsRopidGTFSStops: IJISInfotextsRopidGTFSStops[];
}> = [
    {
        infotext: {
            id: "8556cf18-786b-453a-a516-85c8fe2b9878",
            severity_level: InfotextSeverityLevel.Severe,
            display_type: "INLINE",
            active_period_start: new Date("2024-03-02T08:00:00.000Z"),
            active_period_end: null,
            description_text: {
                cs: "Dnes v noci tramvaje mimo provoz. Zastávky náhradních autobusů X8 a X90: směr Lehovec - vlevo, u Lidlu; směr Palmovka - na chodníku u původní tramvajové zastávky.",
                en: "Trams out of service tonight. Stopping points for replacement buses X8 and X90: direction Lehovec - left, near Lidl; direction Palmovka - on the pavement near the original tram stop.",
            },
            created_timestamp: new Date("2024-03-02T08:00:00.000Z"),
            updated_timestamp: new Date("2024-03-02T08:00:00.000Z"),
        },
        infotextsRopidGTFSStops: [{ infotext_id: "8556cf18-786b-453a-a516-85c8fe2b9878", stop_id: "U72Z1P" }],
    },
    {
        infotext: {
            id: "9923c199-d61c-47fa-9f7e-14d01473eb18",
            severity_level: InfotextSeverityLevel.Severe,
            display_type: "GENERAL",
            active_period_start: new Date("2024-03-18T14:00:00.000Z"),
            active_period_end: null,
            description_text: {
                cs: "Dnes v sobotu 8. konání půl maratonu. Od 16h do 23h změna trasy linek 392, 395, 500, 501, 502, 503, 506, 517, 531, 754. Podrobnosti na zastávkách a pid.cz/zmeny",
                en: "Today on Saturday the 8th holding of the half marathon. From 16h to 23h change of route of lines 392, 395, 500, 501, 502, 503, 506, 517, 531, 754. Details at the stops and pid.cz/changes",
            },
            created_timestamp: new Date("2024-03-02T08:00:00.000Z"),
            updated_timestamp: new Date("2024-03-02T08:00:00.000Z"),
        },
        infotextsRopidGTFSStops: [{ infotext_id: "9923c199-d61c-47fa-9f7e-14d01473eb18", stop_id: "U2549Z1" }],
    },
    {
        infotext: {
            id: "0a3c97d0-43d4-4882-8a1d-27079044f51b",
            severity_level: InfotextSeverityLevel.Severe,
            display_type: "INLINE",
            active_period_start: new Date("2024-03-25T08:00:00.000Z"),
            active_period_end: null,
            description_text: {
                cs: "Do 15:00 je zastávka mimo provoz. Linky 1, 12 a 25 odjíždí ze stanoviště D, od nábřeží. Linky 8 jede ze stanoviště E od Výstaviště, linka 26 zde neprojíždí.",
                en: "The stop is out of service until 15:00. Lines 1, 12 and 25 depart from Station D, from the waterfront. Line 8 departs from station E from the Exhibition Grounds, line 26 does not run here.",
            },
            created_timestamp: new Date("2024-03-02T08:00:00.000Z"),
            updated_timestamp: new Date("2024-03-02T08:00:00.000Z"),
        },
        infotextsRopidGTFSStops: [
            { infotext_id: "0a3c97d0-43d4-4882-8a1d-27079044f51b", stop_id: "U532Z1P" },
            { infotext_id: "0a3c97d0-43d4-4882-8a1d-27079044f51b", stop_id: "U532Z2P" },
        ],
    },
    {
        infotext: {
            id: "ba52df69-5db3-4c60-94be-8f4b5442c1e0",
            severity_level: InfotextSeverityLevel.Severe,
            display_type: "INLINE",
            active_period_start: new Date("2024-03-11T11:11:00.000Z"),
            active_period_end: new Date("2024-09-11T14:00:00.000Z"),
            description_text: {
                cs: "Do 16:00 zde nejezdí tramvaje z důvodu údržby tratě, mezi Palmovkou a Lehovcem využijte náhradní autobus X-8.",
                en: "Trams do not run here until 16:00 due to track maintenance, use the alternative bus X-8 between Palmovka and Lehovec.",
            },
            created_timestamp: new Date("2024-03-02T08:00:00.000Z"),
            updated_timestamp: new Date("2024-03-02T08:00:00.000Z"),
        },
        infotextsRopidGTFSStops: [{ infotext_id: "ba52df69-5db3-4c60-94be-8f4b5442c1e0", stop_id: "U529Z9P" }],
    },
    {
        infotext: {
            id: "265cdc5b-d6da-48e0-a114-6901200c4d46",
            severity_level: InfotextSeverityLevel.Severe,
            display_type: "GENERAL",
            active_period_start: new Date("2024-03-27T08:00:00.000Z"),
            active_period_end: new Date("2024-09-10T22:00:00.000Z"),
            description_text: {
                cs: "Do 10.9. 24:00 nepojedou tramvaje mezi Výstavištěm a Strossmayerovým náměstím. Pro cestu na Strossmayerovo náměstí použijte linky 12 nebo 17.",
                en: "Trams will not run between Výstaviště and Strossmayer Square until 24:00 on 24 April. Use lines 12 or 17 to get to Strossmayer Square.",
            },
            created_timestamp: new Date("2024-03-02T08:00:00.000Z"),
            updated_timestamp: new Date("2024-03-02T08:00:00.000Z"),
        },
        infotextsRopidGTFSStops: [{ infotext_id: "265cdc5b-d6da-48e0-a114-6901200c4d46", stop_id: "U532Z4P" }],
    },
    {
        infotext: {
            id: "59a0b461-df44-42a6-940f-2abf0fd4e980",
            severity_level: InfotextSeverityLevel.Info,
            display_type: "GENERAL",
            active_period_start: new Date("2024-02-29T22:00:00.000Z"),
            active_period_end: null,
            description_text: {
                cs: "Do pondělí 12.6. bude uzavřen tento vstup do metra. Vstup od zastávky tramvaje v ulici Plynární zůstává v provozu.",
                en: null,
            },
            created_timestamp: new Date("2024-03-02T08:00:00.000Z"),
            updated_timestamp: new Date("2024-03-02T08:00:00.000Z"),
        },
        infotextsRopidGTFSStops: [
            { infotext_id: "59a0b461-df44-42a6-940f-2abf0fd4e980", stop_id: "U115Z101P" },
            { infotext_id: "59a0b461-df44-42a6-940f-2abf0fd4e980", stop_id: "U115Z102P" },
        ],
    },
    {
        infotext: {
            id: "e9a15133-e606-406b-b492-9af05c643e7a",
            severity_level: InfotextSeverityLevel.Severe,
            display_type: "INLINE",
            active_period_start: new Date("2024-03-26T22:00:00.000Z"),
            active_period_end: new Date("2024-09-21T22:00:00.000Z"),
            description_text: {
                cs: "Do pondělí probíhá výluka metra C mezi I. P. Pavlova a Nádraží Holešovice. Náhradní autobus X-C navazuje ze zastávky I. P. Pavlova.",
                en: "Until Monday, the C metro station is closed between I. P. Pavlova and Nádraží Holešovice. The replacement bus X-C will connect from the stop I. P. Pavlova.",
            },
            created_timestamp: new Date("2024-03-02T08:00:00.000Z"),
            updated_timestamp: new Date("2024-03-02T08:00:00.000Z"),
        },
        infotextsRopidGTFSStops: [{ infotext_id: "e9a15133-e606-406b-b492-9af05c643e7a", stop_id: "U190Z101P" }],
    },
];
