import { JISContainerToken } from "#ie/jis/ioc/JISContainerToken";
import { JISInfotextsTransformation } from "#ie/jis/transformations/JISInfotextsTransformation";
import { container } from "@golemio/core/dist/shared/tsyringe";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";
import { infotextsDataSourceFixture } from "../fixtures/infotextsDataSourceFixture";
import { infotextsTransformedDataFixture } from "../fixtures/infotextsTransformedDataFixture";

/**
 * Recursively delete all properties of `null` value from a given object and all its nested objects
 *
 * @param objectToModify The object to delete null properties from (including those in nested objects)
 * @param options Options for the operation
 * @param options.ignoredProperties Names of properties to be ignored (including any of their nested objects)
 * @returns The number of deleted properties
 */
function deepDeleteNullProperties(
    objectToModify: unknown,
    options: { ignoredProperties: string[] } = { ignoredProperties: [] }
): number {
    if (typeof objectToModify !== "object" || objectToModify === null) return 0;
    if (Array.isArray(objectToModify)) {
        return objectToModify.reduce((sum, item) => sum + deepDeleteNullProperties(item, options), 0);
    }
    let deleteCount = 0;
    const records = objectToModify as Record<string, unknown>;
    for (const key of Object.keys(records)) {
        if ((options?.ignoredProperties ?? []).includes(key)) continue;
        if (records[key] === null) {
            delete records[key];
            deleteCount++;
        } else {
            deleteCount += deepDeleteNullProperties(records[key], options);
        }
    }
    return deleteCount;
}

describe("JISInfotextsTransformation (unit)", () => {
    let createContainer: () => JISInfotextsTransformation;
    let sandbox: SinonSandbox;

    before(() => {
        sandbox = sinon.createSandbox();
    });

    beforeEach(() => {
        createContainer = () =>
            container
                .createChildContainer()
                .register(JISContainerToken.JISInfotextsTransformation, JISInfotextsTransformation)
                .resolve<JISInfotextsTransformation>(JISContainerToken.JISInfotextsTransformation);
    });

    afterEach(() => {
        container.clearInstances();
        sandbox.restore();
    });

    after(() => {
        sinon.restore();
    });

    describe("transformArray", () => {
        it("should properly transform data", async () => {
            const transformation = createContainer();
            const transformedData = await transformation.transformArray(infotextsDataSourceFixture);
            expect(transformedData).to.deep.equal(infotextsTransformedDataFixture);
        });

        it("should properly transform empty data array", async () => {
            const transformation = createContainer();
            const transformedData = await transformation.transformArray([]);
            expect(transformedData).to.deep.equal([]);
        });

        it("should properly transform data without null properties", async () => {
            const transformation = createContainer();
            const data = structuredClone(infotextsDataSourceFixture);
            expect(deepDeleteNullProperties(data)).to.equal(5);

            const expectedTransformedData = structuredClone(infotextsTransformedDataFixture);
            const deepDeleteNullIgnoredProperties = Object.keys(expectedTransformedData[0].infotext)
                .filter((key) => key !== "description_text")
                .concat("infotextsRopidGTFSStops");
            expect(
                deepDeleteNullProperties(expectedTransformedData, { ignoredProperties: deepDeleteNullIgnoredProperties })
            ).to.equal(1);

            const transformedData = await transformation.transformArray(data);

            expect(transformedData).to.deep.equal(expectedTransformedData);
        });
    });
});
