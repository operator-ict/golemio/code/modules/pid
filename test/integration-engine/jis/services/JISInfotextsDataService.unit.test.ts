import { JISContainerToken } from "#ie/jis/ioc/JISContainerToken";
import { JISInfotextsDataService } from "#ie/jis/services/JISInfotextsDataService";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { container } from "@golemio/core/dist/shared/tsyringe";
import { expect } from "chai";
import sinon, { SinonSandbox, SinonStub } from "sinon";

describe("JISInfotextsDataService (unit)", () => {
    let createContainer: () => JISInfotextsDataService;
    let sandbox: SinonSandbox;
    let stubs: Record<string, SinonStub>;

    before(() => {
        sandbox = sinon.createSandbox();
    });

    beforeEach(() => {
        stubs = {};
        stubs.transactionCommit = sandbox.stub().resolves();
        stubs.transactionRollback = sandbox.stub().resolves();
        stubs.connectionTransaction = sandbox.stub().resolves({
            commit: stubs.transactionCommit,
            rollback: stubs.transactionRollback,
        });
        stubs.postgresConnectorGetConnection = sandbox.stub().returns({
            transaction: stubs.connectionTransaction,
        });
        stubs.loggerInfo = sandbox.stub();
        stubs.jisInfotextsRepositoryRefreshData = sandbox.stub().resolves([]);
        stubs.jisInfotextsRopidGTFSStopsRepositoryDeleteAllLastUpdatedBefore = sandbox.stub().resolves(0);
        stubs.jisInfotextsRopidGTFSStopsRepositoryUpsertAll = sandbox.stub().resolves([]);
        stubs.jisInfotextsRepositoryDeleteAll = sandbox.stub().resolves([]);
        stubs.jisInfotextsRopidGTFSStopsRepositoryDeleteAll = sandbox.stub().resolves([]);

        createContainer = () =>
            container
                .createChildContainer()
                .register(
                    CoreToken.PostgresConnector,
                    class PostgresConnector {
                        getConnection = stubs.postgresConnectorGetConnection;
                    }
                )
                .register(
                    CoreToken.Logger,
                    class Logger {
                        info = stubs.loggerInfo;
                    }
                )
                .registerSingleton(
                    JISContainerToken.JISInfotextsRepository,
                    class JISInfotextsRepository {
                        refreshData = stubs.jisInfotextsRepositoryRefreshData;
                        deleteAll = stubs.jisInfotextsRepositoryDeleteAll;
                    }
                )
                .registerSingleton(
                    JISContainerToken.JISInfotextsRopidGTFSStopsRepository,
                    class JISInfotextsRopidGTFSStopsRepository {
                        upsertAll = stubs.jisInfotextsRopidGTFSStopsRepositoryUpsertAll;
                        deleteAll = stubs.jisInfotextsRopidGTFSStopsRepositoryDeleteAll;
                    }
                )
                .register(JISContainerToken.JISInfotextsDataService, JISInfotextsDataService)
                .resolve<JISInfotextsDataService>(JISContainerToken.JISInfotextsDataService);
    });

    afterEach(() => {
        container.clearInstances();
        sandbox.restore();
    });

    after(() => {
        sinon.restore();
    });

    describe("refreshData", () => {
        it("should call the correct methods", async () => {
            const service = createContainer();
            await service.refreshData([], []);

            expect(stubs.jisInfotextsRopidGTFSStopsRepositoryDeleteAll.calledOnce).to.be.true;
            expect(stubs.jisInfotextsRepositoryDeleteAll.calledOnce).to.be.true;
            expect(stubs.transactionCommit.calledOnce).to.be.true;
            expect(stubs.loggerInfo.calledOnce).to.be.true;
            expect(stubs.transactionRollback.notCalled).to.be.true;
            expect(stubs.jisInfotextsRopidGTFSStopsRepositoryUpsertAll.notCalled).to.be.true;
            sandbox.assert.callOrder(
                stubs.jisInfotextsRopidGTFSStopsRepositoryDeleteAll,
                stubs.jisInfotextsRepositoryDeleteAll,
                stubs.transactionCommit,
                stubs.loggerInfo
            );
        });

        it("should rollback after an error during database delete", async () => {
            const service = createContainer();
            stubs.jisInfotextsRopidGTFSStopsRepositoryDeleteAll.rejects(new Error("test"));

            await expect(service.refreshData([], [])).to.be.rejectedWith(GeneralError);

            expect(stubs.jisInfotextsRopidGTFSStopsRepositoryDeleteAll.calledOnce).to.be.true;
            expect(stubs.jisInfotextsRepositoryDeleteAll.notCalled).to.be.true;
            expect(stubs.jisInfotextsRopidGTFSStopsRepositoryDeleteAllLastUpdatedBefore.notCalled).to.be.true;
            expect(stubs.transactionCommit.notCalled).to.be.true;
            expect(stubs.loggerInfo.notCalled).to.be.true;
            expect(stubs.transactionRollback.calledOnce).to.be.true;
            expect(stubs.jisInfotextsRepositoryRefreshData.notCalled).to.be.true;
            expect(stubs.jisInfotextsRopidGTFSStopsRepositoryUpsertAll.notCalled).to.be.true;
            sandbox.assert.callOrder(stubs.jisInfotextsRopidGTFSStopsRepositoryDeleteAll, stubs.transactionRollback);
        });
        it("should rollback after an error during database upsert", async () => {
            const service = createContainer();
            stubs.jisInfotextsRepositoryRefreshData.rejects(new Error("test"));

            await expect(service.refreshData(["a"] as any, ["a1", "a2"] as any)).to.be.rejectedWith(GeneralError);

            expect(stubs.jisInfotextsRepositoryRefreshData.calledOnce).to.be.true;
            expect(stubs.jisInfotextsRopidGTFSStopsRepositoryDeleteAll.notCalled).to.be.true;
            expect(stubs.jisInfotextsRepositoryDeleteAll.notCalled).to.be.true;
            expect(stubs.jisInfotextsRopidGTFSStopsRepositoryDeleteAllLastUpdatedBefore.notCalled).to.be.true;
            expect(stubs.transactionCommit.notCalled).to.be.true;
            expect(stubs.loggerInfo.notCalled).to.be.true;
            expect(stubs.transactionRollback.calledOnce).to.be.true;
            expect(stubs.jisInfotextsRopidGTFSStopsRepositoryUpsertAll.notCalled).to.be.true;
            sandbox.assert.callOrder(stubs.jisInfotextsRepositoryRefreshData, stubs.transactionRollback);
        });
    });
});
