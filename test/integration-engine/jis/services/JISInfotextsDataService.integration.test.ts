import { JISContainer } from "#ie/jis/ioc/Di";
import { JISContainerToken } from "#ie/jis/ioc/JISContainerToken";
import { JISInfotextsRepository } from "#ie/jis/repositories/JISInfotextsRepository";
import { JISInfotextsRopidGTFSStopsRepository } from "#ie/jis/repositories/JISInfotextsRopidGTFSStopsRepository";
import { JISInfotextsDataService } from "#ie/jis/services/JISInfotextsDataService";
import { InfotextSeverityLevel } from "#og/shared/constants/jis/InfotextSeverityLevelEnum";
import { IJISInfotext, IJISInfotextsRopidGTFSStops } from "#sch/jis/models/interfaces";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { container } from "@golemio/core/dist/shared/tsyringe";
import { expect } from "chai";
import sinon, { SinonSandbox, SinonSpy, SinonStub } from "sinon";
import { dataInDb } from "../fixtures/InfotextsToBeUpdatedFixture";

describe("JISInfotextsDataService (integration)", () => {
    let createContainer: () => JISInfotextsDataService;
    let postgresConnector: IDatabaseConnector;
    let infotextsRepository: JISInfotextsRepository;
    let infotextsRopidGTFSStopsRepository: JISInfotextsRopidGTFSStopsRepository;
    let sandbox: SinonSandbox;
    let stubs: Record<string, SinonStub>;

    before(async () => {
        postgresConnector = JISContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector);
        infotextsRepository = JISContainer.resolve<JISInfotextsRepository>(JISContainerToken.JISInfotextsRepository);
        infotextsRopidGTFSStopsRepository = JISContainer.resolve<JISInfotextsRopidGTFSStopsRepository>(
            JISContainerToken.JISInfotextsRopidGTFSStopsRepository
        );
        sandbox = sinon.createSandbox();
    });

    beforeEach(() => {
        stubs = {};
        stubs.loggerInfo = sandbox.stub();
        stubs.jisInfotextsRepositoryDeleteAllLastUpdatedBefore = sandbox.stub().resolves(0);
        stubs.jisInfotextsRepositoryRefreshData = sandbox.stub().resolves([]);
        stubs.jisInfotextsRopidGTFSStopsRepositoryDeleteAllLastUpdatedBefore = sandbox.stub().resolves(0);
        stubs.jisInfotextsRopidGTFSStopsRepositoryUpsertAll = sandbox.stub().resolves([]);

        createContainer = () =>
            container
                .createChildContainer()
                .registerInstance(CoreToken.PostgresConnector, postgresConnector)
                .register(
                    CoreToken.Logger,
                    class Logger {
                        info = stubs.loggerInfo;
                    }
                )
                .registerInstance(JISContainerToken.JISInfotextsRepository, infotextsRepository)
                .registerInstance(JISContainerToken.JISInfotextsRopidGTFSStopsRepository, infotextsRopidGTFSStopsRepository)
                .register(JISContainerToken.JISInfotextsDataService, JISInfotextsDataService)
                .resolve<JISInfotextsDataService>(JISContainerToken.JISInfotextsDataService);
    });

    afterEach(() => {
        container.clearInstances();
        sandbox.restore();
    });

    after(async () => {
        expect(await infotextsRopidGTFSStopsRepository.deleteAllLastUpdatedBefore(new Date())).to.equal(0);

        sinon.restore();
    });

    describe("refreshData", async () => {
        let infotextsRepositoryRefreshDataSpy: SinonSpy;
        let infotextsRopidGTFSStopsRepositoryDeleteAllLastUpdatedBeforeSpy: SinonSpy;
        let infotextsRopidGTFSStopsRepositoryUpsertAllSpy: SinonSpy;

        const infotexts: IJISInfotext[] = [
            {
                id: "d6b2c5b5-401e-4325-87fa-a40bff6b5158",
                severity_level: InfotextSeverityLevel.Severe,
                display_type: "INLINE",
                active_period_start: new Date("2001-01-01T00:00:00Z"),
                active_period_end: null,
                description_text: {
                    cs: "test",
                },
                created_timestamp: new Date("2001-01-01T00:00:00Z"),
                updated_timestamp: new Date(),
            },
            {
                id: "aa0af446-0569-4211-8914-ab9332226c98",
                severity_level: InfotextSeverityLevel.Severe,
                display_type: "INLINE",
                active_period_start: new Date("2001-01-01T00:00:00Z"),
                active_period_end: new Date("2001-01-02T00:00:00Z"),
                description_text: {
                    cs: "test",
                    en: "test.en",
                },
                created_timestamp: new Date("2001-01-01T00:00:00Z"),
                updated_timestamp: new Date(),
            },
        ];
        const infotextsRopidGTFSStops: IJISInfotextsRopidGTFSStops[] = [
            {
                infotext_id: "d6b2c5b5-401e-4325-87fa-a40bff6b5158",
                stop_id: "U52Z2P",
            },
            {
                infotext_id: "aa0af446-0569-4211-8914-ab9332226c98",
                stop_id: "U1341Z1P",
            },
            {
                infotext_id: "aa0af446-0569-4211-8914-ab9332226c98",
                stop_id: "U52Z4P",
            },
        ];

        beforeEach(async () => {
            infotextsRepositoryRefreshDataSpy = sandbox.spy(infotextsRepository, "refreshData");
            infotextsRopidGTFSStopsRepositoryUpsertAllSpy = sandbox.spy(infotextsRopidGTFSStopsRepository, "upsertAll");
            infotextsRopidGTFSStopsRepositoryDeleteAllLastUpdatedBeforeSpy = sandbox.spy(
                infotextsRopidGTFSStopsRepository,
                "deleteAllLastUpdatedBefore"
            );
        });

        it("should insert new data and delete nothing when there are no older data", async () => {
            const service = createContainer();
            await service.refreshData([...dataInDb, ...infotexts], infotextsRopidGTFSStops);

            expect(await infotextsRepositoryRefreshDataSpy.firstCall.returnValue)
                .to.be.an("array")
                .of.length(infotexts.length);
            expect(await infotextsRopidGTFSStopsRepositoryUpsertAllSpy.firstCall.returnValue)
                .to.be.an("array")
                .of.length(infotextsRopidGTFSStops.length);
            expect(await infotextsRopidGTFSStopsRepositoryDeleteAllLastUpdatedBeforeSpy.firstCall.returnValue).to.equal(0);
        });

        it("should insert or update data and delete older data", async () => {
            const service = createContainer();
            const infotextsData = structuredClone(infotexts);
            const infotextsRopidGTFSStopsData = structuredClone(infotextsRopidGTFSStops);
            infotextsData[0].id = "5d519cb6-8bd8-4a2f-a1ed-14c90b284fa3";
            infotextsData[0].updated_timestamp = new Date();
            infotextsRopidGTFSStopsData[0].infotext_id = "5d519cb6-8bd8-4a2f-a1ed-14c90b284fa3";
            infotextsData[1].description_text.cs = `update - ${infotextsData[1].description_text.cs}`;
            infotextsData[1].updated_timestamp = new Date();

            await service.refreshData([...dataInDb, ...infotextsData], infotextsRopidGTFSStopsData);

            expect(await infotextsRepositoryRefreshDataSpy.firstCall.returnValue)
                .to.be.an("array")
                .of.length(infotextsData.length);
            expect(await infotextsRopidGTFSStopsRepositoryUpsertAllSpy.firstCall.returnValue)
                .to.be.an("array")
                .of.length(infotextsRopidGTFSStopsData.length);
            expect(await infotextsRopidGTFSStopsRepositoryDeleteAllLastUpdatedBeforeSpy.firstCall.returnValue).to.equal(1);

            // delete all updated infotexts and stops
            await infotextsRopidGTFSStopsRepository.deleteAllLastUpdatedBefore(new Date());
        });

        it("should rollback when trying to upsert malformed data", async () => {
            const service = createContainer();
            const infotextsRopidGTFSStopsData = structuredClone(infotextsRopidGTFSStops);
            (infotextsRopidGTFSStopsData[0].infotext_id as any) = null;

            await expect(service.refreshData([...dataInDb, ...infotexts], infotextsRopidGTFSStopsData)).to.be.rejectedWith(
                GeneralError
            );

            expect(await infotextsRopidGTFSStopsRepository.deleteAllLastUpdatedBefore(new Date())).to.equal(0);
        });
    });
});
