import { JISContainerToken } from "#ie/jis/ioc/JISContainerToken";
import { container } from "@golemio/core/dist/shared/tsyringe";
import { expect } from "chai";
import sinon, { SinonSandbox, SinonStub } from "sinon";
import { infotextsDataSourceFixture } from "../../fixtures/infotextsDataSourceFixture";
import { FetchJISInfotextsTask } from "#ie/jis/workers/tasks/FetchJISInfotextsTask";
import { QueueManager } from "@golemio/core/dist/integration-engine/queueprocessors";

describe("FetchJISInfotextsTask (unit)", () => {
    let createContainer: () => FetchJISInfotextsTask;
    let sandbox: SinonSandbox;
    let stubs: Record<string, SinonStub>;

    before(() => {
        sandbox = sinon.createSandbox();
    });

    beforeEach(() => {
        stubs = {};
        stubs.getAll = sandbox.stub().resolves(infotextsDataSourceFixture);
        stubs.getDataSource = sandbox.stub().returns({
            getAll: stubs.getAll,
        });
        stubs.sendMessageToExchange = sandbox.stub(QueueManager, "sendMessageToExchange").resolves();

        createContainer = () =>
            container
                .createChildContainer()
                .register(
                    JISContainerToken.JISInfotextsDataSourceFactory,
                    class JISInfotextsDataSourceFactory {
                        getDataSource = stubs.getDataSource;
                    }
                )
                .registerSingleton(JISContainerToken.FetchJISInfotextsTask, FetchJISInfotextsTask)
                .resolve<FetchJISInfotextsTask>(JISContainerToken.FetchJISInfotextsTask);
    });

    afterEach(() => {
        container.clearInstances();
        sandbox.restore();
    });

    after(() => {
        sinon.restore();
    });

    describe("execute", () => {
        it("should call the correct methods", async () => {
            const task = createContainer();
            await task["execute"]();

            expect(stubs.getAll.calledOnce).to.be.true;
            expect(stubs.getDataSource.calledOnce).to.be.true;
            expect(stubs.sendMessageToExchange.calledOnce).to.be.true;
            sandbox.assert.callOrder(stubs.getDataSource, stubs.getAll, stubs.sendMessageToExchange);
        });

        it("should handle an error during getting data", async () => {
            const task = createContainer();
            stubs.getAll.rejects(new Error("test"));

            await expect(task["execute"]()).to.be.rejectedWith(Error);

            expect(stubs.getDataSource.calledOnce).to.be.true;
            expect(stubs.getAll.calledOnce).to.be.true;
            expect(stubs.sendMessageToExchange.notCalled).to.be.true;
        });
    });
});
