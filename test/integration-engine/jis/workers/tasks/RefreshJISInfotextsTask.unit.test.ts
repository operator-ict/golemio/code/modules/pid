import { JISContainerToken } from "#ie/jis/ioc/JISContainerToken";
import { RefreshJISInfotextsTask } from "#ie/jis/workers/tasks/RefreshJISInfotextsTask";
import { container } from "@golemio/core/dist/shared/tsyringe";
import { expect } from "chai";
import sinon, { SinonSandbox, SinonStub } from "sinon";
import { infotextsDataSourceFixture } from "../../fixtures/infotextsDataSourceFixture";

describe("RefreshJISInfotextsTask (unit)", () => {
    let createContainer: () => RefreshJISInfotextsTask;
    let sandbox: SinonSandbox;
    let stubs: Record<string, SinonStub>;

    before(() => {
        sandbox = sinon.createSandbox();
    });

    beforeEach(() => {
        stubs = {};
        stubs.jisInfotextsDataServiceRefreshData = sandbox.stub().resolves();
        stubs.jisInfotextsTransformationTransformArray = sandbox.stub().returns([]);

        createContainer = () =>
            container
                .createChildContainer()
                .register(
                    JISContainerToken.JISInfotextsDataService,
                    class JISInfotextsDataService {
                        refreshData = stubs.jisInfotextsDataServiceRefreshData;
                    }
                )
                .registerSingleton(
                    JISContainerToken.JISInfotextsTransformation,
                    class JISInfotextsTransformation {
                        transformArray = stubs.jisInfotextsTransformationTransformArray;
                    }
                )
                .registerSingleton(JISContainerToken.RefreshJISInfotextsTask, RefreshJISInfotextsTask)
                .resolve<RefreshJISInfotextsTask>(JISContainerToken.RefreshJISInfotextsTask);
    });

    afterEach(() => {
        container.clearInstances();
        sandbox.restore();
    });

    after(() => {
        sinon.restore();
    });

    describe("execute", () => {
        it("should call the correct methods", async () => {
            const task = createContainer();
            await task["execute"](infotextsDataSourceFixture);

            expect(stubs.jisInfotextsTransformationTransformArray.calledOnce).to.be.true;
            expect(stubs.jisInfotextsDataServiceRefreshData.calledOnce).to.be.true;
            sandbox.assert.callOrder(stubs.jisInfotextsTransformationTransformArray, stubs.jisInfotextsDataServiceRefreshData);
        });

        it("should halt after an error during transformation", async () => {
            const task = createContainer();
            stubs.jisInfotextsTransformationTransformArray.throws(new Error("test"));

            await expect(task["execute"](infotextsDataSourceFixture)).to.be.rejectedWith(Error);

            expect(stubs.jisInfotextsTransformationTransformArray.calledOnce).to.be.true;
            expect(stubs.jisInfotextsDataServiceRefreshData.notCalled).to.be.true;
        });

        it("should get entities for data service from transformed data", async () => {
            const task = createContainer();
            stubs.jisInfotextsTransformationTransformArray.returns([
                { infotext: "a", infotextsRopidGTFSStops: ["a1"] },
                { infotext: "b", infotextsRopidGTFSStops: ["b1", "b2"] },
                { infotext: "c", infotextsRopidGTFSStops: ["c1", "c2"] },
            ]);

            await task["execute"](infotextsDataSourceFixture);

            expect(stubs.jisInfotextsDataServiceRefreshData.calledOnce).to.be.true;
            expect(stubs.jisInfotextsDataServiceRefreshData.firstCall.firstArg).to.deep.equal(["a", "b", "c"]);
            expect(stubs.jisInfotextsDataServiceRefreshData.firstCall.args[1]).to.deep.equal(["a1", "b1", "b2", "c1", "c2"]);
        });

        it("should not import infotext without stops", async () => {
            const task = createContainer();

            stubs.jisInfotextsTransformationTransformArray.returns([
                { infotext: "a", infotextsRopidGTFSStops: [] },
                { infotext: "b", infotextsRopidGTFSStops: ["b1", "b2"] },
                { infotext: "c", infotextsRopidGTFSStops: [] },
            ]);

            await task["execute"](infotextsDataSourceFixture);
            expect(stubs.jisInfotextsDataServiceRefreshData.calledOnce).to.be.true;
            expect(stubs.jisInfotextsDataServiceRefreshData.firstCall.firstArg).to.deep.equal(["b"]);
            expect(stubs.jisInfotextsDataServiceRefreshData.firstCall.args[1]).to.deep.equal(["b1", "b2"]);
        });

        it("should halt after an error during data refresh", async () => {
            const task = createContainer();
            stubs.jisInfotextsDataServiceRefreshData.rejects(new Error("test"));

            await expect(task["execute"](infotextsDataSourceFixture)).to.be.rejectedWith(Error);

            expect(stubs.jisInfotextsTransformationTransformArray.calledOnce).to.be.true;
            expect(stubs.jisInfotextsDataServiceRefreshData.calledOnce).to.be.true;
        });
    });
});
