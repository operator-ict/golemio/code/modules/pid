import { JISContainerToken } from "#ie/jis/ioc/JISContainerToken";
import { JISInfotextsRepository } from "#ie/jis/repositories/JISInfotextsRepository";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { Op } from "@golemio/core/dist/shared/sequelize";
import { container } from "@golemio/core/dist/shared/tsyringe";
import { expect } from "chai";
import sinon, { SinonSandbox, SinonStub } from "sinon";

describe("JISInfotextsRepository (unit)", () => {
    let createContainer: () => JISInfotextsRepository;
    let sandbox: SinonSandbox;
    let stubs: Record<string, SinonStub>;

    before(() => {
        sandbox = sinon.createSandbox();
    });

    beforeEach(() => {
        stubs = {};
        stubs.sequelizeModelBulkCreate = sandbox.stub().resolves([]);
        stubs.sequelizeModelDestroy = sandbox.stub().resolves(0);
        stubs.sequelizeModelFindAll = sandbox
            .stub()
            .resolves([
                { id: "c", updated_at: new Date("2024-12-03T15:10:31.108Z") },
                { id: "d" },
                { id: "e", updated_at: new Date("2024-12-03T15:11:31.108Z") },
            ]);
        stubs.connectionDefine = sandbox.stub().returns({
            bulkCreate: stubs.sequelizeModelBulkCreate,
            destroy: stubs.sequelizeModelDestroy,
            findAll: stubs.sequelizeModelFindAll,
        });
        stubs.postgresConnectorGetConnection = sandbox.stub().returns({
            define: stubs.connectionDefine,
        });

        createContainer = () =>
            container
                .createChildContainer()
                .register(
                    CoreToken.PostgresConnector,
                    class PostgresConnector {
                        getConnection = stubs.postgresConnectorGetConnection;
                    }
                )
                .register(CoreToken.Logger, class Logger {})
                .register(JISContainerToken.JISInfotextsRepository, JISInfotextsRepository)
                .resolve<JISInfotextsRepository>(JISContainerToken.JISInfotextsRepository);
    });

    afterEach(() => {
        container.clearInstances();
        sandbox.restore();
    });

    after(() => {
        sinon.restore();
    });

    describe("upsertAll", () => {
        const attributesToUpdate = [
            "id",
            "severity_level",
            "display_type",
            "active_period_start",
            "active_period_end",
            "description_text",
            "created_timestamp",
            "updated_timestamp",
            "updated_at",
        ];

        it("should call the correct methods with the correct arguments", async () => {
            const repository = createContainer();
            const now = new Date();
            await repository.refreshData([
                { id: "a" },
                { id: "b" },
                { id: "c", updated_timestamp: now },
                { id: "e", updated_timestamp: new Date("2024-12-03T15:10:31.108Z") },
            ] as any);

            expect(stubs.sequelizeModelBulkCreate.calledOnce).to.be.true;
            expect(stubs.sequelizeModelBulkCreate.firstCall.firstArg).to.deep.equal([
                { id: "a" },
                { id: "b" },
                { id: "c", updated_timestamp: now },
            ]);
            expect(stubs.sequelizeModelDestroy.firstCall.firstArg).to.deep.equal({
                where: { id: { [Op.in]: ["d"] } },
                transaction: undefined,
            });
            expect(stubs.sequelizeModelBulkCreate.firstCall.args[1]).to.deep.equal({
                updateOnDuplicate: attributesToUpdate,
                transaction: undefined,
            });
        });

        it("should call the correct methods with the correct arguments including a transaction", async () => {
            const repository = createContainer();
            const now = new Date();
            await repository.refreshData(
                [
                    { id: "a" },
                    { id: "b" },
                    { id: "c", updated_timestamp: now },
                    { id: "e", updated_timestamp: new Date("2024-12-03T15:10:31.108Z") },
                ] as any,
                { transaction: "t" as any }
            );

            expect(stubs.sequelizeModelBulkCreate.calledOnce).to.be.true;
            expect(stubs.sequelizeModelBulkCreate.firstCall.firstArg).to.deep.equal([
                { id: "a" },
                { id: "b" },
                { id: "c", updated_timestamp: now },
            ]);
            expect(stubs.sequelizeModelDestroy.firstCall.firstArg).to.deep.equal({
                where: { id: { [Op.in]: ["d"] } },
                transaction: "t",
            });
            expect(stubs.sequelizeModelBulkCreate.firstCall.args[1]).to.deep.equal({
                updateOnDuplicate: attributesToUpdate,
                transaction: "t",
            });
        });

        it("should handle an error during upsert", async () => {
            const repository = createContainer();
            stubs.sequelizeModelBulkCreate.rejects(new Error("test"));
            await expect(repository.refreshData([])).to.be.rejectedWith(GeneralError);
        });
    });
});
