import { JISContainerToken } from "#ie/jis/ioc/JISContainerToken";
import { JISInfotextsRopidGTFSStopsRepository } from "#ie/jis/repositories/JISInfotextsRopidGTFSStopsRepository";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { Op } from "@golemio/core/dist/shared/sequelize";
import { container } from "@golemio/core/dist/shared/tsyringe";
import { expect } from "chai";
import sinon, { SinonSandbox, SinonStub } from "sinon";

describe("JISInfotextsRopidGTFSStopsRepository (unit)", () => {
    let createContainer: () => JISInfotextsRopidGTFSStopsRepository;
    let sandbox: SinonSandbox;
    let stubs: Record<string, SinonStub>;

    before(() => {
        sandbox = sinon.createSandbox();
    });

    beforeEach(() => {
        stubs = {};
        stubs.sequelizeModelBulkCreate = sandbox.stub().resolves([]);
        stubs.sequelizeModelDestroy = sandbox.stub().resolves(0);
        stubs.connectionDefine = sandbox.stub().returns({
            bulkCreate: stubs.sequelizeModelBulkCreate,
            destroy: stubs.sequelizeModelDestroy,
        });
        stubs.postgresConnectorGetConnection = sandbox.stub().returns({
            define: stubs.connectionDefine,
        });

        createContainer = () =>
            container
                .createChildContainer()
                .register(
                    CoreToken.PostgresConnector,
                    class PostgresConnector {
                        getConnection = stubs.postgresConnectorGetConnection;
                    }
                )
                .register(CoreToken.Logger, class Logger {})
                .register(JISContainerToken.JISInfotextsRopidGTFSStopsRepository, JISInfotextsRopidGTFSStopsRepository)
                .resolve<JISInfotextsRopidGTFSStopsRepository>(JISContainerToken.JISInfotextsRopidGTFSStopsRepository);
    });

    afterEach(() => {
        container.clearInstances();
        sandbox.restore();
    });

    after(() => {
        sinon.restore();
    });

    describe("upsertAll", () => {
        const attributesToUpdate = ["infotext_id", "stop_id", "updated_at"];

        it("should call the correct methods with the correct arguments", async () => {
            const repository = createContainer();

            await repository.upsertAll(["a", "b", "c"] as any);

            expect(stubs.sequelizeModelBulkCreate.calledOnce).to.be.true;
            expect(stubs.sequelizeModelBulkCreate.firstCall.firstArg).to.deep.equal(["a", "b", "c"]);
            expect(stubs.sequelizeModelBulkCreate.firstCall.args[1]).to.deep.equal({
                updateOnDuplicate: attributesToUpdate,
                transaction: undefined,
            });
        });

        it("should call the correct methods with the correct arguments including a transaction", async () => {
            const repository = createContainer();

            await repository.upsertAll(["a", "b", "c"] as any, { transaction: "t" as any });

            expect(stubs.sequelizeModelBulkCreate.calledOnce).to.be.true;
            expect(stubs.sequelizeModelBulkCreate.firstCall.firstArg).to.deep.equal(["a", "b", "c"]);
            expect(stubs.sequelizeModelBulkCreate.firstCall.args[1]).to.deep.equal({
                updateOnDuplicate: attributesToUpdate,
                transaction: "t",
            });
        });

        it("should handle an error during upsert", async () => {
            const repository = createContainer();
            stubs.sequelizeModelBulkCreate.rejects(new Error("test"));
            await expect(repository.upsertAll([])).to.be.rejectedWith(GeneralError);
        });
    });

    describe("deleteAllLastUpdatedBefore", () => {
        it("should call the correct methods with the correct arguments", async () => {
            const repository = createContainer();
            const date = new Date();

            await repository.deleteAllLastUpdatedBefore(date);

            expect(stubs.sequelizeModelDestroy.calledOnce).to.be.true;
            expect(stubs.sequelizeModelDestroy.firstCall.firstArg).to.deep.equal({
                where: {
                    updated_at: { [Op.lt]: date },
                },
                transaction: undefined,
            });
        });

        it("should call the correct methods with the correct arguments including a transaction", async () => {
            const repository = createContainer();
            const date = new Date();

            await repository.deleteAllLastUpdatedBefore(date, { transaction: "t" as any });

            expect(stubs.sequelizeModelDestroy.calledOnce).to.be.true;
            expect(stubs.sequelizeModelDestroy.firstCall.firstArg).to.deep.equal({
                where: {
                    updated_at: { [Op.lt]: date },
                },
                transaction: "t",
            });
        });

        it("should handle an error during delete", async () => {
            const repository = createContainer();
            stubs.sequelizeModelDestroy.rejects(new Error("test"));
            await expect(repository.deleteAllLastUpdatedBefore(new Date())).to.be.rejectedWith(GeneralError);
        });
    });
});
