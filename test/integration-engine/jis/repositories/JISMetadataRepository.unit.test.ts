import { JISContainerToken } from "#ie/jis/ioc/JISContainerToken";
import { JISMetadataRepository } from "#ie/jis/repositories/JISMetadataRepository";
import { ContainerToken, IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { container } from "@golemio/core/dist/shared/tsyringe";
import { expect } from "chai";
import sinon, { SinonFakeTimers, SinonSandbox, SinonStub } from "sinon";

describe("JISMetadataRepository (unit)", () => {
    let createContainer: () => JISMetadataRepository;
    let sandbox: SinonSandbox;
    let clock: SinonFakeTimers;
    let stubs: Record<string, SinonStub>;

    before(() => {
        sandbox = sinon.createSandbox();
    });

    beforeEach(() => {
        clock = sandbox.useFakeTimers({
            now: new Date("2024-11-11T12:11:11.111Z"),
        });

        stubs = {
            redisConnectorGetConnection: sandbox.stub(),
        };

        const ieContainerResolveStub = sandbox.stub(IntegrationEngineContainer, "resolve");
        ieContainerResolveStub.callsFake((...args) => {
            if (args[0] === ContainerToken.RedisConnector) {
                return new (class RedisConnector {
                    getConnection = stubs.redisConnectorGetConnection;
                })();
            }
            return ieContainerResolveStub.wrappedMethod.apply(IntegrationEngineContainer, args);
        });

        createContainer = () => {
            const repository = container
                .createChildContainer()
                .register(JISContainerToken.JISMetadataRepository, JISMetadataRepository)
                .resolve<JISMetadataRepository>(JISContainerToken.JISMetadataRepository);
            stubs.redisModelGet = sandbox.stub(repository, "get").resolves({
                infotexts: {
                    lastResponse: {
                        etag: "test",
                        updatedAt: new Date("2024-11-11T11:11:11.111Z").toISOString(),
                    },
                },
            });
            stubs.redisModelSet = sandbox.stub(repository, "set").resolves();
            return repository;
        };
    });

    afterEach(() => {
        container.clearInstances();
        clock.restore();
        sandbox.restore();
    });

    after(() => {
        sinon.restore();
    });

    describe("getLastInfotextsEtag", () => {
        it("should call the correct methods with the correct arguments", async () => {
            const repository = createContainer();

            const etag = await repository.getLastInfotextsEtag();

            expect(etag).to.equal("test");
            expect(stubs.redisModelGet.calledOnce).to.be.true;
            expect(stubs.redisModelGet.firstCall.firstArg).to.equal("main");
        });

        it("should handle an error during Redis get", async () => {
            const repository = createContainer();
            stubs.redisModelGet.rejects(new Error("test"));
            await expect(repository.getLastInfotextsEtag()).to.be.rejectedWith(GeneralError);
        });
    });

    describe("setLastInfotextsEtag", () => {
        it("should call the correct methods with the correct arguments", async () => {
            const repository = createContainer();

            await repository.setLastInfotextsEtag("test 2");

            expect(stubs.redisModelGet.calledOnce).to.be.true;
            expect(stubs.redisModelSet.calledOnce).to.be.true;
            expect(stubs.redisModelSet.firstCall.firstArg).to.equal("main");
            expect(stubs.redisModelSet.firstCall.args[1]).to.deep.equal({
                infotexts: {
                    lastResponse: {
                        etag: "test 2",
                        updatedAt: new Date("2024-11-11T12:11:11.111Z").toISOString(),
                    },
                },
            });
        });

        it("should work as expected when Redis contains no JIS metadata", async () => {
            const repository = createContainer();
            stubs.redisModelGet.resolves();

            await repository.setLastInfotextsEtag("test 2");

            expect(stubs.redisModelGet.calledOnce).to.be.true;
            expect(stubs.redisModelSet.calledOnce).to.be.true;
            expect(stubs.redisModelSet.firstCall.firstArg).to.equal("main");
            expect(stubs.redisModelSet.firstCall.args[1]).to.deep.equal({
                infotexts: {
                    lastResponse: {
                        etag: "test 2",
                        updatedAt: new Date("2024-11-11T12:11:11.111Z").toISOString(),
                    },
                },
            });
        });

        it("should handle an error during Redis get", async () => {
            const repository = createContainer();
            stubs.redisModelGet.rejects(new Error("test"));
            await expect(repository.setLastInfotextsEtag("test 2")).to.be.rejectedWith(GeneralError);
            expect(stubs.redisModelGet.calledOnce).to.be.true;
            expect(stubs.redisModelSet.notCalled).to.be.true;
        });

        it("should handle an error during Redis set", async () => {
            const repository = createContainer();
            stubs.redisModelSet.rejects(new Error("test"));
            await expect(repository.setLastInfotextsEtag("test 2")).to.be.rejectedWith(GeneralError);
        });
    });
});
