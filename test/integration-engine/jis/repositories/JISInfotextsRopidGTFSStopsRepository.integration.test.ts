import { JISContainer } from "#ie/jis/ioc/Di";
import { JISContainerToken } from "#ie/jis/ioc/JISContainerToken";
import { JISInfotextsRopidGTFSStopsRepository } from "#ie/jis/repositories/JISInfotextsRopidGTFSStopsRepository";
import { IJISInfotextsRopidGTFSStops } from "#sch/jis/models/interfaces";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { container } from "@golemio/core/dist/shared/tsyringe";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";

describe("JISInfotextsRopidGTFSStopsRepository (integration)", () => {
    let createContainer: () => JISInfotextsRopidGTFSStopsRepository;
    let postgresConnector: IDatabaseConnector;
    let sandbox: SinonSandbox;

    before(async () => {
        postgresConnector = JISContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector);
        sandbox = sinon.createSandbox();

        await postgresConnector.connect();
    });

    beforeEach(() => {
        createContainer = () =>
            container
                .createChildContainer()
                .registerInstance(CoreToken.PostgresConnector, postgresConnector)
                .register(CoreToken.Logger, class Logger {})
                .registerSingleton(JISContainerToken.JISInfotextsRopidGTFSStopsRepository, JISInfotextsRopidGTFSStopsRepository)
                .resolve<JISInfotextsRopidGTFSStopsRepository>(JISContainerToken.JISInfotextsRopidGTFSStopsRepository);
    });

    afterEach(() => {
        container.clearInstances();
        sandbox.restore();
    });

    after(async () => {
        const repository = createContainer();
        expect(await repository.deleteAllLastUpdatedBefore(new Date())).to.equal(0);

        sinon.restore();
    });

    describe("upsertAll", () => {
        const dataToUpsert: IJISInfotextsRopidGTFSStops[] = [
            {
                infotext_id: "40507cee-6468-488e-ba78-e36d9c20e67c",
                stop_id: "U52Z2P",
            },
            {
                infotext_id: "6bdc3fdb-dd18-438b-878a-7972bddb0d92",
                stop_id: "U1341Z1P",
            },
            {
                infotext_id: "6bdc3fdb-dd18-438b-878a-7972bddb0d92",
                stop_id: "U52Z4P",
            },
        ];

        it("should insert new data to database", async () => {
            const repository = createContainer();
            const upsertedData = await repository.upsertAll(dataToUpsert);

            expect(upsertedData).to.be.an("array").of.length(dataToUpsert.length);
            const upsertedDataValues = upsertedData.map(({ dataValues }) => {
                const { created_at, updated_at, ...values } = dataValues;
                return values;
            });
            expect(upsertedDataValues).to.deep.equal(dataToUpsert);
        });

        it("should handle upserting empty data array", async () => {
            const repository = createContainer();
            const upsertedData = await repository.upsertAll([]);
            expect(upsertedData).to.be.an("array").that.is.empty;
        });

        it("should handle trying to upsert malformed data", async () => {
            const repository = createContainer();
            const data = structuredClone(dataToUpsert[0]);
            (data.infotext_id as any) = null;

            await expect(repository.upsertAll([data])).to.be.rejectedWith(GeneralError);
        });
    });

    describe("deleteAllLastUpdatedBefore", () => {
        it("should delete old data from database", async () => {
            const repository = createContainer();
            const numberOfDeletedItems = await repository.deleteAllLastUpdatedBefore(new Date());
            expect(numberOfDeletedItems).to.equal(3);
        });

        it("should delete nothing when there are no data older than the given date", async () => {
            const repository = createContainer();
            const numberOfDeletedItems = await repository.deleteAllLastUpdatedBefore(new Date());
            expect(numberOfDeletedItems).to.equal(0);
        });
    });
});
