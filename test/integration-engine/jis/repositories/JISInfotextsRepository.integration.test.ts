import { JISContainer } from "#ie/jis/ioc/Di";
import { JISContainerToken } from "#ie/jis/ioc/JISContainerToken";
import { JISInfotextsRepository } from "#ie/jis/repositories/JISInfotextsRepository";
import { InfotextSeverityLevel } from "#og/shared/constants/jis/InfotextSeverityLevelEnum";
import { IJISInfotext } from "#sch/jis/models/interfaces";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { container } from "@golemio/core/dist/shared/tsyringe";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";
import { dataInDb } from "../fixtures/InfotextsToBeUpdatedFixture";
import { skip } from "node:test";

describe("JISInfotextsRepository (integration)", () => {
    let createContainer: () => JISInfotextsRepository;
    let postgresConnector: IDatabaseConnector;
    let sandbox: SinonSandbox;

    before(async () => {
        postgresConnector = JISContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector);
        sandbox = sinon.createSandbox();

        await postgresConnector.connect();
    });

    beforeEach(() => {
        createContainer = () =>
            container
                .createChildContainer()
                .registerInstance(CoreToken.PostgresConnector, postgresConnector)
                .register(CoreToken.Logger, class Logger {})
                .registerSingleton(JISContainerToken.JISInfotextsRepository, JISInfotextsRepository)
                .resolve<JISInfotextsRepository>(JISContainerToken.JISInfotextsRepository);
    });

    afterEach(() => {
        container.clearInstances();
        sandbox.restore();
    });

    after(async () => {
        const repository = createContainer();
        await repository.refreshData(dataInDb);
        sinon.restore();
    });

    describe("upsertAll", () => {
        const dataToUpsert: IJISInfotext[] = [
            {
                id: "d6b2c5b5-401e-4325-87fa-a40bff6b5158",
                severity_level: InfotextSeverityLevel.Severe,
                display_type: "INLINE",
                active_period_start: new Date("2001-01-01T00:00:00Z"),
                active_period_end: null,
                description_text: {
                    cs: "test",
                },
                created_timestamp: new Date("2001-01-01T00:00:00Z"),
                updated_timestamp: new Date(),
            },
            {
                id: "aa0af446-0569-4211-8914-ab9332226c98",
                severity_level: InfotextSeverityLevel.Severe,
                display_type: "INLINE",
                active_period_start: new Date("2001-01-01T00:00:00Z"),
                active_period_end: new Date("2001-01-02T00:00:00Z"),
                description_text: {
                    cs: "test",
                    en: "test.en",
                },
                created_timestamp: new Date("2001-01-01T00:00:00Z"),
                updated_timestamp: new Date(),
            },
        ];

        it("should insert new data to database", async () => {
            const repository = createContainer();
            const upsertedData = await repository.refreshData([...dataInDb, ...dataToUpsert]);

            expect(upsertedData).to.be.an("array").of.length(dataToUpsert.length);
            const upsertedDataValues = upsertedData.map(({ dataValues }) => {
                const { created_at, updated_at, ...values } = dataValues;
                return values;
            });
            expect(upsertedDataValues).to.deep.equal(dataToUpsert);
        });

        it("should update existing data in database", async () => {
            const repository = createContainer();
            const dataToUpdate = structuredClone(dataToUpsert);
            for (const datum of dataToUpdate) {
                datum.description_text.cs = `update - ${datum.description_text.cs}`;
                datum.updated_timestamp = new Date();
            }

            const upsertedData = await repository.refreshData([...dataInDb, ...dataToUpdate]);

            expect(upsertedData).to.be.an("array").of.length(dataToUpdate.length);
            const date = new Date();
            for (const { dataValues } of upsertedData) {
                expect(dataValues.updated_at).to.be.lessThan(date);
            }
            const upsertedDataValues = upsertedData.map(({ dataValues }) => {
                const { created_at, updated_at, ...values } = dataValues;
                return values;
            });
            expect(upsertedDataValues).to.deep.equal(dataToUpdate);
        });

        it("should handle trying to upsert malformed data", async () => {
            const repository = createContainer();
            const data = structuredClone(dataToUpsert[0]);
            (data.severity_level as any) = null;
            data.updated_timestamp = new Date();
            await expect(repository.refreshData([data])).to.be.rejectedWith(GeneralError);
        });
        it("should handle upserting empty data array and delete data from database", async () => {
            const repository = createContainer();
            const upsertedData = await repository.refreshData([]);
            expect(upsertedData.length).to.be.eq(0);

            const data = await repository["sequelizeModel"].findAll({});
            expect(data.length).to.be.eq(0);
        });
    });
});
