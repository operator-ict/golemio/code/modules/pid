import { FetchAndProcessEventsTask } from "#ie/ropid-vymi/workers/tasks/FetchAndProcessEventsTask";
import RopidVYMIApiHelper from "#ie/ropid-vymi/workers/tasks/helpers/RopidVYMIApiHelper";
import { IRopidVYMIEvent } from "#sch/ropid-vymi";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import DataSourceStreamManager from "@golemio/core/dist/integration-engine/datasources/DataSourceStreamManager";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import sinon, { SinonSandbox } from "sinon";

chai.use(chaiAsPromised);

describe("RopidVYMIWorker - FetchAndProcessEventsTask", () => {
    let sandbox: SinonSandbox;
    let task: FetchAndProcessEventsTask;
    let helper: RopidVYMIApiHelper;
    let testSourceData: IRopidVYMIEvent;

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        testSourceData = JSON.parse(fs.readFileSync(__dirname + "/../data/ropidvymi-datasource.json").toString("utf8"))[
            "vymi-report"
        ]["vymi-list"][0];
        sandbox
            .stub(PostgresConnector, "getConnection")
            .callsFake(() => Object.assign({ define: sandbox.stub(), transaction: sandbox.stub() }));

        task = new FetchAndProcessEventsTask("test.test");
        helper = RopidVYMIApiHelper.getInstance();
        //sandbox.stub(task, "sendMessageToExchange" as any).resolves();

        sandbox.stub(helper, <any>"datasource").value(Object.assign({ getAll: sandbox.stub() }));
        sandbox.stub(task["modelVYMIEvents"], "save");
        sandbox.stub(task["modelVYMIEventsRoutes"], "save");
        sandbox.stub(task["modelVYMIEventsStops"], "save");
        sandbox.stub(task["modelVYMIMeta"], "save");
        sandbox.stub(task["modelVYMIMeta"], "getLastModified").resolves({ version: 69, lastModified: null });
        sandbox.stub(task["modelVYMIMeta"], "checkSavedRows");
        sandbox.stub(task["modelVYMIMeta"], "replaceTmpTables");
        sandbox.stub(task["modelVYMIMeta"], "rollbackFailedSaving");
        sandbox.stub(task["modelVYMIMeta"], "getDigest").resolves("cf23df2207d99a74fbe169e3eba035e633b65d94");
    });

    afterEach(() => {
        sandbox?.restore();
    });

    it("fetchAndProcessEvents should fetch data again", async () => {
        sandbox.stub(helper, "getAllEvents" as any).resolves({ data: [], digest: "some_bs" });
        sandbox.stub(task, "prepareAndTransformEvents" as any);
        sandbox.stub(task, "prepareAndSaveEvents" as any);
        await task["execute"](null as any);
    });

    it("fetchAndProcessEvents should fetch data again", async () => {
        sandbox.stub(helper, "getAllEvents" as any).resolves({ data: [], digest: "some_bs" });
        sandbox.stub(task, "prepareAndTransformEvents" as any).resolves({});
        sandbox.stub(task, "prepareAndSaveEvents" as any);
        await task["execute"]({ data: {} } as any);

        sandbox.assert.calledOnce(task["prepareAndTransformEvents"] as any);
        sandbox.assert.callCount(task["prepareAndSaveEvents"] as any, 3);
        sandbox.assert.calledOnce(task["modelVYMIMeta"].replaceTmpTables as any);
    });

    it("fetchAndProcessEvents should rollback", async () => {
        sandbox.stub(DataSourceStreamManager, "processDataStream" as any).resolves();
        sandbox.stub(task, "prepareAndTransformEvents" as any).rejects();
        await task["execute"]("test" as any);

        sandbox.assert.notCalled(task["modelVYMIMeta"].replaceTmpTables as any);
        sandbox.assert.calledOnce(task["modelVYMIMeta"].rollbackFailedSaving as any);
    });

    it("prepareAndSaveEvents should call certain functions", async () => {
        sandbox.stub(DataSourceStreamManager, "processDataStream" as any).resolves();
        sandbox.stub(task, "prepareAndTransformEvents" as any).rejects();

        const model = { truncate: sinon.stub(), save: sinon.stub() };
        await task["prepareAndSaveEvents"]("test", model as any, [], 69);

        sandbox.assert.calledOnce(model.truncate);
        sandbox.assert.calledOnceWithExactly(model.save, [], true);
    });

    it("getAllEvents should throw an error", async () => {
        sandbox.stub(DataSourceStreamManager, "processDataStream" as any).rejects();
        await expect(helper["getAllEvents"]()).to.be.rejectedWith();
    });

    it("validate message content", async () => {
        testSourceData["@cas_do"] = 123 as any;
        const testParamsInvalid = {
            digest: "abc",
            data: [testSourceData],
        };
        const promise = task.consume({ content: Buffer.from(JSON.stringify(testParamsInvalid)) } as any);

        await expect(promise).to.be.rejectedWith(
            GeneralError,
            "[Queue test.test.fetchAndProcessEvents] Message validation failed: [@cas_do must be a string]"
        );
    });
});
