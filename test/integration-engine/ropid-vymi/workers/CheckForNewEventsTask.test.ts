import { CheckForNewEventsTask } from "#ie/ropid-vymi/workers/tasks/CheckForNewEventsTask";
import RopidVYMIApiHelper from "#ie/ropid-vymi/workers/tasks/helpers/RopidVYMIApiHelper";
import { QueueManager } from "@golemio/core/dist/integration-engine";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import chai from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";

chai.use(chaiAsPromised);

describe("RopidVYMIWorker - CheckForNewEventsTask", () => {
    let sandbox: SinonSandbox;
    let task: CheckForNewEventsTask;
    let helper: RopidVYMIApiHelper;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        sandbox = sinon.createSandbox();

        sandbox
            .stub(PostgresConnector, "getConnection")
            .callsFake(() => Object.assign({ define: sandbox.stub(), transaction: sandbox.stub() }));

        task = new CheckForNewEventsTask("test.test");
        helper = RopidVYMIApiHelper.getInstance();
        sandbox.stub(QueueManager, "sendMessageToExchange" as any).resolves();

        sandbox.stub(helper, <any>"datasource").value(Object.assign({ getAll: sandbox.stub() }));
        sandbox.stub(task["modelVYMIMeta"], "getDigest").resolves("cf23df2207d99a74fbe169e3eba035e633b65d94");
    });

    afterEach(() => {
        sandbox?.restore();
    });

    it("checkForNewEvents should send a message to exchange", async () => {
        sandbox.stub(helper, "getAllEvents" as any).resolves({ data: [], digest: "some_bs" });
        await task["execute"]();

        sandbox.assert.calledOnce(QueueManager["sendMessageToExchange"] as any);
    });

    it("checkForNewEvents should not do anything", async () => {
        sandbox.stub(helper, "getAllEvents" as any).resolves({ data: [], digest: "cf23df2207d99a74fbe169e3eba035e633b65d94" });
        await task["execute"]();

        sandbox.assert.notCalled(QueueManager["sendMessageToExchange"] as any);
    });
});
