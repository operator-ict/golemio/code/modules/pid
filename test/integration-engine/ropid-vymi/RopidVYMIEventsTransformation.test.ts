import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import { RopidVYMIEventsTransformation } from "#ie/ropid-vymi/RopidVYMIEventsTransformation";

chai.use(chaiAsPromised);

describe("RopidVYMIEventsTransformation", () => {
    let transformation: RopidVYMIEventsTransformation;
    let testSourceData: any;
    let testTransformedEventsData: any;
    let testTransformedRoutesData: any[];
    let testTransformedStopsData: any[];

    beforeEach(async () => {
        transformation = new RopidVYMIEventsTransformation();
        testSourceData = JSON.parse(fs.readFileSync(__dirname + "/data/ropidvymi-datasource.json").toString("utf8"));
        testTransformedEventsData = JSON.parse(
            fs.readFileSync(__dirname + "/data/ropidvymi_events-transformed.json").toString("utf8")
        );

        testTransformedRoutesData = JSON.parse(
            fs.readFileSync(__dirname + "/data/ropidvymi_routes-transformed.json").toString("utf8")
        );

        testTransformedStopsData = JSON.parse(
            fs.readFileSync(__dirname + "/data/ropidvymi_stops-transformed.json").toString("utf8")
        );
    });

    it("should have name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("RopidVYMIEvents");
    });

    it("should have transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should properly transform data", async () => {
        const { events, routes, stops } = await transformation.transform(testSourceData["vymi-report"]["vymi-list"]);

        expect(events).to.deep.equal(testTransformedEventsData);
        expect(routes).to.deep.equal(testTransformedRoutesData);
        expect(stops).to.deep.equal(testTransformedStopsData);
    });
});
