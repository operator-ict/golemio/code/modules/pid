import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { RopidVYMIEventsModel } from "#ie/ropid-vymi/models/RopidVYMIEventsModel";

chai.use(chaiAsPromised);

describe("RopidVYMIEventsModel", () => {
    let model: RopidVYMIEventsModel;

    before(async () => {
        await PostgresConnector.connect();
        model = new RopidVYMIEventsModel();
    });

    it("vymi_id 83 -> event_type 268435456", async () => {
        const result = await model.findOne({
            where: {
                vymi_id: "83",
            },
        });

        expect(result.dataValues.event_type).to.eq("268435456");
    });

    it("vymi_id 71 -> event_type 1040187392", async () => {
        const result = await model.findOne({
            where: {
                vymi_id: "71",
            },
        });

        expect(result.dataValues.event_type).to.eq("1040187392");
    });

    it("vymi_id 13581 -> event_type 524288", async () => {
        const result = await model.findOne({
            where: {
                vymi_id: "13581",
            },
        });

        expect(result.dataValues.event_type).to.eq("524288");
    });
});
