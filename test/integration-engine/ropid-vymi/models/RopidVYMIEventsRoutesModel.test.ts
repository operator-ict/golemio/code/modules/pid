import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { RopidVYMIEventsRoutesModel } from "#ie/ropid-vymi/models/RopidVYMIEventsRoutesModel";

chai.use(chaiAsPromised);

describe("RopidVYMIEventsRoutesModel", () => {
    let model: RopidVYMIEventsRoutesModel;

    before(async () => {
        await PostgresConnector.connect();
        model = new RopidVYMIEventsRoutesModel();
    });

    it("gtfs_route_id L152 -> text sample 1, 2", async () => {
        const result = await model.findAndCountAll({
            where: {
                gtfs_route_id: "L152",
            },
        });

        expect(result.rows[0].dataValues.text).to.eq("text sample 1");
        expect(result.rows[1].dataValues.text).to.eq("text sample 2");
    });

    it("gtfs_route_id L177 -> text sample 3", async () => {
        const result = await model.findOne({
            where: {
                gtfs_route_id: "L177",
            },
        });

        expect(result.dataValues.text).to.eq("text sample 3");
    });
});
