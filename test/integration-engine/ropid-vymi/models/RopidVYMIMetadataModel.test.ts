import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox, SinonSpy } from "sinon";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { RopidVYMIMetadataModel } from "#ie/ropid-vymi/models";

chai.use(chaiAsPromised);

describe("RopidVYMIMetadataModel", () => {
    let model: RopidVYMIMetadataModel;
    let sandbox: SinonSandbox;

    beforeEach(async () => {
        sandbox = sinon.createSandbox();
        await PostgresConnector.connect();
        model = new RopidVYMIMetadataModel();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("getDigest should return a digest", async () => {
        sandbox
            .stub(model["sequelizeModel"], "findOne")
            .resolves({ dataValues: { value: "cf23df2207d99a74fbe169e3eba035e633b65d94" } } as any);

        expect(await model.getDigest("")).to.equal("cf23df2207d99a74fbe169e3eba035e633b65d94");
    });

    it("getDigest should return null", async () => {
        sandbox.stub(model["sequelizeModel"], "findOne").resolves(null as any);
        expect(await model.getDigest("")).to.be.null;
    });

    it("getDigest should log an error and return null", async () => {
        sandbox.stub(model["sequelizeModel"], "findOne").rejects("sth");
        sandbox.stub(log, "warn");

        expect(await model.getDigest("")).to.be.null;
        sandbox.assert.calledOnce(log.warn as SinonSpy);
    });
});
