import { RetentionContainer } from "#ie/data-retention/ioc/Di";
import { RetentionContainerToken } from "#ie/data-retention/ioc/RetentionContainerToken";
import { ITableStorageRetentionParams } from "#ie/data-retention/workers/interfaces/ITableStorageRetentionParams";
import { TableStorageRetentionTask } from "#ie/data-retention/workers/tasks/TableStorageRetentionTask";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";

chai.use(chaiAsPromised);

describe("VPWorker - TableStorageRetentionTask", () => {
    let sandbox: SinonSandbox;
    let task: TableStorageRetentionTask;

    const testParamsValid: ITableStorageRetentionParams = {
        tableName: "TcpDppTramData",
    };

    const testParamsInvalid: ITableStorageRetentionParams = {
        tableName: "TcpDppAirplaneData",
    };

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        task = RetentionContainer.resolve(RetentionContainerToken.TableStorageRetentionTask);
    });

    afterEach(() => {
        sandbox?.restore();
    });

    it("should validate and execute", async () => {
        const clock = sinon.useFakeTimers({
            now: new Date(2023, 0, 31, 11, 0, 0),
        });

        const deleteEntitiesOlderThanStub = sandbox.stub(task["storageService"], "deleteEntitiesOlderThan").resolves();
        const promise = task.consume({ content: Buffer.from(JSON.stringify(testParamsValid)) } as any);

        await expect(promise).to.be.fulfilled;
        expect(deleteEntitiesOlderThanStub.getCall(0).args).to.deep.equal(["TcpDppTramData", "2022-12-31T00:00:00.000Z"]);

        clock.restore();
    });

    it("should not validate (table name not listed)", async () => {
        const deleteEntitiesOlderThanStub = sandbox.stub(task["storageService"], "deleteEntitiesOlderThan").resolves();
        const promise = task.consume({ content: Buffer.from(JSON.stringify(testParamsInvalid)) } as any);

        await expect(promise).to.be.rejectedWith(
            GeneralError,
            // eslint-disable-next-line max-len
            "[Queue DataRetention.deleteMonthOldStorageData] Message validation failed: [tableName must be one of the following values: TcpDppTramData, TcpDppBusData, TcpDppMetroData, TcpArrivaCityData]"
        );

        expect(deleteEntitiesOlderThanStub.callCount).to.equal(0);
    });
});
