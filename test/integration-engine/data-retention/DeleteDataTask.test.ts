import { RepositoryTableName } from "#ie/data-retention/workers/constants/RepositoryTableNameEnum";
import { DeleteDataTask } from "#ie/data-retention/workers/tasks/DeleteDataTask";
import { VPContainer } from "#ie/vehicle-positions/ioc/Di";
import { PostgresModel } from "@golemio/core/dist/integration-engine";
import IPostgresConnector from "@golemio/core/dist/integration-engine/connectors/interfaces/IPostgresConnector";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc/ContainerToken";
import { Op, Sequelize } from "@golemio/core/dist/shared/sequelize";
import { expect } from "chai";

describe("DeleteDataTask", () => {
    let task: DeleteDataTask;

    before(async () => {
        const postgresConnector = VPContainer.resolve<IPostgresConnector>(ContainerToken.PostgresConnector);
        await postgresConnector.connect();
    });

    beforeEach(() => {
        task = new DeleteDataTask("deleteData");
    });

    it("should delete old data in the descriptor table", async () => {
        await task["execute"]({ targetHours: 24, repoName: RepositoryTableName.Descriptor });
        const repo = (await task["selectRepositoryFactory"].select(RepositoryTableName.Descriptor)) as PostgresModel;
        const result = await repo.findAndCountAll({
            where: {
                updated_at: {
                    [Op.lt]: Sequelize.literal(`NOW() - INTERVAL '${24} HOURS'`),
                },
            },
        });
        expect(result.count).to.be.equal(0);
    });

    it("should delete old data in the logs table", async () => {
        await task["execute"]({ targetHours: 24, repoName: RepositoryTableName.Logs });
        const repo = (await task["selectRepositoryFactory"].select(RepositoryTableName.Logs)) as PostgresModel;
        const result = await repo.findAndCountAll({
            where: {
                updated_at: {
                    [Op.lt]: Sequelize.literal(`NOW() - INTERVAL '${24} HOURS'`),
                },
            },
        });
        expect(result.count).to.be.equal(0);
    });
});
