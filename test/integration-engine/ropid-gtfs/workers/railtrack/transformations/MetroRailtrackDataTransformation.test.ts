import { expect } from "chai";
import { MetroRailtrackDataTransformation } from "#ie/ropid-gtfs/workers/timetables/tasks/transformations/MetroRailtrackDataTransformation";
import { IMetroRailtrack } from "#sch/datasources/static-data";

describe("MetroRailtrackDataTransformation", () => {
    const testData: IMetroRailtrack[] = [
        {
            track_section: "2109",
            line: "A",
            gps_lat: "50.075172",
            gps_lon: "14.340794",
            stop_id: "",
        },
        {
            track_section: "2107",
            line: "A",
            gps_lat: "50.075360",
            gps_lon: "14.341560",
            stop_id: "U306Z101P",
        },
    ];

    it("transform should transform array", async () => {
        const transformation = new MetroRailtrackDataTransformation();
        const result = transformation.transformArray(testData);
        expect(result[0]).to.deep.equal({
            track_id: "2109",
            route_name: "A",
            coordinates: {
                type: "Point",
                coordinates: [14.340794, 50.075172],
            },
            gtfs_stop_id: null,
        });

        expect(result[1]).to.deep.equal({
            track_id: "2107",
            route_name: "A",
            coordinates: {
                type: "Point",
                coordinates: [14.34156, 50.07536],
            },
            gtfs_stop_id: "U306Z101P",
        });
    });
});
