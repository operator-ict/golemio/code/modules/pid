import { expect } from "chai";
import { IDeparturesDirection } from "#sch/datasources/static-data";
import { DeparturesDirectionTransformation } from "#ie/ropid-gtfs/workers/timetables/tasks/transformations/DeparturesDirectionTransformation";

describe("DeparturesDirectionTransformation", () => {
    const testData: IDeparturesDirection[] = [
        {
            create_batch_id: "1",
            direction: "bottom",
            departure_stop_id: "",
            next_stop_id_regexp: "123",
            rule_order: "1",
            update_batch_id: "",
            updated_by: "1",
            created_by: "2",
        },
        {
            create_batch_id: "1",
            direction: "bottom",
            departure_stop_id: "",
            next_stop_id_regexp: "123",
            rule_order: "12",
            update_batch_id: "1",
            updated_by: "",
            created_by: "",
        },
    ];

    it("transformArray should transform array", async () => {
        const transformation = new DeparturesDirectionTransformation();
        const result = transformation.transformArray(testData);
        expect(result).to.deep.equal([
            {
                create_batch_id: "1",
                direction: "bottom",
                departure_stop_id: "",
                next_stop_id_regexp: "123",
                rule_order: 1,
                update_batch_id: null,
                updated_by: "1",
                created_by: "2",
            },
            {
                create_batch_id: "1",
                direction: "bottom",
                departure_stop_id: "",
                next_stop_id_regexp: "123",
                rule_order: 12,
                update_batch_id: "1",
                updated_by: null,
                created_by: null,
            },
        ]);
    });
});
