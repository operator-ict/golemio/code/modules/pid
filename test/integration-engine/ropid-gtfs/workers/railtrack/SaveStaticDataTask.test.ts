import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { IDeparturesDirection, IMetroRailtrack } from "#sch/datasources/static-data";
import { SaveStaticDataTask } from "#ie/ropid-gtfs/workers/timetables/tasks/SaveStaticDataTask";
import { RopidGtfsContainerToken } from "#ie/ropid-gtfs/ioc/RopidGtfsContainerToken";
import { DeparturesDirectionModel } from "#sch/ropid-gtfs/models/DeparturesDirectionDto";
import { DependencyContainer, container } from "@golemio/core/dist/shared/tsyringe";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { RopidGtfsContainer } from "#ie/ropid-gtfs/ioc/Di";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { MetroRailtrackGPSModel } from "#sch/ropid-gtfs/models/railtrack";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";

chai.use(chaiAsPromised);

describe("SaveStaticDataTask", async () => {
    let sandbox: SinonSandbox;
    let task: SaveStaticDataTask;
    let testContainer: DependencyContainer;
    let config: ISimpleConfig;

    const MetroRailTrackTestDataValid: IMetroRailtrack[] = [
        {
            track_section: "2107",
            line: "A",
            gps_lat: "50.075360",
            gps_lon: "14.341560",
            stop_id: "U306Z101P",
        },
    ];
    const DeparturesDirectionsTestDataValid: IDeparturesDirection[] = [
        {
            create_batch_id: "1",
            direction: "bottom",
            departure_stop_id: "",
            next_stop_id_regexp: "123",
            rule_order: "12",
            update_batch_id: "1",
            updated_by: "",
            created_by: "",
        },
    ];
    before(() => {
        sandbox = sinon.createSandbox();
        testContainer = RopidGtfsContainer.createChildContainer();
        testContainer.registerSingleton(
            CoreToken.SimpleConfig,
            class DummyConfig {
                getValue = () => "https://1";
            }
        );
    });

    beforeEach(() => {
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() => Object.assign({ define: sandbox.stub() }));
        task = testContainer.resolve<SaveStaticDataTask>(RopidGtfsContainerToken.SaveStaticDataTask);
        config = testContainer.resolve(CoreToken.SimpleConfig);
    });

    afterEach(() => {
        sandbox?.restore();
    });

    it("should validate and execute", async () => {
        const saveRailtrackDataStub = sinon.stub(task["railtrackGPSRepository"], "saveData").resolves();
        const getRailtrackDataStub = sinon
            .stub(task["staticDataSourceFactory"]["getDataSource"]("metroRailTrack"), "getAll")
            .resolves(MetroRailTrackTestDataValid);
        const getDeparturesDirectionDataStub = sinon
            .stub(task["staticDataSourceFactory"]["getDataSource"]("departuresDirections"), "getAll")
            .resolves(DeparturesDirectionsTestDataValid);
        const saveDeparturesDirectionDataStub = sinon.stub(task["departuresDirectionRepository"], "saveData").resolves();
        const promise = task.consume({ content: Buffer.from("") } as any);

        await expect(promise).to.be.fulfilled;
        expect(getDeparturesDirectionDataStub.callCount).to.equal(1);
        expect(saveDeparturesDirectionDataStub.callCount).to.equal(1);
        expect(
            saveDeparturesDirectionDataStub.calledWith([
                {
                    create_batch_id: "1",
                    direction: "bottom",
                    departure_stop_id: "",
                    next_stop_id_regexp: "123",
                    rule_order: 12,
                    update_batch_id: "1",
                    updated_by: null,
                    created_by: null,
                },
            ] as DeparturesDirectionModel[])
        ).to.be.true;
        expect(
            saveRailtrackDataStub.calledWith([
                {
                    track_id: "2107",
                    route_name: "A",
                    coordinates: {
                        type: "Point",
                        coordinates: [Number.parseFloat("14.341560"), Number.parseFloat("50.075360")],
                    },
                    gtfs_stop_id: "U306Z101P",
                },
            ] as MetroRailtrackGPSModel[])
        ).to.be.true;
        expect(getRailtrackDataStub.callCount).to.equal(1);
        expect(saveRailtrackDataStub.callCount).to.equal(1);
    });
});
