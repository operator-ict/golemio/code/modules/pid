import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { DeparturesDirectionRepository } from "#ie/ropid-gtfs/workers/timetables/tasks/data-access/DeparturesDirectionRepository";
import { IDeparturesDirectionDto } from "#sch/ropid-gtfs/interfaces/IDeparturesDirectionDto";

chai.use(chaiAsPromised);

describe("DeparturesDirectionRepository", () => {
    let repository: DeparturesDirectionRepository;

    before(() => {
        return PostgresConnector.connect();
    });

    beforeEach(() => {
        repository = new DeparturesDirectionRepository();
    });

    it("saveData should save data", async () => {
        const auditTimestamp = new Date().getTime();
        const data: Array<IDeparturesDirectionDto & { created_at: number; updated_at: number }> = [
            {
                create_batch_id: "1",
                direction: "bottom",
                departure_stop_id: "",
                next_stop_id_regexp: "",
                rule_order: 1,
                update_batch_id: "1",
                updated_by: "",
                created_by: "",

                // Inject timestamps
                created_at: auditTimestamp,
                updated_at: auditTimestamp,
            },
        ];

        const result = await repository.saveData(data as any, false, true);
        expect(result[0].toJSON()).to.deep.equal({
            id: 2,
            create_batch_id: "1",
            direction: "bottom",
            departure_stop_id: "",
            next_stop_id_regexp: "",
            rule_order: 1,
            update_batch_id: "1",
            created_by: "",
            updated_by: "",
            created_at: new Date(auditTimestamp),
            updated_at: new Date(auditTimestamp),
        });
    });
});
