import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { IMetroRailtrackGPSModel } from "#sch/ropid-gtfs/models/railtrack/interfaces/IMetroRailtrackGPSModel";
import { MetroRailtrackGPSRepository } from "#ie/ropid-gtfs/workers/timetables/tasks/data-access/MetroRailtrackGPSRepository";

chai.use(chaiAsPromised);

describe("MetroRailtrackGPSRepository", () => {
    let repository: MetroRailtrackGPSRepository;

    before(() => {
        return PostgresConnector.connect();
    });

    beforeEach(() => {
        repository = new MetroRailtrackGPSRepository();
    });

    it("findCoordinates should find data", async () => {
        const result = await repository.findCoordinates("A", ["1806", "1209"]);
        expect(result).to.deep.equal({
            coordinates: {
                "1806": {
                    lat: 50.098434,
                    lon: 14.362868,
                    gtfs_stop_id: "U157Z102P",
                },
                "1209": {
                    lat: 50.08006,
                    lon: 14.430655,
                    gtfs_stop_id: null,
                },
            },
        });
    });

    it("saveData should save data", async () => {
        const auditTimestamp = new Date().getTime();
        const data: Array<IMetroRailtrackGPSModel & { created_at: number; updated_at: number }> = [
            {
                track_id: "2107",
                route_name: "A",
                coordinates: {
                    type: "Point",
                    coordinates: [14.34156, 50.07536],
                },
                gtfs_stop_id: "U306Z101P",

                // Inject timestamps
                created_at: auditTimestamp,
                updated_at: auditTimestamp,
            },
        ];

        const result = await repository.saveData(data as any, false, true);
        expect(result[0].toJSON()).to.deep.equal({
            track_id: "2107",
            route_name: "A",
            coordinates: {
                type: "Point",
                coordinates: [14.34156, 50.07536],
            },
            gtfs_stop_id: "U306Z101P",
            created_at: new Date(auditTimestamp),
            updated_at: new Date(auditTimestamp),
        });

        await repository["sequelizeModel"].destroy({
            where: {
                track_id: "2107",
                route_name: "A",
            },
        });
    });
});
