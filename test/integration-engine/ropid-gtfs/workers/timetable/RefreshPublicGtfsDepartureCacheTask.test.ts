import { GTFSRouteTypeEnum } from "#helpers/RouteTypeEnums";
import { IPublicDepartureDto } from "#ie/ropid-gtfs/interfaces/IPublicDepartureDto";
import { RopidGtfsContainerToken } from "#ie/ropid-gtfs/ioc/RopidGtfsContainerToken";
import { PublicDepartureCacheTransformation } from "#ie/ropid-gtfs/transformations/PublicDepartureCacheTransformation";
import { RefreshPublicGtfsDepartureCacheTask } from "#ie/ropid-gtfs/workers/timetables/tasks/RefreshPublicGtfsDepartureCacheTask";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ContainerToken, IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import { expect } from "chai";
import sinon, { SinonSandbox, SinonStub } from "sinon";
import { VPContainer } from "#ie/vehicle-positions/ioc/Di";
import IPostgresConnector from "@golemio/core/dist/integration-engine/connectors/interfaces/IPostgresConnector";

describe("RefreshPublicGtfsDepartureCacheTask", () => {
    let sandbox: SinonSandbox;
    let container: DependencyContainer;
    let mockReplaceDepartures: SinonStub;
    let mockConfigGetValue: SinonStub;
    let mockRopidGTFSStopsModel: SinonStub;
    before(async () => {
        const postgresConnector = VPContainer.resolve<IPostgresConnector>(ContainerToken.PostgresConnector);
        await postgresConnector.connect();
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        container = IntegrationEngineContainer.createChildContainer();

        mockReplaceDepartures = sandbox.stub().resolves();
        mockConfigGetValue = sandbox.stub().returns("1");
        mockRopidGTFSStopsModel = sandbox.stub().resolves([{ stop_id: "U717Z6P" }, { stop_id: "U717Z7P" }]);

        container
            .registerSingleton(RopidGtfsContainerToken.PublicDepartureCacheTransformation, PublicDepartureCacheTransformation)
            .registerSingleton(
                RopidGtfsContainerToken.DepartureRepository,
                class DummyDepartureRepository {
                    countDeparturesForPublicCache = async () => 2;
                    getDepaturesForPublicCache = (page: number): IPublicDepartureDto[] => {
                        if (page === 0) {
                            return [
                                {
                                    stop_id: "U717Z6P",
                                    departure_datetime: new Date("2024-04-24T22:50:00+02:00"),
                                    arrival_datetime: null,
                                    route_short_name: "X1",
                                    route_type: GTFSRouteTypeEnum.BUS,
                                    trip_id: "801_2228_240417",
                                    stop_sequence: 3,
                                    platform_code: "F",
                                    trip_headsign: "Hradčanská",
                                },
                            ];
                        } else if (page === 1) {
                            return [
                                {
                                    stop_id: "U717Z5P",
                                    departure_datetime: new Date("2024-04-24T22:52:00+02:00"),
                                    arrival_datetime: new Date("2024-04-24T22:52:00+02:00"),
                                    route_short_name: "27",
                                    route_type: GTFSRouteTypeEnum.TRAM,
                                    trip_id: "27_3074_240217",
                                    stop_sequence: 4,
                                    platform_code: "E",
                                    trip_headsign: "Libuš",
                                },
                            ];
                        }

                        return [];
                    };
                }
            )
            .registerSingleton(
                RopidGtfsContainerToken.PublicGtfsDepartureRepository,
                class DummyPublicGtfsDepartureRepository {
                    replaceDeparturesForStop = mockReplaceDepartures;
                }
            )
            .registerSingleton(
                CoreToken.SimpleConfig,
                class DummySimpleConfig {
                    getValue = mockConfigGetValue;
                }
            );

        container.registerSingleton(
            RopidGtfsContainerToken.RefreshPublicGtfsDepartureCacheTask,
            RefreshPublicGtfsDepartureCacheTask
        );

        container.registerSingleton(
            RopidGtfsContainerToken.RopidGTFSStopsModel,
            class DummyRopidGTFSStopsModel {
                getAll = mockRopidGTFSStopsModel;
            }
        );
    });

    afterEach(() => {
        sandbox?.restore();
    });

    it("should create departures for public cache", async () => {
        const task = container.resolve<RefreshPublicGtfsDepartureCacheTask>(
            RopidGtfsContainerToken.RefreshPublicGtfsDepartureCacheTask
        );

        await task["execute"]({
            intervalFromHours: -12,
            intervalToHours: 12,
        });
        expect(mockReplaceDepartures.callCount).to.equal(3);
        expect(mockRopidGTFSStopsModel.callCount).to.equal(1);

        expect(mockReplaceDepartures.firstCall.args).to.deep.equal([
            [
                {
                    stop_id: "U717Z6P",
                    departure_datetime: "2024-04-24T20:50:00.000Z",
                    arrival_datetime: null,
                    route_short_name: "X1",
                    route_type: 3,
                    trip_id: "801_2228_240417",
                    stop_sequence: 3,
                    platform_code: "F",
                    trip_headsign: "Hradčanská",
                },
            ],
            "U717Z6P",
            { intervalFromHours: -12, intervalToHours: 12 },
        ]);

        expect(mockReplaceDepartures.secondCall.args).to.deep.equal([
            [
                {
                    stop_id: "U717Z5P",
                    departure_datetime: "2024-04-24T20:52:00.000Z",
                    arrival_datetime: "2024-04-24T20:52:00.000Z",
                    route_short_name: "27",
                    route_type: 0,
                    trip_id: "27_3074_240217",
                    stop_sequence: 4,
                    platform_code: "E",
                    trip_headsign: "Libuš",
                },
            ],
            "U717Z5P",
            { intervalFromHours: -12, intervalToHours: 12 },
        ]);
        expect(mockReplaceDepartures.secondCall.args).to.deep.equal([
            [
                {
                    stop_id: "U717Z5P",
                    departure_datetime: "2024-04-24T20:52:00.000Z",
                    arrival_datetime: "2024-04-24T20:52:00.000Z",
                    route_short_name: "27",
                    route_type: 0,
                    trip_id: "27_3074_240217",
                    stop_sequence: 4,
                    platform_code: "E",
                    trip_headsign: "Libuš",
                },
            ],
            "U717Z5P",
            { intervalFromHours: -12, intervalToHours: 12 },
        ]);
        expect(mockReplaceDepartures.thirdCall.args).to.deep.equal([
            [],
            "U717Z7P",
            { intervalFromHours: -12, intervalToHours: 12 },
        ]);
    });

    it("should throw an error when replacing departures fails", async () => {
        mockReplaceDepartures.rejects(new Error("Failed to replace departures"));

        const task = container.resolve<RefreshPublicGtfsDepartureCacheTask>(
            RopidGtfsContainerToken.RefreshPublicGtfsDepartureCacheTask
        );

        try {
            await task["execute"]({
                intervalFromHours: -12,
                intervalToHours: 12,
            });

            expect.fail("Should throw an error");
        } catch (err) {
            expect(err.message).to.equal("Error while refreshing public departure cache");
        }
    });

    it("getIntervalParamsWithDefault should return provided params", () => {
        const task = container.resolve<RefreshPublicGtfsDepartureCacheTask>(
            RopidGtfsContainerToken.RefreshPublicGtfsDepartureCacheTask
        );

        const intervalParams = task["getIntervalParamsWithDefault"]({
            intervalFromHours: -10,
            intervalToHours: 10,
        });
        expect(intervalParams).to.deep.equal({
            intervalFromHours: -10,
            intervalToHours: 10,
        });
    });

    it("getIntervalParamsWithDefault should return default values when params are not provided", () => {
        mockConfigGetValue
            .withArgs("module.pid.ropid-gtfs.timetables.refreshPublicGtfsDepartureCache.defaultIntervalFromHours")
            .returns("-3");

        mockConfigGetValue
            .withArgs("module.pid.ropid-gtfs.timetables.refreshPublicGtfsDepartureCache.defaultIntervalToHours")
            .returns("6");

        const task = container.resolve<RefreshPublicGtfsDepartureCacheTask>(
            RopidGtfsContainerToken.RefreshPublicGtfsDepartureCacheTask
        );

        const intervalParams = task["getIntervalParamsWithDefault"]({});
        expect(intervalParams).to.deep.equal({
            intervalFromHours: -3,
            intervalToHours: 6,
        });
    });
});
