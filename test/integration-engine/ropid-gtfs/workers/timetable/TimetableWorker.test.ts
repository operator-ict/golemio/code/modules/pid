import { DatasetEnum, IRopidGtfsTransformationData, RopidGtfsFacade } from "#ie/ropid-gtfs";
import { RopidGtfsMetadataRepository } from "#ie/ropid-gtfs/RopidGtfsMetadataRepository";
import { RopidGtfsRepository } from "#ie/ropid-gtfs/data-access/RopidGtfsRepository";
import { StaticFileRedisRepository } from "#ie/ropid-gtfs/data-access/cache/StaticFileRedisRepository";
import { DataCacheManager } from "#ie/ropid-gtfs/helpers/DataCacheManager";
import { RopidGtfsContainer } from "#ie/ropid-gtfs/ioc/Di";
import { RopidGtfsContainerToken } from "#ie/ropid-gtfs/ioc/RopidGtfsContainerToken";
import { CheckForNewDataTask } from "#ie/ropid-gtfs/workers/timetables/tasks/CheckForNewDataTask";
import { CheckSavedRowsAndReplaceTablesTask } from "#ie/ropid-gtfs/workers/timetables/tasks/CheckSavedRowsAndReplaceTablesTask";
import { DownloadDatasetsTask } from "#ie/ropid-gtfs/workers/timetables/tasks/DownloadDatasetsTask";
import { TransformAndSaveDataTask } from "#ie/ropid-gtfs/workers/timetables/tasks/TransformAndSaveDataTask";
import { PrecomputedTablesFacade } from "#ie/ropid-gtfs/workers/timetables/tasks/helpers/PrecomputedTablesFacade";
import { RopidGtfsFactory } from "#ie/ropid-gtfs/workers/timetables/tasks/helpers/RopidGtfsFactory";
import { ICheckForNewDataInput } from "#ie/ropid-gtfs/workers/timetables/tasks/interfaces/ICheckForNewDataInput";
import { IDatasetsInput } from "#ie/ropid-gtfs/workers/timetables/tasks/interfaces/IDatasetsInput";
import { DatasetsInputSchema } from "#ie/ropid-gtfs/workers/timetables/tasks/schema/DownloadDataInputSchema";
import { QueueManager } from "@golemio/core/dist/integration-engine";
import { config } from "@golemio/core/dist/integration-engine/config";
import { PostgresConnector, RedisConnector } from "@golemio/core/dist/integration-engine/connectors";
import { log } from "@golemio/core/dist/integration-engine/helpers/Logger";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox, SinonSpy, SinonStub } from "sinon";
import { Readable } from "stream";

chai.use(chaiAsPromised);

describe("TimetableWorker", () => {
    let sandbox: SinonSandbox;
    let testGtfsData: any[];
    let agencyData: Record<string, any>;
    let testTransformedData: IRopidGtfsTransformationData;
    let testDataCis: Record<string, any>;
    let testDataRunNumbers: Record<string, any>;
    let testDataOis: Record<string, any>;

    let modelSyncStub: SinonStub;
    let staticFileRedisRepository: StaticFileRedisRepository;
    let dataCacheManager: DataCacheManager;
    let ropidGtfsFactory: RopidGtfsFactory;
    let transformAndSaveTask: TransformAndSaveDataTask;
    let precomputeFacade: PrecomputedTablesFacade;
    let metadataRepository: RopidGtfsMetadataRepository;
    let ropidGtfsFacade: RopidGtfsFacade;
    let ropidGtfsRepository: RopidGtfsRepository;

    let modelTruncateStub: SinonStub;
    let modelSaveStub: SinonStub;

    beforeEach(async () => {
        sandbox = sinon.createSandbox({ useFakeTimers: true });
        await RedisConnector.connect();
        await PostgresConnector.connect();
        metadataRepository = RopidGtfsContainer.resolve<RopidGtfsMetadataRepository>(
            RopidGtfsContainerToken.RopidGtfsMetadataRepository
        );
        ropidGtfsFacade = RopidGtfsContainer.resolve<RopidGtfsFacade>(RopidGtfsContainerToken.RopidGtfsFacade);

        ropidGtfsRepository = RopidGtfsContainer.resolve<RopidGtfsRepository>(RopidGtfsContainerToken.RopidGtfsRepository);

        testGtfsData = [
            {
                filepath: "PID_GTFS/agency.txt",
                mtime: "2021-03-18T03:54:20.000Z",
                name: "agency",
                path: "agency.txt",
            },
            {
                filepath: "PID_GTFS/calendar.txt",
                mtime: "2021-03-18T03:54:32.000Z",
                name: "calendar",
                path: "calendar.txt",
            },
        ];

        testDataCis = { filepath: "Stops.Min.json", name: "Stops.Min" };

        testDataOis = { filepath: "oisMapping.json", name: "oisMapping" };

        testDataRunNumbers = { filepath: "obehy.csv", name: "obehy" };

        agencyData = {
            agency_id: "99",
            agency_name: "Pražská integrovaná doprava",
            agency_url: "https://pid.cz",
            agency_timezone: "Europe/Prague",
            agency_lang: "cs",
            agency_phone: "+420234704560",
        };

        testTransformedData = { sourceStream: Readable.from(JSON.stringify(agencyData)), name: "agency" };

        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().callsFake(() =>
                    Object.assign({
                        removeAttribute: sandbox.stub(),
                    })
                ),
                query: sandbox.stub(),
            })
        );
        sandbox.stub(RedisConnector, "getConnection");

        sandbox.stub(config, "datasources").value({
            RopidGTFSRunNumbersFilename: "obehy.csv",
            RopidGTFSOisFilename: "oisMapping.json",
            RopidGTFSCisStopsFilename: "Stops.Min.json",
        });

        sandbox
            .stub(metadataRepository, "getLastModified")
            .callsFake(() => Object.assign({ lastModified: "2019-01-18T03:24:09.000Z", version: 0 }));
        sandbox.stub(metadataRepository, "save");
        sandbox.stub(metadataRepository, "checkSavedRows");

        sandbox.stub(metadataRepository, "replaceTmpTables");
        sandbox.stub(metadataRepository, "rollbackFailedSaving");
        sandbox.stub(metadataRepository, "updateState");
        sandbox.stub(metadataRepository, "checkIfNewVersionIsAlreadyDeployed");
        sandbox.stub(metadataRepository, "checkAllTablesHasSavedState").callsFake(() => Promise.resolve(true));
        sandbox.stub(ropidGtfsFacade, "checkSavedTmpTables");
        sandbox.stub(ropidGtfsFacade, "replaceTables");

        RopidGtfsContainer.register(RopidGtfsContainerToken.RopidGtfsMetadataRepository, { useValue: metadataRepository });
        RopidGtfsContainer.register(RopidGtfsContainerToken.RopidGtfsFacade, { useValue: ropidGtfsFacade });

        sandbox.stub(QueueManager, "sendMessageToExchange" as any);

        staticFileRedisRepository = RopidGtfsContainer.resolve<StaticFileRedisRepository>(
            RopidGtfsContainerToken.StaticFileRedisRepository
        );
        sandbox.stub(staticFileRedisRepository, "set");
        sandbox.stub(staticFileRedisRepository, "get").callsFake(async () => Buffer.from(JSON.stringify(agencyData)));
        sandbox.stub(staticFileRedisRepository, "pipeStream");
        sandbox.stub(staticFileRedisRepository, "getReadableStream").callsFake(() => Readable.from(JSON.stringify(agencyData)));
        dataCacheManager = DataCacheManager.getInstance();
        sandbox.stub(dataCacheManager, "cleanCache");

        precomputeFacade = RopidGtfsContainer.resolve<PrecomputedTablesFacade>(RopidGtfsContainerToken.PrecomputedTablesFacade);
        sandbox.stub(precomputeFacade, "createAndPopulatePrecomputedTmpTables" as any);

        ropidGtfsFactory = RopidGtfsContainer.resolve<RopidGtfsFactory>(RopidGtfsContainerToken.RopidGtfsFactory);
        sandbox.stub(ropidGtfsFactory["dataSourceGtfs"], "getAll").resolves(testGtfsData);
        sandbox.stub(ropidGtfsFactory["transformationGtfs"], "transform");

        sandbox.stub(ropidGtfsFactory["dataSourceCisStops"], "getAll").resolves(testDataCis);
        sandbox.stub(ropidGtfsFactory["dataSourceCisStops"], "getLastModified").resolves("2019-01-18T03:24:09.000Z");
        sandbox.stub(ropidGtfsFactory["transformationCisStops"], "transform");

        sandbox.stub(ropidGtfsFactory["dataSourceOisMapping"], "getAll").resolves(testDataOis);
        sandbox.stub(ropidGtfsFactory["dataSourceOisMapping"], "getLastModified").resolves("2019-01-18T03:24:09.000Z");
        sandbox.stub(ropidGtfsFactory["transformationOisMapping"], "transform");

        sandbox.stub(ropidGtfsFactory["dataSourceRunNumbers"], "getAll").resolves(testDataRunNumbers);
        sandbox.stub(ropidGtfsFactory["dataSourceRunNumbers"], "getLastModified").resolves("2019-01-18T03:24:09.000Z");
        RopidGtfsContainer.registerInstance(RopidGtfsContainerToken.RopidGtfsFactory, ropidGtfsFactory);

        sandbox.stub(ropidGtfsRepository, "replaceAllTable");
        sandbox.stub(ropidGtfsRepository, "createTmpTables");
        sandbox.stub(ropidGtfsRepository, "createPrecomputedTmpTables");
        sandbox.stub(ropidGtfsRepository, "cleanTmpAndOldTables");
        sandbox.stub(ropidGtfsRepository, "cleanOldTables");
        RopidGtfsContainer.registerInstance(RopidGtfsContainerToken.RopidGtfsRepository, ropidGtfsRepository);

        modelTruncateStub = sandbox.stub();
        modelSaveStub = sandbox.stub();

        transformAndSaveTask = new TransformAndSaveDataTask("test");
        modelTruncateStub = sandbox.stub();
        modelSaveStub = sandbox.stub();
        modelSyncStub = sandbox.stub();
        sandbox.stub(transformAndSaveTask, "getTmpModelByName" as any).callsFake(() =>
            Object.assign({
                save: modelSaveStub,
                saveBySqlFunction: modelSaveStub,
                truncate: modelTruncateStub,
                sync: modelSyncStub,
            })
        );
        sandbox.stub(transformAndSaveTask, "streamDataToTmp" as any);
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should calls the correct methods by checkForNewData method", async () => {
        sandbox.stub(ropidGtfsFactory["dataSourceGtfs"], "getLastModified").resolves("2019-01-18T03:22:09.000Z");
        sandbox.stub(metadataRepository, "isDeployed").resolves(false);

        await new CheckForNewDataTask("test").consume({
            content: Buffer.from(JSON.stringify({ forceRefresh: false } as ICheckForNewDataInput)),
        } as any);

        sandbox.assert.calledOnce(ropidGtfsFactory["dataSourceGtfs"].getLastModified as SinonSpy);
        sandbox.assert.calledOnce(ropidGtfsFactory["dataSourceCisStops"].getLastModified as SinonSpy);
        sandbox.assert.calledOnce(ropidGtfsFactory["dataSourceOisMapping"].getLastModified as SinonSpy);
        sandbox.assert.calledOnce(ropidGtfsFactory["dataSourceRunNumbers"].getLastModified as SinonSpy);
        sandbox.assert.callCount(metadataRepository.getLastModified as SinonSpy, 4);
        sandbox.assert.calledTwice(QueueManager["sendMessageToExchange"] as SinonSpy);
        sandbox.assert.callOrder(
            ropidGtfsFactory["dataSourceGtfs"].getLastModified as SinonSpy,
            metadataRepository.getLastModified as SinonSpy,
            ropidGtfsFactory["dataSourceCisStops"].getLastModified as SinonSpy,
            metadataRepository.getLastModified as SinonSpy,
            ropidGtfsFactory["dataSourceOisMapping"].getLastModified as SinonSpy,
            metadataRepository.getLastModified as SinonSpy,
            ropidGtfsFactory["dataSourceRunNumbers"].getLastModified as SinonSpy,
            metadataRepository.getLastModified as SinonSpy,
            QueueManager["sendMessageToExchange"] as SinonSpy,
            QueueManager["sendMessageToExchange"] as SinonSpy
        );
    });

    it("checkForNewData should send a message when forceRefresh is true", async () => {
        sandbox.stub(ropidGtfsFactory["dataSourceGtfs"], "getLastModified").resolves("2019-01-18T03:24:09.000Z");
        sandbox.stub(metadataRepository, "isDeployed").resolves(true);
        const logInfoStub = sandbox.stub(log, "info").resolves(true);

        await new CheckForNewDataTask("test").consume({
            content: Buffer.from(JSON.stringify({ forceRefresh: true } as ICheckForNewDataInput)),
        } as any);

        expect(logInfoStub.lastCall.args).to.deep.equal(["RopidGTFS datasets are up to date"]);
        expect((QueueManager["sendMessageToExchange"] as SinonStub).lastCall.args).to.deep.equal(["test", "saveStaticData", {}]);
        expect((QueueManager["sendMessageToExchange"] as SinonStub).firstCall.args).to.deep.equal([
            "test",
            "refreshPrecomputedTables",
            {},
        ]);
    });

    it("checkForNewData should send a message with all datasets when all of them are outdated", async () => {
        sandbox.stub(ropidGtfsFactory["dataSourceGtfs"], "getLastModified").resolves("2019-01-18T03:24:09.000Z");
        sandbox.stub(metadataRepository, "isDeployed").resolves(false);

        const task = new CheckForNewDataTask("test");
        const warnStub = sandbox.stub(task["logger"], "warn");

        await new CheckForNewDataTask("test").consume({
            content: Buffer.from(JSON.stringify({ forceRefresh: false } as ICheckForNewDataInput)),
        } as any);

        expect(warnStub.callCount).to.equal(0);
        expect((QueueManager["sendMessageToExchange"] as SinonStub).lastCall.args).to.deep.equal(["test", "saveStaticData", {}]);
        expect((QueueManager["sendMessageToExchange"] as SinonStub).firstCall.args).to.deep.equal([
            "test",
            "downloadDatasets",
            {
                datasets: ["PID_GTFS", "RUN_NUMBERS", "CIS_STOPS", "OIS_MAPPING"],
            },
        ]);
    });

    it("checkForNewData should send a message with all datasets when only some of them are outdated", async () => {
        sandbox.stub(ropidGtfsFactory["dataSourceGtfs"], "getLastModified").resolves("2019-01-18T03:24:09.000Z");
        sandbox
            .stub(metadataRepository, "isDeployed")
            .callsFake((dataset: string) => Promise.resolve(dataset !== DatasetEnum.PID_GTFS));

        const task = new CheckForNewDataTask("test");
        const warnStub = sandbox.stub(task["logger"], "warn");

        await new CheckForNewDataTask("test").consume({
            content: Buffer.from(JSON.stringify({ forceRefresh: false } as ICheckForNewDataInput)),
        } as any);

        expect(warnStub.lastCall?.args).to.deep.equal(["RopidGTFS datasets are out of sync"]);
        expect((QueueManager["sendMessageToExchange"] as SinonStub).lastCall.args).to.deep.equal(["test", "saveStaticData", {}]);
        expect((QueueManager["sendMessageToExchange"] as SinonStub).firstCall.args).to.deep.equal([
            "test",
            "downloadDatasets",
            {
                datasets: ["PID_GTFS", "RUN_NUMBERS", "CIS_STOPS", "OIS_MAPPING"],
            },
        ]);
    });

    it("should calls the correct methods by downloadDatasets method", async () => {
        const subscribeStub = sandbox.stub().resolves();
        const listenStub = sandbox.stub().resolves();
        const unsubscribeStub = sandbox.stub();
        const task = new DownloadDatasetsTask("test");
        sandbox.stub(ropidGtfsFactory["dataSourceGtfs"], "getLastModified").resolves("2019-01-18T03:22:09.000Z");
        sandbox.stub(task["gtfsRedisChannel"], "createSubscriber").returns({
            subscribe: subscribeStub,
            listen: listenStub,
            unsubscribe: unsubscribeStub,
        } as any);

        const promise = task.consume({
            content: Buffer.from(
                JSON.stringify({ datasets: ["PID_GTFS", "RUN_NUMBERS", "CIS_STOPS", "OIS_MAPPING"] } as IDatasetsInput)
            ),
        } as any);
        await sandbox.clock.tickAsync(5 * 60 * 1000);
        await promise;

        sandbox.assert.calledOnce(ropidGtfsFactory["dataSourceGtfs"].getAll as SinonSpy);
        sandbox.assert.calledOnce(ropidGtfsFactory["dataSourceCisStops"].getAll as SinonSpy);
        sandbox.assert.calledOnce(ropidGtfsFactory["dataSourceOisMapping"].getAll as SinonSpy);
        sandbox.assert.calledOnce(ropidGtfsFactory["dataSourceRunNumbers"].getAll as SinonSpy);

        sandbox.assert.callCount(staticFileRedisRepository.set as SinonSpy, 2);
        sandbox.assert.callCount(staticFileRedisRepository.pipeStream as SinonSpy, 3);

        testGtfsData.forEach((f) => {
            sandbox.assert.calledWith(staticFileRedisRepository.pipeStream as SinonSpy, f.filepath, f.data);
        });

        sandbox.assert.calledOnce(ropidGtfsFactory["dataSourceGtfs"].getLastModified as SinonSpy);
        sandbox.assert.calledOnce(ropidGtfsFactory["dataSourceCisStops"].getLastModified as SinonSpy);
        sandbox.assert.calledOnce(ropidGtfsFactory["dataSourceOisMapping"].getLastModified as SinonSpy);
        sandbox.assert.calledOnce(ropidGtfsFactory["dataSourceRunNumbers"].getLastModified as SinonSpy);

        sandbox.assert.callCount(metadataRepository.getLastModified as SinonSpy, 4);
        sandbox.assert.callCount(metadataRepository.save as SinonSpy, 10);

        sandbox.assert.calledOnce(ropidGtfsRepository.createTmpTables as SinonSpy);
        sandbox.assert.callCount(QueueManager["sendMessageToExchange"] as SinonSpy, 6);

        testGtfsData.forEach((f) => {
            sandbox.assert.calledWith(QueueManager["sendMessageToExchange"] as SinonSpy, "test", "transformAndSaveData", {
                ...f,
                dataset: DatasetEnum.PID_GTFS,
            });
        });

        [testDataRunNumbers].forEach((f) => {
            sandbox.assert.calledWith(QueueManager["sendMessageToExchange"] as SinonSpy, "test", "transformAndSaveData", {
                ...f,
                dataset: DatasetEnum.RUN_NUMBERS,
            });
        });

        [testDataCis].forEach((f) => {
            sandbox.assert.calledWith(QueueManager["sendMessageToExchange"] as SinonSpy, "test", "transformAndSaveData", {
                ...f,
                dataset: DatasetEnum.CIS_STOPS,
            });
        });

        [testDataOis].forEach((f) => {
            sandbox.assert.calledWith(QueueManager["sendMessageToExchange"] as SinonSpy, "test", "transformAndSaveData", {
                ...f,
                dataset: DatasetEnum.OIS_MAPPING,
            });
        });

        sandbox.assert.callCount(subscribeStub, 1);
        sandbox.assert.callCount(listenStub, 1);
        sandbox.assert.callCount(unsubscribeStub, 1);
        const expectedResult = new DatasetsInputSchema();
        expectedResult.datasets = [DatasetEnum.PID_GTFS, DatasetEnum.RUN_NUMBERS, DatasetEnum.CIS_STOPS, DatasetEnum.OIS_MAPPING];
        sandbox.assert.calledWith(
            QueueManager["sendMessageToExchange"] as SinonSpy,
            "test",
            "checkSavedRowsAndReplaceTables",
            expectedResult
        );

        sandbox.assert.callOrder(
            ropidGtfsFactory["dataSourceGtfs"].getAll as SinonSpy,
            metadataRepository.save as SinonSpy,
            subscribeStub,
            QueueManager["sendMessageToExchange"] as SinonSpy,
            listenStub,
            unsubscribeStub,
            QueueManager["sendMessageToExchange"] as SinonSpy
        );
    });

    it("should calls the correct methods by transformAndSaveData method (agency)", async () => {
        const publishStub = sandbox.stub(transformAndSaveTask["gtfsRedisChannel"], "publishMessage").resolves();
        await transformAndSaveTask.consume({
            content: Buffer.from(JSON.stringify({ ...testGtfsData[0], dataset: DatasetEnum.PID_GTFS })),
            properties: { headers: { "x-origin-hostname": "test" } },
        } as any);

        sandbox.assert.calledOnce(ropidGtfsFactory["transformationGtfs"].transform as SinonSpy);
        expect((ropidGtfsFactory["transformationGtfs"].transform as SinonSpy).getCall(0).args[0].name).to.equal(
            testGtfsData[0].name
        );
        expect((ropidGtfsFactory["transformationGtfs"].transform as SinonSpy).getCall(0).args[0].sourceStream).to.be.instanceOf(
            Readable
        );
        sandbox.assert.notCalled(modelSyncStub);
        sandbox.assert.calledOnce(transformAndSaveTask["streamDataToTmp"] as SinonSpy);
        sandbox.assert.calledOnce(metadataRepository.getLastModified as SinonSpy);
        sandbox.assert.calledOnce(metadataRepository.updateState as SinonSpy);
        expect(publishStub.getCall(0).args[0]).to.equal(`OK (${testGtfsData[0].name})`);
        expect(publishStub.getCall(0).args[1]).to.deep.equal({ channelSuffix: "test" });
    });

    it("should calls the correct methods by checkSavedRowsAndReplaceTables method", async () => {
        const task = new CheckSavedRowsAndReplaceTablesTask("test");
        await task.consume({
            content: Buffer.from(JSON.stringify({ datasets: [DatasetEnum.PID_GTFS] })),
        } as any);

        sandbox.assert.calledOnce(metadataRepository.getLastModified as SinonSpy);
        sandbox.assert.calledOnce(metadataRepository.checkIfNewVersionIsAlreadyDeployed as SinonSpy);
        sandbox.assert.calledOnce(metadataRepository.checkAllTablesHasSavedState as SinonSpy);
        sandbox.assert.calledOnce(ropidGtfsFacade.checkSavedTmpTables as SinonSpy);
        sandbox.assert.calledOnce(ropidGtfsFacade.replaceTables as SinonSpy);
        sandbox.assert.calledOnce(dataCacheManager.cleanCache as SinonSpy);
        sandbox.assert.calledOnce(precomputeFacade["createAndPopulatePrecomputedTmpTables"] as SinonSpy);
        sandbox.assert.calledOnce(ropidGtfsRepository.cleanTmpAndOldTables as SinonSpy);
    });
});
