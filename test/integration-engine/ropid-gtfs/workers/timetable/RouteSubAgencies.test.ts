import { RopidGtfsRouteSubAgencyTransformation } from "#ie/ropid-gtfs/transformations/RopidGtfsRouteSubAgencyTransformation";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import csv from "csv-parser";
import fs from "fs";
import path from "path";
import sinon, { SinonSandbox } from "sinon";

chai.use(chaiAsPromised);

describe("Route Sub Agencies", () => {
    let sandbox: SinonSandbox;
    let testData: any[];
    let transformation: RopidGtfsRouteSubAgencyTransformation;

    beforeEach(async () => {
        sandbox = sinon.createSandbox();
        testData = await new Promise((resolve, reject) => {
            const results: any[] = [];

            fs.createReadStream(path.join(__dirname, "../../data/ropidgtfs-routeSubAgencies_small.csv"))
                .pipe(csv())
                .on("data", (data) => {
                    results.push(data);
                })
                .on("end", () => {
                    resolve(results);
                });
            transformation = new RopidGtfsRouteSubAgencyTransformation();
        });
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("just transformation", () => {
        const result = transformation.transformArray(testData, true);
        const expected = fs.readFileSync(path.join(__dirname, "../../data/ropidgtfs-routeSubAgencies_transformed.json"), "utf8");
        expect(result).to.deep.equal(JSON.parse(expected));
    });

    it("transformation and duplicates removal", () => {
        const result = transformation.transformArray(testData);
        const expected = fs.readFileSync(path.join(__dirname, "../../data/ropidgtfs-routeSubAgencies_tfinal.json"), "utf8");
        expect(result).to.deep.equal(JSON.parse(expected));
    });
});
