import { PidContainer } from "#ie/ioc/Di";
import { CollectAndSaveLogsTask } from "#ie/ropid-gtfs/workers/presets/tasks/CollectAndSaveLogsTask";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { QueueManager } from "@golemio/core/dist/integration-engine";
import IPostgresConnector from "@golemio/core/dist/integration-engine/connectors/interfaces/IPostgresConnector";
import { expect } from "chai";
import sinon, { SinonStub } from "sinon";
import { InputLogsFixture } from "./fixtures/InputLogsFixture";

describe("RopidPresetWorker - CollectAndSaveLogsTask", () => {
    let task: CollectAndSaveLogsTask;
    let queueStub: SinonStub;

    before(async () => {
        const postgresConnector = PidContainer.resolve<IPostgresConnector>(CoreToken.PostgresConnector);
        await postgresConnector.connect();
    });

    beforeEach(() => {
        queueStub = sinon.stub(QueueManager, "sendMessageToExchange");
        task = new CollectAndSaveLogsTask("test.test");
    });

    afterEach(async () => {
        sinon.restore();
    });

    it("should collect and save logs", async () => {
        const dataSourceStub = sinon.stub(task["dataSourceFactory"], "getDataSource").returns({
            getAll: () => Promise.resolve([InputLogsFixture[0]]),
        } as any);

        const logFilterStub = sinon.stub(task["logFilter"], "getValidLogEntries").returns([InputLogsFixture[0]] as any);

        await task["execute"]({ targetMinutes: 1 });
        expect(dataSourceStub.calledOnce).to.be.true;
        expect(logFilterStub.calledOnce).to.be.true;
        expect(queueStub.calledOnce).to.be.true;

        const result = await task["logRepository"]["sequelizeModel"].findAll({
            where: {
                device_alias: "test-povstani-fhana",
            },
        });

        expect(result).to.have.length(1);
    });

    it("should not collect and save logs if there are no valid logs", async () => {
        const dataSourceStub = sinon.stub(task["dataSourceFactory"], "getDataSource").returns({
            getAll: () => Promise.resolve([]),
        } as any);

        const logFilterStub = sinon.stub(task["logFilter"], "getValidLogEntries").returns([] as any);
        const logRepositoryStub = sinon.stub(task["logRepository"], "save").returns({} as any);

        await task["execute"]({ targetMinutes: 1 });
        expect(dataSourceStub.calledOnce).to.be.true;
        expect(logFilterStub.calledOnce).to.be.true;
        expect(logRepositoryStub.called).to.be.false;
        expect(queueStub.calledOnce).to.be.true;
    });

    it("should not collect and save logs if there is a datasource error", async () => {
        const dataSourceStub = sinon.stub(task["dataSourceFactory"], "getDataSource").returns({
            getAll: () => Promise.reject(new Error("test")),
        } as any);

        const logFilterStub = sinon.stub(task["logFilter"], "getValidLogEntries").returns([] as any);
        const logRepositoryStub = sinon.stub(task["logRepository"], "save").returns({} as any);

        await expect(task["execute"]({ targetMinutes: 1 })).to.be.rejectedWith("Error while fetching preset logs");
        expect(dataSourceStub.calledOnce).to.be.true;
        expect(logFilterStub.called).to.be.false;
        expect(logRepositoryStub.called).to.be.false;
        expect(queueStub.called).to.be.false;
    });
});
