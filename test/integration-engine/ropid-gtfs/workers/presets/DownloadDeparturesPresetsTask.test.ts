import { PidContainer } from "#ie/ioc/Di";
import { RopidGtfsContainer } from "#ie/ropid-gtfs/ioc/Di";
import { RopidGtfsContainerToken } from "#ie/ropid-gtfs/ioc/RopidGtfsContainerToken";
import { DownloadDeparturesPresetsTask } from "#ie/ropid-gtfs/workers/presets/tasks/DownloadDeparturesPresetsTask";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import IPostgresConnector from "@golemio/core/dist/integration-engine/connectors/interfaces/IPostgresConnector";
import sinon, { SinonSandbox, SinonSpy } from "sinon";
import fs from "fs";
import { IRopidDeparturesPresetsOutput } from "#sch/ropid-departures-presets";
import { IDeparturesPresetsData } from "#ie/ropid-gtfs/workers/presets/data-access/DeparturesPresetsDatasource";

describe("DownloadDeparturesPresetsTask", () => {
    let task: DownloadDeparturesPresetsTask;
    let sandbox: SinonSandbox;
    let testDataDeparturesPresets: IDeparturesPresetsData;
    let testTransformedDeparturesPresets: IRopidDeparturesPresetsOutput[];

    before(async () => {
        const postgresConnector = PidContainer.resolve<IPostgresConnector>(CoreToken.PostgresConnector);
        await postgresConnector.connect();
    });

    beforeEach(async () => {
        task = RopidGtfsContainer.resolve(RopidGtfsContainerToken.DownloadDeparturesPresetsTask);
        sandbox = sinon.createSandbox({ useFakeTimers: true });

        testDataDeparturesPresets = JSON.parse(
            fs.readFileSync(__dirname + "/../../data/ropiddeparturespresets-data.json").toString("utf8")
        );
        testTransformedDeparturesPresets = JSON.parse(
            fs.readFileSync(__dirname + "/../../data/ropiddeparturespresets-transformed.json").toString("utf8")
        );

        sandbox.stub(task["departuresPresetsDataSource"], "getAll").resolves(testDataDeparturesPresets);
        sandbox.stub(task["departuresPresetsDataSource"], "getLastModified");

        sandbox
            .stub(task["facade"]["metadataRepository"], "getLastModified")
            .callsFake(() => Object.assign({ lastModified: "2019-01-18T03:24:09.000Z", version: 0 }));
        sandbox.stub(task["facade"]["metadataRepository"], "save");
        sandbox.stub(task["facade"]["metadataRepository"], "checkSavedRows");

        sandbox.stub(task["facade"]["metadataRepository"], "replaceTmpTables");
        sandbox.stub(task["facade"]["metadataRepository"], "rollbackFailedSaving");
        sandbox.stub(task["facade"]["metadataRepository"], "updateState");
        sandbox.stub(task["facade"]["metadataRepository"], "checkIfNewVersionIsAlreadyDeployed");
        sandbox.stub(task["facade"]["metadataRepository"], "checkAllTablesHasSavedState").callsFake(() => Promise.resolve(true));
        sandbox.stub(task["facade"]["transformationDeparturesPresets"], "transform").resolves(testTransformedDeparturesPresets);
        sandbox.stub(task["facade"]["departurePresetsRepository"], "truncate");
        sandbox.stub(task["facade"]["departurePresetsRepository"], "save");
    });
    afterEach(async () => {
        sandbox.restore();
    });

    it("should call the correct methods by checkForNewDeparturesPresets method", async () => {
        await task["execute"]();
        sandbox.assert.calledOnce(task["departuresPresetsDataSource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(task["departuresPresetsDataSource"].getLastModified as SinonSpy);
        sandbox.assert.calledOnce(task["facade"]["metadataRepository"].getLastModified as SinonSpy);
        sandbox.assert.callCount(task["facade"]["metadataRepository"].save as SinonSpy, 3);
        sandbox.assert.calledOnce(task["facade"]["transformationDeparturesPresets"].transform as SinonSpy);
        sandbox.assert.calledOnce(task["facade"]["departurePresetsRepository"].truncate as SinonSpy);
        sandbox.assert.calledOnce(task["facade"]["departurePresetsRepository"].save as SinonSpy);
        sandbox.assert.calledOnce(task["facade"]["metadataRepository"].checkSavedRows as SinonSpy);
        sandbox.assert.calledOnce(task["facade"]["metadataRepository"].replaceTmpTables as SinonSpy);
    });
});
