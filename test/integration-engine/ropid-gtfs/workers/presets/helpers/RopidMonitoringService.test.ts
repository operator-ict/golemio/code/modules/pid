import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { RopidMonitoringService } from "#ie/ropid-gtfs/workers/presets/helpers/RopidMonitoringService";
import { IPresetLogProcessedDto } from "#sch/ropid-departures-preset-logs/models/interfaces/IPresetLogProcessedDto";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc";
import { container } from "@golemio/core/dist/shared/tsyringe";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";

describe("RopidMonitoringService", () => {
    let sandbox: SinonSandbox;

    const createContainer = () =>
        container
            .createChildContainer()
            .register(
                ContainerToken.Config,
                class DummyConfig {
                    public datasources = {
                        RopidMonitoringPushUrl: "https://ropid.example.com",
                        RopidMonitoringPushHeaders: {
                            "Content-Type": "application/kek+json",
                        },
                    };
                }
            )
            .register(ModuleContainerToken.RopidMonitoringService, RopidMonitoringService)
            .resolve<RopidMonitoringService>(ModuleContainerToken.RopidMonitoringService);

    const logDtos: IPresetLogProcessedDto[] = [
        {
            deviceAlias: "test-povstani",
            receivedAt: "2023-06-27T14:39:58Z",
        },
    ];

    before(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        container.clearInstances();
        sandbox.restore();
    });

    describe("sendLogsToMonitoringSystem", () => {
        it("should send logs to monitoring system", async () => {
            const service = createContainer();
            const fetchStub = sandbox.stub().resolves({ status: 200 });
            sandbox.replace(service, "getRequestConfig" as any, sandbox.stub().returns({}));
            sandbox.replace(globalThis, "fetch", fetchStub);

            await expect(service.sendLogsToMonitoringSystem(logDtos)).to.be.fulfilled;
            expect(fetchStub.callCount).to.equal(1);
        });

        it("should throw error when response status is not 2xx", async () => {
            const service = createContainer();
            const fetchStub = sandbox.stub().resolves({ status: 400 });
            sandbox.replace(service, "getRequestConfig" as any, sandbox.stub().returns({}));
            sandbox.replace(globalThis, "fetch", fetchStub);

            await expect(service.sendLogsToMonitoringSystem(logDtos)).to.be.rejectedWith(
                "Error while sending logs to the ROPID monitoring system. Status code: 400"
            );
            expect(fetchStub.callCount).to.equal(1);
        });
    });

    describe("getRequestConfig", () => {
        it("should return request config", () => {
            const service = createContainer();
            const result = service["getRequestConfig"](logDtos);
            expect(result).to.deep.equal({
                method: "POST",
                headers: {
                    "Content-Type": "application/kek+json",
                },
                body: JSON.stringify(logDtos),
                signal: new AbortController().signal,
            });
        });
    });
});
