import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { LogFilter } from "#ie/ropid-gtfs/workers/presets/helpers/LogFilter";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { container } from "@golemio/core/dist/shared/tsyringe";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";
import { InputLogsFixture } from "../fixtures/InputLogsFixture";

describe("LogFilter", () => {
    let sandbox: SinonSandbox;

    const createContainer = () =>
        container
            .createChildContainer()
            .register(
                CoreToken.Logger,
                class DummyLogger {
                    public debug = sandbox.stub();
                    public warn = sandbox.stub();
                }
            )
            .register(ModuleContainerToken.PresetLogFilter, LogFilter)
            .resolve<LogFilter>(ModuleContainerToken.PresetLogFilter);

    before(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        container.clearInstances();
        sandbox.restore();
    });

    describe("getValidLogEntries", () => {
        it("should return valid log entries", () => {
            const inputData = [
                InputLogsFixture[0],
                { stream: InputLogsFixture[0].stream },
                { values: InputLogsFixture[0].values },
                {},
                InputLogsFixture[1],
            ];

            const logFilter = createContainer();
            const result = logFilter.getValidLogEntries(inputData as any);
            expect(result).to.deep.equal([InputLogsFixture[0], InputLogsFixture[1]]);
        });
    });
});
