import { LogDateTimeUtils } from "#ie/ropid-gtfs/workers/presets/helpers/LogDateTimeUtils";
import { expect } from "chai";

describe("LogDateTimeUtils", () => {
    describe("parseISONoFractionalFromDate", () => {
        it("should parse ISO date", () => {
            const result = LogDateTimeUtils.parseISONoFractionalFromDate(new Date("2023-06-27T14:39:58.696+02:00"));
            expect(result).to.equal("2023-06-27T12:39:58Z");
        });
    });
});
