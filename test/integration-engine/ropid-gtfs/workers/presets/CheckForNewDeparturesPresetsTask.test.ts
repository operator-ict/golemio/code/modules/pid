import { PidContainer } from "#ie/ioc/Di";
import { RopidGtfsContainer } from "#ie/ropid-gtfs/ioc/Di";
import { RopidGtfsContainerToken } from "#ie/ropid-gtfs/ioc/RopidGtfsContainerToken";
import { CheckForNewDeparturesPresetsTask } from "#ie/ropid-gtfs/workers/presets/tasks/CheckForNewDeparturesPresetsTask";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { QueueManager } from "@golemio/core/dist/integration-engine";
import IPostgresConnector from "@golemio/core/dist/integration-engine/connectors/interfaces/IPostgresConnector";
import sinon, { SinonSandbox, SinonSpy } from "sinon";

describe("CheckForNewDeparturesPresetsTask", () => {
    let task: CheckForNewDeparturesPresetsTask;
    let sandbox: SinonSandbox;

    before(async () => {
        const postgresConnector = PidContainer.resolve<IPostgresConnector>(CoreToken.PostgresConnector);
        await postgresConnector.connect();
    });

    beforeEach(async () => {
        task = RopidGtfsContainer.resolve(RopidGtfsContainerToken.CheckForNewDeparturesPresetsTask);
        sandbox = sinon.createSandbox({ useFakeTimers: true });

        sandbox.stub(task["departuresPresetsDataSource"], "getLastModified");
        sandbox
            .stub(task["metadataRepository"], "getLastModified")
            .callsFake(() => Object.assign({ lastModified: "2019-01-18T03:24:09.000Z", version: 0 }));
        sandbox.stub(QueueManager, "sendMessageToExchange");
    });

    afterEach(async () => {
        sandbox.restore();
    });

    it("should call the correct methods by checkForNewDeparturesPresets method", async () => {
        await task["execute"]();
        sandbox.assert.calledOnce(task["departuresPresetsDataSource"].getLastModified as SinonSpy);
        sandbox.assert.calledOnce(task["metadataRepository"].getLastModified as SinonSpy);
        sandbox.assert.calledOnce(QueueManager.sendMessageToExchange as SinonSpy);
    });
});
