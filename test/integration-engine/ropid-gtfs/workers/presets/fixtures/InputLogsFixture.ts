export const InputLogsFixture = [
    {
        stream: {
            container_name: "url-proxy",
            instance: "url-proxy-7b94d89848",
            job: "loki.source.kubernetes.pods",
            level: "info",
            message: "request completed",
            req_httpVersion: "1.1",
            req_method: "GET",
            req_remoteAddress: "::ffff:10.141.9.119",
            req_url: "/pid/test-povstani-fhana?fhana",
            req_userAgent: "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:109.0) Gecko/20100101 Firefox/114.0",
            res_status: "200",
            responseTime: "41",
        },
        values: [
            [
                "1687876798696442813",
                // eslint-disable-next-line max-len
                '{"level":"info","req":{"httpVersion":"1.1","method":"GET","url":"/pid/test-povstani-fhana?fhana","remoteAddress":"::ffff:10.141.9.119","userAgent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:109.0) Gecko/20100101 Firefox/114.0"},"res":{"status":200,"userId":null},"responseTime":41,"message":"request completed"}\n',
            ],
        ],
    },
    {
        stream: {
            container_name: "url-proxy",
            instance: "url-proxy-7b94d89848",
            job: "loki.source.kubernetes.pods",
            level: "info",
            message: "request completed",
            req_httpVersion: "1.1",
            req_method: "GET",
            req_remoteAddress: "::ffff:10.141.9.119",
            req_url: "/pid/test-povstani",
            req_userAgent: "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:109.0) Gecko/20100101 Firefox/114.0",
            res_status: "200",
            responseTime: "530",
        },
        values: [
            [
                "1687876795160218036",
                // eslint-disable-next-line max-len
                '{"level":"info","req":{"httpVersion":"1.1","method":"GET","url":"/pid/test-povstani","remoteAddress":"::ffff:10.141.9.119","userAgent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:109.0) Gecko/20100101 Firefox/114.0"},"res":{"status":200,"userId":null},"responseTime":530,"message":"request completed"}\n',
            ],
        ],
    },
    {
        stream: {
            container_name: "url-proxy",
            instance: "url-proxy-7b94d89848",
            job: "loki.source.kubernetes.pods",
            level: "info",
            message: "request completed",
            req_httpVersion: "1.1",
            req_method: "GET",
            req_remoteAddress: "::ffff:10.141.9.119",
            req_url: "/pid/bustec-testovaci/",
            req_userAgent: "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:109.0) Gecko/20100101 Firefox/114.0",
            res_status: "200",
            responseTime: "44",
        },
        values: [
            [
                "1687876806731340735",
                // eslint-disable-next-line max-len
                '{"level":"info","req":{"httpVersion":"1.1","method":"GET","url":"/pid/bustec-testovaci/","remoteAddress":"::ffff:10.141.9.119","userAgent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:109.0) Gecko/20100101 Firefox/114.0"},"res":{"status":200,"userId":null},"responseTime":44,"message":"request completed"}\n',
            ],
        ],
    },
    {
        stream: {
            container_name: "url-proxy",
            instance: "url-proxy-7b94d89848",
            job: "loki.source.kubernetes.pods",
            level: "info",
            message: "request completed",
            req_httpVersion: "1.1",
            req_method: "GET",
            req_remoteAddress: "::ffff:10.141.9.119",
            req_url: "/pid/bustec-testovaci",
            req_userAgent: "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:109.0) Gecko/20100101 Firefox/114.0",
            res_status: "200",
            responseTime: "1614",
        },
        values: [
            [
                "1687876804396421305",
                // eslint-disable-next-line max-len
                '{"level":"info","req":{"httpVersion":"1.1","method":"GET","url":"/pid/bustec-testovaci","remoteAddress":"::ffff:10.141.9.119","userAgent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:109.0) Gecko/20100101 Firefox/114.0"},"res":{"status":200,"userId":null},"responseTime":1614,"message":"request completed"}\n',
            ],
        ],
    },
];
