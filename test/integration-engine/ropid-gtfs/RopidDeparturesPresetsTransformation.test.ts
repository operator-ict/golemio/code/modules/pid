import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import { RopidDeparturesPresetsTransformation } from "#ie/ropid-gtfs";
import { IRopidDeparturesPresetsOutput } from "#sch/ropid-departures-presets";
import { IDeparturesPresetsData } from "#ie/ropid-gtfs/workers/presets/data-access/DeparturesPresetsDatasource";

chai.use(chaiAsPromised);

describe("RopidDeparturesPresetsTransformation", () => {
    let transformation: RopidDeparturesPresetsTransformation;
    let testSourceData: IDeparturesPresetsData;
    let testTransformedData: IRopidDeparturesPresetsOutput[];

    beforeEach(() => {
        transformation = new RopidDeparturesPresetsTransformation();
        testSourceData = JSON.parse(fs.readFileSync(__dirname + "/data/ropiddeparturespresets-data.json").toString("utf8"));
        testTransformedData = JSON.parse(
            fs.readFileSync(__dirname + "/data/ropiddeparturespresets-transformed.json").toString("utf8")
        );
    });

    it("should have name", () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("RopidDeparturesPresets");
    });

    it("should properly transform data", async () => {
        const data = await transformation.transform(testSourceData.data);
        expect(data).to.deep.equal(testTransformedData);
    });
});
