import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { promises as fs } from "fs";
import {
    ICisStopsTransformationData,
    ICisStopsTransformedData,
    RopidGTFSCisStopsTransformation,
} from "#ie/ropid-gtfs/transformations/RopidGTFSCisStopsTransformation";

chai.use(chaiAsPromised);

describe("RopidGTFSCisStopsTransformation", () => {
    let transformation: RopidGTFSCisStopsTransformation;
    let testSourceData: ICisStopsTransformationData;
    let testTransformedData: ICisStopsTransformedData;

    beforeEach(async () => {
        transformation = new RopidGTFSCisStopsTransformation();
        testSourceData = {
            name: "Stops.Min",
            data: JSON.parse(await fs.readFile(__dirname + "/./data/ropidgtfsstops-datasource.json", "utf8")),
        };
        testTransformedData = require("./data/ropidgtfsstops-transformed").default;
    });

    it("should has name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("RopidGTFSCisStops");
    });

    it("should has transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should properly transform element", async () => {
        const data = await transformation.transform(testSourceData);
        expect(data).to.have.property("cis_stop_groups");
        expect(data).to.have.property("cis_stops");
        expect(data).to.deep.equal(testTransformedData);
    });
});
