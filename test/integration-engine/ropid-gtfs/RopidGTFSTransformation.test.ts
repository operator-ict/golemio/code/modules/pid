import { IRopidGtfsTransformationData, RopidGTFSTransformation } from "#ie/ropid-gtfs/transformations/RopidGTFSTransformation";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { createReadStream } from "fs";
import { Readable } from "stream";

chai.use(chaiAsPromised);

describe("RopidGTFSTransformation", () => {
    let transformation: RopidGTFSTransformation;
    let testSourceData: IRopidGtfsTransformationData;

    beforeEach(async () => {
        transformation = new RopidGTFSTransformation();
        testSourceData = {
            name: "calendar",
            sourceStream: createReadStream(__dirname + "/./data/ropidgtfs-data.csv", "utf8"),
        };
    });

    it("should has name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("RopidGTFS");
    });

    it("should has transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should properly transform element", async () => {
        const data = await transformation.transform(testSourceData);
        expect(data).instanceof(Readable);
    });
});
