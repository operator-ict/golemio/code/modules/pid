import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { ITripStopsResult, RopidGTFSStopTimesModel } from "#ie/ropid-gtfs/RopidGTFSStopTimesModel";

chai.use(chaiAsPromised);

describe("RopidGTFSStopTimesModel", () => {
    let model: RopidGTFSStopTimesModel;
    let tripStopsTestData: ITripStopsResult[];

    before(async () => {
        await PostgresConnector.connect();
        model = new RopidGTFSStopTimesModel();
        tripStopsTestData = JSON.parse(
            fs.readFileSync(__dirname + "/../data/tripStopsBlockId_1320_9472_211213_schedule.json").toString("utf8")
        );
    });

    it("should return rows (trip_id 115_6_201230)", async () => {
        const result = await model.findAndCountAll({
            where: {
                trip_id: "115_6_201230",
            },
        });

        expect(result.count).to.equal(8);
    });

    it("findTripStops should return data (trip_id [1302_9472_211213, 1309_9472_211213])", async () => {
        const result = await model.findTripStops(["1302_9472_211213", "1309_9472_211213"]);
        expect(result).to.deep.equal(result);
    });
});
