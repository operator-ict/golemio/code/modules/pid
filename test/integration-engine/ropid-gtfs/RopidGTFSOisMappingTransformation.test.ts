import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { promises as fs } from "fs";
import {
    IOisMappingTransformationData,
    RopidGTFSOisMappingTransformation,
} from "#ie/ropid-gtfs/transformations/RopidGTFSOisTransformation";
import { config } from "@golemio/core/dist/integration-engine/config";
import sinon from "sinon";
import { IRopidGTFSOisMappingData } from "#sch/ropid-gtfs/RopidGTFSOisMapping";

chai.use(chaiAsPromised);

describe("RopidGTFSOisMappingTransformation", () => {
    let transformation: RopidGTFSOisMappingTransformation;
    let testSourceData: IOisMappingTransformationData;
    let testTransformedData: IRopidGTFSOisMappingData[];

    beforeEach(async () => {
        transformation = new RopidGTFSOisMappingTransformation();
        testSourceData = {
            name: "oisMapping",
            data: JSON.parse(await fs.readFile(__dirname + "/./data/ropidgtfs_ois-datasource.json", "utf8")),
        };
        testTransformedData = require("./data/ropidgtfs_ois-transformed").default;
    });

    afterEach(() => {
        sinon.restore();
    });

    it("should have name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("RopidGTFSOisMapping");
    });

    it("should have transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should properly transform data", async () => {
        sinon.stub(config, "datasources").value({
            RopidGTFSRunNumbersFilename: "obehy.csv",
            RopidGTFSOisFilename: "oisMapping.json",
            RopidGTFSCisStopsFilename: "Stops.Min.json",
        });

        const data = await transformation.transform(testSourceData);
        expect(data).to.deep.equal(testTransformedData);
    });
});
