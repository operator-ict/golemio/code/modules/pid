export default [
    {
        ois: 5067,
        node: 67,
        name: "Čechovo náměstí",
    },
    {
        ois: 5068,
        node: 68,
        name: "Čechův most",
    },
    {
        ois: 5070,
        node: 70,
        name: "Nové Strašnice",
    },
    {
        ois: 942,
        node: 81,
        name: "Tusarova",
    },
    {
        ois: 5115,
        node: 115,
        name: "Nádraží Holešovice",
    },
];
