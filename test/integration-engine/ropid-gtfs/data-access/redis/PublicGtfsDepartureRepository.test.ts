import { PublicGtfsDepartureRepository } from "#ie/ropid-gtfs/data-access/cache/PublicGtfsDepartureRepository";
import { RopidGtfsContainerToken } from "#ie/ropid-gtfs/ioc/RopidGtfsContainerToken";
import { IPublicGtfsDepartureCacheDto } from "#sch/ropid-gtfs/redis/interfaces/IPublicGtfsDepartureCacheDto";
import { IoRedisConnector } from "@golemio/core/dist/helpers/data-access/redis/IoRedisConnector";
import { ContainerToken, IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";

describe("PublicGtfsDepartureRepository", () => {
    let sandbox: SinonSandbox;
    let container: DependencyContainer;

    before(async () => {
        const redisConnector = IntegrationEngineContainer.resolve<IoRedisConnector>(ContainerToken.RedisConnector);
        await redisConnector.connect();
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        container = IntegrationEngineContainer.createChildContainer();

        container.registerSingleton(RopidGtfsContainerToken.PublicGtfsDepartureRepository, PublicGtfsDepartureRepository);
    });

    afterEach(() => {
        sandbox?.restore();
    });

    it("should replace departures for stop", async () => {
        const clock = sandbox.useFakeTimers(new Date("2024-04-24T22:50:00+02:00").getTime());

        const repository = container.resolve<PublicGtfsDepartureRepository>(
            RopidGtfsContainerToken.PublicGtfsDepartureRepository
        );

        const stub = {
            zremrangebyscore: sandbox.stub().resolves(),
            zadd: sandbox.stub().resolves(),
            expire: sandbox.stub().resolves(),
            exec: sandbox.stub().resolves(),
            zrem: sandbox.stub().resolves(),
        };

        const originalPipeline = repository["connection"].pipeline;
        repository["connection"].pipeline = sandbox.stub().returns({
            zremrangebyscore: stub.zremrangebyscore,
            zadd: stub.zadd,
            expire: stub.expire,
            exec: stub.exec,
            zrem: stub.zrem,
        });

        const departures: IPublicGtfsDepartureCacheDto[] = [
            {
                stop_id: "U717Z6P",
                departure_datetime: "2024-04-24T20:50:00.000Z",
                arrival_datetime: null,
                route_short_name: "X1",
                route_type: 3,
                trip_id: "801_2228_240417",
                stop_sequence: 3,
                platform_code: "F",
                trip_headsign: "Hradčanská",
            },
            {
                stop_id: "U717Z5P",
                departure_datetime: "2024-04-24T20:52:00.000Z",
                arrival_datetime: "2024-04-24T20:52:00.000Z",
                route_short_name: "27",
                route_type: 0,
                trip_id: "27_3074_240217",
                stop_sequence: 4,
                platform_code: "E",
                trip_headsign: "Libuš",
            },
        ];

        await repository.replaceDeparturesForStop([departures[0]], "U717Z6P", {
            intervalFromHours: -1,
            intervalToHours: 2,
        });

        await repository.replaceDeparturesForStop([departures[1]], "U717Z5P", {
            intervalFromHours: -1,
            intervalToHours: 2,
        });

        clock.restore();
        repository["connection"].pipeline = originalPipeline;

        // zremrangebyscore
        expect(stub.zremrangebyscore.callCount).to.equal(4);
        expect(stub.zremrangebyscore.firstCall.args).to.deep.equal(["gtfsPublicDepartureCache:U717Z6P", "-inf", 1713981000]);
        expect(stub.zremrangebyscore.secondCall.args).to.deep.equal(["gtfsPublicDepartureCache:U717Z6P", 1713991800, 1713991800]);
        expect(stub.zremrangebyscore.thirdCall.args).to.deep.equal(["gtfsPublicDepartureCache:U717Z5P", "-inf", 1713981000]);
        expect(stub.zremrangebyscore.lastCall.args).to.deep.equal(["gtfsPublicDepartureCache:U717Z5P", 1713991920, 1713991920]);

        // zadd
        expect(stub.zadd.callCount).to.equal(2);
        expect(stub.zadd.firstCall.args).to.deep.equal([
            "gtfsPublicDepartureCache:U717Z6P",
            1713991800,
            JSON.stringify(departures[0]),
        ]);
        expect(stub.zadd.lastCall.args).to.deep.equal([
            "gtfsPublicDepartureCache:U717Z5P",
            1713991920,
            JSON.stringify(departures[1]),
        ]);

        // expire
        expect(stub.expire.callCount).to.equal(2);
        expect(stub.expire.firstCall.args).to.deep.equal(["gtfsPublicDepartureCache:U717Z6P", 7200]);
        expect(stub.expire.lastCall.args).to.deep.equal(["gtfsPublicDepartureCache:U717Z5P", 7200]);

        // exec
        expect(stub.exec.callCount).to.equal(2);
    });

    it("should throw due to validation error", async () => {
        const repository = container.resolve<PublicGtfsDepartureRepository>(
            RopidGtfsContainerToken.PublicGtfsDepartureRepository
        );

        const departures = [
            {
                stop_id: "U717Z6P",
                departure_datetime: null,
                arrival_datetime: null,
                route_short_name: "X1",
                route_type: 3,
                trip_id: "801_2228_240417",
                stop_sequence: 3,
                platform_code: "F",
                trip_headsign: "Hradčanská",
            },
        ];

        try {
            await repository.replaceDeparturesForStop(departures as any, "U717Z6P", {
                intervalFromHours: -1,
                intervalToHours: 2,
            });

            expect.fail("Should throw validation error");
        } catch (err) {
            expect(err.message).to.equal("Error while validating departures");
        }
    });

    it("should throw due to error while saving departures", async () => {
        const repository = container.resolve<PublicGtfsDepartureRepository>(
            RopidGtfsContainerToken.PublicGtfsDepartureRepository
        );

        const departures: IPublicGtfsDepartureCacheDto[] = [
            {
                stop_id: "U717Z6P",
                departure_datetime: "2024-04-24T20:50:00.000Z",
                arrival_datetime: null,
                route_short_name: "X1",
                route_type: 3,
                trip_id: "801_2228_240417",
                stop_sequence: 3,
                platform_code: "F",
                trip_headsign: "Hradčanská",
            },
        ];

        const stub = {
            zremrangebyscore: sandbox.stub().rejects(new Error("Failed to remove departures")),
            zadd: sandbox.stub().rejects(new Error("Failed to add departures")),
            expire: sandbox.stub().rejects(new Error("Failed to expire key")),
            exec: sandbox.stub().rejects(new Error("Failed to execute pipeline")),
        };

        const originalPipeline = repository["connection"].pipeline;
        repository["connection"].pipeline = sandbox.stub().returns({
            zremrangebyscore: stub.zremrangebyscore,
            zadd: stub.zadd,
            expire: stub.expire,
            exec: stub.exec,
        });

        try {
            await repository.replaceDeparturesForStop(departures, "U717Z6P", {
                intervalFromHours: -1,
                intervalToHours: 2,
            });

            expect.fail("Should throw an error");
        } catch (err) {
            expect(err.message).to.equal("PublicGtfsDepartureRepository: error while saving departures");
        } finally {
            repository["connection"].pipeline = originalPipeline;
        }
    });
});
