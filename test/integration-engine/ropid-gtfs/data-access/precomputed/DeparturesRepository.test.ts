import { DeparturesRepository } from "#ie/ropid-gtfs/data-access/precomputed/DeparturesRepository";
import { SourceTableSuffixEnum } from "#ie/ropid-gtfs/helpers/SourceTableSuffixEnum";
import { RopidGtfsContainerToken } from "#ie/ropid-gtfs/ioc/RopidGtfsContainerToken";
import { DeparturesModel } from "#sch/ropid-gtfs/models/precomputed";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { DependencyContainer, container } from "@golemio/core/dist/shared/tsyringe";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";

chai.use(chaiAsPromised);

describe("DeparturesRepository", () => {
    let sandbox: SinonSandbox;
    let testContainer: DependencyContainer;

    before(async () => {
        sandbox = sinon.createSandbox();
        testContainer = container.createChildContainer();
        testContainer.registerSingleton(CoreToken.Logger, class DummyLogger {});
        testContainer.registerSingleton(
            CoreToken.SimpleConfig,
            class DummyConfig {
                getValue = () => "development";
            }
        );
        testContainer.register(RopidGtfsContainerToken.DepartureRepository, DeparturesRepository);

        await PostgresConnector.connect();
    });

    afterEach(() => {
        sandbox?.restore();
    });

    it("should return data (trip_id 91_158_211029)", async () => {
        const repository = testContainer.resolve<DeparturesRepository>(RopidGtfsContainerToken.DepartureRepository);
        const result = await repository["sequelizeModel"].findAll<DeparturesModel>({
            where: { trip_id: "91_158_211029" },
            raw: true,
        });

        for (const departure of result) {
            expect(departure.trip_headsign).to.equal("Staré Strašnice");
        }
    });

    it("createAndPopulate should generate and execute query", async () => {
        const repository = testContainer.resolve<DeparturesRepository>(RopidGtfsContainerToken.DepartureRepository);
        const queryStub = sandbox.stub(repository["sequelizeModel"].sequelize, "query" as never);

        await repository.createAndPopulate(SourceTableSuffixEnum.Tmp);
        expect(queryStub.getCall(0).args[0]).to.equal(
            /* eslint-disable max-len */
            `
                SET LOCAL search_path TO pid;
                DROP TABLE IF EXISTS ropidgtfs_precomputed_departures_tmp;
                CREATE TABLE ropidgtfs_precomputed_departures_tmp (LIKE ropidgtfs_precomputed_departures including all);
                INSERT INTO ropidgtfs_precomputed_departures_tmp \n            SELECT
                t.stop_sequence,
                t.stop_headsign,
                t.pickup_type,
                t.drop_off_type,
                t.arrival_time,
                gtfs_timestamp(t.arrival_time, t4.date) AS arrival_datetime,
                t.departure_time,
                gtfs_timestamp(t.departure_time, t4.date) AS departure_datetime,
                t0.stop_id,
                t0.stop_name,
                t0.platform_code,
                t0.wheelchair_boarding,
                t1.min_stop_sequence,
                t1.max_stop_sequence,
                t2.trip_id,
                t2.trip_headsign,
                t2.trip_short_name,
                t2.wheelchair_accessible,
                t3.service_id,
                t4.date,
                t5.route_short_name,
                coalesce(t5.route_type, :defaultRouteType) AS route_type,
                t5.route_id,
                t5.is_night,
                t5.is_regional,
                t5.is_substitute_transport,
                t6.stop_sequence AS next_stop_sequence,
                t6.stop_id AS next_stop_id,
                t7.stop_sequence AS last_stop_sequence,
                t7.stop_id AS last_stop_id,
                cis_stop.cis AS cis_stop_group_id
            FROM ropidgtfs_stop_times_tmp t
                LEFT JOIN ropidgtfs_stops_tmp t0 ON t.stop_id = t0.stop_id
                LEFT JOIN ropidgtfs_trips_tmp t2 ON t.trip_id = t2.trip_id
                INNER JOIN ropidgtfs_precomputed_services_calendar_tmp t4 ON t2.service_id = t4.service_id
                INNER JOIN ropidgtfs_precomputed_minmax_stop_sequences_tmp t1 ON t.trip_id = t1.trip_id
                LEFT JOIN ropidgtfs_calendar_tmp t3 ON t2.service_id = t3.service_id
                LEFT JOIN ropidgtfs_routes_tmp t5 ON t2.route_id = t5.route_id
                LEFT JOIN ropidgtfs_stop_times_tmp t6 ON t.trip_id = t6.trip_id AND t6.stop_sequence = t.stop_sequence + 1
                LEFT JOIN ropidgtfs_stop_times_tmp t7 ON t.trip_id = t7.trip_id AND t7.stop_sequence = t.stop_sequence - 1
                LEFT JOIN ropidgtfs_cis_stops cis_stop on cis_stop.id = t0.computed_cis_stop_id
            WHERE gtfs_timestamp(t.arrival_time, t4.date) > now() - interval '6 hours';`
            /* eslint-enable max-len */
        );
    });

    it("countDeparturesForPublicCache should return count", async () => {
        const repository = testContainer.resolve<DeparturesRepository>(RopidGtfsContainerToken.DepartureRepository);
        const count = await repository.countDeparturesForPublicCache({
            intervalFromHours: -12,
            intervalToHours: 12,
        });

        expect(count).to.be.greaterThan(1000);
    });

    it("countDeparturesForPublicCache should throw error", async () => {
        const repository = testContainer.resolve<DeparturesRepository>(RopidGtfsContainerToken.DepartureRepository);
        await expect(repository.countDeparturesForPublicCache({} as any)).to.be.rejectedWith(
            "Error while getting number of departures"
        );
    });

    it("getDepaturesForPublicCache should return data", async () => {
        const repository = testContainer.resolve<DeparturesRepository>(RopidGtfsContainerToken.DepartureRepository);
        const result = await repository.getDepaturesForPublicCache(0, 100, {
            intervalFromHours: -12,
            intervalToHours: 12,
        });

        expect(result.length).to.equal(100);
    });

    it("getDepaturesForPublicCache should throw error", async () => {
        const repository = testContainer.resolve<DeparturesRepository>(RopidGtfsContainerToken.DepartureRepository);
        await expect(repository.getDepaturesForPublicCache(0, 100, {} as any)).to.be.rejectedWith(
            "Error while getting departures for public cache"
        );
    });
});
