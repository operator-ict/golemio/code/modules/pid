import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { TripScheduleRepository } from "#ie/ropid-gtfs/data-access/precomputed/TripScheduleRepository";
chai.use(chaiAsPromised);

describe("TripScheduleRepository", () => {
    let repository: TripScheduleRepository;

    before(async () => {
        await PostgresConnector.connect();
        repository = new TripScheduleRepository();
    });

    it("should return a row (trip_id 991_1_210419)", async () => {
        const result = await repository.findOne({
            where: {
                trip_id: "991_1_210419",
            },
        });

        expect(result).to.contain({
            service_id: "1111100-1",
            run_number: 1,
        });
    });
});
