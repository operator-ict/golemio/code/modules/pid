import { expect } from "chai";
import { HTMLUtils } from "#ie/shared/HTMLUtils";

describe("HTMLUtils", () => {
    it("outputPlainText should return null", () => {
        expect(HTMLUtils.outputPlainText(undefined)).to.eq(null);
    });

    it("outputPlainText should return plain text (fragment)", () => {
        const html = `<p>test&amp;test</p>`;
        expect(HTMLUtils.outputPlainText(html)).to.eq("test&test");
    });

    it("outputPlainText should return plain text (document)", () => {
        const html =
            // eslint-disable-next-line max-len
            '<html><body style="font-family: Tahoma"><h2>Opatřen&iacute; pro linku</h2> <p>Linka je vedena v tras&aacute;ch</p></body></html>';
        expect(HTMLUtils.outputPlainText(html)).to.eq("Opatření pro linku Linka je vedena v trasách");
    });
});
