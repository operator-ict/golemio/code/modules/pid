import { RopidMetadataModel } from "#ie/shared/RopidMetadataModel";
import { PG_SCHEMA } from "#sch/const";
import { RopidVYMI } from "#sch/ropid-vymi";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";

chai.use(chaiAsPromised);

describe("RopidMetadataModel", () => {
    let model: RopidMetadataModel;
    let sandbox: SinonSandbox;

    before(async () => {
        await PostgresConnector.connect();
        model = new RopidMetadataModel({
            name: "TestModel",
            // TODO dedicate custom schema definitions for testing purposes (we shouldn't be referencing RopidVYMI here)
            pgSchema: PG_SCHEMA,
            metadata: RopidVYMI.metadata,
            events: RopidVYMI.events,
        });
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("replaceTmpTables should swap vymi events with tmp events", async () => {
        sandbox.stub(model["sequelizeModel"], "findAll").resolves([{ dataValues: { tn: "events" } }] as any);
        sandbox.stub(model["sequelizeModel"], "destroy");
        sandbox.stub(model["sequelizeModel"], "create");

        const connection = PostgresConnector.getConnection();
        const tableName = `"${PG_SCHEMA}"."${RopidVYMI.events.pgTableName}"`;
        const oldEntry = await connection.query(`SELECT * FROM ${tableName} WHERE vymi_id = 9292`, {
            raw: true,
            plain: true,
        });

        expect(oldEntry).to.equal(null);
        await model.replaceTmpTables("dummy", 1);
        const newEntry = await connection.query(`SELECT * FROM ${tableName} WHERE vymi_id = 9292`, {
            raw: true,
            plain: true,
        });

        expect(newEntry).to.not.equal(null);
    });
});
