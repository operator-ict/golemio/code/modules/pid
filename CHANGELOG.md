# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [3.9.2] - 2025-03-03

### Changed

-   add integer max/min value validation for `/v2/pid/depatureboards`, `/v3/pid/transferboards` endpoints for `cisIds` and `aswIds` parameters ([pid#459](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/459))

## [3.9.1] - 2025-02-24

### Added

-   Filter that suppresses irrelevant infotexts from being displayed on JIS infotext overview, departure boards and transfer boards ([vymi-alerts#121](https://gitlab.com/operator-ict/golemio/jis/vymi-alerts/vymi-alerts-general/-/issues/121))

### Changed

-  `qs` lib is imported from core
-   Move all infotext retrieval, mapping and filtering logic to the InfotextFacade ([vymi-alerts#121](https://gitlab.com/operator-ict/golemio/jis/vymi-alerts/vymi-alerts-general/-/issues/121))

## [3.9.0] - 2025-02-10

### Changed

-   Refactoring mapping functions for MPVNet ([pid#423](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/423))

### Removed

-   `static/metro_railtrack_data.csv` file which is no longer used

## [3.8.0] - 2025-02-06

### Changed

- Log level to warning when MPVnet message is not mapped to gtfs trip correctly

### Added

-   Propagate position id as `origin_postion_id` when delay is propagated ([pid#447](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/447))

## [3.7.0] - 2025-01-29

### Fixed

-   Fix Postgres CPU overload by adding indexes and removing redundant computed column ([pid#177](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/177))

## [3.6.0] - 2025-01-27

### Added

-   Departure Boards - `skip[]=missing` parameter for filtering departures with missing vehicle info([pid#177](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/177))

### Removed

-   `access-control-expose-headers: date` header from /v2/pid/depatureboards ([core#123](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/123))

## [3.5.2] - 2025-01-22

### Fixed

-   MPVNet message handling where invalid spoj `attributes` no longer cause entire message rejection, allowing valid trips within the same message to process normally

## [3.5.1] - 2025-01-22

### Changed

-   Transfer Boards - improve mechanism for matching `tripNumber` to GTFS trip short name ([pid#444](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/444))
-   Transfer Boards - `tripNumber` is now an optional parameter ([pid#444](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/444))

## [3.5.0] - 2025-01-20

### Changed

-   Change TS build target from ES2015 to ES2021 ([core#121](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/121))

## [3.4.0] - 2025-01-13

### Added

-   Add push ep for infotexts ([pid#441](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/441))

## [3.3.0] - 2024-12-18

### Added

- `updated_timestamp` for infotexts ([vymi-general#72](https://gitlab.com/operator-ict/golemio/jis/vymi-alerts/vymi-alerts-general/-/issues/72))

### Changed

- updating infotext only if incoming `updated_timestamp` is bigger or equal then current `updated_at` ([vymi-general#72](https://gitlab.com/operator-ict/golemio/jis/vymi-alerts/vymi-alerts-general/-/issues/72))

## [3.2.2] - 2024-12-04

### Fixed

-   Include canceled trips in trip_updates ([pid#431](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/431))
-   Include canceled trips in public api ([pid#431](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/431))
-   Public VP - validation of the `boundingBox` query parameter to handle all valid coordinate combinations according to [OSM specification](https://wiki.openstreetmap.org/wiki/Bounding_box), resolving incorrect 400 errors ([pid#440](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/440))

## [3.2.1] - 2024-11-27

### Fixed

-   Replace truncate with destroy ([pid#445](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/445))

## [3.2.0] - 2024-11-25

### Changed

-   Public EPs are no longer experimental and now require API key authentication ([pid#415](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/415))

### Security

-   Added API key requirement for all public EPs to ensure proper access control and usage monitoring ([pid#415](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/415))

## [3.1.0] - 2024-11-20

### Changed

-   Refresh VYMI (JIS) Infotexts only conditionally (when the data source has been updated) based on resource ETag ([vymi-alerts#61](https://gitlab.com/operator-ict/golemio/jis/vymi-alerts/vymi-alerts-general/-/issues/61))

## [3.0.0] - 2024-11-13

### Removed

-   EP `/v2/departureboards` that was deprecated several years ago and is no longer maintained ([pid#428](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/428))

### Fixed

-   Fatal error when generating GTFS-RT
-   Unwanted side effects (such as empty precomputed tables) when importing GTFS Static data

## [2.24.1] - 2024-11-11

### Changed

-   Delete all infotexts when imported data are empty ([pid#439](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/439))
-   Do not import infotexts without stops ([pid#439](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/439))

## [2.24.0] - 2024-11-06

### Added

-   EP `/v3/pid/transferboards` for showing transfer departures in vehicles ([pid#424](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/424))

### Changed

-   Determine departure timestamp intervals using native Date instead of Moment.js ([pid#424](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/424))

## [2.23.2] - 2024-11-04

### Fixed

-   Database structure incompatible with PostgreSQL 16 ([pid#437](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/437))

## [2.23.1] - 2024-10-23

### Fixed

-   wrong predictions in /public/vehiclepositions EP ([pid#405](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/405))
-   Overnight stop times in Public VP during DST changes ([pid#356](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/356))
-   Initial stop times of before track trips in VP during DST changes ([pid#359](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/359))

## [2.23.0] - 2024-10-14

### Added

-   New state position `after_track_delayed` - the same as `after_track`, but the position always has defined delay (not null) if possible to deduct from GTFS static data and origin timestamp ([pid#322](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/322))
-   Propagate azimuth to the next trip ([pid#315](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/315))


### Changed

-   Detect backtracking vehicles and invalidate their older positions/past stop times ([pid#382](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/382))
-   Propagate delay from `after_track_delayed` positions to the next trip's `before_track_delayed` positions ([pid#322](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/322))
-   `computePositions` should always run synchronously/never return a promise ([pid#322](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/322))
-   `after_track` positions should always have a delay of null ([pid#322](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/322))

### Fixed

-   `after_track` positions could return data about the next stop in some instances (TCP DPP and Telmax Arriva City trips). At this point, the next stop of the vehicle should always be null ([pid#322](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/322))

## [2.22.0] - 2024-09-30

### Added

-   Cloudflare GTFS-RT Protobuffer cache purge ([pid#417](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/417))

### Changed

-   Turn on GTFS-RT Protobuffer caching ([pid#417](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/417))

## [2.21.0] - 2024-09-18

### Added

-   Description of `state_position` values to OpenAPI documentation ([pid#400](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/400))
-   Import vehicles from data-source `SeznamAutobusu` by status ([pid#401](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/401))

### Changed

-   `.gitlab-ci.yml` cleanup ([devops#320](https://gitlab.com/operator-ict/golemio/devops/infrastructure/-/issues/320))

## [2.20.0] - 2024-09-09

### Added

-   Add `priority` to the /v3/pid/infotexts EP ([vymi-alerts#48](https://gitlab.com/operator-ict/golemio/jis/vymi-alerts/vymi-alerts-general/-/issues/48))

### Changed

-   Enumerable `severity_level` ([vymi-alerts#48](https://gitlab.com/operator-ict/golemio/jis/vymi-alerts/vymi-alerts-general/-/issues/48))
-   Propagate negative delays ([pid#137](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/137))
-   Set default `turnaroundEstimateInSeconds` for delay propagation to 0
-   Arriva City data are coming directly trough telmax datasource ([pid#346](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/346))
-   Replace node-redis with ioredis ([core#39](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/39))

## [2.19.0] - 2024-09-02

### Added

-   Integration of VYMI (JIS) Infotexts ([vymi-alerts#30](https://gitlab.com/operator-ict/golemio/jis/vymi-alerts/vymi-alerts-general/-/issues/30))
-   Introduce proper API versioning
-   EP /v3/pid/infotexts for VYMI (JIS) Infotexts

### Changed

-   asyncAPI yaml ([integration-engine#258](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/issues/258))
-   EP /v2/pid/departureboards - fetch VYMI (JIS) Infotexts for departures instead of the legacy infotexts

### Removed

-   Unused data view `analytic.v_ropid_departure_boards` ([Slack thread](https://golemiocz.slack.com/archives/CDBB07T7G/p1724745122298239))

## [2.18.0] - 2024-08-19

### Added

-   add backstage metadata files
-   add .gitattributes file

### Changed

-   Set canceled position's coordinates to the null island, if the previous position is not available ([pid#404](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/404))
-   Filter out positions with null island coordinates from some APIs (vehiclepositions, GTFS-RT)

### Fixed

-   Untracked vehicles can now be canceled ([pid#404](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/404))
-   Positions with `state_position=unknown` should not be considered at all

## [2.17.0] - 2024-08-14

### Fixed

-   Incorrect type casts in view `v_public_vehiclepositions_future_stop_times` ([general#610](https://gitlab.com/operator-ict/golemio/code/general/-/issues/610))

### Added

-   Last-Modified header for all GTFS-RT PB endpoints ([pid#408](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/408))

## [2.16.0] - 2024-08-07

### Changed

-   Replace invalid Prague Open Data links in the API docs ([pid#414](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/414))
-   Always use `shape_dist_traveled` from the closest anchor point, even if the vehicle announces arrival (TCP event 'P') outside of the stop ([pid#380](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/380))

### Fixed

-   Fix missing delay in public departure boards ([pid#3378](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/394))

## [2.15.0] - 2024-07-31

### Added

-   Parametrize `CommonMessageProcessor` error log

### Changed

-   Increase the retention period of cancelled trips to 6 hours ([pid#375](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/375))
-   Configurable retention period for cancelled and active trips

### Fixed

-   Canceled vehicle positions without location coords not getting processed ([pid#375](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/375))
-   Fix of occasional unwanted propagation of canceled vehicle positions ([pid#375](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/375))
-   Typo in `CommonMessageProcessor` error log
-   Exclude vehicle positions without latitude and longitude coords from OG repositories and from queries used for GTFS-RT feeds ([pid#404](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/404))
-   Sequelize data types of the `lan` and `lng` columns in several sources ([pid#404](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/404))

## [2.14.6] - 2024-07-29

### Added

-   Ropid static data to blob storage ([pid#329](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/329))

### Fixed

-   Valid header definition in OpenAPI docs

## [2.14.5] - 2024-07-17

### Added

-   Apply compression to OG responses for endpoints `/vehiclepositions*`, `/public/gtfs/trips/{gtfsTripId}`, `/public/vehiclepositions*`, and `/public/departureboards` by default, unless explicitly requested otherwise ([pid#383](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/383))

### Fixed

-   api validations after adding checkExact in core ([core#109](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/109))

## [2.14.4] - 2024-07-15

### Fixed

-   EP: /public/vehiclepositions/{vehicle_id} return correct realtime arrival and departure time([pid#3378](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/378))

## [2.14.3] - 2024-07-10

### Changed

-   Adjust stop limits for the `/public/departureboards` EP ([pid#393](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/393))
-   Improved GTFS-RT feed message verification error messages

## [2.14.2] - 2024-07-04

### Changed

-   Descriptor data saved in transaction to prevent aircon info outages ([pid#388](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/388))
-   Improve metro railtrack data

### Fixed

-   At stop vehicles have `shape_dist_traveled` behind or ahead of the stop ([pid#385](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/385))

### Removed

-   Duplicate rounding of `shape_dist_traveled`

### Added

-   Info about USB chargers availability in vehicle ([pid#357](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/357))

## [2.14.1] - 2024-07-01

### Changed

-   Update OpenAPI docs for PID Departure Boards ([pid#399](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/399))
-   Descriptor data saved in transaction to prevent aircon info outages ([pid#388](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/388))

### Fixed

-   removing includePositions parameter from /vehiclepositions [pid#381](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/381)
-   EP: /public/departureboards do not return departures where next_stop_sequence is null [pid#395](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/395)

## [2.14.0] - 2024-06-24

### Added

-   Asyncapi documentation [pid#372](https://gitlab.com/operator-ict/golemio/projekty/ropid/polohy-vozidel/-/issues/372)
-   Determine train platform code based on RT cis stop info (if available) ([pid#391](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/391))
-   Trim sectors from train platform codes

### Fixed

-   Fixing past_stop_times views to get correct delays[pid#44](https://gitlab.com/operator-ict/golemio/projekty/ropid/polohy-vozidel/-/issues/44)
-   Invalid CIS stop platform codes in public stop time cache

## [2.13.9] - 2024-06-12

### Changed

-   EP: /public/departureboards returning HTTP 200 and empty array when one of the provided stops exists ([pid#47](https://gitlab.com/operator-ict/golemio/projekty/ropid/polohy-vozidel/-/issues/47))

## [2.13.8] - 2024-06-05

### Fixed

-   Generating of `trip_updates.pb`

## [2.13.7] - 2024-06-05

### Added

-   Add information about stop name and gps to parquet files [pid#46](https://gitlab.com/operator-ict/golemio/projekty/ropid/polohy-vozidel/-/issues/46)

### Fixed

-   Correctly assign `last_stop_headsign` to before track positions

## [2.13.6] - 2024-06-03

### Added

-   public endopint for vehicle position extended for combination of vehicle id and trip id ([polohy-vozidel#49](https://gitlab.com/operator-ict/golemio/projekty/ropid/polohy-vozidel/-/issues/49))
-   New public EP for serving future GTFS trip info ([polohy-vozidel#48](https://gitlab.com/operator-ict/golemio/projekty/ropid/polohy-vozidel/-/issues/48))

### Changed

-   Use cache-control header instead of Redis cache middleware ([core#108](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/108))

### Removed

-   Computed/internal GTFS fields from `/gtfs` EPs not present in the GTFS data - we don't need to expose them in the API

## [2.13.5] - 2024-05-29

### Changed

-   Public API - `trip_headsign` now represents the destination of the trip based on the last stop sequence (headsign can change during the trip) ([pid#386](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/386))

### Fixed

-   GTFS-RT and public EP - incorrect stop time predictions when the vehicle is before the track with `delay=null`

## [2.13.4] - 2024-05-22

### Added

-   Add timestamp validation [pid#365](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/365)

### Changed

-   Data retention - only trips with last position's `valid_to` set to `null` should be considered for deletion when the initial `valid_to` check fails ([polohy-vozidel#39](https://gitlab.com/operator-ict/golemio/projekty/ropid/polohy-vozidel/-/issues/39))

### Fixed

-   Saving logic about delay to stop.times.history ([pid#44](https://gitlab.com/operator-ict/golemio/projekty/ropid/polohy-vozidel/-/issues/44))

## [2.13.3] - 2024-05-20

### Changed

-   Vehicle descriptor type is now always the manufacturer with the vehicle type (i.e. `Solaris Urbino 8.9 LE`) instead of MPV vehicle type ([pid#379](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/379))

### Fixed

-   Failing IG tests

### Removed

-   `vehicle_type_id` (MPV vehicle type) from public API cache

## [2.13.2] - 2024-05-15

### Fixed

-   GTFS-RT - incorrect stop time predictions when the vehicle is before the track with `delay=0` (similar hotfix as in the public EPs)

## [2.13.1] - 2024-05-13

### Changed

-   Rework worker to use AbstractWorker class ([pid#175](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/175))

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [2.13.0] - 2024-05-09

### Changed

-   Include delays at past stops in GTFS-RT ([pid#313](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/313))
-   Increaase shared delay cache TTL to 1 hour (plus 5 minutes leeway) to prevent cache misses resulting in error responses on public EPs ([pid#360](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/360))
-   `valid_to` position attribute is now always considered when retrieving active trips for GTFS-RT feeds and public vehicle positions
-   Adjust the trip schedule identification boundary for metro trips to 4 hours

### Fixed

-   Disappearing vehicles in the response of the public vehicle positions endpoints ([pid#370](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/370))
-   Incorrect stop time predictions when the vehicle is before the track with `delay=0` ([pid#371](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/371))

## [2.12.11] - 2024-05-06

### Added

-   Caching mechanism for upcoming and recent departures
-   New public EP (experimental) for serving departures to the PID lítačka app optimized for higher traffic ([polohy#42](https://gitlab.com/operator-ict/golemio/projekty/ropid/polohy-vozidel/-/issues/42))

### Changed

-   Hide no stop waypoints on /public/vehiclepositions/{vehicleId} ([pid#369](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/369))
-   fix improving trip schedule identification for metro tripes

## [2.12.10] - 2024-04-29

### Changed

-   More vehicles per gtfs trip ([pid#350](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/350))

## [2.12.9] - 2024-04-24

### Added

-   Add `stop_sequence` info to endpoint `/public/vehiclepositions/{vehicleId}` [pid#366](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/366)

## [2.12.8] - 2024-04-15

-   No changelog

## [2.12.7] - 2024-04-15

### Removed

-   Remove mentions of the `includePositions` query parameter of the `/vehiclepositions` EP from the API docs - the parameter is no longer supported and will be removed in the future

### Changed

-   processing of old messages for updateDelay is skipped ([pid#303](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/303))

## [2.12.6] - 2024-04-10

### Changed

-   axios exchanged for native fetch ([core#99](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/99))

## [2.12.5] - 2024-04-03

### Added

-   Log TCP message timestamps `tjr` that are 22+ hrs ahead ([pid#325](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/325))

### Changed

-   Filter servicing tram lines and tram lines with unusual run number (internal lines) from runs ([pid#355](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/355))

### Fixed

-   Adjustmets for metro stop detection ([pid#281](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/281))

## [2.12.4] - 2024-03-27

### Changed

-   Find and invalidate MPV trips with unknown GTFS trip id ([pid#352](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/352))

### Fixed

-   GTFS trips start/end timestamp computation ([pid#250](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/250))
-   VP estimated point delay and timestamp computation ([pid#250](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/250))
-   GTFS departures arrival/departure timestamp computation ([pid#250](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/250))
-   VP trips start/end timestamp computation ([pid#250](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/250))
-   VP delay before track computation ([pid#250](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/250))

## [2.12.3] - 2024-03-25

### Changed

-   Split data for saveDataToDb queue ([pid#354](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/354))

### Fixed

-   Fixed router query validation rules ([core#93](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/93))

## [2.12.2] - 2024-03-11

### Fixed

-   Additional data in `pid_feed.pb` ([pid#351](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/351))

## [2.12.1] - 2024-03-06

### Changed

-   Update mapping of VYMI events ([pid#348](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/348))

## [2.12.0] - 2024-03-04

### Fixed

-   Missing data in `pid_feed.pb` ([pid#351](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/351))

## [2.11.10] - 2024-02-28

### Added

-   History tables' data retention jobs

### Changed

-   Retry sending unprocessed logs to DCIP in 10 minutes window ([pid#343](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/343))

## [2.11.9] - 2024-02-21

### Changed

-   Update express validator to 7.0.1 ([core#94](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/94))
-   queue `saveDataToDb` changed to classic with transient messages

### Added

-   Additional `minutesBefore` and `minutesAfter` query param limitation on the `/pid/departureboards` EP to prevent excessive data processing
-   Data retention job for the `vehiclepositions_cis_stops` table

## [2.11.8] - 2024-02-14

### Changed

-   Update OG OpenAPI docs

### Removed

-   Remove unnecessary or unwanted GTFS attributes from our output API (stops, stop times, departure boards)

## [2.11.7] - 2024-02-12

### Added

-   Add `vehiclepositions_cis_stops` for storing RT CIS stops from MPVNet

### Changed

-   unify delete tasks ([pid#331](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/331))
-   Propagate RT platform code to GTFS-RT trip updates and departure boards (trains only) ([pid#106](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/106))

### Fixed

-   Public EP - Unknown operator of train trips

## [2.11.6] - 2024-02-05

### Added

-   Create new EP for detailed public trip info ([pid#335](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/335))

### Changed

-   Decrease delay computation cache TTL to 30 minut, with increasing TTL (+30 minutes) for the most recent data

### Fixed

-   gtfs_date for trains added to trips db table
-   Filter trips for vehiclepositions endpoint based on their status and time([pid#341](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/341))

## [2.11.5] - 2024-01-29

### Fixed

-   don't overwrite departure delay with another O message ([pid#338](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/338))
-   warning messages for non existent gtfs trip id for train delay propagation

## [2.11.3] - 2024-01-22

### Added

-   propagation delay for trains ([pid#332](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/332))

## [2.11.2] - 2024-01-17

### Changed

-   Refactor anchor point iterators - replace nested recursive calls with synchronous iterations
-   Limit anchor point search to the bounding box of the current position to avoid unnecessary distance calculations

### Fixed

-   Correctly compute delay of vehicles performing switchbacks/zigzags ([pid#233](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/233))

### Removed

-   Feature flags
    -   pid-enable-air-conditioning - replace with env variable `VEHICLE_POSITIONS_IS_AIR_COND_ENABLED` (default `true`)
    -   pid-show-metro-on-map - always return VP metro trips

## [2.11.1] - 2024-01-10

### Added

-   Add state_position and vehicle_id ([pid#336](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/336))

### Changed

-   Improve metro railtrack data

## [2.11.0] - 2023-12-20

### Added

-   Create `vehiclepositions_stop_times_history` table for storing stop times of all terminated realtime trips ([polohy-vozidel#32](https://gitlab.com/operator-ict/golemio/projekty/ropid/polohy-vozidel/-/issues/32))
-   Store GTFS `direction_id` with RT trip data
-   Internal run number to metro trips ([pid#330](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/330))

### Changed

-   Refactor - run number is now always a number
-   Keep the original bearing when possible ([pid#323](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/323))

## [2.10.3] - 2023-12-18

### Changed

-   Update OAS docs

## [2.10.2] - 2023-12-18

### Changed

-   Optimize delay computation database query - only use a single query to fetch GTFS shapes and stop times ([pid#304](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/304))
-   `updateDelay` minor refactoring and improvements

### Fixed

-   reappearing of deleted trips ([pid#320](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/320))

## [2.10.1] - 2023-12-11

### Added

-   Azure Table Storage data retention task ([core#84](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/84))

### Fixed

-   Duplicate vehicle descriptors

## [2.10.0] - 2023-12-06

### Removed

-   Remove `_history` tables from DeleteDataTask ([polohy-vozidel#31](https://gitlab.com/operator-ict/golemio/projekty/ropid/polohy-vozidel/-/issues/31))

## [2.9.9] - 2023-11-29

### Fixed

-   Incorrect `delay` when in the `before_track` position state, during the day of the change to/from daylight saving time ([pid#327](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/327))

## [2.9.8] - 2023-11-22

### Fixed

-   Duplicate `VehicleDescriptor` IDs in `VehiclePosition` GTFS-RT entities ([pid#318](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/318))

## [2.9.7] - 2023-11-15

### Changed

-   Improve regional bus tracking detection ([polohy-vozidel#30](https://gitlab.com/operator-ict/golemio/projekty/ropid/polohy-vozidel/-/issues/30))
-   Regional bus before track positions should always have `delay=0`

## [2.9.6] - 2023-11-13

### Added

-   new public API ([pid#310](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/310))

### Changed

-   Determine metro run by trip_id ([pid#308](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/308))

### Fixed

-   Failing unit test for metro trip due to timezone change

## [2.9.5] - 2023-10-23

### Changed

-   Refactor tracking status as boolean `is_tracked` ([pid#279](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/279))
-   Improve VP tracking detection
-   Improve ARRIVA City GTFS pairing ([polohy-vozidel#29](https://gitlab.com/operator-ict/golemio/projekty/ropid/polohy-vozidel/-/issues/29))
-   Regional bus whitelist is now based on vehicle registration numbers instead of cis line ids

### Fixed

-   Fix edge cases in `RegionalBusPositionsManager` and `PositionsManager`, which can cause nullish values of `delay` and `origin_timestamp` attributes in `vehiclepositions_positions` ([pid#314](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/314))
-   Fix non-`null` position attributes being overwritten with `null` by `bulkSave` used in `UpdateDelayTask` and `ProcessRegionalBusPositionsTask`, when the first saved object contains extra attributes ([pid#314](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/314))
-   Fix incorrect `null` values of `delay` and `next_stop_*` attributes in `vehiclepositions_positions` when a track position is in `state_position=before_track` and follows a position with `state_position=before_track_delayed` ([pid#314](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/314))
-   `NaN` `lat` and `lng` values from `PositionsRepository.getPositionsForUpdateDelay`
-   Hotfix of non-null delays of vehicles in depots which are incorrectly in the position state `before_track`. The proper fix will be part of issue ([pid#320](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/320)).

## [2.9.4] - 2023-10-18

### Fixed

-   Invalid StopTimeUpdates in GTFS-RT Trip Updates feed
-   Unknown delay of train trips on departure boards

## [2.9.3] - 2023-10-09

### Changed

-   Set `vehiclepositions_positions` and `vehiclepositions_trips` tables as unlogged ([pid#241](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/241))
-   Integrate ARRIVA City positions ([polohy-vozidel#28](https://gitlab.com/operator-ict/golemio/projekty/ropid/polohy-vozidel/-/issues/28))
-   Import vehicle wheelchair accessibility information from the Seznam Autobusu API

## [2.9.2] - 2023-10-02

### Changed

-   removed unnecessary columns from departure boards queries ([polohy-vozidel#27](https://gitlab.com/operator-ict/golemio/projekty/ropid/polohy-vozidel/-/issues/27))

## [2.9.1] - 2023-09-11

### Added

-   New task for transforming and saving ARRIVA City run messages ([polohy-vozidel#26](https://gitlab.com/operator-ict/golemio/projekty/ropid/polohy-vozidel/-/issues/26))

### Fixed

-   Support both `?minutesBefore > 0` and `?skip=atStop` in one request ([pid#307](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/307))

## [2.9.0] - 2023-09-06

### Added

-   `trip_number` column to `ropidgtfs_run_numbers` table ([polohy-vozidel#25](https://gitlab.com/operator-ict/golemio/projekty/ropid/polohy-vozidel/-/issues/25))
-   Ropid GTFS route_sub_agencies added as new table ([pid#24](https://gitlab.com/operator-ict/golemio/projekty/ropid/polohy-vozidel/-/issues/24))

### Changed

-   propagateDelay - utilize bulkSave to reduce the number of database queries

### Fixed

-   Retrieval of delay computation data from Redis cache

## [2.8.10] - 2023-09-04

### Removed

-   Remove GTFS-RT feed testing JSON endpoints ([pid#301](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/301))

## [2.8.9] - 2023-08-30

### Changed

-   Refactor repositories for trips and positions (bulkUpsert, bulkUpdate and save) to reduce the number of database queries ([pid#302](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/302))

## [2.8.8] - 2023-08-23

### Changed

-   Run integration apidocs tests via Golemio CLI ([core#46](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/46))

## [2.8.7] - 2023-08-21

### Changed

-   Abortable workers interface ([core#64](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/64))
-   Update download of gtfs data, responsibility for creating / destroying tables is moved to sql procedures ([pid#295](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/295))

## [2.8.6] - 2023-08-07

### Fixed

-   Common run unique constraint error ([pid#206](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/206))

## [2.8.5] - 2023-08-02

### Fixed

-   Loki datasource schema validation

## [2.8.4] - 2023-08-02

### Changed

-   Merge `BusDelayAtStop` and `TramDelayAtStop` to ensure consistent delay at stop calculation for all TCP DPP common runs ([pid#285](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/285))
-   Preset logs - change push format

### Fixed

-   Exception handling for departure presets

## [2.8.3] - 2023-07-31

### Changed

-   retry when unable to download Ropid Gtfs data from ftp ([pid#291](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/291))
-   loading of metadata regarding synchroniztation from ftp
-   Minor refactoring of PubSub concept in timetables loading

## [2.8.2] - 2023-07-26

-   Departure boards - metro trips are deemed accessible (`departure.trip.is_wheelchair_accessible`) only if the station is accessible ([pid#325](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/283))

## [2.8.1] - 2023-07-24

### Changed

-   Decrease TTL of the `vehiclepositionsgtfsrt.generateFiles` queue ([pid#276](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/276))
-   Update GTFS-RT openapi docs

## [2.8.0] - 2023-07-24

### Changed

-   Generate additional metro positions within a stricter time limit ([pid#265](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/265))
-   Optimize delay propagation logic

### Fixed

-   Opentelemetry child span initialization on core bump- Opentelemetry child span initialization on core bump

### Removed

-   Duplicate interface for GTFS run trip schedule

## [2.7.12] - 2023-07-20

### Fixed

-   Change column ordering in `ropid_departures_presets` to prevent errors when inserting data from the tmp table

## [2.7.11] - 2023-07-17

### Fixed

-   Recast the SQL function EXTRACT's return type to `integer`

## [2.7.10] - 2023-07-10

### Added

-   Collect preset logs and send them to the ROPID monitoring API ([pid#230](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/230))

## [2.7.9] - 2023-06-26

### Changed

-   Refactoring connectors ([core#17](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/17))

## [2.7.8] - 2023-06-14

### Added

-   Enable processing of trolleybus run messages ([pid#271](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/271))
-   GTFS-RT - combine `trip_updates.pb` and `vehicle_positions.pb` into a single feed ([pid#255](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/255))

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))
-   GTFS-RT - change entity.id of `vehicle_positions.pb` from GTFS trip id to realtime trip id
-   Improve API docs

## [2.7.7] - 2023-06-07

### Added

-   possiblity to upload departure presets trough input gateway([pid#247](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/247))

## [2.7.6] - 2023-06-05

### Fixed

-   Timestamp validation ([pid#263](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/263))

## [2.7.5] - 2023-05-31

### Changed

-   Order unprocessed positions by `created_at` instead of `origin_timestamp` to ensure the correct order of positions ([pid#259](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/259))
-   Refactor metro run timestamps ([pid#263](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/263))
-   Refactor not public trip (greasing tram) mappers
-   Update golemio errors ([core#62](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/62))
-   Increase `ropid_departures_presets.url_query_params` column size ([pid#266](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/266))

### Fixed

-   `delay.last_stop_departure` is null when a bus or tram is running on time

## [2.7.4] - 2023-05-29

### Fixed

-   `off_track` tram position processing

## [2.7.3] - 2023-05-24

### Changed

-   Replace bigint timestamps with timestamptz/date objects/iso strings wherever possible ([pid#263](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/263))
-   Improve typings of RT data

## [2.7.2] - 2023-05-22

### Changed

-   Update GTFS trip data in the realtime `vehiclepositions_trips` feed after downloading GTFS static ([pid#82](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/82))
-   Always treat gtfs route type as an integer

## [2.7.1] - 2023-05-17

### Changed

-   GTFS checkForNewData - when forceRefresh is true, always refresh precomputed tables to ensure the most recent data is served to the API ([pid#270](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/270))

### Fixed

-   GTFS downloadDatasets - fix infinite loop when the FTP server is unreachable or the connection is unstable

## [2.7.0] - 2023-05-10

### Added

-   Create and utilize optimized views to serve data to the vehiclepositions output API ([pid#262](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/262))

### Changed

-   Rename the alerting view `v_vehiclepositions_last_position` to `v_vehiclepositions_alerts_last_position`

## [2.6.9] - 2023-05-03

### Changed

-   Adjust IG logger emitter imports

## [2.6.8] - 2023-04-26

### Added

-   Predict stop time delays in GTFS-RT trip_updates.pb ([pid#235](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/235))
-   Add `provider_source_type` column to the `vehiclepositions_trips` table

### Changed

-   Optimize db query for GTFS-RT trip data
-   Set limit for `/vehiclepositions` response with `includePositions=true` parameter ([pid#254](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/254))

### Fixed

-   Download datasets if the last deployment failed ([pid#248](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/248))

## [2.6.7] - 2023-04-19

### Changed

-   Optimize precomputed GTFS tables ([pid#251](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/251))
-   Properly clean cache after replacing GTFS tables instead of flushing all keys in the database

## [2.6.6] - 2023-04-12

### Added

-   Publish airConditioned info to GTFS ([pid#239](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/239))

### Fixed

-   propagate delay over midnight
-   Processing of NOT_PUBLIC run messages (trams without schedule) ([pid#232](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/232))

## [2.6.4] - 2023-04-05

### Changed

-   API docs - improve description of air condition switch ([pid#252](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/252))

## [2.6.3] - 2023-03-27

### Fixed

-   Fix TripHistory delete old data.
-   Fix http busses delay computation.

## [2.6.2] - 2023-03-27

### Added

-   Vehicle descriptor integration of Seznam Autobusu API ([pid#107](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/107))
-   Query param `airCondition` on `/pid/departureboards` endpoint
-   Introduce DI

### Changed

-   Enrich trips with air condition information ([pid#107](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/107))
-   Rename feature toggle for metro
-   Minor refactoring of VP router and repositories

### Fixed

-   `delay_stop_arrival` of `off_track` TCP tram position is `NaN` ([pid#214](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/214))
-   Fix showing delays for night lines. ([pid#240](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/240))
-   Fix presets loading ([pid#242](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/242))

## [2.6.1] - 2023-03-16

### Fixed

-   GTFS departure timestamp DB index typo

## [2.6.0] - 2023-03-15

### Added

-   `delay.last_stop_arrival` and `delay.last_stop_departure` calculation for metro trips ([pid#219](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/219))

### Changed

-   Change validations from Mongoose to JSON Schema
-   Decide this/last/next stops based on metro railtrack data instead of estimated anchor points
-   Correct route id/run number for HTTP trips ([pid#225](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/225))
-   Rename `sequence_id` VP trips column to `run_number`
-   Improve vehicle descriptor, improve metro id format ([pid#226](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/226))

## [2.5.3] - 2023-03-01

### Changed

-   Decide the `after_track` position state for TCP bus/tram runs based on scheduled timestamp (the `tjr` input attribute) ([pid#224](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/224))

### Fixed

-   Metro origin time

## [2.5.2] - 2023-02-27

### Changed

-   Replace some exceptions and warning logs that are of lesser importance with verbose logs ([pid#207](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/207))
-   Update Node.js to v18.14.0 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [2.5.1] - 2023-02-15

### Fixed

-   Fixed validation of preferredTimezone query param ([pid#216](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/216))

## [2.5.0] - 2023-02-13

### Added

-   Before track delay correction ([pid#199](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/199))
-   `delay.last_stop_departure` calculation for TCP (DPP) busses ([pid#186](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/186))

### Changed

-   Rename TCP event enum keys

## [2.4.2] - 2023-01-30

### Changed

-   fixed not copying delay from before_track_delayed positions known before for HTTP data by updating also last_position_context in VPTrip model
-   fixed not propagating delay for HTTP trips ([pid#190](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/190))
-   delay calculation for propagating delay for followup connection. New constant of 30 seconds was introduced which is estimated time to go trough turning point. ([pid#191](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/191))
-   vehicle is changed to state after track, after last stop ([pid#217](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/217))
-   Refactor VP worker methods into separate tasks, add message validation ([pid#176](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/176))

### Added

-   Feature toggle for hiding metro trips
-   Metro changed from udp to tcp

## [2.4.1] - 2023-01-23

### Changed

-   Update openapi docs

## [2.4.0] - 2023-01-23

### Added

-   Migrate to npm

## [2.3.7] - 2023-01-16

### Added

-   analyze is called for `ropidgtfs_departures` during update of schedules ([pid#215](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/215))

## [2.3.6] - 2023-01-04

### Changed

-   Utilize streams for GTFS datasets ([pid#209](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/209))
-   Replace `SET` with `SET LOCAL`, remove `SET search_path` from the `departure_part` SQL function
-   Enable processing of all metro messages ([pid#218](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/218))

## [2.3.5] - 2022-12-13

### Added

-   processing of run messages for trams without tjr ([pid#183](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/183))

### Changed

-   Store last position's context in `vehiclepositions_trips`, fetch only unprocessed positions ([pid#208](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/208))
-   Refactor schema definitions for VP trips, improve typings
-   Find stops directly in `ropigtfs_stops` with ASW ids ([pid#213](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/213))
-   Improve validation of aswIds and cisIds

## [2.3.4] - 2022-11-29

### Added

-   new schema analytic
-   new analytic view for departure boards `v_ropid_departure_boards`
-   Implementation documentation ([pid#96](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/96))

### Changed

-   added stopts for before_track trips into GTFSRt Feed ([pid#202](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/202))
-   added before_track trips into GTFSRt Feed ([pid#202](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/202))
-   Set message TTL on remaining `vehiclepositions.*` queues ([pid#204](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/204))

## [2.3.3] - 2022-11-10

### Added

-   data retention for tables vehiclepositions_positions and vehiclepositons_trips ([pid#189](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/189))
-   Metro line A positions integration ([pid#181](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/181))
-   Helper for generating trips id from runs

### Changed

-   filter `routeHeadingOnceNoGap` for departure boards considers also stop id to show also departures from different directions ([pid#151](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/151))
-   Change common run empty schedule log level to verbose
-   Change column type of `vehiclepositions_metro_runs_messages`.`delay_origin` from smallint to int

### Fixed

-   sorting of `stopTimeUpdate` array in an endpoint `/gtfsrt/trip_updates.pb` ([pid#192](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/192))
-   Fatal error on VYMI datasource - replace streamed datasource to correctly catch parsing and validation errors ([pid#201](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/201))
-   Fix error `Maximum call stack size exceeded` by removing recursion in PositionManager and PositionCalculator helpers. ([pid#200](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/200))

## [2.3.2] - 2022-10-13

### Added

-   New fields added to `/pid/infotexts` endpoint ([pid#152](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/152))
-   Apidocs integration tests ([pid#179](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/179))
-   Create separate worker for railtrack data
-   Receive and save metro railtrack data to `ropidgtfs_metro_railtrack_gps` ([pid#157](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/157))

### Changed

-   Update TypeScript to v4.7.2

### Fixed

-   Format GTFS trip route boolean values
-   GTFS stoptimes time parsing (from, to query parameters) ([pid#184](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/184))

## [2.3.1] - 2022-10-05

### Changed

-   Revert back to the node redis connector implementation in OG ([core#39](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/39))

## [2.3.0] - 2022-10-04

### Added

-   Create separate worker for runs
-   Receive and save metro data to `vehiclepositions_metro_runs_messages` ([pid#166](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/166))
-   Calculation of a delay on arrival and departure from stops for trams ([pid#168](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/168))

### Changed

-   Replace regular expressions in GTFS stop model ([pid#163](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/163))
-   Ropid VYMI worker updated to task based architecture ([pid#170](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/170))
-   Create partitions of departures ([pid#169](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/169))
-   reflect-metadata changed for core shared reflect object ([core#44](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/44))
-   Exchange node redis for ioredis ([core#39](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/39))

### Fixed

-   Show previous departures with the minutesbefore query parameter gt 0 ([pid#178](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/178))
-   Format GTFS trip route boolean values

### Removed

-   Remove query generator helper

## [2.2.11] - 2022-09-01

### Added

-   Add _includeMetroTrains_ query parameter to departureboards ([pid#97](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/97))
-   Save ASW attributes to DB ([pid#162](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/162))

### Changed

-   Skip departures at and pass stops with _?skip=atStop_ ([pid#141](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/141))
-   Time condition is removed for propagating delay ([pid#154](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/154))
-   The Lodash library was replaced with js functions ([core#42](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/42))
-   Only generate delay message for current trips with the uttermost start timestamp ([pid#155](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/155))
-   Ignore tram/bus run messages without the TJR attribute ([pid#155](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/155))
-   Rename external audit columns in `ropidvymi_events`
-   Move audit datetime parsing to the vymi transformation ([pid#156](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/156))

### Removed

-   Remove valid_to property from vehiclepositions last_position ([pid#161](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/161))

## [2.2.10] - 2022-07-27

### Added

-   Openapi docs for IG and OG [OG#213](https://gitlab.com/operator-ict/golemio/code/output-gateway/-/issues/213)
-   Create new route for pid infotexts [pid#133](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/133)
-   Different calculation position valid_to attribute for cancelled lines.[pid#148](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/148)

### Changed

-   New separate calculation of valid_to for trams before track.
-   Improved logic to recognize vehicles on the way from/to garage to mark them as invisible[pid#146](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/146)
-   Move logic from pid router to separate controllers

## [2.2.9] - 2022-06-28

### Changed

-   added possibility of multiple current trips (before one) for situation, when line changes number at a stop ([pid#71](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/71))
-   interpret the position state on the "V" TCP event for both trams and buses as "after track" ([pid#131](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/131))

### Added

-   Wheelchair accessibility to GTFS RT via OVapi extension

### Removed

-   Remove queue for trips model - accidental deadlocks should already be handled in the DB

## [2.2.8] - 2022-06-09

### Changed

-   re-enabled gtfs alerts

## [2.2.7] - 2022-06-09

### Changed

-   Removal of saveStopsToDB worker method and its processing branch including db table.
-   Replacing booleanPointInPolygon with CheapRuler.distance()
-   Added error handling for getShapePointsAroundPosition

### Fixed

-   Fix for /departureboards, add type for PIDDeparturesModel return value

## [2.2.6] - 2022-06-06

### Added

-   Add indexes to vymi tables

### Changed

-   Replace expireat with TTL for delay computation trips cache
-   Replace jsdom fragment functionality with regexes and html-entities
-   Disabletd GTFS Alerts generating

### Fixed

-   Get expire timestamp using the native Date object instead of Luxon DateTime

### Removed

-   Remove jsdom dependency

## [2.2.5] - 2022-05-31

### Added

-   add GTFS RT Alerts

### Changed

-   Expire VP Redis keys ([pid#139](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/139))

## [2.2.4] - 2022-05-26

### Added

-   For departure boards new attribute minutes in departures[].departure_timestamp path was added showing number of minutes till departure. ([pid#132](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/132))
-   Added filters routeHeadingOnceNoGap, routeHeadingOnceNoGapFill for departure boards

## [2.2.3] - 2022-05-20

### Fixed

-   hotfix nullable last_stop_sequence position attribute for canceled trip

## [2.2.2] - 2022-05-18

### Changed

-   instead of 3 map fns only one for cycle
-   instead moment.format() use sql literal
-   removed trip.updated_at attribute, not in API specs
-   added indexes for \_departures, \_trips, \_positions
-   refactoring of OG/PIDDepartureBoardsModel and time optimalization
-   fix ?updatedSince default value
-   fix ambiguous column name

### Fixed

-   Create shared queue for vehicle positions trips model. This should help prevent accidental deadlocks
-   Database error for multiple date parameters of GTFS routes (enforce single string validation) ([pid#126](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/126))
-   Determine train tracking status as NOT_TRACKING if last cis stop is actually the last stop of a trip ([pid#123](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/123))
-   Simplify transactions in trips and positions models to prevent more accidental deadlocks, add more logging

## [2.2.1] - 2022-05-04

### Changed

-   Always add delay to the real arrival time (even when ahead of time)
-   Always add delay to the real departure time (even when ahead of time)
-   Substract waiting time from delay (real repature time) as long as the result delay is greater than zero ([pid#51](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/51))
-   Adapted to MR add valid_to attributes ([pid#35](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/35))
-   Separate processing of tram and bus runs ([pid#66](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/66))
-   Filter internal bus lines from runs ([pid#127](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/127))

## [2.2.0] - 2022-04-13

### Changed

-   Refactoring Shapes Anchor Points algorithm, first taking shape points complementary with stops and than divide trip shape line to get desired precision. ([pid#86](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/86))
-   Minor logging improvements
-   Improve train tracking at stops ([pid#70](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/70))

## [2.1.13] - 2022-04-07

### Fixed

-   Conflicting locks in replaced tables ([pid#120](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/120))

## [2.1.12] - 2022-03-26

### Changed

-   Offset TJR timestamps for TCP busses

## [2.1.11] - 2022-03-01

### Changed

-   Added atribute is_air_conditioned (null value yet) to object trip for endpoints /vehiclepositions and /pid/departureboards
-   Fixed bug for setting next_stop_xy position attributes when trip is in BEFORE_TRACK state.

## [2.1.10] - 2022-02-21

### Added

-   Fixed bug in correction fn for already computed positions. Issue see below.
-   Correction fn for delays at stop where trips dwell for departure. ([pid#55](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/55))

## [2.1.9] - 2022-02-10

### Changed

-   Filter out infotexts based on the time_from field ([pid#92](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/92))

## [2.1.8] - 2022-01-26

### Fixed

-   Missing origin route id

## [2.1.7] - 2022-01-25

### Fixed

-   Consistent timestamp formatting in infotexts

## [2.1.6] - 2022-01-20

### Added

-   Precomputed trips ([pid#80](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/80))

### Changed

-   Change default timezone ([pid#76](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/76))
-   Remove milliseconds from timestamps ([pid#77](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/77))

## [2.1.5] - 2021-12-23

### Changed

-   Refactor and fix infotexts ([pid#74](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/74))

## [2.1.4] - 2021-12-14

### Fixed

-   Proper run scheduled gtfstrip

## [2.1.3] - 2021-12-08

### Added

-   Not public trips

### Fixed

-   Zero stoptimes
-   bikes_allowed property of undefined

### Security

-   Reenabled package auditing

## [2.1.0] - 2021-11-21

### Added

-   Modularized migrations ([pid#53](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/53))

### Changed

-   Optimization of timetables download table switching ([pid#65](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/65))
-   Replace db retention with sequelize destroy
-   Added trip_short_name to vehiclepositions ([pid#29](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/29))
-   Filter DPP after_track ([pid#34](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/34))
-   Modification of last_stop parameters ([pid#42](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/42))
-   Added stop_id to GTFS-Realtime trip update ([pid#54](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/54))

## [2.0.12] - 2021-10-18

### Fixed

-   Empty array of tripsIds in updateDelay

