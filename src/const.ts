export enum StatePositionEnum {
    OFF_TRACK = "off_track",
    AT_STOP = "at_stop",
    ON_TRACK = "on_track",
    BEFORE_TRACK = "before_track",
    AFTER_TRACK = "after_track",
    AFTER_TRACK_DELAYED = "after_track_delayed",
    CANCELED = "canceled",
    INVISIBLE = "invisible",
    UNKNOWN = "unknown",
    BEFORE_TRACK_DELAYED = "before_track_delayed",
    DUPLICATE = "duplicate",
    MISMATCHED = "mismatched",
    NOT_PUBLIC = "not_public",
}

export enum StateProcessEnum {
    TCP_INPUT = "tcp_input",
    UDP_INPUT = "udp_input",
    INPUT = "input",
    PROCESSED = "processed",
    INVALIDATED = "invalidated",
}

export enum TCPEventEnum {
    ARRIVAL_ANNOUNCED = "P",
    DEPARTURED = "O",
    TERMINATED = "V",
    TIME = "T",
}

export enum RegionalBusEventEnum {
    DEPARTURED = "R",
}

export const ArrayNotPublicRegistrationNumbers: string[] = ["2210", "5572"];

export const DATA_RETENTION_IN_MINUTES = 30;
export const GTFS_CALENDAR_LIMIT_IN_MINUTES = 3 * 60 * 24; // 3 days (see IE/ServicesCalendarRepository SQL query)

export const PG_INT_MAX = 2 ** 31 - 1;
export const PG_INT_MIN = -(2 ** 31);
