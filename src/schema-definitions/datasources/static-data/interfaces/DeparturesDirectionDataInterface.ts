import { DeparturesDirectionsEnum } from "#sch/ropid-departures-directions";

export interface IDeparturesDirection {
    departure_stop_id: string;
    next_stop_id_regexp: string;
    direction: DeparturesDirectionsEnum;
    rule_order: string;
    create_batch_id: string;
    created_by: string;
    update_batch_id: string;
    updated_by: string;
}
