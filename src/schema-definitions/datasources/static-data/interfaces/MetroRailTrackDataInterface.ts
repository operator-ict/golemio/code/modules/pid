/**
 * @example
 * {
 *   data: [
 *     {
 *       track_section: "2107",
 *       line: "A",
 *       gps_lat: "50.075360",
 *       gps_lon: "14.341560",
 *       stop_id: "U306Z101P"
 *     }
 *   ]
 * }
 */

export interface IMetroRailtrack {
    track_section: string;
    line: string;
    gps_lat: string;
    gps_lon: string;
    stop_id: string;
}
