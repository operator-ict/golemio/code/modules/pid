const metroRailTrackJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            track_section: {
                type: "string",
            },
            line: {
                type: "string",
            },
            gps_lat: {
                type: "string",
            },
            gps_lon: {
                type: "string",
            },
            stop_id: {
                type: "string",
            },
        },
    },
};

export { metroRailTrackJsonSchema };
