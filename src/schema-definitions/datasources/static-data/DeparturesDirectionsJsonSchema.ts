import { PossibleDeparturesDirections } from "#sch/ropid-departures-directions";

const departuresDirectionsJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            departure_stop_id: {
                type: "string",
            },
            next_stop_id_regexp: {
                type: "string",
            },
            direction: {
                type: "string",
                enum: PossibleDeparturesDirections,
            },
            rule_order: {
                type: "string",
            },
            create_batch_id: {
                type: "string",
            },
            created_at: {
                type: "string",
            },
            created_by: {
                type: "string",
            },

            update_batch_id: {
                type: "string",
            },
            updated_at: {
                type: "string",
            },
            updated_by: {
                type: "string",
            },
        },
    },
};

export { departuresDirectionsJsonSchema };
