interface IEndpointMetadata {
    lastResponse: {
        etag: string;
        updatedAt: string;
    };
}

export interface IJISMetadataDto {
    infotexts: IEndpointMetadata;
}
