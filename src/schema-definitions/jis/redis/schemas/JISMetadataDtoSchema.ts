import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IJISMetadataDto } from "../interfaces/IJISMetadataDto";

const jisMetadataDtoSchema: JSONSchemaType<IJISMetadataDto> = {
    $schema: "http://json-schema.org/draft-04/schema#",
    title: "JISMetadataDto",
    type: "object",
    properties: {
        infotexts: {
            type: "object",
            properties: {
                lastResponse: {
                    type: "object",
                    properties: {
                        etag: { type: "string" },
                        updatedAt: { type: "string", format: "date-time" },
                    },
                    required: ["etag", "updatedAt"],
                },
            },
            required: ["lastResponse"],
        },
    },
    required: ["infotexts"],
};
export { jisMetadataDtoSchema as JISMetadataDtoSchema };
