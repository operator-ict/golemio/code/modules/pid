import { InfotextSeverityLevel } from "#og/shared/constants/jis/InfotextSeverityLevelEnum";
import { StopDto } from "#sch/ropid-gtfs/models/StopDto";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import {
    CreationOptional,
    DataTypes,
    InferAttributes,
    InferCreationAttributes,
    Model,
    ModelAttributes,
} from "@golemio/core/dist/shared/sequelize";
import { IJISInfotext, IJISTranslationText } from "./interfaces";

export class JISInfotextsModel
    extends Model<InferAttributes<JISInfotextsModel>, InferCreationAttributes<JISInfotextsModel>>
    implements IJISInfotext
{
    public static tableName = "jis_infotexts";

    declare id: string;
    declare severity_level: InfotextSeverityLevel;
    declare display_type: string;
    declare active_period_start: Date;
    declare active_period_end: Date | null;
    declare description_text: IJISTranslationText;
    declare created_timestamp: Date;
    declare updated_timestamp: Date;
    declare created_at: CreationOptional<Date>;
    declare updated_at: CreationOptional<Date>;

    // associations
    declare stops?: StopDto[];

    public static attributeModel: ModelAttributes<JISInfotextsModel> = {
        id: {
            primaryKey: true,
            allowNull: false,
            type: DataTypes.UUID,
        },
        severity_level: {
            type: DataTypes.ENUM(...Object.values(InfotextSeverityLevel)),
            allowNull: false,
        },
        display_type: {
            type: DataTypes.STRING(255),
            allowNull: false,
        },
        active_period_start: {
            type: DataTypes.DATE,
            allowNull: false,
        },
        active_period_end: {
            type: DataTypes.DATE,
        },
        description_text: {
            type: DataTypes.JSONB,
            allowNull: false,
        },
        created_timestamp: {
            type: DataTypes.DATE,
            allowNull: false,
        },
        updated_timestamp: {
            type: DataTypes.DATE,
            allowNull: false,
        },
        created_at: {
            type: DataTypes.DATE,
            allowNull: false,
        },
        updated_at: {
            type: DataTypes.DATE,
            allowNull: false,
        },
    };

    public static jsonSchema: JSONSchemaType<IJISInfotext[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                id: { type: "string" },
                severity_level: { type: "string", enum: Object.values(InfotextSeverityLevel) },
                display_type: { type: "string", enum: ["GENERAL", "INLINE"] },
                active_period_start: { type: "object", required: ["toISOString"] },
                active_period_end: {
                    oneOf: [
                        { type: "object", required: ["toISOString"] },
                        { type: "null", nullable: true },
                    ],
                },
                description_text: { $ref: "#/definitions/Translation" },
                created_timestamp: { type: "object", required: ["toISOString"] },
                updated_timestamp: { type: "object", required: ["toISOString"] },
            },
            additionalProperties: false,
            required: [
                "id",
                "severity_level",
                "display_type",
                "active_period_start",
                "description_text",
                "created_timestamp",
                "updated_timestamp",
            ],
        },
        definitions: {
            Translation: {
                type: "object",
                properties: {
                    cs: { type: "string" },
                    en: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                },
                additionalProperties: false,
                required: ["cs"],
            },
        },
    };
}
