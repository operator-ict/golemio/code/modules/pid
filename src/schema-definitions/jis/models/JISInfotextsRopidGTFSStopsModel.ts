import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import {
    CreationOptional,
    DataTypes,
    InferAttributes,
    InferCreationAttributes,
    Model,
    ModelAttributes,
} from "@golemio/core/dist/shared/sequelize";
import { IJISInfotextsRopidGTFSStops } from "./interfaces";

export class JISInfotextsRopidGTFSStopsModel
    extends Model<InferAttributes<JISInfotextsRopidGTFSStopsModel>, InferCreationAttributes<JISInfotextsRopidGTFSStopsModel>>
    implements IJISInfotextsRopidGTFSStops
{
    public static tableName = "jis_infotexts_ropidgtfs_stops";

    declare infotext_id: string;
    declare stop_id: string;
    declare created_at: CreationOptional<Date>;
    declare updated_at: CreationOptional<Date>;

    public static attributeModel: ModelAttributes<JISInfotextsRopidGTFSStopsModel> = {
        infotext_id: {
            primaryKey: true,
            type: DataTypes.UUID,
            allowNull: false,
        },
        stop_id: {
            primaryKey: true,
            type: DataTypes.STRING(50),
            allowNull: false,
        },
        created_at: {
            type: DataTypes.DATE,
            allowNull: false,
        },
        updated_at: {
            type: DataTypes.DATE,
            allowNull: false,
        },
    };

    public static jsonSchema: JSONSchemaType<IJISInfotextsRopidGTFSStops[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                infotext_id: { type: "string" },
                stop_id: { type: "string" },
            },
            additionalProperties: false,
            required: ["infotext_id", "stop_id"],
        },
    };
}
