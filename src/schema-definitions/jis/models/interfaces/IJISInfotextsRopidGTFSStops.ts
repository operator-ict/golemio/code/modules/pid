export interface IJISInfotextsRopidGTFSStops {
    infotext_id: string;
    stop_id: string;
}
