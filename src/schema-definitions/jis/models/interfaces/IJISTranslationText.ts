export interface IJISTranslationText {
    cs: string;
    en?: string | null;
}
