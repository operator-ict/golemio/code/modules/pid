import { InfotextSeverityLevel } from "#og/shared/constants/jis/InfotextSeverityLevelEnum";
import { IJISTranslationText } from "./IJISTranslationText";

export interface IJISInfotext {
    id: string;
    severity_level: InfotextSeverityLevel;
    display_type: string;
    active_period_start: Date;
    active_period_end: Date | null;
    description_text: IJISTranslationText;
    created_timestamp: Date;
    updated_timestamp: Date;
}
