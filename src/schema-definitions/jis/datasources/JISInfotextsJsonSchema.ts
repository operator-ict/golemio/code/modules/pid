import { InfotextSeverityLevel } from "#og/shared/constants/jis/InfotextSeverityLevelEnum";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IJISInfotext } from "./interfaces/IJISInfotext";

export const jisInfotextsJsonSchema: JSONSchemaType<IJISInfotext[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            id: { type: "string" },
            severity_level: { type: "string", enum: Object.values(InfotextSeverityLevel) },
            display_type: { type: "string", enum: ["GENERAL", "INLINE"] },
            active_period: {
                type: "object",
                properties: {
                    start: { type: "string", format: "date-time" },
                    end: {
                        oneOf: [
                            { type: "string", format: "date-time" },
                            { type: "null", nullable: true },
                        ],
                    },
                },
                additionalProperties: false,
                required: ["start"],
            },
            description_text: {
                type: "object",
                properties: {
                    cs: { type: "string" },
                    en: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                },
                additionalProperties: false,
                required: ["cs"],
            },
            informed_entity: {
                type: "object",
                nullable: true,
                properties: {
                    stops: {
                        type: "array",
                        nullable: true,
                        items: {
                            type: "object",
                            properties: {
                                stop_id: { type: "string" },
                            },
                            additionalProperties: false,
                            required: ["stop_id"],
                        },
                    },
                },
                additionalProperties: false,
            },
            created_timestamp: { type: "string", format: "date-time" },
            last_modified_timestamp: { type: "string", format: "date-time" },
        },
        required: [
            "id",
            "severity_level",
            "display_type",
            "active_period",
            "description_text",
            "created_timestamp",
            "last_modified_timestamp",
        ],
    },
};
