import { InfotextSeverityLevel } from "#og/shared/constants/jis/InfotextSeverityLevelEnum";
import { IJISInfotextStop } from "./IJISInfotextStop";
import { IJISTranslationText } from "./IJISTranslationText";

export interface IJISInfotext {
    id: string;
    severity_level: InfotextSeverityLevel;
    display_type: string;
    active_period: {
        start: string;
        end: string | null;
    };
    description_text: IJISTranslationText;
    informed_entity?: {
        stops?: IJISInfotextStop[] | null;
    } | null;
    created_timestamp: string;
    last_modified_timestamp: string;
}
