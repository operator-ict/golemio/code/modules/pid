const outputDeparturesDirectionsJsonSchema = {
    type: "object",
    properties: {
        departure_stop_id: {
            type: "string",
        },
        next_stop_id_regexp: {
            type: "string",
        },
        direction: {
            type: "string",
        },
        rule_order: {
            type: "number",
        },
    },
};

export { outputDeparturesDirectionsJsonSchema };
