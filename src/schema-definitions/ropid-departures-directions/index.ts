import "@golemio/core/dist/shared/sequelize";
import { outputDeparturesDirectionsJsonSchema } from "./RopidDeparturesDirectionsOutputSchemas";
import { IRopidDeparturesDirectionsSDMAOutput, outputDeparturesDirectionsSDMA } from "./RopidDeparturesDirectionsSDMA";

const PossibleDeparturesDirections = [
    "top",
    "top-left",
    "left",
    "bottom-left",
    "bottom",
    "bottom-right",
    "right",
    "top-right",
] as const;

type DeparturesDirectionsEnum = (typeof PossibleDeparturesDirections)[number];

interface IRopidDeparturesDirectionsOutput {
    departure_stop_id: string;
    next_stop_id_regexp: RegExp;
    direction: DeparturesDirectionsEnum;
    rule_order: number;
}

const forExport = {
    name: "RopidDeparturesDirections",
    departureDirections: {
        name: "RopidDeparturesDirections",
        outputJsonSchema: outputDeparturesDirectionsJsonSchema,
        outputSequelizeAttributes: outputDeparturesDirectionsSDMA,
        pgTableName: "ropid_departures_directions",
    },
};

export { forExport as RopidDeparturesDirections, DeparturesDirectionsEnum, PossibleDeparturesDirections };
export type { IRopidDeparturesDirectionsSDMAOutput, IRopidDeparturesDirectionsOutput };
