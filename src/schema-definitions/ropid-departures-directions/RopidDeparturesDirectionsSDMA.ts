import { DataTypes, ModelAttributes } from "@golemio/core/dist/shared/sequelize";

interface IRopidDeparturesDirectionsSDMAOutput {
    departure_stop_id: string;
    next_stop_id_regexp: string;
    direction: string;
    rule_order: number;
    [audit: string]: unknown;
}

const outputDeparturesDirectionsSDMA: ModelAttributes<any, IRopidDeparturesDirectionsSDMAOutput> = {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
    },
    departure_stop_id: DataTypes.STRING,
    next_stop_id_regexp: DataTypes.STRING,
    direction: DataTypes.STRING,
    rule_order: DataTypes.INTEGER,
};

export { outputDeparturesDirectionsSDMA };
export type { IRopidDeparturesDirectionsSDMAOutput };
