import { IRopidSchemaDefinitions } from "#sch/shared";
import "@golemio/core/dist/shared/sequelize";
import { outputEventsJsonSchema, outputEventsRoutesJsonSchema, outputEventsStopsJsonSchema } from "./RopidVYMIOutputSchemas";
import {
    IRopidVYMIEventOutput,
    IRopidVYMIEventRouteOutput,
    IRopidVYMIEventStopOutput,
    outputEventsRoutesSDMA,
    outputEventsSDMA,
    outputEventsStopsSDMA,
} from "./RopidVYMISDMA";

interface IRopidVYMIEvent {
    "@id": string;
    "@id_dtb": string;
    "@stav": string;
    "@pa_01": string;
    "@pa_02": string;
    "@pa_03": string;
    "@pa_04": string;
    "@typ_zaznamu": string;
    "@typ_udalost"?: string;
    "@kanaly": string;
    "@nazev": string;
    "@cas_od": string;
    "@typ_do": string;
    "@cas_do"?: string;
    "@zobrazit_do"?: string;
    "@druh_dopr": string;
    "@priorita"?: string;
    "@duvod"?: string;
    "@link"?: string;
    "@opatr_ropid"?: string;
    "@opatr_dpp"?: string;
    "@popis"?: string;
    "linky-zmena"?: IRopidVYMIEventRoute[];
    "zastavky-zmena"?: IRopidVYMIEventStop[];
}

interface IRopidVYMIEventRoute {
    "@id": string;
    "@id_dtb": string;
    "@cislo": string;
    "@nazev": string;
    "@linka_typ": string;
    "@plati_od"?: string;
    "@plati_do"?: string;
    "#text"?: string;
}

interface IRopidVYMIEventStop {
    "@id": string;
    "@id_dtb": string;
    "@uzelCislo": string;
    "@zastCislo": string;
    "@zast_typ": string;
    "@plati_od"?: string;
    "@plati_do"?: string;
    "#text"?: string;
}

const datasourceJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: <Record<keyof IRopidVYMIEvent, any>>{
            "@id": {
                type: "string",
            },
            "@id_dtb": {
                type: "string",
            },
            "@stav": {
                type: "string",
            },
            "@pa_01": {
                type: "string",
            },
            "@pa_02": {
                type: "string",
            },
            "@pa_03": {
                type: "string",
            },
            "@pa_04": {
                type: "string",
            },
            "@typ_zaznamu": {
                type: "string",
            },
            "@typ_udalost": {
                type: ["null", "string"],
            },
            "@kanaly": {
                type: "string",
            },
            "@nazev": {
                type: "string",
            },
            "@cas_od": {
                type: "string",
            },
            "@typ_do": {
                type: "string",
            },
            "@cas_do": {
                type: ["null", "string"],
            },
            "@zobrazit_do": {
                type: ["null", "string"],
            },
            "@druh_dopr": {
                type: "string",
            },
            "@priorita": {
                type: ["null", "string"],
            },
            "@duvod": {
                type: ["null", "string"],
            },
            "@link": {
                type: ["null", "string"],
            },
            "@opatr_ropid": {
                type: ["null", "string"],
            },
            "@opatr_dpp": {
                type: ["null", "string"],
            },
            "@popis": {
                type: ["null", "string"],
            },
            "linky-zmena": {
                type: "array",
                items: {
                    type: "object",
                    properties: <Record<keyof IRopidVYMIEventRoute, any>>{
                        "@id": {
                            type: "string",
                        },
                        "@id_dtb": {
                            type: "string",
                        },
                        "@cislo": {
                            type: "string",
                        },
                        "@nazev": {
                            type: "string",
                        },
                        "@linka_typ": {
                            type: "string",
                        },
                        "@plati_od": {
                            type: ["null", "string"],
                        },
                        "@plati_do": {
                            type: ["null", "string"],
                        },
                        "#text": {
                            type: ["null", "string"],
                        },
                    },
                },
            },
            "zastavky-zmena": {
                type: "array",
                items: {
                    type: "object",
                    properties: <Record<keyof IRopidVYMIEventStop, any>>{
                        "@id": {
                            type: "string",
                        },
                        "@id_dtb": {
                            type: "string",
                        },
                        "@uzelCislo": {
                            type: "string",
                        },
                        "@zastCislo": {
                            type: "string",
                        },
                        "@zast_typ": {
                            type: "string",
                        },
                        "@plati_od": {
                            type: ["null", "string"],
                        },
                        "@plati_do": {
                            type: ["null", "string"],
                        },
                        "#text": {
                            type: ["null", "string"],
                        },
                    },
                },
            },
        },
    },
};

const forExport: IRopidSchemaDefinitions = {
    name: "RopidVYMI",
    events: {
        name: "RopidVYMIEvents",
        datasourceJsonSchema,
        outputJsonSchema: outputEventsJsonSchema,
        outputSequelizeAttributes: outputEventsSDMA,
        pgTableName: "ropidvymi_events",
    },
    eventsRoutes: {
        name: "RopidVYMIEventsRoutes",
        datasourceJsonSchema,
        outputJsonSchema: outputEventsRoutesJsonSchema,
        outputSequelizeAttributes: outputEventsRoutesSDMA,
        pgTableName: "ropidvymi_events_routes",
    },
    eventsStops: {
        name: "RopidVYMIEventsStops",
        datasourceJsonSchema,
        outputJsonSchema: outputEventsStopsJsonSchema,
        outputSequelizeAttributes: outputEventsStopsSDMA,
        pgTableName: "ropidvymi_events_stops",
    },
    metadata: {
        name: "RopidVYMIMetadata",
        pgTableName: "ropidvymi_metadata",
    },
};

export { forExport as RopidVYMI };
export type {
    IRopidVYMIEvent,
    IRopidVYMIEventOutput,
    IRopidVYMIEventRoute,
    IRopidVYMIEventRouteOutput,
    IRopidVYMIEventStop,
    IRopidVYMIEventStopOutput,
};
