const outputEventsJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            vymi_id: {
                type: "number",
            },
            vymi_id_dtb: {
                type: "number",
            },
            state: {
                type: "number",
            },
            ropid_created_at: {
                type: "string",
            },
            ropid_created_by: {
                type: "string",
            },
            ropid_updated_at: {
                type: "string",
            },
            ropid_updated_by: {
                type: "string",
            },
            record_type: {
                type: "number",
            },
            event_type: {
                type: ["null", "number"],
            },
            channels: {
                type: "number",
            },
            title: {
                type: "string",
            },
            time_from: {
                type: "string",
            },
            time_to_type: {
                type: "number",
            },
            time_to: {
                type: ["null", "string"],
            },
            expiration_date: {
                type: ["null", "string"],
            },
            transportation_type: {
                type: "number",
            },
            priority: {
                type: ["null", "number"],
            },
            cause: {
                type: ["null", "string"],
            },
            link: {
                type: ["null", "string"],
            },
            ropid_action: {
                type: ["null", "string"],
            },
            dpp_action: {
                type: ["null", "string"],
            },
            description: {
                type: ["null", "string"],
            },
        },
    },
};

const outputEventsRoutesJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            event_id: {
                type: "number",
            },
            gtfs_route_id: {
                type: ["null", "string"],
            },
            vymi_id: {
                type: "number",
            },
            vymi_id_dtb: {
                type: "number",
            },
            number: {
                type: "number",
            },
            name: {
                type: "string",
            },
            route_type: {
                type: "number",
            },
            valid_from: {
                type: ["null", "string"],
            },
            valid_to: {
                type: ["null", "string"],
            },
            text: {
                type: ["null", "string"],
            },
        },
    },
};

const outputEventsStopsJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            event_id: {
                type: "number",
            },
            gtfs_stop_id: {
                type: ["null", "string"],
            },
            vymi_id: {
                type: "number",
            },
            vymi_id_dtb: {
                type: "number",
            },
            node_number: {
                type: "number",
            },
            stop_number: {
                type: "number",
            },
            stop_type: {
                type: "number",
            },
            valid_from: {
                type: ["null", "string"],
            },
            valid_to: {
                type: ["null", "string"],
            },
            text: {
                type: ["null", "string"],
            },
        },
    },
};

export { outputEventsJsonSchema, outputEventsRoutesJsonSchema, outputEventsStopsJsonSchema };
