import { DataTypes, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";

interface IRopidVYMIEventOutput {
    vymi_id: number;
    vymi_id_dtb: number;
    state: number;
    ropid_created_at: string;
    ropid_created_by: string;
    ropid_updated_at: string;
    ropid_updated_by: string;
    record_type: number;
    event_type: number | null;
    channels: number;
    title: string;
    time_from: string;
    time_to_type: number;
    time_to: string | null;
    expiration_date: string | null;
    transportation_type: number;
    priority: number | null;
    cause: string | null;
    link: string | null;
    ropid_action: string | null;
    dpp_action: string | null;
    description: string | null;
    [audit: string]: unknown;
}

interface IRopidVYMIEventRouteOutput {
    event_id: number;
    gtfs_route_id: string | null;
    vymi_id: number;
    vymi_id_dtb: number;
    number: number;
    name: string;
    route_type: GTFSRouteTypeEnum;
    valid_from: string | null;
    valid_to: string | null;
    text: string | null;
    [audit: string]: unknown;
}

interface IRopidVYMIEventStopOutput {
    event_id: number;
    gtfs_stop_id: string | null;
    vymi_id: number;
    vymi_id_dtb: number;
    node_number: number;
    stop_number: number;
    stop_type: number;
    valid_from: string | null;
    valid_to: string | null;
    text: string | null;
    [audit: string]: unknown;
}

const outputEventsSDMA: ModelAttributes<any, IRopidVYMIEventOutput> = {
    vymi_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        unique: true,
    },
    vymi_id_dtb: DataTypes.INTEGER,
    state: DataTypes.INTEGER,
    ropid_created_at: DataTypes.DATE,
    ropid_created_by: DataTypes.STRING(150),
    ropid_updated_at: DataTypes.DATE,
    ropid_updated_by: DataTypes.STRING(150),
    record_type: DataTypes.INTEGER,
    event_type: DataTypes.BIGINT,
    channels: DataTypes.INTEGER,
    title: DataTypes.STRING(255),
    time_from: DataTypes.DATE,
    time_to_type: DataTypes.INTEGER,
    time_to: DataTypes.DATE,
    expiration_date: DataTypes.DATE,
    transportation_type: DataTypes.INTEGER,
    priority: DataTypes.INTEGER,
    cause: DataTypes.STRING(255),
    link: DataTypes.STRING(255),
    ropid_action: DataTypes.STRING(100000),
    dpp_action: DataTypes.STRING(100000),
    description: DataTypes.STRING(1000),
};

const outputEventsRoutesSDMA: ModelAttributes<any, IRopidVYMIEventRouteOutput> = {
    event_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
    },
    gtfs_route_id: DataTypes.STRING(255),
    vymi_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
    },
    vymi_id_dtb: DataTypes.INTEGER,
    number: DataTypes.INTEGER,
    name: DataTypes.STRING(255),
    route_type: DataTypes.INTEGER,
    valid_from: DataTypes.DATE,
    valid_to: DataTypes.DATE,
    text: DataTypes.STRING(10000),
};

const outputEventsStopsSDMA: ModelAttributes<any, IRopidVYMIEventStopOutput> = {
    event_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
    },
    gtfs_stop_id: DataTypes.STRING(255),
    vymi_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
    },
    vymi_id_dtb: DataTypes.INTEGER,
    node_number: {
        type: DataTypes.INTEGER,
        primaryKey: true,
    },
    stop_number: {
        type: DataTypes.INTEGER,
        primaryKey: true,
    },
    stop_type: DataTypes.INTEGER,
    valid_from: DataTypes.DATE,
    valid_to: DataTypes.DATE,
    text: DataTypes.STRING(10000),
};

export { outputEventsSDMA, outputEventsRoutesSDMA, outputEventsStopsSDMA };
export type { IRopidVYMIEventOutput, IRopidVYMIEventRouteOutput, IRopidVYMIEventStopOutput };
