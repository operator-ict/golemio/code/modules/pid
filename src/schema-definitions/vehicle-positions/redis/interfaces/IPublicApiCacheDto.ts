import { GTFSRouteTypeEnum } from "#helpers/RouteTypeEnums";
import { StatePositionEnum } from "src/const";

export interface IPublicApiCacheDto {
    gtfs_trip_id: string;
    route_type: GTFSRouteTypeEnum;
    gtfs_route_short_name: string;
    lat: number;
    lng: number;
    bearing: number | null;
    delay: number | null;
    vehicle_id: string;
    state_position: StatePositionEnum;
    detailed_info: {
        is_wheelchair_accessible: boolean | null;
        origin_route_name: string | null;
        shape_id: string | null;
        run_number: number | null;
        trip_headsign: string | null;
        shape_dist_traveled: number | null;
        last_stop_sequence: number | null;
        origin_timestamp: string;
        registration_number: number | null;
        operator: string | null;
    };
}
