export interface IRegionalBusGtfsCacheDto {
    gtfs_trip_id: string;
    route_name: string;
    run_number: number;
    agency_name: string;
    is_wheelchair_accessible: boolean | null;
}
