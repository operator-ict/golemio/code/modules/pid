export interface IRegionalBusCisCacheDto {
    cis_line_id: string;
    cis_trip_number: number;
    registration_number: number;
}
