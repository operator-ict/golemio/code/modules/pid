export interface IPublicStopTimeCacheDto {
    sequence: number;
    arr_delay: number | null;
    dep_delay: number | null;
    cis_stop_platform_code: string | null;
    platform_code: string | null;
    stop_id: string;
}
