import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";

export interface IScheduleDto {
    trip_id: string;
    service_id: string;
    direction_id: number | null;
    shape_id: string | null;
    date: string;
    route_id: string;
    route_type: GTFSRouteTypeEnum;
    route_short_name: string;
    origin_route_name: string;
    run_number: number;
    is_regional: string;
    is_substitute_transport: string;
    is_night: string;
    trip_headsign: string;
    trip_short_name: string | null;
    block_id: string | null;
    exceptional: number;
    min_stop_time: {
        hours: number;
        minutes?: number;
    };
    max_stop_time: {
        hours: number;
        minutes?: number;
    };
    start_timestamp: string;
    end_timestamp: string;
    first_stop_id: string;
    last_stop_id: string;
    trip_number: number | null;
    route_licence_number: number | null;
    requiredTurnaroundSeconds?: number;
}

export interface IGtfsRunTripCacheDto {
    schedule: IScheduleDto[];
}
