import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IRegionalBusGtfsCacheDto } from "../interfaces/IRegionalBusGtfsCacheDto";

const regionalBusGtfsCacheDtoSchema: JSONSchemaType<IRegionalBusGtfsCacheDto> = {
    $schema: "http://json-schema.org/draft-04/schema#",
    title: "RegionalBusGtfsCacheDto",
    type: "object",
    properties: {
        gtfs_trip_id: {
            type: "string",
        },
        route_name: {
            type: "string",
        },
        run_number: {
            type: "number",
        },
        agency_name: {
            type: "string",
        },
        is_wheelchair_accessible: {
            oneOf: [
                {
                    type: "boolean",
                },
                {
                    type: "null",
                    nullable: true,
                },
            ],
        },
    },
    required: ["gtfs_trip_id", "route_name", "run_number", "agency_name", "is_wheelchair_accessible"],
};
export { regionalBusGtfsCacheDtoSchema as RegionalBusGtfsCacheDtoSchema };
