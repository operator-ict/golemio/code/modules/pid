import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IPublicApiCacheDto } from "../interfaces/IPublicApiCacheDto";

const publicApiCacheDtoSchema: JSONSchemaType<IPublicApiCacheDto[]> = {
    $schema: "http://json-schema.org/draft-04/schema#",
    title: "PublicApiCacheDto",
    type: "array",
    items: {
        type: "object",
        properties: {
            gtfs_trip_id: {
                type: "string",
            },
            route_type: {
                type: "number",
            },
            gtfs_route_short_name: {
                type: "string",
            },
            vehicle_id: {
                type: "string",
            },
            state_position: { type: "string" },
            lat: {
                type: "number",
            },
            lng: {
                type: "number",
            },
            bearing: {
                oneOf: [
                    {
                        type: "number",
                    },
                    {
                        type: "null",
                        nullable: true,
                    },
                ],
            },
            delay: {
                oneOf: [
                    {
                        type: "number",
                    },
                    {
                        type: "null",
                        nullable: true,
                    },
                ],
            },
            detailed_info: {
                type: "object",
                properties: {
                    is_wheelchair_accessible: {
                        oneOf: [{ type: "boolean" }, { type: "null", nullable: true }],
                    },
                    origin_route_name: {
                        oneOf: [{ type: "string" }, { type: "null", nullable: true }],
                    },
                    shape_id: {
                        oneOf: [{ type: "string" }, { type: "null", nullable: true }],
                    },
                    run_number: {
                        oneOf: [{ type: "number" }, { type: "null", nullable: true }],
                    },
                    trip_headsign: {
                        oneOf: [{ type: "string" }, { type: "null", nullable: true }],
                    },
                    shape_dist_traveled: {
                        oneOf: [{ type: "number" }, { type: "null", nullable: true }],
                    },
                    last_stop_sequence: {
                        oneOf: [{ type: "number" }, { type: "null", nullable: true }],
                    },
                    origin_timestamp: { type: "string" },
                    registration_number: {
                        oneOf: [{ type: "number" }, { type: "null", nullable: true }],
                    },
                    operator: {
                        oneOf: [{ type: "string" }, { type: "null", nullable: true }],
                    },
                },
                required: [
                    "origin_timestamp",
                    "origin_route_name",
                    "shape_id",
                    "run_number",
                    "trip_headsign",
                    "shape_dist_traveled",
                    "last_stop_sequence",
                    "registration_number",
                    "operator",
                ],
            },
        },
        required: [
            "vehicle_id",
            "gtfs_trip_id",
            "route_type",
            "gtfs_route_short_name",
            "lat",
            "lng",
            "bearing",
            "delay",
            "state_position",
            "detailed_info",
        ],
    },
};
export { publicApiCacheDtoSchema as PublicApiCacheDtoSchema };
