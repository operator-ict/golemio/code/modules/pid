import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IPublicStopTimeCacheDto } from "../interfaces/IPublicStopTimeCacheDto";

const publicStopTimeCacheDtoSchema: JSONSchemaType<IPublicStopTimeCacheDto[]> = {
    $schema: "http://json-schema.org/draft-04/schema#",
    title: "PublicApiCacheDto",
    type: "array",
    items: {
        type: "object",
        properties: {
            sequence: {
                type: "number",
            },
            arr_delay: {
                oneOf: [
                    {
                        type: "number",
                    },
                    {
                        type: "null",
                        nullable: true,
                    },
                ],
            },
            dep_delay: {
                oneOf: [
                    {
                        type: "number",
                    },
                    {
                        type: "null",
                        nullable: true,
                    },
                ],
            },
            cis_stop_platform_code: {
                oneOf: [
                    {
                        type: "string",
                    },
                    {
                        type: "null",
                        nullable: true,
                    },
                ],
            },
            platform_code: {
                oneOf: [
                    {
                        type: "string",
                    },
                    {
                        type: "null",
                        nullable: true,
                    },
                ],
            },
            stop_id: {
                type: "string",
            },
        },
        required: ["sequence", "arr_delay", "dep_delay", "cis_stop_platform_code", "platform_code", "stop_id"],
    },
};
export { publicStopTimeCacheDtoSchema as PublicStopTimeCacheDtoSchema };
