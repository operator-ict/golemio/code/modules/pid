import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IRegionalBusCisCacheDto } from "../interfaces/IRegionalBusCisCacheDto";

const regionalBusCisCacheDtoSchema: JSONSchemaType<IRegionalBusCisCacheDto> = {
    $schema: "http://json-schema.org/draft-04/schema#",
    title: "RegionalBusCisCacheDto",
    type: "object",
    properties: {
        cis_line_id: {
            type: "string",
        },
        cis_trip_number: {
            type: "number",
        },
        registration_number: {
            type: "number",
        },
    },
    required: ["cis_line_id", "cis_trip_number", "registration_number"],
};
export { regionalBusCisCacheDtoSchema as RegionalBusCisCacheDtoSchema };
