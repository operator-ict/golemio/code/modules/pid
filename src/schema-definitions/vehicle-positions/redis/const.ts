export const GTFS_DELAY_COMPUTATION_NAMESPACE_PREFIX = "gtfsTripDelayComputation";
export const PUBLIC_CACHE_NAMESPACE_PREFIX = "vpPublicCache";
export const PUBLIC_STOP_TIME_CACHE_NAMESPACE_PREFIX = "vpPublicStopTimeCache";
