import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IMetroRunsMessagesModel } from "./interfaces/IMetroRunsMessagesModel";

export class MetroRunsMessagesModel extends Model<MetroRunsMessagesModel> implements IMetroRunsMessagesModel {
    public static TABLE_NAME = "vehiclepositions_metro_runs_messages";

    declare route_name: string;
    declare message_timestamp: Date;
    declare train_set_number_scheduled: string;
    declare train_set_number_real: string;
    declare train_number: string;
    declare track_id: string;
    declare delay_origin: number;
    declare actual_position_timestamp_scheduled: Date;

    public static attributeModel: ModelAttributes<MetroRunsMessagesModel> = {
        route_name: {
            type: DataTypes.CHAR(1),
            primaryKey: true,
        },
        message_timestamp: {
            type: DataTypes.DATE,
            primaryKey: true,
        },
        train_set_number_scheduled: {
            type: DataTypes.STRING(15),
            allowNull: false,
        },
        train_set_number_real: {
            type: DataTypes.STRING(15),
            allowNull: false,
        },
        train_number: {
            type: DataTypes.STRING(15),
            primaryKey: true,
        },
        track_id: {
            type: DataTypes.STRING(15),
            primaryKey: true,
        },
        delay_origin: {
            type: DataTypes.SMALLINT,
            allowNull: false,
        },
        actual_position_timestamp_scheduled: {
            type: DataTypes.DATE,
            allowNull: false,
        },
    };

    public static arrayJsonSchema: JSONSchemaType<IMetroRunsMessagesModel[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                route_name: {
                    type: "string",
                },
                message_timestamp: {
                    type: "object",
                    required: ["toISOString"],
                },
                train_set_number_scheduled: {
                    type: "string",
                },
                train_set_number_real: {
                    type: "string",
                },
                train_number: {
                    type: "string",
                },
                track_id: {
                    type: "string",
                },
                delay_origin: {
                    type: "integer",
                },
                actual_position_timestamp_scheduled: {
                    type: "object",
                    required: ["toISOString"],
                },
            },
            required: [
                "route_name",
                "message_timestamp",
                "train_set_number_scheduled",
                "train_set_number_real",
                "train_number",
                "track_id",
                "delay_origin",
                "actual_position_timestamp_scheduled",
            ],
        },
    };
}
