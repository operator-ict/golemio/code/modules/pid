import { SharedSchemaProvider } from "@golemio/core/dist/schema-definitions";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { Point } from "@golemio/core/dist/shared/geojson";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IRegionalBusRunsMessagesModel } from "./interfaces/IRegionalBusRunsMessagesModel";

export class RegionalBusRunsMessagesModel extends Model<RegionalBusRunsMessagesModel> implements IRegionalBusRunsMessagesModel {
    public static TABLE_NAME = "vehiclepositions_regional_bus_runs_messages";

    declare external_trip_id: string;
    declare cis_line_id: string | null;
    declare cis_trip_number: number | null;
    declare events: string;
    declare coordinates: Point;
    declare vehicle_timestamp: Date;
    declare registration_number: number | null;
    declare speed_kmh: number;
    declare bearing: number;
    declare is_terminated: boolean;

    public static attributeModel: ModelAttributes<RegionalBusRunsMessagesModel> = {
        external_trip_id: {
            type: DataTypes.STRING(100),
            primaryKey: true,
        },
        cis_line_id: {
            type: DataTypes.STRING(10),
        },
        cis_trip_number: {
            type: DataTypes.INTEGER,
        },
        events: {
            type: DataTypes.STRING(10),
            allowNull: false,
        },
        coordinates: {
            type: DataTypes.GEOMETRY,
            allowNull: false,
        },
        vehicle_timestamp: {
            type: DataTypes.DATE,
            primaryKey: true,
        },
        registration_number: {
            type: DataTypes.INTEGER,
        },
        speed_kmh: {
            type: DataTypes.SMALLINT,
            allowNull: false,
        },
        bearing: {
            type: DataTypes.SMALLINT,
            allowNull: false,
        },
        is_terminated: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
        },
    };

    public static arrayJsonSchema: JSONSchemaType<IRegionalBusRunsMessagesModel[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                external_trip_id: {
                    type: "string",
                },
                cis_line_id: {
                    type: ["null", "string"],
                },
                cis_trip_number: {
                    type: ["null", "number"],
                },
                events: {
                    type: "string",
                },
                coordinates: {
                    $ref: "#/definitions/geometry",
                },
                vehicle_timestamp: {
                    type: "object",
                    required: ["toISOString"],
                },
                registration_number: {
                    type: ["null", "integer"],
                },
                speed_kmh: {
                    type: "integer",
                },
                bearing: {
                    type: "integer",
                },
                is_terminated: {
                    type: "boolean",
                },
            },
            required: ["external_trip_id", "events", "coordinates", "vehicle_timestamp", "speed_kmh", "bearing", "is_terminated"],
        },
        definitions: {
            // @ts-expect-error
            geometry: SharedSchemaProvider.Geometry,
        },
    };
}
