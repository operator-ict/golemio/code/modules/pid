import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { ICisStopDto } from "./interfaces/ICisStopDto";

export class CisStopModel extends Model<CisStopModel> implements ICisStopDto {
    public static tableName = "vehiclepositions_cis_stops";

    declare rt_trip_id: string;
    declare cis_stop_group_id: number;
    declare cis_stop_platform_code: string | null;

    public static attributeModel: ModelAttributes<CisStopModel> = {
        rt_trip_id: {
            type: DataTypes.STRING(255),
            primaryKey: true,
        },
        cis_stop_group_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },
        cis_stop_platform_code: {
            type: DataTypes.STRING(15),
            allowNull: true,
        },
    };

    public static arrayJsonSchema: JSONSchemaType<ICisStopDto[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                rt_trip_id: {
                    type: "string",
                },
                cis_stop_group_id: {
                    type: "integer",
                },
                cis_stop_platform_code: {
                    oneOf: [{ type: "string" }, { type: "null", nullable: true }],
                },
            },
            required: ["rt_trip_id", "cis_stop_group_id", "cis_stop_platform_code"],
        },
    };
}
