import { SharedSchemaProvider } from "@golemio/core/dist/schema-definitions";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";
import { IVPTripsModel } from "./interfaces/IVPTripsModel";
import { IVPTripsLastPositionContext } from "./interfaces/VPTripsLastPositionInterfaces";

export class VPTripsModel extends Model<VPTripsModel> implements IVPTripsModel {
    public static TABLE_NAME = "vehiclepositions_trips";
    public static TABLE_NAME_HISTORY = "vehiclepositions_trips_history";

    declare cis_line_id: string;
    declare cis_trip_number: number;
    declare run_number: number;
    declare internal_run_number: number | null;
    declare cis_line_short_name: string;
    declare gtfs_route_id: string;
    declare gtfs_route_short_name: string;
    declare gtfs_trip_id: string;
    declare gtfs_date: string | null;
    declare gtfs_direction_id: number | null;
    declare gtfs_shape_id: string | null;
    declare id: string;
    declare start_cis_stop_id: number;
    declare start_cis_stop_platform_code: string;
    declare start_time: string;
    declare start_timestamp: Date;
    declare vehicle_type_id: number | null;
    declare wheelchair_accessible: boolean;
    declare agency_name_scheduled: string;
    declare origin_route_name: string;
    declare internal_route_name: string | null;
    declare agency_name_real: string;
    declare vehicle_registration_number: number;
    declare gtfs_trip_headsign: string;
    declare start_asw_stop_id: string;
    declare gtfs_route_type: GTFSRouteTypeEnum;
    declare gtfs_block_id: string;
    declare last_position_id: string;
    declare is_canceled: boolean;
    declare end_timestamp: Date;
    declare gtfs_trip_short_name: string;
    declare last_position_context: IVPTripsLastPositionContext;
    declare provider_source_type: string;

    public static attributeModel: ModelAttributes<VPTripsModel> = {
        agency_name_real: DataTypes.STRING, // doprSkut "Jaroslav Štěpánek"
        agency_name_scheduled: DataTypes.STRING, // dopr "Jaroslav Štěpánek"
        cis_line_id: DataTypes.STRING, // lin "100155", "X7"
        cis_line_short_name: DataTypes.STRING, // alias "155", "X7"
        cis_trip_number: DataTypes.INTEGER, // spoj "40"
        gtfs_block_id: DataTypes.STRING, // 1345_1573_201213
        gtfs_route_id: DataTypes.STRING,
        gtfs_route_short_name: DataTypes.STRING,
        gtfs_route_type: DataTypes.INTEGER,
        gtfs_trip_headsign: DataTypes.STRING,
        gtfs_trip_short_name: DataTypes.STRING,
        gtfs_trip_id: DataTypes.STRING,
        gtfs_date: DataTypes.DATEONLY,
        gtfs_direction_id: DataTypes.SMALLINT,
        gtfs_shape_id: DataTypes.STRING(15),
        // ⬐ hash(start_timestamp, cis_id, cis_short_name, cis_number);
        id: {
            primaryKey: true,
            type: DataTypes.STRING,
        },
        is_canceled: DataTypes.BOOLEAN, // zrus "true"
        last_position_id: DataTypes.BIGINT,
        last_position_context: DataTypes.JSON,
        origin_route_name: DataTypes.STRING, // kmenl "110"
        run_number: DataTypes.INTEGER, // po "1"
        start_asw_stop_id: DataTypes.STRING, // zast[0].zast "1234/1"
        start_cis_stop_id: DataTypes.INTEGER, // zast[0].zast "57517"
        start_cis_stop_platform_code: DataTypes.STRING, // zast[0].stan "C"
        start_time: DataTypes.TIME, // zast[0].prij "" || "11:00"
        // ⬐ Převod příjezdu první zastávky na date (pozor, čas 23:59 který přišel v 0:01 bude mít včerejší datum!)
        start_timestamp: DataTypes.DATE, // zast[0].prij "" || "11:00" --- po nalezení gtfs_trip_id nahrazeno JŘ
        end_timestamp: DataTypes.DATE, // --- po nalezení gtfs_trip_id vyplněno z JŘ
        vehicle_registration_number: DataTypes.INTEGER, // vuzevc "1030"
        vehicle_type_id: DataTypes.INTEGER, // t "3"
        wheelchair_accessible: DataTypes.BOOLEAN, // np "true"
        internal_route_name: DataTypes.STRING(50),
        internal_run_number: DataTypes.INTEGER,
        provider_source_type: DataTypes.STRING(1),
    };

    public static jsonSchema: JSONSchemaType<IVPTripsModel | IVPTripsModel[]> = {
        oneOf: [
            {
                type: "array",
                items: {
                    $ref: "#/definitions/trip",
                },
            },
            {
                $ref: "#/definitions/trip",
            },
        ],
        definitions: {
            // @ts-expect-error
            geometry: SharedSchemaProvider.Geometry,
            // @ts-expect-error JSONSchemaType does not like type pairings in definitions
            trip: {
                title: "trip",
                description: "Vehicle Positions Trip",
                type: "object",
                properties: {
                    cis_line_id: {
                        type: ["string", "null"],
                    },
                    cis_trip_number: {
                        type: ["number", "null"],
                    },
                    run_number: {
                        type: ["number", "null"],
                    },
                    internal_run_number: {
                        type: ["number", "null"],
                    },
                    cis_line_short_name: {
                        type: ["string", "null"],
                    },
                    gtfs_route_id: {
                        type: ["string", "null"],
                    },
                    gtfs_route_short_name: {
                        type: ["string", "null"],
                    },
                    gtfs_trip_id: {
                        type: ["string", "null"],
                    },
                    gtfs_date: {
                        type: ["string", "null"],
                    },
                    gtfs_direction_id: {
                        type: ["number", "null"],
                    },
                    gtfs_shape_id: {
                        type: ["string", "null"],
                    },
                    id: {
                        type: "string",
                    },
                    start_cis_stop_id: {
                        type: ["number", "null"],
                    },
                    start_cis_stop_platform_code: {
                        type: ["string", "null"],
                    },
                    start_time: {
                        type: ["string", "null"],
                    },
                    start_timestamp: {
                        type: ["object", "null"],
                        required: ["toISOString"],
                    },
                    vehicle_type_id: {
                        type: "number",
                    },
                    wheelchair_accessible: {
                        type: ["boolean", "null"],
                    },
                    agency_name_scheduled: {
                        type: ["string", "null"],
                    },
                    origin_route_name: {
                        type: ["string", "null"],
                    },
                    internal_route_name: {
                        type: ["string", "null"],
                    },
                    agency_name_real: {
                        type: ["string", "null"],
                    },
                    vehicle_registration_number: {
                        type: ["number", "null"],
                    },
                    gtfs_trip_headsign: {
                        type: ["string", "null"],
                    },
                    start_asw_stop_id: {
                        type: ["string", "null"],
                    },
                    gtfs_route_type: {
                        type: ["string", "number", "null"],
                    },
                    gtfs_block_id: {
                        type: ["string", "null"],
                    },
                    last_position_id: {
                        type: ["string", "null"],
                    },
                    is_canceled: {
                        type: ["boolean", "null"],
                    },
                    end_timestamp: {
                        type: ["object", "null"],
                        required: ["toISOString"],
                    },
                    gtfs_trip_short_name: {
                        type: ["string", "null"],
                    },
                    last_position_context: {
                        oneOf: [
                            {
                                $ref: "#/definitions/context",
                            },
                            {
                                type: "null",
                            },
                        ],
                    },
                    provider_source_type: {
                        type: ["string"],
                    },
                },
                required: ["id"],
            },
            // @ts-expect-error JSONSchemaType does not like type pairings in definitions
            context: {
                title: "context",
                description: "Last position's context",
                type: "object",
                properties: {
                    lastPositionId: {
                        type: ["string", "null"],
                    },
                    lastPositionLat: {
                        type: ["number", "null"],
                    },
                    lastPositionLng: {
                        type: ["number", "null"],
                    },
                    lastStopSequence: {
                        type: ["number", "null"],
                    },
                    lastPositionOriginTimestamp: {
                        type: ["number", "null"],
                    },
                    lastPositionTracking: {
                        oneOf: [
                            {
                                type: "object",
                                properties: {
                                    type: {
                                        type: "string",
                                        enum: ["Feature"],
                                    },
                                    properties: {
                                        type: "object",
                                        // TODO add properties after creating shared schema for VP positions
                                    },
                                    geometry: {
                                        $ref: "#/definitions/geometry",
                                    },
                                },
                                required: ["type", "properties", "geometry"],
                            },
                            {
                                type: "null",
                            },
                        ],
                    },
                    lastPositionCanceled: {
                        type: ["boolean", "null"],
                    },
                    lastPositionLastStop: {
                        type: "object",
                        properties: {
                            id: {
                                type: ["string", "null"],
                            },
                            sequence: {
                                type: ["number", "null"],
                            },
                            arrival_time: {
                                type: ["number", "null"],
                            },
                            departure_time: {
                                type: ["number", "null"],
                            },
                        },
                        required: [],
                    },
                    lastPositionDelay: {
                        type: ["number", "null"],
                    },
                    atStopStreak: {
                        type: "object",
                        properties: {
                            firstPositionTimestamp: {
                                type: ["number", "null"],
                            },
                            firstPositionDelay: {
                                type: ["number", "null"],
                            },
                            stop_sequence: {
                                type: ["number", "null"],
                            },
                        },
                        required: [],
                    },
                    lastPositionBeforeTrackDelayed: {
                        oneOf: [
                            {
                                type: "object",
                                properties: {
                                    delay: {
                                        oneOf: [
                                            {
                                                type: "number",
                                            },
                                            {
                                                type: "null",
                                                nullable: true,
                                            },
                                        ],
                                    },
                                    origin_timestamp: {
                                        oneOf: [
                                            {
                                                type: "object",
                                                required: ["toISOString"],
                                            },
                                            {
                                                type: "string",
                                            },
                                            {
                                                type: "null",
                                                nullable: true,
                                            },
                                        ],
                                    },
                                },
                            },
                            {
                                type: "null",
                                nullable: true,
                            },
                        ],
                    },
                    lastPositionState: {
                        oneOf: [
                            {
                                type: "string",
                            },
                            {
                                type: "null",
                                nullable: true,
                            },
                        ],
                    },
                    lastPositionStateChange: {
                        oneOf: [
                            {
                                type: "string",
                                format: "date-time",
                            },
                            {
                                type: "null",
                                nullable: true,
                            },
                        ],
                    },
                    tripId: {
                        type: "string",
                    },
                },
                required: ["lastPositionLastStop", "atStopStreak", "tripId"],
            },
        },
    };
}
