import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { TCPEventEnum } from "src/const";
import { ICommonRunsMessagesModel } from "./interfaces/ICommonRunsMessagesModel";

export class CommonRunsMessagesModel extends Model<CommonRunsMessagesModel> implements ICommonRunsMessagesModel {
    public static TABLE_NAME = "vehiclepositions_runs_messages";

    declare id?: number; // - autoincrement
    declare runs_id?: string; // - associated fk
    declare lat: number;
    declare lng: number;
    declare actual_stop_asw_id: string;
    declare actual_stop_timestamp_real: Date;
    declare actual_stop_timestamp_scheduled?: Date;
    declare last_stop_asw_id: string;
    declare packet_number: string;
    declare msg_timestamp: Date;
    declare events: TCPEventEnum;

    public static attributeModel: ModelAttributes<CommonRunsMessagesModel> = {
        id: {
            primaryKey: true,
            type: DataTypes.BIGINT,
            autoIncrement: true,
        },
        runs_id: DataTypes.STRING,
        lat: DataTypes.DECIMAL,
        lng: DataTypes.DECIMAL,
        actual_stop_asw_id: DataTypes.STRING,
        actual_stop_timestamp_real: DataTypes.DATE,
        actual_stop_timestamp_scheduled: DataTypes.DATE,
        last_stop_asw_id: DataTypes.STRING,
        packet_number: DataTypes.STRING,
        msg_timestamp: DataTypes.DATE,
        events: DataTypes.STRING,
    };

    public static arrayJsonSchema: JSONSchemaType<ICommonRunsMessagesModel[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                id: {
                    type: "integer",
                    nullable: true,
                },
                runs_id: {
                    type: "string",
                    nullable: true,
                },
                lat: {
                    type: "number",
                },
                lng: {
                    type: "number",
                },
                actual_stop_asw_id: {
                    type: "string",
                    nullable: true,
                },
                actual_stop_timestamp_real: {
                    type: "object",
                    required: ["toUTCString"],
                },
                actual_stop_timestamp_scheduled: {
                    type: "object",
                    required: ["toUTCString"],
                    nullable: true,
                },
                last_stop_asw_id: {
                    type: "string",
                    nullable: true,
                },
                packet_number: {
                    type: "string",
                },
                msg_timestamp: {
                    type: "object",
                    required: ["toUTCString"],
                },
                events: {
                    type: "string",
                },
            },
            required: ["lat", "lng", "actual_stop_timestamp_real", "packet_number", "msg_timestamp", "events"],
        },
    };
}
