import { StatePositionEnum } from "src/const";
import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";

export interface ITripWithLastPositionDto {
    id: string;
    agency_name_real: string;
    agency_name_scheduled: string;
    cis_line_id: string;
    cis_trip_number: number;
    gtfs_route_id: string;
    gtfs_route_short_name: string;
    gtfs_route_type: GTFSRouteTypeEnum;
    gtfs_trip_headsign: string;
    gtfs_trip_short_name: string;
    gtfs_trip_id: string | null;
    origin_route_name: string;
    run_number: number;
    vehicle_registration_number: number;
    vehicle_type_id: number;
    wheelchair_accessible: boolean;
    updated_at: Date;
    start_timestamp_isostring: string;
    bearing: number;
    delay: number;
    delay_stop_arrival: number;
    delay_stop_departure: number;
    is_canceled: boolean;
    last_stop_id: string;
    last_stop_sequence: number;
    last_stop_headsign: string;
    lat: number | null;
    lng: number | null;
    next_stop_id: string;
    next_stop_sequence: number;
    shape_dist_traveled: number;
    speed: number;
    state_position: StatePositionEnum;
    is_tracked: boolean;
    last_stop_arrival_time_isostring: string;
    last_stop_departure_time_isostring: string;
    next_stop_arrival_time_isostring: string;
    next_stop_departure_time_isostring: string;
    origin_timestamp_isostring: string;
}
