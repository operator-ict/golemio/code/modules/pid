import { StatePositionEnum } from "src/const";

export interface IProcessedPositionDto {
    trips_id: string;
    bearing: number;
    delay: number;
    delay_stop_arrival: number;
    delay_stop_departure: number;
    is_canceled: boolean;
    last_stop_arrival_time_isostring: string;
    last_stop_departure_time_isostring: string;
    last_stop_id: string;
    last_stop_sequence: number;
    lat: number | null;
    lng: number | null;
    next_stop_arrival_time_isostring: string;
    next_stop_departure_time_isostring: string;
    next_stop_id: string;
    next_stop_sequence: number;
    origin_timestamp_isostring: string;
    shape_dist_traveled: number;
    speed: number;
    state_position: StatePositionEnum;
}
