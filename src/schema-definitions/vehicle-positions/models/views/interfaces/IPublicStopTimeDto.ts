import { GTFSRouteTypeEnum } from "#helpers/RouteTypeEnums";
import { ProviderSourceTypeEnum } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/ProviderSourceTypeEnum";

export interface IPublicStopTimeDto {
    rt_trip_id: string;
    gtfs_trip_id: string;
    stop_sequence: number;
    stop_arr_delay: number | null;
    stop_dep_delay: number | null;
    gtfs_route_type: GTFSRouteTypeEnum | null;
    gtfs_route_short_name: string | null;
    run_number: number | null;
    internal_run_number: number | null;
    provider_source_type: ProviderSourceTypeEnum;
    cis_trip_number: number | null;
    vehicle_registration_number: number | null;
    cis_stop_platform_code: string | null;
    platform_code: string | null;
    stop_id: string;
}
