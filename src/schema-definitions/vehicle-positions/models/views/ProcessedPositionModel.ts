import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { StatePositionEnum } from "src/const";
import { IProcessedPositionDto } from "./interfaces/IProcessedPositionDto";

export class ProcessedPositionModel extends Model<ProcessedPositionModel> implements IProcessedPositionDto {
    public static tableName = "v_vehiclepositions_all_processed_positions";

    declare trips_id: string;
    declare bearing: number;
    declare delay: number;
    declare delay_stop_arrival: number;
    declare delay_stop_departure: number;
    declare is_canceled: boolean;
    declare last_stop_arrival_time_isostring: string;
    declare last_stop_departure_time_isostring: string;
    declare last_stop_id: string;
    declare last_stop_sequence: number;
    declare lat: number | null;
    declare lng: number | null;
    declare next_stop_arrival_time_isostring: string;
    declare next_stop_departure_time_isostring: string;
    declare next_stop_id: string;
    declare next_stop_sequence: number;
    declare origin_timestamp_isostring: string;
    declare shape_dist_traveled: number;
    declare speed: number;
    declare state_position: StatePositionEnum;

    public static attributeModel: ModelAttributes<ProcessedPositionModel, IProcessedPositionDto> = {
        trips_id: {
            type: DataTypes.STRING,
            primaryKey: true,
        },
        bearing: DataTypes.INTEGER,
        delay: DataTypes.INTEGER,
        delay_stop_arrival: DataTypes.INTEGER,
        delay_stop_departure: DataTypes.INTEGER,
        is_canceled: DataTypes.BOOLEAN,
        last_stop_arrival_time_isostring: DataTypes.STRING,
        last_stop_departure_time_isostring: DataTypes.STRING,
        last_stop_id: DataTypes.STRING,
        last_stop_sequence: DataTypes.INTEGER,
        lat: DataTypes.DECIMAL,
        lng: DataTypes.DECIMAL,
        next_stop_arrival_time_isostring: DataTypes.STRING,
        next_stop_departure_time_isostring: DataTypes.STRING,
        next_stop_id: DataTypes.STRING,
        next_stop_sequence: DataTypes.INTEGER,
        origin_timestamp_isostring: {
            type: DataTypes.STRING,
            primaryKey: true,
        },
        shape_dist_traveled: DataTypes.FLOAT,
        speed: DataTypes.INTEGER,
        state_position: DataTypes.INTEGER,
    };
}
