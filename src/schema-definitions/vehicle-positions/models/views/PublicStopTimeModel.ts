import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IPublicStopTimeDto } from "./interfaces/IPublicStopTimeDto";
import { GTFSRouteTypeEnum } from "#helpers/RouteTypeEnums";
import { ProviderSourceTypeEnum } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/ProviderSourceTypeEnum";

export class PublicStopTimeModel extends Model<PublicStopTimeModel> implements IPublicStopTimeDto {
    public static tableName = "v_public_vehiclepositions_combined_stop_times";

    declare rt_trip_id: string;
    declare gtfs_trip_id: string;
    declare stop_sequence: number;
    declare stop_arr_delay: number | null;
    declare stop_dep_delay: number | null;
    declare gtfs_route_type: GTFSRouteTypeEnum | null;
    declare gtfs_route_short_name: string | null;
    declare run_number: number | null;
    declare internal_run_number: number | null;
    declare provider_source_type: ProviderSourceTypeEnum;
    declare cis_trip_number: number | null;
    declare vehicle_registration_number: number | null;
    declare cis_stop_platform_code: string | null;
    declare platform_code: string | null;
    declare stop_id: string;

    public static attributeModel: ModelAttributes<PublicStopTimeModel> = {
        rt_trip_id: {
            type: DataTypes.STRING,
            allowNull: false,
            primaryKey: true,
        },
        gtfs_trip_id: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        stop_sequence: {
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true,
        },
        stop_arr_delay: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        stop_dep_delay: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        gtfs_route_type: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        gtfs_route_short_name: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        run_number: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        internal_run_number: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        provider_source_type: {
            type: DataTypes.STRING(1),
            allowNull: false,
        },
        cis_trip_number: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        vehicle_registration_number: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        cis_stop_platform_code: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        platform_code: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        stop_id: {
            type: DataTypes.STRING,
            allowNull: false,
        },
    };
}
