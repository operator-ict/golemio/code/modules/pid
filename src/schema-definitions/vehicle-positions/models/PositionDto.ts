import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { StatePositionEnum, StateProcessEnum, TCPEventEnum } from "src/const";
import { IPositionDto } from "./interfaces/IPositionDto";

export class PositionDto extends Model implements IPositionDto {
    declare id: string;
    declare asw_last_stop_id: string | null;
    declare bearing: number | null;
    declare cis_last_stop_id: number | null;
    declare cis_last_stop_sequence: number | null;
    declare delay: number;
    declare delay_stop_arrival: number | null;
    declare delay_stop_departure: number | null;
    declare is_canceled: boolean;
    declare last_stop_arrival_time: Date;
    declare last_stop_departure_time: Date;
    declare last_stop_id: string;
    declare last_stop_sequence: number;
    declare lat: number | null;
    declare lng: number | null;
    declare next_stop_arrival_time: Date;
    declare next_stop_departure_time: Date;
    declare next_stop_id: string;
    declare next_stop_sequence: number;
    declare origin_time: string;
    declare origin_timestamp: Date;
    declare shape_dist_traveled: number;
    declare speed: number;
    declare state_position: StatePositionEnum;
    declare state_process: StateProcessEnum;
    declare this_stop_id: string | null;
    declare this_stop_sequence: number | null;
    declare is_tracked: boolean | null;
    declare trips_id: string | null;
    declare tcp_event: TCPEventEnum | null;
    declare last_stop_headsign: string | null;
    declare last_stop_name: string | null;
    declare valid_to: Date;
    declare scheduled_timestamp: Date | null;
    declare origin_position_id: string | null;

    public static attributeModel: ModelAttributes<PositionDto, IPositionDto> = {
        asw_last_stop_id: DataTypes.STRING, // zast "28010"
        bearing: DataTypes.INTEGER, // azimut "0"
        cis_last_stop_id: DataTypes.INTEGER, // zast "28010"
        cis_last_stop_sequence: DataTypes.INTEGER, // stop_sequence
        delay: DataTypes.INTEGER,
        delay_stop_arrival: { type: DataTypes.INTEGER }, // zpoz_prij "426"
        delay_stop_departure: DataTypes.INTEGER, // zpoz_odj "426"
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.BIGINT,
        },
        is_canceled: DataTypes.BOOLEAN, // zrus "false"
        last_stop_arrival_time: DataTypes.DATE,
        last_stop_departure_time: DataTypes.DATE,
        last_stop_id: DataTypes.STRING,
        last_stop_sequence: DataTypes.INTEGER,
        lat: DataTypes.DECIMAL, // lat "50.08323"
        lng: DataTypes.DECIMAL, // lng "14.51035"
        next_stop_arrival_time: DataTypes.DATE,
        next_stop_departure_time: DataTypes.DATE,
        next_stop_id: DataTypes.STRING,
        next_stop_sequence: DataTypes.INTEGER,
        origin_time: DataTypes.TIME, // cpoz "11:09:06"
        origin_timestamp: DataTypes.DATE, // cpoz "11:09:06"
        shape_dist_traveled: DataTypes.DECIMAL,
        speed: DataTypes.INTEGER, // rychl "0"
        state_position: DataTypes.STRING,
        state_process: DataTypes.STRING,
        this_stop_id: DataTypes.STRING,
        this_stop_sequence: DataTypes.INTEGER,
        is_tracked: DataTypes.BOOLEAN,
        // ⬐ hash(start_timestamp, cis_id, cis_short_name, cis_number);
        trips_id: { type: DataTypes.STRING },
        tcp_event: { type: DataTypes.STRING },
        last_stop_headsign: { type: DataTypes.STRING },
        last_stop_name: { type: DataTypes.STRING },
        valid_to: DataTypes.DATE,
        scheduled_timestamp: DataTypes.DATE,
        origin_position_id: DataTypes.STRING,
    };

    public static jsonSchema: JSONSchemaType<IPositionDto[]> = {
        $schema: "http://json-schema.org/draft-04/schema#",
        type: "array",
        items: {
            type: "object",
            properties: {
                id: { type: "string" },
                asw_last_stop_id: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                bearing: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                cis_last_stop_id: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                cis_last_stop_sequence: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                delay: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                delay_stop_arrival: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                delay_stop_departure: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                is_canceled: { oneOf: [{ type: "boolean" }, { type: "null", nullable: true }] },
                last_stop_arrival_time: {
                    oneOf: [
                        { type: "object", required: ["toISOString"] },
                        { type: "null", nullable: true },
                    ],
                },
                last_stop_departure_time: {
                    oneOf: [
                        { type: "object", required: ["toISOString"] },
                        { type: "null", nullable: true },
                    ],
                },
                last_stop_id: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                last_stop_sequence: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                lat: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                lng: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                next_stop_arrival_time: {
                    oneOf: [
                        { type: "object", required: ["toISOString"] },
                        { type: "null", nullable: true },
                    ],
                },
                next_stop_departure_time: {
                    oneOf: [
                        { type: "object", required: ["toISOString"] },
                        { type: "null", nullable: true },
                    ],
                },
                next_stop_id: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                next_stop_sequence: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                origin_time: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                origin_timestamp: {
                    oneOf: [
                        { type: "object", required: ["toISOString"] },
                        { type: "string", format: "date-time" },
                        { type: "null", nullable: true },
                    ],
                },
                shape_dist_traveled: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                speed: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                state_position: { type: "string" },
                state_process: { type: "string" },
                this_stop_id: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                this_stop_sequence: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                is_tracked: { oneOf: [{ type: "boolean" }, { type: "null", nullable: true }] },
                trips_id: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                tcp_event: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                last_stop_headsign: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                last_stop_name: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                valid_to: {
                    oneOf: [
                        { type: "object", required: ["toISOString"] },
                        { type: "null", nullable: true },
                    ],
                },
                scheduled_timestamp: {
                    oneOf: [
                        { type: "object", required: ["toISOString"] },
                        { type: "null", nullable: true },
                    ],
                },
                origin_position_id: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            },
            required: ["state_position", "state_process"],
        },
    };
}
