import { Point } from "@golemio/core/dist/shared/geojson";

export interface IRegionalBusRunsMessagesModel {
    external_trip_id: string;
    cis_line_id: string | null;
    cis_trip_number: number | null;
    events: string;
    coordinates: Point;
    vehicle_timestamp: Date | string;
    registration_number: number | null;
    speed_kmh: number;
    bearing: number;
    is_terminated: boolean;
    timestamp?: string;
}
