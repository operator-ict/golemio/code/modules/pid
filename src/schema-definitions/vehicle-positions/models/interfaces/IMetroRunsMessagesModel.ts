export interface IMetroRunsMessagesModel {
    route_name: string;
    message_timestamp: Date;
    train_set_number_scheduled: string;
    train_set_number_real: string;
    train_number: string;
    track_id: string;
    delay_origin: number;
    actual_position_timestamp_scheduled: Date;
}
