export interface ICisStopDto {
    rt_trip_id: string;
    cis_stop_group_id: number;
    cis_stop_platform_code: string | null;
}
