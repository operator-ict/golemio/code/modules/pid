export interface IV1PositionDto {
    bearing: number;
    cis_last_stop_id: number | null;
    cis_last_stop_sequence: number | null;
    delay: number | null;
    delay_stop_arrival: number | null;
    delay_stop_departure: number | null;
    gtfs_last_stop_id: string;
    gtfs_last_stop_sequence: number | null;
    gtfs_next_stop_id: string;
    gtfs_next_stop_sequence: number | null;
    gtfs_shape_dist_traveled: number | null;
    is_canceled: boolean;
    lat: number;
    lng: number;
    origin_time: string;
    origin_timestamp: number;
    speed: number | null;
    tracking: number;
    trips_id: string;
}
