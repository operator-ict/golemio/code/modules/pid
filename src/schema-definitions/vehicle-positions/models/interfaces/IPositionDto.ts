import { StatePositionEnum, StateProcessEnum, TCPEventEnum } from "src/const";

export interface IPositionDto {
    id: string;
    asw_last_stop_id: string | null;
    bearing: number | null;
    cis_last_stop_id: number | null;
    cis_last_stop_sequence: number | null;
    delay: number | null;
    delay_stop_arrival: number | null;
    delay_stop_departure: number | null;
    is_canceled: boolean | null;
    last_stop_arrival_time: Date | null;
    last_stop_departure_time: Date | null;
    last_stop_id: string | null;
    last_stop_sequence: number | null;
    lat: number | null;
    lng: number | null;
    next_stop_arrival_time: Date | null;
    next_stop_departure_time: Date | null;
    next_stop_id: string | null;
    next_stop_sequence: number | null;
    origin_time: string | null;
    origin_timestamp: Date | string | null;
    shape_dist_traveled: number | null;
    speed: number | null;
    state_position: StatePositionEnum;
    state_process: StateProcessEnum;
    this_stop_id: string | null;
    this_stop_sequence: number | null;
    is_tracked: boolean | null;
    trips_id: string | null;
    tcp_event: TCPEventEnum | null;
    last_stop_headsign: string | null;
    last_stop_name: string | null;
    valid_to: Date | null;
    scheduled_timestamp: Date | null;
    origin_position_id: string | null;
}
