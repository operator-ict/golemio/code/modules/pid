import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";
import { ProviderSourceTypeEnum } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/ProviderSourceTypeEnum";
import { StatePositionEnum } from "src/const";

export interface IGtfsRtTripDto {
    last_position: {
        is_canceled: boolean | null;
        origin_timestamp: Date;
        last_stop_sequence: number | null;
        bearing: number | null;
        lat: number;
        lng: number;
        speed: number | null;
        delay: number | null;
        state_position: StatePositionEnum;
    };
    stop_times: Array<{
        stop_id: string;
        platform_code: string | null;
        cis_stop_platform_code: string | null;
        stop_sequence: number;
        arrival_delay_seconds: number | null;
        departure_delay_seconds: number | null;
    }>;
    id: string;
    run_number: number | null;
    internal_run_number: number | null;
    start_timestamp: string;
    cis_line_id: string | null;
    cis_trip_number: number | null;
    start_time: string | null;
    vehicle_registration_number: number | null;
    gtfs_trip_id: string;
    gtfs_route_id: string | null;
    gtfs_route_type: GTFSRouteTypeEnum;
    gtfs_route_short_name: string | null;
    provider_source_type: ProviderSourceTypeEnum;
    wheelchair_accessible: boolean | null;
    vehicle_descriptor?: {
        is_air_conditioned: boolean | null;
        has_usb_chargers: boolean | null;
    };
}
