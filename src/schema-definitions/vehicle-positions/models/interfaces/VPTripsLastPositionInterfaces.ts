import { Feature, Point } from "@turf/turf";
import { IVPTripsPositionAttributes } from "./IVPTripsPositionAttributes";
import { StatePositionEnum } from "src/const";

export interface IVPTripsComputedPositionAtStopStreak {
    firstPositionTimestamp: number | null;
    firstPositionDelay: number | null;
    stop_sequence: number | null;
}

export interface IVPTripsLastPositionContext {
    lastPositionId: string | null;
    lastPositionLat?: number | null;
    lastPositionLng?: number | null;
    lastPositionOriginTimestamp: number | null;
    lastPositionTracking: Feature<Point, IVPTripsPositionAttributes> | null;
    lastPositionCanceled: boolean | null;
    lastPositionLastStop: {
        id: string | null;
        sequence: number | null;
        arrival_time: number | null;
        arrival_delay: number | null;
        departure_time: number | null;
        departure_delay: number | null;
    };
    lastPositionDelay: number | null;
    atStopStreak: IVPTripsComputedPositionAtStopStreak;
    lastPositionBeforeTrackDelayed: {
        delay: number | null;
        origin_timestamp: Date;
    } | null;
    lastPositionState: StatePositionEnum | null;
    lastStopSequence?: number | null;
    lastPositionStateChange: string | null;
    tripId: string;
}
