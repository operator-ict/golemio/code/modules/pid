export interface ICommonRunsModel {
    id: string; // - "route_id"_"run_number"_"registration_number"
    route_id: string;
    run_number: number;
    line_short_name: string;
    registration_number: string;
    msg_start_timestamp: string;
    msg_last_timestamp: string;
    wheelchair_accessible: boolean;
}
