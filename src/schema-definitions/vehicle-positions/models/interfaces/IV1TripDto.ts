export interface IV1TripsDto {
    cis_agency_name: string | null;
    cis_id: string;
    cis_number: number;
    cis_order: number | null;
    cis_parent_route_name: number | null;
    cis_real_agency_name: string | null;
    cis_short_name: string;
    cis_vehicle_registration_number: number | null;
    gtfs_route_id: string | null;
    gtfs_route_short_name: string | null;
    gtfs_trip_id: string | null;
    id: string;
    start_cis_stop_id: number | null;
    start_cis_stop_platform_code: string | null;
    start_time: string;
    start_timestamp: number;
    vehicle_type: number | null;
    wheelchair_accessible: boolean | null;
}
