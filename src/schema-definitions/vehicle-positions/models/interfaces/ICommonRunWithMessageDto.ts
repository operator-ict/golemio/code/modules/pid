import { ICommonRunsMessagesModel } from "./ICommonRunsMessagesModel";
import { ICommonRunsModel } from "./ICommonRunsModel";

export interface ICommonRunWithMessageDto {
    run: ICommonRunsModel;
    run_message: ICommonRunsMessagesModel;
}
