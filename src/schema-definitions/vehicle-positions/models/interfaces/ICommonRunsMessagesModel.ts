import { TCPEventEnum } from "src/const";

export interface ICommonRunsMessagesModel {
    id?: number; // - autoincrement
    runs_id?: string; // - associated fk
    lat: number;
    lng: number;
    actual_stop_asw_id?: string;
    actual_stop_timestamp_real: Date;
    actual_stop_timestamp_scheduled?: Date | null;
    last_stop_asw_id?: string;
    packet_number: string;
    msg_timestamp: Date;
    events: TCPEventEnum;
}
