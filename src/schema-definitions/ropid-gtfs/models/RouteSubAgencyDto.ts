import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IRouteSubAgencyDto } from "../interfaces/IRouteSubAgencyDto";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";

export class RouteSubAgencyDto extends Model<IRouteSubAgencyDto> implements IRouteSubAgencyDto {
    declare route_id: string;
    declare route_licence_number: number | null;
    declare sub_agency_id: number;
    declare sub_agency_name: string;

    public static attributeModel: ModelAttributes<RouteSubAgencyDto> = {
        route_id: { type: DataTypes.STRING, primaryKey: true },
        route_licence_number: { type: DataTypes.INTEGER, allowNull: true },
        sub_agency_id: { type: DataTypes.INTEGER, primaryKey: true },
        sub_agency_name: { type: DataTypes.STRING },
    };

    public static jsonSchema: JSONSchemaType<IRouteSubAgencyDto[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                route_id: { type: "string" },
                route_licence_number: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                sub_agency_id: { type: "integer" },
                sub_agency_name: { type: "string" },
            },
            required: ["route_id", "sub_agency_id", "sub_agency_name"],
        },
    };
}
