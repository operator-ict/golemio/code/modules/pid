import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { DeparturesDirectionsEnum, PossibleDeparturesDirections } from "#sch/ropid-departures-directions";
import { IDeparturesDirectionDto } from "../interfaces/IDeparturesDirectionDto";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";

export class DeparturesDirectionModel extends Model<DeparturesDirectionModel> implements IDeparturesDirectionDto {
    public static TABLE_NAME = "ropid_departures_directions";

    declare id: number;
    declare departure_stop_id: string;
    declare next_stop_id_regexp: string;
    declare direction: DeparturesDirectionsEnum;
    declare rule_order: number;
    declare create_batch_id: string | null;
    declare created_by: string | null;
    declare update_batch_id: string | null;
    declare updated_by: string | null;

    public static attributeModel: ModelAttributes<DeparturesDirectionModel> = {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            unique: true,
        },
        departure_stop_id: {
            type: DataTypes.TEXT,
            allowNull: false,
        },
        next_stop_id_regexp: {
            type: DataTypes.TEXT,
            allowNull: false,
        },
        direction: {
            type: DataTypes.TEXT,
            allowNull: false,
        },
        rule_order: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0,
        },
        create_batch_id: {
            type: DataTypes.BIGINT,
            allowNull: true,
        },
        created_by: {
            type: DataTypes.STRING(150),
            allowNull: true,
        },
        update_batch_id: {
            type: DataTypes.BIGINT,
            allowNull: true,
        },
        updated_by: {
            type: DataTypes.STRING(150),
            allowNull: true,
        },
    };
    // @ts-expect-error
    public static arrayJsonSchema: JSONSchemaType<IDeparturesDirectionDto[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                departure_stop_id: {
                    type: "string",
                },
                next_stop_id_regexp: {
                    type: "string",
                },
                direction: {
                    type: "string",
                    enum: PossibleDeparturesDirections,
                },
                rule_order: {
                    type: "number",
                },
                create_batch_id: {
                    type: ["string", "null"],
                },
                created_by: {
                    type: ["string", "null"],
                },

                update_batch_id: {
                    type: ["string", "null"],
                },
                updated_by: {
                    type: ["string", "null"],
                },
            },
            required: ["departure_stop_id", "next_stop_id_regexp", "direction", "rule_order"],
        },
    };
}
