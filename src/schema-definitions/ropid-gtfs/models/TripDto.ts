import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { ITripDto } from "../interfaces/ITripDto";

export class TripDto extends Model<TripDto> implements ITripDto {
    declare trip_id: string;
    declare bikes_allowed: number;
    declare block_id: string;
    declare direction_id: number;
    declare exceptional: number;
    declare route_id: string;
    declare service_id: string;
    declare shape_id: string;
    declare trip_headsign: string;
    declare trip_operation_type: number;
    declare trip_short_name: string;
    declare wheelchair_accessible: number;

    public static attributeModel: ModelAttributes<TripDto> = {
        bikes_allowed: DataTypes.INTEGER,
        block_id: DataTypes.STRING,
        direction_id: DataTypes.INTEGER,
        exceptional: DataTypes.INTEGER, // neni v GTFS
        route_id: DataTypes.STRING,
        service_id: DataTypes.STRING,
        shape_id: DataTypes.STRING,
        trip_headsign: DataTypes.STRING,
        trip_id: { type: DataTypes.STRING, primaryKey: true },
        wheelchair_accessible: DataTypes.INTEGER,
        trip_operation_type: DataTypes.INTEGER, // neni v GTFS
        trip_short_name: DataTypes.STRING,
    };

    public static jsonSchema: JSONSchemaType<ITripDto[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                trip_id: { type: "string" },
                bikes_allowed: { type: "integer" },
                block_id: { type: "string" },
                direction_id: { type: "integer" },
                exceptional: { type: "integer" },
                route_id: { type: "string" },
                service_id: { type: "string" },
                shape_id: { type: "string" },
                trip_headsign: { type: "string" },
                trip_operation_type: { type: "integer" },
                trip_short_name: { type: "string" },
                wheelchair_accessible: { type: "integer" },
            },
            required: ["trip_id"],
        },
    };
}
