import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { Point } from "@golemio/core/dist/shared/geojson";
import { SharedSchemaProvider } from "@golemio/core/dist/schema-definitions";
import { IMetroRailtrackGPSModel } from "./interfaces/IMetroRailtrackGPSModel";

export class MetroRailtrackGPSModel extends Model<MetroRailtrackGPSModel> implements IMetroRailtrackGPSModel {
    public static TABLE_NAME = "ropidgtfs_metro_railtrack_gps";

    declare track_id: string;
    declare route_name: string;
    declare coordinates: Point;
    declare gtfs_stop_id: string | null;

    public static attributeModel: ModelAttributes<MetroRailtrackGPSModel> = {
        track_id: {
            type: DataTypes.STRING(15),
            primaryKey: true,
        },
        route_name: {
            type: DataTypes.CHAR(1),
            primaryKey: true,
        },
        coordinates: {
            type: DataTypes.GEOMETRY,
            allowNull: false,
        },
        gtfs_stop_id: {
            type: DataTypes.STRING(25),
        },
    };

    public static arrayJsonSchema: JSONSchemaType<IMetroRailtrackGPSModel[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                track_id: {
                    type: "string",
                },
                route_name: {
                    type: "string",
                },
                coordinates: {
                    $ref: "#/definitions/geometry",
                },
                gtfs_stop_id: {
                    type: ["null", "string"],
                },
            },
            required: ["track_id", "route_name", "coordinates", "gtfs_stop_id"],
        },
        definitions: {
            // @ts-expect-error
            geometry: SharedSchemaProvider.Geometry,
        },
    };
}
