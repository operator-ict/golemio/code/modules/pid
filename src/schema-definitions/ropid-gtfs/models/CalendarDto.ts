import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { ICalendarDto } from "../interfaces/ICalendarDto";

export class CalendarDto extends Model<CalendarDto> implements ICalendarDto {
    declare service_id: string;
    declare start_date: string;
    declare end_date: string;
    declare monday: number;
    declare tuesday: number;
    declare wednesday: number;
    declare thursday: number;
    declare friday: number;
    declare saturday: number;
    declare sunday: number;

    public static attributeModel: ModelAttributes<CalendarDto> = {
        end_date: DataTypes.STRING,
        friday: DataTypes.INTEGER,
        monday: DataTypes.INTEGER,
        saturday: DataTypes.INTEGER,
        service_id: { type: DataTypes.STRING, primaryKey: true },
        start_date: DataTypes.STRING,
        sunday: DataTypes.INTEGER,
        thursday: DataTypes.INTEGER,
        tuesday: DataTypes.INTEGER,
        wednesday: DataTypes.INTEGER,
    };

    public static jsonSchema: JSONSchemaType<ICalendarDto[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                service_id: { type: "string" },
                start_date: { type: "string" },
                end_date: { type: "string" },
                monday: { type: "integer" },
                tuesday: { type: "integer" },
                wednesday: { type: "integer" },
                thursday: { type: "integer" },
                friday: { type: "integer" },
                saturday: { type: "integer" },
                sunday: { type: "integer" },
            },
            required: ["service_id"],
        },
    };
}
