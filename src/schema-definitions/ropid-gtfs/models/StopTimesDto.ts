import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IStopTimesComputedDto, IStopTimesDto } from "../interfaces/IStopTimesDto";

export class StopTimesDto extends Model<IStopTimesDto & IStopTimesComputedDto> implements IStopTimesDto {
    declare trip_id: string;
    declare stop_sequence: number;
    declare arrival_time: string;
    declare arrival_time_seconds: number;
    declare departure_time: string;
    declare departure_time_seconds: number;
    declare drop_off_type: string;
    declare pickup_type: string;
    declare shape_dist_traveled: number;
    declare stop_headsign: string;
    declare stop_id: string;
    declare timepoint: number;
    declare computed_dwell_time_seconds: number;

    public static attributeModel: ModelAttributes<StopTimesDto> = {
        arrival_time: DataTypes.STRING,
        arrival_time_seconds: DataTypes.INTEGER, // not in GTFS
        departure_time: DataTypes.STRING,
        departure_time_seconds: DataTypes.INTEGER, // not in GTFS
        drop_off_type: DataTypes.STRING,
        pickup_type: DataTypes.STRING,
        shape_dist_traveled: DataTypes.DOUBLE,
        stop_headsign: DataTypes.STRING,
        stop_id: DataTypes.STRING,
        stop_sequence: { type: DataTypes.INTEGER, primaryKey: true },
        trip_id: { type: DataTypes.STRING, primaryKey: true },
        timepoint: DataTypes.INTEGER,
        computed_dwell_time_seconds: {
            type: DataTypes.SMALLINT,
            set() {
                throw new GeneralError("computed_dwell_time_seconds is read-only", "StopTimesDto");
            },
        },
    };

    public static jsonSchema: JSONSchemaType<IStopTimesDto[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                trip_id: { type: "string" },
                stop_sequence: { type: "integer" },
                arrival_time: { type: "string" },
                arrival_time_seconds: { type: "integer" },
                departure_time: { type: "string" },
                departure_time_seconds: { type: "integer" },
                drop_off_type: { type: "string" },
                pickup_type: { type: "string" },
                shape_dist_traveled: { type: "number" },
                stop_headsign: { type: "string" },
                stop_id: { type: "string" },
                timepoint: { type: "integer" },
            },
            required: ["trip_id", "stop_sequence"],
        },
    };
}
