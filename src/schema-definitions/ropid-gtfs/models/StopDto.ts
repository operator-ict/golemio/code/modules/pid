import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IStopComputedDto, IStopDto } from "../interfaces/IStopDto";

export class StopDto extends Model<IStopDto & IStopComputedDto> implements IStopDto {
    declare stop_id: string;
    declare level_id: string;
    declare location_type: number;
    declare parent_station: string;
    declare platform_code: string;
    declare stop_code: string;
    declare stop_desc: string;
    declare stop_lat: number;
    declare stop_lon: number;
    declare stop_name: string;
    declare stop_timezone: string;
    declare stop_url: string;
    declare wheelchair_boarding: number;
    declare zone_id: string;
    declare asw_node_id: number;
    declare asw_stop_id: number;
    declare computed_cis_stop_id: string | null;

    public static attributeModel: ModelAttributes<StopDto> = {
        location_type: DataTypes.INTEGER,
        parent_station: DataTypes.STRING,
        platform_code: DataTypes.STRING,
        stop_id: { type: DataTypes.STRING, primaryKey: true },
        stop_lat: DataTypes.DOUBLE,
        stop_lon: DataTypes.DOUBLE,
        stop_name: DataTypes.STRING,
        stop_url: DataTypes.STRING,
        wheelchair_boarding: DataTypes.INTEGER,
        zone_id: DataTypes.STRING,
        level_id: DataTypes.STRING,
        stop_code: DataTypes.STRING,
        stop_desc: DataTypes.STRING,
        stop_timezone: DataTypes.STRING,
        asw_node_id: DataTypes.INTEGER,
        asw_stop_id: DataTypes.INTEGER,
        computed_cis_stop_id: {
            type: DataTypes.STRING,
            set() {
                throw new GeneralError("computed_cis_stop_id is read-only", "StopDto");
            },
        },
    };

    public static jsonSchema: JSONSchemaType<IStopDto[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                stop_id: { type: "string" },
                level_id: { type: "string" },
                location_type: { type: "integer" },
                parent_station: { type: "string" },
                platform_code: { type: "string" },
                stop_code: { type: "string" },
                stop_desc: { type: "string" },
                stop_lat: { type: "number" },
                stop_lon: { type: "number" },
                stop_name: { type: "string" },
                stop_timezone: { type: "string" },
                stop_url: { type: "string" },
                wheelchair_boarding: { type: "integer" },
                zone_id: { type: "string" },
                asw_node_id: { type: "integer" },
                asw_stop_id: { type: "integer" },
            },
            required: ["stop_id"],
        },
    };
}
