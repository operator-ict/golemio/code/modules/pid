import { DataTypes, ModelAttributes } from "@golemio/core/dist/shared/sequelize";

const outputServicesCalendarSDMA: ModelAttributes<any> = {
    date: DataTypes.DATE,
    day_diff: DataTypes.INTEGER,
    service_id: DataTypes.STRING(255),
};

const outputServicesCalendarJsonSchema = {
    type: "object",
    properties: {
        date: {
            type: "string",
        },
        day_diff: {
            type: "integer",
        },
        service_id: {
            type: "string",
        },
    },
    required: ["date", "day_diff", "service_id"],
};

const outputMinMaxStopSequencesSDMA: ModelAttributes<any> = {
    trip_id: DataTypes.STRING,
    max_stop_sequence: DataTypes.INTEGER, // 9
    min_stop_sequence: DataTypes.INTEGER, // 1
    max_stop_time: DataTypes.STRING, // "11:58:00"
    min_stop_time: DataTypes.STRING, // "11:58:00"
};

const outputMinMaxStopSequencesJsonSchema = {
    type: "object",
    properties: {
        trip_id: {
            type: "string",
        },
        max_stop_sequence: {
            type: "integer",
        },
        min_stop_sequence: {
            type: "integer",
        },
        max_stop_time: {
            type: "string",
        },
        min_stop_time: {
            type: "string",
        },
    },
    required: ["trip_id", "max_stop_sequence", "min_stop_sequence", "max_stop_time", "min_stop_time"],
};

const outputTripScheduleSDMA: ModelAttributes<any> = {
    origin_route_id: DataTypes.STRING,
    trip_id: {
        type: DataTypes.STRING,
        primaryKey: true,
    },
    service_id: DataTypes.STRING,
    direction_id: DataTypes.SMALLINT,
    shape_id: DataTypes.STRING(15),
    run_number: DataTypes.INTEGER,
    date: {
        type: DataTypes.STRING,
        primaryKey: true,
    },
    route_id: DataTypes.STRING,
    route_type: {
        type: DataTypes.SMALLINT,
        allowNull: false,
    },
    route_short_name: DataTypes.STRING,
    is_regional: DataTypes.STRING,
    is_substitute_transport: DataTypes.STRING,
    is_night: DataTypes.STRING,
    trip_headsign: DataTypes.STRING,
    trip_short_name: DataTypes.STRING,
    block_id: DataTypes.STRING,
    exceptional: DataTypes.NUMBER,
    min_stop_time: DataTypes.STRING,
    max_stop_time: DataTypes.STRING,
    start_timestamp: DataTypes.STRING,
    end_timestamp: DataTypes.STRING,
    first_stop_id: DataTypes.STRING,
    last_stop_id: DataTypes.STRING,
    origin_route_name: DataTypes.STRING(50),
    trip_number: DataTypes.SMALLINT,
    route_licence_number: DataTypes.INTEGER,
};

const outputTripScheduleJsonSchema = {
    type: "object",
    properties: {
        origin_route_id: {
            type: "string",
        },
        trip_id: {
            type: "string",
        },
        service_id: {
            type: "string",
        },
        direction_id: {
            type: "integer",
        },
        shape_id: {
            type: "string",
        },
        run_number: {
            type: "string",
        },
        date: {
            type: "string",
        },
        route_id: {
            type: "string",
        },
        route_type: {
            type: "integer",
        },
        route_short_name: {
            type: "string",
        },
        is_regional: {
            type: "string",
        },
        is_substitute_transport: {
            type: "string",
        },
        is_night: {
            type: "string",
        },
        trip_headsign: {
            type: "string",
        },
        trip_short_name: {
            type: "string",
        },
        block_id: {
            type: "string",
        },
        exceptional: {
            type: "integer",
        },
        min_stop_time: {
            type: "string",
        },
        max_stop_time: {
            type: "string",
        },
        start_timestamp: {
            type: "string",
        },
        end_timestamp: {
            type: "string",
        },
        first_stop_id: {
            type: "string",
        },
        last_stop_id: {
            type: "string",
        },
        origin_route_name: {
            type: "string",
        },
        trip_number: {
            type: "integer",
        },
        route_licence_number: {
            type: "integer",
        },
    },
    required: ["route_type"],
};

export const RopidGTFSPrecomputed = {
    servicesCalendar: {
        outputJsonSchema: outputServicesCalendarJsonSchema,
        outputSequelizeAttributes: outputServicesCalendarSDMA,
        pgTableName: "ropidgtfs_precomputed_services_calendar",
    },
    minMaxStopSequences: {
        outputJsonSchema: outputMinMaxStopSequencesJsonSchema,
        outputSequelizeAttributes: outputMinMaxStopSequencesSDMA,
        pgTableName: "ropidgtfs_precomputed_minmax_stop_sequences",
    },
    tripSchedule: {
        outputJsonSchema: outputTripScheduleJsonSchema,
        outputSequelizeAttributes: outputTripScheduleSDMA,
        pgTableName: "ropidgtfs_precomputed_trip_schedule",
    },
};
