import Sequelize, { ModelAttributes, DataTypes } from "@golemio/core/dist/shared/sequelize";

export interface IRopidGTFSOisMappingInputData {
    ois: number;
    node: number;
    name: string;
}

export interface IRopidGTFSOisMappingData extends IRopidGTFSOisMappingInputData {
    [auditFields: string]: unknown;
}

const datasourceJsonSchema = {
    type: "array",
    items: {
        $ref: "#/$defs/Item",
    },
    $defs: {
        Item: {
            type: "object",
            properties: {
                ois: {
                    type: "number",
                },
                node: {
                    type: "number",
                },
                name: {
                    type: "string",
                },
            },
            required: ["ois", "node", "name"],
        },
    },
};

const outputJsonSchema = datasourceJsonSchema;

const outputSequelizeAttributes: ModelAttributes<any, IRopidGTFSOisMappingData> = {
    ois: {
        primaryKey: true,
        type: Sequelize.INTEGER,
    },
    node: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    name: {
        type: Sequelize.STRING(255),
        allowNull: false,
    },
};

export const RopidGTFSOisMapping = {
    name: "RopidGTFSOisMapping",
    datasourceJsonSchema,
    outputJsonSchema,
    outputSequelizeAttributes,
    pgTableName: "ropidgtfs_ois",
};
