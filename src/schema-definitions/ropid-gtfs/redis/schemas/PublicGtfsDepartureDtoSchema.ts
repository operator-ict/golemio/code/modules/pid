import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IPublicGtfsDepartureCacheDto } from "../interfaces/IPublicGtfsDepartureCacheDto";

const publicGtfsDepartureDtoSchema: JSONSchemaType<IPublicGtfsDepartureCacheDto[]> = {
    $schema: "http://json-schema.org/draft-04/schema#",
    title: "PublicGtfsDepartureDto",
    type: "array",
    items: {
        type: "object",
        properties: {
            stop_id: {
                type: "string",
            },
            departure_datetime: {
                type: "string",
            },
            arrival_datetime: {
                oneOf: [
                    {
                        type: "string",
                    },
                    {
                        type: "null",
                        nullable: true,
                    },
                ],
            },
            route_short_name: {
                type: "string",
            },
            route_type: {
                type: "number",
            },
            trip_id: {
                type: "string",
            },
            stop_sequence: {
                type: "number",
            },
            platform_code: {
                oneOf: [
                    {
                        type: "string",
                    },
                    {
                        type: "null",
                        nullable: true,
                    },
                ],
            },
            trip_headsign: {
                type: "string",
            },
        },
        required: [
            "stop_id",
            "departure_datetime",
            "arrival_datetime",
            "route_short_name",
            "route_type",
            "trip_id",
            "stop_sequence",
            "platform_code",
            "trip_headsign",
        ],
    },
};
export { publicGtfsDepartureDtoSchema as PublicGtfsDepartureDtoSchema };
