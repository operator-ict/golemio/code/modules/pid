import { GTFSRouteTypeEnum } from "#helpers/RouteTypeEnums";

export interface IPublicGtfsDepartureCacheDto {
    stop_id: string;
    departure_datetime: string;
    arrival_datetime: string | null;
    route_short_name: string;
    route_type: GTFSRouteTypeEnum;
    trip_id: string;
    stop_sequence: number;
    platform_code: string | null;
    trip_headsign: string;
}
