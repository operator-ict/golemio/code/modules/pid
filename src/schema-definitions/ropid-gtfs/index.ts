import { RopidDeparturesPresets as departuresPresets } from "#sch/ropid-departures-presets";
import { RopidGTFSCisStopGroups } from "#sch/ropid-gtfs/RopidGTFSCisStopGroups";
import { RopidGTFSCisStops } from "#sch/ropid-gtfs/RopidGTFSCisStops";
import { RopidGTFSOisMapping } from "#sch/ropid-gtfs/RopidGTFSOisMapping";
import { RopidGTFSPrecomputed } from "#sch/ropid-gtfs/RopidGTFSPrecomputed";
import { RopidGTFSRunNumbers } from "#sch/ropid-gtfs/RopidGTFSRunNumbers";
import { IRopidSchemaDefinitions } from "#sch/shared";
import Sequelize from "@golemio/core/dist/shared/sequelize";
import { RopidGTFSSchedule, ScheduleModelName } from "./RopidGtfsSchedule";

// SDMA = Sequelize DefineModelAttributes

const outputTripsStopTimesViewSDMA: Sequelize.ModelAttributes<any> = {
    bikes_allowed: Sequelize.INTEGER,
    block_id: Sequelize.STRING,
    direction_id: Sequelize.INTEGER,
    exceptional: Sequelize.INTEGER,
    route_id: Sequelize.STRING,
    service_id: Sequelize.STRING,
    shape_id: Sequelize.STRING,
    trip_headsign: Sequelize.STRING,
    trip_id: Sequelize.STRING,
    wheelchair_accessible: Sequelize.INTEGER,

    stop_times_arrival_time: Sequelize.STRING,
    stop_times_arrival_time_seconds: Sequelize.INTEGER,
    stop_times_departure_time: Sequelize.STRING,
    stop_times_departure_time_seconds: Sequelize.INTEGER,
    stop_times_shape_dist_traveled: Sequelize.DOUBLE,
    stop_times_stop_id: Sequelize.STRING,
    stop_times_stop_sequence: Sequelize.INTEGER,
    stop_times_stop_headsign: Sequelize.STRING,
    stop_times_trip_id: Sequelize.STRING,

    stop_times_stop_stop_id: Sequelize.STRING,
    stop_times_stop_stop_lat: Sequelize.DOUBLE,
    stop_times_stop_stop_lon: Sequelize.DOUBLE,
};

const outputTripsShapesViewSDMA: Sequelize.ModelAttributes<any> = {
    bikes_allowed: Sequelize.INTEGER,
    block_id: Sequelize.STRING,
    direction_id: Sequelize.INTEGER,
    exceptional: Sequelize.INTEGER,
    route_id: Sequelize.STRING,
    service_id: Sequelize.STRING,
    shape_id: Sequelize.STRING,
    trip_headsign: Sequelize.STRING,
    trip_id: Sequelize.STRING,
    wheelchair_accessible: Sequelize.INTEGER,

    shapes_shape_dist_traveled: Sequelize.DOUBLE,
    shapes_shape_id: Sequelize.STRING,
    shapes_shape_pt_lat: Sequelize.DOUBLE,
    shapes_shape_pt_lon: Sequelize.DOUBLE,
    shapes_shape_pt_sequence: Sequelize.INTEGER,
};

const forExport: IRopidSchemaDefinitions = {
    name: "RopidGTFS",
    ...RopidGTFSSchedule,
    ...RopidGTFSPrecomputed,
    metadata: {
        name: "RopidGTFSMetadata",
        pgTableName: "ropidgtfs_metadata",
    },
    tripsShapesView: {
        name: "RopidGTFSTripsShapesView",
        outputSequelizeAttributes: outputTripsShapesViewSDMA,
        pgTableName: "v_ropidgtfs_trips_shapes_view",
    },
    tripsStopTimesView: {
        name: "RopidGTFSTripsStopTimesView",
        outputSequelizeAttributes: outputTripsStopTimesViewSDMA,
        pgTableName: "v_ropidgtfs_trips_stop_times_view",
    },
    cis_stops: RopidGTFSCisStops,
    cis_stop_groups: RopidGTFSCisStopGroups,
    run_numbers: RopidGTFSRunNumbers,
    ois: RopidGTFSOisMapping,
    departuresPresets,
};

type DatasetModelName = ScheduleModelName | "cis_stops" | "cis_stop_groups" | "run_numbers" | "ois";

export { forExport as RopidGTFS, ScheduleModelName, DatasetModelName };
