import { ModelAttributes, DataTypes } from "@golemio/core/dist/shared/sequelize";

export interface IRopidGTFSCisStopsInputData {
    altIdosName: string;
    cis: number;
    id: string;
    jtskX: number;
    jtskY: number;
    lat: number;
    lon: number;
    platform: string;
    wheelchairAccess: string;
    zone: string;
}

export interface IRopidGTFSCisStopsData {
    alt_idos_name: string;
    cis: number;
    id: string;
    jtsk_x: number;
    jtsk_y: number;
    lat: number;
    lon: number;
    platform: string;
    wheelchair_access: string;
    zone: string;

    [auditFields: string]: unknown;
}

const datasourceJsonSchema = {
    type: "array",
    items: {
        $ref: "#/$defs/Item",
    },
    $defs: {
        Item: {
            type: "object",
            properties: {
                altIdosName: { type: "string" },
                cis: { type: "integer" },
                id: { type: "string" },
                jtskX: { type: "number" },
                jtskY: { type: "number" },
                lat: { type: "number" },
                lon: { type: "number" },
                platform: { type: "string" },
                wheelchairAccess: { type: "string" },
                zone: { type: "string" },
            },
            required: ["id"],
        },
    },
};

const outputJsonSchema = {
    type: "array",
    items: {
        $ref: "#/$defs/Item",
        $defs: {
            Item: {
                type: "object",
                properties: {
                    alt_idos_name: { type: "string" },
                    cis: { type: "integer" },
                    id: { type: "string" },
                    jtsk_x: { type: "number" },
                    jtsk_y: { type: "number" },
                    lat: { type: "number" },
                    lon: { type: "number" },
                    platform: { type: "string" },
                    wheelchair_access: { type: "string" },
                    zone: { type: "string" },
                },
                required: ["cis"],
            },
        },
    },
};

const outputSequelizeAttributes: ModelAttributes<any, IRopidGTFSCisStopsData> = {
    alt_idos_name: DataTypes.STRING,
    cis: DataTypes.INTEGER,
    id: {
        primaryKey: true,
        type: DataTypes.STRING,
    },
    jtsk_x: DataTypes.DOUBLE,
    jtsk_y: DataTypes.DOUBLE,
    lat: DataTypes.DOUBLE,
    lon: DataTypes.DOUBLE,
    platform: DataTypes.STRING,
    wheelchair_access: DataTypes.STRING,
    zone: DataTypes.STRING,
};

export const RopidGTFSCisStops = {
    name: "RopidGTFSCisStops",
    datasourceJsonSchema,
    outputJsonSchema,
    outputSequelizeAttributes,
    pgTableName: "ropidgtfs_cis_stops",
};
