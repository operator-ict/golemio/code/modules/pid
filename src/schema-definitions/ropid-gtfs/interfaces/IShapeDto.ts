export interface IShapeDto {
    shape_id: string;
    shape_pt_sequence: number;
    shape_dist_traveled: number;
    shape_pt_lat: number;
    shape_pt_lon: number;
}
