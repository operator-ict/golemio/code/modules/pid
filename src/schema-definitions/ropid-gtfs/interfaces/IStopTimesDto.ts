export interface IStopTimesDto {
    trip_id: string;
    stop_sequence: number;
    arrival_time: string;
    arrival_time_seconds: number;
    departure_time: string;
    departure_time_seconds: number;
    drop_off_type: string;
    pickup_type: string;
    shape_dist_traveled: number;
    stop_headsign: string;
    stop_id: string;
    timepoint: number;
}

export interface IStopTimesComputedDto {
    computed_dwell_time_seconds: number;
}
