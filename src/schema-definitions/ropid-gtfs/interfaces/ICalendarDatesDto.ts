export interface ICalendarDatesDto {
    service_id: string;
    date: string;
    exception_type: number;
}
