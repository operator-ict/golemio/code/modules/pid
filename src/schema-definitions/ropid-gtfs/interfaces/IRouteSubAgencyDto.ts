export interface IRouteSubAgencyDto {
    route_id: string;
    route_licence_number: number | null;
    sub_agency_id: number;
    sub_agency_name: string;
}
