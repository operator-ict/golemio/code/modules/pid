import { IRouteDto } from "./IRouteDto";
import { IShapeDto } from "./IShapeDto";
import { IStopDto } from "./IStopDto";
import { IStopTimesDto } from "./IStopTimesDto";
import { ITripDto } from "./ITripDto";

export interface IStopTimeWithStopDto extends IStopTimesDto {
    stop: IStopDto;
}

export interface ITripWithDefinedRouteDto extends ITripWithOptionalAssociationsDto {
    route: IRouteDto;
}

export interface ITripWithOptionalAssociationsDto extends ITripDto {
    route?: IRouteDto;
    stop_times?: IStopTimeWithStopDto[];
    shapes?: IShapeDto[];
    schedule?: {
        run_number: number;
    };
}
