export interface IAgencyDto {
    agency_email: string;
    agency_fare_url: string;
    agency_id: string;
    agency_lang: string;
    agency_name: string;
    agency_phone: string;
    agency_timezone: string;
    agency_url: string;
}
