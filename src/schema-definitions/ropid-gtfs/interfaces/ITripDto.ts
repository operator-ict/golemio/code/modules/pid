export interface ITripDto {
    trip_id: string;
    bikes_allowed: number;
    block_id: string;
    direction_id: number;
    exceptional: number;
    route_id: string;
    service_id: string;
    shape_id: string;
    trip_headsign: string;
    trip_operation_type: number;
    trip_short_name: string;
    wheelchair_accessible: number;
}
