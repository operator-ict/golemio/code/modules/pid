import { DeparturesDirectionsEnum } from "#sch/ropid-departures-directions";

export interface IDeparturesDirectionDto {
    departure_stop_id: string;
    next_stop_id_regexp: string;
    direction: DeparturesDirectionsEnum;
    rule_order: number;
    created_by: string | null;
    updated_by: string | null;
    update_batch_id: string | null;
    create_batch_id: string | null;
}
