import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";

export interface IRouteDto {
    route_id: string;
    agency_id: string;
    is_night: string;
    is_regional: string;
    is_substitute_transport: string;
    route_color: string;
    route_desc: string;
    route_long_name: string;
    route_short_name: string;
    route_text_color: string;
    route_type: GTFSRouteTypeEnum;
    route_url: string;
}
