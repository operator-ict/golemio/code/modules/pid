export interface ICalendarDto {
    service_id: string;
    start_date: string;
    end_date: string;
    monday: number;
    tuesday: number;
    wednesday: number;
    thursday: number;
    friday: number;
    saturday: number;
    sunday: number;
}
