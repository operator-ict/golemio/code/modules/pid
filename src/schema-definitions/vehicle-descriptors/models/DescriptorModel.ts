import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";
import { VehicleDescriptorStateEnum } from "src/helpers/VehicleDescriptorEnums";
import { IDescriptorAuditDto, IDescriptorOutputDto } from "./interfaces/IDescriptorOutputDto";

export class DescriptorModel extends Model<IDescriptorOutputDto & IDescriptorAuditDto> implements IDescriptorOutputDto {
    public static tableName = "vehiclepositions_vehicle_descriptors";

    declare id: number;
    declare state: VehicleDescriptorStateEnum;
    declare registration_number: number;
    declare registration_number_index: number;
    declare license_plate: string | null;
    declare operator: string;
    declare manufacturer: string;
    declare type: string;
    declare traction: string;
    declare gtfs_route_type: GTFSRouteTypeEnum;
    declare is_wheelchair_accessible: boolean | null;
    declare is_air_conditioned: boolean;
    declare has_usb_chargers: boolean;
    declare paint: string | null;
    declare thumbnail_url: string | null;
    declare photo_url: string | null;

    public static attributeModel: ModelAttributes<DescriptorModel, IDescriptorOutputDto> = {
        id: { type: DataTypes.INTEGER, primaryKey: true },
        state: { type: DataTypes.STRING, allowNull: false },
        registration_number: { type: DataTypes.INTEGER, allowNull: false },
        registration_number_index: { type: DataTypes.SMALLINT, allowNull: false },
        license_plate: { type: DataTypes.STRING, allowNull: true },
        operator: { type: DataTypes.STRING, allowNull: false },
        manufacturer: { type: DataTypes.STRING, allowNull: false },
        type: { type: DataTypes.STRING, allowNull: false },
        traction: { type: DataTypes.STRING, allowNull: false },
        gtfs_route_type: { type: DataTypes.SMALLINT, allowNull: false },
        is_wheelchair_accessible: { type: DataTypes.BOOLEAN, allowNull: true },
        is_air_conditioned: { type: DataTypes.BOOLEAN, allowNull: false },
        has_usb_chargers: { type: DataTypes.BOOLEAN, allowNull: false },
        paint: { type: DataTypes.STRING, allowNull: true },
        thumbnail_url: { type: DataTypes.STRING, allowNull: true },
        photo_url: { type: DataTypes.STRING, allowNull: true },
    };

    public static updateAttributes = Object.keys(DescriptorModel.attributeModel)
        .filter((attribute) => attribute !== "id")
        .concat("updated_at") as Array<keyof IDescriptorOutputDto>;

    public static arrayJsonSchema: JSONSchemaType<IDescriptorOutputDto[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                id: { type: "integer" },
                state: { type: "string", enum: Object.values(VehicleDescriptorStateEnum) },
                registration_number: { type: "integer" },
                registration_number_index: { type: "integer" },
                license_plate: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                operator: { type: "string" },
                manufacturer: { type: "string" },
                type: { type: "string" },
                traction: { type: "string" },
                gtfs_route_type: { type: "integer" },
                is_wheelchair_accessible: { oneOf: [{ type: "boolean" }, { type: "null", nullable: true }] },
                is_air_conditioned: { type: "boolean" },
                has_usb_chargers: { type: "boolean" },
                paint: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                thumbnail_url: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                photo_url: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            },
            required: [
                "id",
                "state",
                "registration_number",
                "registration_number_index",
                "license_plate",
                "operator",
                "manufacturer",
                "type",
                "traction",
                "gtfs_route_type",
                "is_wheelchair_accessible",
                "is_air_conditioned",
                "has_usb_chargers",
                "paint",
                "thumbnail_url",
                "photo_url",
            ],
        },
    };
}
