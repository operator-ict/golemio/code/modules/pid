import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import Sequelize, { Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IMetadataDto } from "../interfaces/IMetadataDto";

export class MetadataDto extends Model<IMetadataDto> implements IMetadataDto {
    declare dataset: string;
    declare key: string;
    declare type: string;
    declare value: number | string;
    declare version: number;

    public static attributeModel: ModelAttributes<MetadataDto, IMetadataDto> = {
        dataset: Sequelize.STRING,
        key: Sequelize.STRING,
        type: Sequelize.STRING,
        value: Sequelize.STRING,
        version: Sequelize.INTEGER,
    };

    public static jsonSchema: JSONSchemaType<IMetadataDto> = {
        type: "object",
        properties: {
            dataset: { type: "string" },
            key: { type: "string" },
            type: { type: "string" },
            value: { oneOf: [{ type: "integer" }, { type: "string" }] },
            version: { type: "number" },
        },
        required: ["key", "value"],
    };
}
