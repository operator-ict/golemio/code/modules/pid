const outputPresetsJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            route_name: {
                type: "string",
            },
            api_version: {
                type: "integer",
            },
            route: {
                type: "string",
            },
            url_query_params: {
                type: "string",
            },
            note: {
                type: "string",
            },
        },
    },
};

export { outputPresetsJsonSchema };
