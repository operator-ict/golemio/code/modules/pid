import { DataTypes, ModelAttributes } from "@golemio/core/dist/shared/sequelize";

interface IRopidDeparturesPresetsOutput {
    route_name: string;
    api_version: number;
    route: string;
    url_query_params: string;
    note: string;
    is_testing: boolean;
    [audit: string]: unknown;
}

const outputPresetsSDMA: ModelAttributes<any, IRopidDeparturesPresetsOutput> = {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    route_name: {
        type: DataTypes.STRING(100),
        unique: true,
    },
    api_version: DataTypes.SMALLINT,
    route: DataTypes.STRING(100),
    url_query_params: DataTypes.STRING(400),
    note: DataTypes.STRING(1000),
    is_testing: DataTypes.BOOLEAN,
};

export { outputPresetsSDMA };
export type { IRopidDeparturesPresetsOutput };
