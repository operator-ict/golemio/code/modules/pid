export interface IPresetLogProcessedDto {
    deviceAlias: string;
    receivedAt: string;
}
