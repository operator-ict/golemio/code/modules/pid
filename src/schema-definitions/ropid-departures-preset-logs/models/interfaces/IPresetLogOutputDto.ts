export interface IPresetLogOutputDto {
    device_alias: string;
    received_at: Date;
    is_processed: boolean;
    request_url: string;
    request_method: string;
    request_user_agent: string;
    response_status: number;
    response_time_ms: number;
}

export interface IPresetLogGenerated {
    id: string;
}
