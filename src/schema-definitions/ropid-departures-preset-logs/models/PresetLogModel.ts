import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes, ModelIndexesOptions } from "@golemio/core/dist/shared/sequelize";
import { IPresetLogOutputDto, IPresetLogGenerated } from "./interfaces/IPresetLogOutputDto";

export class PresetLogModel extends Model<IPresetLogOutputDto> implements IPresetLogOutputDto {
    public static tableName = "ropid_departures_preset_logs";

    declare device_alias: string;
    declare received_at: Date;
    declare is_processed: boolean;
    declare request_url: string;
    declare request_method: string;
    declare request_user_agent: string;
    declare response_status: number;
    declare response_time_ms: number;

    public static attributeModel: ModelAttributes<PresetLogModel, IPresetLogOutputDto & IPresetLogGenerated> = {
        id: { type: DataTypes.BIGINT, autoIncrement: true, primaryKey: true },
        device_alias: { type: DataTypes.STRING },
        received_at: { type: DataTypes.DATE },
        is_processed: { type: DataTypes.BOOLEAN, allowNull: false },
        request_url: { type: DataTypes.STRING, allowNull: false },
        request_method: { type: DataTypes.STRING, allowNull: false },
        request_user_agent: { type: DataTypes.STRING, allowNull: false },
        response_status: { type: DataTypes.SMALLINT, allowNull: false },
        response_time_ms: { type: DataTypes.INTEGER, allowNull: false },
    };

    public static indexes: ModelIndexesOptions[] = [
        {
            unique: true,
            using: "BTREE",
            name: "ropid_departures_preset_logs_unique_idx",
            fields: ["device_alias", "received_at"],
        },
        {
            unique: false,
            using: "BTREE",
            name: "ropid_departures_preset_logs_is_processed_idx",
            fields: ["is_processed"],
        },
    ];

    public static arrayJsonSchema: JSONSchemaType<IPresetLogOutputDto[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                device_alias: { type: "string" },
                received_at: { type: "object", required: ["toISOString"] },
                is_processed: { type: "boolean" },
                request_url: { type: "string" },
                request_method: { type: "string" },
                request_user_agent: { type: "string" },
                response_status: { type: "number" },
                response_time_ms: { type: "number" },
            },
            required: [
                "device_alias",
                "received_at",
                "is_processed",
                "request_url",
                "request_method",
                "request_user_agent",
                "response_status",
                "response_time_ms",
            ],
        },
    };
}
