interface IQueryStream {
    level: string;
    message: string;
    req_httpVersion: string;
    req_method: string;
    req_remoteAddress: string;
    req_url: string;
    req_userAgent?: string;
    res_contentLength?: string;
    res_status: string;
    responseTime: string;
}

type IQueryValue = [string, string];

export interface ILogQueryData {
    stream?: IQueryStream;
    values?: IQueryValue[];
}
