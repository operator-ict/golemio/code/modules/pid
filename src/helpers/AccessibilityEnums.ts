export enum GtfsStopWheelchairBoardingEnum {
    /** No accessibility information for the station/stop */
    NoInformation = 0,

    /** Some vehicles at this station/stop can be boarded by a rider in a wheelchair */
    AccessibleStation = 1,

    /** Wheelchair boarding is not possible at this station/stop  */
    InaccessibleStation = 2,
}

export enum GtfsTripWheelchairAccessEnum {
    /** No accessibility information for the trip */
    NoInformation = 0,

    /** Vehicle being used on this particular trip can accommodate at least one rider in a wheelchair */
    AccessibleVehicle = 1,

    /** No riders in wheelchairs can be accommodated on this trip */
    InaccessibleVehicle = 2,
}
