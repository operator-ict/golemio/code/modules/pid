export enum GtfsStopLocationType {
    /** A location where passengers board or disembark from a transit vehicle */
    StopOrPlatform = 0,

    /** A physical structure or area that contains one or more platform */
    Station = 1,

    /** A location where passengers can enter or exit a station from the street */
    EntranceOrExit = 2,

    /** A location within a station, not matching any other location_type */
    GenericNode = 3,

    /** A specific location on a platform, where passengers can board and/or alight vehicles */
    BoardingArea = 4,
}
