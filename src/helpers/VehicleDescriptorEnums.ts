export enum VehicleDescriptorStateEnum {
    ZAR = "zar",
    DOCO = "doco",
    NZR = "nzr",
    MUZ = "muz",
    DILNY = "dilny",
    NEZ = "nez",
    ODS = "ods",
    SLUZ = "sluz",
    PROD = "prod",
    ZRUS = "zrus",
    VRAK = "vrak",
}

export enum VehicleDescriptorTractionEnum {
    Tram = "tramvaj",
    Bus = "autobus",
    ElectricBus = "elektrobus",
    TrolleyBus = "trolejbus",
}
