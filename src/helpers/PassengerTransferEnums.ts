export enum GtfsStopTimePickupType {
    /** Regularly scheduled pickup */
    RegularlyScheduled = 0,

    /** No pickup available */
    NoPickup = 1,

    /** Must phone agency to arrange pickup */
    MustPhone = 2,

    /** Must coordinate with driver to arrange pickup */
    MustCoordinate = 3,
}

export enum GtfsStopTimeDropOffType {
    /** Regularly scheduled drop off */
    RegularlyScheduled = 0,

    /** No drop off available */
    NoDropOff = 1,

    /** Must phone agency to arrange drop off */
    MustPhone = 2,

    /** Must coordinate with driver to arrange drop off */
    MustCoordinate = 3,
}
