import { DateTime } from "@golemio/core/dist/shared/luxon";
import moment from "@golemio/core/dist/shared/moment-timezone";

export class DateTimeUtils {
    public static LOCALE = "cs-CZ";
    public static TIMEZONE = "Europe/Prague";
    // ISO 8601, no fractional seconds (same as moment.defaultFormat)
    private static FORMAT_DATE_TIME = "yyyy-LL-dd'T'HH:mm:ssZZ";
    private static FORMAT_TIME = "HH:mm:ss";

    private static stopTimeFormatter = new Intl.DateTimeFormat(this.LOCALE, {
        hour: "2-digit",
        minute: "2-digit",
        second: "2-digit",
        timeZone: this.TIMEZONE,
    });

    /**
     * @example 2022-10-26 08:55:10+01 -> 2022-10-26T09:55:10+02:00
     */
    public static formatSQLTimestamp(sqlTimestamp: string) {
        return DateTime.fromSQL(sqlTimestamp).setZone(this.TIMEZONE).toFormat(this.FORMAT_DATE_TIME);
    }

    /**
     * @example 2022-10-26T08:55:10+01:00 -> 09:55:10
     */
    public static parseUTCTimeFromISO(isoTimestamp = new Date().toISOString()) {
        return DateTime.fromISO(isoTimestamp).setZone(this.TIMEZONE).toFormat(this.FORMAT_TIME);
    }

    /**
     * Get a `Date` object corresponding to a given GTFS stop time and a given trip origin timestamp
     *
     * @param stopTime Time in the HH:MM:SS format (H:MM:SS is also accepted). The time is measured from "noon minus 12h" of the
     * service day (effectively midnight except for days on which daylight savings time changes occur). For times occurring after
     * midnight on the service day, enter the time as a value greater than 24:00:00 in HH:MM:SS. See also
     * <https://gtfs.org/schedule/reference/#:~:text=A%20phone%20number.-,Time,-%2D%20Time%20in%20the>.
     * @param tripOriginTimestamp The ISO 8601 timestamp of the trip origin
     */
    public static getStopDateTimeForTripOrigin(stopTime: string, tripOriginTimestamp: string): Date {
        const [stopTimeHours, stopTimeMinutes, stopTimeSeconds] = stopTime.split(":").map((e) => parseInt(e, 10));
        const tripOriginMoment = moment.utc(tripOriginTimestamp).tz(this.TIMEZONE);
        const isOvernight = stopTimeHours >= 24;
        const isOriginAfterMidnight = tripOriginMoment.hours() < 12;
        const shouldSubtractADayFromTripOrigin = isOvernight && isOriginAfterMidnight;
        tripOriginMoment.subtract(shouldSubtractADayFromTripOrigin ? 1 : 0, "days");
        tripOriginMoment.hours(12).minutes(0).seconds(0).milliseconds(0);
        tripOriginMoment.subtract(12, "hours");
        tripOriginMoment.add(stopTimeHours, "hours").add(stopTimeMinutes, "minutes").add(stopTimeSeconds, "seconds");
        return tripOriginMoment.toDate();
    }

    /**
     * Get a `Date` object corresponding to a given GTFS stop time and a given start of the trip service day
     *
     * @param stopTimeInSeconds GTFS stop time in seconds. The time is measured from "noon minus 12h" of the service day
     * (effectively midnight except for days on which daylight savings time changes occur). For times occurring after midnight on
     * the service day, enter the time as a value greater than 24:00:00 in HH:MM:SS. See also
     * <https://gtfs.org/schedule/reference/#:~:text=A%20phone%20number.-,Time,-%2D%20Time%20in%20the>.
     * @param startDayTimestamp Unix timestamp of the local time start of the service day (that is, the midnight before the trip
     * starts) **in milliseconds**. If not provided, the start of the current day is used.
     */
    public static getStopDateTimeForDayStart(stopTimeInSeconds: number, startDayTimestamp?: number): Date {
        let stopTimeMoment: moment.Moment;
        if (startDayTimestamp) {
            stopTimeMoment = moment(startDayTimestamp).tz(this.TIMEZONE);
        } else {
            stopTimeMoment = moment().tz(this.TIMEZONE).startOf("day");
        }
        stopTimeMoment.hours(12).minutes(0).seconds(0).milliseconds(0);
        stopTimeMoment.subtract(12, "hours");
        stopTimeMoment.add(stopTimeInSeconds, "seconds");
        return stopTimeMoment.toDate();
    }

    /**
     * Get a localized, time-zone aware stop time formatted to `HH:mm:ss`
     *
     * @param stopTime The stop time to format
     */
    public static formatStopTime(stopTime: Date) {
        return this.stopTimeFormatter.format(stopTime);
    }
}
