import { Position } from "@golemio/core/dist/shared/geojson";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import CheapRuler, { BBox, Point } from "cheap-ruler";
import { IGeoMeasurementHelper } from "./interfaces/IGeoMeasurementHelper";

type Unit = "kilometers" | "meters";
const APPROXIMATION_LATITUDE = 50.0;

@injectable()
export class GeoMeasurementHelper implements IGeoMeasurementHelper {
    private rulerInstanceDict: Map<Unit, CheapRuler>;

    constructor() {
        this.rulerInstanceDict = new Map();
    }

    /** Get distance in kilometers between two points */
    public getDistanceInKilometers(pointA: Point | Position, pointB: Point | Position): number {
        const validPointA = this.getValidPoint(pointA);
        const validPointB = this.getValidPoint(pointB);
        return this.getRulerInstance("kilometers").distance(validPointA, validPointB);
    }

    /** Get distance in meters between two points */
    public getDistanceInMeters(pointA: Point | Position, pointB: Point | Position): number {
        const validPointA = this.getValidPoint(pointA);
        const validPointB = this.getValidPoint(pointB);
        return this.getRulerInstance("meters").distance(validPointA, validPointB);
    }

    /** Get buffered bounding box in kilometers */
    public getBufferedBBoxInKilometers(point: Point | Position, bufferInKm: number): BBox {
        const validPoint = this.getValidPoint(point);
        return this.getRulerInstance("kilometers").bufferPoint(validPoint, bufferInKm);
    }

    /** Check if point is inside bounding box */
    public isPointInBBox(point: Point | Position, bbox: BBox): boolean {
        const validPoint = this.getValidPoint(point);
        return this.getRulerInstance("kilometers").insideBBox(validPoint, bbox);
    }

    private getRulerInstance(unit: Unit): CheapRuler {
        let ruler = this.rulerInstanceDict.get(unit);
        if (!ruler) {
            ruler = new CheapRuler(APPROXIMATION_LATITUDE, unit);
            this.rulerInstanceDict.set(unit, ruler);
        }

        return ruler;
    }

    private getValidPoint(point: Point | Position): Point {
        if (point.length !== 2) {
            throw new GeneralError(
                "getValidPoint: invalid point " + JSON.stringify(point),
                this.constructor.name,
                undefined,
                422
            );
        }

        return point as Point;
    }
}
