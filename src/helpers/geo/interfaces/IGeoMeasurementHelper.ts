import { Position } from "@golemio/core/dist/shared/geojson";
import { BBox, Point } from "cheap-ruler";

export interface IGeoMeasurementHelper {
    getDistanceInKilometers(pointA: Point | Position, pointB: Point | Position): number;
    getDistanceInMeters(pointA: Point | Position, pointB: Point | Position): number;
    getBufferedBBoxInKilometers(point: Point | Position, bufferInKm: number): BBox;
    isPointInBBox(point: Point | Position, bbox: BBox): boolean;
}
