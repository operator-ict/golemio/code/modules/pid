import { IDataSourceProvider } from "#ie/shared/datasources/IDataSourceProvider";
import { CommonRunsValidationSchema } from "#ie/vehicle-positions/workers/runs/schema/CommonRunsSchema";
import { IMetroRailtrack, metroRailTrackJsonSchema } from "#sch/datasources/static-data";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { CSVDataTypeStrategy, DataSource, IDataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class MetroRailTrackDataSourceProvider implements IDataSourceProvider<IMetroRailtrack[]> {
    constructor(@inject(CoreToken.SimpleConfig) public config: ISimpleConfig) {}

    public getDataSource(): IDataSource {
        const baseUrl = this.config.getValue<string>("module.pid.staticData.baseUrl");
        const urlPath = this.config.getValue<string>("module.pid.staticData.metroRailTracks.path");
        const url = new URL(urlPath, baseUrl);

        return new DataSource<IMetroRailtrack[]>(
            "metroRailTrackDataSourceProvider",
            new HTTPFetchProtocolStrategy({
                headers: {
                    Accept: "text/csv",
                },
                method: "GET",
                timeoutInSeconds: 20,
                url: url.toString(),
                responseType: "text",
            }),
            new CSVDataTypeStrategy({
                fastcsvParams: { headers: true },
                subscribe: (json: any) => json,
            }),
            new JSONSchemaValidator("metroRailTrackDataSourceValidator", metroRailTrackJsonSchema)
        );
    }
}
