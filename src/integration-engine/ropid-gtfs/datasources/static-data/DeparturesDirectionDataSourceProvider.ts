import { IDataSourceProvider } from "#ie/shared/datasources/IDataSourceProvider";
import { IDeparturesDirection, departuresDirectionsJsonSchema } from "#sch/datasources/static-data";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { CSVDataTypeStrategy, DataSource, IDataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class DeparturesDirectionDataSourceProvider implements IDataSourceProvider<IDeparturesDirection[]> {
    constructor(@inject(CoreToken.SimpleConfig) private config: ISimpleConfig) {}

    public getDataSource(): IDataSource {
        const baseUrl = this.config.getValue<string>("module.pid.staticData.baseUrl");
        const urlPath = this.config.getValue<string>("module.pid.staticData.departuresDirection.path");
        const url = new URL(urlPath, baseUrl);

        return new DataSource<IDeparturesDirection[]>(
            "departuresDirectionsDataSource",
            new HTTPFetchProtocolStrategy({
                headers: {
                    Accept: "text/csv",
                },
                method: "GET",
                timeoutInSeconds: 20,
                url: url.toString(),
                responseType: "text",
            }),
            new CSVDataTypeStrategy({
                fastcsvParams: { headers: true },
                subscribe: (json: any) => json,
            }),
            new JSONSchemaValidator("departuresDirectionDataSourceValidator", departuresDirectionsJsonSchema)
        );
    }
}
