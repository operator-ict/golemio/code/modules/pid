import { IDataSourceProvider } from "#ie/shared/datasources/IDataSourceProvider";
import { IMetroRailtrack, IDeparturesDirection } from "#sch/datasources/static-data";
import { IDataSource } from "@golemio/core/dist/integration-engine";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { RopidGtfsContainerToken } from "../ioc/RopidGtfsContainerToken";

type DataSourceDict = {
    metroRailTrack: IDataSource<IMetroRailtrack[]>;
    departuresDirections: IDataSource<IDeparturesDirection[]>;
};

@injectable()
export class StaticDataSourceFactory {
    private readonly dataSourceDict: DataSourceDict;

    constructor(
        @inject(RopidGtfsContainerToken.MetroRailTrackDataSourceProvider)
        metroRailTrackDataSourceProvider: IDataSourceProvider<IMetroRailtrack[]>,
        @inject(RopidGtfsContainerToken.DeparturesDirectionDataSourceProvider)
        departuresDirectionDataSourceProvider: IDataSourceProvider<IDeparturesDirection[]>
    ) {
        this.dataSourceDict = {
            metroRailTrack: metroRailTrackDataSourceProvider.getDataSource(),
            departuresDirections: departuresDirectionDataSourceProvider.getDataSource(),
        };
    }

    public getDataSource<T extends keyof DataSourceDict>(descriptorProvider: T): DataSourceDict[T] {
        return this.dataSourceDict[descriptorProvider];
    }
}
