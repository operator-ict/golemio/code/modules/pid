import { PG_SCHEMA } from "#sch/const";
import { RopidGTFS } from "#sch/ropid-gtfs";
import { StopTimesDto } from "#sch/ropid-gtfs/models/StopTimesDto";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { IValidator, JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import Sequelize from "@golemio/core/dist/shared/sequelize";

export interface ITripStopsResult {
    trip_id: string;
    trip_stops: ITripStop[];
}

interface ITripStop {
    stop_id: string;
    stop_sequence: number;
    cis: number;
}

export class RopidGTFSStopTimesModel extends PostgresModel implements IModel {
    /** Model name */
    public name!: string;
    /** The Sequelize Model */
    public sequelizeModel!: Sequelize.ModelCtor<any>;
    /** The Sequelize Model for temporary table */
    protected tmpSequelizeModel!: Sequelize.ModelCtor<any> | null;
    /** Validation helper */
    protected validator!: IValidator;
    /** Type/Strategy of saving the data */
    protected savingType!: "insertOnly" | "insertOrUpdate";

    public constructor() {
        super(
            RopidGTFS.stop_times.name + "Model",
            {
                outputSequelizeAttributes: RopidGTFS.stop_times.outputSequelizeAttributes,
                pgTableName: RopidGTFS.stop_times.pgTableName,
                pgSchema: PG_SCHEMA,
                savingType: "insertOnly",
            },
            new JSONSchemaValidator(RopidGTFS.stop_times.name + "ModelValidator", StopTimesDto.jsonSchema)
        );
    }

    public findTripStops = async (tripIds: string[]): Promise<ITripStopsResult[]> => {
        const connection = PostgresConnector.getConnection();
        try {
            return await connection.query<ITripStopsResult>(
                `
                    SELECT t1.trip_id, array_to_json(array_agg(
                        json_build_object('stop_id', t1.stop_id, 'stop_sequence', t1.stop_sequence, 'cis', t2.cis)
                            ORDER BY t1.stop_sequence
                    )) as trip_stops
                    FROM ${PG_SCHEMA}."${this.tableName}" t1
                    LEFT JOIN ${PG_SCHEMA}."${RopidGTFS.cis_stops.pgTableName}" t2
                        on t1.stop_id = CONCAT('U',REPLACE(id,'/','Z'))
                    WHERE t1.trip_id in (${tripIds.map((tripId) => "'" + tripId + "'").join(",")})
                    GROUP BY t1.trip_id
                `,
                { type: Sequelize.QueryTypes.SELECT, raw: true }
            );
        } catch (err) {
            throw new GeneralError("Cannot find trip stops", this.name, err);
        }
    };
}
