import { MetaDatasetInfoKeyEnum, MetaStateEnum, MetaTypeEnum, RopidMetadataModel } from "#ie/shared";
import { RopidGTFS } from "#sch/ropid-gtfs";
import Sequelize, { BulkCreateOptions, DestroyOptions } from "@golemio/core/dist/shared/sequelize";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

export enum DatasetEnum {
    PID_GTFS = "PID_GTFS",
    CIS_STOPS = "CIS_STOPS",
    OIS_MAPPING = "OIS_MAPPING",
    RUN_NUMBERS = "RUN_NUMBERS",
    DEPARTURES_PRESETS = "DEPARTURES_PRESETS",
}

@injectable()
export class RopidGtfsMetadataRepository extends RopidMetadataModel {
    constructor() {
        super(RopidGTFS);
    }

    public getAllSaved = async (dataset: string, version: number): Promise<any[]> => {
        const tables = await this.sequelizeModel.findAll({
            attributes: [["key", "tn"]],
            where: {
                dataset,
                type: MetaTypeEnum.STATE,
                value: MetaStateEnum.SAVED,
                version,
            },
        });

        return tables;
    };

    public checkAllTablesHasSavedState = async (datasets: Array<{ dataset: string; version: number }>): Promise<boolean> => {
        const notSaved = await this.sequelizeModel.count({
            where: {
                [Sequelize.Op.or]: datasets.map((item) => ({
                    dataset: item.dataset,
                    version: item.version,
                    type: MetaTypeEnum.STATE,
                    value: { [Sequelize.Op.ne]: MetaStateEnum.SAVED },
                })),
            },
        });
        return notSaved === 0;
    };

    public checkIfNewVersionIsAlreadyDeployed = async (dataset: string, version: number): Promise<boolean> => {
        const alreadyDeployed = await this.sequelizeModel.count({
            where: {
                dataset,
                key: MetaDatasetInfoKeyEnum.DEPLOYED,
                type: MetaTypeEnum.DATASET_INFO,
                value: "true",
                version,
            },
        });
        return alreadyDeployed !== 0;
    };

    public updateState = async (dataset: string, name: string, state: MetaStateEnum, version: number): Promise<any> => {
        return this.sequelizeModel.update(
            {
                dataset,
                key: name,
                type: MetaTypeEnum.STATE,
                value: state,
                version,
            },
            {
                where: {
                    dataset,
                    key: name,
                    type: MetaTypeEnum.STATE,
                    version,
                },
            }
        );
    };

    public getNumberOfDownloadRetries = async (dataset: string, version: number): Promise<number> => {
        const numberOfRetries = await this.sequelizeModel.findOne({
            attributes: [["value", "retries"]],
            where: {
                dataset,
                key: MetaDatasetInfoKeyEnum.NUMBER_OF_RETRIES,
                type: MetaTypeEnum.DATASET_INFO,
                version: version - 1,
            },
        });

        if (!numberOfRetries) {
            return 0;
        }

        return +numberOfRetries.dataValues.retries;
    };

    public bulkCreate = async (data: any[], options: BulkCreateOptions): Promise<void> => {
        await this.sequelizeModel.bulkCreate(data, options);
    };

    public destroy = async (options: DestroyOptions): Promise<void> => {
        await this.sequelizeModel.destroy(options);
    };
}
