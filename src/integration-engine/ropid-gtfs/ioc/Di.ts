import { PidContainer } from "#ie/ioc/Di";
import { RopidGTFS } from "#sch/ropid-gtfs";
import { RedisPubSubChannel } from "@golemio/core/dist/integration-engine/data-access/pubsub";
import { DependencyContainer, Lifecycle } from "@golemio/core/dist/shared/tsyringe";
import { RopidGtfsFacade } from "../RopidGtfsFacade";
import { RopidGtfsMetadataRepository } from "../RopidGtfsMetadataRepository";
import { RopidGtfsRepository } from "../data-access/RopidGtfsRepository";
import { BlockStopsRedisRepository } from "../data-access/cache/BlockStopsRedisRepository";
import { PublicGtfsDepartureRepository } from "../data-access/cache/PublicGtfsDepartureRepository";
import { StaticFileRedisRepository } from "../data-access/cache/StaticFileRedisRepository";
import { DeparturesRepository } from "../data-access/precomputed";
import { DeparturePresetsFacade } from "../facade/DeparturePresetsFacade";
import { PublicDepartureCacheTransformation } from "../transformations/PublicDepartureCacheTransformation";
import { RopidGTFSTransformation } from "../transformations/RopidGTFSTransformation";
import { RefreshPublicGtfsDepartureCacheTask } from "../workers/timetables/tasks/RefreshPublicGtfsDepartureCacheTask";
import { PrecomputedTablesFacade } from "../workers/timetables/tasks/helpers/PrecomputedTablesFacade";
import { RopidGtfsFactory } from "../workers/timetables/tasks/helpers/RopidGtfsFactory";
import { RopidGtfsContainerToken } from "./RopidGtfsContainerToken";
import { DeparturesPresetsDataSource } from "../workers/presets/data-access/DeparturesPresetsDatasource";
import { CheckForNewDeparturesPresetsTask } from "../workers/presets/tasks/CheckForNewDeparturesPresetsTask";
import { DownloadDeparturesPresetsTask } from "../workers/presets/tasks/DownloadDeparturesPresetsTask";
import { RopidGTFSStopsModel } from "../RopidGTFSStopsModel";
import { StaticDataSourceFactory } from "../datasources/StaticDataSourceFactory";
import { DeparturesDirectionRepository } from "../workers/timetables/tasks/data-access/DeparturesDirectionRepository";
import { MetroRailtrackDataTransformation } from "../workers/timetables/tasks/transformations/MetroRailtrackDataTransformation";
import { MetroRailtrackGPSRepository } from "../workers/timetables/tasks/data-access/MetroRailtrackGPSRepository";
import { DeparturesDirectionDataSourceProvider } from "../datasources/static-data/DeparturesDirectionDataSourceProvider";
import { MetroRailTrackDataSourceProvider } from "../datasources/static-data/MetroRailTrackDataSourceProvider";
import { SaveStaticDataTask } from "../workers/timetables/tasks/SaveStaticDataTask";
import { DeparturesDirectionTransformation } from "../workers/timetables/tasks/transformations/DeparturesDirectionTransformation";

//#region Initialization
const RopidGtfsContainer: DependencyContainer = PidContainer.createChildContainer();
//#endregion

//#region Datasources
RopidGtfsContainer.registerSingleton(RopidGtfsContainerToken.BlockStopsRedisRepository, BlockStopsRedisRepository);
RopidGtfsContainer.registerSingleton(RopidGtfsContainerToken.DeparturesPresetsDataSource, DeparturesPresetsDataSource);
RopidGtfsContainer.registerSingleton(RopidGtfsContainerToken.StaticDataSourceFactory, StaticDataSourceFactory);
RopidGtfsContainer.registerSingleton(RopidGtfsContainerToken.MetroRailTrackDataSourceProvider, MetroRailTrackDataSourceProvider);
RopidGtfsContainer.registerSingleton(
    RopidGtfsContainerToken.DeparturesDirectionDataSourceProvider,
    DeparturesDirectionDataSourceProvider
);

//#endregion

//#region Repositories
RopidGtfsContainer.registerSingleton(
    RopidGtfsContainerToken.StaticFileRedisRepository,
    StaticFileRedisRepository
).registerSingleton(RopidGtfsContainerToken.RopidGtfsRepository, RopidGtfsRepository);
RopidGtfsContainer.register(RopidGtfsContainerToken.RopidGtfsMetadataRepository, RopidGtfsMetadataRepository);
RopidGtfsContainer.register(RopidGtfsContainerToken.DepartureRepository, DeparturesRepository);
RopidGtfsContainer.register(RopidGtfsContainerToken.PublicGtfsDepartureRepository, PublicGtfsDepartureRepository);
RopidGtfsContainer.register(RopidGtfsContainerToken.RopidGTFSStopsModel, RopidGTFSStopsModel);
RopidGtfsContainer.register(RopidGtfsContainerToken.DeparturesDirectionRepository, DeparturesDirectionRepository);

//#endregion

//#region Transformations
RopidGtfsContainer.registerSingleton(RopidGtfsContainerToken.RopidGTFSTransformation, RopidGTFSTransformation);
RopidGtfsContainer.registerSingleton(
    RopidGtfsContainerToken.PublicDepartureCacheTransformation,
    PublicDepartureCacheTransformation
);
RopidGtfsContainer.registerSingleton(RopidGtfsContainerToken.MetroRailtrackDataTransformation, MetroRailtrackDataTransformation);
RopidGtfsContainer.registerSingleton(RopidGtfsContainerToken.MetroRailtrackGPSRepository, MetroRailtrackGPSRepository);
RopidGtfsContainer.registerSingleton(
    RopidGtfsContainerToken.DeparturesDirectionTransformation,
    DeparturesDirectionTransformation
);

//#endregion

//#region Facade
RopidGtfsContainer.registerSingleton(RopidGtfsContainerToken.RopidGtfsFacade, RopidGtfsFacade);
RopidGtfsContainer.registerSingleton(RopidGtfsContainerToken.PrecomputedTablesFacade, PrecomputedTablesFacade)
    .register(RopidGtfsContainerToken.RopidGtfsFactory, RopidGtfsFactory, { lifecycle: Lifecycle.Transient })
    .registerInstance(RopidGtfsContainerToken.RedisPubSubChannel, new RedisPubSubChannel(RopidGTFS.name));

RopidGtfsContainer.registerSingleton(RopidGtfsContainerToken.DeparturePresetsFacade, DeparturePresetsFacade);
//#endregion

//#region Tasks
RopidGtfsContainer.registerSingleton(
    RopidGtfsContainerToken.RefreshPublicGtfsDepartureCacheTask,
    RefreshPublicGtfsDepartureCacheTask
);
RopidGtfsContainer.registerSingleton(RopidGtfsContainerToken.DownloadDeparturesPresetsTask, DownloadDeparturesPresetsTask);
RopidGtfsContainer.registerSingleton(RopidGtfsContainerToken.CheckForNewDeparturesPresetsTask, CheckForNewDeparturesPresetsTask);
RopidGtfsContainer.registerSingleton(RopidGtfsContainerToken.SaveStaticDataTask, SaveStaticDataTask);

//#endregion

export { RopidGtfsContainer };
