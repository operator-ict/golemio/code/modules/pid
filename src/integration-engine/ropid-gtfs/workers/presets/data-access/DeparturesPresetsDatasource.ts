import { IRopidDeparturesPreset, RopidDeparturesPresets } from "#sch/ropid-departures-presets";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { FTPProtocolStrategy, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { RetryDataSource } from "@golemio/core/dist/integration-engine/datasources/RetryDataSource";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { AccessOptions } from "basic-ftp";

export interface IDeparturesPresetsData {
    data: IRopidDeparturesPreset[];
}

@injectable()
export class DeparturesPresetsDataSource {
    private departurePresets: RetryDataSource<IDeparturesPresetsData>;

    constructor(@inject(CoreToken.SimpleConfig) private config: ISimpleConfig) {
        this.departurePresets = new RetryDataSource(
            RopidDeparturesPresets.name + "DataSource",
            new FTPProtocolStrategy({
                filename: this.config.getValue("module.RopidDeparturesPresetsFilename", ""),
                path: this.config.getValue("module.RopidDeparturesPresetsPath", ""),
                url: this.config.getValue("module.RopidFTP", "") as AccessOptions,
                encoding: "utf8",
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(RopidDeparturesPresets.name + "DataSource", RopidDeparturesPresets.datasourceJsonSchema)
        );
    }

    public getAll() {
        return this.departurePresets.getAll();
    }

    public getLastModified() {
        return this.departurePresets.getLastModified();
    }
}
