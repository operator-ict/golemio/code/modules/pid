import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { DeparturePresetsRepository } from "#ie/ropid-gtfs/data-access/DeparturePresetsRepository";
import { PG_SCHEMA } from "#sch/const";
import { PresetLogModel } from "#sch/ropid-departures-preset-logs/models";
import { IPresetLogOutputDto } from "#sch/ropid-departures-preset-logs/models/interfaces";
import { RopidDeparturesPresets } from "#sch/ropid-departures-presets";
import { PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import Sequelize, { ModelStatic, Op } from "@golemio/core/dist/shared/sequelize";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { IPresetLogRepository } from "./interfaces/IPresetLogRepository";

@injectable()
export class PresetLogRepository extends PostgresModel implements IPresetLogRepository {
    public sequelizeModel!: ModelStatic<PresetLogModel>;

    constructor(@inject(ModuleContainerToken.DeparturePresetsRepository) departurePresetsRepository: DeparturePresetsRepository) {
        super(
            "PresetLogPepository",
            {
                pgTableName: PresetLogModel.tableName,
                pgSchema: PG_SCHEMA,
                outputSequelizeAttributes: PresetLogModel.attributeModel,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator("PresetLogPepository", PresetLogModel.arrayJsonSchema)
        );

        this.sequelizeModel.hasOne(departurePresetsRepository["sequelizeModel"], {
            as: RopidDeparturesPresets.pgTableName,
            foreignKey: "route_name",
            sourceKey: "device_alias",
            constraints: false,
            scope: {
                is_testing: false,
            },
        });
    }

    public findUnprocessed = async (): Promise<PresetLogModel[]> => {
        try {
            return await this.sequelizeModel.findAll({
                attributes: ["id", "device_alias", "received_at"],
                include: [
                    {
                        association: RopidDeparturesPresets.pgTableName,
                        attributes: [],
                        required: true,
                    },
                ],
                where: {
                    is_processed: false,
                    received_at: {
                        [Op.gte]: Sequelize.literal(`NOW() - INTERVAL '10 MIN'`),
                    },
                },
            });
        } catch (err) {
            throw new GeneralError("Error while fetching unprocessed preset logs", this.constructor.name, err);
        }
    };

    public saveData = async (data: IPresetLogOutputDto[]): Promise<void> => {
        try {
            await this.validate(data);
            await this.sequelizeModel.bulkCreate(data, {
                ignoreDuplicates: true,
            });
        } catch (err) {
            throw new GeneralError("Error while creating preset logs", this.constructor.name, err);
        }
    };

    public markAsProcessed = async (logIds: string[]): Promise<void> => {
        try {
            await this.sequelizeModel.update(
                { is_processed: true },
                {
                    where: Sequelize.where(Sequelize.col("id"), {
                        [Op.in]: logIds,
                    }),
                }
            );
        } catch (err) {
            throw new GeneralError("Error while marking preset logs as processed", this.constructor.name, err);
        }
    };

    public deleteNHoursOldData = async (hours: number): Promise<number> => {
        try {
            return await this.sequelizeModel.destroy({
                where: {
                    received_at: {
                        [Op.lt]: Sequelize.literal(`NOW() - INTERVAL '${hours} HOURS'`),
                    },
                },
            });
        } catch (err) {
            throw new GeneralError("Error while purging preset logs", this.constructor.name, err);
        }
    };
}
