import { AbstractTask, AbstractWorker } from "@golemio/core/dist/integration-engine/workers";
import { CollectAndSaveLogsTask } from "./tasks/CollectAndSaveLogsTask";
import { ProcessAndSendLogsTask } from "./tasks/ProcessAndSendLogsTask";
import { SavePresetsDataTask } from "./tasks/SavePresetsDataTask";
import { AbstractTaskJsonSchema } from "@golemio/core/dist/integration-engine/workers/AbstractTaskJsonSchema";
import { RopidGtfsContainer } from "#ie/ropid-gtfs/ioc/Di";
import { RopidGtfsContainerToken } from "#ie/ropid-gtfs/ioc/RopidGtfsContainerToken";
import { CheckForNewDeparturesPresetsTask } from "./tasks/CheckForNewDeparturesPresetsTask";
import { DownloadDeparturesPresetsTask } from "./tasks/DownloadDeparturesPresetsTask";

export class PresetWorker extends AbstractWorker {
    protected name = "RopidPresets";

    constructor() {
        super();

        // Register tasks
        this.registerTask(new SavePresetsDataTask(this.getQueuePrefix()));
        this.registerTask(new CollectAndSaveLogsTask(this.getQueuePrefix()));
        this.registerTask(new ProcessAndSendLogsTask(this.getQueuePrefix()));

        this.registerTask(
            RopidGtfsContainer.resolve<CheckForNewDeparturesPresetsTask>(RopidGtfsContainerToken.CheckForNewDeparturesPresetsTask)
        );
        this.registerTask(
            RopidGtfsContainer.resolve<DownloadDeparturesPresetsTask>(RopidGtfsContainerToken.DownloadDeparturesPresetsTask)
        );
    }

    public registerTask = (task: AbstractTaskJsonSchema<any> | AbstractTask<any>): void => {
        super.registerTask(task);
        task.queuePrefix = this.getQueuePrefix();
    };
}
