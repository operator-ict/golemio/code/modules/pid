import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { IDataSourceProvider } from "#ie/shared/datasources/IDataSourceProvider";
import { ILogQueryData } from "#sch/ropid-departures-preset-logs/datasources/interfaces";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { PresetLogProvider } from "./helpers/PresetLogProviderEnum";
import {
    DataSourceProviderDict,
    DataSourceReturnType,
    IPresetLogDataSourceFactory,
} from "./interfaces/IPresetLogDataSourceFactory";

@injectable()
export class PresetLogDataSourceFactory implements IPresetLogDataSourceFactory {
    private readonly dataSourceProviderDict: DataSourceProviderDict;

    constructor(
        @inject(ModuleContainerToken.GrafanaLokiDataSourceProvider)
        grafanaLokiDataSourceProvider: IDataSourceProvider<ILogQueryData[]>
    ) {
        this.dataSourceProviderDict = {
            [PresetLogProvider.GrafanaLoki]: grafanaLokiDataSourceProvider,
        };
    }

    public getDataSource<T extends PresetLogProvider>(
        presetLogsProvider: T,
        ...params: Parameters<DataSourceProviderDict[T]["getDataSource"]>
    ): DataSourceReturnType<T> {
        return this.dataSourceProviderDict[presetLogsProvider].getDataSource(...params) as DataSourceReturnType<T>;
    }
}
