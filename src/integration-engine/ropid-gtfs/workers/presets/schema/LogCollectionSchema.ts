import { IsInt, Max, Min } from "@golemio/core/dist/shared/class-validator";
import { ILogCollectionParams } from "../interfaces/ILogCollectionParams";

export class LogCollectionValidationSchema implements ILogCollectionParams {
    @IsInt()
    @Min(1)
    @Max(120)
    targetMinutes!: number;
}
