import { ILogQueryData } from "#sch/ropid-departures-preset-logs/datasources/interfaces";
import { IPresetLogOutputDto } from "#sch/ropid-departures-preset-logs/models/interfaces";
import { BaseTransformation } from "@golemio/core/dist/integration-engine/transformations";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { IPresetLogTransformation } from "./interfaces/IPresetLogTransformation";

@injectable()
export class PresetLogTransformation extends BaseTransformation implements IPresetLogTransformation {
    public name = "PresetLogTransformation";

    public transform = (data: ILogQueryData[]): Promise<IPresetLogOutputDto[]> => {
        const dto: IPresetLogOutputDto[] = [];
        for (const dataItem of data) {
            const dtoItem = this.transformElement(dataItem as Required<ILogQueryData>);
            if (dtoItem) {
                dto.push(dtoItem);
            }
        }

        return Promise.resolve(dto);
    };

    protected transformElement = (dataItem: Required<ILogQueryData>): IPresetLogOutputDto => {
        // Use dummy hostname to parse relative URLs
        // we want to normalize the relative URL without query params, leading slashes and other irregularities
        const url = new URL(dataItem.stream.req_url, "http://golemio");
        const logTimestampInNanoseconds = BigInt(dataItem.values[0][0]);
        const receivedAtDateObject = new Date(Number(logTimestampInNanoseconds / BigInt(1000000)));

        return {
            received_at: receivedAtDateObject,
            device_alias: url.pathname.split("/pid/").at(-1)!.replace("/", ""),
            is_processed: false,
            request_url: dataItem.stream.req_url,
            request_method: dataItem.stream.req_method,
            request_user_agent: dataItem.stream.req_userAgent ?? "",
            response_status: Number.parseInt(dataItem.stream.res_status),
            response_time_ms: Number.parseInt(dataItem.stream.responseTime),
        };
    };
}
