import { ILogQueryData } from "#sch/ropid-departures-preset-logs/datasources/interfaces";
import { IPresetLogOutputDto } from "#sch/ropid-departures-preset-logs/models/interfaces";
import { ITransformation } from "@golemio/core/dist/integration-engine/transformations";

export interface IPresetLogTransformation extends ITransformation {
    transform: (data: ILogQueryData[]) => Promise<IPresetLogOutputDto[]>;
}
