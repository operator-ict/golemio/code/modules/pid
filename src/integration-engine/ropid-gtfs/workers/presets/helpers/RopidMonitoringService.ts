import { IPresetLogProcessedDto } from "#sch/ropid-departures-preset-logs/models/interfaces/IPresetLogProcessedDto";
import { IConfiguration } from "@golemio/core/dist/integration-engine/config";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc/ContainerToken";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { IRopidMonitoringService } from "./interfaces/IRopidMonitoringService";

@injectable()
export class RopidMonitoringService implements IRopidMonitoringService {
    constructor(@inject(ContainerToken.Config) private config: IConfiguration) {}

    public async sendLogsToMonitoringSystem(logEntries: IPresetLogProcessedDto[]): Promise<void> {
        const requestConfig = this.getRequestConfig(logEntries);

        try {
            const response = await fetch(this.config.datasources.RopidMonitoringPushUrl, requestConfig);
            if (response.status < 200 || response.status >= 300) {
                throw new GeneralError(
                    `Error while sending logs to the ROPID monitoring system. Status code: ${response.status}`,
                    this.constructor.name
                );
            }
        } catch (err) {
            if (err instanceof GeneralError) {
                throw err;
            }

            throw new GeneralError("Error while sending logs to the ROPID monitoring system", this.constructor.name, err);
        }
    }

    private getRequestConfig(logEntries: IPresetLogProcessedDto[]): RequestInit {
        return {
            method: "POST",
            headers: this.config.datasources.RopidMonitoringPushHeaders,
            body: JSON.stringify(logEntries),
            signal: AbortSignal.timeout(20 * 1000),
        };
    }
}
