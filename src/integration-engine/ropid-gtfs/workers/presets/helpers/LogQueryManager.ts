/**
 * This class is responsible for generating the LogQL queries to be used in the log search
 */
export class LogQueryManager {
    public static getCompletedPresetRequestLogsQuery(): string {
        return (
            this.getUrlProxyJsonQuery() +
            " | req_url =~ `\\/pid\\/[^\\/]*\\/?` | req_method = `GET` | message = `request completed`"
        );
    }

    private static getUrlProxyJsonQuery(): string {
        return '{container_name="url-proxy"} | json';
    }
}
