export class LogDateTimeUtils {
    public static parseISONoFractionalFromDate(dateObject: Date) {
        const isoTimestamp = dateObject.toISOString();
        return isoTimestamp.split(".")[0] + "Z";
    }
}
