import { ILogQueryData } from "#sch/ropid-departures-preset-logs/datasources/interfaces";

export interface ILogFilter {
    getValidLogEntries(logEntries: ILogQueryData[]): ILogQueryData[];
}
