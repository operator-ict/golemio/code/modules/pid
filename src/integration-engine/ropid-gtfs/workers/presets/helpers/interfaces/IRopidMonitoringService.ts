import { IPresetLogProcessedDto } from "#sch/ropid-departures-preset-logs/models/interfaces/IPresetLogProcessedDto";

export interface IRopidMonitoringService {
    sendLogsToMonitoringSystem(logEntries: IPresetLogProcessedDto[]): Promise<void>;
}
