import { ILogQueryData } from "#sch/ropid-departures-preset-logs/datasources/interfaces";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { ILogFilter } from "./interfaces/ILogFilter";

@injectable()
export class LogFilter implements ILogFilter {
    constructor(@inject(CoreToken.Logger) private logger: ILogger) {}

    public getValidLogEntries(logEntries: ILogQueryData[]): ILogQueryData[] {
        const validLogEntries: ILogQueryData[] = [];

        for (const logEntry of logEntries) {
            if (!logEntry.stream || !logEntry.values) {
                this.logger.warn(`${this.constructor.name}: log entry is missing stream or values`);
                this.logger.debug(logEntry);
                continue;
            }

            validLogEntries.push(logEntry);
        }

        return validLogEntries;
    }
}
