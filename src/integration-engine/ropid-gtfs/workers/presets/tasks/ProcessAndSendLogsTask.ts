import { PidContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { PresetLogModel } from "#sch/ropid-departures-preset-logs/models";
import { IPresetLogProcessedDto } from "#sch/ropid-departures-preset-logs/models/interfaces/IPresetLogProcessedDto";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { IoRedisConnector } from "@golemio/core/dist/helpers/data-access/redis/IoRedisConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc";
import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine/workers";
import { Mutex } from "@golemio/core/dist/shared/redis-semaphore";
import { IPresetLogRepository } from "../data-access/interfaces/IPresetLogRepository";
import { IRopidMonitoringService } from "../helpers/interfaces/IRopidMonitoringService";
import { LogDateTimeUtils } from "../helpers/LogDateTimeUtils";

const DEFAULT_BATCH_SIZE = 1000;
const DEFAULT_SEND_LOGS_LOCK_TIMEOUT = 60 * 1000;

export class ProcessAndSendLogsTask extends AbstractEmptyTask {
    public readonly queueName = "processAndSendLogs";
    public readonly queueTtl = 59 * 1000; // 59 seconds
    private readonly LOCK_KEY_PHRASE = "ropidGTFSProcessAndSendLogsTask";
    private lockTimeout: number;
    private readonly refreshInterval: number;

    private logRepository: IPresetLogRepository;
    private monitoringService: IRopidMonitoringService;
    private logger: ILogger;
    private redisClient: IoRedisConnector;
    private batchSize: number;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        const config = PidContainer.resolve<ISimpleConfig>(CoreToken.SimpleConfig);

        this.logRepository = PidContainer.resolve<IPresetLogRepository>(ModuleContainerToken.PresetLogRepository);
        this.monitoringService = PidContainer.resolve<IRopidMonitoringService>(ModuleContainerToken.RopidMonitoringService);
        this.redisClient = PidContainer.resolve<IoRedisConnector>(ContainerToken.RedisConnector);
        this.logger = PidContainer.resolve<ILogger>(CoreToken.Logger);

        const batchSize = config.getValue("env.ROPID_PRESET_LOG_BATCH_SIZE", DEFAULT_BATCH_SIZE.toString());
        this.batchSize = Number.parseInt(batchSize);

        const lockTimeout = config.getValue("env.ROPID_PRESET_SEND_LOGS_LOCK_TIMEOUT", DEFAULT_SEND_LOGS_LOCK_TIMEOUT.toString());
        this.lockTimeout = Number.parseInt(lockTimeout);
        this.refreshInterval = this.lockTimeout * 0.8; // recommended
    }

    protected async execute() {
        const mutex = this.createMutex();
        const lockAcquired = await mutex.tryAcquire();

        if (!lockAcquired) {
            this.logger.error(`${this.constructor.name}: unable to obtain mutex lock.`);
            return;
        }

        try {
            const logEntries = await this.logRepository.findUnprocessed();
            if (logEntries.length === 0) {
                this.logger.warn(`${this.constructor.name}: no logs found to send to the ROPID monitoring system`);
                return;
            }

            // Process logs in batches to avoid exceeding the maximum allowed size of the request
            for (let i = 0; i < logEntries.length; i += this.batchSize) {
                try {
                    const batchedLogs = logEntries.slice(i, i + this.batchSize);
                    await this.processBatch(batchedLogs);
                } catch (err) {
                    this.logger.warn(err, `${this.constructor.name}: error while processing DCIP system logs`);
                }
            }
        } finally {
            await mutex.release();
        }
    }

    private async processBatch(logEntries: PresetLogModel[]) {
        let logIds: string[] = [];
        let logData: IPresetLogProcessedDto[] = [];

        for (const logEntry of logEntries) {
            const { device_alias, received_at } = logEntry.toJSON();

            logIds.push(logEntry.get("id") as string);
            logData.push({
                deviceAlias: device_alias,
                receivedAt: LogDateTimeUtils.parseISONoFractionalFromDate(received_at),
            });
        }

        await this.monitoringService.sendLogsToMonitoringSystem(logData);
        this.logger.info(`${this.constructor.name}: ${logData.length} logs sent to the ROPID monitoring system`);
        await this.logRepository.markAsProcessed(logIds);
    }

    private createMutex() {
        return new Mutex(this.redisClient.getConnection(), this.LOCK_KEY_PHRASE, {
            acquireAttemptsLimit: 1,
            lockTimeout: this.lockTimeout,
            refreshInterval: this.refreshInterval,
            onLockLost: (err) => {
                this.logger.error(err, `${this.constructor.name}: mutex lock was lost`);
            },
        });
    }
}
