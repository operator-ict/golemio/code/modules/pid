import { RopidGtfsMetadataRepository } from "#ie/ropid-gtfs/RopidGtfsMetadataRepository";
import { RopidGtfsContainerToken } from "#ie/ropid-gtfs/ioc/RopidGtfsContainerToken";
import { AbstractEmptyTask, QueueManager } from "@golemio/core/dist/integration-engine";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { DatasetEnum } from "../../../RopidGtfsFacade";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { DeparturesPresetsDataSource } from "../data-access/DeparturesPresetsDatasource";
import { RopidGTFS } from "#sch/ropid-gtfs";

@injectable()
export class CheckForNewDeparturesPresetsTask extends AbstractEmptyTask {
    public readonly queueName = "checkForNewDeparturesPresets";
    public readonly queueTtl = 59 * 1000 * 4; // 4 minutes

    constructor(
        @inject(RopidGtfsContainerToken.RopidGtfsMetadataRepository) private metadataRepository: RopidGtfsMetadataRepository,
        @inject(RopidGtfsContainerToken.DeparturesPresetsDataSource)
        private departuresPresetsDataSource: DeparturesPresetsDataSource
    ) {
        super(RopidGTFS.name.toLowerCase());
    }

    protected async execute() {
        try {
            const departuresPresetsServerLastModified = await this.departuresPresetsDataSource.getLastModified();
            const departuresPresetsDbLastModified = await this.metadataRepository.getLastModified(DatasetEnum.DEPARTURES_PRESETS);
            if (departuresPresetsServerLastModified !== departuresPresetsDbLastModified.lastModified) {
                await QueueManager.sendMessageToExchange(this.queuePrefix, "downloadDeparturesPresets", {});
            }
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            } else {
                throw new GeneralError("Error while checking for new departures presets", this.constructor.name, err);
            }
        }
    }
}
