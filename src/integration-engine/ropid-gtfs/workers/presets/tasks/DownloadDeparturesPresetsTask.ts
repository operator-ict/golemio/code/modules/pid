import { DeparturePresetsFacade } from "#ie/ropid-gtfs/facade/DeparturePresetsFacade";
import { RopidGtfsContainerToken } from "#ie/ropid-gtfs/ioc/RopidGtfsContainerToken";
import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { DeparturesPresetsDataSource } from "../data-access/DeparturesPresetsDatasource";
import { IRopidDeparturesPreset } from "#sch/ropid-departures-presets";
import { RopidGTFS } from "#sch/ropid-gtfs";

@injectable()
export class DownloadDeparturesPresetsTask extends AbstractEmptyTask {
    queueName = "downloadDeparturesPresets";
    queueTtl = 4 * 60 * 1000; // 4 minutes

    constructor(
        @inject(RopidGtfsContainerToken.DeparturesPresetsDataSource)
        private departuresPresetsDataSource: DeparturesPresetsDataSource,
        @inject(RopidGtfsContainerToken.DeparturePresetsFacade) private facade: DeparturePresetsFacade
    ) {
        super(RopidGTFS.name.toLowerCase());
    }

    protected async execute() {
        try {
            const file = await this.departuresPresetsDataSource.getAll();
            const lastModified = await this.departuresPresetsDataSource.getLastModified();
            await this.facade.handleNewDeparturePresets(file.data, lastModified);
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            } else {
                throw new GeneralError("Error while downloading departures presets: " + err.message, this.constructor.name, err);
            }
        }
    }
}
