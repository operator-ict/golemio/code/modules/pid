import { PidContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { ILogQueryData } from "#sch/ropid-departures-preset-logs/datasources/interfaces";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger";
import { QueueManager } from "@golemio/core/dist/integration-engine/queueprocessors";
import { AbstractTask } from "@golemio/core/dist/integration-engine/workers";
import { AbstractGolemioError, DatasourceError } from "@golemio/core/dist/shared/golemio-errors";
import { IPresetLogRepository } from "../data-access/interfaces/IPresetLogRepository";
import { PresetLogProvider } from "../datasources/helpers/PresetLogProviderEnum";
import { IPresetLogDataSourceFactory } from "../datasources/interfaces/IPresetLogDataSourceFactory";
import { LogQueryManager } from "../helpers/LogQueryManager";
import { ILogFilter } from "../helpers/interfaces/ILogFilter";
import { ILogCollectionParams } from "../interfaces/ILogCollectionParams";
import { LogCollectionValidationSchema } from "../schema/LogCollectionSchema";
import { IPresetLogTransformation } from "../transformations/interfaces/IPresetLogTransformation";

export class CollectAndSaveLogsTask extends AbstractTask<ILogCollectionParams> {
    public readonly queueName = "collectAndSaveLogs";
    public readonly queueTtl = 59 * 1000; // 59 seconds
    public readonly schema = LogCollectionValidationSchema;

    private dataSourceFactory: IPresetLogDataSourceFactory;
    private logTransformation: IPresetLogTransformation;
    private logRepository: IPresetLogRepository;
    private logFilter: ILogFilter;
    private logger: ILogger;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.dataSourceFactory = PidContainer.resolve<IPresetLogDataSourceFactory>(
            ModuleContainerToken.PresetLogDataSourceFactory
        );
        this.logTransformation = PidContainer.resolve<IPresetLogTransformation>(ModuleContainerToken.PresetLogTransformation);
        this.logRepository = PidContainer.resolve<IPresetLogRepository>(ModuleContainerToken.PresetLogRepository);
        this.logFilter = PidContainer.resolve<ILogFilter>(ModuleContainerToken.PresetLogFilter);
        this.logger = PidContainer.resolve<ILogger>(CoreToken.Logger);
    }

    protected async execute(data: ILogCollectionParams) {
        const currentTimestampInSeconds = Math.round(new Date().getTime() / 1000);
        const dataSource = this.dataSourceFactory.getDataSource(PresetLogProvider.GrafanaLoki, {
            start: currentTimestampInSeconds - data.targetMinutes * 60,
            end: currentTimestampInSeconds,
            query: LogQueryManager.getCompletedPresetRequestLogsQuery(),
        });

        let queryResult: ILogQueryData[] = [];

        try {
            queryResult = await dataSource.getAll();
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            }

            throw new DatasourceError("Error while fetching preset logs", this.constructor.name, err);
        }

        const validLogEntries = this.logFilter.getValidLogEntries(queryResult);
        if (validLogEntries.length > 0) {
            const presetLogDtos = await this.logTransformation.transform(queryResult);
            this.logger.info(`${this.constructor.name}: ${presetLogDtos.length} log entries collected`);

            await this.logRepository.saveData(presetLogDtos);
        } else {
            this.logger.warn(`${this.constructor.name}: no valid log entries collected`);
        }

        await QueueManager.sendMessageToExchange(this.queuePrefix, "processAndSendLogs", {});
    }
}
