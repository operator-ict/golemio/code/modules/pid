import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine/workers";
import { StaticDataSourceFactory } from "#ie/ropid-gtfs/datasources/StaticDataSourceFactory";
import { RopidGtfsContainerToken } from "#ie/ropid-gtfs/ioc/RopidGtfsContainerToken";
import { DeparturesDirectionRepository } from "./data-access/DeparturesDirectionRepository";
import { MetroRailtrackDataTransformation } from "./transformations/MetroRailtrackDataTransformation";
import { MetroRailtrackGPSRepository } from "./data-access/MetroRailtrackGPSRepository";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { DeparturesDirectionTransformation } from "./transformations/DeparturesDirectionTransformation";
import { TIMETABLE_WORKER_NAME } from "../constants";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";

@injectable()
export class SaveStaticDataTask extends AbstractEmptyTask {
    public readonly queueName = "saveStaticData";
    public readonly queueTtl = 5 * 60 * 1000; // 5 minutes

    constructor(
        @inject(RopidGtfsContainerToken.StaticDataSourceFactory)
        private staticDataSourceFactory: StaticDataSourceFactory,
        @inject(RopidGtfsContainerToken.DeparturesDirectionRepository)
        private departuresDirectionRepository: DeparturesDirectionRepository,
        @inject(RopidGtfsContainerToken.MetroRailtrackDataTransformation)
        private railtrackDataTransformation: MetroRailtrackDataTransformation,
        @inject(RopidGtfsContainerToken.MetroRailtrackGPSRepository)
        private railtrackGPSRepository: MetroRailtrackGPSRepository,
        @inject(RopidGtfsContainerToken.DeparturesDirectionTransformation)
        private departuresDirectionTransformation: DeparturesDirectionTransformation
    ) {
        super(TIMETABLE_WORKER_NAME);
    }

    protected async execute() {
        try {
            const departuresDirections = await this.staticDataSourceFactory.getDataSource("departuresDirections").getAll();
            const departuresDirectionsDto = this.departuresDirectionTransformation.transformArray(departuresDirections);
            await this.departuresDirectionRepository.saveData(departuresDirectionsDto);
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            }
            throw new GeneralError("Error while refreshing departures directions", this.constructor.name, err);
        }

        try {
            const metroRailTrack = await this.staticDataSourceFactory.getDataSource("metroRailTrack").getAll();
            const railtrackGPSData = this.railtrackDataTransformation.transformArray(metroRailTrack);
            await this.railtrackGPSRepository.saveData(railtrackGPSData);
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            }
            throw new GeneralError("Error while refreshing metro rail tracks", this.constructor.name, err);
        }
    }
}
