import { PublicGtfsDepartureRepository } from "#ie/ropid-gtfs/data-access/cache/PublicGtfsDepartureRepository";
import { DeparturesRepository } from "#ie/ropid-gtfs/data-access/precomputed";
import { IPublicDepartureDto } from "#ie/ropid-gtfs/interfaces/IPublicDepartureDto";
import { RopidGtfsContainerToken } from "#ie/ropid-gtfs/ioc/RopidGtfsContainerToken";
import { PublicDepartureCacheTransformation } from "#ie/ropid-gtfs/transformations/PublicDepartureCacheTransformation";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { AbstractTask } from "@golemio/core/dist/integration-engine/workers";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { TIMETABLE_WORKER_NAME } from "../constants";
import { IRefreshPublicDepartureParams } from "./interfaces/IRefreshPublicDepartureParams";
import { RefreshPublicDepartureValidationSchema } from "./schema/RefreshPublicDepartureSchema";
import { RopidGTFSStopsModel } from "#ie/ropid-gtfs/RopidGTFSStopsModel";

const DEFAULT_PAGE_SIZE = 50000;

@injectable()
export class RefreshPublicGtfsDepartureCacheTask extends AbstractTask<IRefreshPublicDepartureParams> {
    public readonly queueName = "refreshPublicGtfsDepartureCache";
    public readonly queueTtl = 59 * 60 * 1000; // 59 minutes
    public readonly schema = RefreshPublicDepartureValidationSchema;
    private readonly departurePageSize: number;

    constructor(
        @inject(RopidGtfsContainerToken.PublicDepartureCacheTransformation)
        private departureTransformation: PublicDepartureCacheTransformation,
        @inject(RopidGtfsContainerToken.DepartureRepository) private departureRepository: DeparturesRepository,
        @inject(RopidGtfsContainerToken.PublicGtfsDepartureRepository)
        private publicDepartureRepository: PublicGtfsDepartureRepository,
        @inject(CoreToken.SimpleConfig) private config: ISimpleConfig,
        @inject(RopidGtfsContainerToken.RopidGTFSStopsModel) private stopsModel: RopidGTFSStopsModel
    ) {
        super(TIMETABLE_WORKER_NAME);

        const departurePageSize = this.config.getValue(
            "module.pid.ropid-gtfs.timetables.refreshPublicGtfsDepartureCache.departurePageSize",
            DEFAULT_PAGE_SIZE.toString()
        );
        this.departurePageSize = Number.parseInt(departurePageSize);
    }

    protected async execute(params: IRefreshPublicDepartureParams) {
        const intervalParams = this.getIntervalParamsWithDefault(params);
        const numberOfDepartures = await this.departureRepository.countDeparturesForPublicCache(intervalParams);
        const pages = Math.ceil(numberOfDepartures / this.departurePageSize);

        const stopIdsInDepartures: Set<string> = new Set();
        for (let page = 0; page < pages; page++) {
            const departuresForCache = await this.departureRepository.getDepaturesForPublicCache(
                page,
                this.departurePageSize,
                intervalParams
            );
            const departuresByStopIdDict = this.getDeparturesByStopId(departuresForCache);

            for (const [stopId, departures] of departuresByStopIdDict.entries()) {
                stopIdsInDepartures.add(stopId);
                try {
                    const departuresDto = this.departureTransformation.transformArray(departures);
                    await this.publicDepartureRepository.replaceDeparturesForStop(departuresDto, stopId, intervalParams);
                } catch (err) {
                    if (err instanceof AbstractGolemioError) {
                        throw err;
                    }

                    throw new GeneralError("Error while refreshing public departure cache", this.constructor.name, err);
                }
            }
        }
        const allStops = await this.stopsModel.getAll();

        for (const { stop_id: id } of allStops) {
            if (!stopIdsInDepartures.has(id)) {
                await this.publicDepartureRepository.replaceDeparturesForStop([], id, intervalParams);
            }
        }
    }

    private getIntervalParamsWithDefault(params: IRefreshPublicDepartureParams): Required<IRefreshPublicDepartureParams> {
        const defaultIntervalFromHours = this.config.getValue(
            "module.pid.ropid-gtfs.timetables.refreshPublicGtfsDepartureCache.defaultIntervalFromHours",
            "-3"
        );

        const defaultIntervalToHours = this.config.getValue(
            "module.pid.ropid-gtfs.timetables.refreshPublicGtfsDepartureCache.defaultIntervalToHours",
            "6"
        );

        return {
            intervalFromHours: params.intervalFromHours ?? Number.parseInt(defaultIntervalFromHours),
            intervalToHours: params.intervalToHours ?? Number.parseInt(defaultIntervalToHours),
        };
    }

    private getDeparturesByStopId(departures: IPublicDepartureDto[]) {
        const departuresByStopId = new Map<string, IPublicDepartureDto[]>();
        for (const departure of departures) {
            if (!departuresByStopId.has(departure.stop_id)) {
                departuresByStopId.set(departure.stop_id, []);
            }

            departuresByStopId.get(departure.stop_id)!.push(departure);
        }

        return departuresByStopId;
    }
}
