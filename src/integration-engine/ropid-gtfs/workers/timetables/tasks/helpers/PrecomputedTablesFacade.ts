import { RopidGtfsRepository } from "#ie/ropid-gtfs/data-access/RopidGtfsRepository";
import { DeparturesRepository } from "#ie/ropid-gtfs/data-access/precomputed/DeparturesRepository";
import { MinMaxStopSequencesRepository } from "#ie/ropid-gtfs/data-access/precomputed/MinMaxStopSequencesRepository";
import { ServicesCalendarRepository } from "#ie/ropid-gtfs/data-access/precomputed/ServicesCalendarRepository";
import { TripScheduleRepository } from "#ie/ropid-gtfs/data-access/precomputed/TripScheduleRepository";
import { SourceTableSuffixEnum } from "#ie/ropid-gtfs/helpers/SourceTableSuffixEnum";
import { RopidGtfsContainerToken } from "#ie/ropid-gtfs/ioc/RopidGtfsContainerToken";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class PrecomputedTablesFacade {
    /** Precomputed data repositories */
    private minMaxStopSequencesRepository: MinMaxStopSequencesRepository;
    private tripScheduleRepository: TripScheduleRepository;
    private servicesCalendarRepository: ServicesCalendarRepository;

    constructor(
        @inject(CoreToken.Logger) private readonly logger: ILogger,
        @inject(RopidGtfsContainerToken.RopidGtfsRepository) private readonly ropidGtfsRepository: RopidGtfsRepository,
        @inject(RopidGtfsContainerToken.DepartureRepository) private readonly departureRepository: DeparturesRepository
    ) {
        /** Precomputed data repositories */
        this.minMaxStopSequencesRepository = new MinMaxStopSequencesRepository();
        this.tripScheduleRepository = new TripScheduleRepository();
        this.servicesCalendarRepository = new ServicesCalendarRepository();
    }

    public createAndPopulatePrecomputedTmpTables = async (sourceTableSuffix: SourceTableSuffixEnum): Promise<void> => {
        await this.ropidGtfsRepository.createPrecomputedTmpTables();

        this.logger.info(
            `Datasets: populating temp table ${this.servicesCalendarRepository["tableName"]}${SourceTableSuffixEnum.Tmp}`
        );
        await this.servicesCalendarRepository.populate(sourceTableSuffix);

        this.logger.info(
            `Datasets: populating temp table ${this.minMaxStopSequencesRepository["tableName"]}${SourceTableSuffixEnum.Tmp}`
        );
        await this.minMaxStopSequencesRepository.populate(sourceTableSuffix);

        this.logger.info(
            `Datasets: populating temp table ${this.tripScheduleRepository["tableName"]}${SourceTableSuffixEnum.Tmp}`
        );
        await this.tripScheduleRepository.populate(sourceTableSuffix);

        this.logger.info(`Datasets: populating temp table ${this.departureRepository["tableName"]}${SourceTableSuffixEnum.Tmp}`);
        await this.departureRepository.createAndPopulate(sourceTableSuffix);

        this.logger.info(`Datasets: running analyze on  ${this.departureRepository["tableName"]}${SourceTableSuffixEnum.Tmp}`);
        await this.departureRepository.analyze();
    };
}
