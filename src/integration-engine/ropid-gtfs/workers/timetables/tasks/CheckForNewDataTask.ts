import { RopidGtfsMetadataRepository } from "#ie/ropid-gtfs/RopidGtfsMetadataRepository";
import { RopidGtfsContainer } from "#ie/ropid-gtfs/ioc/Di";
import { RopidGtfsContainerToken } from "#ie/ropid-gtfs/ioc/RopidGtfsContainerToken";
import { ICheckForNewDataInput } from "#ie/ropid-gtfs/workers/timetables/tasks/interfaces/ICheckForNewDataInput";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { QueueManager } from "@golemio/core/dist/integration-engine";
import { AbstractTask } from "@golemio/core/dist/integration-engine/workers";
import { GTFSDatasets } from "./helpers/HelperTypes";
import { RopidGtfsFactory } from "./helpers/RopidGtfsFactory";
import { IDatasetsInput } from "./interfaces/IDatasetsInput";
import { CheckForNewDataMessageValidation } from "./schema/CheckForNewDataMessageValidation";

export class CheckForNewDataTask extends AbstractTask<ICheckForNewDataInput> {
    public readonly queueName = "checkForNewData";
    public readonly queueTtl = 19 * 60 * 1000; // 19 minutes
    public readonly schema = CheckForNewDataMessageValidation;
    private logger: ILogger;

    private metadataRepository: RopidGtfsMetadataRepository;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.logger = RopidGtfsContainer.resolve<ILogger>(CoreToken.Logger);
        this.metadataRepository = RopidGtfsContainer.resolve<RopidGtfsMetadataRepository>(
            RopidGtfsContainerToken.RopidGtfsMetadataRepository
        );
    }

    protected async execute(data: ICheckForNewDataInput) {
        let forceRefresh = !!data.forceRefresh;
        let outdatedDatasetCount = 0;
        const ropidGtfsFactory = RopidGtfsContainer.resolve<RopidGtfsFactory>(RopidGtfsContainerToken.RopidGtfsFactory);
        const datasetMap = ropidGtfsFactory.getDatasetMap();
        const datasetList = Object.keys(datasetMap) as GTFSDatasets[];

        for (const dataset of datasetList) {
            const serverLastModified = await datasetMap[dataset].datasource.getLastModified();
            const dbLastModified = await this.metadataRepository.getLastModified(dataset);
            const isDeployed = await this.metadataRepository.isDeployed(dataset);

            if (serverLastModified !== dbLastModified.lastModified || !isDeployed) {
                outdatedDatasetCount++;
            }
        }

        if (outdatedDatasetCount !== datasetList.length) {
            this.logger.warn(`RopidGTFS datasets are out of sync`);
        }

        if (outdatedDatasetCount > 0) {
            // NOTE: We are sending all datasets to download even if only some of them are outdated
            // to avoid unwanted side effects (such as empty precomputed tables)
            await QueueManager.sendMessageToExchange(this.queuePrefix, "downloadDatasets", {
                datasets: datasetList,
            } as IDatasetsInput);
        } else {
            this.logger.info("RopidGTFS datasets are up to date");
            if (forceRefresh) {
                await QueueManager.sendMessageToExchange(this.queuePrefix, "refreshPrecomputedTables", {});
            }
        }
        await QueueManager.sendMessageToExchange(this.queuePrefix, "saveStaticData", {});
    }
}
