import { DatasetEnum } from "#ie/ropid-gtfs";
import { IsEnum, IsOptional, IsString } from "@golemio/core/dist/shared/class-validator";
import { GTFSDatasets, ICachedDataset } from "../helpers/HelperTypes";

export class CachedDatasetSchema implements ICachedDataset {
    @IsString()
    filepath!: string;
    @IsString()
    name!: string;
    @IsOptional()
    @IsString()
    mtime?: string | undefined;
    @IsOptional()
    @IsString()
    type?: string | undefined;
    @IsEnum(DatasetEnum)
    dataset!: GTFSDatasets;
}
