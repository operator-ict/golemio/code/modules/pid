import { IsInt, IsOptional, Min } from "@golemio/core/dist/shared/class-validator";
import { IRefreshPublicDepartureParams } from "../interfaces/IRefreshPublicDepartureParams";

export class RefreshPublicDepartureValidationSchema implements IRefreshPublicDepartureParams {
    @IsOptional()
    @IsInt()
    // we do not want to have older departures than 3 hours in the cache for now
    // see the PublicGtfsDepartureRepository.replaceDeparturesForStop method
    @Min(-3)
    intervalFromHours?: number;

    @IsOptional()
    @IsInt()
    intervalToHours?: number;
}
