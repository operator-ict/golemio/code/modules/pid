import { IsBoolean, IsOptional } from "@golemio/core/dist/shared/class-validator";
import { ICheckForNewDataInput } from "../interfaces/ICheckForNewDataInput";

export class CheckForNewDataMessageValidation implements ICheckForNewDataInput {
    @IsOptional()
    @IsBoolean()
    forceRefresh?: boolean | undefined;
}
