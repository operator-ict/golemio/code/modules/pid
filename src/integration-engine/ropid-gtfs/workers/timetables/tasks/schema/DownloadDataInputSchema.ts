import { DatasetEnum } from "#ie/ropid-gtfs";
import { IsArray, IsEnum } from "@golemio/core/dist/shared/class-validator";
import { GTFSDatasets } from "../helpers/HelperTypes";
import { IDatasetsInput } from "../interfaces/IDatasetsInput";

export class DatasetsInputSchema implements IDatasetsInput {
    @IsArray()
    @IsEnum(DatasetEnum, { each: true })
    datasets!: GTFSDatasets[];
}
