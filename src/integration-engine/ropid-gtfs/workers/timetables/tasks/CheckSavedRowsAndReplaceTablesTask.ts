import { RopidGtfsFacade } from "#ie/ropid-gtfs/RopidGtfsFacade";
import { RopidGtfsMetadataRepository } from "#ie/ropid-gtfs/RopidGtfsMetadataRepository";
import { RopidGtfsRepository } from "#ie/ropid-gtfs/data-access/RopidGtfsRepository";
import { DataCacheManager } from "#ie/ropid-gtfs/helpers/DataCacheManager";
import { SourceTableSuffixEnum } from "#ie/ropid-gtfs/helpers/SourceTableSuffixEnum";
import { RopidGtfsContainer } from "#ie/ropid-gtfs/ioc/Di";
import { RopidGtfsContainerToken } from "#ie/ropid-gtfs/ioc/RopidGtfsContainerToken";
import { ILastModifiedInfo } from "#ie/shared";
import { WORKER_NAME as VP_WORKER_NAME } from "#ie/vehicle-positions/workers/vehicle-positions/constants";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { IntegrationErrorHandler, QueueManager } from "@golemio/core/dist/integration-engine";
import { AbstractTask } from "@golemio/core/dist/integration-engine/workers";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { GTFSDatasets } from "./helpers/HelperTypes";
import { PrecomputedTablesFacade } from "./helpers/PrecomputedTablesFacade";
import { IDatasetsInput } from "./interfaces/IDatasetsInput";
import { DatasetsInputSchema } from "./schema/DownloadDataInputSchema";

export class CheckSavedRowsAndReplaceTablesTask extends AbstractTask<IDatasetsInput> {
    public readonly queueName = "checkSavedRowsAndReplaceTables";
    public readonly queueTtl = 23 * 60 * 60 * 1000; // 23 hours
    public readonly schema = DatasetsInputSchema;
    private logger: ILogger;
    private config: ISimpleConfig;

    private metadataRepository: RopidGtfsMetadataRepository;
    private ropidGtfsRepository: RopidGtfsRepository;

    private dataCacheManager: DataCacheManager;
    private precomputeTablesFacade: PrecomputedTablesFacade;
    private ropidGtfsFacade: RopidGtfsFacade;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.logger = RopidGtfsContainer.resolve<ILogger>(CoreToken.Logger);
        this.config = RopidGtfsContainer.resolve<ISimpleConfig>(CoreToken.SimpleConfig);

        this.metadataRepository = RopidGtfsContainer.resolve<RopidGtfsMetadataRepository>(
            RopidGtfsContainerToken.RopidGtfsMetadataRepository
        );
        this.dataCacheManager = DataCacheManager.getInstance();
        this.precomputeTablesFacade = RopidGtfsContainer.resolve<PrecomputedTablesFacade>(
            RopidGtfsContainerToken.PrecomputedTablesFacade
        );
        this.ropidGtfsRepository = RopidGtfsContainer.resolve<RopidGtfsRepository>(RopidGtfsContainerToken.RopidGtfsRepository);
        this.ropidGtfsFacade = RopidGtfsContainer.resolve<RopidGtfsFacade>(RopidGtfsContainerToken.RopidGtfsFacade);
    }

    protected async execute(data: IDatasetsInput) {
        const { notDeployed, datasetDeployInfo } = await this.getDatasetsToDeploy(data);

        if (data.datasets.length !== notDeployed.length) {
            const names = notDeployed.map((el) => el.dataset);
            this.logger.error(`Error while synchronized download of datasets: [${names}] of [${data.datasets}] deployed already`);
        }

        if (!notDeployed.length) {
            this.logger.debug(`Datasets status: already deployed`);
            return;
        }

        const notDeployedVersions = notDeployed.map((el) => ({ dataset: el.dataset, version: el.lastModified.version }));
        const allSaved = await this.metadataRepository.checkAllTablesHasSavedState(notDeployedVersions);

        try {
            await this.checkSavedRows(allSaved, datasetDeployInfo);
            await this.precomputeTablesFacade.createAndPopulatePrecomputedTmpTables(SourceTableSuffixEnum.Tmp);
            await this.ropidGtfsFacade.replaceTables(notDeployedVersions);
            await this.dataCacheManager.cleanCache();
            await this.ropidGtfsRepository.cleanTmpAndOldTables();

            const rabbitExchangeName = this.config.getValue<string>("env.RABBIT_EXCHANGE_NAME");

            // send message to VP worker to refresh GTFS trip data (see task description for more info)
            await QueueManager.sendMessageToExchange(
                `${rabbitExchangeName}.${VP_WORKER_NAME.toLowerCase()}`,
                "refreshGTFSTripData",
                {}
            );

            // send message to GTFS timetable worker to refresh public GTFS departure cache
            //   so that the new data is available in the public (Lítačka) API
            await QueueManager.sendMessageToExchange(this.queuePrefix, "refreshPublicGtfsDepartureCache", {});
        } catch (err) {
            await this.retryOrThrow(notDeployedVersions, data, err);
        }
    }

    private async retryOrThrow(
        notDeployedVersions: Array<{ dataset: GTFSDatasets; version: number }>,
        data: IDatasetsInput,
        err: any
    ) {
        // log failed saving process
        await Promise.all(
            notDeployedVersions.map(async (el) => {
                await this.metadataRepository.rollbackFailedSaving(el.dataset, el.version);
            })
        );
        // check number of tries
        const retries = await Promise.all(
            notDeployedVersions.map(async (el) => {
                return await this.metadataRepository.getNumberOfDownloadRetries(el.dataset, el.version);
            })
        );

        await this.ropidGtfsRepository.cleanTmpAndOldTables();

        if (Math.max(...retries) <= 5) {
            // send new downloadFiles, log it and finish
            QueueManager.sendMessageToExchange(this.queuePrefix, "downloadDatasets", data);

            IntegrationErrorHandler.handle(
                new GeneralError(
                    `Error while checking RopidGTFS saved rows. Attempt number ${Math.min(...retries)} was resent.`,
                    this.constructor.name,
                    err
                )
            );
        } else {
            // at least refresh precomputed tables and finish with error
            QueueManager.sendMessageToExchange(this.queuePrefix, "refreshPrecomputedTables", {});

            throw new GeneralError("Error while checking RopidGTFS saved rows.", this.constructor.name, err);
        }
    }

    private async checkSavedRows(
        allSaved: boolean,
        datasetDeployInfo: Array<{
            dataset: GTFSDatasets;
            lastModified: ILastModifiedInfo;
            isDeployed: boolean;
        }>
    ) {
        if (!allSaved) {
            throw new GeneralError("Some GTFS datasets did not properly save", this.constructor.name);
        }

        await Promise.all(
            datasetDeployInfo.map(async (el) => {
                await this.ropidGtfsFacade.checkSavedTmpTables(el.dataset, el.lastModified.version);
            })
        );
    }

    private async getDatasetsToDeploy(data: IDatasetsInput) {
        const datasetDeployInfo = await Promise.all(
            data.datasets.map(async (dataset) => {
                const lastModified = await this.metadataRepository.getLastModified(dataset);
                const isDeployed = await this.metadataRepository.checkIfNewVersionIsAlreadyDeployed(
                    dataset,
                    lastModified.version
                );
                return { dataset, lastModified, isDeployed };
            })
        );

        this.logger.debug(`Datasets status: ${JSON.stringify(datasetDeployInfo)}`);

        const notDeployed = datasetDeployInfo.filter((info) => !info.isDeployed);
        return { notDeployed, datasetDeployInfo };
    }
}
