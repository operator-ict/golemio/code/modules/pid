export interface IRailtrackCoordinates {
    lon: number;
    lat: number;
    gtfs_stop_id: string | null;
}

/**
 * Coordinates are aggregated by track_id
 */
export interface IAggregatedRailtrackGPSData {
    coordinates: Record<string, IRailtrackCoordinates> | null;
}
