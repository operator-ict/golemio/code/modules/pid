export interface ICheckForNewDataInput {
    forceRefresh?: boolean;
}
