export interface IRefreshPublicDepartureParams {
    intervalFromHours?: number;
    intervalToHours?: number;
}
