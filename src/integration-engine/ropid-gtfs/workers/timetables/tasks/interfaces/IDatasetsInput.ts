import { GTFSDatasets } from "../helpers/HelperTypes";

export interface IDatasetsInput {
    datasets: GTFSDatasets[];
}
