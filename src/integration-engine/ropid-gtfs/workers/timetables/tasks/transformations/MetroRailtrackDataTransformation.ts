import { MetroRailtrackGPSModel } from "#sch/ropid-gtfs/models/railtrack/MetroRailtrackGPSModel";
import { IMetroRailtrack } from "#sch/datasources/static-data";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";

export class MetroRailtrackDataTransformation extends AbstractTransformation<IMetroRailtrack, MetroRailtrackGPSModel> {
    public name = "MetroRailtrackData";

    constructor() {
        super();
    }

    protected transformInternal = (element: IMetroRailtrack): MetroRailtrackGPSModel => {
        return {
            track_id: element.track_section,
            route_name: element.line,
            coordinates: {
                type: "Point",
                coordinates: [Number.parseFloat(element.gps_lon), Number.parseFloat(element.gps_lat)],
            },
            gtfs_stop_id: element.stop_id === "" ? null : element.stop_id,
        } as MetroRailtrackGPSModel;
    };
}
