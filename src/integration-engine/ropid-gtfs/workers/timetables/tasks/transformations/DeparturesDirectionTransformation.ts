import { IDeparturesDirection } from "#sch/datasources/static-data";
import { DeparturesDirectionModel } from "#sch/ropid-gtfs/models/DeparturesDirectionDto";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class DeparturesDirectionTransformation extends AbstractTransformation<IDeparturesDirection, DeparturesDirectionModel> {
    public name = "DeparturesDirection";

    constructor() {
        super();
    }

    protected transformInternal = (element: IDeparturesDirection): DeparturesDirectionModel => {
        return {
            ...element,
            rule_order: Number(element.rule_order),
            create_batch_id: element.create_batch_id || null,
            update_batch_id: element.update_batch_id || null,
            updated_by: element.updated_by || null,
            created_by: element.created_by || null,
        } as DeparturesDirectionModel;
    };
}
