import { PidContainer } from "#ie/ioc/Di";
import { DatasetEnum } from "#ie/ropid-gtfs";
import { RopidGtfsMetadataRepository } from "#ie/ropid-gtfs/RopidGtfsMetadataRepository";
import { SourceTableSuffixEnum } from "#ie/ropid-gtfs/helpers/SourceTableSuffixEnum";
import { RopidGtfsContainer } from "#ie/ropid-gtfs/ioc/Di";
import { RopidGtfsContainerToken } from "#ie/ropid-gtfs/ioc/RopidGtfsContainerToken";
import { MetaStateEnum } from "#ie/shared/RopidMetadataModel";
import { PG_SCHEMA } from "#sch/const";
import { DatasetModelName, RopidGTFS, ScheduleModelName } from "#sch/ropid-gtfs";
import { RopidGTFSPgProp } from "#sch/ropid-gtfs/RopidGtfsSchedule";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { RedisPubSubChannel } from "@golemio/core/dist/integration-engine/data-access/pubsub";
import { PostgresModel } from "@golemio/core/dist/integration-engine/models/PostgresModel";
import { AbstractTask } from "@golemio/core/dist/integration-engine/workers";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { MessageProperties } from "amqplib";
import { from as copyFrom } from "pg-copy-streams";
import { GTFSDatasets, ICachedDataset, ORIGIN_HOSTNAME_HEADER, SaveMethod, datasetFileModelMap } from "./helpers/HelperTypes";
import { RopidGtfsFactory } from "./helpers/RopidGtfsFactory";
import { CachedDatasetSchema } from "./schema/CachedDatasetSchema";

export class TransformAndSaveDataTask extends AbstractTask<ICachedDataset> {
    public readonly queueName = "transformAndSaveData";
    public readonly queueTtl = 23 * 60 * 60 * 1000; // 23 hours
    public readonly schema = CachedDatasetSchema;
    private logger: ILogger;

    private metadataRepository: RopidGtfsMetadataRepository;

    private ropidGtfsFactory: RopidGtfsFactory;
    private gtfsRedisChannel: RedisPubSubChannel;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.logger = RopidGtfsContainer.resolve<ILogger>(CoreToken.Logger);

        this.ropidGtfsFactory = RopidGtfsContainer.resolve<RopidGtfsFactory>(RopidGtfsContainerToken.RopidGtfsFactory);
        this.gtfsRedisChannel = RopidGtfsContainer.resolve<RedisPubSubChannel>(RopidGtfsContainerToken.RedisPubSubChannel);
        this.metadataRepository = RopidGtfsContainer.resolve<RopidGtfsMetadataRepository>(
            RopidGtfsContainerToken.RopidGtfsMetadataRepository
        );
    }

    protected async execute(inputData: ICachedDataset, msgProperties?: MessageProperties) {
        this.logger.info(`Datasets: transforming and saving (${inputData.name})`);

        let modelName = datasetFileModelMap(inputData.dataset, inputData.name);
        if (!Array.isArray(modelName)) {
            modelName = [modelName];
        }

        let errorMessage: string | undefined;
        try {
            const transformedData = await this.ropidGtfsFactory.loadAndTransformDataset(inputData, modelName[0]);

            if (inputData.dataset === DatasetEnum.CIS_STOPS) {
                await this.saveTransformedDataset(transformedData.cis_stops, inputData.dataset, modelName[0]);
                await this.saveTransformedDataset(transformedData.cis_stop_groups, inputData.dataset, modelName[1]);
            } else {
                await this.saveTransformedDataset(transformedData, inputData.dataset, modelName[0]);
            }
        } catch (err) {
            this.logger.error(`Datasets: transforming and saving error (${modelName}): ${err.toString()}`);
            errorMessage = err.message;
        }

        if (msgProperties?.headers[ORIGIN_HOSTNAME_HEADER]) {
            await this.gtfsRedisChannel.publishMessage(`${errorMessage ?? "OK"} (${modelName})`, {
                channelSuffix: msgProperties.headers[ORIGIN_HOSTNAME_HEADER],
            });
        }

        if (errorMessage) {
            throw new GeneralError(errorMessage, this.constructor.name);
        }
    }

    private saveTransformedDataset = async (
        transformedData: any,
        dataset: GTFSDatasets,
        modelName: DatasetModelName
    ): Promise<void> => {
        const saveMethod = this.ropidGtfsFactory.getSaveMethod(dataset, modelName);

        if (saveMethod === SaveMethod.STREAM) {
            await this.streamDataToTmp(transformedData, modelName as ScheduleModelName);
        } else {
            await this.getTmpModelByName(modelName)?.save(transformedData, false);
            this.logger.info(`Datasets: copying to tmp success (${modelName})`);
        }

        const dbLastModified = await this.metadataRepository.getLastModified(dataset);
        await this.metadataRepository.updateState(dataset, modelName, MetaStateEnum.SAVED, dbLastModified.version);
    };

    private streamDataToTmp = async (data: any, modelName: ScheduleModelName) => {
        // get connection and db client
        const connection = PidContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector).getConnection();
        const client = (await connection.connectionManager.getConnection({ type: "write" })) as any;

        const tmpTableName = RopidGTFS[modelName as RopidGTFSPgProp].pgTableName + SourceTableSuffixEnum.Tmp;

        // copy transformed data to tmp table by stream
        await new Promise<void>((resolve, reject) => {
            const stream = client
                .query(copyFrom(`COPY "${PG_SCHEMA}".${tmpTableName} FROM STDIN DELIMITER ',' CSV HEADER;`))
                .on("error", async (err: any) => {
                    this.logger.error(`Datasets: copying to tmp error (${modelName}): ${err.toString()}`);
                    await connection.connectionManager.releaseConnection(client);
                    return reject(err);
                })
                .on("finish", async () => {
                    this.logger.info(`Datasets: copying to tmp success (${modelName})`);
                    await connection.connectionManager.releaseConnection(client);
                    return resolve();
                });
            data.pipe(stream);
        });
    };

    private getTmpModelByName = (name: string): PostgresModel => {
        const modelDef = RopidGTFS[name as RopidGTFSPgProp];

        if (!modelDef || !modelDef.name || !modelDef.pgTableName || !modelDef.outputSequelizeAttributes) {
            throw new Error("Model not found.");
        }

        const model = new PostgresModel(
            modelDef.name + "Tmp" + "Model",
            {
                outputSequelizeAttributes: modelDef.outputSequelizeAttributes,
                pgSchema: PG_SCHEMA,
                pgTableName: modelDef.pgTableName + SourceTableSuffixEnum.Tmp,
                savingType: "insertOnly",
                sequelizeAdditionalSettings: {
                    timestamps: false,
                },
            },
            new JSONSchemaValidator(modelDef.name + "Tmp" + "ModelValidator", modelDef.mongo)
        );

        return model;
    };
}
