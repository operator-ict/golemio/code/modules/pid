import { PG_SCHEMA } from "#sch/const";
import { MetroRailtrackGPSModel } from "#sch/ropid-gtfs/models/railtrack/MetroRailtrackGPSModel";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import Sequelize, { Transaction } from "@golemio/core/dist/shared/sequelize";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { IAggregatedRailtrackGPSData } from "../interfaces/IAggregatedRailtrackGPSData";

@injectable()
export class MetroRailtrackGPSRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            "MetroRailtrackGPSRepository",
            {
                pgTableName: MetroRailtrackGPSModel.TABLE_NAME,
                pgSchema: PG_SCHEMA,
                outputSequelizeAttributes: MetroRailtrackGPSModel.attributeModel,
                savingType: "insertOnly",
            },
            new JSONSchemaValidator("MetroRailtrackGPSRepository", MetroRailtrackGPSModel.arrayJsonSchema)
        );
    }

    public findCoordinates = async (routeName: string, trackIds: string[]): Promise<IAggregatedRailtrackGPSData | null> => {
        try {
            return await this.sequelizeModel.findOne({
                attributes: [
                    [
                        Sequelize.fn(
                            "json_object_agg",
                            Sequelize.col("track_id"),
                            Sequelize.fn(
                                "json_build_object",
                                "lon",
                                Sequelize.fn("ST_X", Sequelize.col("coordinates")),
                                "lat",
                                Sequelize.fn("ST_Y", Sequelize.col("coordinates")),
                                "gtfs_stop_id",
                                Sequelize.col("gtfs_stop_id")
                            )
                        ),
                        "coordinates",
                    ],
                ],
                where: {
                    route_name: routeName,
                    track_id: {
                        [Sequelize.Op.in]: trackIds,
                    },
                },
                raw: true,
                plain: true,
            });
        } catch (err) {
            const exception = new GeneralError(
                `[${this.constructor.name}] Could not find data: ${err.message}`,
                this.constructor.name,
                err
            );

            log.error(exception);
            throw exception;
        }
    };

    public saveData = async (
        data: MetroRailtrackGPSModel[],
        shouldReplace = true,
        isReturning = false
    ): Promise<MetroRailtrackGPSModel[]> => {
        let transaction: Transaction | undefined;

        try {
            await this.validate(data);

            transaction = await this.sequelizeModel.sequelize?.transaction();
            if (shouldReplace) {
                await this.sequelizeModel.destroy({ where: {}, transaction });
            }

            const instances = await this.sequelizeModel.bulkCreate<MetroRailtrackGPSModel>(data, {
                ignoreDuplicates: true,
                returning: isReturning,
                transaction,
            });

            await transaction?.commit();
            return instances;
        } catch (err) {
            const exception = new GeneralError(
                `[${this.constructor.name}] Could not save data: ${err.message}`,
                this.constructor.name,
                err
            );

            log.error(exception);
            await transaction?.rollback();
            throw exception;
        }
    };
}
