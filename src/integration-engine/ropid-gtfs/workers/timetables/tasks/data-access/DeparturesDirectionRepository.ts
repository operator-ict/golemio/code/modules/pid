import { PG_SCHEMA } from "#sch/const";
import { MetroRailtrackGPSModel } from "#sch/ropid-gtfs/models/railtrack/MetroRailtrackGPSModel";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { Transaction } from "@golemio/core/dist/shared/sequelize";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { DeparturesDirectionModel } from "#sch/ropid-gtfs/models/DeparturesDirectionDto";

@injectable()
export class DeparturesDirectionRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            "DeparturesDirectionRepository",
            {
                pgTableName: DeparturesDirectionModel.TABLE_NAME,
                pgSchema: PG_SCHEMA,
                outputSequelizeAttributes: DeparturesDirectionModel.attributeModel,
                savingType: "insertOnly",
            },
            new JSONSchemaValidator("DeparturesDirectionRepository", DeparturesDirectionModel.arrayJsonSchema)
        );
    }

    public saveData = async (
        data: DeparturesDirectionModel[],
        shouldReplace = true,
        isReturning = false
    ): Promise<DeparturesDirectionModel[]> => {
        let transaction: Transaction | undefined;

        try {
            await this.validate(data);

            transaction = await this.sequelizeModel.sequelize?.transaction();
            if (shouldReplace) {
                await this.sequelizeModel.destroy({ where: {}, transaction });
            }

            const instances = await this.sequelizeModel.bulkCreate<DeparturesDirectionModel>(data, {
                ignoreDuplicates: true,
                returning: isReturning,
                transaction,
            });

            await transaction?.commit();
            return instances;
        } catch (err) {
            const exception = new GeneralError(
                `[${this.constructor.name}] Could not save data: ${err.message}`,
                this.constructor.name,
                err
            );

            log.error(exception);
            await transaction?.rollback();
            throw exception;
        }
    };
}
