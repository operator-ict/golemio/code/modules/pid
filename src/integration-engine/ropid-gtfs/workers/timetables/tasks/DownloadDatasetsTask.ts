import { PidContainer } from "#ie/ioc/Di";
import { DatasetEnum } from "#ie/ropid-gtfs/RopidGtfsFacade";
import { RopidGtfsMetadataRepository } from "#ie/ropid-gtfs/RopidGtfsMetadataRepository";
import { RopidGtfsRepository } from "#ie/ropid-gtfs/data-access/RopidGtfsRepository";
import { StaticFileRedisRepository } from "#ie/ropid-gtfs/data-access/cache/StaticFileRedisRepository";
import { RopidGtfsContainer } from "#ie/ropid-gtfs/ioc/Di";
import { RopidGtfsContainerToken } from "#ie/ropid-gtfs/ioc/RopidGtfsContainerToken";
import { MetaDatasetInfoKeyEnum, MetaStateEnum, MetaTypeEnum } from "#ie/shared/RopidMetadataModel";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { IFile, QueueManager } from "@golemio/core/dist/integration-engine";
import { RedisPubSubChannel } from "@golemio/core/dist/integration-engine/data-access/pubsub";
import { AbstractTask } from "@golemio/core/dist/integration-engine/workers";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { ReadStream } from "fs";
import os from "os";
import { GTFSDatasets, ICachedDataset, ORIGIN_HOSTNAME_HEADER, datasetFileModelMap } from "./helpers/HelperTypes";
import { RopidGtfsFactory } from "./helpers/RopidGtfsFactory";
import { IDatasetsInput } from "./interfaces/IDatasetsInput";
import { DatasetsInputSchema } from "./schema/DownloadDataInputSchema";

export class DownloadDatasetsTask extends AbstractTask<IDatasetsInput> {
    public readonly queueName = "downloadDatasets";
    public readonly queueTtl = 59 * 60 * 1000; // 59 minutes
    public readonly schema = DatasetsInputSchema;
    private logger: ILogger;

    private metadataRepository: RopidGtfsMetadataRepository;
    private staticFileRedisRepository: StaticFileRedisRepository;
    private ropidGtfsRepository: RopidGtfsRepository;

    private ropidGtfsFactory?: RopidGtfsFactory;
    private gtfsRedisChannel: RedisPubSubChannel;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.logger = PidContainer.resolve<ILogger>(CoreToken.Logger);
        this.gtfsRedisChannel = RopidGtfsContainer.resolve<RedisPubSubChannel>(RopidGtfsContainerToken.RedisPubSubChannel);

        this.staticFileRedisRepository = RopidGtfsContainer.resolve<StaticFileRedisRepository>(
            RopidGtfsContainerToken.StaticFileRedisRepository
        );

        // MetaData Model
        this.metadataRepository = RopidGtfsContainer.resolve<RopidGtfsMetadataRepository>(
            RopidGtfsContainerToken.RopidGtfsMetadataRepository
        );
        this.ropidGtfsRepository = RopidGtfsContainer.resolve<RopidGtfsRepository>(RopidGtfsContainerToken.RopidGtfsRepository);
    }

    protected async execute(data: IDatasetsInput) {
        this.logger.info(`Datasets: downloading ${data.datasets}`);
        /** Ropid GTFS Dataset Map Dependencies **/
        this.ropidGtfsFactory = RopidGtfsContainer.resolve<RopidGtfsFactory>(RopidGtfsContainerToken.RopidGtfsFactory);

        let cachedDatasets: ICachedDataset[] = [];

        for (const dataset of data.datasets) {
            try {
                cachedDatasets = cachedDatasets.concat(await this.downloadAndPersistDatasetFiles(dataset));
            } catch (err) {
                await QueueManager.sendMessageToExchange(this.queuePrefix, "refreshPrecomputedTables", {});

                if (err instanceof AbstractGolemioError) {
                    throw err;
                }

                throw new GeneralError("Error while downloading GTFS datasets", this.constructor.name, err);
            }
        }
        await this.ropidGtfsRepository.createTmpTables();
        const subscriber = this.gtfsRedisChannel.createSubscriber({
            channelSuffix: os.hostname(),
            maxMessageCount: cachedDatasets.length,
        });
        await subscriber.subscribe();

        for (const cachedDataset of cachedDatasets) {
            QueueManager.sendMessageToExchange(this.queuePrefix, "transformAndSaveData", cachedDataset, {
                headers: {
                    [ORIGIN_HOSTNAME_HEADER]: os.hostname(),
                },
            });
        }

        // Listen for messages from workers and wait for all of them to finish
        // before continuing to the next step (replacing tables)
        try {
            await subscriber.listen(({ message, messageCount, isFinal }) => {
                this.logger.debug(`Datasets: received message: ${message} (${messageCount}/${cachedDatasets.length})`);

                if (!message.startsWith("OK")) {
                    this.logger.error(`Datasets: subscription error: ${message}`);
                    throw new GeneralError(message, this.constructor.name);
                }

                if (isFinal) {
                    this.logger.info("Datasets: all messages received");
                }
            });
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            }
            throw new GeneralError("Error while listening for messages", this.constructor.name, err);
        } finally {
            await subscriber.unsubscribe();
            QueueManager.sendMessageToExchange(this.queuePrefix, "checkSavedRowsAndReplaceTables", data);
        }
    }

    /**
     * Helper method - download and persist dataset
     */
    private downloadAndPersistDatasetFiles = async (dataset: GTFSDatasets): Promise<ICachedDataset[]> => {
        this.logger.info(`Datasets: processing ${dataset}`);
        let files: IFile | IFile[] = await this.ropidGtfsFactory!.getDatasetMap()[dataset].datasource.getAll();
        if (!Array.isArray(files)) files = [files];

        const dbLastModified = await this.metadataRepository.getLastModified(dataset);
        const cachedDataset: ICachedDataset[] = [];

        for (const file of files) {
            this.logger.info(`Datasets: processing file '${file.name}'`);
            const { data, ...meta } = file;
            if (dataset === DatasetEnum.PID_GTFS || dataset === DatasetEnum.RUN_NUMBERS) {
                await this.staticFileRedisRepository.pipeStream(file.filepath, data as ReadStream);
            } else {
                await this.staticFileRedisRepository.set(file.filepath, typeof data === "string" ? data : JSON.stringify(data));
            }

            const key = datasetFileModelMap(dataset, file.name);
            const metaStates = (Array.isArray(key) ? key : [key]).map((name) => {
                return {
                    dataset,
                    key: name,
                    type: MetaTypeEnum.STATE,
                    value: MetaStateEnum.DOWNLOADED,
                    version: dbLastModified.version + 1,
                };
            });

            for (const metaState of metaStates) {
                await this.metadataRepository.save(metaState);
            }

            cachedDataset.push({
                ...meta,
                dataset,
            });
        }

        const lastModified = await this.ropidGtfsFactory!.getDatasetMap()[dataset as GTFSDatasets].datasource.getLastModified();

        await this.metadataRepository.save({
            dataset,
            key: MetaDatasetInfoKeyEnum.LAST_MODIFIED,
            type: MetaTypeEnum.DATASET_INFO,
            value: lastModified,
            version: dbLastModified.version + 1,
        });

        this.logger.info(`Datasets: processing ${dataset} finished - ${JSON.stringify(cachedDataset)}`);
        return cachedDataset;
    };
}
