import { RopidGtfsContainer } from "#ie/ropid-gtfs/ioc/Di";
import { RopidGtfsContainerToken } from "#ie/ropid-gtfs/ioc/RopidGtfsContainerToken";
import { AbstractTask, AbstractWorker } from "@golemio/core/dist/integration-engine/workers";
import { TIMETABLE_WORKER_NAME } from "./constants";
import { CheckForNewDataTask } from "./tasks/CheckForNewDataTask";
import { CheckSavedRowsAndReplaceTablesTask } from "./tasks/CheckSavedRowsAndReplaceTablesTask";
import { DownloadDatasetsTask } from "./tasks/DownloadDatasetsTask";
import { RefreshPrecomputedTablesTask } from "./tasks/RefreshPrecomputedTablesTask";
import { RefreshPublicGtfsDepartureCacheTask } from "./tasks/RefreshPublicGtfsDepartureCacheTask";
import { TransformAndSaveDataTask } from "./tasks/TransformAndSaveDataTask";
import { SaveStaticDataTask } from "./tasks/SaveStaticDataTask";

export class TimetableWorker extends AbstractWorker {
    protected name = TIMETABLE_WORKER_NAME;

    constructor() {
        super();

        // Register tasks
        this.registerTask(
            RopidGtfsContainer.resolve<RefreshPublicGtfsDepartureCacheTask>(
                RopidGtfsContainerToken.RefreshPublicGtfsDepartureCacheTask
            )
        );
        this.registerTask(new CheckForNewDataTask(this.getQueuePrefix()));
        this.registerTask(new CheckSavedRowsAndReplaceTablesTask(this.getQueuePrefix()));
        this.registerTask(new DownloadDatasetsTask(this.getQueuePrefix()));
        this.registerTask(new RefreshPrecomputedTablesTask(this.getQueuePrefix()));
        this.registerTask(new TransformAndSaveDataTask(this.getQueuePrefix()));
        this.registerTask(RopidGtfsContainer.resolve<SaveStaticDataTask>(RopidGtfsContainerToken.SaveStaticDataTask));
    }

    public registerTask = (task: AbstractTask<any>): void => {
        super.registerTask(task);
        task.queuePrefix = this.getQueuePrefix();
    };
}
