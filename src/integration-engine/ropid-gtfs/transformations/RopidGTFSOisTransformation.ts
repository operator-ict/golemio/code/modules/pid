import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { RopidGTFS } from "#sch/ropid-gtfs";
import { IRopidGTFSOisMappingInputData, IRopidGTFSOisMappingData } from "#sch/ropid-gtfs/RopidGTFSOisMapping";

export interface IOisMappingTransformationData {
    data: IRopidGTFSOisMappingInputData[];
    name: string;
}

export class RopidGTFSOisMappingTransformation extends BaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = RopidGTFS.ois.name;
    }

    /**
     * Overrides BaseTransformation::transform
     */
    public transform = async ({ data }: IOisMappingTransformationData): Promise<IRopidGTFSOisMappingData[]> => {
        const promises = data.map((element) => {
            return this.transformElement(element);
        });
        return await Promise.all(promises);
    };

    protected transformElement = (element: IRopidGTFSOisMappingInputData): IRopidGTFSOisMappingData => {
        return {
            ois: element.ois,
            node: element.node,
            name: element.name,
        };
    };
}
