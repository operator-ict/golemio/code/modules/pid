import { RopidGTFS } from "#sch/ropid-gtfs";
import { RopidGTFSPgProp } from "#sch/ropid-gtfs/RopidGtfsSchedule";
import { log } from "@golemio/core/dist/integration-engine";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import csv from "csv-parser";
import stringify from "csv-stringify";
import { Readable, Transform, pipeline } from "stream";

export interface IRopidGtfsTransformationData {
    sourceStream: Readable;
    name: string;
}

@injectable()
export class RopidGTFSTransformation extends BaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = RopidGTFS.name;
    }

    public transform = async (input: IRopidGtfsTransformationData): Promise<Readable> => {
        // create object by table structure and fill its values with null
        const tableToObjectFilledWithNulls = Object.keys(RopidGTFS[input.name as RopidGTFSPgProp].outputSequelizeAttributes)
            .filter((column) => !column.startsWith("computed_"))
            .reduce((acc, curr) => ((acc[curr] = null), acc), {} as Record<string, null>);

        const addColumns = new Transform({
            objectMode: true,
            transform(chunk, _encoding, callback) {
                this.push({
                    // prepare object to be same as db table (number and order of columns)
                    ...tableToObjectFilledWithNulls,
                    // filter redundant properties and fill data to prepared structure
                    ...Object.keys(chunk)
                        .filter((key) => Object.keys(tableToObjectFilledWithNulls).includes(key))
                        .reduce((obj: any, key) => {
                            obj[key] = chunk[key];
                            return obj;
                        }, {}),
                });
                callback();
            },
        });

        return pipeline(input.sourceStream, csv(), addColumns, stringify({ header: true }), (err) => {
            if (err) {
                log.error(`Datasets: transformation error (${input.name}): ${err.toString()}`);
                throw err;
            } else {
                log.info(`Datasets: transformation success (${input.name})`);
            }
        });
    };

    protected transformElement = () => {};
}
