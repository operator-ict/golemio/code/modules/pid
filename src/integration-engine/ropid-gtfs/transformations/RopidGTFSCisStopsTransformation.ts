import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { RopidGTFS } from "#sch/ropid-gtfs";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { IRopidGTFSCisStopsData, IRopidGTFSCisStopsInputData } from "#sch/ropid-gtfs/RopidGTFSCisStops";
import { IRopidGTFSCisStopGroupsData, IRopidGTFSCisStopGroupsInputData } from "#sch/ropid-gtfs/RopidGTFSCisStopGroups";

export interface ICisStopsTransformationData {
    data: IInputDataset[];
    name: string;
}

interface IInputDataset extends IRopidGTFSCisStopGroupsInputData {
    stops: IRopidGTFSCisStopsInputData[];
}

export interface ICisStopsTransformedData {
    cis_stop_groups: IRopidGTFSCisStopGroupsData[];
    cis_stops: IRopidGTFSCisStopsData[];
}

export class RopidGTFSCisStopsTransformation extends BaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = RopidGTFS.name + "CisStops";
    }

    /**
     * Overrides BaseTransformation::transform
     */
    public transform = async ({ data }: ICisStopsTransformationData): Promise<ICisStopsTransformedData> => {
        const res: ICisStopsTransformedData = {
            cis_stop_groups: [],
            cis_stops: [],
        };

        const promises = data.map(async (stopGroup) => {
            const promisesStops = stopGroup.stops.map((stop: IRopidGTFSCisStopsInputData) => {
                return {
                    alt_idos_name: stop.altIdosName,
                    cis: stopGroup.cis,
                    id: stop.id,
                    jtsk_x: stop.jtskX,
                    jtsk_y: stop.jtskY,
                    lat: stop.lat,
                    lon: stop.lon,
                    platform: stop.platform,
                    wheelchair_access: stop.wheelchairAccess,
                    zone: stop.zone,
                };
            });
            res.cis_stops.push(...(await Promise.all(promisesStops)));
            return {
                avg_jtsk_x: stopGroup.avgJtskX,
                avg_jtsk_y: stopGroup.avgJtskY,
                avg_lat: stopGroup.avgLat,
                avg_lon: stopGroup.avgLon,
                cis: stopGroup.cis,
                district_code: stopGroup.districtCode,
                full_name: stopGroup.fullName,
                idos_category: stopGroup.idosCategory,
                idos_name: stopGroup.idosName,
                municipality: stopGroup.municipality,
                name: stopGroup.name,
                node: stopGroup.node,
                unique_name: stopGroup.uniqueName,
            };
        });
        res.cis_stop_groups = this.getUniqueStopGroups(await Promise.all(promises));
        return res;
    };

    protected transformElement = async (): Promise<any> => {};

    private getUniqueStopGroups = (data: IRopidGTFSCisStopGroupsData[]): IRopidGTFSCisStopGroupsData[] => {
        // duplicity checking
        // TODO is it ok if all duplicate items are removed?
        const uniqueMap = new Map();
        const duplicateCisStopGroups: number[] = [];
        data.forEach((item) => {
            if (uniqueMap.has(item.cis)) {
                duplicateCisStopGroups.push(item.cis);
                uniqueMap.delete(item.cis);
            } else {
                uniqueMap.set(item.cis, item);
            }
        });

        if (duplicateCisStopGroups.length > 0) {
            log.warn(
                "CIS_STOP_GROUPS contains duplicate cis. Items with this cis was removed: " +
                    JSON.stringify(duplicateCisStopGroups)
            );
        }

        return [...uniqueMap.values()];
    };
}
