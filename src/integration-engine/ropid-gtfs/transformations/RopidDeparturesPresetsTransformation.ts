import { IRopidDeparturesPreset, IRopidDeparturesPresetsOutput, RopidDeparturesPresets } from "#sch/ropid-departures-presets";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";

export class RopidDeparturesPresetsTransformation extends BaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = RopidDeparturesPresets.name;
    }

    /**
     * Overrides BaseTransformation::transform
     */
    public transform = (data: IRopidDeparturesPreset[]): Promise<IRopidDeparturesPresetsOutput[]> => {
        return Promise.resolve(data.map((element) => this.transformElement(element)));
    };

    protected transformElement = (element: IRopidDeparturesPreset): IRopidDeparturesPresetsOutput => {
        return {
            route_name: element.routeName,
            api_version: element.apiVersion,
            route: element.route,
            url_query_params: new URLSearchParams(element.query).toString(),
            note: element.note,
            is_testing: element.note.endsWith("(testovací)"),
        };
    };
}
