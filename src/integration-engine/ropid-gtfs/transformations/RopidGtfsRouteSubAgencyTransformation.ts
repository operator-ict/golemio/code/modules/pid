import { IRouteSubAgencyDto } from "#sch/ropid-gtfs/interfaces/IRouteSubAgencyDto";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { IRouteSubAgencyRaw } from "../data-access/cache/interfaces/IRouteSubAgencyRaw";

@injectable()
export class RopidGtfsRouteSubAgencyTransformation extends AbstractTransformation<IRouteSubAgencyRaw, IRouteSubAgencyDto> {
    public name = "RopidGtfsRouteSubAgencyTransformation";

    constructor() {
        super();
    }

    public transformArray = (input: IRouteSubAgencyRaw[], includeDuplicates = false): IRouteSubAgencyDto[] => {
        const output: IRouteSubAgencyDto[] = [];

        for (let index = 0; index < input.length; index++) {
            const element = input[index];
            if (!element.route_id) {
                continue;
            }

            if (!/^\d+$/.test(element.sub_agency_id!)) {
                new GeneralError(
                    `sub_agency_id should be number, got ${element.sub_agency_id} for route_id ${element.route_id}`,
                    this.name
                );
            }

            output.push(this.transformElement(element));
        }

        return !includeDuplicates ? this.filterDuplicates(output) : output;
    };

    /**
     * Filter duplicates by route_id and sub_agency_id.
     * In case of duplicates, keep the first one in order with route_licence_number.
     */
    public filterDuplicates = (data: IRouteSubAgencyDto[]): IRouteSubAgencyDto[] => {
        const transformation: Record<string, IRouteSubAgencyDto> = {};

        for (let index = 0; index < data.length; index++) {
            const element = data[index];
            const key = `${element.route_id}-${element.sub_agency_id}`;

            if (!transformation[key]) {
                transformation[key] = element;
            } else if (!transformation[key].route_licence_number && element.route_licence_number) {
                transformation[key] = element;
            }
        }

        return Object.values(transformation);
    };

    protected transformInternal = (data: IRouteSubAgencyRaw): IRouteSubAgencyDto => {
        return {
            route_id: data.route_id,
            route_licence_number: /^\d+$/.test(data.route_licence_number!) ? Number.parseInt(data.route_licence_number!) : null,
            sub_agency_id: Number.parseInt(data.sub_agency_id!),
            sub_agency_name: data.sub_agency_name,
        } as IRouteSubAgencyDto;
    };
}
