import { IPublicGtfsDepartureCacheDto } from "#sch/ropid-gtfs/redis/interfaces/IPublicGtfsDepartureCacheDto";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { IPublicDepartureDto } from "../interfaces/IPublicDepartureDto";

@injectable()
export class PublicDepartureCacheTransformation extends AbstractTransformation<
    IPublicDepartureDto,
    IPublicGtfsDepartureCacheDto
> {
    public name = "PublicDepartureCacheTransformation";

    protected transformInternal = (inputDto: IPublicDepartureDto) => {
        const outputDto: IPublicGtfsDepartureCacheDto = {
            stop_id: inputDto.stop_id,
            departure_datetime: inputDto.departure_datetime.toISOString(),
            arrival_datetime: inputDto.arrival_datetime?.toISOString() ?? null,
            route_short_name: inputDto.route_short_name,
            route_type: inputDto.route_type,
            trip_id: inputDto.trip_id,
            stop_sequence: inputDto.stop_sequence,
            platform_code: inputDto.platform_code,
            trip_headsign: inputDto.trip_headsign,
        };

        return outputDto;
    };
}
