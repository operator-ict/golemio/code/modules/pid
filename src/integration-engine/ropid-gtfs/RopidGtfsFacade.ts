import {
    DeparturesRepository,
    MinMaxStopSequencesRepository,
    ServicesCalendarRepository,
    TripScheduleRepository,
} from "#ie/ropid-gtfs/data-access/precomputed";
import { MetaDatasetInfoKeyEnum, MetaStateEnum, MetaTypeEnum } from "#ie/shared";
import { PG_SCHEMA } from "#sch/const";
import { DatasetModelName, RopidGTFS } from "#sch/ropid-gtfs";
import { RopidGTFSPgProp } from "#sch/ropid-gtfs/RopidGtfsSchedule";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import Sequelize, { Model, Transaction } from "@golemio/core/dist/shared/sequelize";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

import { RopidGtfsMetadataRepository } from "./RopidGtfsMetadataRepository";
import { RopidGtfsRepository } from "./data-access/RopidGtfsRepository";
import { SourceTableSuffixEnum } from "./helpers/SourceTableSuffixEnum";
import { RopidGtfsContainerToken } from "./ioc/RopidGtfsContainerToken";
import { getTableNameFromModel } from "./workers/timetables/tasks/helpers/HelperTypes";

interface IMetadataTable {
    key: string;
    tn: string;
}

export enum DatasetEnum {
    PID_GTFS = "PID_GTFS",
    CIS_STOPS = "CIS_STOPS",
    OIS_MAPPING = "OIS_MAPPING",
    RUN_NUMBERS = "RUN_NUMBERS",
    DEPARTURES_PRESETS = "DEPARTURES_PRESETS",
}

@injectable()
export class RopidGtfsFacade {
    private name = this.constructor.name;
    /** Precomputed data repositories */
    private minMaxStopSequencesRepository: MinMaxStopSequencesRepository;
    private tripScheduleRepository: TripScheduleRepository;
    private servicesCalendarRepository: ServicesCalendarRepository;

    constructor(
        @inject(RopidGtfsContainerToken.RopidGtfsRepository) private ropidGtfsRepository: RopidGtfsRepository,
        @inject(RopidGtfsContainerToken.RopidGtfsMetadataRepository)
        private ropidGtfsMetadataRepository: RopidGtfsMetadataRepository,
        @inject(RopidGtfsContainerToken.DepartureRepository) private departureRepository: DeparturesRepository,
        @inject(CoreToken.PostgresConnector) private databaseConnector: IDatabaseConnector
    ) {
        this.minMaxStopSequencesRepository = new MinMaxStopSequencesRepository();
        this.tripScheduleRepository = new TripScheduleRepository();
        this.servicesCalendarRepository = new ServicesCalendarRepository();
    }

    public checkSavedTmpTables = async (dataset: string, version: number): Promise<void> => {
        const tables = await this.ropidGtfsMetadataRepository.getAllSaved(dataset, version);

        const tablesArray: any[] = [];
        tables.map((table) => {
            tablesArray.push(`'${RopidGTFS[table.dataValues.tn as RopidGTFSPgProp].pgTableName}${SourceTableSuffixEnum.Tmp}'`);
        });
        const connection = this.databaseConnector.getConnection();
        const result: Array<{ name: string; total: number }> = await connection.query(
            `
            SELECT table_name AS name, count_rows(table_schema, table_name) AS total
            FROM information_schema.tables
            WHERE table_schema NOT IN ('pg_catalog', 'information_schema')
                AND table_type = 'BASE TABLE'
                AND table_name in (${tablesArray.join(",")})
                AND table_schema = '${PG_SCHEMA}';
            `,
            { type: Sequelize.QueryTypes.SELECT }
        );

        log.info(`Datasets: total rows for ${dataset}: ${JSON.stringify(result)}`);

        if (!result || !result.length || result.some((table) => table.total === 0)) {
            throw new GeneralError("Empty table found", this.constructor.name);
        }
    };

    public replaceTables = async (datasetInfo: Array<{ dataset: string; version: number }>): Promise<boolean> => {
        const tables = await this.ropidGtfsMetadataRepository.find({
            attributes: [[Sequelize.literal(`DISTINCT "key"`), "tn"]],
            where: {
                [Sequelize.Op.or]: datasetInfo.map((item) => ({
                    dataset: item.dataset,
                    type: MetaTypeEnum.STATE,
                    value: MetaStateEnum.SAVED,
                    version: item.version,
                })),
            },
        });

        const connection = this.databaseConnector.getConnection();
        const t = await connection.transaction();

        try {
            await this.replaceStaticTables(tables, t);
            await this.replacePrecomputedTables(t);

            // save meta
            await this.ropidGtfsMetadataRepository.bulkCreate(
                datasetInfo.map((el) => ({
                    dataset: el.dataset,
                    key: MetaDatasetInfoKeyEnum.DEPLOYED,
                    type: MetaTypeEnum.DATASET_INFO,
                    value: "true",
                    version: el.version,
                })),
                { transaction: t }
            );

            await this.ropidGtfsMetadataRepository.destroy({
                transaction: t,
                where: {
                    [Sequelize.Op.or]: datasetInfo.map((item) => ({
                        dataset: item.dataset,
                        version: {
                            [Sequelize.Op.and]: [
                                // delete all versions older than ten versions back
                                { [Sequelize.Op.lt]: item.version - 10 },
                                { [Sequelize.Op.ne]: -1 },
                            ],
                        },
                    })),
                },
            });

            await t.commit();
            return true;
        } catch (err) {
            log.error(err);
            await t.rollback();
            log.error(`Datasets: replacing failed ${err.toString()}`);
            throw new GeneralError(this.name + ": replaceTables() failed.", undefined, err);
        }
    };

    private replaceStaticTables = async (tables: Array<Model<IMetadataTable>>, t: Transaction): Promise<void> => {
        const tablesArray: string[] = tables.map((table) => getTableNameFromModel(table.dataValues.tn as DatasetModelName));
        await this.ropidGtfsRepository.replaceAllTable(tablesArray, t);
    };

    public replacePrecomputedTables = async (t: Transaction): Promise<void> => {
        await this.ropidGtfsRepository.replaceAllTable(
            [
                this.servicesCalendarRepository["tableName"],
                this.minMaxStopSequencesRepository["tableName"],
                this.tripScheduleRepository["tableName"],
            ],
            t
        );
        await this.departureRepository.replaceByTmp(t);
    };
}
