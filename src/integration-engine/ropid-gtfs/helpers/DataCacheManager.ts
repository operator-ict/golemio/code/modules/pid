import { RegionalBusGtfsCacheRepository } from "#ie/vehicle-positions/workers/runs/data-access/cache/RegionalBusGtfsCacheRepository";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import {
    BlockStopsRedisRepository,
    DelayComputationRedisRepository,
    RunTripsRedisRepository,
    StaticFileRedisRepository,
} from "../data-access/cache";
import { RopidGtfsContainer } from "../ioc/Di";
import { RopidGtfsContainerToken } from "../ioc/RopidGtfsContainerToken";
import { IDataCacheManager } from "./interfaces/IDataCacheManager";

export class DataCacheManager implements IDataCacheManager {
    private static _instance: DataCacheManager;
    private readonly staticFileRepository: StaticFileRedisRepository;
    private readonly runTripsRepository: RunTripsRedisRepository;
    private readonly blockStopsRepository: BlockStopsRedisRepository;
    private readonly delayComputationRepository: DelayComputationRedisRepository;
    private readonly regionalBusGtfsLookupCacheRepository: RegionalBusGtfsCacheRepository;

    public static getInstance() {
        if (!this._instance) {
            this._instance = new DataCacheManager();
        }

        return this._instance;
    }

    private constructor() {
        this.staticFileRepository = new StaticFileRedisRepository();
        this.runTripsRepository = new RunTripsRedisRepository();
        this.blockStopsRepository = RopidGtfsContainer.resolve<BlockStopsRedisRepository>(
            RopidGtfsContainerToken.BlockStopsRedisRepository
        );
        this.delayComputationRepository = new DelayComputationRedisRepository();
        this.regionalBusGtfsLookupCacheRepository = new RegionalBusGtfsCacheRepository();
    }

    public async cleanCache(): Promise<void> {
        try {
            await this.staticFileRepository.truncate(true);
            await this.runTripsRepository.truncate(true);
            await this.blockStopsRepository.truncate(true);
            await this.delayComputationRepository.truncate(true);
            await this.regionalBusGtfsLookupCacheRepository.truncate(true);
        } catch (err) {
            throw new GeneralError("DataCacheManager: Failed to clean cache", this.constructor.name, err);
        }
    }
}
