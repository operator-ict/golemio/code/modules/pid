export interface IDataCacheManager {
    cleanCache(): Promise<void>;
}
