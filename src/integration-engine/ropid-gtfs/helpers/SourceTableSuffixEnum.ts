export enum SourceTableSuffixEnum {
    Actual = "_actual",
    Tmp = "_tmp",
    Drop = "_drop",
}
