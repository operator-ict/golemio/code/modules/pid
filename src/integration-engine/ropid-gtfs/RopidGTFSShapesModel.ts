import { PG_SCHEMA } from "#sch/const";
import { RopidGTFS } from "#sch/ropid-gtfs";
import { ShapeDto } from "#sch/ropid-gtfs/models/ShapeDto";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine";
import { IValidator, JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import Sequelize from "@golemio/core/dist/shared/sequelize";

export class RopidGTFSShapesModel extends PostgresModel implements IModel {
    /** Model name */
    public name!: string;
    /** The Sequelize Model */
    public sequelizeModel!: Sequelize.ModelCtor<any>;
    /** The Sequelize Model for temporary table */
    protected tmpSequelizeModel!: Sequelize.ModelCtor<any> | null;
    /** Validation helper */
    protected validator!: IValidator;
    /** Type/Strategy of saving the data */
    protected savingType!: "insertOnly" | "insertOrUpdate";

    public constructor() {
        super(
            RopidGTFS.shapes.name + "Model",
            {
                outputSequelizeAttributes: ShapeDto.attributeModel,
                pgTableName: RopidGTFS.shapes.pgTableName,
                pgSchema: PG_SCHEMA,
                savingType: "insertOnly",
            },
            new JSONSchemaValidator(RopidGTFS.shapes.name + "ModelValidator", ShapeDto.jsonSchema)
        );
    }
}
