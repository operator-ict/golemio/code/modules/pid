import { PG_SCHEMA } from "#sch/const";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { Transaction } from "@golemio/core/dist/shared/sequelize";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class RopidGtfsRepository {
    private preparationProcedure = "ropidgtfs_prepareTmpTables";
    private cleaningTmpProcedure = "ropidgtfs_cleanTmpTables";
    private cleaningOldProcedure = "ropidgtfs_cleanOldTables";
    private replaceAllProcedure = "ropidgtfs_replaceAllSavedTables";
    private createPrecomputedTmpTablesProcedure = "ropidgtfs_preparePrecomputedTmpTables";

    constructor(
        @inject(CoreToken.PostgresConnector) private connector: IDatabaseConnector,
        @inject(CoreToken.Logger) private logger: ILogger
    ) {}

    public async replaceAllTable(tables: string[], t: Transaction) {
        this.logger.info(`Replacing tmp for actual tables: ${tables.join(", ")}`);
        const connection = this.connector.getConnection();
        await connection.query(`CALL ${PG_SCHEMA}.${this.replaceAllProcedure}($1);`, {
            transaction: t,
            bind: [tables.join(",")],
        });
    }

    public async createTmpTables() {
        this.logger.info(`Creating tmp tables`);
        const connection = this.connector.getConnection();
        await connection.query(`CALL ${PG_SCHEMA}.${this.preparationProcedure}();`);
    }

    public async createPrecomputedTmpTables() {
        this.logger.info(`Creating precomputed tmp tables`);
        const connection = this.connector.getConnection();
        await connection.query(`CALL ${PG_SCHEMA}.${this.createPrecomputedTmpTablesProcedure}();`);
    }

    public async cleanTmpAndOldTables() {
        try {
            this.logger.info(`Cleaning tmp and old tables`);
            const connection = this.connector.getConnection();
            await connection.query(`CALL ${PG_SCHEMA}.${this.cleaningOldProcedure}();`);
            await connection.query(`CALL ${PG_SCHEMA}.${this.cleaningTmpProcedure}();`);
        } catch (err) {
            this.logger.error(new GeneralError("Error while cleaning tmp and old tables", this.constructor.name, err));
        }
    }

    public async cleanOldTables() {
        try {
            this.logger.info(`Cleaning tmp and old tables`);
            const connection = this.connector.getConnection();
            await connection.query(`CALL ${PG_SCHEMA}.${this.cleaningOldProcedure}();`);
        } catch (err) {
            this.logger.error(new GeneralError("Error while cleaning old tables", this.constructor.name, err));
        }
    }
}
