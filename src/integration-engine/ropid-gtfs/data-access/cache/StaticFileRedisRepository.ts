import { RedisModel } from "@golemio/core/dist/integration-engine/models/RedisModel";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import csv from "csv-parser";
import { IRouteSubAgencyRaw } from "./interfaces/IRouteSubAgencyRaw";

@injectable()
export class StaticFileRedisRepository extends RedisModel {
    public static NAMESPACE_PREFIX = "files:gtfsStatic";

    constructor() {
        super("GTFSStaticFileRedisRepository", {
            isKeyConstructedFromData: false,
            prefix: StaticFileRedisRepository.NAMESPACE_PREFIX,
        });
    }

    public async getCsvAsJson(key: string): Promise<IRouteSubAgencyRaw[]> {
        return await new Promise((resolve, reject) => {
            const results: IRouteSubAgencyRaw[] = [];

            this.getReadableStream(key)
                .pipe(csv())
                .on("data", (data) => {
                    results.push(data);
                })
                .on("end", () => {
                    resolve(results);
                });
        });
    }
}
