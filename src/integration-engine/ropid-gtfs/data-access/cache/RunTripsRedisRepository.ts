import { GTFS_RUN_SCHEDULE_NAMESPACE_PREFIX } from "#sch/ropid-gtfs/redis/const";
import { GtfsRunTripCacheDtoSchema } from "#sch/vehicle-positions/redis/schemas/GtfsRunTripCacheDtoSchema";
import { RedisModel } from "@golemio/core/dist/integration-engine/models/RedisModel";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class RunTripsRedisRepository extends RedisModel {
    public static NAMESPACE_PREFIX = GTFS_RUN_SCHEDULE_NAMESPACE_PREFIX;

    constructor() {
        super(
            "GTFSRunTripsRedisRepository",
            {
                decodeDataAfterGet: JSON.parse,
                encodeDataBeforeSave: JSON.stringify,
                isKeyConstructedFromData: false,
                prefix: RunTripsRedisRepository.NAMESPACE_PREFIX,
            },
            new JSONSchemaValidator("GTFSRunTripsRedisRepositoryValidator", GtfsRunTripCacheDtoSchema)
        );
    }
}
