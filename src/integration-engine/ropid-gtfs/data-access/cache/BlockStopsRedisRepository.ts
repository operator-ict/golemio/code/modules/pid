import { TripStopsCacheDtoSchema } from "#sch/vehicle-positions/redis/schemas/TripStopsCacheDtoSchema";
import { RedisModel } from "@golemio/core/dist/integration-engine/models/RedisModel";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class BlockStopsRedisRepository extends RedisModel {
    public static NAMESPACE_PREFIX = "gtfsBlockSchedule";

    constructor() {
        super(
            "GTFSBlockStopsRedisRepository",
            {
                decodeDataAfterGet: JSON.parse,
                encodeDataBeforeSave: JSON.stringify,
                isKeyConstructedFromData: false,
                prefix: BlockStopsRedisRepository.NAMESPACE_PREFIX,
            },
            new JSONSchemaValidator("GTFSBlockStopsRedisRepositoryValidator", TripStopsCacheDtoSchema)
        );
    }
}
