import { IRefreshPublicDepartureParams } from "#ie/ropid-gtfs/workers/timetables/tasks/interfaces/IRefreshPublicDepartureParams";
import { PUBLIC_DEPARTURE_NAMESPACE_PREFIX } from "#sch/ropid-gtfs/redis/const";
import { IPublicGtfsDepartureCacheDto } from "#sch/ropid-gtfs/redis/interfaces/IPublicGtfsDepartureCacheDto";
import { PublicGtfsDepartureDtoSchema } from "#sch/ropid-gtfs/redis/schemas/PublicGtfsDepartureDtoSchema";
import { RedisModel } from "@golemio/core/dist/integration-engine/models/RedisModel";
import { AbstractGolemioError, GeneralError, ValidationError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class PublicGtfsDepartureRepository extends RedisModel {
    public static NAMESPACE_PREFIX = PUBLIC_DEPARTURE_NAMESPACE_PREFIX;

    constructor() {
        super(
            "GtfsDepartureRepository",
            {
                decodeDataAfterGet: JSON.parse,
                encodeDataBeforeSave: JSON.stringify,
                isKeyConstructedFromData: false,
                prefix: PublicGtfsDepartureRepository.NAMESPACE_PREFIX,
            },
            new JSONSchemaValidator("PublicGtfsDepartureRepositoryValidator", PublicGtfsDepartureDtoSchema)
        );
    }

    /**
     * Save new departures for a stop and
     *   - remove all older departures
     *   - remove departures that are in the same time range as the new ones
     *     (to prevent duplicates and to keep the cache clean and up-to-date)
     */
    public async replaceDeparturesForStop(
        departures: IPublicGtfsDepartureCacheDto[],
        stopId: string,
        intervalParams: Required<IRefreshPublicDepartureParams>
    ): Promise<void> {
        try {
            await this.validator!.Validate(departures);
        } catch (err) {
            throw new ValidationError("Error while validating departures", this.constructor.name, err);
        }

        try {
            const sortedSetKey = `${this.prefix}:${stopId}`;
            const pipeline = this.connection.pipeline();
            // Automatically remove departures that are older than 3 hours to keep the cache clean
            pipeline.zremrangebyscore(sortedSetKey, "-inf", new Date().getTime() / 1000 - 3 * 3600);
            if (departures.length === 0) {
                // If stop already has an empty object it will just update the empty objects score/time
                pipeline.zadd(sortedSetKey, new Date().getTime() / 1000, "{}");
            } else {
                // remove empty departures from stop
                pipeline.zrem(sortedSetKey, "{}");
                const firstDepartureScore = new Date(departures[0].departure_datetime).getTime() / 1000;
                const lastDepartureScore = new Date(departures[departures.length - 1].departure_datetime).getTime() / 1000;

                // Remove departures that are in the same time range
                pipeline.zremrangebyscore(sortedSetKey, firstDepartureScore, lastDepartureScore);

                for (const departure of departures) {
                    const departureScore = new Date(departure.departure_datetime).getTime() / 1000;
                    pipeline.zadd(sortedSetKey, departureScore, JSON.stringify(departure));
                }
            }
            pipeline.expire(sortedSetKey, intervalParams.intervalToHours * 3600);
            await pipeline.exec();
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            }

            throw new GeneralError(`${this.constructor.name}: error while saving departures`, this.constructor.name, err);
        }
    }
}
