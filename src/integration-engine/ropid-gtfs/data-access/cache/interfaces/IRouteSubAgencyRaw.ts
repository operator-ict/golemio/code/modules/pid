export interface IRouteSubAgencyRaw {
    route_id: string | undefined;
    route_licence_number: string | undefined;
    sub_agency_id: string | undefined;
    sub_agency_name: string | undefined;
}
