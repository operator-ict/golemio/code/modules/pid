import { DelayComputationDtoSchema } from "#sch/ropid-gtfs/redis/schemas/DelayComputationDtoSchema";
import { GTFS_DELAY_COMPUTATION_NAMESPACE_PREFIX } from "#sch/vehicle-positions/redis/const";
import { RedisModel } from "@golemio/core/dist/integration-engine/models/RedisModel";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class DelayComputationRedisRepository extends RedisModel {
    public static NAMESPACE_PREFIX = GTFS_DELAY_COMPUTATION_NAMESPACE_PREFIX;

    constructor() {
        super(
            "GTFSDelayComputationRedisRepository",
            {
                decodeDataAfterGet: JSON.parse,
                encodeDataBeforeSave: JSON.stringify,
                isKeyConstructedFromData: true,
                prefix: DelayComputationRedisRepository.NAMESPACE_PREFIX,
            },
            new JSONSchemaValidator("GTFSDelayComputationRedisRepositoryValidator", DelayComputationDtoSchema)
        );
    }

    public async expire(keys: string[], ttlInSeconds: number): Promise<void> {
        const pipeline = this.connection.pipeline();
        for (const key of keys) {
            const redisKey = `${this.prefix}:${key}`;
            pipeline.expire(redisKey, ttlInSeconds);
        }

        try {
            await pipeline.exec();
        } catch (err) {
            throw new GeneralError("Error while expiring keys in Redis", this.constructor.name, err);
        }
    }
}
