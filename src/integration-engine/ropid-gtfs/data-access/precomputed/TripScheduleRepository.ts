import { PG_SCHEMA } from "#sch/const";
import { RopidGTFS } from "#sch/ropid-gtfs";
import { IScheduleDto } from "#sch/vehicle-positions/redis/interfaces/IGtfsRunTripCacheDto";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { FindOptions } from "@golemio/core/dist/shared/sequelize";
import { SourceTableSuffixEnum } from "../../helpers/SourceTableSuffixEnum";

export class TripScheduleRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            "TripScheduleRepository",
            {
                outputSequelizeAttributes: RopidGTFS.tripSchedule.outputSequelizeAttributes,
                pgTableName: RopidGTFS.tripSchedule.pgTableName,
                pgSchema: PG_SCHEMA,
                savingType: "insertOnly",
                attributesToRemove: ["id", "created_at", "updated_at"],
            },
            new JSONSchemaValidator("TripScheduleRepositoryValidator", RopidGTFS.tripSchedule.outputJsonSchema)
        );
    }

    public findAll = (options: FindOptions): Promise<IScheduleDto[]> => {
        return this.sequelizeModel.findAll({ raw: true, ...options });
    };

    public populate = async (sourceTableSuffix: SourceTableSuffixEnum): Promise<void> => {
        /* eslint-disable max-len */
        const sql = `
            SELECT
                t0.origin_route_id,
                t0.trip_id,
                t0.service_id,
                t0.run_number,
                t0.date,
                t0.route_id,
                t0.route_type,
                t0.route_short_name,
                t0.is_regional,
                t0.is_substitute_transport,
                t0.is_night,
                t0.trip_headsign,
                t0.trip_short_name,
                t0.block_id,
                t0.exceptional,
                t0.min_stop_time,
                t0.max_stop_time,
                gtfs_timestamp(t0.min_stop_time::varchar, t0.date) AS start_timestamp,
                gtfs_timestamp(t0.max_stop_time::varchar, t0.date) AS end_timestamp,
                t0.first_stop_id,
                t0.last_stop_id,
                right(t0.origin_route_id, -1) as origin_route_name,
                t0.trip_number,
                t0.route_licence_number,
                t0.direction_id,
                t0.shape_id
            FROM (
                SELECT
                    t1.route_id AS origin_route_id,
                    t1.trip_id,
                    t1.service_id,
                    t1.run_number,
                    t3.date,
                    t7.route_id,
                    t7.route_type,
                    t7.route_short_name,
                    t7.is_regional,
                    t7.is_substitute_transport,
                    t7.is_night,
                    t8.trip_headsign,
                    t8.trip_short_name,
                    t8.block_id,
                    t8.exceptional,
                    LEAST(t5.arrival_time::INTERVAL, t5.departure_time::INTERVAL) AS min_stop_time,
                    GREATEST(t6.arrival_time::INTERVAL, t6.departure_time::INTERVAL) AS max_stop_time,
                    t5.stop_id AS first_stop_id,
                    t6.stop_id AS last_stop_id,
                    t1.trip_number,
                    t1.route_licence_number,
                    t8.direction_id,
                    t8.shape_id
                FROM "${PG_SCHEMA}".ropidgtfs_run_numbers${sourceTableSuffix} t1
                INNER JOIN "${PG_SCHEMA}".ropidgtfs_precomputed_services_calendar_tmp t3 ON t1.service_id = t3.service_id
                INNER JOIN "${PG_SCHEMA}".ropidgtfs_precomputed_minmax_stop_sequences_tmp t4 ON t1.trip_id = t4.trip_id
                LEFT JOIN "${PG_SCHEMA}".ropidgtfs_trips${sourceTableSuffix} t8 ON t1.trip_id = t8.trip_id
                LEFT JOIN "${PG_SCHEMA}".ropidgtfs_routes${sourceTableSuffix} t7 ON t8.route_id = t7.route_id
                LEFT JOIN "${PG_SCHEMA}".ropidgtfs_stop_times${sourceTableSuffix} t5 ON t1.trip_id = t5.trip_id AND t5.stop_sequence = t4.min_stop_sequence
                LEFT JOIN "${PG_SCHEMA}".ropidgtfs_stop_times${sourceTableSuffix} t6 ON t1.trip_id = t6.trip_id AND t6.stop_sequence = t4.max_stop_sequence
            ) t0;
        `;
        /* eslint-enable max-len */

        try {
            const tmpTable = RopidGTFS.tripSchedule.pgTableName + SourceTableSuffixEnum.Tmp;

            await this.sequelizeModel.sequelize!.query(`
                SET LOCAL search_path TO ${PG_SCHEMA};
                INSERT INTO "${tmpTable}" ${sql};
            `);
        } catch (err) {
            log.error(err);
            throw err;
        }
    };
}
