import { PG_SCHEMA } from "#sch/const";
import { RopidGTFS } from "#sch/ropid-gtfs";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { SourceTableSuffixEnum } from "../../helpers/SourceTableSuffixEnum";

export class MinMaxStopSequencesRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            "MinMaxStopSequencesRepository",
            {
                outputSequelizeAttributes: RopidGTFS.minMaxStopSequences.outputSequelizeAttributes,
                pgTableName: RopidGTFS.minMaxStopSequences.pgTableName,
                pgSchema: PG_SCHEMA,
                savingType: "insertOnly",
                attributesToRemove: ["id", "created_at", "updated_at"],
            },
            new JSONSchemaValidator("MinMaxStopSequencesRepositoryValidator", RopidGTFS.minMaxStopSequences.outputJsonSchema)
        );
    }

    public populate = async (sourceTableSuffix: SourceTableSuffixEnum): Promise<void> => {
        const sql = `
            SELECT
                trip_id,
                max(stop_sequence) AS max_stop_sequence,
                min(stop_sequence) AS min_stop_sequence,
                GREATEST(
                    max(arrival_time::interval),
                    max(departure_time::interval)
                ) AS max_stop_time,
                LEAST(
                    min(arrival_time::interval),
                    min(departure_time::interval)
                ) AS min_stop_time
            FROM "${PG_SCHEMA}".ropidgtfs_stop_times${sourceTableSuffix}
            GROUP BY trip_id
        `;

        try {
            const tmpTable = RopidGTFS.minMaxStopSequences.pgTableName + SourceTableSuffixEnum.Tmp;

            await this.sequelizeModel.sequelize!.query(`INSERT INTO "${PG_SCHEMA}"."${tmpTable}" ${sql};`);
        } catch (err) {
            log.error(err);
            throw err;
        }
    };
}
