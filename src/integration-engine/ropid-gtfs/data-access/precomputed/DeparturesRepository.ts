import { GTFSRouteTypeEnum } from "#helpers/RouteTypeEnums";
import { IPublicDepartureDto } from "#ie/ropid-gtfs/interfaces/IPublicDepartureDto";
import { IRefreshPublicDepartureParams } from "#ie/ropid-gtfs/workers/timetables/tasks/interfaces/IRefreshPublicDepartureParams";
import { PG_SCHEMA } from "#sch/const";
import { DeparturesModel } from "#sch/ropid-gtfs/models/precomputed";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { Op, Sequelize, Transaction } from "@golemio/core/dist/shared/sequelize";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { SourceTableSuffixEnum } from "../../helpers/SourceTableSuffixEnum";

@injectable()
export class DeparturesRepository extends PostgresModel implements IModel {
    constructor(
        @inject(CoreToken.SimpleConfig) private config: ISimpleConfig,
        @inject(CoreToken.Logger) private logger: ILogger
    ) {
        super(
            "DeparturesRepository",
            {
                pgTableName: DeparturesModel.TABLE_NAME,
                pgSchema: PG_SCHEMA,
                outputSequelizeAttributes: DeparturesModel.attributeModel,
                attributesToRemove: ["id", "created_at", "updated_at"],
                savingType: "insertOnly",
            },
            new JSONSchemaValidator("DeparturesRepository", DeparturesModel.jsonSchema)
        );
    }

    public createAndPopulate = async (sourceTableSuffix: SourceTableSuffixEnum): Promise<void> => {
        const nodeEnv = this.config.getValue("env.NODE_ENV", "development");
        const timeConstraintLiteral =
            nodeEnv !== "test" ? `WHERE gtfs_timestamp(t.arrival_time, t4.date) > now() - interval '6 hours'` : "";

        /* eslint-disable max-len */
        const sql = `
            SELECT
                t.stop_sequence,
                t.stop_headsign,
                t.pickup_type,
                t.drop_off_type,
                t.arrival_time,
                gtfs_timestamp(t.arrival_time, t4.date) AS arrival_datetime,
                t.departure_time,
                gtfs_timestamp(t.departure_time, t4.date) AS departure_datetime,
                t0.stop_id,
                t0.stop_name,
                t0.platform_code,
                t0.wheelchair_boarding,
                t1.min_stop_sequence,
                t1.max_stop_sequence,
                t2.trip_id,
                t2.trip_headsign,
                t2.trip_short_name,
                t2.wheelchair_accessible,
                t3.service_id,
                t4.date,
                t5.route_short_name,
                coalesce(t5.route_type, :defaultRouteType) AS route_type,
                t5.route_id,
                t5.is_night,
                t5.is_regional,
                t5.is_substitute_transport,
                t6.stop_sequence AS next_stop_sequence,
                t6.stop_id AS next_stop_id,
                t7.stop_sequence AS last_stop_sequence,
                t7.stop_id AS last_stop_id,
                cis_stop.cis AS cis_stop_group_id
            FROM ropidgtfs_stop_times${sourceTableSuffix} t
                LEFT JOIN ropidgtfs_stops${sourceTableSuffix} t0 ON t.stop_id = t0.stop_id
                LEFT JOIN ropidgtfs_trips${sourceTableSuffix} t2 ON t.trip_id = t2.trip_id
                INNER JOIN ropidgtfs_precomputed_services_calendar_tmp t4 ON t2.service_id = t4.service_id
                INNER JOIN ropidgtfs_precomputed_minmax_stop_sequences_tmp t1 ON t.trip_id = t1.trip_id
                LEFT JOIN ropidgtfs_calendar${sourceTableSuffix} t3 ON t2.service_id = t3.service_id
                LEFT JOIN ropidgtfs_routes${sourceTableSuffix} t5 ON t2.route_id = t5.route_id
                LEFT JOIN ropidgtfs_stop_times${sourceTableSuffix} t6 ON t.trip_id = t6.trip_id AND t6.stop_sequence = t.stop_sequence + 1
                LEFT JOIN ropidgtfs_stop_times${sourceTableSuffix} t7 ON t.trip_id = t7.trip_id AND t7.stop_sequence = t.stop_sequence - 1
                LEFT JOIN ropidgtfs_cis_stops cis_stop on cis_stop.id = t0.computed_cis_stop_id
            ${timeConstraintLiteral}`;
        /* eslint-enable max-len */

        try {
            const tableName = DeparturesModel.TABLE_NAME;
            const tmpTableName = tableName + SourceTableSuffixEnum.Tmp;

            await this.sequelizeModel.sequelize!.query(
                `
                SET LOCAL search_path TO ${PG_SCHEMA};
                DROP TABLE IF EXISTS ${tmpTableName};
                CREATE TABLE ${tmpTableName} (LIKE ${tableName} including all);
                INSERT INTO ${tmpTableName} ${sql};`,
                {
                    replacements: {
                        defaultRouteType: GTFSRouteTypeEnum.EXT_MISCELLANEOUS,
                    },
                }
            );
        } catch (err) {
            this.logger.error(err);
            throw err;
        }
    };

    public countDeparturesForPublicCache = async (intervalParams: Required<IRefreshPublicDepartureParams>): Promise<number> => {
        try {
            return await this.sequelizeModel.count({
                where: {
                    [Op.and]: {
                        departure_datetime: {
                            [Op.between]: [
                                Sequelize.literal(`now() + interval '${intervalParams.intervalFromHours} hours'`),
                                Sequelize.literal(`now() + interval '${intervalParams.intervalToHours} hours'`),
                            ],
                        },
                        next_stop_sequence: {
                            [Op.not]: null,
                        },
                    },
                },
            });
        } catch (err) {
            throw new GeneralError("Error while getting number of departures", this.constructor.name, err);
        }
    };

    public getDepaturesForPublicCache = async (
        page: number,
        pageSize: number,
        intervalParams: Required<IRefreshPublicDepartureParams>
    ): Promise<IPublicDepartureDto[]> => {
        try {
            return await this.sequelizeModel.findAll({
                attributes: [
                    "stop_id",
                    "departure_datetime",
                    "arrival_datetime",
                    "route_short_name",
                    "route_type",
                    "trip_id",
                    "stop_sequence",
                    "platform_code",
                    "trip_headsign",
                ],
                where: {
                    [Op.and]: {
                        departure_datetime: {
                            [Op.between]: [
                                Sequelize.literal(`now() + interval '${intervalParams.intervalFromHours} hours'`),
                                Sequelize.literal(`now() + interval '${intervalParams.intervalToHours} hours'`),
                            ],
                        },
                        next_stop_sequence: {
                            [Op.not]: null,
                        },
                    },
                },
                order: [["departure_datetime", "ASC"]],
                offset: page * pageSize,
                limit: pageSize,
                raw: true,
            });
        } catch (err) {
            throw new GeneralError("Error while getting departures for public cache", this.constructor.name, err);
        }
    };

    public replaceByTmp = async (transaction: Transaction): Promise<void> => {
        try {
            const tableName = DeparturesModel.TABLE_NAME;
            const tmpTableName = tableName + SourceTableSuffixEnum.Tmp;
            const dropTableName = tableName + SourceTableSuffixEnum.Drop;

            await this.sequelizeModel.sequelize!.query(
                `
                SET LOCAL search_path TO ${PG_SCHEMA};
                LOCK ${tableName} IN EXCLUSIVE MODE;
                ALTER TABLE ${tableName} RENAME TO ${dropTableName};
                ALTER TABLE ${tmpTableName} RENAME TO ${tableName};
                DROP TABLE ${dropTableName} CASCADE;`,
                { transaction }
            );
        } catch (err) {
            this.logger.error(err);
            throw err;
        }
    };

    public analyze = async (): Promise<void> => {
        try {
            await this.sequelizeModel.sequelize!.query(
                `analyze ${PG_SCHEMA}.${DeparturesModel.TABLE_NAME}${SourceTableSuffixEnum.Tmp};`
            );
        } catch (err) {
            this.logger.error(err);
            throw err;
        }
    };
}
