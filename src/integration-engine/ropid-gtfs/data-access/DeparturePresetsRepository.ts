import { PG_SCHEMA } from "#sch/const";
import { RopidDeparturesPresets } from "#sch/ropid-departures-presets";
import { PostgresModel } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class DeparturePresetsRepository extends PostgresModel {
    constructor() {
        super(
            RopidDeparturesPresets.name + "Model",
            {
                hasTmpTable: true,
                outputSequelizeAttributes: RopidDeparturesPresets.outputSequelizeAttributes,
                pgTableName: RopidDeparturesPresets.pgTableName,
                pgSchema: PG_SCHEMA,
                savingType: "insertOnly",
                addAuditAttributes: true,
            },
            new JSONSchemaValidator(RopidDeparturesPresets.name + "ModelValidator", RopidDeparturesPresets.outputJsonSchema)
        );
    }
}
