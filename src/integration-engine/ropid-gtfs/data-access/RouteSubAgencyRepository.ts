import { PG_SCHEMA } from "#sch/const";
import { RopidGTFS } from "#sch/ropid-gtfs";
import { RouteSubAgencyDto } from "#sch/ropid-gtfs/models/RouteSubAgencyDto";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class RouteSubAgencyRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            "RouteSubAgencyRepository",
            {
                pgTableName: RopidGTFS.route_sub_agencies.pgTableName,
                pgSchema: PG_SCHEMA,
                outputSequelizeAttributes: RouteSubAgencyDto.attributeModel,
                savingType: "insertOnly",
            },
            new JSONSchemaValidator("RouteSubAgencyRepository", RouteSubAgencyDto.jsonSchema)
        );
    }

    public async findSubAgenciesByCisLineIds(cisLineIds: string[]): Promise<RouteSubAgencyDto[]> {
        try {
            return await this.sequelizeModel.findAll<RouteSubAgencyDto>({
                attributes: ["route_licence_number", "route_id", "sub_agency_name"],
                where: {
                    route_licence_number: cisLineIds,
                },
            });
        } catch (err) {
            throw new GeneralError("findSubAgenciesByCisLineIds: failed to get sub-agencies", this.name, err);
        }
    }
}
