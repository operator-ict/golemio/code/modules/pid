import { PG_SCHEMA } from "#sch/const";
import { RopidGTFS } from "#sch/ropid-gtfs";
import { TripDto } from "#sch/ropid-gtfs/models/TripDto";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import Sequelize from "@golemio/core/dist/shared/sequelize";
import { RopidGTFSShapesModel, RopidGTFSStopTimesModel, RopidGTFSStopsModel } from ".";
import { ITripWithShapesAndStopTimes } from "./interfaces/TripModelInterfaces";

export class RopidGTFSTripsModel extends PostgresModel implements IModel {
    private modelGTFSShapes: RopidGTFSShapesModel;
    private modelGTFSStopTimes: RopidGTFSStopTimesModel;
    private modelGTFSStops: RopidGTFSStopsModel;

    constructor() {
        super(
            RopidGTFS.trips.name + "Model",
            {
                outputSequelizeAttributes: RopidGTFS.trips.outputSequelizeAttributes,
                pgTableName: RopidGTFS.trips.pgTableName,
                pgSchema: PG_SCHEMA,
                savingType: "insertOnly",
            },
            new JSONSchemaValidator(RopidGTFS.trips.name + "ModelValidator", TripDto.jsonSchema)
        );
        this.modelGTFSStopTimes = new RopidGTFSStopTimesModel();
        this.modelGTFSStops = new RopidGTFSStopsModel();
        this.modelGTFSShapes = new RopidGTFSShapesModel();

        this.sequelizeModel.hasMany(this.modelGTFSStopTimes.sequelizeModel, {
            as: "stop_times",
            foreignKey: "trip_id",
            sourceKey: "trip_id",
            constraints: false,
        });
        this.modelGTFSStopTimes.sequelizeModel.hasOne(this.modelGTFSStops.sequelizeModel, {
            as: "stop",
            foreignKey: "stop_id",
            sourceKey: "stop_id",
            constraints: false,
        });
        this.sequelizeModel.hasMany(this.modelGTFSShapes.sequelizeModel, {
            as: "shapes",
            foreignKey: "shape_id",
            sourceKey: "shape_id",
            constraints: false,
        });
    }

    public findByIdForDelayComputation = async (tripId: string): Promise<ITripWithShapesAndStopTimes | null> => {
        const gtfsTripEntity = await this.sequelizeModel.findOne({
            attributes: ["shape_id"],
            include: [
                {
                    attributes: [
                        "arrival_time",
                        "departure_time",
                        "shape_dist_traveled",
                        "stop_headsign",
                        "stop_id",
                        "stop_sequence",
                        [Sequelize.literal(`EXTRACT(EPOCH FROM "arrival_time"::INTERVAL)::int`), "arrival_time_seconds"],
                        [Sequelize.literal(`EXTRACT(EPOCH FROM "departure_time"::INTERVAL)::int`), "departure_time_seconds"],
                        [
                            Sequelize.literal(`CASE when drop_off_type = '1' and pickup_type = '1' then true else false END`),
                            "is_no_stop_waypoint",
                        ],
                    ],
                    as: "stop_times",
                    model: this.modelGTFSStopTimes.sequelizeModel,
                    include: [
                        {
                            attributes: ["stop_id", "stop_lat", "stop_lon", "stop_name", "zone_id", "wheelchair_boarding"],
                            as: "stop",
                            model: this.modelGTFSStops.sequelizeModel,
                            required: true,
                        },
                    ],
                },
                {
                    as: "shapes",
                    model: this.modelGTFSShapes.sequelizeModel,
                    attributes: ["shape_dist_traveled", "shape_id", "shape_pt_lat", "shape_pt_lon", "shape_pt_sequence"],
                },
            ],
            order: [
                ["stop_times", "stop_sequence", "ASC"],
                ["shapes", "shape_pt_sequence", "ASC"],
            ],
            where: { trip_id: tripId },
        });

        if (!gtfsTripEntity) {
            return null;
        }

        return gtfsTripEntity.toJSON();
    };
}
