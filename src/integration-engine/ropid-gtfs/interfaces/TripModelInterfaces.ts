import { GtfsStopWheelchairBoardingEnum } from "#helpers/AccessibilityEnums";

export interface IShape {
    shape_dist_traveled: number;
    shape_id: string;
    shape_pt_lat: number;
    shape_pt_lon: number;
    shape_pt_sequence: number;
}

export interface IStop {
    stop_id: string;
    stop_lat: number;
    stop_lon: number;
    stop_name: string;
    zone_id: string | null;
    wheelchair_boarding: GtfsStopWheelchairBoardingEnum | null;
}

export interface IStopTime {
    arrival_time_seconds: number;
    departure_time_seconds: number;
    shape_dist_traveled: number;
    stop_id: string;
    stop_sequence: number;
    stop_headsign: string;
    is_no_stop_waypoint: boolean;

    stop: IStop;
}

export interface ITripWithShapesAndStopTimes {
    shape_id: string;
    shapes: IShape[] | null;
    stop_times: IStopTime[] | null;
}
