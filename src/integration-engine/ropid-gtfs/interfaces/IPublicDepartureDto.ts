import { GTFSRouteTypeEnum } from "#helpers/RouteTypeEnums";

export interface IPublicDepartureDto {
    stop_id: string;
    departure_datetime: Date;
    arrival_datetime: Date | null;
    route_short_name: string;
    route_type: GTFSRouteTypeEnum;
    trip_id: string;
    stop_sequence: number;
    platform_code: string | null;
    trip_headsign: string;
}
