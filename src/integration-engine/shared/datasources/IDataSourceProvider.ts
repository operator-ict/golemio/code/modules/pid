import { IDataSource } from "@golemio/core/dist/integration-engine/datasources";

export interface IDataSourceProvider<T> {
    getDataSource: (...args: any[]) => IDataSource<T>;
}
