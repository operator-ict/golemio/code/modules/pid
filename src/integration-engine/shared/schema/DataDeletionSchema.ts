import { RepositoryTableName } from "#ie/data-retention/workers/constants/RepositoryTableNameEnum";
import { IsInt, Min, IsString, IsEnum } from "@golemio/core/dist/shared/class-validator";
import { IDataDeletionParams } from "../interfaces/IDataDeletionParams";

export class DataDeletionValidationSchema implements IDataDeletionParams {
    @IsInt()
    @Min(8, { message: "Data newer than 8 hours cannot be deleted" })
    targetHours!: number;

    @IsString()
    @IsEnum(RepositoryTableName)
    repoName!: RepositoryTableName;
}
