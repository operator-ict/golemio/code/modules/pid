import { RepositoryTableName } from "#ie/data-retention/workers/constants/RepositoryTableNameEnum";

export interface IDataDeletionParams {
    targetHours: number;
    repoName: RepositoryTableName;
}
