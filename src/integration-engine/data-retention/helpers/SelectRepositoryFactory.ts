import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { IPresetLogRepository } from "#ie/ropid-gtfs/workers/presets/data-access/interfaces/IPresetLogRepository";
import { VPContainer } from "#ie/vehicle-positions/ioc/Di";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { CisStopRepository } from "#ie/vehicle-positions/workers/vehicle-positions/data-access/CisStopRepository";
import { PositionsHistoryRepository } from "#ie/vehicle-positions/workers/vehicle-positions/data-access/PositionsHistoryRepository";
import { StopTimesHistoryRepository } from "#ie/vehicle-positions/workers/vehicle-positions/data-access/StopTimesHistoryRepository";
import { TripsHistoryRepository } from "#ie/vehicle-positions/workers/vehicle-positions/data-access/TripsHistoryRepository";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { AbstractRunRepository } from "../../vehicle-positions/workers/runs/data-access/AbstractRunRepository";
import { CommonRunsRepository } from "../../vehicle-positions/workers/runs/data-access/CommonRunsRepository";
import { MetroRunsMessagesRepository } from "../../vehicle-positions/workers/runs/data-access/MetroRunsMessagesRepository";
import { RegionalBusRunsMessagesRepository } from "../../vehicle-positions/workers/runs/data-access/RegionalBusRunsMessagesRepository";
import { DescriptorRepository } from "../../vehicle-positions/workers/vehicle-descriptors/data-access/DescriptorRepository";
import { PositionsRepository } from "../../vehicle-positions/workers/vehicle-positions/data-access/PositionsRepository";
import { TripsRepository } from "../../vehicle-positions/workers/vehicle-positions/data-access/TripsRepository";
import { RepositoryTableName } from "../workers/constants/RepositoryTableNameEnum";

export class SelectRepositoryFactory {
    public select(repo: RepositoryTableName): IPresetLogRepository | AbstractRunRepository {
        switch (repo) {
            case RepositoryTableName.Trips:
                return VPContainer.resolve<TripsRepository>(VPContainerToken.TripRepository);
            case RepositoryTableName.TripsHistory:
                return new TripsHistoryRepository();
            case RepositoryTableName.Positions:
                return new PositionsRepository();
            case RepositoryTableName.PositionsHistory:
                return new PositionsHistoryRepository();
            case RepositoryTableName.CisStops:
                return new CisStopRepository();
            case RepositoryTableName.StopTimesHistory:
                return new StopTimesHistoryRepository();
            case RepositoryTableName.Descriptor:
                return VPContainer.resolve<DescriptorRepository>(VPContainerToken.DescriptorRepository);
            case RepositoryTableName.Metro:
                return new MetroRunsMessagesRepository();
            case RepositoryTableName.RegionalBus:
                return new RegionalBusRunsMessagesRepository();
            case RepositoryTableName.CommonRuns:
                return new CommonRunsRepository();
            case RepositoryTableName.Logs:
                return VPContainer.resolve<IPresetLogRepository>(ModuleContainerToken.PresetLogRepository);
            default:
                throw new GeneralError("Repository type undefined.");
        }
    }

    public getColumnNameByRepositoryName(repo: RepositoryTableName) {
        switch (repo) {
            case RepositoryTableName.Metro:
                return "message_timestamp";
            case RepositoryTableName.RegionalBus:
                return "vehicle_timestamp";
            case RepositoryTableName.CommonRuns:
                return "msg_last_timestamp";
            default:
                return "";
        }
    }
}
