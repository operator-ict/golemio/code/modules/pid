import { RetentionContainer } from "#ie/data-retention/ioc/Di";
import { RetentionContainerToken } from "#ie/data-retention/ioc/RetentionContainerToken";
import { WORKER_NAME } from "#ie/data-retention/workers/constants/WorkerName";
import { TableStorageRetentionTask } from "#ie/data-retention/workers/tasks/TableStorageRetentionTask";
import { AbstractTask } from "@golemio/core/dist/integration-engine";
import { AbstractWorker } from "@golemio/core/dist/integration-engine/workers";
import { DeleteDataTask } from "#ie/data-retention/workers/tasks/DeleteDataTask";

export class DataRetentionWorker extends AbstractWorker {
    protected name = WORKER_NAME;

    constructor() {
        super();

        // Register tasks
        this.registerTask(
            RetentionContainer.resolve<TableStorageRetentionTask>(RetentionContainerToken.TableStorageRetentionTask)
        );
        this.registerTask(new DeleteDataTask(this.getQueuePrefix()));
    }

    public registerTask = (task: AbstractTask<any>): void => {
        super.registerTask(task);
        task.queuePrefix = this.getQueuePrefix();
    };
}
