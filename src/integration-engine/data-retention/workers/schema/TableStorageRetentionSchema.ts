import { StorageTableName } from "#ie/data-retention/workers/constants/TableStorageNameEnum";
import { ITableStorageRetentionParams } from "#ie/data-retention/workers/interfaces/ITableStorageRetentionParams";
import { IsEnum } from "@golemio/core/dist/shared/class-validator";

export class TableStorageRetentionSchema implements ITableStorageRetentionParams {
    @IsEnum(StorageTableName)
    tableName!: string;
}
