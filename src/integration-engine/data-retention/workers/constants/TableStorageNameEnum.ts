export enum StorageTableName {
    TRAM = "TcpDppTramData",
    BUS = "TcpDppBusData",
    METRO = "TcpDppMetroData",
    ARRIVA = "TcpArrivaCityData",
}
