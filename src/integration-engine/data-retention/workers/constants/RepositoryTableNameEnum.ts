export enum RepositoryTableName {
    Trips = "TripsRepository",
    TripsHistory = "TripsHistoryRepository",
    Positions = "PositionsRepository",
    PositionsHistory = "PositionsHistoryRepository",
    CisStops = "CisStopRepository",
    StopTimesHistory = "StopTimesHistoryRepository",
    Descriptor = "DescriptorRepository",
    Metro = "MetroRunsMessagesRepository",
    RegionalBus = "RegionalBusRunsMessagesRepository",
    CommonRuns = "CommonRunsRepository",
    Logs = "PresetLogRepository",
}
