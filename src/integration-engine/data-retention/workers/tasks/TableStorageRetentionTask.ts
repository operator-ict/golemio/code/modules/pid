import { ITableStorageRetentionParams } from "#ie/data-retention/workers/interfaces/ITableStorageRetentionParams";
import { TableStorageRetentionSchema } from "#ie/data-retention/workers/schema/TableStorageRetentionSchema";
import { ITableStorageService } from "@golemio/core/dist/helpers/data-access/table-storage/providers/interfaces/ITableStorageService";
import { AbstractTask } from "@golemio/core/dist/integration-engine";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { WORKER_NAME } from "../constants/WorkerName";

@injectable()
export class TableStorageRetentionTask extends AbstractTask<ITableStorageRetentionParams> {
    public readonly queueName = "deleteMonthOldStorageData";
    public readonly queueTtl = 60 * 60 * 1000; // 1 hour
    public readonly schema = TableStorageRetentionSchema;

    constructor(@inject(ContainerToken.TableStorageService) private storageService: ITableStorageService) {
        super(WORKER_NAME);
    }

    protected async execute(data: ITableStorageRetentionParams) {
        const monthAgoMs = new Date(new Date().setHours(0, 0, 0, 0)).setMonth(new Date().getMonth() - 1);
        try {
            await this.storageService.deleteEntitiesOlderThan(data.tableName, new Date(monthAgoMs).toISOString());
        } catch (err) {
            throw new GeneralError("Error while deleting old storage data", this.constructor.name, err);
        }
    }
}
