import { IDataDeletionParams } from "#ie/shared/interfaces/IDataDeletionParams";
import { DataDeletionValidationSchema } from "#ie/shared/schema/DataDeletionSchema";
import { AbstractTask } from "@golemio/core/dist/integration-engine/workers";
import { SelectRepositoryFactory } from "../../helpers/SelectRepositoryFactory";

export class DeleteDataTask extends AbstractTask<IDataDeletionParams> {
    public readonly queueName = "deleteData";
    public readonly queueTtl = 15 * 60 * 1000; // 15 minutes
    public readonly schema = DataDeletionValidationSchema;
    private readonly selectRepositoryFactory: SelectRepositoryFactory;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.selectRepositoryFactory = new SelectRepositoryFactory();
    }

    protected async execute(data: IDataDeletionParams) {
        const repository = this.selectRepositoryFactory.select(data.repoName);
        const columnName = this.selectRepositoryFactory.getColumnNameByRepositoryName(data.repoName);
        await repository.deleteNHoursOldData(data.targetHours, columnName);
    }
}
