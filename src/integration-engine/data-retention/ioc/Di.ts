import { TableStorageRetentionTask } from "#ie/data-retention/workers/tasks/TableStorageRetentionTask";
import { PidContainer } from "#ie/ioc/Di";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import { RetentionContainerToken } from "./RetentionContainerToken";

//#region Initialization
const RetentionContainer: DependencyContainer = PidContainer.createChildContainer();
//#endregion

//#region Tasks
RetentionContainer.registerSingleton(RetentionContainerToken.TableStorageRetentionTask, TableStorageRetentionTask);
//#endregion

export { RetentionContainer };
