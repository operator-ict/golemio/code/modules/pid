const VPContainerToken = {
    //#region Vehicle Positions
    TripRepository: Symbol(),
    PublicStopTimeRepository: Symbol(),
    PublicApiCacheRepository: Symbol(),
    PublicStopTimeCacheRepository: Symbol(),
    PublicApiTripTransformation: Symbol(),
    GtfsTripDataFixerFactory: Symbol(),
    HttpGtfsTripDataFixer: Symbol(),
    RefreshPublicTripCacheTask: Symbol(),
    RefreshPublicStopTimeCacheTask: Symbol(),
    RefreshGtfsTripDataTask: Symbol(),
    PositionsManager: Symbol(),
    RegionalBusPositionsManager: Symbol(),
    //#endregion

    //#region Runs
    RegionalBusCisCacheRepository: Symbol(),
    RegionalBusGtfsCacheRepository: Symbol(),
    RegionalBusRunsMessagesRepository: Symbol(),
    RegionalBusRunsMessagesTransformation: Symbol(),
    CisLookupManager: Symbol(),
    GtfsLookupManager: Symbol(),
    TripScheduleManager: Symbol(),
    BusMessageFilter: Symbol(),
    TramMessageFilter: Symbol(),
    RegionalBusMessageFilter: Symbol(),
    RegionalBusRunsFacade: Symbol(),
    SaveArrivaCityRunsToDBTask: Symbol(),
    SaveTramRunsToDBTask: Symbol(),
    SaveBusRunsToDBTask: Symbol(),
    SaveMetroRunsToDBTask: Symbol(),
    ProcessMetroRunMessagesTask: Symbol(),
    ProcessRegionalBusRunMessagesTask: Symbol(),
    MetroRunsMessagesTransformation: Symbol(),
    CommonRunsMessagesTransformation: Symbol(),

    //#endregion

    //#region Vehicle Descriptors
    SeznamAutobusuDataSourceProvider: Symbol(),
    DescriptorDataSourceFactory: Symbol(),
    DescriptorRepository: Symbol(),
    DescriptorTransformation: Symbol(),
    DescriptorFilter: Symbol(),
    RefreshDescriptorsTask: Symbol(),
    DeleteDataTask: Symbol(),
    //#endregion

    //#region Vehicle Positions

    //#region DataAccess
    MetroRailtrackGPSRepository: Symbol(),
    CachedMetroRailTrackLookup: Symbol(),
    //#endregion

    //#region Helpers
    MetroShapePointsFixer: Symbol(),
    TimestampValidator: Symbol(),
    ValidToCalculator: Symbol(),
    //#endregion

    //#endregion
};

export { VPContainerToken };
