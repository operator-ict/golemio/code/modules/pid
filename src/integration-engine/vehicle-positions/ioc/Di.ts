import { PidContainer } from "#ie/ioc/Di";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import { RegionalBusRunsMessagesRepository } from "../workers/runs/data-access/RegionalBusRunsMessagesRepository";
import { RegionalBusCisCacheRepository } from "../workers/runs/data-access/cache/RegionalBusCisCacheRepository";
import { RegionalBusGtfsCacheRepository } from "../workers/runs/data-access/cache/RegionalBusGtfsCacheRepository";
import { BusMessageFilter } from "../workers/runs/helpers/BusMessageFilter";
import { TimestampValidator } from "../workers/runs/helpers/TimestampValidator";
import { TramMessageFilter } from "../workers/runs/helpers/TramMessageFilter";
import { CisLookupManager } from "../workers/runs/helpers/regional-bus/CisLookupManager";
import { GtfsLookupManager } from "../workers/runs/helpers/regional-bus/GtfsLookupManager";
import { RegionalBusMessageFilter } from "../workers/runs/helpers/regional-bus/RegionalBusMessageFilter";
import { RegionalBusRunsFacade } from "../workers/runs/helpers/regional-bus/RegionalBusRunsFacade";
import { TripScheduleManager } from "../workers/runs/helpers/regional-bus/TripScheduleManager";
import { ProcessMetroRunMessagesTask } from "../workers/runs/tasks/ProcessMetroRunMessagesTask";
import { ProcessRegionalBusRunMessagesTask } from "../workers/runs/tasks/ProcessRegionalBusRunMessagesTask";
import { SaveArrivaCityRunsToDBTask } from "../workers/runs/tasks/SaveArrivaCityRunsToDBTask";
import { SaveBusRunsToDBTask } from "../workers/runs/tasks/SaveBusRunsToDBTask";
import { SaveMetroRunsToDBTask } from "../workers/runs/tasks/SaveMetroRunsToDBTask";
import { SaveTramRunsToDBTask } from "../workers/runs/tasks/SaveTramRunsToDBTask";
import { CommonRunsMessagesTransformation } from "../workers/runs/transformations/CommonRunsMessagesTransformation";
import { MetroRunsMessagesTransformation } from "../workers/runs/transformations/MetroRunsMessagesTransformation";
import { RegionalBusRunsMessagesTransformation } from "../workers/runs/transformations/RegionalBusRunsMessagesTransformation";
import { DescriptorRepository } from "../workers/vehicle-descriptors/data-access/DescriptorRepository";
import { DescriptorDataSourceFactory } from "../workers/vehicle-descriptors/datasources/DescriptorDataSourceFactory";
import { SeznamAutobusuDataSourceProvider } from "../workers/vehicle-descriptors/datasources/seznam-autobusu/SeznamAutobusuDataSourceProvider";
import { DescriptorFilter } from "../workers/vehicle-descriptors/helpers/DescriptorFilter";
import { RefreshDescriptorsTask } from "../workers/vehicle-descriptors/tasks/RefreshDescriptorsTask";
import { DescriptorTransformation } from "../workers/vehicle-descriptors/transformations/DescriptorTransformation";
import { TripsRepository } from "../workers/vehicle-positions/data-access/TripsRepository";
import { PublicApiCacheRepository } from "../workers/vehicle-positions/data-access/cache/PublicApiCacheRepository";
import { PublicStopTimeCacheRepository } from "../workers/vehicle-positions/data-access/cache/PublicStopTimeCacheRepository";
import { CachedMetroRailTrackLookup } from "../workers/vehicle-positions/data-access/metro/CachedMetroRailtrackLookup";
import { PublicStopTimeRepository } from "../workers/vehicle-positions/data-access/views/PublicStopTimeRepository";
import { PositionsManager } from "../workers/vehicle-positions/helpers/PositionsManager";
import { ValidToCalculator } from "../workers/vehicle-positions/helpers/ValidToCalculator";
import { GtfsTripDataFixerFactory } from "../workers/vehicle-positions/helpers/gtfs-trip-data/GtfsTripDataFixerFactory";
import { HttpGtfsTripDataFixer } from "../workers/vehicle-positions/helpers/gtfs-trip-data/strategy/HttpGtfsTripDataFixer";
import { MetroShapePointsFixer } from "../workers/vehicle-positions/helpers/metro/MetroShapePointsFixer";
import { RegionalBusPositionsManager } from "../workers/vehicle-positions/helpers/regional-bus/RegionalBusPositionsManager";
import { RefreshGtfsTripDataTask } from "../workers/vehicle-positions/tasks/RefreshGtfsTripDataTask";
import { RefreshPublicStopTimeCacheTask } from "../workers/vehicle-positions/tasks/RefreshPublicStopTimeCacheTask";
import { RefreshPublicTripCacheTask } from "../workers/vehicle-positions/tasks/RefreshPublicTripCacheTask";
import { PublicApiTripTransformation } from "../workers/vehicle-positions/transformations/PublicApiTripTransformation";
import { VPContainerToken } from "./VPContainerToken";
import { MetroRailtrackGPSRepository } from "#ie/ropid-gtfs/workers/timetables/tasks/data-access/MetroRailtrackGPSRepository";

//#region Initialization
const VPContainer: DependencyContainer = PidContainer.createChildContainer();
//#endregion

//#region Datasources
VPContainer.register(VPContainerToken.SeznamAutobusuDataSourceProvider, SeznamAutobusuDataSourceProvider);
VPContainer.registerSingleton(VPContainerToken.DescriptorDataSourceFactory, DescriptorDataSourceFactory);
//#endregion

//#region Repositories
VPContainer.register(VPContainerToken.TripRepository, TripsRepository);
VPContainer.register(VPContainerToken.DescriptorRepository, DescriptorRepository);
VPContainer.register(VPContainerToken.RegionalBusCisCacheRepository, RegionalBusCisCacheRepository);
VPContainer.register(VPContainerToken.RegionalBusGtfsCacheRepository, RegionalBusGtfsCacheRepository);
VPContainer.register(VPContainerToken.RegionalBusRunsMessagesRepository, RegionalBusRunsMessagesRepository);
VPContainer.register(VPContainerToken.PublicStopTimeRepository, PublicStopTimeRepository);
VPContainer.register(VPContainerToken.PublicApiCacheRepository, PublicApiCacheRepository);
VPContainer.register(VPContainerToken.PublicStopTimeCacheRepository, PublicStopTimeCacheRepository);
VPContainer.registerSingleton(VPContainerToken.MetroRailtrackGPSRepository, MetroRailtrackGPSRepository);
VPContainer.registerSingleton(VPContainerToken.CachedMetroRailTrackLookup, CachedMetroRailTrackLookup);
//#endregion

//#region Transformations
VPContainer.register(VPContainerToken.DescriptorTransformation, DescriptorTransformation);
VPContainer.register(VPContainerToken.RegionalBusRunsMessagesTransformation, RegionalBusRunsMessagesTransformation);
VPContainer.register(VPContainerToken.PublicApiTripTransformation, PublicApiTripTransformation);
VPContainer.register(VPContainerToken.CommonRunsMessagesTransformation, CommonRunsMessagesTransformation);
VPContainer.register(VPContainerToken.MetroRunsMessagesTransformation, MetroRunsMessagesTransformation);
//#endregion

//#region Helpers
VPContainer.registerSingleton(VPContainerToken.DescriptorFilter, DescriptorFilter);
VPContainer.registerSingleton(VPContainerToken.CisLookupManager, CisLookupManager);
VPContainer.registerSingleton(VPContainerToken.GtfsLookupManager, GtfsLookupManager);
VPContainer.registerSingleton(VPContainerToken.TripScheduleManager, TripScheduleManager);
VPContainer.registerSingleton(VPContainerToken.BusMessageFilter, BusMessageFilter);
VPContainer.registerSingleton(VPContainerToken.TramMessageFilter, TramMessageFilter);
VPContainer.registerSingleton(VPContainerToken.RegionalBusMessageFilter, RegionalBusMessageFilter);
VPContainer.registerSingleton(VPContainerToken.RegionalBusRunsFacade, RegionalBusRunsFacade);
VPContainer.registerSingleton(VPContainerToken.HttpGtfsTripDataFixer, HttpGtfsTripDataFixer);
VPContainer.registerSingleton(VPContainerToken.GtfsTripDataFixerFactory, GtfsTripDataFixerFactory);
VPContainer.registerSingleton(VPContainerToken.TimestampValidator, TimestampValidator);
VPContainer.registerSingleton(VPContainerToken.TimestampValidator, TimestampValidator);
VPContainer.registerSingleton(VPContainerToken.ValidToCalculator, ValidToCalculator);
VPContainer.registerSingleton(VPContainerToken.PositionsManager, PositionsManager);
VPContainer.registerSingleton(VPContainerToken.RegionalBusPositionsManager, RegionalBusPositionsManager);
VPContainer.register(VPContainerToken.MetroShapePointsFixer, MetroShapePointsFixer);
//#endregion

//#region Tasks
VPContainer.registerSingleton(VPContainerToken.RefreshDescriptorsTask, RefreshDescriptorsTask);
VPContainer.registerSingleton(VPContainerToken.SaveArrivaCityRunsToDBTask, SaveArrivaCityRunsToDBTask);
VPContainer.registerSingleton(VPContainerToken.ProcessRegionalBusRunMessagesTask, ProcessRegionalBusRunMessagesTask);
VPContainer.registerSingleton(VPContainerToken.RefreshPublicTripCacheTask, RefreshPublicTripCacheTask);
VPContainer.registerSingleton(VPContainerToken.RefreshPublicStopTimeCacheTask, RefreshPublicStopTimeCacheTask);
VPContainer.registerSingleton(VPContainerToken.RefreshGtfsTripDataTask, RefreshGtfsTripDataTask);
VPContainer.register(VPContainerToken.SaveTramRunsToDBTask, SaveTramRunsToDBTask);
VPContainer.register(VPContainerToken.SaveBusRunsToDBTask, SaveBusRunsToDBTask);
VPContainer.register(VPContainerToken.SaveMetroRunsToDBTask, SaveMetroRunsToDBTask);
VPContainer.register(VPContainerToken.ProcessMetroRunMessagesTask, ProcessMetroRunMessagesTask);
//#endregion

export { VPContainer };
