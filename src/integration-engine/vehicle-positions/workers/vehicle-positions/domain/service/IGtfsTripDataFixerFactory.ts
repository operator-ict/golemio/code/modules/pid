import { ProviderSourceTypeEnum } from "../../helpers/ProviderSourceTypeEnum";
import { AbstractGtfsTripDataFixer } from "../../helpers/gtfs-trip-data/strategy/AbstractGtfsTripDataFixer";

export interface IGtfsTripDataFixerFactory {
    getStrategy(providerSourceType: ProviderSourceTypeEnum): AbstractGtfsTripDataFixer;
}
