export enum PositionHandlerEnum {
    TRACKING = "TRACKING",
    NOT_TRACKING = "NOT_TRACKING",
    CANCELED = "CANCELED",
    DO_NOTHING = "DO_NOTHING",
}
