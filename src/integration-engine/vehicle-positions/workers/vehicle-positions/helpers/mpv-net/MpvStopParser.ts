import { IMpvStopContent } from "#sch/vehicle-positions/interfaces/IMpvMessageInterfaces";

export class MpvStopParser {
    /**
     * Parse and sanitize current platform code
     *   1/3 -> 1/3
     *   -/3 -> 3
     *   1/- -> 1
     *   -/BUS -> BUS
     *   2/100V -> 2/100V
     *   -/5 C-F -> 5
     *   2/5 A -> 2/5
     *   5A/- A C -> 5A
     */
    static parsePlatformCode(cisStop: IMpvStopContent): string | null {
        const platformCode = cisStop.$.stan;
        if (!platformCode) {
            return null;
        }

        return platformCode.replace(/-\/|\/-|\s+.*$/g, "");
    }
}
