import { IVPTripsLastPositionContext } from "#sch/vehicle-positions/models/interfaces/VPTripsLastPositionInterfaces";
import { StatePositionEnum } from "src/const";

export class PositionStateDelayManager {
    /**
     * Check if delay should be propagated for after track state
     */
    public static shouldPropagateAfterTrackDelay(context: IVPTripsLastPositionContext | null): boolean {
        if (context && context.lastPositionState !== StatePositionEnum.AFTER_TRACK && context.lastPositionStateChange) {
            const nowTimestamp = new Date().getTime();
            const stateChangedTimestamp = new Date(context.lastPositionStateChange).getTime();

            // state was changed in the last 30 seconds, then it is allowed to propagate delay
            return context.lastPositionState === StatePositionEnum.AFTER_TRACK_DELAYED
                ? nowTimestamp - stateChangedTimestamp <= 30 * 1000
                : true;
        }

        return false;
    }
}
