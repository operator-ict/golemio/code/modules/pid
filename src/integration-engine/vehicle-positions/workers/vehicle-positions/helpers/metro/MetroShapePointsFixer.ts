import { GeoMeasurementHelper } from "#helpers/geo/GeoMeasurementHelper";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { IStopTime } from "#ie/ropid-gtfs/interfaces/TripModelInterfaces";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { MetroRouteHelper } from "#ie/vehicle-positions/workers/runs/helpers/MetroRouteHelper";
import { IMetroRailtrackGPSModel } from "#sch/ropid-gtfs/models/railtrack/interfaces/IMetroRailtrackGPSModel";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { Point } from "cheap-ruler";
import { CachedMetroRailTrackLookup } from "../../data-access/metro/CachedMetroRailtrackLookup";
import { IComputationTrip, IShapeAnchorPoint } from "../../interfaces/VPInterfaces";

@injectable()
export class MetroShapePointsFixer {
    constructor(
        @inject(VPContainerToken.CachedMetroRailTrackLookup) private cachedMetroRailTrackLookup: CachedMetroRailTrackLookup,
        @inject(ModuleContainerToken.GeoMeasurementHelper) private geoMeasurementHelper: GeoMeasurementHelper
    ) {}

    public async fix(data: IComputationTrip): Promise<IComputationTrip> {
        try {
            const clonedData = structuredClone(data);

            // load metro obvody
            const metroRailtrackLookup: IMetroRailtrackGPSModel[] = await this.cachedMetroRailTrackLookup.getAllByRouteName(
                MetroRouteHelper.getRouteName(data.trip_id)
            );

            let lastKnownStopSequence: number | null = null;
            const maxStopSequence = clonedData.stop_times.length;

            for (let index = 0; index < clonedData.shapes_anchor_points.length; index++) {
                const shape = clonedData.shapes_anchor_points[index];
                const possibleStops = this.getPossibleStops(clonedData.stop_times, lastKnownStopSequence);
                const closestMetroRailtrack = this.getClosestMetroRailtrack(shape, metroRailtrackLookup, possibleStops);
                const thisStopSequence = this.getSequenceByStopId(closestMetroRailtrack.gtfs_stop_id, clonedData.stop_times);

                if (thisStopSequence) {
                    lastKnownStopSequence = thisStopSequence;
                }

                shape.at_stop = closestMetroRailtrack.gtfs_stop_id && thisStopSequence ? true : false;
                shape.this_stop_sequence = thisStopSequence;
                shape.next_stop_sequence = lastKnownStopSequence ? Math.min(lastKnownStopSequence + 1, maxStopSequence) : 2;
                shape.last_stop_sequence = lastKnownStopSequence && lastKnownStopSequence > 1 ? lastKnownStopSequence : 1;

                if (shape.at_stop) {
                    shape.last_stop_sequence = shape.this_stop_sequence!;
                }
            }

            return clonedData;
        } catch (err) {
            throw new GeneralError("Unable to fix metro shape points: " + err.message, this.constructor.name, err);
        }
    }

    private getSequenceByStopId(gtfs_stop_id: string | null, stop_times: IStopTime[]): number | null {
        const regex = /U(?<node>.*)Z/;
        const node = regex.exec(gtfs_stop_id || "")?.groups?.node;

        if (node && node.length > 0) {
            return stop_times.find((stop_time) => regex.exec(stop_time.stop_id)?.groups?.node === node)?.stop_sequence || null;
        } else {
            return null;
        }
    }

    private getClosestMetroRailtrack(
        shape: IShapeAnchorPoint,
        metroRailtrackLookup: IMetroRailtrackGPSModel[],
        possibleStops: string[]
    ) {
        // find closest metroRailtrack for shape.coordinates
        const result = metroRailtrackLookup
            .map((metroRailtrack) => {
                return {
                    ...metroRailtrack,
                    distance: this.geoMeasurementHelper.getDistanceInMeters(
                        shape.coordinates,
                        metroRailtrack.coordinates.coordinates as Point
                    ),
                };
            })
            .sort((a, b) => a.distance - b.distance);

        const resultsWithing15Meters = result.filter((el) => el.distance < 15);
        if (resultsWithing15Meters.length > 0) {
            for (const railtrack of resultsWithing15Meters) {
                if (possibleStops.some((el) => el === railtrack.gtfs_stop_id)) {
                    return railtrack;
                }
            }

            return resultsWithing15Meters[0];
        }

        // if no railtrack is within 15 meters, return closest railtrack without stop_id
        return result.filter((el) => !el.gtfs_stop_id)[0];
    }

    private getPossibleStops(stopTimes: IStopTime[], lastKnownStopSequence: number | null) {
        // since we dont know direction for railtrack data, we check concrete stops in one direction
        // to ensure proper matching with shape
        const result = [];
        if (lastKnownStopSequence === null) {
            result.push(stopTimes[0].stop_id);
        } else {
            result.push(stopTimes[lastKnownStopSequence - 1].stop_id);
            if (lastKnownStopSequence < stopTimes.length) {
                result.push(stopTimes[lastKnownStopSequence].stop_id);
            }
        }

        return result;
    }
}
