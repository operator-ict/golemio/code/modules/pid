import { StatePositionEnum } from "src/const";

export interface IPositionStateAndStopSequences {
    statePosition: StatePositionEnum;
    thisStopSequence: number | null;
    lastStopSequence: number;
    nextStopSequence: number | null;
}
