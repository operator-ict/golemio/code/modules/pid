import { DateTimeUtils } from "#helpers/DateTimeUtils";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { IVPTripsPositionAttributes } from "#sch/vehicle-positions/models/interfaces/IVPTripsPositionAttributes";
import { IVPTripsLastPositionContext } from "#sch/vehicle-positions/models/interfaces/VPTripsLastPositionInterfaces";
import { IScheduleDto } from "#sch/vehicle-positions/redis/interfaces/IGtfsRunTripCacheDto";
import moment from "@golemio/core/dist/shared/moment-timezone";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import * as turf from "@turf/turf";
import { Feature, Point } from "@turf/turf";
import { StatePositionEnum, StateProcessEnum, TCPEventEnum } from "src/const";
import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";
import {
    IComputationTrip,
    ICurrentPositionProperties,
    IPositionToUpdate,
    IProcessedPositions,
    IShapeAnchorPoint,
    ITripPositionsWithGTFS,
    IUpdatePositionsIteratorOptions,
} from "../interfaces/VPInterfaces";
import DPPUtils from "./DPPUtils";
import { PositionHandlerEnum } from "./PositionHandlerEnum";
import { ValidToCalculator } from "./ValidToCalculator";
import { AnchorPointSegmenter } from "./anchor-points/AnchorPointSegmenter";
import ComputeDelayHelper from "./compute-positions/ComputeDelayHelper";
import { IPositionStateAndStopSequences } from "./interfaces/IPositionStateAndStopSequences";
import { PositionStateDelayManager } from "./state-position/PositionStateDelayManager";

const ONE_DAY_IN_SECONDS = 24 * 60 * 60;
const ONE_SECOND_IN_MILLIS = 1000;

@injectable()
export class PositionsManager {
    constructor(@inject(VPContainerToken.ValidToCalculator) private validToCalculator: ValidToCalculator) {}

    /**
     * Compute positions and return computed positions
     *
     * @param {ITripPositionsWithGTFS} tripPositions - Trip positions with shape anchors data
     * @returns {IProcessedPositions} - Returns computed/updated positions
     */
    public computePositions = (
        tripPositions: ITripPositionsWithGTFS,
        schedule: IScheduleDto[] | undefined
    ): IProcessedPositions => {
        const startTimestamp = tripPositions.start_timestamp.getTime();
        const startDayTimestamp = this.getStartDayTimestamp(
            startTimestamp,
            tripPositions.gtfsData.shapes_anchor_points[0].time_scheduled_seconds
        );

        const gtfsRouteType = tripPositions.gtfs_route_type;
        const context = this.getCurrentContext(tripPositions);
        const computedPositions: IPositionToUpdate[] = [];

        const endTimestamp = tripPositions.end_timestamp;

        return this.updatePositions(
            { tripPositions, startTimestamp, endTimestamp, startDayTimestamp, context, computedPositions, gtfsRouteType },
            schedule
        );
    };

    /**
     * Takes position one by one, set proper handler for type of position, and do the process of position
     *
     * @param {number} i - Iteration
     * @param {IUpdatePositionsIteratorOptions} options - Initial options
     * @param {number} cb - Callback function of iterator
     * @returns {IProcessedPositions} - Returns computed/updated positions
     */
    public updatePositions = (
        options: IUpdatePositionsIteratorOptions,
        schedule: IScheduleDto[] | undefined
    ): IProcessedPositions => {
        const { tripPositions, startDayTimestamp, startTimestamp, endTimestamp, context, computedPositions, gtfsRouteType } =
            options;

        let isBacktrackingDetected = false;

        for (let i = 0; i < tripPositions.positions.length; i++) {
            const position = tripPositions.positions[i];
            let positionToUpdate: IPositionToUpdate | null = null;

            // situations
            switch (this.setPositionUpdateHandler(position)) {
                case PositionHandlerEnum.TRACKING:
                    const currentPosition = turf.point([position.lng, position.lat], {
                        id: position.id,
                        origin_time: position.origin_time,
                        origin_timestamp: position.origin_timestamp,
                        scheduled_timestamp: position.scheduled_timestamp,
                        this_stop_id: position.this_stop_id,
                        tcp_event: position.tcp_event,
                    } as ICurrentPositionProperties);

                    positionToUpdate = this.getEstimatedPoint(
                        tripPositions.gtfsData,
                        currentPosition,
                        context,
                        startDayTimestamp,
                        tripPositions.gtfs_route_type
                    );

                    if (positionToUpdate.state_position !== StatePositionEnum.MISMATCHED) {
                        positionToUpdate = ComputeDelayHelper.updatePositionToUpdate(
                            context,
                            positionToUpdate,
                            position,
                            gtfsRouteType
                        );
                    }

                    positionToUpdate.bearing = position.bearing ?? positionToUpdate.bearing;
                    break;
                case PositionHandlerEnum.NOT_TRACKING:
                    // if there is no previous positions with tracking status, set position as before_track
                    const firstShapesAnchorPoint: IShapeAnchorPoint = tripPositions.gtfsData.shapes_anchor_points[0];
                    if (
                        context.lastPositionTracking === null &&
                        (endTimestamp === null ||
                            position.scheduled_timestamp === null ||
                            endTimestamp > position.scheduled_timestamp)
                    ) {
                        const firstStopTime = tripPositions.gtfsData.stop_times[0];
                        // if there is propagated delay we can use it for new position
                        const lastPositionDelayed = context.lastPositionBeforeTrackDelayed;
                        // if there is no delay to duplicate and DPP trip is far from start then invisible
                        const setAsInvisible: boolean =
                            !lastPositionDelayed &&
                            DPPUtils.isInvisible(
                                tripPositions.agency_name_scheduled,
                                position.origin_timestamp,
                                startTimestamp,
                                [position.lng, position.lat],
                                firstShapesAnchorPoint.coordinates,
                                schedule,
                                tripPositions.gtfs_trip_id,
                                context.lastPositionTracking,
                                position.is_tracked
                            );

                        positionToUpdate = {
                            id: position.id,
                            next_stop_arrival_time: DateTimeUtils.getStopDateTimeForDayStart(
                                firstStopTime.arrival_time_seconds,
                                startDayTimestamp
                            ),
                            next_stop_departure_time: DateTimeUtils.getStopDateTimeForDayStart(
                                firstStopTime.departure_time_seconds,
                                startDayTimestamp
                            ),
                            next_stop_id: firstStopTime.stop_id,
                            next_stop_sequence: firstStopTime.stop_sequence,
                            next_stop_name: firstStopTime.stop.stop_name,
                            shape_dist_traveled: firstStopTime.shape_dist_traveled,
                            state_position: setAsInvisible ? StatePositionEnum.INVISIBLE : StatePositionEnum.BEFORE_TRACK,
                            state_process: StateProcessEnum.PROCESSED,
                            ...(firstStopTime.stop_headsign && {
                                last_stop_headsign: firstStopTime.stop_headsign,
                            }),
                        };

                        if (positionToUpdate.state_position === StatePositionEnum.BEFORE_TRACK) {
                            positionToUpdate.delay = this.getDelayBeforeTrack(
                                lastPositionDelayed?.delay ?? position.delay,
                                firstStopTime.departure_time_seconds,
                                position.origin_timestamp.getTime(),
                                startDayTimestamp
                            );
                        }
                    } else {
                        // if there is tracking 2 position with same origin_timestamp then this position is duplicate
                        const statePosition =
                            position.origin_timestamp.getTime() === context.lastPositionOriginTimestamp ||
                            tripPositions.positions.findIndex(
                                (positionItem) =>
                                    positionItem.origin_timestamp.getTime() === position.origin_timestamp.getTime() &&
                                    positionItem.is_tracked
                            ) >= 0
                                ? StatePositionEnum.DUPLICATE
                                : StatePositionEnum.AFTER_TRACK;
                        const lastShapesAnchorPoint: IShapeAnchorPoint =
                            tripPositions.gtfsData.shapes_anchor_points[tripPositions.gtfsData.shapes_anchor_points.length - 1];
                        const lastStopTime = tripPositions.gtfsData.stop_times[tripPositions.gtfsData.stop_times.length - 1];
                        // set as invisible if there are some AFTER_TRACK before
                        const setAsInvisible: boolean =
                            DPPUtils.isInvisible(
                                tripPositions.agency_name_scheduled,
                                position.origin_timestamp,
                                startTimestamp,
                                [position.lng, position.lat],
                                firstShapesAnchorPoint.coordinates,
                                schedule,
                                tripPositions.gtfs_trip_id,
                                context.lastPositionTracking,
                                position.is_tracked
                            ) && statePosition === StatePositionEnum.AFTER_TRACK;
                        const lastShapesAnchorPointTimeScheduled = DateTimeUtils.getStopDateTimeForDayStart(
                            lastShapesAnchorPoint.time_scheduled_seconds,
                            startDayTimestamp
                        );
                        positionToUpdate = {
                            id: position.id,
                            last_stop_arrival_time: lastShapesAnchorPointTimeScheduled,
                            last_stop_departure_time: lastShapesAnchorPointTimeScheduled,
                            last_stop_id: lastStopTime.stop_id,
                            last_stop_sequence: lastShapesAnchorPoint.last_stop_sequence,
                            shape_dist_traveled: lastShapesAnchorPoint.shape_dist_traveled,
                            state_position: setAsInvisible ? StatePositionEnum.INVISIBLE : statePosition,
                            state_process: StateProcessEnum.PROCESSED,
                            delay: position.delay ?? null,
                            ...(lastStopTime.stop_headsign && {
                                last_stop_headsign: lastStopTime.stop_headsign,
                            }),
                        };

                        // if state was changed in last 30 seconds, then it is AFTER_TRACK_DELAYED
                        // with delay computation (copied from delay at stop)
                        if (
                            positionToUpdate.state_position === StatePositionEnum.AFTER_TRACK &&
                            PositionStateDelayManager.shouldPropagateAfterTrackDelay(context)
                        ) {
                            positionToUpdate.state_position = StatePositionEnum.AFTER_TRACK_DELAYED;
                        }

                        positionToUpdate = ComputeDelayHelper.updatePositionToUpdate(
                            context,
                            positionToUpdate,
                            position,
                            gtfsRouteType
                        );

                        if (positionToUpdate.state_position === StatePositionEnum.AFTER_TRACK_DELAYED) {
                            positionToUpdate.delay = positionToUpdate.delay_stop_arrival ?? null;
                        }
                    }
                    break;
                case PositionHandlerEnum.CANCELED:
                    positionToUpdate = {
                        id: position.id,
                        lat: position.lat ?? context.lastPositionLat ?? null,
                        lng: position.lng ?? context.lastPositionLng ?? null,
                        state_position: StatePositionEnum.CANCELED,
                        state_process: StateProcessEnum.PROCESSED,
                    };
                    if (positionToUpdate.lat === null || positionToUpdate.lng === null) {
                        // Null island, to be filtered out on all APIs except for departure boards
                        // where it is used to indicate a canceled trip
                        positionToUpdate.lat = 0;
                        positionToUpdate.lng = 0;
                    }
                    break;
                case PositionHandlerEnum.DO_NOTHING:
                    break;
                default:
                    break;
            }

            // if not null push to update
            if (positionToUpdate) {
                positionToUpdate.is_tracked ??= position.is_tracked;
                positionToUpdate.valid_to = this.validToCalculator.getValidToAttribute(
                    positionToUpdate,
                    position,
                    gtfsRouteType,
                    tripPositions.gtfsData,
                    startTimestamp
                );

                // backtracking detection so we can later invalidate old positions
                if (!isBacktrackingDetected && positionToUpdate.last_stop_sequence && context.lastStopSequence) {
                    isBacktrackingDetected = positionToUpdate.last_stop_sequence < context.lastStopSequence;
                }

                computedPositions.push(positionToUpdate);
            } else {
                computedPositions.push(null);
            }

            // set last known BEFORE_TRACK_DELAYED position
            if (position.state_position === StatePositionEnum.BEFORE_TRACK_DELAYED) {
                context.lastPositionBeforeTrackDelayed = {
                    delay: positionToUpdate?.delay ?? position.delay,
                    origin_timestamp: positionToUpdate?.origin_timestamp ?? position.origin_timestamp,
                };
            }
            // set last position tracking (only for at_stop and on_track)
            if (
                positionToUpdate?.state_position === StatePositionEnum.AT_STOP ||
                positionToUpdate?.state_position === StatePositionEnum.ON_TRACK ||
                position.state_position === StatePositionEnum.AT_STOP ||
                position.state_position === StatePositionEnum.ON_TRACK
            ) {
                context.lastPositionTracking = turf.point([position.lng, position.lat], {
                    ...position,
                    ...positionToUpdate,
                } as IVPTripsPositionAttributes);
            }

            // set new first position at stop streak if this stop seqence is set and it is not same as before
            if (
                positionToUpdate
                    ? positionToUpdate.this_stop_sequence &&
                      context.atStopStreak.stop_sequence !== positionToUpdate.this_stop_sequence
                    : position.this_stop_sequence && context.atStopStreak.stop_sequence !== position.this_stop_sequence
            ) {
                context.atStopStreak.stop_sequence = positionToUpdate
                    ? positionToUpdate.this_stop_sequence!
                    : position.this_stop_sequence;
                context.atStopStreak.firstPositionTimestamp = position.origin_timestamp.getTime();
                context.atStopStreak.firstPositionDelay = positionToUpdate ? positionToUpdate.delay! : position.delay;
            }
            // IF currently valid updated position / position was processed before
            // and it is NOT AT_STOP
            // then disrupt atStopStreak
            if (
                positionToUpdate &&
                positionToUpdate.state_position !== StatePositionEnum.MISMATCHED &&
                !positionToUpdate.this_stop_sequence
            ) {
                context.atStopStreak.stop_sequence = null;
            } else if (!positionToUpdate && !position.this_stop_sequence) {
                context.atStopStreak.stop_sequence = null;
            }

            // duplicated or mismatched position should not be considered at all
            if (
                positionToUpdate &&
                positionToUpdate.state_position !== StatePositionEnum.DUPLICATE &&
                positionToUpdate.state_position !== StatePositionEnum.INVISIBLE &&
                positionToUpdate.state_position !== StatePositionEnum.MISMATCHED
            ) {
                context.lastPositionId = position.id;
                context.lastPositionLat = position.lat;
                context.lastPositionLng = position.lng;
                context.lastPositionCanceled = position.is_canceled;
                context.lastPositionOriginTimestamp = position.origin_timestamp.getTime();
                context.lastStopSequence = positionToUpdate.last_stop_sequence ?? context.lastStopSequence ?? null;

                if (context.lastPositionState !== positionToUpdate.state_position) {
                    context.lastPositionState = positionToUpdate.state_position;
                    context.lastPositionStateChange = new Date().toISOString();
                }

                ComputeDelayHelper.updateContext(context, positionToUpdate, position, gtfsRouteType);
            }
        }

        return {
            context,
            positions: computedPositions,
            isBacktrackingDetected: isBacktrackingDetected,
        };
    };

    private getCurrentContext = (tripPositions: ITripPositionsWithGTFS): IVPTripsLastPositionContext => {
        const context: IVPTripsLastPositionContext = tripPositions.last_position_context ?? {
            atStopStreak: {
                stop_sequence: null,
                firstPositionTimestamp: null,
                firstPositionDelay: null,
            },
            lastPositionLastStop: {
                id: null,
                sequence: null,
                arrival_time: null,
                arrival_delay: null,
                departure_time: null,
                departure_delay: null,
            },
            lastPositionDelay: null,
            lastPositionId: null,
            lastPositionLat: null,
            lastPositionLng: null,
            lastPositionOriginTimestamp: null,
            lastPositionTracking: null,
            lastPositionCanceled: null,
            lastPositionBeforeTrackDelayed: null,
            lastPositionState: null,
            lastStopSequence: null,
            lastPositionStateChange: null,
            tripId: tripPositions.id,
        };

        return context;
    };

    /**
     * Decide how to process input position data
     *
     * @param {IVPTripsPositionAttributes} position - Input vehiclepositions_positions row data
     * @returns {PositionHandlerEnum} - Returns action handler enum
     */
    private setPositionUpdateHandler = (position: IVPTripsPositionAttributes): PositionHandlerEnum => {
        if (position.state_process === StateProcessEnum.PROCESSED || position.state_process === StateProcessEnum.INVALIDATED)
            return PositionHandlerEnum.DO_NOTHING;
        else if (position.is_canceled) return PositionHandlerEnum.CANCELED;
        else if (position.is_tracked) return PositionHandlerEnum.TRACKING;
        else return PositionHandlerEnum.NOT_TRACKING;
    };

    /**
     * Returns estimate of point on shape, where the trip should be with appropriate delay
     *
     * @param {IShapeAnchorPoint[]} tripShapePoints - Precalculated trip shape equidistant points with scheduled times
     * @param {Feature<Point, ICurrentPositionProperties>} currentPosition - Current position of trip
     * @param {IVPTripsLastPositionContext | null} context - Context state, holds information about previous positions
     * @param {number} startDayTimestamp - Unix timestamp of midnight before trip starts
     * @returns {IPositionToUpdate} - Position object to update
     */
    private getEstimatedPoint = (
        tripGtfsData: IComputationTrip,
        currentPosition: Feature<Point, ICurrentPositionProperties>,
        context: IVPTripsLastPositionContext | null,
        startDayTimestamp: number,
        gtfsRouteType: GTFSRouteTypeEnum
    ): IPositionToUpdate => {
        const anchorPointSegmenter = new AnchorPointSegmenter(tripGtfsData.shapes_anchor_points, currentPosition);
        const defaultStatePosition = StatePositionEnum.OFF_TRACK;

        // Initial value
        let estimatedPoint: IPositionToUpdate = {
            id: currentPosition.properties.id,
            state_position: defaultStatePosition,
            state_process: StateProcessEnum.PROCESSED,
            tcp_event: currentPosition.properties.tcp_event,
            valid_to: this.validToCalculator.getDefaultValidToAttribute(currentPosition.properties.origin_timestamp),
            ...(context &&
                context.lastPositionTracking?.properties.last_stop_sequence && {
                    shape_dist_traveled: context.lastPositionTracking?.properties.shape_dist_traveled!,
                    last_stop_arrival_time: context.lastPositionTracking?.properties.last_stop_arrival_time
                        ? new Date(context.lastPositionTracking?.properties.last_stop_arrival_time)
                        : undefined,
                    last_stop_departure_time: context.lastPositionTracking?.properties.last_stop_departure_time
                        ? new Date(context.lastPositionTracking?.properties.last_stop_departure_time)
                        : undefined,
                    last_stop_sequence: context.lastPositionTracking?.properties.last_stop_sequence!,
                    last_stop_id: context.lastPositionTracking?.properties.last_stop_id!,
                }),
        };

        let lastStopSequence: number | null = null;
        if (context?.lastPositionState === StatePositionEnum.BEFORE_TRACK) {
            lastStopSequence = 1;
        } else if (context?.lastPositionTracking) {
            lastStopSequence = context.lastPositionTracking.properties.last_stop_sequence;
        }

        const closesPoint = anchorPointSegmenter.getClosesPoint(lastStopSequence);
        if (!closesPoint) {
            return estimatedPoint;
        }

        return this.getClosestPoint(currentPosition, context, startDayTimestamp, closesPoint, tripGtfsData, gtfsRouteType);
    };

    /**
     * Picks only one closest point for multiple possible points based on delay of last position and stop times
     *
     * @param {Feature<Point, ICurrentPositionProperties>} currentPosition - Feature Point of current position
     * @param {IVPTripsLastPositionContext | null} context - Context state, holds information about previous positions
     * @param {number} startDayTimestamp - Unix timestamp of start of the day
     * @param {IShapeAnchorPoint[]} closestPts - All closest points of possible segments
     * @param {IComputationTrip} tripGtfsData - GTFS data and all set of known positions
     * @returns {IPositionToUpdate} - Result point as position to update in DB
     */
    private getClosestPoint = (
        currentPosition: Feature<Point, ICurrentPositionProperties>,
        context: IVPTripsLastPositionContext | null,
        startDayTimestamp: number,
        thisClosestPoint: IShapeAnchorPoint,
        tripGtfsData: IComputationTrip,
        gtfsRouteType: GTFSRouteTypeEnum
    ): IPositionToUpdate => {
        // want to find minimum difference of our prediction, where the bus should be
        const tripShapePoints = tripGtfsData.shapes_anchor_points;
        const tripStopTimes = tripGtfsData.stop_times;
        const firstTripShapePoint = tripShapePoints[0];
        const lastTripShapePoint = tripShapePoints[tripShapePoints.length - 1];
        const closestShapeDistTraveled = thisClosestPoint.shape_dist_traveled;
        const closestBearing = thisClosestPoint.bearing;

        const timeScheduledTimestamp = DateTimeUtils.getStopDateTimeForDayStart(
            // take stop arrival time if is point at stop
            thisClosestPoint.this_stop_sequence
                ? tripStopTimes[thisClosestPoint.this_stop_sequence - 1].arrival_time_seconds
                : thisClosestPoint.time_scheduled_seconds,
            startDayTimestamp
        );

        let timeDelayInSeconds: number | null = Math.round(
            (currentPosition.properties.origin_timestamp.getTime() - timeScheduledTimestamp.getTime()) / 1000
        );

        const isBusOrTram =
            gtfsRouteType === GTFSRouteTypeEnum.BUS ||
            gtfsRouteType === GTFSRouteTypeEnum.TROLLEYBUS ||
            gtfsRouteType === GTFSRouteTypeEnum.TRAM;

        // push trip into nearest at_stop shape anchor point if trip "P" event  (TCP tram or bus)
        if (
            isBusOrTram &&
            currentPosition.properties.tcp_event === TCPEventEnum.ARRIVAL_ANNOUNCED &&
            thisClosestPoint.this_stop_sequence === null
        ) {
            if (thisClosestPoint.last_stop_sequence === 1 && !context?.lastPositionTracking) {
                thisClosestPoint = firstTripShapePoint;
            } else {
                let movingShapePointIndex = thisClosestPoint.index;
                while (thisClosestPoint.this_stop_sequence === null && movingShapePointIndex < lastTripShapePoint.index) {
                    movingShapePointIndex++;
                    thisClosestPoint = tripShapePoints[movingShapePointIndex];
                }
            }
        }

        // lets correct delay if it is at stop
        if (thisClosestPoint.this_stop_sequence) {
            const thisStopSequence = thisClosestPoint.this_stop_sequence;
            timeDelayInSeconds = this.getCorrectedTimeDelay(timeDelayInSeconds, context, currentPosition, thisStopSequence, {
                departureTime: tripStopTimes[thisStopSequence - 1].departure_time_seconds,
                arrivalTime: tripStopTimes[thisStopSequence - 1].arrival_time_seconds,
            });

            // delay can not be negative if the vehicle is at the first stop
            if (thisStopSequence === firstTripShapePoint.this_stop_sequence) {
                timeDelayInSeconds = Math.max(timeDelayInSeconds, 0);
            }
        }

        let { statePosition, thisStopSequence, lastStopSequence, nextStopSequence } = this.getStateAndStopSequences(
            gtfsRouteType,
            thisClosestPoint,
            currentPosition.properties,
            context
        );

        // test if TCP trip with DEPARTURED event should not be at stop (not applied for last stop)
        if (
            currentPosition.properties.tcp_event === TCPEventEnum.DEPARTURED &&
            thisStopSequence !== lastTripShapePoint.this_stop_sequence
        ) {
            statePosition = StatePositionEnum.ON_TRACK;
        }

        if (isBusOrTram && this.isAfterTrack(lastStopSequence, lastTripShapePoint)) {
            statePosition = StatePositionEnum.AFTER_TRACK;

            if (PositionStateDelayManager.shouldPropagateAfterTrackDelay(context)) {
                statePosition = StatePositionEnum.AFTER_TRACK_DELAYED;
            } else {
                // normal after track positions (without propagation) should always have delay null
                timeDelayInSeconds = null;
            }

            thisStopSequence = lastTripShapePoint.this_stop_sequence;
            lastStopSequence = lastTripShapePoint.last_stop_sequence;
            nextStopSequence = null;
        }

        const isTracked = statePosition === StatePositionEnum.AT_STOP || statePosition === StatePositionEnum.ON_TRACK;

        // save it for result
        const estimatedPoint = {
            id: currentPosition.properties.id,
            // always use shape_dist_traveled and bearing from the closest anchor point
            // even if the vehicle announces arrival (TCP event 'P') outside of the stop
            bearing: closestBearing,
            shape_dist_traveled: closestShapeDistTraveled,
            next_stop_id: nextStopSequence ? tripStopTimes[nextStopSequence - 1].stop_id : null,
            last_stop_id: tripStopTimes[lastStopSequence - 1].stop_id,
            next_stop_name: nextStopSequence ? tripStopTimes[nextStopSequence - 1].stop.stop_name : null,
            last_stop_name: tripStopTimes[lastStopSequence - 1].stop.stop_name,
            next_stop_sequence: nextStopSequence,
            last_stop_sequence: lastStopSequence,
            next_stop_arrival_time: nextStopSequence
                ? DateTimeUtils.getStopDateTimeForDayStart(
                      tripStopTimes[nextStopSequence - 1].arrival_time_seconds,
                      startDayTimestamp
                  )
                : null,
            last_stop_arrival_time: DateTimeUtils.getStopDateTimeForDayStart(
                tripStopTimes[lastStopSequence - 1].arrival_time_seconds,
                startDayTimestamp
            ),
            next_stop_departure_time: nextStopSequence
                ? DateTimeUtils.getStopDateTimeForDayStart(
                      tripStopTimes[nextStopSequence - 1].departure_time_seconds,
                      startDayTimestamp
                  )
                : null,
            last_stop_departure_time: DateTimeUtils.getStopDateTimeForDayStart(
                tripStopTimes[lastStopSequence - 1].departure_time_seconds,
                startDayTimestamp
            ),
            delay: timeDelayInSeconds,
            this_stop_id:
                (statePosition === StatePositionEnum.AT_STOP &&
                    thisStopSequence &&
                    tripStopTimes[thisStopSequence - 1].stop_id) ||
                undefined,
            this_stop_name:
                (statePosition === StatePositionEnum.AT_STOP &&
                    thisStopSequence &&
                    tripStopTimes[thisStopSequence - 1].stop.stop_name) ||
                undefined,
            this_stop_sequence: (statePosition === StatePositionEnum.AT_STOP && thisStopSequence) || undefined,
            last_stop_headsign: tripStopTimes[lastStopSequence - 1].stop_headsign || undefined,
            state_position: statePosition,
            state_process: StateProcessEnum.PROCESSED,
            valid_to: this.validToCalculator.getDefaultValidToAttribute(currentPosition.properties.origin_timestamp),
            is_tracked: isTracked,
        };

        return estimatedPoint;
    };

    /**
     * Corrects time delay at stop with dwelling time
     *
     * @param {number} timeDelay - Initial computed delay in seconds, can be negative for trip ahead
     * @param {IVPTripsLastPositionContext | null} context - Context state, holds information about previous positions
     * @param {Feature<Point, ICurrentPositionProperties>} currentPosition - Feature Point of current position
     * @param {IShapeAnchorPoint} thisClosestPoint - Closest point of shape anchors
     * @param { departureTime: number; arrivalTime: number } stopTimes - departure and arrival stop times in seconds
     * @returns {number} - Result delay in seconds, can be negative for trip ahead
     */
    private getCorrectedTimeDelay = (
        timeDelay: number,
        context: IVPTripsLastPositionContext | null,
        currentPosition: Feature<Point, ICurrentPositionProperties>,
        thisStopSequence: number,
        stopTimes: { departureTime: number; arrivalTime: number }
    ): number => {
        // compute dwell time in stop, most common is zero
        const stopDwellTimeSeconds = stopTimes.departureTime - stopTimes.arrivalTime;
        // if dwell time is sheduled as zero, return initial computed delay
        if (stopDwellTimeSeconds <= 0) {
            return timeDelay;
        }

        // if last position was not in this same stop or there is no last position at all
        if (!context || context.atStopStreak.stop_sequence !== thisStopSequence) {
            // timeDelay >= 0 trip is DELAYED
            // we presume it will lower delay by shortening its scheduled dwell time,
            // cant go under zero of course, trip should not go ahead
            // else trip is AHeAD
            // left computed delay as it was
            return timeDelay >= 0 ? Math.max(timeDelay - stopDwellTimeSeconds, 0) : timeDelay;
        }

        // we presume that first position at same stop is real arrival time
        if (context.atStopStreak.firstPositionDelay! >= 0) {
            // trip was DELAYED before
            // we presume it will lower delay by shortening its scheduled dwell time,
            // cant go under zero of course, trip should not go ahead
            return Math.max(timeDelay - stopDwellTimeSeconds, 0);
        }

        // trip was AHEAD before
        // real dwell time so far
        const realDwellTimeSeconds = Math.round(
            (currentPosition.properties.origin_timestamp.getTime() - context.atStopStreak.firstPositionTimestamp!) / 1000
        );

        // if real dwell is longer than scheduled, then add to negative delay time
        return context.atStopStreak.firstPositionDelay! + Math.max(realDwellTimeSeconds - stopDwellTimeSeconds, 0);
    };

    /**
     * Get delay in seconds for positions before track to ensure correct delay propagation
     *
     * @param lastPositionDelay The last position delay in seconds
     * @param firstStopDepartureTime The first stop departure time in seconds. The time is measured from "noon minus 12h" of the
     * service day (effectively midnight except for days on which daylight savings time changes occur). For times occurring after
     * midnight on the service day, enter the time as a value greater than 24:00:00 in seconds. See also
     * <https://gtfs.org/schedule/reference/#:~:text=A%20phone%20number.-,Time,-%2D%20Time%20in%20the>.
     * @param positionOriginUnixTimestamp The Unix Epoch timestamp of the current position, in milliseconds
     * @param startDayUnixTimestamp The Unix Epoch timestamp of the start of the service day, in milliseconds
     */
    private getDelayBeforeTrack(
        lastPositionDelay: number | null | undefined,
        firstStopDepartureTime: number,
        positionOriginUnixTimestamp: number,
        startDayUnixTimestamp: number
    ): number | null {
        if (typeof lastPositionDelay !== "number") {
            return null;
        }

        const startDayMoment = moment.utc(startDayUnixTimestamp).tz(DateTimeUtils.TIMEZONE);
        startDayMoment.hours(12).minutes(0).seconds(0).milliseconds(0);
        startDayMoment.subtract(12, "hours");
        startDayMoment.add(firstStopDepartureTime, "seconds");
        const departureTimeInSeconds = Math.floor(startDayMoment.toDate().valueOf() / ONE_SECOND_IN_MILLIS);
        const positionOriginTimeInSeconds = Math.floor(positionOriginUnixTimestamp / ONE_SECOND_IN_MILLIS);
        return Math.max(lastPositionDelay, positionOriginTimeInSeconds - departureTimeInSeconds);
    }

    /**
     * Compute UTC timestamp of start of day when trip starts
     *
     * @param {number} startTimestamp - Unix timestamp of start of the trip
     * @param {number} firstStopTimeScheduledSeconds - Number of seconds from midnight of first stop departure
     * @returns {number} - Returns unix timestamp in milliseconds.
     */
    private getStartDayTimestamp = (startTimestamp: number, firstStopTimeScheduledSeconds: number): number => {
        let startDayTimestamp = moment.utc(startTimestamp).tz("Europe/Prague").startOf("day");
        const stopTimeDayOverflow = Math.floor(firstStopTimeScheduledSeconds / ONE_DAY_IN_SECONDS);
        // if trip has 24+ stop times set real startDay to yesterday
        if (stopTimeDayOverflow > 0) {
            startDayTimestamp.subtract(1, "day");
        }
        return startDayTimestamp.valueOf();
    };

    private getStateAndStopSequences(
        gtfsRouteType: GTFSRouteTypeEnum,
        thisClosestPoint: IShapeAnchorPoint,
        positionProperties: ICurrentPositionProperties,
        context: Pick<IVPTripsLastPositionContext, "lastPositionTracking"> | null
    ): IPositionStateAndStopSequences {
        let statePosition = thisClosestPoint.this_stop_sequence ? StatePositionEnum.AT_STOP : StatePositionEnum.ON_TRACK;
        let thisStopSequence = thisClosestPoint.this_stop_sequence;
        let lastStopSequence = thisClosestPoint.last_stop_sequence;
        let nextStopSequence = thisClosestPoint.next_stop_sequence;

        // if current positions's TCP event is 'T' and previous position is tracked
        if (
            gtfsRouteType !== GTFSRouteTypeEnum.METRO &&
            positionProperties.tcp_event === TCPEventEnum.TIME &&
            context?.lastPositionTracking
        ) {
            const previousPositionTcpEvent = context.lastPositionTracking.properties.tcp_event;
            const previousLastStopSequence = context.lastPositionTracking.properties.last_stop_sequence ?? 0;

            // if previous position was 'P' and current last stop sequence is lower than the previous last stop sequence
            // or if previous position was 'O', current position is at stop
            //    and current last stop sequence is lower than the previous last stop sequence
            // then state is MISMATCHED
            // else if previous position was 'O', current position is at stop
            //    and current last stop sequence is equal to the previous last stop sequence
            // then state is ON_TRACK
            if (previousPositionTcpEvent === TCPEventEnum.ARRIVAL_ANNOUNCED && lastStopSequence < previousLastStopSequence) {
                statePosition = StatePositionEnum.MISMATCHED;
            } else if (previousPositionTcpEvent === TCPEventEnum.DEPARTURED && thisStopSequence) {
                if (thisStopSequence === previousLastStopSequence) {
                    statePosition = StatePositionEnum.ON_TRACK;
                } else if (thisStopSequence < previousLastStopSequence) {
                    statePosition = StatePositionEnum.MISMATCHED;
                }
            }

            if (statePosition === StatePositionEnum.MISMATCHED || statePosition === StatePositionEnum.ON_TRACK) {
                thisStopSequence = null;
            }
        }

        return {
            statePosition,
            thisStopSequence,
            lastStopSequence,
            nextStopSequence,
        };
    }

    private isAfterTrack(lastStopSequence: number, lastTripShapePoint: IShapeAnchorPoint): boolean {
        // Trip is terminated OR the vehicle is close to/in the terminus
        return lastStopSequence >= lastTripShapePoint.last_stop_sequence;
    }
}
