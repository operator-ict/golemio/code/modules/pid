import { VehicleIdGenerator } from "#ie/vehicle-positions/workers/gtfs-rt/helpers/VehicleIdGenerator";
import { IEnrichedTripForPublicApi } from "#ie/vehicle-positions/workers/vehicle-positions/data-access/interfaces/TripRepositoryInterfaces";
import { StatePositionEnum } from "src/const";

export class PublicCacheTripsFilter {
    public static getDistinctLatestTrips(trips: IEnrichedTripForPublicApi[]): {
        filteredTrips: IEnrichedTripForPublicApi[];
        futureTrips: IEnrichedTripForPublicApi[];
        canceledTrips: IEnrichedTripForPublicApi[];
    } {
        const filteredTrips: IEnrichedTripForPublicApi[] = [];
        const canceledTrips: IEnrichedTripForPublicApi[] = [];
        const futureTrips: IEnrichedTripForPublicApi[] = [];
        const tripsByVehicleId: Map<string, IEnrichedTripForPublicApi> = new Map();
        const futureTripsByTripAndVehicle: Map<string, IEnrichedTripForPublicApi> = new Map();
        const canceledTripsByTripId: Map<string, IEnrichedTripForPublicApi> = new Map();

        for (let i = 0; i < trips.length; i++) {
            const trip = trips[i];
            if (trip.state_position === StatePositionEnum.CANCELED) {
                canceledTripsByTripId.set(trip.gtfs_trip_id, trip);
                continue;
            }
            const vehicleId = VehicleIdGenerator.getVehicleId(
                trip.gtfs_trip_id,
                trip.gtfs_route_type,
                trip.provider_source_type,
                trip.gtfs_route_short_name,
                trip.cis_trip_number,
                trip.vehicle_registration_number,
                trip.run_number,
                trip.internal_run_number
            );

            const knownTrip = tripsByVehicleId.get(vehicleId);
            if (knownTrip) {
                if (trip.created_at < knownTrip.created_at) {
                    tripsByVehicleId.set(vehicleId, trip);
                } else if (
                    trip.state_position === StatePositionEnum.BEFORE_TRACK ||
                    trip.state_position === StatePositionEnum.BEFORE_TRACK_DELAYED
                ) {
                    futureTripsByTripAndVehicle.set(trip.gtfs_trip_id + vehicleId, trip);
                }
            } else {
                tripsByVehicleId.set(vehicleId, trip);
            }
        }

        for (const entity of tripsByVehicleId.values()) filteredTrips.push(entity);
        for (const entity of futureTripsByTripAndVehicle.values()) futureTrips.push(entity);
        for (const entity of canceledTripsByTripId.values()) canceledTrips.push(entity);

        return { filteredTrips, futureTrips, canceledTrips };
    }
}
