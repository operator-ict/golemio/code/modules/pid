import { RopidGTFSTripsModel } from "#ie/ropid-gtfs/RopidGTFSTripsModel";
import { IShape, IStopTime } from "#ie/ropid-gtfs/interfaces/TripModelInterfaces";
import { IComputationTripShape } from "#sch/ropid-gtfs/redis/interfaces/IDelayComputationDto";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { config } from "@golemio/core/dist/integration-engine/config";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import * as turf from "@turf/turf";
import { IComputationTrip, ISegmentShape, IShapeAnchorPoint, IShapeGeometryAnchorPoint } from "../interfaces/VPInterfaces";

export class DelayComputationManager {
    private METRO_TRIP_PREFIX_REGEX = /^(991|992|993)/;
    private stepBetweenPoints: number;
    private stepBetweenPointsforMetro: number;

    constructor(private readonly gtfsTripsRepository: RopidGTFSTripsModel) {
        const simpleConfig = IntegrationEngineContainer.resolve<ISimpleConfig>(CoreToken.SimpleConfig);
        this.stepBetweenPoints = config.vehiclePositions.stepBetweenPoints;
        this.stepBetweenPointsforMetro = Number(
            simpleConfig.getValue("module.pid.vehicle-positions.shapes.metro.stepBetweenPointsForMetro", "0.025")
        );
    }

    /**
     * Return shape anchor points along trip track shape with stop times parameters
     *
     * @param {string} tripId - Internal DB trip id
     * @returns {IComputationTrip} - Result with shape anchor points
     */
    public getComputationObject = async (tripId: string): Promise<IComputationTrip | null> => {
        const gtfsTrip = await this.gtfsTripsRepository.findByIdForDelayComputation(tripId);
        if (!gtfsTrip) {
            log.info(`getComputationObject: GTFS trip ${tripId} was not found`);
            return null;
        }

        const { shape_id, shapes, stop_times } = gtfsTrip;
        if (!shapes?.length) {
            throw new GeneralError(`getComputationObject: GTFS trip ${tripId} has no shapes`, this.constructor.name);
        } else if (!stop_times?.length) {
            throw new GeneralError(`getComputationObject: GTFS trip ${tripId} has no stop_times`, this.constructor.name);
        }
        const isMetro = this.METRO_TRIP_PREFIX_REGEX.test(tripId);
        const shapesGeometryAnchorPoints = this.getShapesGeometryAnchorPoints(shape_id, shapes, stop_times, isMetro);

        // MAKING COPY
        const shapePoints: IShapeAnchorPoint[] = [];

        // const to determine shape points of stops (in meters)
        const distanceBefore = 0.1;
        const distanceAfter = 0.05;

        for (let i = 0; i < shapesGeometryAnchorPoints.length; i++) {
            let bearing: number = 0;
            // add bearing from shape computed for previous shapePoint
            if (i > 0) {
                // compute bearing from two shape points
                let shapePointBearing = Math.round(
                    turf.bearing(
                        turf.point(shapesGeometryAnchorPoints[i - 1].coordinates),
                        turf.point(shapesGeometryAnchorPoints[i].coordinates)
                    )
                );
                // turf.bearing returns -180 to 180, when 0 is north
                // we need 0 to 359, for negative value we substract from 360
                if (shapePointBearing < 0) {
                    shapePointBearing = 360 - Math.abs(shapePointBearing);
                }
                // save bearing
                shapePoints[i - 1].bearing = shapePointBearing;
                // for the last shapePoint copy the bearing from last one
                if (shapesGeometryAnchorPoints.length - 1 === i) {
                    bearing = shapePoints[i - 1].bearing;
                }
            }

            // init values
            let thisStopSequence = null;
            let lastStopSequence = shapesGeometryAnchorPoints[i].last_stop_sequence;
            let nextStopSequence = shapesGeometryAnchorPoints[i].next_stop_sequence;
            // these are origin stop_times shape point fron shapes table, clearly at stops points
            if (shapesGeometryAnchorPoints[i].this_stop_sequence) {
                thisStopSequence = shapesGeometryAnchorPoints[i].this_stop_sequence;
            }
            // if is shape anchor point in close distance AFTER LAST stop, then set it also AT STOP
            else if (
                i > 0 &&
                shapesGeometryAnchorPoints[i].shape_dist_traveled -
                    stop_times[shapesGeometryAnchorPoints[i].last_stop_sequence - 1].shape_dist_traveled <=
                    distanceAfter
            ) {
                thisStopSequence = shapesGeometryAnchorPoints[i].last_stop_sequence;
            }
            // if is shape anchor point in close distance BEFORE NEXT stop, then set it also AT STOP
            else if (
                i < shapesGeometryAnchorPoints.length - 1 &&
                stop_times[shapesGeometryAnchorPoints[i].next_stop_sequence - 1].shape_dist_traveled -
                    shapesGeometryAnchorPoints[i].shape_dist_traveled <=
                    distanceBefore
            ) {
                thisStopSequence = shapesGeometryAnchorPoints[i].next_stop_sequence;
                lastStopSequence = shapesGeometryAnchorPoints[i].next_stop_sequence;
                nextStopSequence = Math.min(shapesGeometryAnchorPoints[i].next_stop_sequence + 1, stop_times.length);
            }

            let shapeDistTraveled = shapesGeometryAnchorPoints[i].shape_dist_traveled;
            let distanceFromLastStop: number;

            // fallback to shape_dist_traveled from GTFS stop_times, if the vehicle is at stop
            // and set distance from last stop to 0 (the stop is literally at stop)
            if (thisStopSequence !== null) {
                shapeDistTraveled = stop_times[thisStopSequence - 1].shape_dist_traveled;
                distanceFromLastStop = 0;
            } else {
                distanceFromLastStop =
                    Math.round((shapeDistTraveled - stop_times[lastStopSequence - 1].shape_dist_traveled) * 1000) / 1000;
            }

            // if is trip at stop than set departure by this stop
            // between stops do linear interpolation from departure from last stop and arrival of next stop
            const timeScheduledSeconds =
                thisStopSequence !== null
                    ? stop_times[lastStopSequence - 1].arrival_time_seconds
                    : stop_times[lastStopSequence - 1].departure_time_seconds +
                      Math.round(
                          ((stop_times[nextStopSequence - 1].arrival_time_seconds -
                              stop_times[lastStopSequence - 1].departure_time_seconds) *
                              distanceFromLastStop) /
                              (stop_times[nextStopSequence - 1].shape_dist_traveled -
                                  stop_times[lastStopSequence - 1].shape_dist_traveled)
                      );

            shapePoints.push({
                index: i,
                at_stop: thisStopSequence !== null,
                bearing,
                coordinates: shapesGeometryAnchorPoints[i].coordinates,
                distance_from_last_stop: distanceFromLastStop,
                last_stop_sequence: lastStopSequence,
                next_stop_sequence: nextStopSequence,
                shape_dist_traveled: shapeDistTraveled,
                this_stop_sequence: thisStopSequence,
                time_scheduled_seconds: timeScheduledSeconds,
            });
        }

        return {
            trip_id: tripId,
            stop_times: stop_times,
            shapes_anchor_points: shapePoints,
            shapes: this.mapGtfsShapesToTripShapes(shapes),
        };
    };

    /**
     * Return geometry of anchor points with distance from start along trip track shape
     *
     * @param {string} shapeId - GTFS shape id
     * @param {ITransformedTripShape[]} shapes - GTFS shape points
     * @param {IStopTime[]} stops
     * @returns {IShapeGeometryAnchorPoint[]} - Result array of geometry points along trip track shape
     */
    private getShapesGeometryAnchorPoints = (
        shapeId: string,
        shapes: IShape[],
        stops: IStopTime[],
        isMetro: boolean = false
    ): IShapeGeometryAnchorPoint[] => {
        try {
            let segmentShapes: ISegmentShape[][] = [];
            // Splits shapes array to multiple arrays "delimited" by stops
            for (let st = 0; st < stops.length; st++) {
                let currentSegment: ISegmentShape[] = [];
                for (let sh = 0; sh < shapes.length; sh++) {
                    let shapePoint: ISegmentShape = { ...shapes[sh], shape_at_stop: false };
                    if (stops[st].shape_dist_traveled === shapes[sh].shape_dist_traveled) {
                        shapePoint.shape_at_stop = true;
                    }
                    if (
                        st < stops.length - 1 &&
                        shapePoint.shape_dist_traveled >= stops[st].shape_dist_traveled &&
                        shapePoint.shape_dist_traveled <= stops[st + 1].shape_dist_traveled
                    ) {
                        currentSegment.push(shapePoint);
                    }
                }
                if (currentSegment.length) {
                    segmentShapes.push(currentSegment);
                }
            }

            let segmentArr: ISegmentShape[] = [];

            // concat arrays of stop divided segments with distributed shape points
            for (const segment of segmentShapes) {
                segmentArr = [
                    ...segmentArr,
                    ...this.divideSegmentLine(segment, isMetro ? this.stepBetweenPointsforMetro : this.stepBetweenPoints),
                ];
            }

            // Make initial shapesGeometryAnchorPoints
            let stopCounter = 0;

            const resultArr: IShapeGeometryAnchorPoint[] = segmentArr.map((segment: ISegmentShape, index: number) => {
                let this_stop_sequence: number | null;
                let last_stop_sequence: number;
                let next_stop_sequence: number;

                if (segment.shape_at_stop) {
                    stopCounter++;
                    this_stop_sequence = stopCounter;
                } else {
                    this_stop_sequence = null;
                }

                last_stop_sequence = stopCounter;
                next_stop_sequence = stopCounter + 1;

                return {
                    index,
                    coordinates: [
                        Math.round(segment.shape_pt_lon * 100000) / 100000,
                        Math.round(segment.shape_pt_lat * 100000) / 100000,
                    ],
                    last_stop_sequence,
                    next_stop_sequence,
                    shape_dist_traveled: Math.round(segment.shape_dist_traveled * 1000) / 1000,
                    this_stop_sequence,
                };
            });
            stopCounter++;
            // and lastly shape point for last stop as there is no segment for last stop
            resultArr.push({
                index: resultArr.length,
                coordinates: [
                    Math.round(shapes[shapes.length - 1].shape_pt_lon * 100000) / 100000,
                    Math.round(shapes[shapes.length - 1].shape_pt_lat * 100000) / 100000,
                ],
                last_stop_sequence: stopCounter,
                next_stop_sequence: stopCounter,
                shape_dist_traveled: Math.round(shapes[shapes.length - 1].shape_dist_traveled * 1000) / 1000,
                this_stop_sequence: stopCounter,
            });

            return resultArr;
        } catch (err) {
            log.error(err);
            log.error(shapeId);
            return [];
        }
    };

    /**
     * Generate new shape points from segment of trip shape along original one
     *   - preserving start and end shape points belonging to stops
     *   - creates new set of shape points between stops to get same point distance along original shape
     *
     * @param {ISegmentShape[]} segmentsArr - Original shape points segment between two stops
     * @param {number} distance - Minimal distance for two consequent shape points
     * @returns {ISegmentShape[]} - Result array of new shape points
     */
    private divideSegmentLine = (
        segmentArr: ISegmentShape[],
        distance: number = config.vehiclePositions.stepBetweenPoints
    ): ISegmentShape[] => {
        const shapeId = segmentArr[0].shape_id;
        const shapeCount = segmentArr.length;
        const startShapeDistTraveled = segmentArr[0].shape_dist_traveled;
        const endShapeDistTraveled = segmentArr[shapeCount - 1].shape_dist_traveled;
        const shapeDistLength = endShapeDistTraveled - startShapeDistTraveled;
        const segmentLine = turf.lineString(
            segmentArr.map((s) => {
                return [s.shape_pt_lon, s.shape_pt_lat];
            })
        );
        // because we dont believe source shape distances, rather compute length with turf.js
        const segmentLength = turf.length(segmentLine);
        const middlePointsCount = Math.floor(segmentLength / distance);
        const distanceOffset = (segmentLength - distance * middlePointsCount) / 2;
        // put first shape point
        const resultSegmentPoints: ISegmentShape[] = [
            {
                ...segmentArr[0],
                shape_pt_sequence: 1,
            },
        ];
        // for each middle point create acording shape point with interpolated origin shape dist traveled
        for (let i = 0; i < middlePointsCount; i++) {
            const currentDistance = i * distance + distanceOffset;
            const currentDistDistance = (shapeDistLength / segmentLength) * currentDistance;
            const newPoint = turf.along(segmentLine, i * distance + distanceOffset);
            resultSegmentPoints.push({
                shape_id: shapeId,
                shape_at_stop: false,
                shape_pt_sequence: resultSegmentPoints.length + 1,
                shape_pt_lat: newPoint.geometry.coordinates[1],
                shape_pt_lon: newPoint.geometry.coordinates[0],
                shape_dist_traveled: startShapeDistTraveled + currentDistDistance,
            });
        }
        return resultSegmentPoints;
    };

    private mapGtfsShapesToTripShapes = (shapes: IShape[]): IComputationTripShape[] => {
        return shapes.map((shape) => {
            return {
                dist: shape.shape_dist_traveled,
                coords: [shape.shape_pt_lon, shape.shape_pt_lat],
            };
        });
    };
}
