import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";
import AbstractDelayAtStop from "./AbstractDelayAtStop";
import CommonDelayAtStop from "./CommonDelayAtStop";
import MetroDelayAtStop from "./MetroDelayAtStop";

export default class FactoryDelayAtStop {
    private static _instance: FactoryDelayAtStop;
    private dictionary: Map<GTFSRouteTypeEnum, AbstractDelayAtStop>;

    public static getInstance() {
        if (!this._instance) {
            this._instance = new FactoryDelayAtStop();
        }

        return this._instance;
    }

    private constructor() {
        this.dictionary = new Map<GTFSRouteTypeEnum, AbstractDelayAtStop>();
        this.dictionary.set(GTFSRouteTypeEnum.TRAM, new CommonDelayAtStop());
        this.dictionary.set(GTFSRouteTypeEnum.BUS, new CommonDelayAtStop());
        this.dictionary.set(GTFSRouteTypeEnum.TROLLEYBUS, new CommonDelayAtStop());
        this.dictionary.set(GTFSRouteTypeEnum.METRO, new MetroDelayAtStop());
    }

    public getStrategy(gtfsRouteType: GTFSRouteTypeEnum): AbstractDelayAtStop | null {
        if (this.dictionary.has(gtfsRouteType)) {
            return this.dictionary.get(gtfsRouteType)!;
        }

        return null;
    }
}
