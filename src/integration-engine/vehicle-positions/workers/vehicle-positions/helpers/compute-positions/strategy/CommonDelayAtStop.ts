import { IPositionToUpdate } from "#ie/vehicle-positions/workers/vehicle-positions/interfaces/VPInterfaces";
import { IVPTripsPositionAttributes } from "#sch/vehicle-positions/models/interfaces/IVPTripsPositionAttributes";
import { IVPTripsLastPositionContext } from "#sch/vehicle-positions/models/interfaces/VPTripsLastPositionInterfaces";
import { StatePositionEnum, TCPEventEnum } from "src/const";
import AbstractDelayAtStop from "./AbstractDelayAtStop";

export default class CommonDelayAtStop extends AbstractDelayAtStop {
    public updateContext(
        context: IVPTripsLastPositionContext,
        positionToUpdate: IPositionToUpdate | null,
        position: IVPTripsPositionAttributes
    ): IVPTripsLastPositionContext {
        if (!positionToUpdate || !this.isValidPosition(position, positionToUpdate)) {
            return context;
        }

        context = this.resetContext(context, positionToUpdate, position);

        if (!context.lastPositionLastStop.sequence) {
            context.lastPositionLastStop.sequence = positionToUpdate?.last_stop_sequence ?? position?.last_stop_sequence ?? null;
        }

        if (this.didVehicleArrive(position) || this.isValidUntrackedPosition(positionToUpdate)) {
            context.lastPositionLastStop.arrival_delay = positionToUpdate?.delay_stop_arrival ?? context.lastPositionDelay;
        } else if (this.didVehicleDepart(position, context)) {
            context.lastPositionLastStop.departure_delay = positionToUpdate?.delay_stop_departure ?? context.lastPositionDelay;
        }

        return context;
    }

    public updatePosition(
        context: IVPTripsLastPositionContext,
        positionToUpdate: IPositionToUpdate,
        position: IVPTripsPositionAttributes | undefined
    ): IPositionToUpdate {
        if (!position || !this.isValidPosition(position, positionToUpdate)) {
            return positionToUpdate;
        }

        context = this.resetContext(context, positionToUpdate, position);

        if (this.didVehicleArrive(position) || this.didVehicleArriveAtTheLastStop(positionToUpdate, context)) {
            positionToUpdate.delay_stop_arrival = this.calcDelayAtStop(
                position.origin_timestamp.getTime(),
                positionToUpdate.last_stop_arrival_time
            );
        } else if (this.didVehicleDepart(position, context)) {
            positionToUpdate.delay_stop_departure = this.calcDelayAtStop(
                position.origin_timestamp.getTime(),
                positionToUpdate.last_stop_departure_time
            );
        }

        return this.updatePositionStopDelays(positionToUpdate, context);
    }

    private isValidPosition(position: IVPTripsPositionAttributes, positionToUpdate: IPositionToUpdate): boolean {
        return this.hasTcpEvent(position) || this.isValidUntrackedPosition(positionToUpdate);
    }

    private hasTcpEvent(position: IVPTripsPositionAttributes | undefined): position is IVPTripsPositionAttributes {
        return typeof position?.tcp_event === "string";
    }

    private didVehicleArrive(position: IVPTripsPositionAttributes) {
        return position.is_tracked && position.tcp_event === TCPEventEnum.ARRIVAL_ANNOUNCED;
    }

    private didVehicleDepart(position: IVPTripsPositionAttributes, context: IVPTripsLastPositionContext) {
        return (
            position.is_tracked &&
            position.tcp_event === TCPEventEnum.DEPARTURED &&
            context.lastPositionLastStop.departure_delay === null
        );
    }

    private updatePositionStopDelays(
        positionToUpdate: IPositionToUpdate,
        context: IVPTripsLastPositionContext
    ): IPositionToUpdate {
        const { delay_stop_arrival, delay_stop_departure } = positionToUpdate;

        if (
            (delay_stop_arrival === null || delay_stop_arrival === undefined) &&
            context.lastPositionLastStop.arrival_delay !== null
        ) {
            positionToUpdate.delay_stop_arrival = context.lastPositionLastStop.arrival_delay;
        }

        if (
            (delay_stop_departure === null || delay_stop_departure === undefined) &&
            context.lastPositionLastStop.departure_delay !== null
        ) {
            positionToUpdate.delay_stop_departure = context.lastPositionLastStop.departure_delay;
        }

        return positionToUpdate;
    }
}
