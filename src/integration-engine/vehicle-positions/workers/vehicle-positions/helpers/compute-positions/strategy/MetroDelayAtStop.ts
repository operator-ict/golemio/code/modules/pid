import { IPositionToUpdate } from "#ie/vehicle-positions/workers/vehicle-positions/interfaces/VPInterfaces";
import { IVPTripsPositionAttributes } from "#sch/vehicle-positions/models/interfaces/IVPTripsPositionAttributes";
import { IVPTripsLastPositionContext } from "#sch/vehicle-positions/models/interfaces/VPTripsLastPositionInterfaces";
import { config } from "@golemio/core/dist/integration-engine/config";
import { StatePositionEnum } from "src/const";
import AbstractDelayAtStop from "./AbstractDelayAtStop";

export default class MetroDelayAtStop extends AbstractDelayAtStop {
    private readonly metroArrivalDelayAddition: number;
    private readonly metroDepartureDelayAddition: number;

    constructor() {
        super();
        this.metroArrivalDelayAddition = config.vehiclePositions.metroArrivalDelayAddition;
        this.metroDepartureDelayAddition = config.vehiclePositions.metroDepartureDelayAddition;
    }

    public updateContext(
        context: IVPTripsLastPositionContext,
        positionToUpdate: IPositionToUpdate | null,
        position: IVPTripsPositionAttributes
    ): IVPTripsLastPositionContext {
        if (!positionToUpdate || (!position.is_tracked && !this.isValidUntrackedPosition(positionToUpdate))) {
            return context;
        }

        context = this.resetContext(context, positionToUpdate, position);

        if (!context.lastPositionLastStop.sequence) {
            context.lastPositionLastStop.sequence = positionToUpdate.last_stop_sequence ?? position.last_stop_sequence ?? null;
        }

        if (
            positionToUpdate.state_position === StatePositionEnum.AT_STOP ||
            positionToUpdate.state_position === StatePositionEnum.ON_TRACK ||
            this.isValidUntrackedPosition(positionToUpdate)
        ) {
            context.lastPositionLastStop.arrival_delay = positionToUpdate.delay_stop_arrival ?? null;
            context.lastPositionLastStop.departure_delay = positionToUpdate.delay_stop_departure ?? null;
        }

        return context;
    }

    public updatePosition(
        context: IVPTripsLastPositionContext,
        positionToUpdate: IPositionToUpdate,
        position: IVPTripsPositionAttributes | undefined
    ): IPositionToUpdate {
        if (!position || (!position.is_tracked && !this.isValidUntrackedPosition(positionToUpdate))) {
            return positionToUpdate;
        }

        context = this.resetContext(context, positionToUpdate, position);
        positionToUpdate.delay_stop_arrival = context.lastPositionLastStop.arrival_delay;
        positionToUpdate.delay_stop_departure = context.lastPositionLastStop.departure_delay;

        if (
            (positionToUpdate.state_position === StatePositionEnum.AT_STOP && context.atStopStreak.stop_sequence === null) ||
            this.didVehicleArriveAtTheLastStop(positionToUpdate, context)
        ) {
            if (positionToUpdate.last_stop_sequence === 1) {
                // first stop
                positionToUpdate.delay_stop_arrival = null;
            } else {
                const delayAtStopInSeconds = this.calcDelayAtStop(
                    position.origin_timestamp.getTime(),
                    positionToUpdate.last_stop_arrival_time
                );

                positionToUpdate.delay_stop_arrival = delayAtStopInSeconds
                    ? this.roundToNearestMultipleOfFive(delayAtStopInSeconds + this.metroArrivalDelayAddition)
                    : null;
            }
        } else if (positionToUpdate.state_position === StatePositionEnum.ON_TRACK && context.atStopStreak.stop_sequence) {
            const delayAtStopInSeconds = this.calcDelayAtStop(
                position.origin_timestamp.getTime(),
                positionToUpdate.last_stop_departure_time
            );

            positionToUpdate.delay_stop_departure = delayAtStopInSeconds
                ? this.roundToNearestMultipleOfFive(delayAtStopInSeconds + this.metroDepartureDelayAddition)
                : null;
        }

        return positionToUpdate;
    }

    // [-2,2] -> 0
    // [3,7] -> 5
    // [8,12] -> 10
    // [13,17] -> 15
    // etc.
    private roundToNearestMultipleOfFive(value: number): number {
        return Math.round(value / 5) * 5;
    }
}
