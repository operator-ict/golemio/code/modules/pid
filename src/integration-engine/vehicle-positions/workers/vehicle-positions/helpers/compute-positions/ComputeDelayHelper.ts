import { IPositionToUpdate } from "#ie/vehicle-positions/workers/vehicle-positions/interfaces/VPInterfaces";
import { IVPTripsPositionAttributes } from "#sch/vehicle-positions/models/interfaces/IVPTripsPositionAttributes";
import { IVPTripsLastPositionContext } from "#sch/vehicle-positions/models/interfaces/VPTripsLastPositionInterfaces";
import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";
import FactoryDelayAtStop from "./strategy/FactoryDelayAtStop";

export default class ComputeDelayHelper {
    /**
     * Saves relevant information to calculate delays in future vehicle position iterations.
     */
    public static updateContext(
        context: IVPTripsLastPositionContext,
        positionToUpdate: IPositionToUpdate | null,
        position: IVPTripsPositionAttributes,
        gtfsRouteType: GTFSRouteTypeEnum
    ) {
        context.lastPositionDelay = positionToUpdate ? positionToUpdate.delay! : position.delay;

        return (
            FactoryDelayAtStop.getInstance().getStrategy(gtfsRouteType)?.updateContext(context, positionToUpdate, position) ??
            context
        );
    }

    /**
     * Adds info about delay at stop to positionToUpdate based on current position and context from previous positions.
     */
    public static updatePositionToUpdate(
        context: IVPTripsLastPositionContext,
        positionToUpdate: IPositionToUpdate,
        position: IVPTripsPositionAttributes,
        gtfsRouteType: GTFSRouteTypeEnum
    ) {
        return (
            FactoryDelayAtStop.getInstance().getStrategy(gtfsRouteType)?.updatePosition(context, positionToUpdate, position) ??
            positionToUpdate
        );
    }
}
