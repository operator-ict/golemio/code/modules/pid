import { IGeoMeasurementHelper } from "#helpers/geo/interfaces/IGeoMeasurementHelper";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { IVPTripsPositionAttributes } from "#sch/vehicle-positions/models/interfaces/IVPTripsPositionAttributes";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { StatePositionEnum } from "src/const";
import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";
import { IComputationTrip, IPositionToUpdate } from "../interfaces/VPInterfaces";

@injectable()
export class ValidToCalculator {
    private readonly defaultAdditionInMs: number;
    private readonly canceledAdditionInMs: number;

    constructor(
        @inject(CoreToken.SimpleConfig) simpleConfig: ISimpleConfig,
        @inject(ModuleContainerToken.GeoMeasurementHelper) private geoHelper: IGeoMeasurementHelper
    ) {
        const defaultAdditionInMinutes = Number.parseInt(
            simpleConfig.getValue("module.pid.vehicle-positions.validTo.defaultAdditionInMinutes", "5")
        ); // fallback to 5 minutes

        const canceledAdditionInMinutes = Number.parseInt(
            simpleConfig.getValue("module.pid.vehicle-positions.validTo.canceledAdditionInMinutes", (6 * 60).toString())
        ); // fallback to 6 hours

        this.defaultAdditionInMs = defaultAdditionInMinutes * 60 * 1000;
        this.canceledAdditionInMs = canceledAdditionInMinutes * 60 * 1000;
    }

    /**
     * Calculates valid_to attribute value for updateDelay worker
     */
    public getValidToAttribute = (
        positionToUpdate: IPositionToUpdate,
        tripPositionAttributes: IVPTripsPositionAttributes,
        gtfsRouteType: GTFSRouteTypeEnum,
        gtfsData: IComputationTrip,
        startTimestamp: number
    ): Date => {
        if (positionToUpdate.state_position === StatePositionEnum.CANCELED) {
            return this.getCanceledValidToAttribute(tripPositionAttributes.origin_timestamp);
        } else if (gtfsRouteType === GTFSRouteTypeEnum.TRAM) {
            // For TRAM only:
            // For all real stops:
            // return default valid to attribute + the time difference between departure and arrival

            if (
                positionToUpdate.state_position == StatePositionEnum.AT_STOP &&
                positionToUpdate.last_stop_arrival_time != positionToUpdate.last_stop_departure_time &&
                positionToUpdate.last_stop_arrival_time &&
                positionToUpdate.last_stop_departure_time
            ) {
                return new Date(
                    this.getDefaultValidToAttribute(tripPositionAttributes.origin_timestamp).getTime() +
                        (positionToUpdate.last_stop_departure_time.getTime() - positionToUpdate.last_stop_arrival_time.getTime())
                );
            }

            // For final positions:
            // return default valid to attribute + the time difference between (next trip departure) and arrival

            if (positionToUpdate.state_position == StatePositionEnum.BEFORE_TRACK) {
                const distance = this.geoHelper.getDistanceInKilometers(
                    [tripPositionAttributes.lng, tripPositionAttributes.lat],
                    [gtfsData.stop_times[0].stop.stop_lon, gtfsData.stop_times[0].stop.stop_lat]
                );

                if (distance <= 1) {
                    return this.getDefaultValidToAttribute(
                        new Date(Math.max(tripPositionAttributes.origin_timestamp.getTime(), startTimestamp))
                    );
                }
            }
        }

        // For all other positions:
        return this.getDefaultValidToAttribute(tripPositionAttributes.origin_timestamp);
    };

    /**
     * Calculate default valid_to attribute
     * return 5 minutes + `origin_timestamp`
     */
    public getDefaultValidToAttribute = (origin_timestamp: Date): Date => {
        return new Date(origin_timestamp.getTime() + this.defaultAdditionInMs);
    };

    /**
     * Calculate canceled valid_to attribute
     * return 6 hours + `origin_timestamp`
     */
    private getCanceledValidToAttribute = (origin_timestamp: Date): Date => {
        return new Date(origin_timestamp.getTime() + this.canceledAdditionInMs);
    };
}
