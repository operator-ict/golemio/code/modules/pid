import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { IGtfsTripDataFixerFactory } from "../../domain/service/IGtfsTripDataFixerFactory";
import { ProviderSourceTypeEnum } from "../ProviderSourceTypeEnum";
import { AbstractGtfsTripDataFixer } from "./strategy/AbstractGtfsTripDataFixer";

@injectable()
export class GtfsTripDataFixerFactory implements IGtfsTripDataFixerFactory {
    private dictionary: Map<ProviderSourceTypeEnum, AbstractGtfsTripDataFixer>;

    constructor(@inject(VPContainerToken.HttpGtfsTripDataFixer) httpGtfsTripDataFixer: AbstractGtfsTripDataFixer) {
        this.dictionary = new Map<ProviderSourceTypeEnum, AbstractGtfsTripDataFixer>();
        this.dictionary.set(ProviderSourceTypeEnum.Http, httpGtfsTripDataFixer);
    }

    public getStrategy(providerSourceType: ProviderSourceTypeEnum): AbstractGtfsTripDataFixer {
        if (this.dictionary.has(providerSourceType)) {
            return this.dictionary.get(providerSourceType)!;
        }

        throw new GeneralError(`Unknown provider source type ${providerSourceType}`, this.constructor.name);
    }
}
