import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { QueueManager } from "@golemio/core/dist/integration-engine/queueprocessors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { TripsRepository } from "../../../data-access/TripsRepository";
import { IUpdateGTFSTripIdData } from "../../../data-access/interfaces/TripRepositoryInterfaces";
import { IUpdateGtfsTripIdInput } from "../../../interfaces/IUpdateGtfsTripIdInput";
import { ProviderSourceTypeEnum } from "../../ProviderSourceTypeEnum";
import { AbstractGtfsTripDataFixer } from "./AbstractGtfsTripDataFixer";

@injectable()
export class HttpGtfsTripDataFixer extends AbstractGtfsTripDataFixer {
    constructor(@inject(VPContainerToken.TripRepository) private tripRepository: TripsRepository) {
        super();
    }

    public async fix(queuePrefix: string): Promise<number> {
        const trips = await this.tripRepository.getTripsWithInvalidGtfsTripId(ProviderSourceTypeEnum.Http);
        if (trips.length === 0) {
            return 0;
        }

        QueueManager.sendMessageToExchange(queuePrefix, "updateGTFSTripId", this.mapTripsToQueueMessage(trips));
        return trips.length;
    }

    private mapTripsToQueueMessage(trips: IUpdateGTFSTripIdData[]): IUpdateGtfsTripIdInput {
        return {
            trips,
            positions: [],
        };
    }
}
