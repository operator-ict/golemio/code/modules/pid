export abstract class AbstractGtfsTripDataFixer {
    abstract fix(queuePrefix: string): Promise<number>;
}
