import { IExtendedMetroRunInputForProcessing } from "#ie/vehicle-positions/workers/runs/interfaces/IMetroRunInputForProcessing";
import { IScheduleDto } from "#sch/vehicle-positions/redis/interfaces/IGtfsRunTripCacheDto";
import moment, { Moment } from "@golemio/core/dist/shared/moment-timezone";
import { AbstractGTFSTripRunManager } from "./AbstractGTFSTripRunManager";
import { IDelayMessage } from "./interfaces/IDelayMessage";

export class GTFSTripMetroRunManager extends AbstractGTFSTripRunManager {
    protected CURRENT_TRIP_START_TIME_WINDOW_IN_MS = 4 * 60 * 60 * 1000; // 4 hours

    public async generateDelayMsg(schedule: IScheduleDto[], input: IExtendedMetroRunInputForProcessing): Promise<IDelayMessage> {
        const timestampScheduled = moment(input.timestampScheduled);
        const currentTrip = this.determineCurrentTripFromTripId(schedule, input.tripId);
        let delayMsg: IDelayMessage = {
            updatedTrips: [],
            positions: [],
            schedule,
        };

        if (currentTrip?.trip_id) {
            delayMsg = await this.generateDelayMsgFromCurrentTrip(delayMsg, currentTrip, input);
        } else {
            this.logger.verbose(`generateDelayMsg: could not find current gtfsTrip for metro run: ${JSON.stringify(input)}`);
            return delayMsg;
        }

        // last/next gtfs trip for run
        return this.updateLastTripsAndPositions(schedule, timestampScheduled, currentTrip?.trip_id, input, delayMsg);
    }

    protected getAdjacentTripTimeWindow(): ITimeWindow {
        return {
            startTimeWindowInMinutes: this.config.getValue<number>(
                "old.datasources.pid.vehicle-positions.adjacentTrips.metro.startTimeWindowInMinutes",
                15
            ) as number,
            endTimeWindowInMinutes: this.config.getValue<number>(
                "old.datasources.pid.vehicle-positions.adjacentTrips.metro.endTimeWindowInMinutes",
                2
            ) as number,
        };
    }

    private async generateDelayMsgFromCurrentTrip(
        delayMsg: IDelayMessage,
        gtfsCurrentTrip: IScheduleDto,
        input: IExtendedMetroRunInputForProcessing
    ) {
        const isCurrentTripTracking = true;
        const { trip, position } = await this.updateTrip(input, gtfsCurrentTrip, isCurrentTripTracking);

        delayMsg.updatedTrips.push(trip);
        if (this.testMode) delayMsg.positions.push(position);

        return delayMsg;
    }

    private async updateLastTripsAndPositions(
        schedule: IScheduleDto[],
        timestampScheduled: Moment,
        currentTripId: string | undefined,
        input: IExtendedMetroRunInputForProcessing,
        delayMsg: IDelayMessage
    ): Promise<IDelayMessage> {
        const gtfsAdjacentTrips = this.getAdjacentTrips(schedule, timestampScheduled, currentTripId);

        for (const gtfsTrip of gtfsAdjacentTrips) {
            const { trip, position } = await this.updateTrip(input, gtfsTrip, false);

            delayMsg.updatedTrips.push(trip);

            if (this.testMode) {
                delayMsg.positions.push(position);
            }
        }

        return delayMsg;
    }

    private async updateTrip(input: IExtendedMetroRunInputForProcessing, gtfsTrip: IScheduleDto, isTripTracking: boolean) {
        const trip = await this.tripsRepository.upsertMetroRunTrip(input, gtfsTrip);
        const position = await this.positionsRepository.upsertMetroRunPosition(input, gtfsTrip, isTripTracking);

        return { trip, position };
    }

    protected determineCurrentTripFromTripId(schedule: IScheduleDto[], gtfsTripId: string | undefined): IScheduleDto | undefined {
        let currentTrip: IScheduleDto | undefined = undefined;

        if (!gtfsTripId) {
            return currentTrip;
        }

        for (const tripCache of schedule) {
            if (tripCache.trip_id !== gtfsTripId) {
                continue;
            }
            const currTime = new Date().getTime();
            if (
                new Date(tripCache.start_timestamp).getTime() - this.CURRENT_TRIP_START_TIME_WINDOW_IN_MS < currTime &&
                new Date(tripCache.end_timestamp).getTime() + this.CURRENT_TRIP_START_TIME_WINDOW_IN_MS > currTime
            ) {
                currentTrip = tripCache;
                break;
            }
        }

        return currentTrip;
    }
}
