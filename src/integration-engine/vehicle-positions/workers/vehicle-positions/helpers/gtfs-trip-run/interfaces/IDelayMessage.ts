import { IScheduleDto } from "#sch/vehicle-positions/redis/interfaces/IGtfsRunTripCacheDto";
import { IUpdateDelayRunTripsData } from "../../../data-access/interfaces/TripRepositoryInterfaces";

export interface IDelayMessage {
    updatedTrips: IUpdateDelayRunTripsData[];
    positions: any[];
    schedule?: IScheduleDto[];
}
