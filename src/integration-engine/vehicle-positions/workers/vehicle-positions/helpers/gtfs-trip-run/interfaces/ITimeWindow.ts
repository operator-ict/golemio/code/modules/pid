interface ITimeWindow {
    startTimeWindowInMinutes: number;
    endTimeWindowInMinutes: number;
}
