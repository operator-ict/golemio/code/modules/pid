import { AbstractGTFSTripRunManager } from "./AbstractGTFSTripRunManager";
import { GTFSTripCommonRunManager } from "./GTFSTripCommonRunManager";
import { GTFSTripMetroRunManager } from "./GTFSTripMetroRunManager";

export enum GTFSTripRunType {
    Common,
    Metro,
}

export class GTFSTripRunManagerFactory {
    public static create(
        runType: GTFSTripRunType,
        ...params: ConstructorParameters<typeof AbstractGTFSTripRunManager>
    ): AbstractGTFSTripRunManager {
        switch (runType) {
            case GTFSTripRunType.Common:
            default:
                return new GTFSTripCommonRunManager(...params);

            case GTFSTripRunType.Metro:
                return new GTFSTripMetroRunManager(...params);
        }
    }
}
