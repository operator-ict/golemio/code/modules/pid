import { IScheduleDto } from "#sch/vehicle-positions/redis/interfaces/IGtfsRunTripCacheDto";
import moment, { Moment } from "@golemio/core/dist/shared/moment-timezone";
import { TCPEventEnum } from "src/const";
import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";
import { IUpdateRunsGtfsTripInput } from "../../interfaces/IUpdateRunsGtfsTripInput";
import { AbstractGTFSTripRunManager } from "./AbstractGTFSTripRunManager";
import { IDelayMessage } from "./interfaces/IDelayMessage";

export class GTFSTripCommonRunManager extends AbstractGTFSTripRunManager {
    public async generateDelayMsg(schedule: IScheduleDto[], input: IUpdateRunsGtfsTripInput) {
        const timestampScheduled = moment(input.run_message.actual_stop_timestamp_scheduled);
        const currentTrip = this.determineCurrentTripFromTimestamp(schedule, timestampScheduled);
        let delayMsg: IDelayMessage = {
            updatedTrips: [],
            positions: [],
            schedule,
        };

        if (currentTrip?.trip_id) {
            delayMsg = await this.generateDelayMsgFromCurrentTrip(delayMsg, currentTrip, timestampScheduled, input);
        } else {
            this.logger.verbose(`generateDelayMsg: could not find current gtfsTrip for common run: ${JSON.stringify(input)}`);
        }

        // last/next gtfs trip for run
        return this.updateLastTripsAndPositions(schedule, timestampScheduled, currentTrip?.trip_id, input, delayMsg);
    }

    protected getAdjacentTripTimeWindow(): ITimeWindow {
        return {
            startTimeWindowInMinutes: this.config.getValue<number>(
                "old.datasources.pid.vehicle-positions.adjacentTrips.common.startTimeWindowInMinutes",
                60
            ) as number,
            endTimeWindowInMinutes: this.config.getValue<number>(
                "old.datasources.pid.vehicle-positions.adjacentTrips.common.endTimeWindowInMinutes",
                15
            ) as number,
        };
    }

    private async generateDelayMsgFromCurrentTrip(
        delayMsg: IDelayMessage,
        gtfsCurrentTrip: IScheduleDto,
        timestampScheduled: Moment,
        input: IUpdateRunsGtfsTripInput
    ) {
        const event = input.run_message.events;
        const isCurrentTripTracking = this.isCurrentTripTracking(gtfsCurrentTrip, event, timestampScheduled);
        const { trip, position } = await this.updateTrip(input, gtfsCurrentTrip, isCurrentTripTracking);

        delayMsg.updatedTrips.push(trip);
        if (this.testMode) delayMsg.positions.push(position);

        return delayMsg;
    }

    private isCurrentTripTracking(gtfsCurrentTrip: IScheduleDto, event: TCPEventEnum, timestampScheduled: Moment): boolean {
        return (
            // tram is always on track while pushes messages only on stops
            (gtfsCurrentTrip.route_type === GTFSRouteTypeEnum.TRAM ||
                // when bus announces DEPARTURE TCP event, it means that trip is on track
                ((gtfsCurrentTrip.route_type === GTFSRouteTypeEnum.BUS ||
                    gtfsCurrentTrip.route_type === GTFSRouteTypeEnum.TROLLEYBUS) &&
                    event === TCPEventEnum.DEPARTURED) ||
                // of course when trip announcing some next stop time in schedule, then set as tracking
                timestampScheduled.isSameOrAfter(moment(gtfsCurrentTrip.start_timestamp))) ??
            false
        );
    }

    private async updateLastTripsAndPositions(
        schedule: IScheduleDto[],
        timestampScheduled: Moment,
        currentTripId: string | undefined,
        input: IUpdateRunsGtfsTripInput,
        delayMsg: IDelayMessage
    ) {
        const gtfsAdjacentTrips = this.getAdjacentTrips(schedule, timestampScheduled, currentTripId);

        for (const gtfsTrip of gtfsAdjacentTrips) {
            const { trip, position } = await this.updateTrip(input, gtfsTrip, false);

            delayMsg.updatedTrips.push(trip);
            if (this.testMode) delayMsg.positions.push(position);
        }

        return delayMsg;
    }

    private async updateTrip(input: IUpdateRunsGtfsTripInput, gtfsTrip: IScheduleDto, isTripTracking: boolean) {
        const trip = await this.tripsRepository.upsertCommonRunTrip(input, gtfsTrip);
        const position = await this.positionsRepository.upsertCommonRunPosition(input, gtfsTrip, isTripTracking);

        return { trip, position };
    }
}
