import { IRegionalBusRunInputWithMetadata } from "#ie/vehicle-positions/workers/runs/interfaces/IRegionalBusRunInputWithMetadata";
import { IScheduleDto } from "#sch/vehicle-positions/redis/interfaces/IGtfsRunTripCacheDto";
import moment, { Moment } from "@golemio/core/dist/shared/moment-timezone";
import PositionsMapper from "../../../data-access/helpers/PositionsMapper";
import TripsMapper from "../../../data-access/helpers/TripsMapper";
import { AbstractGTFSTripRunManager } from "../../gtfs-trip-run/AbstractGTFSTripRunManager";
import { IDelayMessage } from "../../gtfs-trip-run/interfaces/IDelayMessage";
import { PositionsRepository } from "../../../data-access/PositionsRepository";
import { TripsRepository } from "../../../data-access/TripsRepository";
import { CommonRunsRepository } from "#ie/vehicle-positions/workers/runs/data-access/CommonRunsRepository";
import { RunTripsRedisRepository } from "#ie/ropid-gtfs/data-access/cache";

export class GtfsTripRegionalBusRunManager extends AbstractGTFSTripRunManager {
    constructor(isTestMode = false) {
        const positionRepository = new PositionsRepository();
        const tripRepository = new TripsRepository();
        const runRepository = new CommonRunsRepository();
        const runTripsRedisRepository = new RunTripsRedisRepository();

        super(positionRepository, tripRepository, runRepository, runTripsRedisRepository, isTestMode);
    }

    public async generateDelayMsg(schedule: IScheduleDto[], input: IRegionalBusRunInputWithMetadata) {
        const timestampScheduled = moment(input.runMessage.vehicle_timestamp);
        const currentTrip = this.determineCurrentTripFromTripId(schedule, input.gtfsTripId);
        let delayMsg: IDelayMessage = {
            updatedTrips: [],
            positions: [],
            schedule,
        };

        if (currentTrip?.trip_id) {
            delayMsg = await this.generateDelayMsgFromCurrentTrip(delayMsg, currentTrip, input);
        } else {
            this.logger.verbose(`generateDelayMsg: could not find current gtfsTrip for metro run: ${JSON.stringify(input)}`);
        }

        // last/next gtfs trip for run
        return this.updateLastTripsAndPositions(schedule, timestampScheduled, currentTrip?.trip_id, input, delayMsg);
    }

    protected getAdjacentTripTimeWindow(): ITimeWindow {
        return {
            startTimeWindowInMinutes: this.config.getValue<number>(
                "old.datasources.pid.vehicle-positions.adjacentTrips.regionalBus.startTimeWindowInMinutes",
                60
            ) as number,
            endTimeWindowInMinutes: this.config.getValue<number>(
                "old.datasources.pid.vehicle-positions.adjacentTrips.regionalBus.endTimeWindowInMinutes",
                15
            ) as number,
        };
    }

    private async generateDelayMsgFromCurrentTrip(
        delayMsg: IDelayMessage,
        gtfsCurrentTrip: IScheduleDto,
        input: IRegionalBusRunInputWithMetadata
    ) {
        const isCurrentTripTracking = true;
        const { trip } = await this.updateTripAndPosition(input, gtfsCurrentTrip, isCurrentTripTracking);

        delayMsg.updatedTrips.push(trip);
        return delayMsg;
    }

    private async updateLastTripsAndPositions(
        schedule: IScheduleDto[],
        timestampScheduled: Moment,
        currentTripId: string | undefined,
        input: IRegionalBusRunInputWithMetadata,
        delayMsg: IDelayMessage
    ) {
        const gtfsAdjacentTrips = this.getAdjacentTrips(schedule, timestampScheduled, currentTripId);

        for (const gtfsTrip of gtfsAdjacentTrips) {
            const { trip } = await this.updateTripAndPosition(input, gtfsTrip, false);

            delayMsg.updatedTrips.push(trip);
        }

        return delayMsg;
    }

    private async updateTripAndPosition(
        input: IRegionalBusRunInputWithMetadata,
        gtfsTrip: IScheduleDto,
        isTripTracking: boolean
    ) {
        //#region position
        const positionDto = PositionsMapper.mapRegionalBusRunToDto(isTripTracking, gtfsTrip, input.runMessage);
        await this.positionsRepository.createEntity(positionDto);
        //#endregion

        //#region trip
        const tripDto = TripsMapper.mapRegionalBusRunToDto(gtfsTrip, input);
        const trip = await this.tripsRepository.upsertEntity(tripDto);
        //#endregion

        return { trip };
    }
}
