import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";
import AbstractDelayAtStop from "../../../compute-positions/strategy/AbstractDelayAtStop";
import { BusDelayAtStop } from "./BusDelayAtStop";

export default class FactoryDelayAtStop {
    private static _instance: FactoryDelayAtStop;
    private dictionary: Map<GTFSRouteTypeEnum, AbstractDelayAtStop>;

    public static getInstance() {
        if (!this._instance) {
            this._instance = new FactoryDelayAtStop();
        }

        return this._instance;
    }

    private constructor() {
        this.dictionary = new Map<GTFSRouteTypeEnum, AbstractDelayAtStop>();
        this.dictionary.set(GTFSRouteTypeEnum.BUS, new BusDelayAtStop());
    }

    public getStrategy(gtfsRouteType: GTFSRouteTypeEnum): AbstractDelayAtStop | null {
        if (this.dictionary.has(gtfsRouteType)) {
            return this.dictionary.get(gtfsRouteType)!;
        }

        return null;
    }
}
