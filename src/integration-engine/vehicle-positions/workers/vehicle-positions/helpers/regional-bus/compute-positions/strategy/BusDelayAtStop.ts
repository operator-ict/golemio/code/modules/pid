import { IPositionToUpdate } from "#ie/vehicle-positions/workers/vehicle-positions/interfaces/VPInterfaces";
import { IVPTripsPositionAttributes } from "#sch/vehicle-positions/models/interfaces/IVPTripsPositionAttributes";
import { IVPTripsLastPositionContext } from "#sch/vehicle-positions/models/interfaces/VPTripsLastPositionInterfaces";
import { RegionalBusEventEnum, StatePositionEnum } from "src/const";
import AbstractDelayAtStop from "../../../compute-positions/strategy/AbstractDelayAtStop";

export class BusDelayAtStop extends AbstractDelayAtStop {
    public updateContext(
        context: IVPTripsLastPositionContext,
        positionToUpdate: IPositionToUpdate | null,
        position: IVPTripsPositionAttributes
    ): IVPTripsLastPositionContext {
        if (!positionToUpdate || (!position.is_tracked && !this.isValidUntrackedPosition(positionToUpdate))) {
            return context;
        }

        context = this.resetContext(context, positionToUpdate, position);

        if (!context.lastPositionLastStop.sequence) {
            context.lastPositionLastStop.sequence = positionToUpdate.last_stop_sequence ?? position.last_stop_sequence ?? null;
        }

        if (
            positionToUpdate.state_position === StatePositionEnum.AT_STOP ||
            positionToUpdate.state_position === StatePositionEnum.ON_TRACK ||
            this.isValidUntrackedPosition(positionToUpdate)
        ) {
            context.lastPositionLastStop.arrival_delay = positionToUpdate.delay_stop_arrival ?? null;
            context.lastPositionLastStop.departure_delay = positionToUpdate.delay_stop_departure ?? null;
        }

        return context;
    }

    public updatePosition(
        context: IVPTripsLastPositionContext,
        positionToUpdate: IPositionToUpdate,
        position: IVPTripsPositionAttributes | undefined
    ): IPositionToUpdate {
        if (!position || (!position.is_tracked && !this.isValidUntrackedPosition(positionToUpdate))) {
            return positionToUpdate;
        }

        context = this.resetContext(context, positionToUpdate, position);
        positionToUpdate.delay_stop_arrival = context.lastPositionLastStop.arrival_delay;
        positionToUpdate.delay_stop_departure = context.lastPositionLastStop.departure_delay;

        if (this.didVehicleArriveAtTheLastStop(positionToUpdate, context)) {
            positionToUpdate.delay_stop_arrival = this.calcDelayAtStop(
                position.origin_timestamp.getTime(),
                positionToUpdate.last_stop_arrival_time
            );
        } else if (positionToUpdate.state_position === StatePositionEnum.AT_STOP) {
            const delayInSeconds = positionToUpdate.delay ?? position.delay ?? null;

            if (context.atStopStreak.stop_sequence === null) {
                positionToUpdate.delay_stop_arrival = delayInSeconds;
            } else if (this.didVehicleDepart(position)) {
                positionToUpdate.delay_stop_departure = delayInSeconds;
            }
        }

        return positionToUpdate;
    }

    private didVehicleDepart(position: IVPTripsPositionAttributes) {
        return position.is_tracked && position.tcp_event === RegionalBusEventEnum.DEPARTURED;
    }
}
