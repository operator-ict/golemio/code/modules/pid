import { RopidGTFSStopTimesModel } from "#ie/ropid-gtfs/RopidGTFSStopTimesModel";
import { BlockStopsRedisRepository } from "#ie/ropid-gtfs/data-access/cache";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { VPUtils } from "../helpers/VPUtils";
import { TripStopsSchedule } from "../interfaces/VPInterfaces";

export class TripStopsManager {
    constructor(
        private readonly gtfsStopTimesRepository: RopidGTFSStopTimesModel,
        private readonly blockStopsRedisRepository: BlockStopsRedisRepository
    ) {}

    /**
     * Get trip stops schedule by block id
     *
     * @param {string} blockId - Target gtfs block id
     * @param {string} tripIds - List of inserted gtfs trip ids
     */
    public setAndReturnTripStopsSchedule = async (blockId: string, tripIds: string[]): Promise<TripStopsSchedule> => {
        const schedule: TripStopsSchedule = await this.blockStopsRedisRepository.get(blockId);
        if (schedule) {
            return schedule;
        }

        try {
            const tripStops = await this.gtfsStopTimesRepository.findTripStops(tripIds);
            const tripStopsSchedule = tripStops.reduce((acc, curr) => {
                acc[curr.trip_id] = { trip_stops: curr.trip_stops };
                return acc;
            }, {} as TripStopsSchedule);

            await this.blockStopsRedisRepository.set(blockId, tripStopsSchedule, undefined, VPUtils.getNextExpireTimestamp());
            return tripStopsSchedule;
        } catch (err) {
            log.warn("Unable to retrieve trip stops", err);
            return {};
        }
    };
}
