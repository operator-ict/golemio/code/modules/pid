import { ICommonRunsMessagesModel } from "#sch/vehicle-positions/models/interfaces/ICommonRunsMessagesModel";
import { ICommonRunsModel } from "#sch/vehicle-positions/models/interfaces/ICommonRunsModel";

export interface IRunMessageWithStringTimestamps
    extends Omit<ICommonRunsMessagesModel, "actual_stop_timestamp_real" | "actual_stop_timestamp_scheduled"> {
    actual_stop_timestamp_real: string;
    actual_stop_timestamp_scheduled?: string;
}

export interface IUpdateRunsGtfsTripInput {
    run: ICommonRunsModel;
    run_message: IRunMessageWithStringTimestamps;
}
