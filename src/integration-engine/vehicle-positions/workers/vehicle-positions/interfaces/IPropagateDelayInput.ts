import { IUpdateDelayRunTripsData, IUpdateDelayTripsIdsData } from "../data-access/interfaces/TripRepositoryInterfaces";
import { IProcessedPositions } from "./VPInterfaces";

export interface IPropagateDelayInput {
    processedPositions: IProcessedPositions[];
    trips: Array<IUpdateDelayTripsIdsData | IUpdateDelayRunTripsData>;
}
