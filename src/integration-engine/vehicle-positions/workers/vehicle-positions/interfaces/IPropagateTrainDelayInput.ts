import { ITripPositionTuple } from "./ITripPositionTuple";

export interface IPropagateTrainDelayInput {
    data: ITripPositionTuple[];
}
