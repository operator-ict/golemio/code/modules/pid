import { IUpdateGTFSTripIdData } from "../data-access/interfaces/TripRepositoryInterfaces";
import { IPositionTransformationResult } from "../interfaces/TransformationInterfaces";

export interface IUpdateGtfsTripIdInput {
    trips: IUpdateGTFSTripIdData[];
    positions: IPositionTransformationResult[];
}
