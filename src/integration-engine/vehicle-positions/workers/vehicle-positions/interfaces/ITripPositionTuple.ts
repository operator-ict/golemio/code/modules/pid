import { IUpdateDelayRunTripsData } from "../data-access/interfaces/TripRepositoryInterfaces";

export interface ITripPositionTuple {
    position: {
        delayInSeconds: number;
        originTimestamp: Date;
        bearing: number;
        originId: string | null;
    };
    trip: IUpdateDelayRunTripsData;
}
