import { ITripStopsResult } from "#ie/ropid-gtfs/RopidGTFSStopTimesModel";
import { IShape, IStopTime } from "#ie/ropid-gtfs/interfaces/TripModelInterfaces";
import { IComputationTripShape } from "#sch/ropid-gtfs/redis/interfaces/IDelayComputationDto";
import { IVPTripsPositionAttributes } from "#sch/vehicle-positions/models/interfaces/IVPTripsPositionAttributes";
import {
    IVPTripsComputedPositionAtStopStreak,
    IVPTripsLastPositionContext,
} from "#sch/vehicle-positions/models/interfaces/VPTripsLastPositionInterfaces";
import { RegionalBusEventEnum, TCPEventEnum } from "src/const";
import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";

type PositionToUpdateRequiredKeys = "state_process" | "state_position" | "id";
type PositionToUpdateExtend = {
    gtfsData?: IComputationTrip;
    last_stop_headsign?: string | null;
    this_stop_name?: string;
    delay_stop_arrival?: number | null;
    delay_stop_departure?: number | null;
};
type PositionToUpdateAux = Pick<IVPTripsPositionAttributes, PositionToUpdateRequiredKeys> &
    Partial<Omit<IVPTripsPositionAttributes, PositionToUpdateRequiredKeys>> &
    PositionToUpdateExtend;
export interface IPositionToUpdate extends PositionToUpdateAux {}

export interface IProcessedPositions {
    context: IVPTripsLastPositionContext;
    positions: Array<IPositionToUpdate | null>;
    isBacktrackingDetected?: boolean;
}

export interface IUpdatePositionsIteratorOptions {
    tripPositions: ITripPositionsWithGTFS;
    startTimestamp: number;
    endTimestamp: Date | null;
    startDayTimestamp: number;
    context: IVPTripsLastPositionContext;
    computedPositions: Array<IPositionToUpdate | null>;
    gtfsRouteType: GTFSRouteTypeEnum;
}

export interface IShapeGeometryAnchorPoint {
    index: number;
    coordinates: [number, number];
    last_stop_sequence: number;
    next_stop_sequence: number;
    shape_dist_traveled: number;
    this_stop_sequence: number | null;
}
export interface IShapeAnchorPoint {
    index: number;
    at_stop: boolean;
    bearing: number;
    coordinates: [number, number];
    distance_from_last_stop: number;
    last_stop_sequence: number;
    next_stop_sequence: number;
    shape_dist_traveled: number;
    this_stop_sequence: number | null;
    time_scheduled_seconds: number;
}

export interface ISegmentShape extends IShape {
    shape_at_stop: boolean;
}

export interface IComputationTrip {
    trip_id: string;
    stop_times: IStopTime[];
    shapes_anchor_points: IShapeAnchorPoint[];
    shapes: IComputationTripShape[];
}

export interface ITripPositionsWithOrWithoutGTFS {
    gtfsData: IComputationTrip | null;
    id: string;
    gtfs_trip_id: string;
    gtfs_route_type: GTFSRouteTypeEnum;
    start_timestamp: Date;
    end_timestamp: Date | null;
    agency_name_scheduled: string;
    start_cis_stop_id: number | null;
    positions: IVPTripsPositionAttributes[];
    last_position_context: IVPTripsLastPositionContext | null;
}
export interface ITripPositionsWithGTFS extends ITripPositionsWithOrWithoutGTFS {
    gtfsData: IComputationTrip;
}

export interface ICurrentPositionProperties {
    id: string;
    origin_time: string;
    origin_timestamp: Date;
    scheduled_timestamp: Date | null;
    this_stop_id: string | null;
    tcp_event: TCPEventEnum | RegionalBusEventEnum | null;
}

export interface ILastPositionProperties {
    time_delay: number;
    atStopStreak: IVPTripsComputedPositionAtStopStreak;
}

export interface IPropagateDelay {
    nextGtfsTrips: Array<{ trip_id: string; requiredTurnaroundSeconds?: number | undefined }>;
    currentOriginTimestamp: Date;
    currentGtfsTripId: string;
    currentGtfsBlockId: string | null;
    currentRegistrationNumber: number;
    currentDelay: number;
    currentStartTime: string;
    currentEndTime: string;
    currentBearing: number;
    originPositionId: string | null;
}

export interface IVehiclePositionsSchedule {
    route_id: string;
    run_number: number;
    msg_last_timestamp: string;
    gtfs_trip_id?: string;
}

export type TripStopsSchedule = Record<string, Omit<ITripStopsResult, "trip_id">>;
