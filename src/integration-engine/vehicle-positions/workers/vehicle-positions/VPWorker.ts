import { VPContainer } from "#ie/vehicle-positions/ioc/Di";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { AbstractTask, AbstractWorker } from "@golemio/core/dist/integration-engine/workers";
import { WORKER_NAME } from "./constants";
import { DataRetentionTask } from "./tasks/DataRetentionTask";
import { ProcessRegionalBusPositionsTask } from "./tasks/ProcessRegionalBusPositionsTask";
import { PropagateDelayTask } from "./tasks/PropagateDelayTask";
import { PropagateTrainDelayTask } from "./tasks/PropagateTrainDelayTask";
import { RefreshGtfsTripDataTask } from "./tasks/RefreshGtfsTripDataTask";
import { RefreshPublicStopTimeCacheTask } from "./tasks/RefreshPublicStopTimeCacheTask";
import { RefreshPublicTripCacheTask } from "./tasks/RefreshPublicTripCacheTask";
import { SaveDataToDBTask } from "./tasks/SaveDataToDBTask";
import { UpdateDelayTask } from "./tasks/UpdateDelayTask";
import { UpdateGtfsTripIdTask } from "./tasks/UpdateGtfsTripIdTask";
import { UpdateRunsGtfsTripIdTask } from "./tasks/UpdateRunsGtfsTripIdTask";

export class VPWorker extends AbstractWorker {
    protected name = WORKER_NAME;

    constructor() {
        super();

        // Register tasks
        this.registerTask(VPContainer.resolve<RefreshPublicTripCacheTask>(VPContainerToken.RefreshPublicTripCacheTask));
        this.registerTask(VPContainer.resolve<RefreshPublicStopTimeCacheTask>(VPContainerToken.RefreshPublicStopTimeCacheTask));
        this.registerTask(VPContainer.resolve<RefreshGtfsTripDataTask>(VPContainerToken.RefreshGtfsTripDataTask));
        this.registerTask(new SaveDataToDBTask(this.getQueuePrefix()));
        this.registerTask(new UpdateGtfsTripIdTask(this.getQueuePrefix()));
        this.registerTask(new UpdateRunsGtfsTripIdTask(this.getQueuePrefix()));
        this.registerTask(new UpdateDelayTask(this.getQueuePrefix()));
        this.registerTask(new ProcessRegionalBusPositionsTask(this.getQueuePrefix()));
        this.registerTask(new PropagateDelayTask(this.getQueuePrefix()));
        this.registerTask(new PropagateTrainDelayTask(this.getQueuePrefix()));
        this.registerTask(new DataRetentionTask(this.getQueuePrefix()));
    }

    public registerTask = (task: AbstractTask<any>): void => {
        super.registerTask(task);
        task.queuePrefix = this.getQueuePrefix();
    };
}
