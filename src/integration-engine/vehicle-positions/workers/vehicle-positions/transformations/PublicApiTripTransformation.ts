import { IPublicApiCacheDto } from "#sch/vehicle-positions/redis/interfaces/IPublicApiCacheDto";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { VehicleIdGenerator } from "../../gtfs-rt/helpers/VehicleIdGenerator";
import { IEnrichedTripForPublicApi } from "../data-access/interfaces/TripRepositoryInterfaces";

type TransformIn = IEnrichedTripForPublicApi;
type TransformOut = IPublicApiCacheDto;

@injectable()
export class PublicApiTripTransformation extends AbstractTransformation<TransformIn, TransformOut> {
    public name = "PublicApiTripTransformation";

    protected transformInternal = (data: TransformIn): TransformOut => {
        return {
            gtfs_trip_id: data.gtfs_trip_id,
            route_type: data.gtfs_route_type,
            gtfs_route_short_name: data.gtfs_route_short_name,
            lat: Number.parseFloat(data.lat),
            lng: Number.parseFloat(data.lng),
            bearing: data.bearing,
            delay: data.delay,
            state_position: data.state_position,
            vehicle_id: VehicleIdGenerator.getVehicleId(
                data.id,
                data.gtfs_route_type,
                data.provider_source_type,
                data.gtfs_route_short_name,
                data.cis_trip_number,
                data.vehicle_registration_number,
                data.run_number,
                data.internal_run_number
            ),
            detailed_info: {
                is_wheelchair_accessible: data.wheelchair_accessible,
                origin_route_name: data.origin_route_name,
                shape_id: data.gtfs_shape_id,
                run_number: data.run_number,
                trip_headsign: data.last_stop_headsign ?? data.gtfs_trip_headsign,
                shape_dist_traveled: data.shape_dist_traveled === null ? null : Number.parseFloat(data.shape_dist_traveled),
                last_stop_sequence: data.last_stop_sequence,
                origin_timestamp: data.origin_timestamp.toISOString(),
                registration_number: data.vehicle_registration_number,
                operator: data.agency_name_real ?? data.agency_name_scheduled,
            },
        };
    };
}
