import { IScheduleDto } from "#sch/vehicle-positions/redis/interfaces/IGtfsRunTripCacheDto";
import { IsArray, IsObject, IsOptional } from "@golemio/core/dist/shared/class-validator";
import { IUpdateDelayRunTripsData, IUpdateDelayTripsIdsData } from "../data-access/interfaces/TripRepositoryInterfaces";
import { IUpdateDelayInput } from "../interfaces/IUpdateDelayInput";
import { IPositionTransformationResult } from "../interfaces/TransformationInterfaces";

export class UpdateDelayValidationSchema implements IUpdateDelayInput {
    @IsArray()
    @IsObject({ each: true })
    updatedTrips!: Array<IUpdateDelayTripsIdsData | IUpdateDelayRunTripsData>;

    @IsArray()
    @IsObject({ each: true })
    positions!: IPositionTransformationResult[];

    @IsOptional()
    @IsArray()
    @IsObject({ each: true })
    schedule?: IScheduleDto[];
}
