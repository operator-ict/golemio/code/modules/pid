import { IsArray, IsObject } from "@golemio/core/dist/shared/class-validator";
import { IUpdateGTFSTripIdData } from "../data-access/interfaces/TripRepositoryInterfaces";
import { IUpdateGtfsTripIdInput } from "../interfaces/IUpdateGtfsTripIdInput";
import { IPositionTransformationResult } from "../interfaces/TransformationInterfaces";

export class UpdateGtfsTripIdValidationSchema implements IUpdateGtfsTripIdInput {
    @IsArray()
    @IsObject({ each: true })
    trips!: IUpdateGTFSTripIdData[];

    @IsArray()
    @IsObject({ each: true })
    positions!: IPositionTransformationResult[];
}
