import { Type } from "@golemio/core/dist/shared/class-transformer";
import { ArrayMinSize, IsArray, IsObject, IsOptional, IsString, ValidateNested } from "@golemio/core/dist/shared/class-validator";
import {
    IMpvMessageContent,
    IMpvMessageInput,
    IMpvPositionContent,
    IMpvPositionProperties,
    IMpvStopContent,
    IMpvStopProperties,
} from "#sch/vehicle-positions/interfaces/IMpvMessageInterfaces";

class PositionProperties implements IMpvPositionProperties {
    @IsOptional()
    @IsString()
    alias?: string;

    @IsOptional()
    @IsString()
    asw?: string;

    @IsOptional()
    @IsString()
    azimut?: string;

    @IsOptional()
    @IsString()
    cpoz?: string;

    @IsOptional()
    @IsString()
    dopr?: string;

    @IsOptional()
    @IsString()
    doprSkut?: string;

    @IsOptional()
    @IsString()
    info?: string;

    @IsOptional()
    @IsString()
    kmenl?: string;

    @IsOptional()
    @IsString()
    lat?: string;

    @IsOptional()
    @IsString()
    lin?: string;

    @IsOptional()
    @IsString()
    lng?: string;

    @IsOptional()
    @IsString()
    np?: string;

    @IsOptional()
    @IsString()
    po?: string;

    @IsOptional()
    @IsString()
    rychl?: string;

    @IsOptional()
    @IsString()
    sled?: string;

    @IsString()
    spoj!: string;

    @IsOptional()
    @IsString()
    t?: string;

    @IsOptional()
    @IsString()
    vuzevc?: string;

    @IsOptional()
    @IsString()
    zast?: string;

    @IsOptional()
    @IsString()
    zpoz_prij?: string;

    @IsOptional()
    @IsString()
    zpoz_odj?: string;

    @IsOptional()
    @IsString()
    zrus?: string;
}

class StopProperties implements IMpvStopProperties {
    @IsOptional()
    @IsString()
    odj?: string;

    @IsOptional()
    @IsString()
    prij?: string;

    @IsOptional()
    @IsString()
    stan?: string;

    @IsOptional()
    @IsString()
    zast?: string;

    @IsOptional()
    @IsString()
    asw?: string;

    @IsOptional()
    @IsString()
    zpoz_odj?: string;

    @IsOptional()
    @IsString()
    zpoz_prij?: string;

    @IsOptional()
    @IsString()
    zpoz_typ?: string;

    @IsOptional()
    @IsString()
    zpoz_typ_odj?: string;

    @IsOptional()
    @IsString()
    zpoz_typ_prij?: string;
}

class StopContent implements IMpvStopContent {
    @IsObject()
    @ValidateNested()
    @Type(() => StopProperties)
    $!: IMpvStopProperties;
}

class PositionContent implements IMpvPositionContent {
    @IsObject()
    @ValidateNested()
    @Type(() => PositionProperties)
    $!: IMpvPositionProperties;

    @IsArray()
    @ArrayMinSize(1)
    @ValidateNested()
    @Type(() => StopContent)
    zast!: IMpvStopContent[];
}

class MessageContent implements IMpvMessageContent {
    @IsObject({ each: true })
    @ValidateNested()
    @Type(() => PositionContent)
    spoj!: IMpvPositionContent | IMpvPositionContent[];
}

export class SaveDataToDBValidationSchema implements IMpvMessageInput {
    @IsObject()
    @ValidateNested()
    @Type(() => MessageContent)
    m!: IMpvMessageContent;
}
