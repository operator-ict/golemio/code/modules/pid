import { IsArray, IsObject } from "@golemio/core/dist/shared/class-validator";
import { IPropagateTrainDelayInput } from "../interfaces/IPropagateTrainDelayInput";
import { ITripPositionTuple } from "../interfaces/ITripPositionTuple";

export class PropagateDelayValidationSchema implements IPropagateTrainDelayInput {
    @IsArray()
    @IsObject({ each: true })
    data!: ITripPositionTuple[];
}
