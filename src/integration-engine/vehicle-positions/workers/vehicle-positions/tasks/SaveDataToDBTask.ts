import { IMpvMessageInput } from "#sch/vehicle-positions/interfaces/IMpvMessageInterfaces";
import { ICisStopDto } from "#sch/vehicle-positions/models/interfaces/ICisStopDto";
import { IVPTripsModel } from "#sch/vehicle-positions/models/interfaces/IVPTripsModel";
import { ILogger } from "@golemio/core/dist/helpers";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc";
import { QueueManager } from "@golemio/core/dist/integration-engine/queueprocessors";
import { AbstractTask } from "@golemio/core/dist/integration-engine/workers";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { MessageProperties } from "amqplib";
import { CisStopRepository } from "../data-access/CisStopRepository";
import { TripsRepository } from "../data-access/TripsRepository";
import TripsMapper from "../data-access/helpers/TripsMapper";
import { IBulkUpsertOutput } from "../data-access/interfaces/TripRepositoryInterfaces";
import { IPositionTransformationResult } from "../interfaces/TransformationInterfaces";
import { SaveDataToDBValidationSchema } from "../schema/SaveDataToDBSchema";
import { MpvMessageTransformation } from "../transformations/MpvMessageTransformation";

export class SaveDataToDBTask extends AbstractTask<IMpvMessageInput> {
    public readonly queueName = "saveDataToDB";
    public readonly queueTtl = 5 * 60 * 1000; // 5 minutes
    public readonly schema = SaveDataToDBValidationSchema;

    private readonly tripsRepository: TripsRepository;
    private readonly cisStopRepository: CisStopRepository;
    private readonly messageTransformation: MpvMessageTransformation;
    private readonly logger: ILogger;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.tripsRepository = new TripsRepository();
        this.cisStopRepository = new CisStopRepository();
        this.messageTransformation = new MpvMessageTransformation();
        this.logger = IntegrationEngineContainer.resolve<ILogger>(CoreToken.Logger);
    }

    protected async execute(data: IMpvMessageInput, messageProperties?: MessageProperties) {
        const taskStartTimestamp = new Date().getTime();
        const { trips, positions, stops } = await this.messageTransformation.transform(data.m.spoj);
        const tripEntities = await this.tripsRepository.bulkSave<IVPTripsModel>(
            trips,
            // Ignore incoming start_timestamp for existing trips to avoid overwriting existing computed timestamp
            // TODO this is a workaround, dedicated column (gtfs_start_timestamp) will be added
            Object.keys(trips[0] ?? {})
                .filter((key) => key !== "start_timestamp")
                .concat("updated_at"),
            true
        );

        const { inserted, updated } = TripsMapper.mapTripEntitiesToUpdateDto(tripEntities, taskStartTimestamp);

        // At this point we want to send ACK to rabbit and let the rest of the processing to be done in the background
        await this.sendDataToUpdateGtfsTripId(inserted, positions, messageProperties?.timestamp);
        await this.sendDataToUpdateDelay(updated, positions, messageProperties?.timestamp);

        // Save cis stops to later propagate platform codes to departure boards
        await this.saveStops(stops);
    }

    private async saveStops(stops: ICisStopDto[]) {
        try {
            await this.cisStopRepository.bulkSave(stops);
        } catch (error) {
            if (error instanceof AbstractGolemioError) {
                this.logger.error(error);
            } else {
                this.logger.error(
                    new GeneralError("Error while saving stops", SaveDataToDBTask.name, error, undefined, "pid", this.queueName)
                );
            }
        }
    }

    private async sendDataToUpdateGtfsTripId(
        insertedTrips: IBulkUpsertOutput["inserted"],
        positions: IPositionTransformationResult[],
        firstMessageCreatedAt?: number
    ) {
        try {
            // send message for update GTFSTripIds
            for (let i = 0, chunkSize = 50; i < insertedTrips.length; i += chunkSize) {
                const insertedBatch = insertedTrips.slice(i, i + chunkSize);
                const insertedIds = insertedBatch.map((ins) => {
                    return ins.id;
                });

                await QueueManager.sendMessageToExchange(
                    this.queuePrefix,
                    "updateGTFSTripId",
                    {
                        trips: insertedBatch,
                        positions: positions.filter((position) => insertedIds.includes(position.trips_id)),
                    },
                    {
                        timestamp: firstMessageCreatedAt,
                    }
                );
            }
        } catch (error) {
            if (error instanceof AbstractGolemioError) {
                this.logger.error(error);
            } else {
                this.logger.error(
                    new GeneralError(
                        "Error while sending data to update GTFS trip id",
                        SaveDataToDBTask.name,
                        error,
                        undefined,
                        "pid",
                        this.queueName
                    )
                );
            }
        }
    }

    private async sendDataToUpdateDelay(
        updatedTrips: IBulkUpsertOutput["updated"],
        positions: IPositionTransformationResult[],
        firstMessageCreatedAt?: number
    ) {
        try {
            // aggregate runs by origin_route_name & run_number to keep related messages in same bulk
            const runTripMap: Record<string, string[]> = updatedTrips.length ? { unknown: [] } : {};

            for (const trip of updatedTrips) {
                if (!trip.origin_route_name || !trip.run_number) {
                    runTripMap.unknown.push(trip.id);
                } else {
                    const key = `${trip.origin_route_name}_${trip.run_number}`;
                    runTripMap[key] ? runTripMap[key].push(trip.id) : (runTripMap[key] = [trip.id]);
                }
            }

            const chunks: string[][] = [];
            let idx = 0;
            for (const key in runTripMap) {
                if (!chunks[idx]) {
                    chunks[idx] = [];
                }
                chunks[idx] = chunks[idx].concat(runTripMap[key]);
                if (chunks[idx].length > 200) {
                    idx++;
                }
            }
            const promises = [];

            for (const key in chunks) {
                promises.push(this.getTripDataAndSendToUpdateDelay(chunks[key], positions, firstMessageCreatedAt));
            }

            await Promise.all(promises);
        } catch (error) {
            if (error instanceof AbstractGolemioError) {
                this.logger.error(error);
            } else {
                this.logger.error(
                    new GeneralError(
                        "Error while sending data to update delay",
                        SaveDataToDBTask.name,
                        error,
                        undefined,
                        "pid",
                        this.queueName
                    )
                );
            }
        }
    }

    private async getTripDataAndSendToUpdateDelay(
        chunk: string[],
        positions: IPositionTransformationResult[],
        firstMessageCreatedAt?: number
    ) {
        try {
            const updatedTrips = await this.tripsRepository.findAllAssocTripIds(chunk);
            await QueueManager.sendMessageToExchange(
                this.queuePrefix,
                "updateDelay",
                {
                    updatedTrips,
                    positions: positions.filter((position) => chunk.includes(position.trips_id)),
                },
                {
                    timestamp: firstMessageCreatedAt,
                }
            );
        } catch (error) {
            if (error instanceof AbstractGolemioError) {
                this.logger.error(error);
            } else {
                this.logger.error(
                    new GeneralError(
                        "Error while sending data to update delay",
                        SaveDataToDBTask.name,
                        error,
                        undefined,
                        "pid",
                        this.queueName
                    )
                );
            }
        }
    }
}
