import { RopidGTFSStopTimesModel } from "#ie/ropid-gtfs/RopidGTFSStopTimesModel";
import { BlockStopsRedisRepository, RunTripsRedisRepository } from "#ie/ropid-gtfs/data-access/cache";
import { CommonRunsRepository } from "#ie/vehicle-positions/workers/runs/data-access/CommonRunsRepository";
import { IntegrationErrorHandler, log } from "@golemio/core/dist/integration-engine/helpers";
import { QueueManager } from "@golemio/core/dist/integration-engine/queueprocessors";
import { AbstractTask } from "@golemio/core/dist/integration-engine/workers";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { MessageProperties } from "amqplib";
import { PositionsRepository } from "../data-access/PositionsRepository";
import { TripsRepository } from "../data-access/TripsRepository";
import { IUpdateDelayTripsIdsData, IUpdateGTFSTripIdData } from "../data-access/interfaces/TripRepositoryInterfaces";
import { TripStopsManager } from "../helpers/TripStopsManager";
import { VPUtils } from "../helpers/VPUtils";
import { AbstractGTFSTripRunManager } from "../helpers/gtfs-trip-run/AbstractGTFSTripRunManager";
import { GTFSTripRunManagerFactory, GTFSTripRunType } from "../helpers/gtfs-trip-run/GTFSTripRunManagerFactory";
import { IUpdateGtfsTripIdInput } from "../interfaces/IUpdateGtfsTripIdInput";
import { IPositionTransformationResult } from "../interfaces/TransformationInterfaces";
import { TripStopsSchedule } from "../interfaces/VPInterfaces";
import { UpdateGtfsTripIdValidationSchema } from "../schema/UpdateGtfsTripIdSchema";

export class UpdateGtfsTripIdTask extends AbstractTask<IUpdateGtfsTripIdInput> {
    public readonly queueName = "updateGTFSTripId";
    public readonly queueTtl = 5 * 60 * 1000; // 5 minutes
    public readonly schema = UpdateGtfsTripIdValidationSchema;

    private readonly tripsRepository: TripsRepository;
    private readonly positionsRepository: PositionsRepository;
    private readonly gtfsStopTimesRepository: RopidGTFSStopTimesModel;
    private readonly gtfsTripRunManager: AbstractGTFSTripRunManager;
    private readonly tripStopsManager: TripStopsManager;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.tripsRepository = new TripsRepository();
        this.positionsRepository = new PositionsRepository();
        this.gtfsStopTimesRepository = new RopidGTFSStopTimesModel();

        const commonRunsRepository = new CommonRunsRepository();
        const runTripsRedisRepository = new RunTripsRedisRepository();
        const blockStopsRedisRepository = new BlockStopsRedisRepository();

        this.gtfsTripRunManager = GTFSTripRunManagerFactory.create(
            GTFSTripRunType.Common,
            this.positionsRepository,
            this.tripsRepository,
            commonRunsRepository,
            runTripsRedisRepository
        );

        this.tripStopsManager = new TripStopsManager(this.gtfsStopTimesRepository, blockStopsRedisRepository);
    }

    /**
     * Take all newly computed positions and save it, then save the last ones to the trips
     */
    protected async execute(data: IUpdateGtfsTripIdInput, messageProperties?: MessageProperties) {
        // unique ids in input data in key-value (id-positions) structure
        let tripIdToPositionDict: Record<string, IPositionTransformationResult> = {};
        for (const position of data.positions) {
            tripIdToPositionDict[position.trips_id] = position;
        }

        // find gtfs data for each trip
        const foundTrips: Array<IUpdateDelayTripsIdsData | IUpdateDelayTripsIdsData[] | undefined | AbstractGolemioError> =
            await Promise.all(
                data.trips.map(async (trip) => {
                    if (!trip.start_timestamp) {
                        return;
                    }

                    try {
                        return await this.tripsRepository.findGTFSTripId(trip);
                    } catch (err) {
                        log.error(err);
                        return err as AbstractGolemioError;
                    }
                })
            );

        let existingTrips: IUpdateDelayTripsIdsData[] = [],
            existingTripErrors: AbstractGolemioError[] = [];

        // filter only trips with found gtfs data
        for (const trip of foundTrips) {
            if (trip instanceof AbstractGolemioError) {
                existingTripErrors.push(trip);
            } else if (trip instanceof Array) {
                existingTrips.push(...trip);
            } else if (trip?.constructor === Object) {
                existingTrips.push(trip);
            }
        }

        let blockIdToTripIdsDict: Record<string, Set<string>> = {};
        for (const trip of existingTrips) {
            if (!trip.gtfs_block_id || !trip.gtfs_trip_id) continue;
            if (!blockIdToTripIdsDict[trip.gtfs_block_id]) {
                blockIdToTripIdsDict[trip.gtfs_block_id] = new Set();
            }

            blockIdToTripIdsDict[trip.gtfs_block_id].add(trip.gtfs_trip_id);
        }

        // create cache for trip stops (grouped by block id)
        const blockIdToTripStopScheduleDict = await Object.entries(blockIdToTripIdsDict).reduce(
            async (acc, [blockId, gtfsTripIds]) => {
                const tripStops = await acc;
                try {
                    tripStops[blockId] = await this.tripStopsManager.setAndReturnTripStopsSchedule(blockId, [...gtfsTripIds]);
                } catch (err) {
                    IntegrationErrorHandler.handle(err);
                }

                return acc;
            },
            Promise.resolve({} as Record<string, TripStopsSchedule>)
        );

        let currentPosition: IPositionTransformationResult | undefined;
        for (const trip of existingTrips) {
            if (tripIdToPositionDict[trip.id]) {
                currentPosition = tripIdToPositionDict[trip.id];

                if (trip.gtfs_block_id) {
                    // Determine current position tracking status
                    currentPosition.is_tracked = VPUtils.determineTrackingStatus(
                        trip.gtfs_trip_id!,
                        blockIdToTripStopScheduleDict[trip.gtfs_block_id],
                        currentPosition
                    );
                }

                continue;
            }

            if (!currentPosition) {
                continue;
            }

            // Determine new position tracking status
            const newPositionTrackingStatus = VPUtils.determineNewPositionTracking(
                blockIdToTripStopScheduleDict[trip.gtfs_block_id!],
                trip,
                currentPosition
            );

            // duplicate position with new created trip for second trip
            const newPosition = { ...currentPosition };
            newPosition.trips_id = trip.id;
            newPosition.is_tracked = newPositionTrackingStatus;
            data.positions.push(newPosition);
        }

        await this.positionsRepository.bulkSave(data.positions);
        await this.generateAndSaveScheduledTrips(tripIdToPositionDict, data.trips, existingTrips);

        // successfully updated gtfs ids
        for (let i = 0, chunkSize = 100, imax = existingTrips.length; i < imax; i += chunkSize) {
            await QueueManager.sendMessageToExchange(
                this.queuePrefix,
                "updateDelay",
                {
                    positions: [],
                    updatedTrips: existingTrips.slice(i, i + chunkSize),
                },
                { timestamp: messageProperties?.timestamp }
            );
        }

        // gtfsId updating errors
        for (let i = 0, imax = existingTripErrors.length; i < imax; i++) {
            IntegrationErrorHandler.handle(existingTripErrors[i] as GeneralError);
        }
    }

    private generateAndSaveScheduledTrips(
        tripIdToPositionDict: Record<string, IPositionTransformationResult>,
        inputTrips: IUpdateGTFSTripIdData[],
        existingTrips: IUpdateDelayTripsIdsData[]
    ): Promise<void[]> {
        let operationPromises: Array<Promise<void>> = [];

        for (const inputTrip of inputTrips) {
            if (!tripIdToPositionDict[inputTrip.id]?.origin_timestamp) {
                continue;
            }

            const existingTrip = existingTrips.find((existingTrip) => existingTrip.id === inputTrip.id)!;
            const lastPositionIsoTimestamp = new Date(tripIdToPositionDict[inputTrip.id].origin_timestamp).toISOString();
            let routeId = inputTrip.origin_route_name;
            let runNumber = inputTrip.run_number;

            // We don't trust the run number and route name from the input data
            // so we use the ones from the scheduled/existing trip if they exist
            if (existingTrip?.internal_run_number && existingTrip?.internal_route_name) {
                routeId = existingTrip.internal_route_name;
                runNumber = existingTrip.internal_run_number;
            }

            if (!routeId || !runNumber) {
                continue;
            }

            const operationPromise = this.gtfsTripRunManager
                .setAndReturnScheduledTrips({
                    route_id: routeId,
                    run_number: runNumber,
                    msg_last_timestamp: lastPositionIsoTimestamp,
                })
                .then((scheduledTrips) => void scheduledTrips);

            operationPromises.push(operationPromise);
        }

        return Promise.all(operationPromises);
    }
}
