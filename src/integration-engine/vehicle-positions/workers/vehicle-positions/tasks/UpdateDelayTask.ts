import { GTFSRouteTypeEnum } from "#helpers/RouteTypeEnums";
import { RopidGTFSStopTimesModel } from "#ie/ropid-gtfs/RopidGTFSStopTimesModel";
import { RopidGTFSTripsModel } from "#ie/ropid-gtfs/RopidGTFSTripsModel";
import { BlockStopsRedisRepository, DelayComputationRedisRepository } from "#ie/ropid-gtfs/data-access/cache";
import { RopidGtfsContainer } from "#ie/ropid-gtfs/ioc/Di";
import { RopidGtfsContainerToken } from "#ie/ropid-gtfs/ioc/RopidGtfsContainerToken";
import { VPContainer } from "#ie/vehicle-positions/ioc/Di";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { IVPTripsModel } from "#sch/vehicle-positions/models/interfaces/IVPTripsModel";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger";
import { IntegrationErrorHandler } from "@golemio/core/dist/integration-engine/helpers";
import { QueueManager } from "@golemio/core/dist/integration-engine/queueprocessors";
import { AbstractTask } from "@golemio/core/dist/integration-engine/workers";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { MessageProperties } from "amqplib";
import { PositionsRepository } from "../data-access/PositionsRepository";
import { TripsRepository } from "../data-access/TripsRepository";
import { DelayComputationManager } from "../helpers/DelayComputationManager";
import { PositionsManager } from "../helpers/PositionsManager";
import { TripStopsManager } from "../helpers/TripStopsManager";
import { VPUtils } from "../helpers/VPUtils";
import { MetroShapePointsFixer } from "../helpers/metro/MetroShapePointsFixer";
import { IUpdateDelayInput } from "../interfaces/IUpdateDelayInput";
import { IPositionTransformationResult } from "../interfaces/TransformationInterfaces";
import {
    IComputationTrip,
    IPositionToUpdate,
    IProcessedPositions,
    ITripPositionsWithGTFS,
    ITripPositionsWithOrWithoutGTFS,
    TripStopsSchedule,
} from "../interfaces/VPInterfaces";
import { UpdateDelayValidationSchema } from "../schema/UpdateDelaySchema";
import { UpdateDelayLimiter } from "./helpers/UpdateDelayLimiter";

export class UpdateDelayTask extends AbstractTask<IUpdateDelayInput> {
    public readonly queueName = "updateDelay";
    public readonly queueTtl = 50 * 1000; // 50 seconds
    public readonly schema = UpdateDelayValidationSchema;

    private readonly logger: ILogger;
    private readonly tripsRepository: TripsRepository;
    private readonly positionsRepository: PositionsRepository;
    private readonly delayComputationRedisRepository: DelayComputationRedisRepository;
    private readonly tripStopsManager: TripStopsManager;
    private readonly delayComputationManager: DelayComputationManager;
    private readonly metroShapePointsFixer: MetroShapePointsFixer;
    private readonly updateDelayLimiter: UpdateDelayLimiter;
    private readonly positionsManager: PositionsManager;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.logger = VPContainer.resolve<ILogger>(CoreToken.Logger);
        this.tripsRepository = new TripsRepository();
        this.positionsRepository = new PositionsRepository();
        this.delayComputationRedisRepository = new DelayComputationRedisRepository();

        const gtfsStopTimesRepository = new RopidGTFSStopTimesModel();
        const gtfsTripsRepository = new RopidGTFSTripsModel();
        const blockStopsRedisRepository = RopidGtfsContainer.resolve<BlockStopsRedisRepository>(
            RopidGtfsContainerToken.BlockStopsRedisRepository
        );

        this.tripStopsManager = new TripStopsManager(gtfsStopTimesRepository, blockStopsRedisRepository);
        this.delayComputationManager = new DelayComputationManager(gtfsTripsRepository);
        this.metroShapePointsFixer = VPContainer.resolve(VPContainerToken.MetroShapePointsFixer);
        this.updateDelayLimiter = new UpdateDelayLimiter("UpdateDelayTask", this.queueTtl);
        this.positionsManager = VPContainer.resolve<PositionsManager>(VPContainerToken.PositionsManager);
    }

    protected async execute(data: IUpdateDelayInput, messageProperties?: MessageProperties) {
        const tripIds: string[] = data.updatedTrips.map((trip) => trip.id);
        if (tripIds.length === 0) {
            return;
        }

        if (data.positions.length > 0) {
            await this.savePositions(data);
        }

        if (!this.updateDelayLimiter.isActualMessage(messageProperties)) {
            return; // skip processing of old messages
        }

        let gtfsComputationTrips: Array<IComputationTrip | null> = [];
        let tripsPositionsWithGtfsData: ITripPositionsWithOrWithoutGTFS[] = [];

        try {
            const tripsPositionsToUpdate = await this.positionsRepository.getPositionsForUpdateDelay(tripIds);
            gtfsComputationTrips = await this.delayComputationRedisRepository.mget<IComputationTrip>(
                // Append gtfs data from cache to each trip
                tripsPositionsToUpdate.map((trip) => trip.gtfs_trip_id)
            );

            for (let i = 0; i < tripsPositionsToUpdate.length; i++) {
                const trip = tripsPositionsToUpdate[i];
                const gtfsData = gtfsComputationTrips[i];
                tripsPositionsWithGtfsData.push({
                    ...trip,
                    gtfsData,
                });
            }

            const processedPositions = await Promise.all(
                tripsPositionsWithGtfsData.map(async (trip) => {
                    if (trip.gtfsData === null) {
                        trip = await this.enrichTripWithGtfsComputationData(trip);

                        if (trip.gtfsData === null) {
                            return null;
                        }
                    }

                    const computedPositions = this.positionsManager.computePositions(
                        trip as ITripPositionsWithGTFS,
                        data.schedule
                    );

                    this.propagateKnownPositionProperties(trip, computedPositions);
                    return computedPositions;
                })
            );

            const validProcessedPositions = processedPositions.filter((e): e is IProcessedPositions => !!e);
            await this.updateComputedPositionsAndTrips(validProcessedPositions);

            await Promise.all([
                this.invalidateBacktrackedPositions(validProcessedPositions),
                this.expireDelayComputationCache(tripsPositionsWithGtfsData),
            ]);

            await QueueManager.sendMessageToExchange(this.queuePrefix, "propagateDelay", {
                processedPositions: validProcessedPositions,
                trips: data.updatedTrips,
            });
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            }

            throw new GeneralError(
                `Error while updating delay. For trip ids:${tripIds.slice(0, 10).join(",")}`,
                this.constructor.name,
                err
            );
        }
    }

    private enrichTripWithGtfsComputationData = async (
        trip: ITripPositionsWithOrWithoutGTFS
    ): Promise<ITripPositionsWithOrWithoutGTFS> => {
        try {
            let gtfsComputationData = await this.delayComputationManager.getComputationObject(trip.gtfs_trip_id);
            if (!gtfsComputationData) {
                return trip;
            }

            if (trip.gtfs_route_type === GTFSRouteTypeEnum.METRO) {
                gtfsComputationData = await this.metroShapePointsFixer.fix(gtfsComputationData);
            }

            await this.delayComputationRedisRepository.set("trip_id", gtfsComputationData, VPUtils.DELAY_COMPUTATION_CACHE_TTL);
            trip.gtfsData = gtfsComputationData;
        } catch (err) {
            if (err instanceof GeneralError) {
                this.logger.warn(err);
            } else {
                this.logger.error(
                    new GeneralError(
                        "Error while getting delay computation data for trip " + trip.gtfs_trip_id,
                        this.constructor.name,
                        err
                    )
                );
            }
        }

        return trip;
    };

    /**
     * Propagate known position properties from trips positions to processed positions, when the processed position property is
     * `undefined`
     *
     * @param tripsPositions Trips positions to propagate properties from
     * @param processedPositions Processed positions to propagate properties to
     */
    private propagateKnownPositionProperties = (
        tripPositions: ITripPositionsWithOrWithoutGTFS,
        processedPositions: IProcessedPositions
    ) => {
        for (let i = 0; i < processedPositions.positions.length; i++) {
            if (processedPositions.positions[i] === null) continue;
            processedPositions.positions[i] = {
                ...tripPositions.positions[i],
                ...processedPositions.positions[i],
            };
        }
    };

    /**
     * Take all newly computed positions and save it, then save the last ones to the trips
     *
     * @param processedPositions - Arrays of all newly computed positions
     */
    private updateComputedPositionsAndTrips = async (processedPositions: IProcessedPositions[]): Promise<void> => {
        let positions: IPositionToUpdate[] = [];
        let trips = new Map<string, Partial<IVPTripsModel>>();

        for (const processedPosition of processedPositions) {
            positions = positions.concat(
                processedPosition.positions.filter((position) => position !== null) as IPositionToUpdate[]
            );
            trips.set(processedPosition.context.tripId, {
                id: processedPosition.context.tripId,
                last_position_id: processedPosition.context.lastPositionId ?? null,
                last_position_context: processedPosition.context,
                is_canceled: processedPosition.context.lastPositionCanceled,
            });
        }

        await this.positionsRepository.bulkSave(positions);
        await this.tripsRepository.bulkSave(Array.from(trips.values()));
    };

    private async savePositions(data: IUpdateDelayInput) {
        // unique ids in input data in key-value (id-positions) structure
        let tripIdToPositionDict: Record<string, IPositionTransformationResult> = {};
        for (const position of data.positions) {
            tripIdToPositionDict[position.trips_id] = position;
        }

        // unique block_ids and its parent input trips_id in key-value structure
        let tripsIdsByBlockIds: Record<string, string> = {};

        // unique block_ids and its parent gtfs_trip_id in key-value structure
        let blockIdToTripIdsDict: Record<string, Set<string>> = {};

        for (const trip of data.updatedTrips) {
            if (!trip.gtfs_block_id) {
                continue;
            }

            if (!tripsIdsByBlockIds[trip.gtfs_block_id]) {
                tripsIdsByBlockIds[trip.gtfs_block_id] = trip.id;
            } else {
                // save only the shorter trip_id (it is the raw input id with no suffixes)
                tripsIdsByBlockIds[trip.gtfs_block_id] =
                    tripsIdsByBlockIds[trip.gtfs_block_id] > trip.id ? trip.id : tripsIdsByBlockIds[trip.gtfs_block_id];
            }

            if (!blockIdToTripIdsDict[trip.gtfs_block_id]) {
                blockIdToTripIdsDict[trip.gtfs_block_id] = new Set();
            }

            if (trip.gtfs_trip_id) {
                blockIdToTripIdsDict[trip.gtfs_block_id].add(trip.gtfs_trip_id);
            }
        }

        // Get cached schedule for each unique block_id
        const blockIdToTripStopScheduleDict = await Object.entries(blockIdToTripIdsDict).reduce(
            async (acc, [blockId, gtfsTripIds]) => {
                const tripStops = await acc;
                try {
                    const schedule = await this.tripStopsManager.setAndReturnTripStopsSchedule(blockId, [...gtfsTripIds]);
                    tripStops[blockId] = schedule;
                } catch (err) {
                    IntegrationErrorHandler.handle(err);
                }

                return acc;
            },
            Promise.resolve({} as Record<string, TripStopsSchedule>)
        );

        // duplicate positions for trips with block_ids
        for (const trip of data.updatedTrips) {
            if (tripIdToPositionDict[trip.id]) {
                if (trip.gtfs_block_id) {
                    // Determine current position tracking status
                    tripIdToPositionDict[trip.id].is_tracked = VPUtils.determineTrackingStatus(
                        trip.gtfs_trip_id!,
                        blockIdToTripStopScheduleDict[trip.gtfs_block_id],
                        tripIdToPositionDict[trip.id]
                    );
                }

                continue;
            }

            const currentPosition = data.positions.find((position) => {
                return position.trips_id === tripsIdsByBlockIds[trip.gtfs_block_id!];
            });

            if (!currentPosition) {
                continue;
            }

            // Determine position duplication and new position tracking status
            const newPositionTrackingStatus = VPUtils.determineNewPositionTracking(
                blockIdToTripStopScheduleDict[trip.gtfs_block_id!],
                trip,
                currentPosition
            );

            // duplicate position with new created trip for second trip
            const newPosition = { ...currentPosition };
            newPosition.trips_id = trip.id;
            newPosition.is_tracked = newPositionTrackingStatus;
            data.positions.push(newPosition);
        }

        // save all new positions
        await this.positionsRepository.bulkSave(data.positions);
    }

    /**
     * Expire delay computation cache for given trips
     */
    private async expireDelayComputationCache(tripPositions: ITripPositionsWithOrWithoutGTFS[]) {
        const tripIds: string[] = [];
        for (const trip of tripPositions) {
            if (trip.gtfsData) {
                tripIds.push(trip.gtfsData.trip_id);
            }
        }

        try {
            await this.delayComputationRedisRepository.expire(tripIds, VPUtils.DELAY_COMPUTATION_CACHE_TTL);
        } catch (err) {
            this.logger.error(err);
        }
    }

    /**
     * Invalidate positions after the last stop sequence of backtracked positions
     * We do this to improve historical data quality
     * as well as to prevent serving of incorrect stop times to API clients
     * (vehicle is expected to repeat some part of the trip)
     */
    private async invalidateBacktrackedPositions(processedPositions: IProcessedPositions[]) {
        const tripWithStopSequenceTuples: Array<[string, number]> = processedPositions
            .filter(({ isBacktrackingDetected }) => !!isBacktrackingDetected)
            .map(({ context, positions }) => {
                const tripId = context.tripId;
                const maxStopSequence = this.getMaxStopSequence(positions);
                return [tripId, maxStopSequence];
            });

        if (tripWithStopSequenceTuples.length === 0) {
            return;
        }

        const count = await this.positionsRepository.invalidatePositionsAfterStopSequence(tripWithStopSequenceTuples);
        this.logger.info(`Invalidated ${count} backtracked positions`);
    }

    private getMaxStopSequence(positions: Array<IPositionToUpdate | null>): number {
        const maxStopSequence = Math.max(...positions.map((position) => position?.last_stop_sequence ?? 0));
        if (maxStopSequence === 0) {
            throw new GeneralError("Max stop sequence must be greater than 0", this.constructor.name);
        }

        return maxStopSequence;
    }
}
