import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { VehicleIdGenerator } from "#ie/vehicle-positions/workers/gtfs-rt/helpers/VehicleIdGenerator";
import { IPublicStopTimeCacheDto } from "#sch/vehicle-positions/redis/interfaces/IPublicStopTimeCacheDto";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { IoRedisConnector } from "@golemio/core/dist/helpers/data-access/redis/IoRedisConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger";
import { QueueManager } from "@golemio/core/dist/integration-engine";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc";
import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine/workers";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { Mutex } from "@golemio/core/dist/shared/redis-semaphore";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { setTimeout as sleep } from "node:timers/promises";
import { WORKER_NAME } from "../constants";
import { PublicStopTimeCacheRepository } from "../data-access/cache/PublicStopTimeCacheRepository";
import { PublicStopTimeRepository } from "../data-access/views/PublicStopTimeRepository";
import { VPUtils } from "../helpers/VPUtils";

@injectable()
export class RefreshPublicStopTimeCacheTask extends AbstractEmptyTask {
    public readonly queueName = "refreshPublicStopTimeCache";
    public readonly queueTtl = 10000; // 10s
    private lockTimeout: number;
    private refreshInterval: number;

    constructor(
        @inject(CoreToken.SimpleConfig) config: ISimpleConfig,
        @inject(CoreToken.Logger) private logger: ILogger,
        @inject(ContainerToken.RedisConnector) private redisClient: IoRedisConnector,
        @inject(VPContainerToken.PublicStopTimeRepository)
        private publicStopTimeRepository: PublicStopTimeRepository,
        @inject(VPContainerToken.PublicStopTimeCacheRepository)
        private publicStopTimeCacheRepository: PublicStopTimeCacheRepository
    ) {
        super(WORKER_NAME);
        this.lockTimeout = config.getValue<number>(
            "old.datasources.pid.vehicle-positions.publicCache.mutexLockStopTimeTimeout",
            6000
        ) as number; // default 6s
        this.refreshInterval = this.lockTimeout * 0.8;
    }

    protected async execute() {
        const startDateTime = new Date();
        const mutex = this.createMutex();
        const lockAcquired = await mutex.tryAcquire();

        if (!lockAcquired) {
            this.logger.info(`${this.constructor.name}: mutex lock was not acquired`);
            return;
        }

        try {
            const stopTimes = await this.publicStopTimeRepository.findAll();
            if (stopTimes.length === 0) {
                return;
            }

            const data = new Map<string, IPublicStopTimeCacheDto[]>();
            for (const stopTime of stopTimes) {
                const vehicleId = VehicleIdGenerator.getVehicleId(
                    stopTime.rt_trip_id,
                    stopTime.gtfs_route_type,
                    stopTime.provider_source_type,
                    stopTime.gtfs_route_short_name,
                    stopTime.cis_trip_number,
                    stopTime.vehicle_registration_number,
                    stopTime.run_number,
                    stopTime.internal_run_number
                );
                const key = `${vehicleId}-${stopTime.gtfs_trip_id}`;
                if (!data.has(key)) {
                    data.set(key, []);
                }

                data.get(key)!.push({
                    sequence: stopTime.stop_sequence,
                    arr_delay: stopTime.stop_arr_delay,
                    dep_delay: stopTime.stop_dep_delay,
                    cis_stop_platform_code: stopTime.cis_stop_platform_code,
                    platform_code: stopTime.platform_code,
                    stop_id: stopTime.stop_id,
                });
            }

            await this.publicStopTimeCacheRepository.createOrUpdate(data, VPUtils.DELAY_COMPUTATION_CACHE_TTL);
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            }

            throw new GeneralError(
                `${this.constructor.name}: error while refreshing public stop time cache`,
                this.constructor.name,
                err
            );
        } finally {
            await mutex.release();
        }

        const ellapsedTimeInMs = new Date().getTime() - startDateTime.getTime();
        this.logger.info(`${this.constructor.name}: finished in ${ellapsedTimeInMs}ms`);

        if (ellapsedTimeInMs < this.refreshInterval) {
            await sleep(this.refreshInterval - ellapsedTimeInMs);
        }

        QueueManager.sendMessageToExchange(this.queuePrefix, this.queueName, {});
    }

    private createMutex() {
        return new Mutex(this.redisClient.getConnection(), this.queueName, {
            acquireAttemptsLimit: 1,
            lockTimeout: this.lockTimeout,
            refreshInterval: this.refreshInterval,
            onLockLost: (err) => {
                this.logger.error(err, `${this.constructor.name}: mutex lock was lost`);
            },
        });
    }
}
