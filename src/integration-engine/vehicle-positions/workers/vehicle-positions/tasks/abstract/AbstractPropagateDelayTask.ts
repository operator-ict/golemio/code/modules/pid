import { IPositionDto } from "#sch/vehicle-positions/models/interfaces/IPositionDto";
import { IVPTripsModel } from "#sch/vehicle-positions/models/interfaces/IVPTripsModel";
import { AbstractTask } from "@golemio/core/dist/integration-engine";
import { StatePositionEnum, StateProcessEnum } from "src/const";
import { PositionsRepository } from "../../data-access/PositionsRepository";
import { TripsRepository } from "../../data-access/TripsRepository";
import { IPropagateDelay } from "../../interfaces/VPInterfaces";

export abstract class AbstractPropagateDelayTask<T extends object> extends AbstractTask<T> {
    public readonly queueTtl = 5 * 60 * 1000; // 5 minutes

    protected readonly tripsRepository: TripsRepository;
    protected readonly positionsRepository: PositionsRepository;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.tripsRepository = new TripsRepository();
        this.positionsRepository = new PositionsRepository();
    }

    /**
     * Takes all newly computed positions search for next trips in near future and propagate delay for them
     */
    protected propagateDelayForGtfsTrip = async (data: IPropagateDelay) => {
        const trips = await this.tripsRepository.getPropagateDelayTripsWithPositions(data);
        if (!trips.length) return;

        let tripsToUpdate = new Map<string, Partial<IVPTripsModel>>();
        let positionsToUpdate: Array<Partial<IPositionDto>> = [];
        let turnTimeAcc = (new Date(trips[0].start_timestamp!).getTime() - new Date(data.currentEndTime).getTime()) / 1000;

        for (let i = 0; i < trips.length; i++) {
            const trip = trips[i];
            const lastPosition = trip.last_position;
            const requiredTurnaroundSeconds =
                data.nextGtfsTrips.find((gtfsTrip) => gtfsTrip.trip_id === trip.gtfs_trip_id)?.requiredTurnaroundSeconds ?? 0;

            let delayInSeconds = data.currentDelay;

            // If trips are not connected through block_id, subtract the time that the vehicle is expected to wait between trips
            // otherwise delay should be propagated to next trips as is
            if (!data.currentGtfsBlockId || !trip.gtfs_block_id || data.currentGtfsBlockId !== trip.gtfs_block_id) {
                if (i === 0) {
                    turnTimeAcc -= requiredTurnaroundSeconds;
                } else {
                    const previousTrip = trips[i - 1];
                    const startTimestamp = new Date(trip.start_timestamp!).getTime();
                    const endTimestamp = new Date(previousTrip.end_timestamp!).getTime();

                    turnTimeAcc += (startTimestamp - endTimestamp) / 1000 - requiredTurnaroundSeconds;
                }

                delayInSeconds = Math.max(0, delayInSeconds - turnTimeAcc);
            }

            tripsToUpdate.set(trip.id, {
                id: trip.id,
                last_position_id: lastPosition.id,
                last_position_context: {
                    ...trip.last_position_context!,
                    lastPositionLat: typeof lastPosition.lat === "string" ? parseFloat(lastPosition.lat) : lastPosition.lat,
                    lastPositionLng: typeof lastPosition.lng === "string" ? parseFloat(lastPosition.lng) : lastPosition.lng,
                    lastPositionState: StatePositionEnum.BEFORE_TRACK_DELAYED,
                    lastPositionStateChange: new Date().toISOString(),
                    lastPositionBeforeTrackDelayed: {
                        delay: delayInSeconds,
                        origin_timestamp: lastPosition.origin_timestamp,
                    },
                },
            });

            positionsToUpdate.push({
                id: lastPosition.id,
                delay: delayInSeconds,
                state_position: StatePositionEnum.BEFORE_TRACK_DELAYED,
                state_process: StateProcessEnum.PROCESSED,
                origin_timestamp: lastPosition.origin_timestamp,
                bearing: data.currentBearing,
                origin_position_id: data.originPositionId,
            });
        }

        await this.positionsRepository.bulkSave(positionsToUpdate);
        await this.tripsRepository.bulkSave(Array.from(tripsToUpdate.values()));
    };
}
