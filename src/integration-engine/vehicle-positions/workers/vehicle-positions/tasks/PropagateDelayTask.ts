import { GTFSRouteTypeEnum } from "#helpers/RouteTypeEnums";
import { RunTripsRedisRepository } from "#ie/ropid-gtfs/data-access/cache/RunTripsRedisRepository";
import { IGtfsRunTripCacheDto, IScheduleDto } from "#sch/vehicle-positions/redis/interfaces/IGtfsRunTripCacheDto";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { QueueManager } from "@golemio/core/dist/integration-engine/queueprocessors/QueueManager";
import { StatePositionEnum } from "src/const";
import { IUpdateDelayRunTripsData, IUpdateDelayTripsIdsData } from "../data-access/interfaces/TripRepositoryInterfaces";
import { IPropagateDelayInput } from "../interfaces/IPropagateDelayInput";
import { IPropagateTrainDelayInput } from "../interfaces/IPropagateTrainDelayInput";
import { ITripPositionTuple } from "../interfaces/ITripPositionTuple";
import { IProcessedPositions } from "../interfaces/VPInterfaces";
import { PropagateDelayValidationSchema } from "../schema/PropagateDelaySchema";
import { AbstractPropagateDelayTask } from "./abstract/AbstractPropagateDelayTask";

const ALLOWED_PROPAGATION_STATES = [StatePositionEnum.ON_TRACK, StatePositionEnum.AT_STOP, StatePositionEnum.AFTER_TRACK_DELAYED];

export class PropagateDelayTask extends AbstractPropagateDelayTask<IPropagateDelayInput> {
    public readonly queueName = "propagateDelay";
    public readonly queueTtl = 5 * 60 * 1000; // 5 minutes
    public readonly schema = PropagateDelayValidationSchema;

    protected readonly runTripsRedisRepository: RunTripsRedisRepository;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.runTripsRedisRepository = new RunTripsRedisRepository();
    }

    protected async execute(data: IPropagateDelayInput) {
        const { tripPositionMap, trainTripPositionMap } = this.mapDelayedPositionsToRunTrips(data.processedPositions, data.trips);

        if (trainTripPositionMap.length > 0) {
            await QueueManager.sendMessageToExchange(this.queuePrefix, "propagateTrainDelay", {
                data: trainTripPositionMap,
            } as IPropagateTrainDelayInput);
        }

        for (const delayedPositions of tripPositionMap) {
            const {
                internal_route_name,
                internal_run_number,
                gtfs_trip_id,
                gtfs_block_id,
                vehicle_registration_number,
                start_timestamp,
            } = delayedPositions.trip;

            const scheduledTrips: IGtfsRunTripCacheDto | undefined = await this.runTripsRedisRepository.get(
                `${internal_route_name}_${internal_run_number}`
            );

            const positionTripWeekDay = new Date(start_timestamp).getDay();
            const gtfsCurrentTrip = scheduledTrips?.schedule.find((trip) => {
                return this.isCurrentGtfsTrip(trip, gtfs_trip_id, positionTripWeekDay);
            });

            if (!gtfsCurrentTrip) {
                log.info(
                    `${this.queueName}: could not determine GTFS trip from run trips data: ` +
                        JSON.stringify(delayedPositions.trip).substring(0, 2000)
                );
                continue;
            }

            const gtfsNextTrips = scheduledTrips!.schedule.filter((trip) => {
                return this.isConnectingLinkIn60Minutes(gtfsCurrentTrip, trip);
            });

            try {
                await this.propagateDelayForGtfsTrip({
                    currentGtfsTripId: gtfs_trip_id,
                    currentGtfsBlockId: gtfs_block_id ?? null,
                    currentOriginTimestamp: delayedPositions.position.originTimestamp,
                    currentDelay: delayedPositions.position.delayInSeconds,
                    currentRegistrationNumber: vehicle_registration_number,
                    currentStartTime: gtfsCurrentTrip.start_timestamp,
                    currentEndTime: gtfsCurrentTrip.end_timestamp,
                    currentBearing: delayedPositions.position.bearing!,
                    nextGtfsTrips: gtfsNextTrips.map((trip) => {
                        return {
                            trip_id: trip.trip_id,
                            requiredTurnaroundSeconds: trip.requiredTurnaroundSeconds,
                        };
                    }),
                    originPositionId: delayedPositions.position.originId,
                });
            } catch (err) {
                log.warn(`${this.queueName}: could not propagate delay for GTFS trip: ${gtfs_trip_id} - ${err.message}`);
            }
        }
    }

    private mapDelayedPositionsToRunTrips = (
        processedPositions: IProcessedPositions[],
        updatedTrips: Array<IUpdateDelayTripsIdsData | IUpdateDelayRunTripsData>
    ): { tripPositionMap: ITripPositionTuple[]; trainTripPositionMap: ITripPositionTuple[] } => {
        const tripPositionMap: ITripPositionTuple[] = [];
        const trainTripPositionMap: ITripPositionTuple[] = [];

        for (const batch of processedPositions) {
            const statePosition = batch.context.lastPositionState;
            if (!statePosition || !ALLOWED_PROPAGATION_STATES.includes(statePosition)) {
                continue;
            }

            const delayInSeconds = batch.context.lastPositionDelay;
            const originUnixTimestamp = batch.context.lastPositionOriginTimestamp;
            if (typeof delayInSeconds !== "number" || typeof originUnixTimestamp !== "number") {
                continue;
            }

            const trip = updatedTrips.find((el) => el.id === batch.context.tripId);
            if (trip && this.isUpdateDelayRunTripsIdsData(trip)) {
                tripPositionMap.push({
                    position: {
                        delayInSeconds,
                        originTimestamp: new Date(originUnixTimestamp),
                        bearing: batch.context.lastPositionTracking?.properties.bearing!,
                        originId: batch.context.lastPositionId,
                    },
                    trip: trip as IUpdateDelayRunTripsData,
                });
            } else if (trip && this.isBlockedTrainTripData(trip)) {
                trainTripPositionMap.push({
                    position: {
                        delayInSeconds,
                        originTimestamp: new Date(originUnixTimestamp),
                        bearing: batch.context.lastPositionTracking?.properties.bearing!,
                        originId: batch.context.lastPositionId,
                    },
                    trip: trip as IUpdateDelayRunTripsData,
                });
            }
        }
        return { tripPositionMap, trainTripPositionMap };
    };

    private isUpdateDelayRunTripsIdsData = (
        data: IUpdateDelayTripsIdsData | IUpdateDelayRunTripsData
    ): data is IUpdateDelayRunTripsData => {
        const { internal_route_name, internal_run_number, gtfs_trip_id, vehicle_registration_number } =
            data as IUpdateDelayRunTripsData;
        return !!(
            internal_route_name &&
            internal_run_number &&
            gtfs_trip_id &&
            typeof vehicle_registration_number !== "undefined"
        );
    };

    private isBlockedTrainTripData = (data: IUpdateDelayTripsIdsData | IUpdateDelayRunTripsData): boolean => {
        const { gtfs_trip_id, gtfs_block_id, gtfs_route_type } = data as IUpdateDelayRunTripsData;

        return !!(gtfs_trip_id && gtfs_block_id && gtfs_route_type === GTFSRouteTypeEnum.TRAIN);
    };

    private isConnectingLinkIn60Minutes(gtfsCurrentTrip: IScheduleDto, trip: IScheduleDto) {
        const currentTripEndTime = new Date(gtfsCurrentTrip.end_timestamp);
        const nextTripStartTime = new Date(trip.start_timestamp);
        const currentTripEndTimePlus60Minutes = new Date(currentTripEndTime.getTime() + 60 * 60 * 1000);

        return currentTripEndTimePlus60Minutes >= nextTripStartTime && currentTripEndTime <= nextTripStartTime;
    }

    private isCurrentGtfsTrip(tripSchedule: IScheduleDto, propagatedGtfsTripId: string, propagatedTripWeekDay: number) {
        const tripWeekDay = new Date(tripSchedule.start_timestamp).getDay();
        return tripSchedule.trip_id === propagatedGtfsTripId && tripWeekDay === propagatedTripWeekDay;
    }
}
