import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { ILogger } from "@golemio/core/dist/helpers";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine/workers";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { WORKER_NAME } from "../constants";
import { TripsRepository } from "../data-access/TripsRepository";
import { IGtfsTripDataFixerFactory } from "../domain/service/IGtfsTripDataFixerFactory";
import { ProviderSourceTypeEnum } from "../helpers/ProviderSourceTypeEnum";

/**
 * Updates GTFS trip data for realtime trips immediately following a GTFS timetable database refresh
 * - Identifies and corrects realtime trips using obsolete GTFS trip IDs (from the previous timetable)
 * - Refreshes GTFS trip data (route ID, block ID, headsign, etc.) based on updated GTFS trip IDs
 */
@injectable()
export class RefreshGtfsTripDataTask extends AbstractEmptyTask {
    public readonly queueName = "refreshGTFSTripData";
    public readonly queueTtl = 10 * 60 * 1000; // 10 minutes

    constructor(
        @inject(CoreToken.Logger) private logger: ILogger,
        @inject(VPContainerToken.GtfsTripDataFixerFactory) private gtfsDataFixerFactory: IGtfsTripDataFixerFactory,
        @inject(VPContainerToken.TripRepository) private tripRepository: TripsRepository
    ) {
        super(WORKER_NAME);
    }

    protected async execute() {
        // Find and fix realtime trips with invalid GTFS trip ID
        // HTTP trips
        const numOfHttpTrips = await this.gtfsDataFixerFactory.getStrategy(ProviderSourceTypeEnum.Http).fix(this.queuePrefix);
        this.logger.info(
            `${this.constructor.name}: ${numOfHttpTrips} HTTP trips with invalid GTFS trip ID detected and sent for fixing`
        );

        // Refresh GTFS trip data based on GTFS trip ID
        await this.tripRepository.refreshGtfsTripData();
    }
}
