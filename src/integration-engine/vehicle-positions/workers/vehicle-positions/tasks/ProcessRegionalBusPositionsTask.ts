import { RopidGTFSTripsModel } from "#ie/ropid-gtfs/RopidGTFSTripsModel";
import { DelayComputationRedisRepository } from "#ie/ropid-gtfs/data-access/cache";
import { VPContainer } from "#ie/vehicle-positions/ioc/Di";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { IVPTripsModel } from "#sch/vehicle-positions/models/interfaces/IVPTripsModel";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger";
import { QueueManager } from "@golemio/core/dist/integration-engine/queueprocessors";
import { AbstractTask } from "@golemio/core/dist/integration-engine/workers";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { MessageProperties } from "amqplib";
import { PositionsRepository } from "../data-access/PositionsRepository";
import { TripsRepository } from "../data-access/TripsRepository";
import { DelayComputationManager } from "../helpers/DelayComputationManager";
import { VPUtils } from "../helpers/VPUtils";
import { RegionalBusPositionsManager } from "../helpers/regional-bus/RegionalBusPositionsManager";
import { IUpdateDelayInput } from "../interfaces/IUpdateDelayInput";
import {
    IComputationTrip,
    IPositionToUpdate,
    IProcessedPositions,
    ITripPositionsWithGTFS,
    ITripPositionsWithOrWithoutGTFS,
} from "../interfaces/VPInterfaces";
import { UpdateDelayValidationSchema } from "../schema/UpdateDelaySchema";
import { UpdateDelayLimiter } from "./helpers/UpdateDelayLimiter";

export class ProcessRegionalBusPositionsTask extends AbstractTask<IUpdateDelayInput> {
    public readonly queueName = "processRegionalBusPositions";
    public readonly queueTtl = 2 * 60 * 1000; // 2 minutes
    public readonly schema = UpdateDelayValidationSchema;

    private readonly logger: ILogger;
    private readonly tripsRepository: TripsRepository;
    private readonly positionsRepository: PositionsRepository;
    private readonly delayComputationRedisRepository: DelayComputationRedisRepository;
    private readonly delayComputationManager: DelayComputationManager;
    private readonly updateDelayLimiter: UpdateDelayLimiter;
    private readonly regionalBusPositionsManager: RegionalBusPositionsManager;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.logger = VPContainer.resolve<ILogger>(CoreToken.Logger);
        this.tripsRepository = new TripsRepository();
        this.positionsRepository = new PositionsRepository();
        this.delayComputationRedisRepository = new DelayComputationRedisRepository();

        const gtfsTripsRepository = new RopidGTFSTripsModel();
        this.delayComputationManager = new DelayComputationManager(gtfsTripsRepository);
        this.updateDelayLimiter = new UpdateDelayLimiter("ProcessRegionalBusPositionsTask", this.queueTtl);
        this.regionalBusPositionsManager = VPContainer.resolve<RegionalBusPositionsManager>(
            VPContainerToken.RegionalBusPositionsManager
        );
    }

    protected async execute(data: IUpdateDelayInput, msgProperties?: MessageProperties) {
        if (!this.updateDelayLimiter.isActualMessage(msgProperties)) {
            return; // skip processing of old messages
        }

        const tripIds: string[] = data.updatedTrips.map((trip) => trip.id);
        let gtfsComputationTrips: Array<IComputationTrip | null> = [];
        let tripsPositionsWithGtfsData: ITripPositionsWithOrWithoutGTFS[] = [];

        try {
            const tripsPositionsToUpdate = await this.positionsRepository.getPositionsForUpdateDelay(tripIds);
            gtfsComputationTrips = await this.delayComputationRedisRepository.mget<IComputationTrip>(
                // Append gtfs data from cache to each trip
                tripsPositionsToUpdate.map((trip) => trip.gtfs_trip_id)
            );

            for (let i = 0; i < tripsPositionsToUpdate.length; i++) {
                const trip = tripsPositionsToUpdate[i];
                const gtfsData = gtfsComputationTrips[i];
                tripsPositionsWithGtfsData.push({
                    ...trip,
                    gtfsData,
                });
            }

            const processedPositions = await Promise.all(
                tripsPositionsWithGtfsData.map(async (trip) => {
                    if (trip.gtfsData === null) {
                        trip = await this.enrichTripWithGtfsComputationData(trip);

                        if (trip.gtfsData === null) {
                            return null;
                        }
                    }

                    const computedPositions = this.regionalBusPositionsManager.computePositions(trip as ITripPositionsWithGTFS);
                    this.propagateKnownPositionProperties(trip, computedPositions);
                    return computedPositions;
                })
            );

            const validProcessedPositions = processedPositions.filter((e): e is IProcessedPositions => !!e);
            await this.updateComputedPositionsAndTrips(validProcessedPositions);

            await Promise.all([
                this.invalidateBacktrackedPositions(validProcessedPositions),
                this.expireDelayComputationCache(tripsPositionsWithGtfsData),
            ]);

            await QueueManager.sendMessageToExchange(this.queuePrefix, "propagateDelay", {
                processedPositions: validProcessedPositions,
                trips: data.updatedTrips,
            });
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            }

            throw new GeneralError(
                `Error while updating delay. For trip ids:${tripIds.slice(0, 10).join(",")}`,
                this.constructor.name,
                err
            );
        }
    }

    private enrichTripWithGtfsComputationData = async (
        trip: ITripPositionsWithOrWithoutGTFS
    ): Promise<ITripPositionsWithOrWithoutGTFS> => {
        try {
            const gtfsComputationData = await this.delayComputationManager.getComputationObject(trip.gtfs_trip_id);
            if (gtfsComputationData === null) {
                return trip;
            }

            await this.delayComputationRedisRepository.set("trip_id", gtfsComputationData, VPUtils.DELAY_COMPUTATION_CACHE_TTL);
            trip.gtfsData = gtfsComputationData;
        } catch (err) {
            if (err instanceof GeneralError) {
                this.logger.warn(err);
            } else {
                this.logger.error(
                    new GeneralError(
                        "Error while getting delay computation data for trip " + trip.gtfs_trip_id,
                        this.constructor.name,
                        err
                    )
                );
            }
        }

        return trip;
    };

    /**
     * Propagate known position properties from trips positions to processed positions, when the processed position property is
     * `undefined`
     *
     * @param tripsPositions Trips positions to propagate properties from
     * @param processedPositions Processed positions to propagate properties to
     */
    private propagateKnownPositionProperties = (
        tripPositions: ITripPositionsWithOrWithoutGTFS,
        processedPositions: IProcessedPositions
    ) => {
        for (let i = 0; i < processedPositions.positions.length; i++) {
            if (processedPositions.positions[i] === null) continue;
            processedPositions.positions[i] = {
                ...tripPositions.positions[i],
                ...processedPositions.positions[i],
            };
        }
    };

    private updateComputedPositionsAndTrips = async (processedPositions: IProcessedPositions[]): Promise<void> => {
        let positions: IPositionToUpdate[] = [];
        let trips = new Map<string, Partial<IVPTripsModel>>();

        for (const processedPosition of processedPositions) {
            positions = positions.concat(
                processedPosition.positions.filter((position) => position !== null) as IPositionToUpdate[]
            );
            trips.set(processedPosition.context.tripId, {
                id: processedPosition.context.tripId,
                last_position_id: processedPosition.context.lastPositionId ?? null,
                last_position_context: processedPosition.context,
                is_canceled: processedPosition.context.lastPositionCanceled,
            });
        }

        await this.positionsRepository.bulkSave(positions);
        await this.tripsRepository.bulkSave(Array.from(trips.values()));
    };

    /**
     * Expire delay computation cache for given trips
     */
    private async expireDelayComputationCache(tripPositions: ITripPositionsWithOrWithoutGTFS[]) {
        const tripIds: string[] = [];
        for (const trip of tripPositions) {
            if (trip.gtfsData) {
                tripIds.push(trip.gtfsData.trip_id);
            }
        }

        try {
            await this.delayComputationRedisRepository.expire(tripIds, VPUtils.DELAY_COMPUTATION_CACHE_TTL);
        } catch (err) {
            this.logger.error(err);
        }
    }

    /**
     * Invalidate positions after the last stop sequence of backtracked positions
     * We do this to improve historical data quality
     * as well as to prevent serving of incorrect stop times to API clients
     * (vehicle is expected to repeat some part of the trip)
     */
    private async invalidateBacktrackedPositions(processedPositions: IProcessedPositions[]) {
        const tripWithStopSequenceTuples: Array<[string, number]> = processedPositions
            .filter(({ isBacktrackingDetected }) => !!isBacktrackingDetected)
            .map(({ context, positions }) => {
                const tripId = context.tripId;
                const maxStopSequence = this.getMaxStopSequence(positions);
                return [tripId, maxStopSequence];
            });

        if (tripWithStopSequenceTuples.length === 0) {
            return;
        }

        const count = await this.positionsRepository.invalidatePositionsAfterStopSequence(tripWithStopSequenceTuples);
        this.logger.info(`Invalidated ${count} backtracked positions`);
    }

    private getMaxStopSequence(positions: Array<IPositionToUpdate | null>): number {
        const maxStopSequence = Math.max(...positions.map((position) => position?.last_stop_sequence ?? 0));
        if (maxStopSequence === 0) {
            throw new GeneralError("Max stop sequence must be greater than 0", this.constructor.name);
        }

        return maxStopSequence;
    }
}
