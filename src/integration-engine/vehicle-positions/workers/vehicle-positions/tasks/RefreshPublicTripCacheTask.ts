import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { PUBLIC_CACHE_NAMESPACE_PREFIX } from "#sch/vehicle-positions/redis/const";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { IoRedisConnector } from "@golemio/core/dist/helpers/data-access/redis/IoRedisConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger";
import { QueueManager } from "@golemio/core/dist/integration-engine";
import { RedisPubSubChannel } from "@golemio/core/dist/integration-engine/data-access/pubsub";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc";
import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine/workers";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { Mutex } from "@golemio/core/dist/shared/redis-semaphore";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { setTimeout as sleep } from "node:timers/promises";
import { WORKER_NAME } from "../constants";
import { TripsRepository } from "../data-access/TripsRepository";
import { PublicApiCacheRepository } from "../data-access/cache/PublicApiCacheRepository";
import { PublicApiTripTransformation } from "../transformations/PublicApiTripTransformation";
import { PublicCacheTripsFilter } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/public-api/PublicCacheTripsFilter";

@injectable()
export class RefreshPublicTripCacheTask extends AbstractEmptyTask {
    public readonly queueName = "refreshPublicTripCache";
    public readonly queueTtl = 10000; // 10s
    private lockTimeout: number;
    private refreshInterval: number;
    private tripRepository: TripsRepository;
    private redisChannel: RedisPubSubChannel;

    constructor(
        @inject(CoreToken.SimpleConfig) config: ISimpleConfig,
        @inject(CoreToken.Logger) private logger: ILogger,
        @inject(ContainerToken.RedisConnector) private redisClient: IoRedisConnector,
        @inject(VPContainerToken.PublicApiCacheRepository)
        private publicApiCacheRepository: PublicApiCacheRepository,
        @inject(VPContainerToken.PublicApiTripTransformation)
        private publicApiTripTransformation: PublicApiTripTransformation
    ) {
        super(WORKER_NAME);
        this.lockTimeout = config.getValue<number>(
            "old.datasources.pid.vehicle-positions.publicCache.mutexLockTimeout",
            3000
        ) as number; // default 3s
        this.refreshInterval = this.lockTimeout * 0.8;
        this.tripRepository = new TripsRepository();
        this.redisChannel = new RedisPubSubChannel(PUBLIC_CACHE_NAMESPACE_PREFIX);
    }

    protected async execute() {
        const startDateTime = new Date();
        const mutex = this.createMutex();
        const lockAcquired = await mutex.tryAcquire();

        if (!lockAcquired) {
            this.logger.info(`${this.constructor.name}: mutex lock was not acquired`);
            return;
        }

        try {
            const trips = await this.tripRepository.findAllForPublicCache(startDateTime);
            if (trips.length === 0) {
                return;
            }

            const { filteredTrips, futureTrips, canceledTrips } = PublicCacheTripsFilter.getDistinctLatestTrips(trips);

            const transformedTrips = this.publicApiTripTransformation.transformArray(filteredTrips);
            const transformedFutureTrips = this.publicApiTripTransformation.transformArray(futureTrips);
            const transformedCanceledTrips = this.publicApiTripTransformation.transformArray(canceledTrips);
            const newSetKey = await this.publicApiCacheRepository.geoadd(
                transformedTrips,
                transformedFutureTrips,
                transformedCanceledTrips
            );

            await this.redisChannel.publishMessage(newSetKey);
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            }

            throw new GeneralError(
                `${this.constructor.name}: error while refreshing public trip cache`,
                this.constructor.name,
                err
            );
        } finally {
            await mutex.release();
        }

        const ellapsedTimeInMs = new Date().getTime() - startDateTime.getTime();
        this.logger.info(`${this.constructor.name}: finished in ${ellapsedTimeInMs}ms`);

        if (ellapsedTimeInMs < this.refreshInterval) {
            await sleep(this.refreshInterval - ellapsedTimeInMs);
        }

        QueueManager.sendMessageToExchange(this.queuePrefix, this.queueName, {});
    }

    private createMutex() {
        return new Mutex(this.redisClient.getConnection(), this.queueName, {
            acquireAttemptsLimit: 1,
            lockTimeout: this.lockTimeout,
            refreshInterval: this.refreshInterval,
            onLockLost: (err) => {
                this.logger.error(err, `${this.constructor.name}: mutex lock was lost`);
            },
        });
    }
}
