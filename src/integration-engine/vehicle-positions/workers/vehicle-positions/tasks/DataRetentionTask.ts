import { PG_SCHEMA } from "#sch/const";
import { config, log, PostgresConnector, RedisConnector } from "@golemio/core/dist/integration-engine";
import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine/workers";
import { Mutex } from "@golemio/core/dist/shared/redis-semaphore";
import { QueryTypes, Sequelize } from "@golemio/core/dist/shared/sequelize";
import { DATA_RETENTION_IN_MINUTES } from "src/const";

interface IDataRetentionResult {
    numberofrows?: number | null;
}

export class DataRetentionTask extends AbstractEmptyTask {
    public readonly queueName = "dataRetention";
    public readonly queueTtl = 1 * 10 * 60 * 1000; // 10 minutes
    private readonly LOCKKEYPHRASE = "vehiclePositionsDataRetentionTask";
    private lockTimeout: number;
    private refreshInterval: number;
    private maintenanceEnabled;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.maintenanceEnabled = config.vehiclePositions.dataMaintenanceEnabled;
        this.lockTimeout = config.vehiclePositions.dataRetentionLockTimeout;
        this.refreshInterval = this.lockTimeout * 0.8; // recommended
    }

    protected async execute() {
        const mutex = this.createMutex();
        const lockAcquired = await mutex.tryAcquire();

        if (!lockAcquired) {
            log.error(`VP Worker - unable to obtain mutex lock.`);
            return;
        }

        try {
            const sequelize = PostgresConnector.getConnection();

            const result = await sequelize.query<IDataRetentionResult>(
                `CALL ${PG_SCHEMA}.vehiclepositions_data_retention(0,${DATA_RETENTION_IN_MINUTES});`,
                {
                    plain: true,
                    type: QueryTypes.SELECT,
                }
            );
            log.silly(`VP Worker - dataRention ${JSON.stringify(result?.numberofrows)} transformed to historic tables.`);

            if (!mutex.isAcquired) {
                log.warn(`VP Worker - mutex lock was lost after procedure vehiclepositions_data_retention.`);
                return;
            }

            await this.maintainTables(result, sequelize, mutex);
        } finally {
            await mutex.release();
        }
    }

    private createMutex() {
        const redisClient = RedisConnector.getConnection();
        return new Mutex(redisClient, this.LOCKKEYPHRASE, {
            acquireAttemptsLimit: 1,
            lockTimeout: this.lockTimeout,
            refreshInterval: this.refreshInterval,
            onLockLost: (err) => {
                log.warn(`VP Worker - ${err.message}`);
            },
        });
    }

    private async maintainTables(result: IDataRetentionResult | null, sequelize: Sequelize, mutex: Mutex) {
        if (this.maintenanceEnabled && result?.numberofrows) {
            log.silly(`VP Worker - dataRention starting maintenance`);
            const commands = this.getMaintenanceCommands();
            for (const command of commands) {
                await sequelize.query(command);
                log.silly(`VP Worker - ${command} done`);
                if (!mutex.isAcquired) {
                    log.warn(`VP Worker - mutex lock was lost after command: ${command}`);
                    return;
                }
            }
        }
    }

    private getMaintenanceCommands(): string[] {
        return [
            `REINDEX TABLE CONCURRENTLY ${PG_SCHEMA}.vehiclepositions_trips;`,
            `REINDEX TABLE CONCURRENTLY ${PG_SCHEMA}.vehiclepositions_positions;`,
        ];
    }
}
