import { BlockStopsRedisRepository } from "#ie/ropid-gtfs/data-access/cache/BlockStopsRedisRepository";
import { RopidGtfsContainer } from "#ie/ropid-gtfs/ioc/Di";
import { RopidGtfsContainerToken } from "#ie/ropid-gtfs/ioc/RopidGtfsContainerToken";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc";
import { Op } from "@golemio/core/dist/shared/sequelize";
import { IPropagateTrainDelayInput } from "../interfaces/IPropagateTrainDelayInput";
import { TripStopsSchedule } from "../interfaces/VPInterfaces";
import { PropagateDelayValidationSchema } from "../schema/PropagateTrainDelaySchema";
import { AbstractPropagateDelayTask } from "./abstract/AbstractPropagateDelayTask";

export class PropagateTrainDelayTask extends AbstractPropagateDelayTask<IPropagateTrainDelayInput> {
    public readonly queueName = "propagateTrainDelay";
    public readonly queueTtl = 5 * 60 * 1000; // 5 minutes
    public readonly schema = PropagateDelayValidationSchema;
    private logger: ILogger;
    private turnaroundEstimateInSeconds: number;
    protected config: ISimpleConfig;

    private readonly blockStopsRepository: BlockStopsRedisRepository;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.logger = IntegrationEngineContainer.resolve<ILogger>(CoreToken.Logger);
        this.blockStopsRepository = RopidGtfsContainer.resolve<BlockStopsRedisRepository>(
            RopidGtfsContainerToken.BlockStopsRedisRepository
        );

        this.config = IntegrationEngineContainer.resolve<ISimpleConfig>(CoreToken.SimpleConfig);
        this.turnaroundEstimateInSeconds = this.config.getValue<number>(
            "module.pid.vehicle-positions.gtfsTripRun.turnaroundEstimateInSeconds",
            0
        ) as number;
    }

    protected async execute(input: IPropagateTrainDelayInput): Promise<void> {
        for (const delayedPositions of input.data) {
            const { gtfs_trip_id, gtfs_block_id, vehicle_registration_number, start_timestamp, end_timestamp } =
                delayedPositions.trip;
            try {
                const nextGtfsTripId = await this.getNextGtfsTripId(gtfs_block_id, start_timestamp);

                if (!nextGtfsTripId) {
                    continue;
                }
                const blockedTrips = (await this.blockStopsRepository.get(gtfs_block_id!)) as TripStopsSchedule;

                const lastStopCurrentTrip = blockedTrips[gtfs_trip_id]?.trip_stops?.at(-1)?.stop_id;
                const firstStopNextTrip = blockedTrips[nextGtfsTripId]?.trip_stops?.[0]?.stop_id;
                const requiredTurnaroundSeconds =
                    lastStopCurrentTrip === firstStopNextTrip ? 0 : this.turnaroundEstimateInSeconds;

                await this.propagateDelayForGtfsTrip({
                    currentGtfsTripId: gtfs_trip_id,
                    currentGtfsBlockId: gtfs_block_id ?? null,
                    currentOriginTimestamp: delayedPositions.position.originTimestamp,
                    currentDelay: delayedPositions.position.delayInSeconds,
                    currentRegistrationNumber: vehicle_registration_number,
                    currentStartTime: start_timestamp,
                    currentEndTime: end_timestamp!,
                    currentBearing: delayedPositions.position.bearing!,
                    nextGtfsTrips: nextGtfsTripId
                        ? [
                              {
                                  trip_id: nextGtfsTripId,
                                  requiredTurnaroundSeconds: requiredTurnaroundSeconds,
                              },
                          ]
                        : [],
                    originPositionId: delayedPositions.position.originId,
                });
            } catch (err) {
                this.logger.warn(
                    `${this.queueName}: could not propagate train delay for GTFS trip: ${gtfs_trip_id} - ${err.message}`
                );
            }
        }
    }

    private getNextGtfsTripId = async (
        gtfs_block_id: string | undefined,
        start_timestamp: string
    ): Promise<string | undefined> => {
        const trip = await this.tripsRepository.findOne({
            attributes: ["gtfs_trip_id"],
            where: {
                gtfs_block_id: gtfs_block_id,
                start_timestamp: { [Op.gt]: start_timestamp },
            },
            order: [["start_timestamp", "ASC"]],
            raw: true,
        });

        return trip?.gtfs_trip_id;
    };
}
