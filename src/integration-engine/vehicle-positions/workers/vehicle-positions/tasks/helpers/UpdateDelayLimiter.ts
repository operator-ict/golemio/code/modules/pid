import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import { MessageProperties } from "amqplib";

/* Helper to decide whether initial message is older than the queue TTL, do not continue with processing it.
 * It is expected that older positions will be processed with more actual data.
 */
export class UpdateDelayLimiter {
    private static readonly MAX_SKIPPED_ITERATIONS = 100;
    private skippedIterations = 0;
    private logger: ILogger;

    constructor(private name: string, private queueTtl: number, logger?: ILogger) {
        this.logger = logger ?? IntegrationEngineContainer.resolve<ILogger>(CoreToken.Logger);
    }

    public isActualMessage(messageProperties: MessageProperties | undefined): boolean {
        if (!messageProperties) {
            this.logger.warn(`[UpdateDelayLimiter] Missing message properties for ${this.name}`);

            return true;
        } else if (!messageProperties.timestamp) {
            this.logger.warn(`[UpdateDelayLimiter] Missing message timestamp for ${this.name}`);

            return true;
        } else if (new Date().getTime() - (messageProperties.timestamp as number) > this.queueTtl) {
            this.skippedIterations++;
            if (this.skippedIterations > UpdateDelayLimiter.MAX_SKIPPED_ITERATIONS) {
                this.logger.warn(`[UpdateDelayLimiter] Skipped processing of update delay task ${this.skippedIterations} times.`);
                this.skippedIterations = 0;
            }

            return false;
        }

        return true;
    }
}
