import { PG_SCHEMA } from "#sch/const";
import { CisStopModel } from "#sch/vehicle-positions/models/CisStopModel";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { Op, Sequelize } from "@golemio/core/dist/shared/sequelize";

export class CisStopRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            "VPCisStopRepository",
            {
                pgTableName: CisStopModel.tableName,
                pgSchema: PG_SCHEMA,
                outputSequelizeAttributes: CisStopModel.attributeModel,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator("VPCisStopRepositoryValidator", CisStopModel.arrayJsonSchema)
        );
    }

    public deleteNHoursOldData = async (hours: number): Promise<number> => {
        try {
            return await this.sequelizeModel.destroy({
                where: {
                    updated_at: {
                        [Op.lt]: Sequelize.literal(`NOW() - INTERVAL '${hours} HOURS'`),
                    },
                },
            });
        } catch (err) {
            throw new GeneralError("Error while purging old data", this.constructor.name, err);
        }
    };
}
