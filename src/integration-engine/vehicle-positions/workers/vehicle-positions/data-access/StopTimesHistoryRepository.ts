import { PG_SCHEMA } from "#sch/const";
import { VehiclePositions } from "#sch/vehicle-positions";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import Sequelize, { Op } from "@golemio/core/dist/shared/sequelize";

export class StopTimesHistoryRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            "VPStopTimesHistoryRepository",
            {
                outputSequelizeAttributes: {},
                pgTableName: VehiclePositions.stopTimes.pgTableNameHistory,
                pgSchema: PG_SCHEMA,
                savingType: "insertOnly",
            },
            new JSONSchemaValidator(VehiclePositions.stopTimes.name + "Validator", {})
        );
    }

    public deleteNHoursOldData = async (hours: number): Promise<number> => {
        try {
            return await this.sequelizeModel.destroy({
                where: {
                    updated_at: {
                        [Op.lt]: Sequelize.literal(`NOW() - INTERVAL '${hours} HOURS'`),
                    },
                },
            });
        } catch (err) {
            throw new GeneralError("Error while purging old data", this.constructor.name, err);
        }
    };
}
