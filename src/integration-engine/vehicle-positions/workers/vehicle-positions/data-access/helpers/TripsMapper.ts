import { DateTimeUtils } from "#helpers/DateTimeUtils";
import { IExtendedMetroRunInputForProcessing } from "#ie/vehicle-positions/workers/runs/interfaces/IMetroRunInputForProcessing";
import { IRegionalBusRunInputWithMetadata } from "#ie/vehicle-positions/workers/runs/interfaces/IRegionalBusRunInputWithMetadata";
import DPPUtils from "#ie/vehicle-positions/workers/vehicle-positions/helpers/DPPUtils";
import { ICommonRunsModel } from "#sch/vehicle-positions/models/interfaces/ICommonRunsModel";
import { IVPTripsModel } from "#sch/vehicle-positions/models/interfaces/IVPTripsModel";
import { IScheduleDto } from "#sch/vehicle-positions/redis/interfaces/IGtfsRunTripCacheDto";
import { BulkSaveResult } from "@golemio/core/dist/integration-engine";
import { GTFSRouteTypeEnum, MPVRouteTypesEnum } from "src/helpers/RouteTypeEnums";
import { ProviderSourceTypeEnum } from "../../helpers/ProviderSourceTypeEnum";
import { IBulkUpsertOutput } from "../interfaces/TripRepositoryInterfaces";
import VPTripsIdGenerator from "./TripsIdGenerator";

export default class TripsMapper {
    public static mapCommonRunToDto(gtfsTrip: IScheduleDto, runInput: ICommonRunsModel): Partial<IVPTripsModel> {
        return {
            ...this.getGTFSAttributes(gtfsTrip),
            id: VPTripsIdGenerator.generateFromCommonRun(gtfsTrip, runInput),
            origin_route_name: runInput.route_id,
            internal_route_name: runInput.route_id,
            run_number: runInput.run_number,
            internal_run_number: runInput.run_number,
            vehicle_registration_number: parseInt(runInput.registration_number),
            vehicle_type_id: this.getVehicleTypeId(
                +gtfsTrip.route_type,
                !!+gtfsTrip.is_night,
                !!+gtfsTrip.is_regional,
                !!+gtfsTrip.is_substitute_transport
            ),
            wheelchair_accessible: runInput.wheelchair_accessible,
            provider_source_type: ProviderSourceTypeEnum.TcpCommon,
        };
    }

    public static mapCommonRunNotPublicToDto(
        runInput: ICommonRunsModel,
        tripId: string,
        lastPositionId: string
    ): Partial<IVPTripsModel> {
        return {
            agency_name_real: DPPUtils.DPP_AGENCY_NAME,
            agency_name_scheduled: DPPUtils.DPP_AGENCY_NAME,
            gtfs_route_type: GTFSRouteTypeEnum.TRAM,
            origin_route_name: runInput.route_id,
            internal_route_name: runInput.route_id,
            run_number: runInput.run_number,
            internal_run_number: runInput.run_number,
            vehicle_registration_number: parseInt(runInput.registration_number),
            vehicle_type_id: MPVRouteTypesEnum.TRAM,
            wheelchair_accessible: runInput.wheelchair_accessible,
            provider_source_type: ProviderSourceTypeEnum.TcpCommon,
            id: tripId,
            last_position_id: lastPositionId,
        };
    }

    public static mapMetroRunToDto(
        gtfsTrip: IScheduleDto,
        runInput: IExtendedMetroRunInputForProcessing
    ): Partial<IVPTripsModel> {
        return {
            ...this.getGTFSAttributes(gtfsTrip),
            id: VPTripsIdGenerator.generateFromMetroRun(gtfsTrip, runInput),
            origin_route_name: runInput.routeId,
            internal_route_name: runInput.routeId,
            run_number: runInput.runNumber,
            internal_run_number: runInput.internalRunNumber ?? runInput.runNumber,
            vehicle_type_id: MPVRouteTypesEnum.METRO,
            wheelchair_accessible: true, // metro vehicles themselves are always wheelchair accessible
            provider_source_type: ProviderSourceTypeEnum.TcpMetro,
        };
    }

    public static mapRegionalBusRunToDto(
        gtfsTrip: IScheduleDto,
        runInput: IRegionalBusRunInputWithMetadata
    ): Partial<IVPTripsModel> {
        return {
            ...this.getGTFSAttributes(gtfsTrip),
            id: VPTripsIdGenerator.generateFromRegionalBusRun(gtfsTrip, runInput.runMessage),
            agency_name_real: runInput.agencyName,
            agency_name_scheduled: runInput.agencyName,
            cis_line_id: runInput.runMessage.cis_line_id,
            cis_trip_number: runInput.runMessage.cis_trip_number,
            origin_route_name: gtfsTrip.origin_route_name,
            internal_route_name: gtfsTrip.origin_route_name,
            run_number: gtfsTrip.run_number,
            vehicle_registration_number: runInput.runMessage.registration_number,
            vehicle_type_id: this.getVehicleTypeId(
                +gtfsTrip.route_type,
                !!+gtfsTrip.is_night,
                !!+gtfsTrip.is_regional,
                !!+gtfsTrip.is_substitute_transport
            ),
            wheelchair_accessible: runInput.isWheelchairAccessible,
            provider_source_type: ProviderSourceTypeEnum.TcpRegionalBus,
        };
    }

    public static mapTripEntitiesToUpdateDto(
        tripEntities: BulkSaveResult<IVPTripsModel>,
        taskStartTimestamp: number
    ): IBulkUpsertOutput {
        const output: IBulkUpsertOutput = {
            inserted: [],
            updated: [],
        };

        for (const tripEntity of tripEntities) {
            const tripCreatedTimestamp = new Date(tripEntity.get("created_at") as string).getTime();
            if (tripCreatedTimestamp > taskStartTimestamp) {
                output.inserted.push({
                    cis_line_short_name: tripEntity.cis_line_short_name,
                    id: tripEntity.id,
                    start_asw_stop_id: tripEntity.start_asw_stop_id,
                    start_cis_stop_id: tripEntity.start_cis_stop_id,
                    start_cis_stop_platform_code: tripEntity.start_cis_stop_platform_code,
                    start_timestamp: tripEntity.start_timestamp?.toISOString() ?? null,
                    agency_name_real: tripEntity.agency_name_real,
                    agency_name_scheduled: tripEntity.agency_name_scheduled,
                    cis_line_id: tripEntity.cis_line_id,
                    cis_trip_number: tripEntity.cis_trip_number,
                    origin_route_name: tripEntity.origin_route_name,
                    run_number: tripEntity.run_number,
                    start_time: tripEntity.start_time,
                    vehicle_registration_number: tripEntity.vehicle_registration_number,
                    vehicle_type_id: tripEntity.vehicle_type_id,
                    wheelchair_accessible: tripEntity.wheelchair_accessible,
                });
            } else {
                output.updated.push({
                    id: tripEntity.id,
                    origin_route_name: tripEntity.origin_route_name,
                    run_number: tripEntity.run_number,
                    start_timestamp: tripEntity.start_timestamp,
                });
            }
        }

        return output;
    }

    private static getGTFSAttributes(gtfsTrip: IScheduleDto): Partial<IVPTripsModel> {
        return {
            agency_name_real: DPPUtils.DPP_AGENCY_NAME,
            agency_name_scheduled: DPPUtils.DPP_AGENCY_NAME,
            gtfs_block_id: gtfsTrip.block_id,
            gtfs_route_id: gtfsTrip.route_id,
            gtfs_route_short_name: gtfsTrip.route_short_name,
            gtfs_route_type: gtfsTrip.route_type,
            gtfs_trip_headsign: gtfsTrip.trip_headsign,
            gtfs_trip_id: gtfsTrip.trip_id,
            gtfs_date: gtfsTrip.date,
            gtfs_direction_id: gtfsTrip.direction_id,
            gtfs_shape_id: gtfsTrip.shape_id,
            start_time: DateTimeUtils.parseUTCTimeFromISO(gtfsTrip.start_timestamp),
            start_timestamp: new Date(gtfsTrip.start_timestamp),
            end_timestamp: new Date(gtfsTrip.end_timestamp),
        };
    }

    private static getVehicleTypeId = (
        routeType: number,
        isNight: boolean,
        isRegional: boolean,
        isSubstitute: boolean
    ): number => {
        let id;
        switch (routeType) {
            case GTFSRouteTypeEnum.TRAM:
                if (isNight) {
                    id = MPVRouteTypesEnum.TRAM_NIGHT;
                } else if (isSubstitute) {
                    id = MPVRouteTypesEnum.TRAM_SUBSTITUTE;
                } else {
                    id = MPVRouteTypesEnum.TRAM;
                }
                break;
            case GTFSRouteTypeEnum.METRO:
                id = MPVRouteTypesEnum.METRO;
                break;
            case GTFSRouteTypeEnum.TRAIN:
                id = MPVRouteTypesEnum.TRAIN;
                break;
            case GTFSRouteTypeEnum.BUS:
                if (isNight && isRegional) {
                    id = MPVRouteTypesEnum.BUS_NIGHT_REGIONAL;
                } else if (isNight) {
                    id = MPVRouteTypesEnum.BUS_NIGHT;
                } else if (isRegional) {
                    id = MPVRouteTypesEnum.BUS_REGIONAL;
                } else if (isSubstitute) {
                    id = MPVRouteTypesEnum.BUS_SUBSTITUTE;
                } else {
                    id = MPVRouteTypesEnum.BUS_CITY;
                }
                break;
            case GTFSRouteTypeEnum.FERRY:
                id = MPVRouteTypesEnum.FERRY;
                break;
            case GTFSRouteTypeEnum.FUNICULAR:
                id = MPVRouteTypesEnum.FUNICULAR;
                break;
            case GTFSRouteTypeEnum.TROLLEYBUS:
                id = MPVRouteTypesEnum.TROLLEYBUS;
                break;
            default:
                throw new Error("Unexpected routeType");
        }
        return id;
    };
}
