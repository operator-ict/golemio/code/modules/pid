import { DateTimeUtils } from "#helpers/DateTimeUtils";
import { PG_SCHEMA } from "#sch/const";
import { RopidGTFSPrecomputed } from "#sch/ropid-gtfs/RopidGTFSPrecomputed";
import { RopidGTFSSchedule } from "#sch/ropid-gtfs/RopidGtfsSchedule";
import { VPTripsModel } from "#sch/vehicle-positions/models/VPTripsModel";
import { TripWithLastPositionModel } from "#sch/vehicle-positions/models/views/TripWithLastPositionModel";
import { StatePositionEnum } from "src/const";

export class RawQueryProvider {
    public static getFindAssociatedTripsQuery(): string {
        return `
            SELECT id,
                gtfs_block_id, gtfs_trip_id, gtfs_route_type,
                start_timestamp, end_timestamp,
                run_number, internal_run_number,
                origin_route_name, internal_route_name,
                vehicle_registration_number
            FROM "${PG_SCHEMA}".${VPTripsModel.TABLE_NAME}
            WHERE id LIKE any (('{'||$tripIds||'}')::text[]);
        `;
    }

    public static getFindGtfsCommonTripsQuery(): string {
        /* eslint-disable max-len */
        return `
          SET LOCAL search_path TO ${PG_SCHEMA};
            SELECT DISTINCT
                ropidgtfs_trips.trip_id AS gtfs_trip_id,
                ropidgtfs_trips.block_id AS gtfs_block_id,
                ropidgtfs_trips.direction_id AS gtfs_direction_id,
                ropidgtfs_trips.shape_id AS gtfs_shape_id,
                ropidgtfs_precomputed_trip_schedule.date AS gtfs_date,
                ropidgtfs_trips.trip_headsign AS gtfs_trip_headsign,
                ropidgtfs_routes.route_id AS gtfs_route_id,
                ropidgtfs_routes.route_type AS gtfs_route_type,
                ropidgtfs_routes.route_short_name AS gtfs_route_short_name,
                ropidgtfs_precomputed_minmax_stop_sequences.min_stop_time,
                ropidgtfs_precomputed_minmax_stop_sequences.max_stop_time,
                ropidgtfs_precomputed_trip_schedule.run_number AS internal_run_number,
                ropidgtfs_precomputed_trip_schedule.origin_route_name AS internal_route_name
            FROM ropidgtfs_trips
            INNER JOIN ropidgtfs_routes 
                ON ropidgtfs_trips.route_id = ropidgtfs_routes.route_id
            INNER JOIN ropidgtfs_stop_times 
                ON ropidgtfs_trips.trip_id = ropidgtfs_stop_times.trip_id
            LEFT JOIN ropidgtfs_precomputed_trip_schedule
                ON ropidgtfs_trips.trip_id = ropidgtfs_precomputed_trip_schedule.trip_id
                AND ropidgtfs_precomputed_trip_schedule.start_timestamp::date = current_date
            LEFT JOIN ropidgtfs_precomputed_minmax_stop_sequences 
                ON ropidgtfs_trips.trip_id = ropidgtfs_precomputed_minmax_stop_sequences.trip_id
            WHERE ropidgtfs_routes.route_short_name LIKE :cisLineShortName
                AND ropidgtfs_stop_times.stop_id = ANY(STRING_TO_ARRAY(get_stop_ids_from_cis(:startCisStopId, :startAswStopId, :startCisStopPlatformCode), ','))    
                AND stop_sequence = '1'
                AND normalize_departure_time(ropidgtfs_stop_times.departure_time) = TO_CHAR(
                    (:startDateFormatted at time zone 'Europe/Prague'), 'FMHH24:MI:SS'
                )
                AND (
                    CASE 
                        WHEN SUBSTRING(LPAD(ropidgtfs_stop_times.departure_time, 8, '0'), 1, 2)::int < 24 THEN
                            ropidgtfs_trips.service_id IN (
                               select service_id from get_service_ids_from_ropidgtfs_calendar(:startDateDayName, :startDateYMD)
                            )
                        ELSE
                            ropidgtfs_trips.service_id IN (
                               select service_id from get_service_ids_from_ropidgtfs_calendar(:startDateDayBeforeDayName, :startDateDayBeforeYMD)
                            )
                    END
                )
            ORDER BY gtfs_trip_id ASC;
        `;
        /* eslint-enable max-len */
    }

    public static getFindGtfsTrainTripsQuery(): string {
        /* eslint-disable max-len */
        return `
            SET LOCAL search_path TO ${PG_SCHEMA};
            SELECT DISTINCT
                ropidgtfs_trips.trip_id AS gtfs_trip_id,
                ropidgtfs_trips.direction_id AS gtfs_direction_id,
                psc.date AS gtfs_date,
                ropidgtfs_trips.shape_id AS gtfs_shape_id,
                ropidgtfs_trips.block_id AS gtfs_block_id,
                ropidgtfs_trips.trip_headsign AS gtfs_trip_headsign,
                ropidgtfs_trips.trip_short_name AS gtfs_trip_short_name,
                ropidgtfs_routes.route_id AS gtfs_route_id,
                ropidgtfs_routes.route_type AS gtfs_route_type,
                ropidgtfs_routes.route_short_name AS gtfs_route_short_name,
                wanted_trip.cis_trip_number,
                ropidgtfs_precomputed_minmax_stop_sequences.min_stop_time,
                ropidgtfs_precomputed_minmax_stop_sequences.max_stop_time,
                NULL AS internal_run_number,
                NULL AS internal_route_name
            FROM 
                ropidgtfs_trips
            INNER JOIN 
                get_wanted_trip(:tripId) AS wanted_trip ON 1 = 1
            INNER JOIN 
                ropidgtfs_routes ON ropidgtfs_trips.route_id = ropidgtfs_routes.route_id
            INNER JOIN 
                ropidgtfs_stop_times ON ropidgtfs_trips.trip_id = ropidgtfs_stop_times.trip_id
            LEFT JOIN 
                ropidgtfs_precomputed_services_calendar psc ON 
                    psc.service_id = ropidgtfs_trips.service_id
                    AND psc.date IN (current_date, current_date - 1)
            LEFT JOIN 
                ropidgtfs_precomputed_minmax_stop_sequences ON 
                    ropidgtfs_trips.trip_id = ropidgtfs_precomputed_minmax_stop_sequences.trip_id
            WHERE
                match_trip_id(ropidgtfs_trips.trip_id, wanted_trip.cis_trip_number)
                AND ropidgtfs_routes.route_type = '2'
                AND stop_sequence = 1
                AND is_within_12_hours(
                    psc.date,
                    ropidgtfs_precomputed_minmax_stop_sequences.min_stop_time,
                    wanted_trip.start_timestamp
                ) -- 12 hours, ensure correct GTFS date is selected by recalculating expected start_timestamp and comparing with wanted_trip
                AND (
                    CASE 
                        WHEN SUBSTRING(LPAD(ropidgtfs_stop_times.departure_time, 8, '0'), 1, 2)::int < 24 THEN -- If departure_time is less then 24 hours
                            ropidgtfs_trips.service_id IN (
                                SELECT service_id 
                                FROM get_service_ids_from_ropidgtfs_calendar(
                                    wanted_trip.start_date_name,
                                    wanted_trip.start_date
                                )
                            )
                        ELSE
                            ropidgtfs_trips.service_id IN (
                                SELECT service_id 
                                FROM get_service_ids_from_ropidgtfs_calendar(
                                    wanted_trip.start_date_day_before_name,
                                    wanted_trip.start_date_day_before
                                )
                            )
                    END
                )
            ORDER BY 
                gtfs_trip_id ASC;
        `;
        /* eslint-enable max-len */
    }

    public static getUpdateGtfsTripDataQuery(): string {
        return `
            update "${PG_SCHEMA}".${VPTripsModel.TABLE_NAME} vp_trip
            set
                gtfs_block_id = trip_with_gtfs_data.gtfs_block_id,
                gtfs_route_id = trip_with_gtfs_data.gtfs_route_id,
                gtfs_route_short_name = trip_with_gtfs_data.gtfs_route_short_name,
                gtfs_route_type = trip_with_gtfs_data.gtfs_route_type,
                gtfs_trip_headsign = trip_with_gtfs_data.gtfs_trip_headsign,
                gtfs_direction_id = trip_with_gtfs_data.gtfs_direction_id,
                gtfs_shape_id = trip_with_gtfs_data.gtfs_shape_id,
                start_time = trip_with_gtfs_data.start_time,
                start_timestamp = trip_with_gtfs_data.start_timestamp,
                end_timestamp = trip_with_gtfs_data.end_timestamp,
                internal_run_number = trip_with_gtfs_data.internal_run_number,
                internal_route_name = trip_with_gtfs_data.internal_route_name,
                updated_at = now()
            from (
                select
                    vp_trip_with_position.id,
                    gtfs_trip.block_id as gtfs_block_id,
                    gtfs_trip.route_id as gtfs_route_id,
                    gtfs_trip.route_short_name as gtfs_route_short_name,
                    gtfs_trip.route_type as gtfs_route_type,
                    gtfs_trip.trip_headsign as gtfs_trip_headsign,
                    gtfs_trip.direction_id as gtfs_direction_id,
                    gtfs_trip.shape_id as gtfs_shape_id,
                    gtfs_trip.start_timestamp::time at time zone '${DateTimeUtils.TIMEZONE}' as start_time,
                    gtfs_trip.start_timestamp,
                    gtfs_trip.end_timestamp,
                    gtfs_trip.run_number as internal_run_number,
                    gtfs_trip.origin_route_name as internal_route_name
                from "${PG_SCHEMA}".${TripWithLastPositionModel.tableName} vp_trip_with_position
                inner join "${PG_SCHEMA}".${RopidGTFSPrecomputed.tripSchedule.pgTableName} gtfs_trip
                    on vp_trip_with_position.gtfs_trip_id = gtfs_trip.trip_id
                    and gtfs_trip.start_timestamp::date = current_date
                where
                    vp_trip_with_position.state_position != '${StatePositionEnum.AFTER_TRACK}'
                    and vp_trip_with_position.state_position != '${StatePositionEnum.AFTER_TRACK_DELAYED}'
            ) as trip_with_gtfs_data
            where vp_trip.id = trip_with_gtfs_data.id;
        `;
    }

    public static getFindTripsWithInvalidGtfsTripIdQuery(): string {
        return `
            select
                id,
                cis_line_short_name,
                start_asw_stop_id,
                start_cis_stop_id,
                start_cis_stop_platform_code,
                start_timestamp,
                agency_name_real,
                agency_name_scheduled,
                cis_line_id,
                cis_trip_number,
                origin_route_name,
                run_number,
                start_time,
                vehicle_registration_number,
                vehicle_type_id,
                wheelchair_accessible
            from
                "${PG_SCHEMA}".${VPTripsModel.TABLE_NAME} vt
            where
                vt.gtfs_trip_id is not null
                and vt.provider_source_type = $providerSourceType
                and not exists (
                    select
                        null
                    from
                        "${PG_SCHEMA}".${RopidGTFSSchedule.trips.pgTableName} rt
                    where
                        rt.trip_id = vt.gtfs_trip_id
                )
        `;
    }
}
