import { DateTimeUtils } from "#helpers/DateTimeUtils";
import { IMetroRunInputForProcessing } from "#ie/vehicle-positions/workers/runs/interfaces/IMetroRunInputForProcessing";
import { IProcessRegionalBusRunMessage } from "#ie/vehicle-positions/workers/runs/interfaces/IProcessRegionalBusRunMessagesInput";
import { ICommonRunsModel } from "#sch/vehicle-positions/models/interfaces/ICommonRunsModel";
import { IPositionDto } from "#sch/vehicle-positions/models/interfaces/IPositionDto";
import { IScheduleDto } from "#sch/vehicle-positions/redis/interfaces/IGtfsRunTripCacheDto";
import { StatePositionEnum, StateProcessEnum, TCPEventEnum } from "src/const";
import { IRunMessageWithStringTimestamps } from "../../interfaces/IUpdateRunsGtfsTripInput";
import VPTripsIdGenerator from "./TripsIdGenerator";

export default class PositionsMapper {
    public static mapCommonRunToDto(
        runMessage: IRunMessageWithStringTimestamps,
        isCurrent: boolean,
        gtfsTrip: IScheduleDto,
        runInput: ICommonRunsModel
    ): Partial<IPositionDto> {
        const scheduledTimestamp = runMessage.actual_stop_timestamp_scheduled
            ? new Date(runMessage.actual_stop_timestamp_scheduled)
            : null;

        const isTerminated =
            runMessage.events === TCPEventEnum.TERMINATED ||
            (scheduledTimestamp ? scheduledTimestamp >= new Date(gtfsTrip.end_timestamp) : false);

        return {
            asw_last_stop_id: runMessage.last_stop_asw_id,
            lat: runMessage.lat,
            lng: runMessage.lng,
            origin_time: DateTimeUtils.parseUTCTimeFromISO(runMessage.actual_stop_timestamp_real),
            origin_timestamp: new Date(runMessage.actual_stop_timestamp_real),
            scheduled_timestamp: scheduledTimestamp,
            state_position: StatePositionEnum.UNKNOWN,
            state_process: StateProcessEnum.TCP_INPUT,
            is_tracked: isCurrent && !isTerminated,
            trips_id: VPTripsIdGenerator.generateFromCommonRun(gtfsTrip, runInput),
            tcp_event: runMessage.events,
        };
    }

    public static mapCommonRunNotPublicToDto(runMessage: IRunMessageWithStringTimestamps, tripId: string): Partial<IPositionDto> {
        return {
            asw_last_stop_id: runMessage.last_stop_asw_id,
            lat: runMessage.lat,
            lng: runMessage.lng,
            origin_time: DateTimeUtils.parseUTCTimeFromISO(runMessage.actual_stop_timestamp_real),
            origin_timestamp: new Date(runMessage.actual_stop_timestamp_real),
            state_position: StatePositionEnum.NOT_PUBLIC,
            state_process: StateProcessEnum.PROCESSED,
            is_tracked: true,
            trips_id: tripId,
            tcp_event: runMessage.events,
        };
    }

    public static mapMetroRunToDto(
        isCurrent: boolean,
        gtfsTrip: IScheduleDto,
        runInput: IMetroRunInputForProcessing
    ): Partial<IPositionDto> {
        const scheduledTimestamp = new Date(runInput.timestampScheduled);
        const isTerminated = scheduledTimestamp >= new Date(gtfsTrip.end_timestamp);
        const beforeTrack = scheduledTimestamp < new Date(gtfsTrip.start_timestamp);

        const is_tracked = beforeTrack ? false : isCurrent && !isTerminated;

        return {
            lat: runInput.coordinates.lat,
            lng: runInput.coordinates.lon,
            origin_time: DateTimeUtils.parseUTCTimeFromISO(runInput.messageTimestamp),
            origin_timestamp: new Date(runInput.messageTimestamp),
            scheduled_timestamp: scheduledTimestamp,
            state_position: StatePositionEnum.UNKNOWN,
            state_process: StateProcessEnum.TCP_INPUT,
            this_stop_id: runInput.coordinates.gtfs_stop_id,
            is_tracked,
            trips_id: VPTripsIdGenerator.generateFromMetroRun(gtfsTrip, runInput),
        };
    }

    public static mapRegionalBusRunToDto(
        isCurrent: boolean,
        gtfsTrip: IScheduleDto,
        runMessage: IProcessRegionalBusRunMessage
    ): Partial<IPositionDto> {
        const isBeforeTrack = new Date() < new Date(gtfsTrip.start_timestamp);
        const isTracked = isCurrent && !isBeforeTrack && !runMessage.is_terminated;

        return {
            trips_id: VPTripsIdGenerator.generateFromRegionalBusRun(gtfsTrip, runMessage),
            bearing: runMessage.bearing,
            lat: runMessage.coordinates.coordinates[1],
            lng: runMessage.coordinates.coordinates[0],
            origin_time: DateTimeUtils.parseUTCTimeFromISO(runMessage.vehicle_timestamp as string),
            origin_timestamp: new Date(runMessage.vehicle_timestamp as string),
            state_position: StatePositionEnum.UNKNOWN,
            state_process: StateProcessEnum.TCP_INPUT,
            speed: runMessage.speed_kmh,
            is_tracked: isTracked,
            tcp_event: runMessage.events as TCPEventEnum,
        };
    }
}
