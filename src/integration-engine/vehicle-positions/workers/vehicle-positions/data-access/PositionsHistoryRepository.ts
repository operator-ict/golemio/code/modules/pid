import { PG_SCHEMA } from "#sch/const";
import { VehiclePositions } from "#sch/vehicle-positions";
import { PositionDto } from "#sch/vehicle-positions/models/PositionDto";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import Sequelize, { Op } from "@golemio/core/dist/shared/sequelize";

export class PositionsHistoryRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            "VPPositionsHistoryRepository",
            {
                outputSequelizeAttributes: PositionDto.attributeModel,
                pgTableName: VehiclePositions.positions.pgTableNameHistory,
                pgSchema: PG_SCHEMA,
                savingType: "insertOnly",
                sequelizeAdditionalSettings: {
                    indexes: [
                        {
                            fields: ["trips_id"],
                            name: "vehiclepositions_positions_trips_id",
                        },
                        {
                            fields: ["origin_time"],
                            name: "vehiclepositions_positions_origin_time",
                        },
                    ],
                },
            },
            new JSONSchemaValidator(VehiclePositions.positions.name + "ModelValidator", PositionDto.jsonSchema)
        );
    }

    public deleteNHoursOldData = async (hours: number): Promise<number> => {
        try {
            return await this.sequelizeModel.destroy({
                where: {
                    updated_at: {
                        [Op.lt]: Sequelize.literal(`NOW() - INTERVAL '${hours} HOURS'`),
                    },
                },
            });
        } catch (err) {
            throw new GeneralError("Error while purging old data", this.constructor.name, err);
        }
    };
}
