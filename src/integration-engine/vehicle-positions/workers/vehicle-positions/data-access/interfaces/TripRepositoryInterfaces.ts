import { GTFSRouteTypeEnum } from "#helpers/RouteTypeEnums";
import { IVPTripsModel } from "#sch/vehicle-positions/models/interfaces/IVPTripsModel";
import { StatePositionEnum } from "src/const";
import { ProviderSourceTypeEnum } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/ProviderSourceTypeEnum";

export interface IUpdateDelayTripsIdsData {
    id: string;
    gtfs_trip_id?: string;
    gtfs_block_id?: string;
    gtfs_route_type?: number;
    gtfs_direction_id?: number;
    gtfs_shape_id?: string;
    start_timestamp: string;
    end_timestamp?: string;
    run_number?: number | null;
    internal_run_number?: number | null;
    origin_route_name?: string | null;
    internal_route_name?: string | null;
    vehicle_registration_number?: number | null;
}

export interface IUpdateDelayRunTripsData extends IUpdateDelayTripsIdsData {
    gtfs_trip_id: string;
    internal_route_name: string | null;
    internal_run_number: number | null;
    vehicle_registration_number: number;
}

export interface IUpdateGTFSTripIdData extends Omit<Partial<IVPTripsModel>, "start_timestamp"> {
    id: string;
    start_timestamp: string | null;
    run_number: number | null;
    origin_route_name: string | null;
    vehicle_registration_number: number | null;
}

export interface IFoundGTFSTripData {
    id?: string;
    gtfs_trip_id: string;
    gtfs_trip_headsign: string;
    gtfs_route_id: string;
    gtfs_route_short_name: string;
    gtfs_direction_id: number;
    gtfs_shape_id: string;
    min_stop_time: string;
    max_stop_time: string;

    gtfs_block_id?: string;
    gtfs_trip_short_name?: string;
    gtfs_route_type?: number;
    cis_trip_number?: number;
    internal_run_number?: number;
    internal_route_name?: string;
}

export interface IBulkUpsertOutput {
    inserted: IUpdateGTFSTripIdData[];
    updated: Array<{
        id: string;
        origin_route_name: string | null;
        run_number: number | null;
        start_timestamp: Date | null;
    }>;
}

export interface IPropagatedDelayTripWithPosition {
    position: {
        id: string;
        delay: number;
        state_position: StatePositionEnum;
        origin_timestamp: Date;
    };
    trip: {
        id: string;
    };
}

export interface IEnrichedTripForPublicApi {
    id: string;
    gtfs_trip_id: string;
    gtfs_route_type: GTFSRouteTypeEnum;
    provider_source_type: ProviderSourceTypeEnum;
    gtfs_route_short_name: string;
    cis_trip_number: number | null;
    lat: string;
    lng: string;
    bearing: number | null;
    delay: number | null;
    state_position: StatePositionEnum;
    created_at: Date;
    // Additional properties for detailed vehiclepositions in public API
    wheelchair_accessible: boolean | null;
    vehicle_registration_number: number | null;
    agency_name_scheduled: string | null;
    agency_name_real: string | null;
    origin_route_name: string | null;
    gtfs_shape_id: string | null;
    run_number: number | null;
    internal_run_number: number | null;
    last_stop_headsign: string | null;
    gtfs_trip_headsign: string;
    shape_dist_traveled: string | null;
    last_stop_sequence: number | null;
    origin_timestamp: Date;
}
