import { VPTripsNonNullableModel } from "#sch/vehicle-positions/models/interfaces/IVPTripsModel";
import { IVPTripsPositionAttributes } from "#sch/vehicle-positions/models/interfaces/IVPTripsPositionAttributes";

export interface ITripAttributes
    extends Pick<
        VPTripsNonNullableModel,
        | "id"
        | "gtfs_trip_id"
        | "gtfs_route_type"
        | "start_timestamp"
        | "end_timestamp"
        | "agency_name_scheduled"
        | "start_cis_stop_id"
        | "last_position_context"
    > {}

export interface ITripPositions extends ITripAttributes {
    positions: IVPTripsPositionAttributes[];
}
