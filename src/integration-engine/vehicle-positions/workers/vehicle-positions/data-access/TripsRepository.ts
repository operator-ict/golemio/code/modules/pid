import { DateTimeUtils } from "#helpers/DateTimeUtils";
import { VPContainer } from "#ie/vehicle-positions/ioc/Di";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { IExtendedMetroRunInputForProcessing } from "#ie/vehicle-positions/workers/runs/interfaces/IMetroRunInputForProcessing";
import { DescriptorRepository } from "#ie/vehicle-positions/workers/vehicle-descriptors/data-access/DescriptorRepository";
import { PG_SCHEMA } from "#sch/const";
import { VPTripsModel } from "#sch/vehicle-positions/models/VPTripsModel";
import { IGtfsRtTripDto } from "#sch/vehicle-positions/models/interfaces/IGtfsRtTripDto";
import { IVPTripsModel } from "#sch/vehicle-positions/models/interfaces/IVPTripsModel";
import { IVPTripsPositionAttributes } from "#sch/vehicle-positions/models/interfaces/IVPTripsPositionAttributes";
import { IScheduleDto } from "#sch/vehicle-positions/redis/interfaces/IGtfsRunTripCacheDto";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import moment from "@golemio/core/dist/shared/moment-timezone";
import Sequelize, { Op, QueryTypes, WhereOptions } from "@golemio/core/dist/shared/sequelize";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { StatePositionEnum } from "src/const";
import { ProviderSourceTypeEnum } from "../helpers/ProviderSourceTypeEnum";
import { IUpdateRunsGtfsTripInput } from "../interfaces/IUpdateRunsGtfsTripInput";
import { IPropagateDelay } from "../interfaces/VPInterfaces";
import { PositionsRepository } from "./PositionsRepository";
import { RawQueryProvider } from "./helpers/RawQueryProvider";
import TripsMapper from "./helpers/TripsMapper";
import {
    IEnrichedTripForPublicApi,
    IFoundGTFSTripData,
    IUpdateDelayRunTripsData,
    IUpdateDelayTripsIdsData,
    IUpdateGTFSTripIdData,
} from "./interfaces/TripRepositoryInterfaces";

@injectable()
export class TripsRepository extends PostgresModel implements IModel {
    private logger: ILogger;
    private connector: IDatabaseConnector;
    private positionsRepository: PositionsRepository;
    private descriptorRepository: DescriptorRepository;

    constructor() {
        super(
            "VPTripsRepository",
            {
                pgTableName: VPTripsModel.TABLE_NAME,
                pgSchema: PG_SCHEMA,
                outputSequelizeAttributes: VPTripsModel.attributeModel,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator("VPTripsRepositoryValidator", VPTripsModel.jsonSchema)
        );

        this.logger = VPContainer.resolve<ILogger>(CoreToken.Logger);
        this.connector = VPContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector);
        this.positionsRepository = new PositionsRepository();
        this.descriptorRepository = VPContainer.resolve<DescriptorRepository>(VPContainerToken.DescriptorRepository);

        this.sequelizeModel.belongsTo(this.positionsRepository["sequelizeModel"], {
            as: "last_position",
            foreignKey: "last_position_id",
        });
        this.sequelizeModel.belongsTo(this.descriptorRepository["sequelizeModel"], {
            as: "vehicle_descriptor",
            foreignKey: "vehicle_registration_number",
            targetKey: "registration_number",
            scope: {
                [Sequelize.Op.and]: [
                    Sequelize.literal(`${VPTripsModel.TABLE_NAME}.gtfs_route_type = vehicle_descriptor.gtfs_route_type`),
                ],
            },
        });
    }

    public findAllAssocTripIds = async (tripIds: string[]): Promise<IUpdateDelayTripsIdsData[]> => {
        const connection = this.connector.getConnection();
        return Array.isArray(tripIds) && tripIds.length > 0
            ? (
                  await connection.query(RawQueryProvider.getFindAssociatedTripsQuery(), {
                      type: Sequelize.QueryTypes.SELECT,
                      bind: { tripIds: tripIds.map((id) => `${id}%`) },
                  })
              ).map((res: any) => ({
                  id: res.id,
                  gtfs_trip_id: res.gtfs_trip_id,
                  gtfs_block_id: res.gtfs_block_id,
                  gtfs_route_type: res.gtfs_route_type,
                  start_timestamp: res.start_timestamp,
                  end_timestamp: res.end_timestamp,
                  run_number: res.run_number,
                  internal_run_number: res.internal_run_number,
                  origin_route_name: res.origin_route_name,
                  internal_route_name: res.internal_route_name,
                  vehicle_registration_number: res.vehicle_registration_number,
              }))
            : [];
    };

    public findGTFSTripId = async (
        trip: IUpdateGTFSTripIdData
    ): Promise<IUpdateDelayTripsIdsData | IUpdateDelayTripsIdsData[] | undefined> => {
        let foundGtfsTrips: IFoundGTFSTripData[] = [];
        if (Number(trip.start_cis_stop_id) >= 5400000 && Number(trip.start_cis_stop_id) < 5500000) {
            // trains
            // array of founded gtfs trips, 0 or 1 or more with same block_id
            foundGtfsTrips = await this.findGTFSTripIdsTrain(trip);
        } else {
            // other
            foundGtfsTrips = await this.findGTFSTripIdBasic(trip);
        }

        if (foundGtfsTrips.length < 1) {
            return;
        }

        // update existing trip with gtfs_trip_id, gtfs_block_id and other gtfs data
        const firstFoundGtfsTrip = foundGtfsTrips.shift()!;
        const { startTimestamp, endTimestamp } = this.deduceTimestamps(
            firstFoundGtfsTrip.min_stop_time,
            firstFoundGtfsTrip.max_stop_time,
            trip.start_timestamp!
        );

        await this.update(
            {
                ...firstFoundGtfsTrip,
                internal_run_number: firstFoundGtfsTrip.internal_run_number ?? trip.run_number,
                internal_route_name: firstFoundGtfsTrip.internal_route_name ?? trip.origin_route_name,
                start_timestamp: startTimestamp,
                end_timestamp: endTimestamp,
            },
            {
                where: {
                    id: trip.id,
                },
            }
        );

        const newIds: IUpdateDelayTripsIdsData[] = [
            {
                id: trip.id,
                gtfs_trip_id: firstFoundGtfsTrip.gtfs_trip_id,
                gtfs_block_id: firstFoundGtfsTrip.gtfs_block_id,
                gtfs_route_type: firstFoundGtfsTrip.gtfs_route_type,
                internal_run_number: firstFoundGtfsTrip.internal_run_number ?? trip.run_number,
                internal_route_name: firstFoundGtfsTrip.internal_route_name ?? trip.origin_route_name,
                start_timestamp: startTimestamp.toISOString(),
                end_timestamp: endTimestamp.toISOString(),
            },
        ];

        // for other trips insert new rows with suffixed id
        for (const foundTrip of foundGtfsTrips) {
            const newId = `${trip.id}_gtfs_trip_id_${foundTrip.gtfs_trip_id}`;

            foundTrip.id = newId;

            const { startTimestamp, endTimestamp } = this.deduceTimestamps(
                foundTrip.min_stop_time,
                foundTrip.max_stop_time,
                trip.start_timestamp!
            );

            await this.save({
                ...trip,
                ...foundTrip,
                internal_run_number: foundTrip.internal_run_number ?? trip.run_number,
                internal_route_name: foundTrip.internal_route_name ?? trip.origin_route_name,
                start_timestamp: startTimestamp,
                end_timestamp: endTimestamp,
            });

            newIds.push({
                id: newId,
                gtfs_trip_id: foundTrip.gtfs_trip_id,
                gtfs_block_id: foundTrip.gtfs_block_id,
                gtfs_route_type: foundTrip.gtfs_route_type,
                start_timestamp: startTimestamp.toISOString(),
                end_timestamp: endTimestamp.toISOString(),
                run_number: trip.run_number,
                internal_run_number: foundTrip.internal_run_number ?? trip.run_number,
                internal_route_name: foundTrip.internal_route_name ?? trip.origin_route_name,
                origin_route_name: trip.origin_route_name,
                vehicle_registration_number: trip.vehicle_registration_number,
            });
        }
        return newIds;
    };

    public deleteNHoursOldData = async (hours: number): Promise<number> => {
        try {
            return await this.sequelizeModel.destroy({
                where: {
                    updated_at: {
                        [Op.lt]: Sequelize.literal(`NOW() - INTERVAL '${hours} HOURS'`),
                    },
                },
            });
        } catch (err) {
            throw new GeneralError("Error while purging old data", this.constructor.name, err);
        }
    };

    private deduceTimestamps = (
        min_stop_time: string,
        max_stop_time: string,
        origin_start_timestamp: string
    ): {
        startTimestamp: Date;
        endTimestamp: Date;
    } => {
        return {
            startTimestamp: DateTimeUtils.getStopDateTimeForTripOrigin(min_stop_time, origin_start_timestamp),
            endTimestamp: DateTimeUtils.getStopDateTimeForTripOrigin(max_stop_time, origin_start_timestamp),
        };
    };

    public upsertCommonRunTrip = async (
        { run }: IUpdateRunsGtfsTripInput,
        gtfsTrip: IScheduleDto
    ): Promise<IUpdateDelayRunTripsData> => {
        try {
            const [record] = await this.sequelizeModel.upsert(TripsMapper.mapCommonRunToDto(gtfsTrip, run), {
                returning: true,
            });

            return record.get({ plain: true });
        } catch (err) {
            throw new GeneralError("upsertCommonRunTrip: Error while saving to database.", this.name, err);
        }
    };

    public upsertMetroRunTrip = async (
        runInput: IExtendedMetroRunInputForProcessing,
        gtfsTrip: IScheduleDto
    ): Promise<IUpdateDelayRunTripsData> => {
        try {
            const [record] = await this.sequelizeModel.upsert(TripsMapper.mapMetroRunToDto(gtfsTrip, runInput), {
                returning: true,
            });

            return record.get({ plain: true });
        } catch (err) {
            throw new GeneralError("upsertMetroRunTrip: Error while saving to database.", this.name, err);
        }
    };

    public upsertEntity = async (tripDto: Partial<IVPTripsModel>): Promise<IUpdateDelayRunTripsData> => {
        try {
            const [record] = await this.sequelizeModel.upsert(tripDto, { returning: true });
            return record.get({ plain: true });
        } catch (err) {
            throw new GeneralError("upsertNotPublic: error while saving data", this.name, err);
        }
    };

    public getPropagateDelayTripsWithPositions = async (
        data: IPropagateDelay
    ): Promise<Array<IVPTripsModel & { last_position: IVPTripsPositionAttributes }>> => {
        const where: WhereOptions = {
            gtfs_trip_id: data.nextGtfsTrips.map((element) => element.trip_id),
        };

        if (data.currentRegistrationNumber) {
            where.vehicle_registration_number = data.currentRegistrationNumber;
        }

        return this.sequelizeModel.findAll({
            include: [
                {
                    as: "last_position",
                    model: this.positionsRepository["sequelizeModel"],
                    where: {
                        state_position: {
                            [Sequelize.Op.in]: [StatePositionEnum.BEFORE_TRACK, StatePositionEnum.INVISIBLE],
                        },
                        origin_timestamp: data.currentOriginTimestamp,
                    },
                },
            ],
            where: where,
            order: [["start_timestamp", "ASC"]],
        });
    };

    public hasOne = (model: any, options: any): any => {
        return this.sequelizeModel.hasOne(model, options);
    };

    public findAll = async (options: Sequelize.FindOptions<any> | undefined): Promise<any> => {
        return this.sequelizeModel.findAll(options);
    };

    private findGTFSTripIdBasic = async (trip: IUpdateGTFSTripIdData): Promise<IFoundGTFSTripData[]> => {
        const connection = this.connector.getConnection();
        const startDate = moment(trip.start_timestamp).tz("Europe/Prague");
        const startDateDayBefore = startDate.clone().subtract(1, "day");

        const startDateYMD = startDate.format("YYYY-MM-DD");
        const startDateDayName = startDate.format("dddd").toLowerCase();
        const startDateDayBeforeYMD = startDateDayBefore.format("YYYY-MM-DD");
        const startDateDayBeforeDayName = startDateDayBefore.format("dddd").toLowerCase();

        const replacements = {
            startDateDayBeforeYMD,
            startDateYMD,
            cisLineShortName: trip.cis_line_short_name,
            startCisStopId: trip.start_cis_stop_id || 0,
            startAswStopId: trip.start_asw_stop_id || "",
            startCisStopPlatformCode: trip.start_cis_stop_platform_code,
            startDateFormatted: startDate.utc().format(),
            startDateDayName,
            startDateDayBeforeDayName,
        };

        const sqlQuery = RawQueryProvider.getFindGtfsCommonTripsQuery();
        let result = (await connection.query(sqlQuery, {
            type: Sequelize.QueryTypes.SELECT,
            replacements,
        })) as IFoundGTFSTripData[];

        if (result.length < 1) {
            this.logger.warn(replacements, `${this.constructor.name}: Model data was not found for id '${trip.id}' (basic).`);
        } else if (result.length > 1) {
            this.logger.warn(
                replacements,
                `There are too many gtfs trips (${result.length}) linked with id '${trip.id}' (trip).`
            );
            result = [];
        }

        return result;
    };

    private findGTFSTripIdsTrain = async (trip: IUpdateGTFSTripIdData): Promise<IFoundGTFSTripData[]> => {
        const connection = this.connector.getConnection();

        const sqlQuery = RawQueryProvider.getFindGtfsTrainTripsQuery();
        let results = (await connection.query(sqlQuery, {
            type: Sequelize.QueryTypes.SELECT,
            replacements: {
                tripId: trip.id,
            },
        })) as IFoundGTFSTripData[];

        if (results.length < 1) {
            this.logger.verbose(`Model data was not found for id '${trip.id}' (train).`, true, this.constructor.name, 5001);
        } else if (results.length > 10) {
            this.logger.verbose(`There are too many gtfs trips (${results.length}) linked with id '${trip.id}' (train).`);
            results = [];
        }

        return results as any[];
    };

    /**
     * Return all valid trips for GTFS Realtime feed
     *
     * @returns {Promise<any[]>}
     */
    public findAllForGTFSRt = async (minValidToDateTime: Date): Promise<IGtfsRtTripDto[]> => {
        return this.sequelizeModel.findAll({
            attributes: [
                "id",
                "run_number",
                "internal_run_number",
                "start_timestamp",
                "cis_line_id",
                "cis_trip_number",
                "start_time",
                "vehicle_registration_number",
                "gtfs_trip_id",
                "gtfs_route_id",
                "gtfs_route_type",
                "gtfs_route_short_name",
                "provider_source_type",
                "wheelchair_accessible",
            ],
            include: [
                {
                    as: "last_position",
                    model: this.positionsRepository["sequelizeModel"],
                    attributes: [
                        "is_canceled",
                        "origin_timestamp",
                        "last_stop_sequence",
                        "bearing",
                        "lat",
                        "lng",
                        "speed",
                        "delay",
                        "state_position",
                    ],
                    where: {
                        state_position: {
                            [Sequelize.Op.in]: [
                                StatePositionEnum.ON_TRACK,
                                StatePositionEnum.AT_STOP,
                                StatePositionEnum.CANCELED,
                                StatePositionEnum.BEFORE_TRACK,
                                StatePositionEnum.BEFORE_TRACK_DELAYED,
                            ],
                        },
                        valid_to: {
                            [Sequelize.Op.or]: [null, { [Sequelize.Op.gte]: minValidToDateTime }],
                        },
                        lat: {
                            [Sequelize.Op.ne]: null,
                        },
                        lng: {
                            [Sequelize.Op.ne]: null,
                        },
                    },
                },
                {
                    as: "vehicle_descriptor",
                    model: this.descriptorRepository["sequelizeModel"],
                    attributes: ["is_air_conditioned", "has_usb_chargers"],
                },
            ],
            order: [
                ["updated_at", "DESC"],
                ["id", "ASC"],
            ],
        });
    };

    public findAllForPublicCache = async (minValidToDateTime: Date): Promise<IEnrichedTripForPublicApi[]> => {
        try {
            return this.sequelizeModel.findAll({
                attributes: [
                    "id",
                    "gtfs_trip_id",
                    "gtfs_route_type",
                    "gtfs_route_short_name",
                    "cis_trip_number",
                    [Sequelize.literal("last_position.lat"), "lat"],
                    [Sequelize.literal("last_position.lng"), "lng"],
                    [Sequelize.literal("last_position.bearing"), "bearing"],
                    [Sequelize.literal("last_position.delay"), "delay"],
                    [Sequelize.literal("last_position.state_position"), "state_position"],
                    "created_at",
                    // Additional properties for detailed vehiclepositions in public API
                    "wheelchair_accessible",
                    "run_number",
                    "internal_run_number",
                    "vehicle_registration_number",
                    "agency_name_scheduled",
                    "agency_name_real",
                    "origin_route_name",
                    "gtfs_shape_id",
                    "gtfs_trip_headsign",
                    "provider_source_type",
                    [Sequelize.literal("last_position.shape_dist_traveled"), "shape_dist_traveled"],
                    [Sequelize.literal("last_position.last_stop_sequence"), "last_stop_sequence"],
                    [Sequelize.literal("last_position.origin_timestamp"), "origin_timestamp"],
                    [Sequelize.literal("last_position.last_stop_headsign"), "last_stop_headsign"],
                ],
                include: [
                    {
                        as: "last_position",
                        model: this.positionsRepository["sequelizeModel"],
                        attributes: [],
                        required: true,
                        where: {
                            state_position: {
                                [Sequelize.Op.in]: [
                                    StatePositionEnum.ON_TRACK,
                                    StatePositionEnum.AT_STOP,
                                    StatePositionEnum.CANCELED,
                                    StatePositionEnum.OFF_TRACK,
                                    StatePositionEnum.BEFORE_TRACK,
                                    StatePositionEnum.BEFORE_TRACK_DELAYED,
                                ],
                            },
                            lat: {
                                [Sequelize.Op.ne]: null,
                            },
                            lng: {
                                [Sequelize.Op.ne]: null,
                            },
                            valid_to: {
                                [Sequelize.Op.or]: [null, { [Sequelize.Op.gte]: minValidToDateTime }],
                            },
                        },
                    },
                ],
                where: {
                    gtfs_trip_id: {
                        [Sequelize.Op.ne]: null,
                    },
                },
                order: [
                    ["created_at", "ASC"],
                    ["id", "ASC"],
                ],
                raw: true,
            });
        } catch (err) {
            throw new GeneralError("findAllForPublicCache: failed to get trips", this.name, err);
        }
    };

    public refreshGtfsTripData = async (): Promise<void> => {
        const connection = this.connector.getConnection();

        try {
            const [_result, rowCount] = await connection.query(RawQueryProvider.getUpdateGtfsTripDataQuery(), {
                type: QueryTypes.UPDATE,
            });

            this.logger.info(`refreshGtfsTripData: updated ${rowCount} trips`);
        } catch (err) {
            throw new GeneralError("refreshGtfsTripData: error while updating data", this.name, err);
        }
    };

    public getTripsWithInvalidGtfsTripId = async (
        providerSourceType: ProviderSourceTypeEnum
    ): Promise<IUpdateGTFSTripIdData[]> => {
        const connection = this.connector.getConnection();
        try {
            return await connection.query<IUpdateGTFSTripIdData>(RawQueryProvider.getFindTripsWithInvalidGtfsTripIdQuery(), {
                type: QueryTypes.SELECT,
                raw: true,
                bind: { providerSourceType },
            });
        } catch (err) {
            throw new GeneralError("getTripsWithInvalidGtfsTripId: error while getting trips", this.name, err);
        }
    };
}
