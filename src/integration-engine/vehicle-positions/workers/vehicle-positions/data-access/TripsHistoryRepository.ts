import { PG_SCHEMA } from "#sch/const";
import { VPTripsModel } from "#sch/vehicle-positions/models";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { Op, Sequelize } from "@golemio/core/dist/shared/sequelize";

export class TripsHistoryRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            "VPTripsHistoryRepository",
            {
                pgTableName: VPTripsModel.TABLE_NAME_HISTORY,
                pgSchema: PG_SCHEMA,
                outputSequelizeAttributes: VPTripsModel.attributeModel,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator("VPTripsHistoryRepositoryValidator", VPTripsModel.jsonSchema)
        );
    }

    public deleteNHoursOldData = async (hours: number): Promise<number> => {
        try {
            return await this.sequelizeModel.destroy({
                where: {
                    updated_at: {
                        [Op.lt]: Sequelize.literal(`NOW() - INTERVAL '${hours} HOURS'`),
                    },
                },
            });
        } catch (err) {
            throw new GeneralError("Error while purging old data", this.constructor.name, err);
        }
    };
}
