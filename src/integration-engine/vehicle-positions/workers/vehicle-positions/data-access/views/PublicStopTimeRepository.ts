import { PG_SCHEMA } from "#sch/const";
import { PublicStopTimeModel } from "#sch/vehicle-positions/models/views";
import { IPublicStopTimeDto } from "#sch/vehicle-positions/models/views/interfaces/IPublicStopTimeDto";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { ModelStatic } from "@golemio/core/dist/shared/sequelize";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class PublicStopTimeRepository extends PostgresModel implements IModel {
    public sequelizeModel!: ModelStatic<PublicStopTimeModel>;

    constructor() {
        super(
            "PublicStopTimeRepository",
            {
                pgTableName: PublicStopTimeModel.tableName,
                pgSchema: PG_SCHEMA,
                outputSequelizeAttributes: PublicStopTimeModel.attributeModel,
                savingType: "insertOnly",
                sequelizeAdditionalSettings: {
                    timestamps: false,
                },
            },
            new JSONSchemaValidator("PublicStopTimeReadOnly", {})
        );
    }

    public async findAll(): Promise<IPublicStopTimeDto[]> {
        return this.sequelizeModel.findAll({ raw: true });
    }
}
