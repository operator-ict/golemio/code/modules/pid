import { MetroRailtrackGPSRepository } from "#ie/ropid-gtfs/workers/timetables/tasks/data-access/MetroRailtrackGPSRepository";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { MetroRailtrackGPSModel } from "#sch/ropid-gtfs/models/railtrack/MetroRailtrackGPSModel";
import { IMetroRailtrackGPSModel } from "#sch/ropid-gtfs/models/railtrack/interfaces/IMetroRailtrackGPSModel";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

/*
 * In case of update of the data for metro railtrack. It is necessary to restart the service to reload cache.
 */

@injectable()
export class CachedMetroRailTrackLookup {
    private cachedLookup: Record<string, IMetroRailtrackGPSModel[]> | undefined;
    constructor(
        @inject(VPContainerToken.MetroRailtrackGPSRepository) private metroRailtrackGPSRepository: MetroRailtrackGPSRepository
    ) {}

    public async getAllByRouteName(route_name: string) {
        if (!this.cachedLookup) {
            const result = await this.metroRailtrackGPSRepository["sequelizeModel"].findAll<MetroRailtrackGPSModel>({
                raw: true,
            });
            this.cachedLookup = this.groupByRouteName(result);
        }

        return this.cachedLookup[route_name];
    }

    private groupByRouteName(result: MetroRailtrackGPSModel[]): Record<string, IMetroRailtrackGPSModel[]> {
        return result.reduce((r, a) => {
            r[a.route_name] = r[a.route_name] || [];
            r[a.route_name].push(a);
            return r;
        }, Object.create(null));
    }
}
