import { PUBLIC_CACHE_NAMESPACE_PREFIX } from "#sch/vehicle-positions/redis/const";
import { IPublicApiCacheDto } from "#sch/vehicle-positions/redis/interfaces/IPublicApiCacheDto";
import { PublicApiCacheDtoSchema } from "#sch/vehicle-positions/redis/schemas/PublicApiCacheDtoSchema";
import { RedisModel } from "@golemio/core/dist/integration-engine/models/RedisModel";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { ChainableCommander } from "ioredis";
import { randomBytes } from "node:crypto";

@injectable()
export class PublicApiCacheRepository extends RedisModel {
    public static NAMESPACE_PREFIX = PUBLIC_CACHE_NAMESPACE_PREFIX;
    private readonly CACHE_TTL = 1 * 60; // 1 minute

    constructor() {
        super(
            "PublicApiCacheRepository",
            {
                isKeyConstructedFromData: false,
                prefix: PublicApiCacheRepository.NAMESPACE_PREFIX,
            },
            new JSONSchemaValidator("PublicApiCacheRepository", PublicApiCacheDtoSchema)
        );
    }

    public async geoadd(
        data: IPublicApiCacheDto[],
        futureData: IPublicApiCacheDto[],
        canceledData: IPublicApiCacheDto[]
    ): Promise<string> {
        try {
            await this.validator!.Validate(data);
        } catch (err) {
            throw new GeneralError(
                `PublicApiCacheRepository: data validation failed: ${err.message}`,
                this.constructor.name,
                err
            );
        }

        const pipeline = this.connection.pipeline();

        try {
            const newSetKey = `${PublicApiCacheRepository.NAMESPACE_PREFIX}:${randomBytes(4).toString("hex")}`;

            for (const item of data) {
                // geo sorted set with vehicle ids (for quick lookup of vehicle ids in given area)
                pipeline.geoadd(newSetKey, item.lng, item.lat, item.vehicle_id);

                // list with vehicle ids for each trip (for quick lookup of vehicle ids for each trip)
                pipeline.rpush(`${newSetKey}:trip-${item.gtfs_trip_id}`, item.vehicle_id);
                pipeline.expire(`${newSetKey}:trip-${item.gtfs_trip_id}`, this.CACHE_TTL);

                // vehicle data for each vehicle id (for quick lookup of vehicle data for each vehicle id)
                pipeline.set(`${newSetKey}:vehicle-${item.vehicle_id}`, JSON.stringify(item), "EX", this.CACHE_TTL);
            }

            if (futureData.length > 0) {
                await this.geoaddFuture(futureData, newSetKey, pipeline);
            }

            if (canceledData.length > 0) {
                await this.geoaddCanceled(canceledData, newSetKey, pipeline);
            }

            pipeline.expire(newSetKey, this.CACHE_TTL);
            await pipeline.exec();

            return newSetKey;
        } catch (err) {
            pipeline.quit();

            if (err instanceof AbstractGolemioError) {
                throw err;
            }

            throw new GeneralError("PublicApiCacheRepository: geoadd failed", err);
        }
    }

    private async geoaddFuture(futureData: IPublicApiCacheDto[], newSetKey: string, pipeline: ChainableCommander): Promise<void> {
        try {
            await this.validator!.Validate(futureData);
        } catch (err) {
            throw new GeneralError(
                `PublicApiCacheRepository: future data validation failed: ${err.message}`,
                this.constructor.name,
                err
            );
        }

        try {
            for (const item of futureData) {
                // list with vehicle ids for future trips (for quick lookup of vehicle ids for each trip in the future)
                pipeline.rpush(`${newSetKey}:future-trip-${item.gtfs_trip_id}`, item.vehicle_id);
                pipeline.expire(`${newSetKey}:future-trip-${item.gtfs_trip_id}`, this.CACHE_TTL);

                // vehicle data for each vehicle id (for quick lookup of vehicle data for each vehicle id)
                pipeline.set(
                    `${newSetKey}:future-vehicle-${item.vehicle_id}-${item.gtfs_trip_id}`,
                    JSON.stringify(item),
                    "EX",
                    this.CACHE_TTL
                );
            }
        } catch (err) {
            throw new GeneralError("PublicApiCacheRepository: geoaddFuture failed", err);
        }
    }

    private async geoaddCanceled(
        canceledData: IPublicApiCacheDto[],
        newSetKey: string,
        pipeline: ChainableCommander
    ): Promise<void> {
        try {
            await this.validator!.Validate(canceledData);
        } catch (err) {
            throw new GeneralError(
                `PublicApiCacheRepository: future data validation failed: ${err.message}`,
                this.constructor.name,
                err
            );
        }
        try {
            for (const item of canceledData) {
                pipeline.set(`${newSetKey}:canceled-trips-${item.gtfs_trip_id}`, JSON.stringify(item), "EX", this.CACHE_TTL);
            }
        } catch (err) {
            throw new GeneralError("PublicApiCacheRepository: geoaddCanceled failed", err);
        }
    }
}
