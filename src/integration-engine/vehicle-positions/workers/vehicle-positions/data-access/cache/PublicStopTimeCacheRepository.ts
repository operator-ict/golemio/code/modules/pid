import { PUBLIC_STOP_TIME_CACHE_NAMESPACE_PREFIX } from "#sch/vehicle-positions/redis/const";
import { IPublicStopTimeCacheDto } from "#sch/vehicle-positions/redis/interfaces/IPublicStopTimeCacheDto";
import { PublicStopTimeCacheDtoSchema } from "#sch/vehicle-positions/redis/schemas/PublicStopTimeCacheDtoSchema";
import { ILogger } from "@golemio/core/dist/helpers";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { RedisModel } from "@golemio/core/dist/integration-engine/models/RedisModel";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class PublicStopTimeCacheRepository extends RedisModel {
    public static NAMESPACE_PREFIX = PUBLIC_STOP_TIME_CACHE_NAMESPACE_PREFIX;

    constructor(@inject(CoreToken.Logger) private log: ILogger) {
        super(
            "PublicStopTimeCacheRepository",
            {
                isKeyConstructedFromData: false,
                prefix: PublicStopTimeCacheRepository.NAMESPACE_PREFIX,
            },
            new JSONSchemaValidator("PublicStopTimeCacheRepository", PublicStopTimeCacheDtoSchema)
        );
    }

    public async createOrUpdate(data: Map<string, IPublicStopTimeCacheDto[]>, ttlInSeconds: number): Promise<void> {
        try {
            for (const stopTimes of data.values()) {
                await this.validator!.Validate(stopTimes);
            }
        } catch (err) {
            throw new GeneralError(
                `PublicStopTimeCacheRepository: data validation failed: ${err.message}`,
                this.constructor.name,
                err
            );
        }

        try {
            const stringifiedData = new Map<string, string>();
            for (const [key, stopTimes] of data.entries()) {
                const redisKey = `${this.prefix}:${key}`;
                stringifiedData.set(redisKey, JSON.stringify(stopTimes));
            }

            const pipeline = this.connection.pipeline();
            pipeline.mset(stringifiedData);

            for (const key of stringifiedData.keys()) {
                pipeline.expire(key, ttlInSeconds);
            }

            await pipeline.exec();
        } catch (err) {
            throw new GeneralError("PublicStopTimeCacheRepository: mset failed", err);
        }
    }

    public async getPublicStopTimeCache(vehicleIds: string[]): Promise<Map<string, IPublicStopTimeCacheDto[]>> {
        let stopTimeCache: Array<string | null> = [];
        try {
            stopTimeCache = await this.connection.mget(
                vehicleIds.map((id) => `${PUBLIC_STOP_TIME_CACHE_NAMESPACE_PREFIX}:${id}`)
            );
        } catch (err) {
            throw new GeneralError(
                `${this.constructor.name}: Cannot get public stop time cache from cache: ${err.message}`,
                this.constructor.name,
                err
            );
        }

        const results = new Map<string, IPublicStopTimeCacheDto[]>();
        for (let i = 0; i < vehicleIds.length; i++) {
            const vehicleId = vehicleIds[i];
            const cacheValue = stopTimeCache[i];
            let value: IPublicStopTimeCacheDto[];
            if (cacheValue === null) {
                this.log.info(`${this.constructor.name}: Cannot find public stop time cache with key '${vehicleId}'`);
                value = [];
            } else {
                try {
                    value = JSON.parse(cacheValue);
                } catch (err) {
                    throw new GeneralError(
                        `${this.constructor.name}: Cannot parse public stop time cache: ${err.message}`,
                        this.constructor.name,
                        err
                    );
                }
            }
            results.set(vehicleId, value);
        }

        return results;
    }
}
