import { IDescriptorOutputDto } from "#sch/vehicle-descriptors/models/interfaces";

export interface IDescriptorFilter {
    yieldFilteredDescriptors(messages: IDescriptorOutputDto[]): Generator<IDescriptorOutputDto>;
}
