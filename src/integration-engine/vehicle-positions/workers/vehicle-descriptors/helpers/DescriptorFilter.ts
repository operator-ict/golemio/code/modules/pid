import { IDescriptorOutputDto } from "#sch/vehicle-descriptors/models/interfaces";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { IDescriptorFilter } from "./interfaces/IDescriptorFilter";
import { VehicleDescriptorStateEnum, VehicleDescriptorTractionEnum } from "#helpers/VehicleDescriptorEnums";

@injectable()
export class DescriptorFilter implements IDescriptorFilter {
    constructor(@inject(CoreToken.Logger) private logger: ILogger) {}

    /**
     * Yield descriptors that are valid for saving (filter out duplicates entirely)
     *   - combination of registration number and gtfs route type is unique
     */
    public *yieldFilteredDescriptors(descriptors: IDescriptorOutputDto[]): Generator<IDescriptorOutputDto> {
        const sortedDescriptors = descriptors.sort((a, b) => {
            return (
                this.getKeyWithState(a.registration_number, a.gtfs_route_type, a.state) -
                this.getKeyWithState(b.registration_number, b.gtfs_route_type, b.state)
            );
        });

        const seenCombinations = new Set<string>();
        for (const descriptor of sortedDescriptors) {
            const { state, traction, registration_number, gtfs_route_type } = descriptor;
            if (this.isValidState(state) && this.isValidTraction(traction)) {
                const combination = `${registration_number}-${gtfs_route_type}`;
                if (!seenCombinations.has(combination)) {
                    seenCombinations.add(combination);
                    yield descriptor;
                } else {
                    this.logger.info(
                        `${this.constructor.name}: descriptor ${registration_number} for route type ${gtfs_route_type} is invalid`
                    );
                }
            } else {
                this.logger.info(`${this.constructor.name}: invalid state: ${state} or traction: ${traction}`);
            }
        }
    }
    private isValidState = (state: string): state is VehicleDescriptorStateEnum => {
        return Object.values(VehicleDescriptorStateEnum).includes(state as VehicleDescriptorStateEnum);
    };

    private isValidTraction = (traction: string): traction is VehicleDescriptorTractionEnum => {
        return Object.values(VehicleDescriptorTractionEnum).includes(traction as VehicleDescriptorTractionEnum);
    };

    private getKeyWithState(registration_number: number, gtfs_route_type: number, state: string): number {
        return parseInt(
            `${registration_number}${gtfs_route_type}${Object.values(VehicleDescriptorStateEnum).indexOf(
                state as VehicleDescriptorStateEnum
            )}`
        );
    }
}
