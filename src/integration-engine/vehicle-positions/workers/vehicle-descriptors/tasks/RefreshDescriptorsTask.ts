import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { DescriptorModel } from "#sch/vehicle-descriptors/models";
import { ILogger } from "@golemio/core/dist/helpers";
import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc/ContainerToken";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { WORKER_NAME } from "../constants";
import { DescriptorRepository } from "../data-access/DescriptorRepository";
import { DescriptorDataSourceFactory, DescriptorProvider } from "../datasources/DescriptorDataSourceFactory";
import { IDescriptorFilter } from "../helpers/interfaces/IDescriptorFilter";
import { DescriptorTransformation } from "../transformations/DescriptorTransformation";

@injectable()
export class RefreshDescriptorsTask extends AbstractEmptyTask {
    public readonly queueName = "refreshDescriptors";
    public readonly queueTtl = 10 * 60 * 1000; // 10 minutes

    constructor(
        @inject(VPContainerToken.DescriptorDataSourceFactory)
        private dataSourceFactory: DescriptorDataSourceFactory,
        @inject(VPContainerToken.DescriptorFilter)
        private descriptorFilter: IDescriptorFilter,
        @inject(VPContainerToken.DescriptorRepository)
        private descriptorRepository: DescriptorRepository,
        @inject(VPContainerToken.DescriptorTransformation)
        private descriptorTransformation: DescriptorTransformation,
        @inject(ContainerToken.Logger) private logger: ILogger
    ) {
        super(WORKER_NAME);
    }

    protected async execute(): Promise<void> {
        const dataSource = this.dataSourceFactory.getDataSource(DescriptorProvider.SeznamAutobusu);
        const descriptorsResult = await dataSource.getAll();
        const descriptorsTransformed = await this.descriptorTransformation.transform(descriptorsResult);
        const descriptorsFiltered = [...this.descriptorFilter.yieldFilteredDescriptors(descriptorsTransformed)];

        if (descriptorsFiltered.length === 0) {
            this.logger.warn(
                `${this.queueName}: no descriptors out of ${descriptorsResult.length} were transformed or were filtered out`
            );
            return;
        }

        await this.descriptorRepository.saveData(descriptorsFiltered);
        this.logger.info(
            `${this.queueName}: ${descriptorsFiltered.length}/${descriptorsResult.length} vehicle descriptors were saved`
        );
    }
}
