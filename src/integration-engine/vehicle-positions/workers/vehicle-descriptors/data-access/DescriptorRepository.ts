import { GTFSRouteTypeEnum } from "#helpers/RouteTypeEnums";
import { PG_SCHEMA } from "#sch/const";
import { DescriptorModel } from "#sch/vehicle-descriptors/models";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import Sequelize, { Op, Transaction } from "@golemio/core/dist/shared/sequelize";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { IDescriptorOutputDto } from "#sch/vehicle-descriptors/models/interfaces";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";

@injectable()
export class DescriptorRepository extends PostgresModel implements IModel {
    private static REPOSITORY_NAME = "VPDescriptorRepository";

    constructor(@inject(CoreToken.Logger) private logger: ILogger) {
        super(
            DescriptorRepository.REPOSITORY_NAME,
            {
                pgTableName: DescriptorModel.tableName,
                pgSchema: PG_SCHEMA,
                outputSequelizeAttributes: DescriptorModel.attributeModel,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(DescriptorRepository.REPOSITORY_NAME + "Validator", DescriptorModel.arrayJsonSchema)
        );
    }

    public async findAccessDescByRegistrationNumbers(
        registrationNumbers: number[],
        gtfsRouteType: GTFSRouteTypeEnum
    ): Promise<DescriptorModel[]> {
        try {
            return await this.sequelizeModel.findAll<DescriptorModel>({
                attributes: ["is_wheelchair_accessible", "registration_number"],
                where: {
                    registration_number: registrationNumbers,
                    gtfs_route_type: gtfsRouteType,
                },
            });
        } catch (err) {
            throw new GeneralError("findAccessDescByRegistrationNumbers: failed to get descriptors", this.name, err);
        }
    }

    public async saveData(descriptors: IDescriptorOutputDto[]): Promise<void> {
        let transaction: Transaction | undefined;

        try {
            await this.validate(descriptors);

            transaction = await this.sequelizeModel.sequelize?.transaction();
            await this.sequelizeModel.destroy({ where: {}, transaction });
            await this.sequelizeModel.bulkCreate<DescriptorModel>(descriptors, {
                ignoreDuplicates: true,
                returning: false,
                transaction,
            });
            await transaction?.commit();
        } catch (err) {
            const exception = new GeneralError(
                `[${this.constructor.name}] Could not save data: ${err.message}`,
                this.constructor.name,
                err
            );

            this.logger.error(exception);
            await transaction?.rollback();
            throw exception;
        }
    }

    public deleteNHoursOldData = async (hours: number): Promise<number> => {
        try {
            return await this.sequelizeModel.destroy({
                where: {
                    updated_at: {
                        [Op.lt]: Sequelize.literal(`NOW() - INTERVAL '${hours} HOURS'`),
                    },
                },
            });
        } catch (err) {
            throw new GeneralError("Error while purging old data", this.constructor.name, err);
        }
    };
}
