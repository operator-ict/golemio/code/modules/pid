import GtfsOvapi from "@golemio/ovapi-gtfs-realtime-bindings";

import IFeedEntity = GtfsOvapi.transit_realtime.IFeedEntity;
import IFeedHeader = GtfsOvapi.transit_realtime.IFeedHeader;
import ITripDescriptor = GtfsOvapi.transit_realtime.ITripDescriptor;
import IStopTimeUpdate = GtfsOvapi.transit_realtime.TripUpdate.IStopTimeUpdate;
import IVehicleDescriptor = GtfsOvapi.transit_realtime.IVehicleDescriptor;

export { IFeedEntity, IFeedHeader, ITripDescriptor, IStopTimeUpdate, IVehicleDescriptor };
