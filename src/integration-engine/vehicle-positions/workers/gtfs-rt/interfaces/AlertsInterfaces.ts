import { GtfsAlertCauseEnum } from "../helpers/enums/AlertCauseEnum";
import { GtfsAlertEffectEnum } from "../helpers/enums/AlertEffectEnum";

export interface IGtfsRtAlert {
    id: string;
    alert: {
        activePeriod: GtfsTimeRange[];
        informedEntity: InformedEntityRoute[];
        cause: GtfsAlertCauseEnum;
        effect: GtfsAlertEffectEnum;
        url: {
            translation: GtfsTranslatedString[];
        };
        headerText: {
            translation: GtfsTranslatedString[];
        };
        descriptionText: {
            translation: GtfsTranslatedString[];
        };
    };
}

export interface InformedEntityRoute {
    routeId: string;
}

export interface GtfsTimeRange {
    start: number;
    end?: number;
}

export interface GtfsTranslatedString {
    text: string;
    language: string;
}
