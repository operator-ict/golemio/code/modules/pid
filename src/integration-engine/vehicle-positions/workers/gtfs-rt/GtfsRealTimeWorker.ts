import { VehiclePositions } from "#sch/vehicle-positions";
import { AbstractWorker } from "@golemio/core/dist/integration-engine/workers";
import { GenerateFilesTask } from "./tasks/GenerateFilesTask";

export class GtfsRealTimeWorker extends AbstractWorker {
    protected name = VehiclePositions.name + "GtfsRt";

    constructor() {
        super();

        // Register tasks
        this.registerTask(new GenerateFilesTask(this.getQueuePrefix()));
    }
}
