import { RedisModel } from "@golemio/core/dist/integration-engine/models/RedisModel";

export class GtfsRtRedisRepository extends RedisModel {
    constructor() {
        super("GtfsRtRedisRepository", {
            isKeyConstructedFromData: false,
            prefix: "files:gtfsRt",
        });
    }
}
