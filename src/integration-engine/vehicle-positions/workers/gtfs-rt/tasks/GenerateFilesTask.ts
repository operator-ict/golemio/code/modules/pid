import { GTFSRouteTypeEnum } from "#helpers/RouteTypeEnums";
import { RopidVYMIEventsModel, RopidVYMIEventsRoutesModel } from "#ie/ropid-vymi";
import { VPContainer } from "#ie/vehicle-positions/ioc/Di";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { TripsRepository } from "#ie/vehicle-positions/workers/vehicle-positions/data-access/TripsRepository";
import { IGtfsRtTripDto } from "#sch/vehicle-positions/models/interfaces/IGtfsRtTripDto";
import { IPublicStopTimeCacheDto } from "#sch/vehicle-positions/redis/interfaces/IPublicStopTimeCacheDto";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc";
import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine/workers";
import { CloudflareCachePurgeWebhook } from "@golemio/core/dist/integration-engine/workers/webhooks/CloudflareCachePurgeWebhook";
import { WebhookDecorators } from "@golemio/core/dist/integration-engine/workers/helpers/WebhookDecorators";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { transit_realtime as gtfsRealtime } from "@golemio/ovapi-gtfs-realtime-bindings";
import { StatePositionEnum } from "src/const";
import { PublicStopTimeCacheRepository } from "../../vehicle-positions/data-access/cache/PublicStopTimeCacheRepository";
import { GtfsRtRedisRepository } from "../data-access/GtfsRtRedisRepository";
import { AlertsGenerator } from "../helpers/AlertsGenerator";
import { VehicleDescriptor } from "../helpers/VehicleDescriptor";
import { MPS_TO_KMH } from "../helpers/const";
import { IFeedEntity, IFeedHeader, IStopTimeUpdate, ITripDescriptor } from "../interfaces/GtfsRealtimeInterfaces";

type EntityWithTripStartByVehicleId = Map<string, { entity: gtfsRealtime.IFeedEntity; entityTripStart: Date }>;

export class GenerateFilesTask extends AbstractEmptyTask {
    private static readonly config: ISimpleConfig = VPContainer.resolve<ISimpleConfig>(CoreToken.SimpleConfig);

    public readonly queueName = "generateFiles";
    public readonly queueTtl = 20 * 1000; // 20 seconds

    private readonly logger: ILogger;
    private readonly stopTimeRepository: PublicStopTimeCacheRepository;
    private readonly tripsRepository: TripsRepository;
    private readonly gtfsRtRedisRepository: GtfsRtRedisRepository;
    private readonly alertsGenerator: AlertsGenerator;
    private readonly vehicleDescriptor: VehicleDescriptor;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.logger = VPContainer.resolve<ILogger>(ContainerToken.Logger);

        this.stopTimeRepository = VPContainer.resolve<PublicStopTimeCacheRepository>(
            VPContainerToken.PublicStopTimeCacheRepository
        );
        this.tripsRepository = new TripsRepository();
        this.gtfsRtRedisRepository = new GtfsRtRedisRepository();

        const vymiEventsRepository = new RopidVYMIEventsModel();
        const vymiRoutesRepository = new RopidVYMIEventsRoutesModel();
        this.alertsGenerator = new AlertsGenerator(this.gtfsRtRedisRepository, vymiEventsRepository, vymiRoutesRepository);
        this.vehicleDescriptor = new VehicleDescriptor();
    }

    @WebhookDecorators.after(
        new CloudflareCachePurgeWebhook(
            Object.values(
                GenerateFilesTask.config.getValue<Record<string, string>>("module.pid.cloudflare.cachedApiUrlPaths", {})
            )
        )
    )
    protected async execute() {
        const startDateTime = new Date();
        const tripData = await this.tripsRepository.findAllForGTFSRt(startDateTime);
        const feedHeader = this.createFeedHeader();
        const updatesMessage = gtfsRealtime.FeedMessage.create({ header: feedHeader });
        const positionsMessage = gtfsRealtime.FeedMessage.create({ header: feedHeader });
        const pidFeedMessage = gtfsRealtime.FeedMessage.create({ header: feedHeader });

        const stopTimesIds: string[] = [];
        for (const tripRecord of tripData) {
            const { id } = this.vehicleDescriptor.getVehicleDescriptor(tripRecord);
            if (typeof id === "string") {
                stopTimesIds.push(`${id}-${tripRecord.gtfs_trip_id}`);
            }
        }

        if (stopTimesIds.length === 0) {
            this.logger.warn("No stop time ids found for GTFS-RT generation. Most likely no vehicles are running.");
            return;
        }

        const stopTimes = await this.stopTimeRepository.getPublicStopTimeCache(stopTimesIds);
        const gtfsTripIdMap: Record<string, boolean> = {};
        const entityTripStarts: string[] = [];

        for (const tripRecord of tripData) {
            let tripUpdateFeedEntity: IFeedEntity | undefined = undefined;
            let feedMessageEntity: gtfsRealtime.FeedEntity | undefined = undefined;
            const vehicleDescriptor = this.vehicleDescriptor.getVehicleDescriptor(tripRecord);
            const entityTimestamp = Math.round(new Date(tripRecord.last_position.origin_timestamp).getTime() / 1000);
            if (tripRecord.last_position.is_canceled && !gtfsTripIdMap[tripRecord.gtfs_trip_id]) {
                tripUpdateFeedEntity = this.handleCanceledTrip(tripRecord, entityTimestamp, updatesMessage);
                gtfsTripIdMap[tripRecord.gtfs_trip_id] = true;
                // For canceled trips we don't want to create positions because they have lng 0 and lat 0
            } else {
                const tripDescriptor: ITripDescriptor = {
                    scheduleRelationship: gtfsRealtime.TripDescriptor.ScheduleRelationship.SCHEDULED,
                    startDate: this.parseUTCDateFromISO(tripRecord.start_timestamp),
                    startTime: tripRecord.start_time,
                    tripId: tripRecord.gtfs_trip_id,
                    routeId: tripRecord.gtfs_route_id,
                };

                let stopTimeUpdates: IStopTimeUpdate[] = [];
                if (typeof vehicleDescriptor.id === "string") {
                    const stopTimesForVehicle = stopTimes.get(`${vehicleDescriptor.id}-${tripRecord.gtfs_trip_id}`)!;
                    stopTimeUpdates = this.generateStopTimeUpdate(
                        tripRecord.gtfs_route_type,
                        stopTimesForVehicle,
                        tripRecord.last_position
                    );
                } else {
                    this.logger.error(
                        new GeneralError(
                            `Cannot get stopTimeUpdates for trip with unknown vehicleId and gtfsTripId \
                             '${tripRecord.gtfs_trip_id}'`,
                            this.constructor.name,
                            undefined,
                            undefined,
                            "pid"
                        )
                    );
                }

                const shouldSkipTripUpdate = stopTimeUpdates.every(
                    (el) =>
                        el.scheduleRelationship === gtfsRealtime.TripUpdate.StopTimeUpdate.ScheduleRelationship.NO_DATA ||
                        gtfsTripIdMap[tripRecord.gtfs_trip_id]
                );

                entityTripStarts.push(tripRecord.start_timestamp);
                const positionEntity: IFeedEntity = {
                    id: tripRecord.id,
                    vehicle: {
                        currentStopSequence: tripRecord.last_position.last_stop_sequence,
                        position: {
                            bearing: tripRecord.last_position.bearing,
                            latitude: tripRecord.last_position.lat,
                            longitude: tripRecord.last_position.lng,
                            speed:
                                tripRecord.last_position.speed &&
                                parseFloat((tripRecord.last_position.speed / MPS_TO_KMH).toFixed(2)),
                        },
                        timestamp: entityTimestamp,
                        trip: tripDescriptor,
                        vehicle: vehicleDescriptor,
                    },
                };
                const positionsMessageEntity = gtfsRealtime.FeedEntity.fromObject(positionEntity);
                positionsMessage.entity.push(positionsMessageEntity);
                feedMessageEntity = structuredClone(positionsMessageEntity);

                if (!shouldSkipTripUpdate) {
                    const tripUpdate = {
                        stopTimeUpdate: stopTimeUpdates,
                        timestamp: entityTimestamp,
                        trip: tripDescriptor,
                        vehicle: vehicleDescriptor,
                    };

                    const updateEntity: IFeedEntity = {
                        id: tripRecord.id,
                        tripUpdate,
                    };

                    const updatesMessageEntity = gtfsRealtime.FeedEntity.fromObject(updateEntity);
                    updatesMessage.entity.push(updatesMessageEntity);
                    gtfsTripIdMap[tripRecord.gtfs_trip_id] = true;
                    feedMessageEntity.tripUpdate = tripUpdate;
                }
            }

            if (feedMessageEntity) {
                pidFeedMessage.entity.push(feedMessageEntity);
            } else if (tripUpdateFeedEntity) {
                // If the trip is canceled we want to update just the trip without positions
                pidFeedMessage.entity.push(tripUpdateFeedEntity);
            }
        }

        this.removeDuplicateVehicleDescriptors(positionsMessage, entityTripStarts);
        this.removeDuplicateVehicleDescriptors(pidFeedMessage, entityTripStarts, { shouldRemoveVehicleOnly: true });

        this.validateAndSaveBuffer("trip_updates", updatesMessage);
        this.validateAndSaveBuffer("vehicle_positions", positionsMessage);
        this.validateAndSaveBuffer("pid_feed", pidFeedMessage);

        await this.alertsGenerator.generateAlerts(feedHeader);
    }

    /**
     * Get rid of vehicle descriptors with duplicate IDs from a given GTFS-RT message
     *
     * @param message The message to get rid of duplicate vehicle descriptors from
     * @param tripData The trip data to get start timestamps from
     * @param options Options for the duplicate removal process
     * @param options.removeVehicleOnly Whether to only remove the duplicate vehicle descriptor from the message entity, instead
     * of removing the whole entity. Defaults to `false`. Note that if an entity with a duplicate vehicle descriptor does not
     * contain any other valuable data, it will be removed regardless of this option.
     */
    private removeDuplicateVehicleDescriptors(
        message: gtfsRealtime.IFeedMessage,
        entityTripStarts: string[],
        options: { shouldRemoveVehicleOnly?: boolean } = {
            shouldRemoveVehicleOnly: false,
        }
    ): void {
        if (!message.entity) {
            return;
        }
        const entitiesWithoutVehicleId: gtfsRealtime.IFeedEntity[] = [];
        const entitiesWithoutTheirDuplicateVehicle: gtfsRealtime.IFeedEntity[] = [];
        const entityWithTripStartByVehicleId: EntityWithTripStartByVehicleId = new Map();
        for (let i = 0; i < message.entity.length; i++) {
            const entity = message.entity[i];
            const vehicleId = entity.vehicle?.vehicle?.id ?? null;
            const entityTripStart = new Date(entityTripStarts[i]);
            if (vehicleId === null) {
                entitiesWithoutVehicleId.push(entity);
            } else if (!entityWithTripStartByVehicleId.has(vehicleId)) {
                entityWithTripStartByVehicleId.set(vehicleId, { entity, entityTripStart });
            } else {
                const unusedEntity = this.conditionallyUpdateEntityByVehicleId(
                    entityWithTripStartByVehicleId,
                    vehicleId,
                    entity,
                    entityTripStart
                );
                if (options.shouldRemoveVehicleOnly) {
                    const { vehicle = null, ...entityWithoutVehicle } = unusedEntity;
                    if (Object.keys(entityWithoutVehicle).find((key) => key !== "id")) {
                        entitiesWithoutTheirDuplicateVehicle.push(entityWithoutVehicle);
                    }
                }
            }
        }
        message.entity = entitiesWithoutVehicleId.concat(entitiesWithoutTheirDuplicateVehicle);
        for (const { entity } of entityWithTripStartByVehicleId.values()) message.entity.push(entity);
    }

    /**
     * Update a given `EntityWithTripStartByVehicleId` Map with given values if the new entity has a lower trip start date/time
     * and is not canceled. The entity that does not end up in the map (either the new entity if it does not meet the conditions
     * for the update or the original entity if the new entity does) is returned.
     *
     * @param entityWithTripStartByVehicleId The Map to update
     * @param vehicleId The vehicle ID to use as a key
     * @param entity The entity to use as part of the new value, if the conditions are met
     * @param entityTripStart The entity trip start date/time to use as part of the new value, if the conditions are met
     */
    private conditionallyUpdateEntityByVehicleId(
        entityWithTripStartByVehicleId: EntityWithTripStartByVehicleId,
        vehicleId: string,
        entity: gtfsRealtime.IFeedEntity,
        entityTripStart: Date
    ): gtfsRealtime.IFeedEntity {
        const mapItem = entityWithTripStartByVehicleId.get(vehicleId)!;
        const entityTripScheduleRelationship = entity.vehicle?.trip?.scheduleRelationship ?? null;
        if (
            entityTripScheduleRelationship !== gtfsRealtime.TripDescriptor.ScheduleRelationship.CANCELED &&
            entityTripStart < mapItem.entityTripStart
        ) {
            entityWithTripStartByVehicleId.set(vehicleId, { entity, entityTripStart });
            return mapItem.entity;
        }
        return entity;
    }

    private generateStopTimeUpdate(
        routeType: GTFSRouteTypeEnum,
        stopTimes: IPublicStopTimeCacheDto[],
        vehiclePosition: IGtfsRtTripDto["last_position"]
    ): IStopTimeUpdate[] {
        const stopTimeUpdates: IStopTimeUpdate[] = [];

        // TODO this is a temporary solution, we need to fix the underlying issue in the source view
        //   v_public_vehiclepositions_combined_stop_times, the delay prediction is seemingly not working properly
        //   when the vehicle is before the track, see https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/371
        const shouldFallbackToScheduleTime = this.shouldFallbackToScheduleTime(vehiclePosition);

        for (const stopTime of stopTimes) {
            const scheduledTrack = stopTime.platform_code;
            let actualTrack = stopTime.platform_code;

            if (routeType === GTFSRouteTypeEnum.TRAIN) {
                actualTrack = stopTime.cis_stop_platform_code;
            }

            const arrivalDelay = shouldFallbackToScheduleTime ? 0 : stopTime.arr_delay ?? stopTime.dep_delay;
            const departureDelay = shouldFallbackToScheduleTime ? 0 : stopTime.dep_delay ?? stopTime.arr_delay;

            stopTimeUpdates.push({
                arrival: {
                    delay: arrivalDelay,
                },
                departure: {
                    delay: departureDelay,
                },
                ...(arrivalDelay === null &&
                    departureDelay === null && {
                        scheduleRelationship: gtfsRealtime.TripUpdate.StopTimeUpdate.ScheduleRelationship.NO_DATA,
                    }),
                stopSequence: stopTime.sequence,
                stopId: stopTime.stop_id,
                ".transit_realtime.ovapiStopTimeUpdate": {
                    ...(scheduledTrack && { scheduledTrack }),
                    ...(actualTrack && { actualTrack }),
                },
            });
        }

        return stopTimeUpdates;
    }

    private createFeedHeader(): IFeedHeader {
        const header: IFeedHeader = {
            gtfsRealtimeVersion: "2.0",
            incrementality: gtfsRealtime.FeedHeader.Incrementality.FULL_DATASET,
            timestamp: Math.round(new Date().getTime() / 1000),
        };

        return gtfsRealtime.FeedHeader.fromObject(header);
    }

    /**
     * Parse UTC date from ISO timestamp
     * @example 20230101
     */
    private parseUTCDateFromISO(timestamp: string): string {
        const startTimestamp = new Date(timestamp);
        const formattedDate =
            startTimestamp.getUTCFullYear() +
            ("0" + (startTimestamp.getUTCMonth() + 1)).slice(-2) +
            ("0" + startTimestamp.getUTCDate()).slice(-2);
        return formattedDate;
    }

    private validateAndSaveBuffer(feedName: string, message: gtfsRealtime.FeedMessage): void {
        const feedMessageVerificationResult = gtfsRealtime.FeedMessage.verify(message);
        if (feedMessageVerificationResult !== null) {
            this.logger.error(
                new GeneralError(
                    `Invalid GTFS-RT message for feed '${feedName}'`,
                    this.constructor.name,
                    feedMessageVerificationResult,
                    undefined,
                    "pid"
                )
            );
            return;
        }

        const buffer = gtfsRealtime.FeedMessage.encode(message).finish() as Buffer;

        this.gtfsRtRedisRepository.set(feedName + "_timestamp", message.header.timestamp);
        this.gtfsRtRedisRepository.hset(feedName + ".pb", buffer.toString("binary"));
    }

    private shouldFallbackToScheduleTime(vehiclePosition: IGtfsRtTripDto["last_position"]): boolean {
        return (
            (vehiclePosition.state_position === StatePositionEnum.BEFORE_TRACK ||
                vehiclePosition.state_position === StatePositionEnum.BEFORE_TRACK_DELAYED) &&
            (vehiclePosition.delay === 0 || vehiclePosition.delay === null)
        );
    }

    private handleCanceledTrip(
        tripRecord: IGtfsRtTripDto,
        timestamp: number,
        updatesMessage: gtfsRealtime.FeedMessage
    ): IFeedEntity {
        const feedEntity = {
            id: tripRecord.id,
            tripUpdate: {
                stopTimeUpdate: null,
                timestamp,
                trip: {
                    scheduleRelationship: gtfsRealtime.TripDescriptor.ScheduleRelationship.CANCELED,
                    startDate: this.parseUTCDateFromISO(tripRecord.start_timestamp),
                    startTime: tripRecord.start_time,
                    tripId: tripRecord.gtfs_trip_id,
                    routeId: tripRecord.gtfs_route_id,
                },
                vehicle: null,
            },
        };

        updatesMessage.entity.push(feedEntity);
        return feedEntity;
    }
}
