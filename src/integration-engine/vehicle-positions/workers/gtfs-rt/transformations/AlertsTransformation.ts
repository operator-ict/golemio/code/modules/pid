import { RopidVYMIEventsRoutesModel } from "#ie/ropid-vymi";
import { HTMLUtils } from "#ie/shared/HTMLUtils";
import { IRopidVYMIEventOutput } from "#sch/ropid-vymi";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { getGtfsCause } from "../helpers/AlertCauseMapper";
import { getGtfsEffectByEventType } from "../helpers/AlertEffectMapper";
import { IGtfsRtAlert, GtfsTimeRange, InformedEntityRoute } from "../interfaces/AlertsInterfaces";

export class AlertsTransformation extends BaseTransformation implements ITransformation {
    public name: string;
    private eventsRoutesModel: RopidVYMIEventsRoutesModel;

    constructor(eventsRoutesModel: RopidVYMIEventsRoutesModel) {
        super();
        this.name = "GtfsRtAlertTransformation";
        this.eventsRoutesModel = eventsRoutesModel;
    }

    public transform = async (alertEntities: IRopidVYMIEventOutput[]): Promise<IGtfsRtAlert[]> => {
        const transformedEntities: IGtfsRtAlert[] = [];
        for (const alertEntity of alertEntities) {
            transformedEntities.push(await this.transformElement(alertEntity));
        }
        return transformedEntities;
    };

    public transformElement = async (alertEntity: IRopidVYMIEventOutput): Promise<IGtfsRtAlert> => {
        return {
            id: `alert-${alertEntity.vymi_id}-${alertEntity.vymi_id_dtb}`,
            alert: {
                activePeriod: [this.getActivePeriod(alertEntity)],
                informedEntity: await this.getAffectedRoutes(alertEntity),
                cause: getGtfsCause(alertEntity.cause ?? ""),
                effect: getGtfsEffectByEventType(Number(alertEntity.event_type ?? 0)),
                url: {
                    translation: [
                        {
                            text: `https://pid.cz/mimoradnost/?id=${alertEntity.vymi_id}-${alertEntity.vymi_id_dtb}`,
                            language: "cs",
                        },
                    ],
                },
                headerText: {
                    translation: [
                        {
                            text: alertEntity.title,
                            language: "cs",
                        },
                    ],
                },
                descriptionText: {
                    translation: [
                        {
                            text: this.getDescription(alertEntity),
                            language: "cs",
                        },
                    ],
                },
            },
        };
    };

    public getDescription = (alertEntity: IRopidVYMIEventOutput): string => {
        const description: string[] = [
            HTMLUtils.outputPlainText(alertEntity.description) ?? "",
            HTMLUtils.outputPlainText(alertEntity.dpp_action) ?? "",
            HTMLUtils.outputPlainText(alertEntity.ropid_action) ?? "",
        ];
        return description.join(" ").replaceAll("\n", " ").trim();
    };

    public getActivePeriod = (alertEntity: IRopidVYMIEventOutput): GtfsTimeRange => {
        if (!alertEntity.expiration_date) {
            return {
                start: new Date(alertEntity.time_from).getTime() / 1000,
            };
        }
        return {
            start: new Date(alertEntity.time_from).getTime() / 1000,
            end: new Date(alertEntity.expiration_date).getTime() / 1000,
        };
    };

    public async getAffectedRoutes(alertEntity: IRopidVYMIEventOutput): Promise<InformedEntityRoute[]> {
        const affectedRotues = await this.eventsRoutesModel.findByEventId(alertEntity.vymi_id);
        const routeNames: InformedEntityRoute[] = [];
        for (const route of affectedRotues) {
            routeNames.push({
                routeId: route.gtfs_route_id ?? route.name,
            });
        }
        return routeNames;
    }
}
