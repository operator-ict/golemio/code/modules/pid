import { GtfsAlertCauseEnum } from "./enums/AlertCauseEnum";

export function getGtfsCause(causeText: string): GtfsAlertCauseEnum {
    for (const alertKey in GtfsAlertCauseMap) {
        const alertCause: GtfsAlertCauseMapping = GtfsAlertCauseMap[alertKey];
        for (const causeStringKey in alertCause.strings) {
            if (causeText.includes(alertCause.strings[causeStringKey])) {
                return alertCause.cause;
            }
        }
    }

    return GtfsAlertCauseEnum.UNKNOWN_CAUSE;
}

interface GtfsAlertCauseMapping {
    strings: string[];
    cause: GtfsAlertCauseEnum;
}

const GtfsAlertCauseMap: GtfsAlertCauseMapping[] = [
    {
        strings: ["nehoda", "nehody", "překážka", "srážka", "střet", "srážky"],
        cause: GtfsAlertCauseEnum.ACCIDENT,
    },
    {
        strings: ["personální", "stávka"],
        cause: GtfsAlertCauseEnum.STRIKE,
    },
    {
        strings: ["porucha", "závada", "výpadek", "provozni", "kolejnice"],
        cause: GtfsAlertCauseEnum.TECHNICAL_PROBLEM,
    },
    {
        strings: ["uzavřená", "havárie", "oprava", "omezení", "uzavírka", "uzavírky", "práce"],
        cause: GtfsAlertCauseEnum.CONSTRUCTION,
    },
    {
        strings: ["sjízdné", "sjízdnost", "nesjízdná", "povětrnostní", "strom", "klimatické", "vítr"],
        cause: GtfsAlertCauseEnum.WEATHER,
    },
    {
        strings: ["demonstrace", "manifestace"],
        cause: GtfsAlertCauseEnum.DEMONSTRATION,
    },
    {
        strings: ["uzávěra", "pčr", "policie"],
        cause: GtfsAlertCauseEnum.POLICE_ACTIVITY,
    },
    {
        strings: ["zásah"],
        cause: GtfsAlertCauseEnum.MEDICAL_EMERGENCY,
    },
    {
        strings: ["silná"],
        cause: GtfsAlertCauseEnum.OTHER_CAUSE,
    },
];
