import { IGtfsRtTripDto } from "#sch/vehicle-positions/models/interfaces/IGtfsRtTripDto";
import { IVehicleDescriptor } from "../interfaces/GtfsRealtimeInterfaces";
import { VehicleIdGenerator } from "./VehicleIdGenerator";

export class VehicleDescriptor {
    public getVehicleDescriptor(tripRecord: IGtfsRtTripDto): IVehicleDescriptor {
        return {
            id: VehicleIdGenerator.getVehicleId(
                tripRecord.id,
                tripRecord.gtfs_route_type,
                tripRecord.provider_source_type,
                tripRecord.gtfs_route_short_name,
                tripRecord.cis_trip_number,
                tripRecord.vehicle_registration_number,
                tripRecord.run_number,
                tripRecord.internal_run_number
            ),
            ...(tripRecord.vehicle_registration_number && {
                label: tripRecord.vehicle_registration_number.toString(),
            }),
            ".transit_realtime.ovapiVehicleDescriptor": {
                wheelchairAccessible: tripRecord.wheelchair_accessible ?? undefined,
                vehicleType: tripRecord.vehicle_descriptor
                    ? JSON.stringify({
                          airConditioned: tripRecord.vehicle_descriptor.is_air_conditioned,
                          usbChargers: tripRecord.vehicle_descriptor.has_usb_chargers,
                      })
                    : undefined,
            },
        };
    }
}
