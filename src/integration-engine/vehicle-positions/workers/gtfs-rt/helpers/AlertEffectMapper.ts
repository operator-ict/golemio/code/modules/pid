import { GtfsAlertEffectEnum } from "./enums/AlertEffectEnum";

const RopidVyMiEventType = {
    PROVOZ_ZASTAVEN: 1,
    PROVOZ_OMEZEN: 3,
    PROVOZ_OBNOVEN: 4,
    PRERUSENI_PROVOZU_METRA: 5,
    PRERUSENI_PROVOZU_TRAMVAJI: 6,
    NEODJETI_SPOJE: 8,
    ZPOZDENI_SPOJE: 12,
    ZPOZDENI_SPOJU: 13,
    STANICE_UZAVRENA: 14,
    PRISTUP_OMEZEN: 15,
    OMEZENI_BEZBARIEROVEHO_PRISTUPU: 16,
    ZMENA_TRASY_A_ZASTAVEK: 17,
    POSILENI_SPOJU: 18,
    ROZVAZANI_PRESTUPNI_VAZBY: 19,
    OSTATNI: 20,
    ODKLON: 21,
    ZRUSENI_ZASTAVKY: 22,
    PREMISTENI_ZASTAVKY: 23,
    NAHRADNI_DOPRAVA: 24,
    PRERUSENI_PROVOZU: 25,
    ZRUSENI_LINKY: 26,
    ZAVEDENI_LINKY: 27,
    ZMENA_TRASY: 28,
    ZMENA_ZASTAVEK: 29,
    ZMENA_JIZDNIHO_RADU: 30,
    OMEZENI_PROVOZU: 31,
};

/**
 * Mapuje eventType na dopravní efekt. EventType může být i součet několika čísel RopidVyMiEventType.
 * @param eventType number
 */
export function getGtfsEffectByEventType(eventType: number): GtfsAlertEffectEnum {
    const values: number[] = findFromBin(eventType);

    if (values.includes(RopidVyMiEventType.ODKLON)) {
        return GtfsAlertEffectEnum.DETOUR;
    }

    if (values.includes(RopidVyMiEventType.ROZVAZANI_PRESTUPNI_VAZBY) || values.includes(RopidVyMiEventType.NAHRADNI_DOPRAVA)) {
        return GtfsAlertEffectEnum.MODIFIED_SERVICE;
    }

    if (
        values.includes(RopidVyMiEventType.PROVOZ_ZASTAVEN) ||
        values.includes(RopidVyMiEventType.PROVOZ_OMEZEN) ||
        values.includes(RopidVyMiEventType.NEODJETI_SPOJE) ||
        values.includes(RopidVyMiEventType.PRERUSENI_PROVOZU_METRA) ||
        values.includes(RopidVyMiEventType.PRERUSENI_PROVOZU_TRAMVAJI) ||
        values.includes(RopidVyMiEventType.PRERUSENI_PROVOZU) ||
        values.includes(RopidVyMiEventType.OMEZENI_PROVOZU)
    ) {
        return GtfsAlertEffectEnum.REDUCED_SERVICE;
    }

    if (values.includes(RopidVyMiEventType.ZPOZDENI_SPOJE) || values.includes(RopidVyMiEventType.ZPOZDENI_SPOJU)) {
        return GtfsAlertEffectEnum.SIGNIFICANT_DELAYS;
    }

    if (values.includes(RopidVyMiEventType.POSILENI_SPOJU) || values.includes(RopidVyMiEventType.ZAVEDENI_LINKY)) {
        return GtfsAlertEffectEnum.ADDITIONAL_SERVICE;
    }

    if (
        values.includes(RopidVyMiEventType.OSTATNI) ||
        values.includes(RopidVyMiEventType.PROVOZ_OBNOVEN) ||
        values.includes(RopidVyMiEventType.PRISTUP_OMEZEN) ||
        values.includes(RopidVyMiEventType.OMEZENI_BEZBARIEROVEHO_PRISTUPU)
    ) {
        return GtfsAlertEffectEnum.OTHER_EFFECT;
    }

    if (
        values.includes(RopidVyMiEventType.STANICE_UZAVRENA) ||
        values.includes(RopidVyMiEventType.ZRUSENI_ZASTAVKY) ||
        values.includes(RopidVyMiEventType.ZRUSENI_LINKY)
    ) {
        return GtfsAlertEffectEnum.NO_SERVICE;
    }

    if (values.includes(RopidVyMiEventType.ZMENA_ZASTAVEK)) {
        return GtfsAlertEffectEnum.MODIFIED_SERVICE;
    }

    return GtfsAlertEffectEnum.UNKNOWN_EFFECT;
}

/**
 * Odhalil @OICT_ParoubekMartin
 * Příklad jak 10551296 přeložit na 17, 22 a 24
 * Desítková: 10551296
 * Binárně: 1010 0001 0000 0000 0000 0000
 * Na 17 místě zprava je 1
 * Na 22 místě zprava je 1
 * Na 24 místě zprava je 1
 *
 * @param num číslo v desítkové soustavě
 **/
const findFromBin = (num: number): number[] => {
    let result = [];
    const bin = (num >>> 0).toString(2);
    for (let i = 0; i < bin.length; i++) {
        if (bin[i] === "1") result.push(bin.length - i);
    }
    return result;
};
