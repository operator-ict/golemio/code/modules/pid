import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";
import { ProviderSourceTypeEnum } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/ProviderSourceTypeEnum";

export class VehicleIdGenerator {
    public static getVehicleId(
        trip_id: string,
        gtfs_route_type: GTFSRouteTypeEnum | null,
        provider_source_type: ProviderSourceTypeEnum,
        gtfs_route_short_name: string | null,
        cis_trip_number: number | null,
        vehicle_registration_number: number | null,
        run_number: number | null,
        internal_run_number: number | null
    ): string {
        if (gtfs_route_type === GTFSRouteTypeEnum.TRAIN) {
            return "train-" + cis_trip_number;
        }

        if (gtfs_route_type === GTFSRouteTypeEnum.METRO) {
            return `metro-${gtfs_route_short_name}-${internal_run_number}-${run_number}`;
        }

        if (provider_source_type === ProviderSourceTypeEnum.TcpRegionalBus) {
            const externalTripId = trip_id.split("_").at(-1)!;
            return `service-${gtfs_route_type}-${externalTripId}`;
        }

        return "service-" + gtfs_route_type + "-" + vehicle_registration_number;
    }
}
