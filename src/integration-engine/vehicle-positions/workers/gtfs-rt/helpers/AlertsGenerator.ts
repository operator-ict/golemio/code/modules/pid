import { RopidVYMIEventsModel, RopidVYMIEventsRoutesModel } from "#ie/ropid-vymi";
import { transit_realtime as gtfsRealtime } from "@golemio/ovapi-gtfs-realtime-bindings";
import { GtfsRtRedisRepository } from "../data-access/GtfsRtRedisRepository";
import { IFeedHeader } from "../interfaces/GtfsRealtimeInterfaces";
import { AlertsTransformation } from "../transformations/AlertsTransformation";

export class AlertsGenerator {
    private alertsTransformation: AlertsTransformation;

    constructor(
        private readonly gtfsRtRedisRepository: GtfsRtRedisRepository,
        private readonly vymiEventsRepository: RopidVYMIEventsModel,
        vymiRoutesRepository: RopidVYMIEventsRoutesModel
    ) {
        this.alertsTransformation = new AlertsTransformation(vymiRoutesRepository);
    }

    public async generateAlerts(feedHeader: IFeedHeader): Promise<void> {
        const alertsMessage = gtfsRealtime.FeedMessage.create({ header: feedHeader });
        const alertEntities = await this.vymiEventsRepository.getCurrentAlerts(
            feedHeader.timestamp ? new Date((feedHeader.timestamp as number) * 1000) : new Date()
        );

        const transformedAlerts = await this.alertsTransformation.transform(alertEntities);
        for (const transformedEntity of transformedAlerts) {
            const feedEntity = gtfsRealtime.FeedEntity.fromObject(transformedEntity);
            alertsMessage.entity.push(feedEntity);
        }

        if (gtfsRealtime.FeedMessage.verify(alertsMessage) === null) {
            const buffer = gtfsRealtime.FeedMessage.encode(alertsMessage).finish() as Buffer;

            await this.gtfsRtRedisRepository.set("alerts" + "_timestamp", alertsMessage.header.timestamp);
            await this.gtfsRtRedisRepository.hset("alerts.pb", buffer.toString("binary"));
        }
    }
}
