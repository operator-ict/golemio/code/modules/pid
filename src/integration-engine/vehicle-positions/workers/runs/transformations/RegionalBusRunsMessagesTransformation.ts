import { IRegionalBusRunsMessagesModel } from "#sch/vehicle-positions/models/interfaces/IRegionalBusRunsMessagesModel";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { IRegionalBusRunsMessagePropertiesWrapper } from "../interfaces/RegionalBusRunsMessageInterfaces";
import { TimestampValidator } from "../helpers/TimestampValidator";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";

type TransformIn = IRegionalBusRunsMessagePropertiesWrapper;
type TransformOut = IRegionalBusRunsMessagesModel;

@injectable()
export class RegionalBusRunsMessagesTransformation extends AbstractTransformation<TransformIn, TransformOut> {
    public name = "RegionalBusRunsMessagesTransformation";

    protected transformInternal = (data: TransformIn): TransformOut => {
        const messageData = data.$;

        return {
            external_trip_id: messageData.imei,
            cis_line_id: messageData.line && messageData.line !== "0" ? messageData.line : null,
            cis_trip_number: messageData.conn && messageData.conn !== "0" ? Number.parseInt(messageData.conn) : null,
            events: messageData.events ?? "",
            coordinates: {
                type: "Point",
                coordinates: [Number.parseFloat(messageData.lng), Number.parseFloat(messageData.lat)],
            },
            vehicle_timestamp: new Date(messageData.tm + "Z"),
            registration_number: messageData.evc && messageData.evc !== "0" ? Number.parseInt(messageData.evc) : null,
            speed_kmh: Number.parseInt(messageData.rych),
            bearing: Number.parseInt(messageData.smer),
            is_terminated: messageData.conn === "0" || messageData.line === "0",
            timestamp: messageData.tm,
        };
    };
}
