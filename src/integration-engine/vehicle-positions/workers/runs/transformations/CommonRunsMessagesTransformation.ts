import { ICommonRunWithMessageDto } from "#sch/vehicle-positions/models/interfaces/ICommonRunWithMessageDto";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { CommonRunHelper } from "../helpers/CommonRunHelper";
import { ICommonRunHelper } from "../helpers/interfaces/ICommonRunHelper";
import { ICommonRunsInputData, ICommonRunsInputElement } from "../interfaces/CommonRunsMessageInterfaces";
import moment from "@golemio/core/dist/shared/moment-timezone";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class CommonRunsMessagesTransformation extends BaseTransformation implements ITransformation {
    public name = "CommonRunsMessagesTransformation";
    private readonly commonRunHelper: ICommonRunHelper;

    constructor() {
        super();
        this.commonRunHelper = new CommonRunHelper();
    }

    /**
     * Overrides BaseTransformation::transform
     */
    public transform = async (
        { data, timestamp }: ICommonRunsInputData,
        isTram: boolean = false
    ): Promise<ICommonRunWithMessageDto[]> => {
        const now = moment(timestamp).tz("Europe/Prague").format();
        const res: ICommonRunWithMessageDto[] = [];

        if (!Array.isArray(data)) {
            data = [data];
        }
        for (const element of data) {
            try {
                const elementTransformed = await this.transformElement({ data: element, timestamp: now }, isTram);
                res.push(elementTransformed);
            } catch (err) {
                log.verbose(`${this.name}: invalid run parameters: ${JSON.stringify(element).substring(0, 2000)}`);
            }
        }
        return res;
    };

    protected transformElement = async (
        element: ICommonRunsInputElement,
        isTram: boolean = false
    ): Promise<ICommonRunWithMessageDto> => {
        const attributes = element.data.$;
        if (!isTram && !attributes.tjr) {
            return Promise.reject();
        }

        const identifiers = this.commonRunHelper.getRunIdentifiers(attributes, element.timestamp);
        const realStopTimestampDate = this.commonRunHelper.parseDateFromRunInput(attributes.takt);
        const messageTimestampDate = this.commonRunHelper.parseDateFromRunInput(attributes.tm);

        if (!identifiers || !realStopTimestampDate || !messageTimestampDate) {
            return Promise.reject();
        }

        const scheduledStopTimestampDate = attributes.tjr ? this.commonRunHelper.parseDateFromRunInput(attributes.tjr) : null;
        const t = {
            run: {
                id: identifiers.runId,
                route_id: identifiers.routeId,
                run_number: Number.parseInt(identifiers.runNumber),
                line_short_name: attributes.line,
                registration_number: attributes.evc,
                msg_start_timestamp: element.timestamp,
                msg_last_timestamp: element.timestamp,
                wheelchair_accessible: attributes.np === "ano",
            },
            run_message: {
                // id - autoincrement
                // runs_id - associated fk
                lat: +attributes.lat,
                lng: +attributes.lng,
                actual_stop_asw_id: attributes.akt,
                actual_stop_timestamp_real: realStopTimestampDate,
                actual_stop_timestamp_scheduled: scheduledStopTimestampDate,
                last_stop_asw_id: attributes.konc,
                packet_number: attributes.pkt,
                msg_timestamp: messageTimestampDate,
                events: attributes.events,
            },
        };

        return t;
    };
}
