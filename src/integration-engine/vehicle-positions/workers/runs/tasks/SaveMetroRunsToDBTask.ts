import { QueueManager } from "@golemio/core/dist/integration-engine/queueprocessors";
import { AbstractTask } from "@golemio/core/dist/integration-engine/workers";
import { MessageProperties } from "amqplib";
import { MetroRunsMessagesRepository } from "../data-access/MetroRunsMessagesRepository";
import { IMetroRunsInput } from "../interfaces/MetroRunsMessageInterfaces";
import { MetroRunsValidationSchema } from "../schema/MetroRunsSchema";
import { MetroRunsMessagesTransformation } from "../transformations/MetroRunsMessagesTransformation";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { VPContainer } from "#ie/vehicle-positions/ioc/Di";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { WORKER_NAME } from "../constants";
import { TimestampValidator } from "../helpers/TimestampValidator";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers";

@injectable()
export class SaveMetroRunsToDBTask extends AbstractTask<IMetroRunsInput> {
    public readonly queueName = "saveMetroRunsToDB";
    public readonly queueTtl = 3 * 60 * 1000; // 3 minutes
    public readonly schema = MetroRunsValidationSchema;

    private readonly messagesTransformation: MetroRunsMessagesTransformation;
    private readonly messagesRepository: MetroRunsMessagesRepository;

    constructor(
        @inject(VPContainerToken.TimestampValidator) private timestampValidator: TimestampValidator,
        @inject(CoreToken.Logger) private logger: ILogger
    ) {
        super(WORKER_NAME);
        this.messagesTransformation = VPContainer.resolve<MetroRunsMessagesTransformation>(
            VPContainerToken.MetroRunsMessagesTransformation
        );
        this.messagesRepository = new MetroRunsMessagesRepository();
    }

    protected async execute(data: IMetroRunsInput, msgProperties?: MessageProperties) {
        const timestamp = msgProperties?.timestamp;
        if (!timestamp) {
            throw new GeneralError(`Missing metro run message timestamp: ${JSON.stringify(data)}`, this.constructor.name);
        }

        if (!this.timestampValidator.isTimestampValid(timestamp, data.m.$.tm)) {
            this.logger.error(
                new GeneralError(
                    `Message timestamp 'tm' of value ${data.m.$.tm} is not valid ` +
                        `for line ${data.m.$.linka}, gvd ${data.m.$.gvd}`,
                    this.constructor.name,
                    undefined,
                    undefined,
                    "pid"
                )
            );
            return Promise.resolve();
        }

        const messages = await this.messagesTransformation.transform({ data });
        if (messages.length === 0) {
            return;
        }

        await this.messagesRepository.bulkSave(messages);
        await QueueManager.sendMessageToExchange(
            this.queuePrefix,
            "processMetroRunMessages",
            {
                routeName: data.m.$.linka,
                messages,
            },
            { timestamp: msgProperties?.timestamp }
        );
    }
}
