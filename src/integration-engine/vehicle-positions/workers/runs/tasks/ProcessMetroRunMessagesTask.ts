import { RunTripsRedisRepository } from "#ie/ropid-gtfs/data-access/cache";
import { TripScheduleRepository } from "#ie/ropid-gtfs/data-access/precomputed";
import { IAggregatedRailtrackGPSData } from "#ie/ropid-gtfs/workers/timetables/tasks/interfaces/IAggregatedRailtrackGPSData";
import {
    IExtendedMetroRunInputForProcessing,
    IMetroRunInputForProcessing,
} from "#ie/vehicle-positions/workers/runs/interfaces/IMetroRunInputForProcessing";
import { PositionsRepository } from "#ie/vehicle-positions/workers/vehicle-positions/data-access/PositionsRepository";
import { TripsRepository } from "#ie/vehicle-positions/workers/vehicle-positions/data-access/TripsRepository";
import { AbstractGTFSTripRunManager } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/gtfs-trip-run/AbstractGTFSTripRunManager";
import {
    GTFSTripRunManagerFactory,
    GTFSTripRunType,
} from "#ie/vehicle-positions/workers/vehicle-positions/helpers/gtfs-trip-run/GTFSTripRunManagerFactory";
import { VehiclePositions } from "#sch/vehicle-positions";
import { AbstractTask, QueueManager, config } from "@golemio/core/dist/integration-engine";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { Op, Sequelize } from "@golemio/core/dist/shared/sequelize";
import { MessageProperties } from "amqplib";
import { CommonRunsRepository } from "../data-access/CommonRunsRepository";
import { MetroMessageFilter } from "../helpers/MetroMessageFilter";
import { IProcessMetroRunsMessage, IProcessMetroRunsMessagesInput } from "../interfaces/IProcessMetroRunsMessagesInput";
import { MetroTransformedRunsValidationSchema } from "../schema/MetroTransformedRunsSchema";
import { MetroRunsMessageProcessingTransformation } from "../transformations/MetroRunsMessageProcessingTransformation";
import { WORKER_NAME } from "../constants";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { MetroRailtrackGPSRepository } from "#ie/ropid-gtfs/workers/timetables/tasks/data-access/MetroRailtrackGPSRepository";

@injectable()
export class ProcessMetroRunMessagesTask extends AbstractTask<IProcessMetroRunsMessagesInput> {
    public readonly queueName = "processMetroRunMessages";
    public readonly queueTtl = 10 * 60 * 1000; // 10 minutes
    public readonly schema = MetroTransformedRunsValidationSchema;

    private readonly processingTransformation: MetroRunsMessageProcessingTransformation;
    private readonly railtrackGPSRepository: MetroRailtrackGPSRepository;
    private readonly positionsRepository: PositionsRepository;
    private readonly tripsRepository: TripsRepository;
    private readonly runsRepository: CommonRunsRepository;
    private readonly runTripsRedisRepository: RunTripsRedisRepository;
    private readonly gtfsTripRunManager: AbstractGTFSTripRunManager;
    private readonly tripScheduleRepository: TripScheduleRepository;

    constructor() {
        super(WORKER_NAME);
        this.processingTransformation = new MetroRunsMessageProcessingTransformation();
        this.railtrackGPSRepository = new MetroRailtrackGPSRepository();
        this.positionsRepository = new PositionsRepository();
        this.tripsRepository = new TripsRepository();
        this.runsRepository = new CommonRunsRepository();
        this.runTripsRedisRepository = new RunTripsRedisRepository();
        this.tripScheduleRepository = new TripScheduleRepository();

        this.gtfsTripRunManager = GTFSTripRunManagerFactory.create(
            GTFSTripRunType.Metro,
            this.positionsRepository,
            this.tripsRepository,
            this.runsRepository,
            this.runTripsRedisRepository
        );
    }

    protected async execute({ routeName, messages }: IProcessMetroRunsMessagesInput, msgProperties?: MessageProperties) {
        const { filteredMessages, trackIds } = MetroMessageFilter.getValidMessagesWithTrackIds(messages);
        if (filteredMessages.length === 0) {
            return;
        }

        const gpsData = await this.railtrackGPSRepository.findCoordinates(routeName, trackIds);

        for (const message of filteredMessages) {
            await this.processMessage(message, gpsData, msgProperties?.timestamp);
        }
    }

    private async processMessage(
        message: IProcessMetroRunsMessage,
        gpsData: IAggregatedRailtrackGPSData | null,
        firstMessageCreatedAt?: number
    ) {
        const data = await this.processingTransformation.transform({ message, gpsData });
        if (!data) {
            return;
        }

        let enhancedMessage: IExtendedMetroRunInputForProcessing | null = null;
        try {
            enhancedMessage = await this.enrichRunMessage(data.runInput);
        } catch (err) {
            throw new GeneralError(`processMessage: error while fetching trip schedule`, this.constructor.name, err);
        }

        if (!enhancedMessage) {
            log.verbose(`processMessage: no trip_id for metro run: ${JSON.stringify(data.runInput)}`);
            return;
        }

        const scheduledTrips = await this.gtfsTripRunManager.setAndReturnScheduledTrips({
            ...data.runSchedule,
            // try to use internal run number if available (input run message is not always reliable)
            run_number: enhancedMessage.internalRunNumber ?? data.runSchedule.run_number,
        });

        if (scheduledTrips.length === 0) {
            log.verbose(`processMessage: no schedule for metro run: ${JSON.stringify(data.runSchedule)}`);
            return;
        }

        const delayMsg = await this.gtfsTripRunManager.generateDelayMsg(scheduledTrips, enhancedMessage);
        if (delayMsg.updatedTrips.length > 0) {
            await QueueManager.sendMessageToExchange(
                `${config.RABBIT_EXCHANGE_NAME}.${VehiclePositions.name.toLowerCase()}`,
                "updateDelay",
                delayMsg,
                { timestamp: firstMessageCreatedAt }
            );
        }
    }

    private async enrichRunMessage(runInput: IMetroRunInputForProcessing): Promise<IExtendedMetroRunInputForProcessing | null> {
        const tripSchedule = await this.tripScheduleRepository.findOne({
            attributes: ["trip_id", "run_number"],
            where: {
                trip_number: runInput.trainNumber,
                route_id: `L${runInput.routeId}`,
                [Op.and]: [Sequelize.literal("start_timestamp::date = current_date")],
            },
        });

        if (!tripSchedule) {
            return null;
        }

        return {
            ...runInput,
            tripId: tripSchedule.trip_id,
            internalRunNumber: tripSchedule.run_number,
        };
    }
}
