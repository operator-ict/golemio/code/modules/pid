import { GTFSRouteTypeEnum } from "#helpers/RouteTypeEnums";
import { RouteSubAgencyRepository } from "#ie/ropid-gtfs/data-access/RouteSubAgencyRepository";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { VehiclePositions } from "#sch/vehicle-positions";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { AbstractTask, QueueManager } from "@golemio/core/dist/integration-engine";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { MessageProperties } from "amqplib";
import { DescriptorRepository } from "../../vehicle-descriptors/data-access/DescriptorRepository";
import { AbstractGTFSTripRunManager } from "../../vehicle-positions/helpers/gtfs-trip-run/AbstractGTFSTripRunManager";
import { GtfsTripRegionalBusRunManager } from "../../vehicle-positions/helpers/regional-bus/gtfs-trip-run/GtfsTripRegionalBusRunManager";
import { WORKER_NAME } from "../constants";
import { IExtendedSchedule, IExtendedScheduleWithExternalId } from "../helpers/regional-bus/interfaces/IExtendedSchedule";
import { IGtfsLookupManager } from "../helpers/regional-bus/interfaces/IGtfsLookupManager";
import { ITripScheduleManager } from "../helpers/regional-bus/interfaces/ITripScheduleManager";
import {
    IProcessRegionalBusRunMessage,
    IProcessRegionalBusRunMessagesInput,
} from "../interfaces/IProcessRegionalBusRunMessagesInput";
import { IRegionalBusRunInputWithMetadata } from "../interfaces/IRegionalBusRunInputWithMetadata";
import { RegionalBusTransformedRunsValidationSchema } from "../schema/RegionalBusTransformedRunsSchema";

@injectable()
export class ProcessRegionalBusRunMessagesTask extends AbstractTask<IProcessRegionalBusRunMessagesInput> {
    public readonly queueName = "processRegionalBusRunMessages";
    public readonly queueTtl = 5 * 60 * 1000; // 5 minutes
    public readonly schema = RegionalBusTransformedRunsValidationSchema;

    private readonly routeSubAgencyRepository: RouteSubAgencyRepository;
    private readonly gtfsTripRunManager: AbstractGTFSTripRunManager;

    constructor(
        @inject(CoreToken.SimpleConfig) private config: ISimpleConfig,
        @inject(VPContainerToken.DescriptorRepository)
        private descriptorRepository: DescriptorRepository,
        @inject(VPContainerToken.GtfsLookupManager)
        private gtfsLookupManager: IGtfsLookupManager,
        @inject(VPContainerToken.TripScheduleManager)
        private tripScheduleManager: ITripScheduleManager
    ) {
        super(WORKER_NAME);

        this.routeSubAgencyRepository = new RouteSubAgencyRepository();
        this.gtfsTripRunManager = new GtfsTripRegionalBusRunManager();
    }

    protected async execute({ messages }: IProcessRegionalBusRunMessagesInput, msgProperties?: MessageProperties) {
        let gtfsTripByExternalId = await this.getGtfsTripsFromCache(messages);
        const messagesWithoutGtfsData = messages.filter((message) => !gtfsTripByExternalId.has(message.external_trip_id));

        if (messagesWithoutGtfsData.length > 0) {
            const scheduleInputData = await this.getGtfsTripsFromDb(messagesWithoutGtfsData);
            for (const schedule of scheduleInputData) {
                gtfsTripByExternalId.set(schedule.cache_lookup_key, schedule);
            }

            await this.gtfsLookupManager.setGtfsScheduleLookup(scheduleInputData);
        }

        for (const message of messages) {
            const schedule = gtfsTripByExternalId.get(message.external_trip_id);
            if (!schedule) {
                continue;
            }

            const scheduledTrips = await this.gtfsTripRunManager.setAndReturnScheduledTrips(schedule);
            if (scheduledTrips.length === 0) {
                continue;
            }

            const runInput: IRegionalBusRunInputWithMetadata = {
                runMessage: message,
                gtfsTripId: schedule.gtfs_trip_id,
                isWheelchairAccessible: schedule.is_wheelchair_accessible,
                agencyName: schedule.agency_name,
            };

            const delayMsg = await this.gtfsTripRunManager.generateDelayMsg(scheduledTrips, runInput);

            if (delayMsg.updatedTrips.length > 0) {
                const rabbitExchangeName = this.config.getValue<string>("env.RABBIT_EXCHANGE_NAME");
                await QueueManager.sendMessageToExchange(
                    `${rabbitExchangeName}.${VehiclePositions.name.toLowerCase()}`,
                    "processRegionalBusPositions",
                    delayMsg,
                    { timestamp: msgProperties?.timestamp }
                );
            }
        }
    }

    private async getGtfsTripsFromCache(messages: IProcessRegionalBusRunMessage[]): Promise<Map<string, IExtendedSchedule>> {
        const gtfsTripByExternalId = new Map<string, IExtendedSchedule>();
        const scheduleInputData = await this.gtfsLookupManager.getScheduleInputData(messages);

        for (let i = 0; i < messages.length; i++) {
            const message = messages[i];
            const schedule = scheduleInputData[i];

            if (schedule) {
                gtfsTripByExternalId.set(message.external_trip_id, schedule);
                continue;
            }
        }

        return gtfsTripByExternalId;
    }

    private async getGtfsTripsFromDb(messages: IProcessRegionalBusRunMessage[]): Promise<IExtendedScheduleWithExternalId[]> {
        let cisLineIds = new Set<string>();
        let registrationNumbers = new Set<number>();

        for (const message of messages) {
            cisLineIds.add(message.cis_line_id);
            registrationNumbers.add(message.registration_number);
        }

        const subAgencyEntities = await this.routeSubAgencyRepository.findSubAgenciesByCisLineIds(Array.from(cisLineIds));
        const descriptorEntities = await this.descriptorRepository.findAccessDescByRegistrationNumbers(
            Array.from(registrationNumbers),
            GTFSRouteTypeEnum.BUS
        );

        let scheduleInputData: IExtendedScheduleWithExternalId[] = [];

        for (const message of messages) {
            const scheduleInputItem = await this.tripScheduleManager.getScheduleInput(
                message,
                subAgencyEntities,
                descriptorEntities
            );

            if (!scheduleInputItem) {
                continue;
            }

            scheduleInputData.push(scheduleInputItem);
        }

        return scheduleInputData;
    }
}
