import { VPContainer } from "#ie/vehicle-positions/ioc/Di";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { AbstractTask } from "@golemio/core/dist/integration-engine/workers";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { MessageProperties } from "amqplib";
import { CommonRunsRepository } from "../data-access/CommonRunsRepository";
import { CommonMessageProcessor } from "../helpers/CommonMessageProcessor";
import { ICommonMessageFilter } from "../helpers/interfaces/ICommonMessageFilter";
import { ICommonRunsInput } from "../interfaces/CommonRunsMessageInterfaces";
import { CommonRunsValidationSchema } from "../schema/CommonRunsSchema";
import { CommonRunsMessagesTransformation } from "../transformations/CommonRunsMessagesTransformation";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { WORKER_NAME } from "../constants";
import { TramMessageFilter } from "../helpers/TramMessageFilter";

@injectable()
export class SaveTramRunsToDBTask extends AbstractTask<ICommonRunsInput> {
    public readonly queueName = "saveTramRunsToDB";
    public readonly queueTtl = 3 * 60 * 1000; // 3 minutes
    public readonly schema = CommonRunsValidationSchema;

    private readonly messagesTransformation: CommonRunsMessagesTransformation;
    private readonly runsRepository: CommonRunsRepository;
    private readonly messageProcessor: CommonMessageProcessor;
    private readonly messageFilter: ICommonMessageFilter;

    constructor() {
        super(WORKER_NAME);
        this.messagesTransformation = VPContainer.resolve<CommonRunsMessagesTransformation>(
            VPContainerToken.CommonRunsMessagesTransformation
        );
        this.runsRepository = new CommonRunsRepository();
        this.messageProcessor = new CommonMessageProcessor(this.runsRepository);
        this.messageFilter = VPContainer.resolve<TramMessageFilter>(VPContainerToken.TramMessageFilter);
    }

    protected async execute(data: ICommonRunsInput, msgProperties?: MessageProperties) {
        const timestamp = msgProperties?.timestamp;
        if (!timestamp) {
            throw new GeneralError(`Missing tram run message timestamp: ${JSON.stringify(data)}`, this.constructor.name);
        }

        const messages = await this.messagesTransformation.transform({ data: data.M.V, timestamp }, true);
        const filteredMessages = this.messageFilter.yieldFilteredMessages(messages, timestamp);
        for (const runMessage of filteredMessages) {
            await this.messageProcessor.processTransformedRun(runMessage, timestamp);
        }
    }
}
