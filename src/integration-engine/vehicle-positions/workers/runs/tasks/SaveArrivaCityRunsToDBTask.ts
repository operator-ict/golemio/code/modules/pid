import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { QueueManager } from "@golemio/core/dist/integration-engine/queueprocessors";
import { AbstractTask } from "@golemio/core/dist/integration-engine/workers";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { MessageProperties } from "amqplib";
import { WORKER_NAME } from "../constants";
import { IRegionalBusRunsInput } from "../interfaces/RegionalBusRunsMessageInterfaces";
import { RegionalBusRunsValidationSchema } from "../schema/RegionalBusRunsSchema";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { RegionalBusRunsFacade } from "../helpers/regional-bus/RegionalBusRunsFacade";

@injectable()
export class SaveArrivaCityRunsToDBTask extends AbstractTask<IRegionalBusRunsInput> {
    public readonly queueName = "saveArrivaCityRunsToDB";
    public readonly queueTtl = 3 * 60 * 1000; // 3 minutes
    public readonly schema = RegionalBusRunsValidationSchema;

    constructor(@inject(VPContainerToken.RegionalBusRunsFacade) private regionalBusRunsFacade: RegionalBusRunsFacade) {
        super(WORKER_NAME);
    }

    protected async execute(data: IRegionalBusRunsInput, msgProperties?: MessageProperties) {
        const inputMessages = Array.isArray(data.M.V) ? data.M.V : [data.M.V];
        const timestamp = msgProperties?.timestamp;
        if (!timestamp) {
            throw new GeneralError(`Missing ArrivaCity run message timestamp: ${JSON.stringify(data)}`, this.constructor.name);
        }
        const transformedMessages = await this.regionalBusRunsFacade.transformAndSaveMessages({
            messages: inputMessages,
            timestamp,
        });
        if (transformedMessages.length === 0) {
            return;
        }

        await QueueManager.sendMessageToExchange(
            this.queuePrefix,
            "processRegionalBusRunMessages",
            {
                messages: transformedMessages,
            },
            { timestamp: msgProperties?.timestamp }
        );
    }
}
