import { VPContainer } from "#ie/vehicle-positions/ioc/Di";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { AbstractTask, AbstractWorker } from "@golemio/core/dist/integration-engine/workers";
import { WORKER_NAME } from "./constants";
import { ProcessMetroRunMessagesTask } from "./tasks/ProcessMetroRunMessagesTask";
import { SaveBusRunsToDBTask } from "./tasks/SaveBusRunsToDBTask";
import { SaveMetroRunsToDBTask } from "./tasks/SaveMetroRunsToDBTask";
import { SaveTramRunsToDBTask } from "./tasks/SaveTramRunsToDBTask";

export class RunsWorker extends AbstractWorker {
    protected name = WORKER_NAME;

    constructor() {
        super();

        // Register tasks
        this.registerTask(VPContainer.resolve(VPContainerToken.SaveArrivaCityRunsToDBTask));
        this.registerTask(VPContainer.resolve(VPContainerToken.ProcessRegionalBusRunMessagesTask));
        this.registerTask(VPContainer.resolve<SaveTramRunsToDBTask>(VPContainerToken.SaveTramRunsToDBTask));

        this.registerTask(VPContainer.resolve<SaveBusRunsToDBTask>(VPContainerToken.SaveBusRunsToDBTask));
        this.registerTask(VPContainer.resolve<SaveMetroRunsToDBTask>(VPContainerToken.SaveMetroRunsToDBTask));
        this.registerTask(VPContainer.resolve<ProcessMetroRunMessagesTask>(VPContainerToken.ProcessMetroRunMessagesTask));
    }

    public registerTask = (task: AbstractTask<any>): void => {
        super.registerTask(task);
        task.queuePrefix = this.getQueuePrefix();
    };
}
