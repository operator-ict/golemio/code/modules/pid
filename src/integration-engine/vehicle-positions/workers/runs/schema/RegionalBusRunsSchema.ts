import { Type } from "@golemio/core/dist/shared/class-transformer";
import {
    IsISO8601,
    IsNumberString,
    IsObject,
    IsOptional,
    IsString,
    ValidateNested,
} from "@golemio/core/dist/shared/class-validator";
import {
    IRegionalBusRunsInput,
    IRegionalBusRunsMessageContent,
    IRegionalBusRunsMessageProperties,
    IRegionalBusRunsMessagePropertiesWrapper,
} from "../interfaces/RegionalBusRunsMessageInterfaces";

class MessagePropertiesValidationSchema implements IRegionalBusRunsMessageProperties {
    @IsString()
    imei!: string;

    @IsOptional()
    @IsString()
    line?: string;

    @IsOptional()
    @IsString()
    conn?: string;

    @IsOptional()
    @IsString()
    events?: string;

    @IsNumberString()
    lat!: string;

    @IsNumberString()
    lng!: string;

    @IsISO8601()
    tm!: string;

    @IsOptional()
    @IsNumberString()
    evc?: string;

    @IsNumberString()
    rych!: string;

    @IsNumberString()
    smer!: string;
}

class MessagePropertiesWrapperValidationSchema implements IRegionalBusRunsMessagePropertiesWrapper {
    @IsObject()
    @ValidateNested()
    @Type(() => MessagePropertiesValidationSchema)
    $!: IRegionalBusRunsMessageProperties;
}

class MessageContentValidationSchema implements IRegionalBusRunsMessageContent {
    @IsObject({ each: true })
    @ValidateNested()
    @Type(() => MessagePropertiesWrapperValidationSchema)
    V!: IRegionalBusRunsMessagePropertiesWrapper | IRegionalBusRunsMessagePropertiesWrapper[];
}

export class RegionalBusRunsValidationSchema implements IRegionalBusRunsInput {
    @IsObject()
    @ValidateNested()
    @Type(() => MessageContentValidationSchema)
    M!: IRegionalBusRunsMessageContent;
}
