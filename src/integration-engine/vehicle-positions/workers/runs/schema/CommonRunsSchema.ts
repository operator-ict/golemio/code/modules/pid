import { Type } from "@golemio/core/dist/shared/class-transformer";
import { IsIn, IsISO8601, IsObject, IsOptional, IsString, ValidateNested } from "@golemio/core/dist/shared/class-validator";
import { TCPEventEnum } from "src/const";
import {
    ICommonRunsInput,
    ICommonRunsMessageContent,
    ICommonRunsMessageProperties,
    ICommonRunsMessagePropertiesWrapper,
} from "../interfaces/CommonRunsMessageInterfaces";

class MessagePropertiesValidationSchema implements ICommonRunsMessageProperties {
    @IsString()
    turnus!: string;

    @IsString()
    line!: string;

    @IsString()
    evc!: string;

    @IsOptional()
    @IsString()
    np?: string;

    @IsString()
    lat!: string;

    @IsString()
    lng!: string;

    @IsOptional()
    @IsString()
    akt?: string;

    @IsISO8601()
    takt!: string;

    @IsOptional()
    @IsString()
    konc?: string;

    @IsOptional()
    @IsISO8601()
    tjr?: string;

    @IsString()
    pkt!: string;

    @IsISO8601()
    tm!: string;

    @IsIn(Object.values(TCPEventEnum))
    events!: TCPEventEnum;
}

class MessagePropertiesWrapperValidationSchema implements ICommonRunsMessagePropertiesWrapper {
    @IsObject()
    @ValidateNested()
    @Type(() => MessagePropertiesValidationSchema)
    $!: ICommonRunsMessageProperties;
}

class MessageContentValidationSchema implements ICommonRunsMessageContent {
    @IsObject({ each: true })
    @ValidateNested()
    @Type(() => MessagePropertiesWrapperValidationSchema)
    V!: ICommonRunsMessagePropertiesWrapper | ICommonRunsMessagePropertiesWrapper[];
}

export class CommonRunsValidationSchema implements ICommonRunsInput {
    @IsObject()
    @ValidateNested()
    @Type(() => MessageContentValidationSchema)
    M!: ICommonRunsInput["M"];
}
