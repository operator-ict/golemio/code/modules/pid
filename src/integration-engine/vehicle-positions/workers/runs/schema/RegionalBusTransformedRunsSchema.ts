import { Type } from "@golemio/core/dist/shared/class-transformer";
import {
    IsArray,
    IsBoolean,
    IsISO8601,
    IsNumber,
    IsObject,
    IsString,
    ValidateNested,
} from "@golemio/core/dist/shared/class-validator";
import { Point } from "@golemio/core/dist/shared/geojson";
import {
    IProcessRegionalBusRunMessage,
    IProcessRegionalBusRunMessagesInput,
} from "../interfaces/IProcessRegionalBusRunMessagesInput";

class RegionalBusTransformedRunMessagesValidationSchema implements IProcessRegionalBusRunMessage {
    @IsString()
    external_trip_id!: string;

    @IsString()
    cis_line_id!: string;

    @IsNumber()
    cis_trip_number!: number;

    @IsString()
    events!: string;

    @IsObject()
    coordinates!: Point;

    @IsISO8601()
    vehicle_timestamp!: Date;

    @IsNumber()
    registration_number!: number;

    @IsNumber()
    speed_kmh!: number;

    @IsNumber()
    bearing!: number;

    @IsBoolean()
    is_terminated!: boolean;

    @IsString()
    timestamp!: string;
}

export class RegionalBusTransformedRunsValidationSchema implements IProcessRegionalBusRunMessagesInput {
    @IsArray()
    @IsObject({ each: true })
    @ValidateNested()
    @Type(() => RegionalBusTransformedRunMessagesValidationSchema)
    messages!: IProcessRegionalBusRunMessage[];
}
