import { Type } from "@golemio/core/dist/shared/class-transformer";
import {
    IsByteLength,
    IsISO8601,
    IsNumberString,
    IsObject,
    IsOptional,
    IsString,
    ValidateNested,
} from "@golemio/core/dist/shared/class-validator";
import {
    IMetroRunsInput,
    IMetroRunsMessageContent,
    IMetroRunsMessageProperties,
    IMetroRunsTrainContent,
    IMetroRunsTrainProperties,
} from "../interfaces/MetroRunsMessageInterfaces";

class MessagePropertiesValidationSchema implements IMetroRunsMessageProperties {
    @IsString()
    @IsByteLength(1, 1)
    linka!: string;

    @IsISO8601({ strict: true })
    tm!: string;

    @IsOptional()
    @IsString()
    gvd?: string;
}

class TrainPropertiesValidationSchema implements IMetroRunsTrainProperties {
    @IsString()
    csp!: string;

    @IsString()
    csr!: string;

    @IsString()
    cv!: string;

    @IsString()
    ko!: string;

    @IsNumberString()
    odch!: string;
}

class TrainContentValidationSchema implements IMetroRunsTrainContent {
    @IsObject()
    @ValidateNested()
    @Type(() => TrainPropertiesValidationSchema)
    $!: IMetroRunsTrainProperties;
}

class MessageContentValidationSchema implements IMetroRunsMessageContent {
    @IsObject()
    @ValidateNested()
    @Type(() => MessagePropertiesValidationSchema)
    $!: IMetroRunsMessageProperties;

    @IsOptional()
    @IsObject({ each: true })
    @ValidateNested()
    @Type(() => TrainContentValidationSchema)
    vlak?: IMetroRunsTrainContent | IMetroRunsTrainContent[];
}

export class MetroRunsValidationSchema implements IMetroRunsInput {
    @IsObject()
    @ValidateNested()
    @Type(() => MessageContentValidationSchema)
    m!: IMetroRunsMessageContent;
}
