import { PG_SCHEMA } from "#sch/const";
import { RegionalBusRunsMessagesModel } from "#sch/vehicle-positions/models";
import { IModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { AbstractRunRepository } from "./AbstractRunRepository";

@injectable()
export class RegionalBusRunsMessagesRepository extends AbstractRunRepository implements IModel {
    constructor() {
        super(
            "RegionalBusRunsMessagesRepository",
            {
                pgTableName: RegionalBusRunsMessagesModel.TABLE_NAME,
                pgSchema: PG_SCHEMA,
                outputSequelizeAttributes: RegionalBusRunsMessagesModel.attributeModel,
                savingType: "insertOnly",
            },
            new JSONSchemaValidator("RegionalBusRunsMessagesRepository", RegionalBusRunsMessagesModel.arrayJsonSchema)
        );
    }
}
