import { PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import Sequelize, { Op } from "@golemio/core/dist/shared/sequelize";

export abstract class AbstractRunRepository extends PostgresModel {
    public deleteNHoursOldData = async (hours: number, columnToCheck: string): Promise<number> => {
        try {
            return await this.sequelizeModel.destroy({
                where: {
                    [columnToCheck]: {
                        [Op.lt]: Sequelize.literal(`NOW() - INTERVAL '${hours} HOURS'`),
                    },
                },
            });
        } catch (err) {
            throw new GeneralError("Error while purging old data", this.constructor.name, err);
        }
    };
}
