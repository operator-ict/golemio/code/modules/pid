import { PG_SCHEMA } from "#sch/const";
import { CommonRunsMessagesModel } from "#sch/vehicle-positions/models";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import Sequelize, { Op } from "@golemio/core/dist/shared/sequelize";

export class CommonRunsMessagesRepository extends PostgresModel implements IModel {
    /** Model name */
    public name!: string;
    /** The Sequelize Model */
    public sequelizeModel!: Sequelize.ModelCtor<any>;
    /** The Sequelize Model for temporary table */
    protected tmpSequelizeModel!: Sequelize.ModelCtor<any> | null;
    /** Validation helper */
    protected validator!: JSONSchemaValidator;
    /** Type/Strategy of saving the data */
    protected savingType!: "insertOnly" | "insertOrUpdate";

    constructor() {
        super(
            "CommonRunsMessagesRepository",
            {
                pgTableName: CommonRunsMessagesModel.TABLE_NAME,
                pgSchema: PG_SCHEMA,
                outputSequelizeAttributes: CommonRunsMessagesModel.attributeModel,
                savingType: "insertOnly",
                sequelizeAdditionalSettings: {
                    indexes: [
                        {
                            fields: ["id"],
                            name: "vehiclepositions_runs_messages_pkey",
                        },
                    ],
                },
            },
            new JSONSchemaValidator("CommonRunsMessagesRepositoryValidator", CommonRunsMessagesModel.arrayJsonSchema)
        );
    }

    public getLastMessage = async (runId: string) => {
        const result = await this.find({
            where: {
                runs_id: runId,
                msg_timestamp: {
                    [Op.gt]: Sequelize.literal("NOW() - INTERVAL '1 HOURS'"),
                },
            },
            order: [["msg_timestamp", "DESC"]],
            limit: 1,
            raw: true,
        });
        return result && result.length === 1 ? result[0] : undefined;
    };
}
