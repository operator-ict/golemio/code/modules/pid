import { PG_SCHEMA } from "#sch/const";
import { MetroRunsMessagesModel } from "#sch/vehicle-positions/models";
import { IModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { AbstractRunRepository } from "./AbstractRunRepository";

export class MetroRunsMessagesRepository extends AbstractRunRepository implements IModel {
    constructor() {
        super(
            "MetroRunsMessagesRepository",
            {
                pgTableName: MetroRunsMessagesModel.TABLE_NAME,
                pgSchema: PG_SCHEMA,
                outputSequelizeAttributes: MetroRunsMessagesModel.attributeModel,
                savingType: "insertOnly",
            },
            new JSONSchemaValidator("MetroRunsMessagesRepository", MetroRunsMessagesModel.arrayJsonSchema)
        );
    }
}
