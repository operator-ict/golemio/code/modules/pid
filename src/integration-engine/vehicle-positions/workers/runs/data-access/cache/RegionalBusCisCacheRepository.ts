import { RegionalBusCisCacheDtoSchema } from "#sch/vehicle-positions/redis/schemas/RegionalBusCisCacheDtoSchema";
import { RedisModel } from "@golemio/core/dist/integration-engine/models/RedisModel";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class RegionalBusCisCacheRepository extends RedisModel {
    public static NAMESPACE_PREFIX = "vpRegionalBusCisLookup";

    constructor() {
        super(
            "RegionalBusCisCacheRepository",
            {
                decodeDataAfterGet: JSON.parse,
                encodeDataBeforeSave: JSON.stringify,
                isKeyConstructedFromData: false,
                prefix: RegionalBusCisCacheRepository.NAMESPACE_PREFIX,
            },
            new JSONSchemaValidator("RegionalBusCisCacheRepository", RegionalBusCisCacheDtoSchema)
        );
    }
}
