import { RegionalBusGtfsCacheDtoSchema } from "#sch/vehicle-positions/redis/schemas/RegionalBusGtfsCacheDtoSchema";
import { RedisModel } from "@golemio/core/dist/integration-engine/models/RedisModel";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class RegionalBusGtfsCacheRepository extends RedisModel {
    public static NAMESPACE_PREFIX = "vpRegionalBusGtfsLookup";

    constructor() {
        super(
            "RegionalBusGtfsCacheRepository",
            {
                decodeDataAfterGet: JSON.parse,
                encodeDataBeforeSave: JSON.stringify,
                isKeyConstructedFromData: false,
                prefix: RegionalBusGtfsCacheRepository.NAMESPACE_PREFIX,
            },
            new JSONSchemaValidator("RegionalBusGtfsCacheRepository", RegionalBusGtfsCacheDtoSchema)
        );
    }
}
