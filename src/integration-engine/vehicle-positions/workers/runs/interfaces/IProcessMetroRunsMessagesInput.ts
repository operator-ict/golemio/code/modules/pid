import { IMetroRunsMessagesModel } from "#sch/vehicle-positions/models/interfaces/IMetroRunsMessagesModel";

export interface IProcessMetroRunsMessage
    extends Omit<IMetroRunsMessagesModel, "message_timestamp" | "actual_position_timestamp_scheduled"> {
    message_timestamp: string;
    actual_position_timestamp_scheduled: string;
}

export interface IProcessMetroRunsMessagesInput {
    routeName: string;
    messages: IProcessMetroRunsMessage[];
}
