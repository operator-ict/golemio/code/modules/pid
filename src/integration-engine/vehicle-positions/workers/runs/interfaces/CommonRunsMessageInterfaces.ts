import { TCPEventEnum } from "src/const";

export interface ICommonRunsInput {
    M: ICommonRunsMessageContent;
}

export interface ICommonRunsMessageContent {
    V: ICommonRunsMessagePropertiesWrapper | ICommonRunsMessagePropertiesWrapper[];
}

export interface ICommonRunsMessagePropertiesWrapper {
    $: ICommonRunsMessageProperties;
}

export interface ICommonRunsMessageProperties {
    turnus: string;
    line: string;
    evc: string;
    np?: string;
    lat: string;
    lng: string;
    akt?: string;
    takt: string;
    konc?: string;
    tjr?: string;
    pkt: string;
    tm: string;
    events: TCPEventEnum;
}

export interface ICommonRunsInputElement {
    data: ICommonRunsMessagePropertiesWrapper;
    timestamp: string;
}

export interface ICommonRunsInputData {
    data: ICommonRunsMessagePropertiesWrapper | ICommonRunsMessagePropertiesWrapper[];
    timestamp: number;
}
