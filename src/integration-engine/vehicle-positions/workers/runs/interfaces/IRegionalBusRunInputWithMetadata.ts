import { IProcessRegionalBusRunMessage } from "./IProcessRegionalBusRunMessagesInput";

export interface IRegionalBusRunInputWithMetadata {
    runMessage: IProcessRegionalBusRunMessage;
    gtfsTripId: string | undefined;
    isWheelchairAccessible: boolean | null;
    agencyName: string;
}
