import { IRailtrackCoordinates } from "#ie/ropid-gtfs/workers/timetables/tasks/interfaces/IAggregatedRailtrackGPSData";

export interface IMetroRunInputForProcessing {
    messageTimestamp: string;
    timestampScheduled: string;
    runNumber: number;
    routeId: string;
    trainSetNumberScheduled: string;
    trainSetNumberReal: string;
    trainNumber: string;
    coordinates: IRailtrackCoordinates;
}

export interface IExtendedMetroRunInputForProcessing extends IMetroRunInputForProcessing {
    tripId: string;
    internalRunNumber: number | null;
}
