import { IRegionalBusRunsMessagesModel } from "#sch/vehicle-positions/models/interfaces/IRegionalBusRunsMessagesModel";

export interface IProcessRegionalBusRunMessage extends IRegionalBusRunsMessagesModel {
    cis_line_id: string;
    cis_trip_number: number;
    registration_number: number;
}

export interface IProcessRegionalBusRunMessagesInput {
    messages: IProcessRegionalBusRunMessage[];
}
