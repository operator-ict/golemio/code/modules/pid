export interface IRegionalBusRunsInput {
    M: IRegionalBusRunsMessageContent;
}

export interface IRegionalBusRunsMessageContent {
    V: IRegionalBusRunsMessagePropertiesWrapper | IRegionalBusRunsMessagePropertiesWrapper[];
}

export interface IRegionalBusRunsMessagePropertiesWrapper {
    $: IRegionalBusRunsMessageProperties;
}

export interface IRegionalBusRunsMessageProperties {
    imei: string;
    line?: string;
    conn?: string;
    events?: string; // different from DPP events
    lat: string;
    lng: string;
    tm: string;
    evc?: string;
    rych: string;
    smer: string;
}

export interface ITransformationInput {
    messages: IRegionalBusRunsMessagePropertiesWrapper[];
    timestamp: number;
}
