import { ICommonRunWithMessageDto } from "#sch/vehicle-positions/models/interfaces/ICommonRunWithMessageDto";

export interface ICommonMessageFilter {
    yieldFilteredMessages(messages: ICommonRunWithMessageDto[], timestamp: number): Generator<ICommonRunWithMessageDto>;
}
