import { ICommonRunsMessageProperties } from "../../interfaces/CommonRunsMessageInterfaces";

export interface ICommonRunIdentifiers {
    runId: string;
    routeId: string;
    runNumber: string;
}

export interface ICommonRunHelper {
    getRunIdentifiers(properties: ICommonRunsMessageProperties, msgTimestamp: string): ICommonRunIdentifiers | null;
    parseDateFromRunInput(utcTimestamp: string): Date | null;
}
