import { PidContainer } from "#ie/ioc/Di";
import { VehiclePositions } from "#sch/vehicle-positions";
import { ICommonRunWithMessageDto } from "#sch/vehicle-positions/models/interfaces/ICommonRunWithMessageDto";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { config } from "@golemio/core/dist/integration-engine/config";
import { QueueManager } from "@golemio/core/dist/integration-engine/queueprocessors";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { ArrayNotPublicRegistrationNumbers } from "../../../../../const";
import { CommonRunsRepository } from "../data-access/CommonRunsRepository";

const ONE_HOUR_IN_MILLIS = 60 * 60 * 1000;
const MIN_HOURS_DIFF_TO_LOG = 22;

export class CommonMessageProcessor {
    private logger: ILogger;

    constructor(private readonly runsRepository: CommonRunsRepository) {
        this.logger = PidContainer.resolve<ILogger>(CoreToken.Logger);
    }

    /**
     * Create/update and process transformed run
     */
    public processTransformedRun = async (element: ICommonRunWithMessageDto, firstMessageCreatedAt?: number): Promise<void> => {
        this.logElementIssues(element);
        const record = await this.runsRepository.getRunRecordForUpdate(element.run);

        let outputMsg: ICommonRunWithMessageDto;
        if (record) {
            if (
                !element.run_message.actual_stop_timestamp_scheduled &&
                !ArrayNotPublicRegistrationNumbers.includes(element.run.registration_number)
            ) {
                const lastRecordMessage = await this.runsRepository["runsMessagesRepository"].getLastMessage(record.id);
                if (!lastRecordMessage || !lastRecordMessage.actual_stop_timestamp_scheduled) return;
                element.run_message.actual_stop_timestamp_scheduled = lastRecordMessage.actual_stop_timestamp_scheduled;
            }

            outputMsg = await this.runsRepository.updateAndAssociate(element, record.id);
        } else {
            outputMsg = await this.runsRepository.createAndAssociate(element);
        }

        await QueueManager.sendMessageToExchange(
            `${config.RABBIT_EXCHANGE_NAME}.${VehiclePositions.name.toLowerCase()}`,
            "updateRunsGTFSTripId",
            outputMsg,
            { timestamp: firstMessageCreatedAt }
        );
    };

    private logElementIssues(element: ICommonRunWithMessageDto): void {
        if (
            element.run_message.actual_stop_timestamp_scheduled instanceof Date &&
            element.run_message.actual_stop_timestamp_scheduled.getTime() - Date.now() >=
                MIN_HOURS_DIFF_TO_LOG * ONE_HOUR_IN_MILLIS
        ) {
            this.logger.error(
                new GeneralError(
                    `Message timestamp 'tjr' of value '${element.run_message.actual_stop_timestamp_scheduled.toISOString()}'` +
                        ` is ${MIN_HOURS_DIFF_TO_LOG}+ hrs ahead (line ${element.run.line_short_name}, run` +
                        ` ${element.run.run_number})`,
                    this.constructor.name,
                    undefined,
                    undefined,
                    "pid"
                )
            );
        }
    }
}
