import { VPContainer } from "#ie/vehicle-positions/ioc/Di";
import { ILogger } from "@golemio/core/dist/helpers/logger";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc";
import { ICommonRunsMessageProperties } from "../interfaces/CommonRunsMessageInterfaces";
import { ICommonRunHelper, ICommonRunIdentifiers } from "./interfaces/ICommonRunHelper";

export class CommonRunHelper implements ICommonRunHelper {
    private logger: ILogger;

    constructor() {
        this.logger = VPContainer.resolve<ILogger>(ContainerToken.Logger);
    }

    /**
     * @returns {ICommonRunIdentifiers | null} commonRunIdentifiers
     *  { runId: "123_456_789_2020-01-01T00:00:00Z", routeId: "123", runNumber: "456" }
     */
    public getRunIdentifiers(properties: ICommonRunsMessageProperties, msgTimestamp: string): ICommonRunIdentifiers | null {
        const { turnus, evc } = properties;
        let identifiers: ICommonRunIdentifiers | null = null;

        const [routeIdFromTurnus, runNumberFromTurnus] = turnus.split("/");
        if (routeIdFromTurnus && runNumberFromTurnus) {
            identifiers = {
                runId: `${routeIdFromTurnus}_${runNumberFromTurnus}_${evc}_${msgTimestamp}`,
                routeId: routeIdFromTurnus,
                runNumber: runNumberFromTurnus,
            };
        }

        return identifiers;
    }

    /**
     * @param {string} utcTimestamp - "2022-03-27T12:04:38"
     * @returns {Date | null} dateObject - Date(2022-03-27T12:04:38.000Z)
     */
    public parseDateFromRunInput(utcTimestamp: string): Date | null {
        const date = new Date(utcTimestamp + "Z");
        if (date.toString() === "Invalid Date") {
            this.logger.info(`parseDateFromRunInput: unable to parse date from ${utcTimestamp + "Z"}`);
            return null;
        }

        return date;
    }
}
