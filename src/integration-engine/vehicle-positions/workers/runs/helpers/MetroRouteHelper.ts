export enum MetroRouteName {
    LINE_A = "A",
    LINE_B = "B",
    LINE_C = "C",
}

const metroRouteId: Record<MetroRouteName | string, string | undefined> = {
    [MetroRouteName.LINE_A]: "991",
    [MetroRouteName.LINE_B]: "992",
    [MetroRouteName.LINE_C]: "993",
};

export class MetroRouteHelper {
    private static tripToRouteMetroMap: Record<string, string> | undefined;

    public static getRouteId(routeName: MetroRouteName | string): string | undefined {
        return metroRouteId[routeName];
    }

    public static getTripRouteMap(): Record<string, string> {
        if (!this.tripToRouteMetroMap) {
            this.tripToRouteMetroMap = {};
            for (const routeName of Object.values(MetroRouteName)) {
                this.tripToRouteMetroMap[`${MetroRouteHelper.getRouteId(routeName)}`] = routeName.toString();
            }
        }

        return this.tripToRouteMetroMap;
    }

    public static getRouteName(tripId: string) {
        return this.getTripRouteMap()[tripId.substring(0, 3)];
    }
}
