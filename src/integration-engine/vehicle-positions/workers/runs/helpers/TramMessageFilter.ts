import { ICommonRunWithMessageDto } from "#sch/vehicle-positions/models/interfaces/ICommonRunWithMessageDto";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { ArrayNotPublicRegistrationNumbers } from "src/const";
import { ICommonMessageFilter } from "./interfaces/ICommonMessageFilter";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { TimestampValidator } from "./TimestampValidator";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";

const TRAM_SERVICING_ROUTE_ID_MIN = 50;
const TRAM_SERVICING_ROUTE_ID_MAX = 89;
const TRAM_UNUSUAL_RUN_NUMBER_MIN = 80;

@injectable()
export class TramMessageFilter implements ICommonMessageFilter {
    constructor(
        @inject(CoreToken.Logger) private logger: ILogger,
        @inject(VPContainerToken.TimestampValidator) private timestampValidator: TimestampValidator
    ) {}

    /**
     * Yield tram messages that are valid for processing
     *   - route id is not considered a servicing route (50-89, note: >89 is a night tram route)
     *   - run number is less than 80
     *   - mazačka trams (maintanance) are always processed, even though they are servicing routes
     *   - timestamp is in acceptable range
     */
    public *yieldFilteredMessages(messages: ICommonRunWithMessageDto[], timestamp: number): Generator<ICommonRunWithMessageDto> {
        for (const message of messages) {
            const { route_id, run_number, registration_number } = message.run;

            if (!this.timestampValidator.isTimestampValid(timestamp, message.run_message.actual_stop_timestamp_real.toString())) {
                this.logger.error(
                    new GeneralError(
                        "Message timestamp 'takt' of value" +
                            message.run_message.actual_stop_timestamp_real.toISOString() +
                            `is not valid for line ${message.run.line_short_name}, run ${message.run.run_number}`,
                        this.constructor.name,
                        undefined,
                        undefined,
                        "pid"
                    )
                );
                continue;
            }

            if (
                ArrayNotPublicRegistrationNumbers.includes(registration_number) ||
                (this.isRouteIdValid(route_id) && this.isRunNumberValid(run_number))
            ) {
                yield message;
            } else {
                this.logger.info(`${this.constructor.name}: route id ${route_id} or run number ${run_number} is invalid`);
                this.logger.debug(message);
                continue;
            }
        }
    }

    private isRouteIdValid(routeId: string): boolean {
        const parsedRouteId = Number.parseInt(routeId);
        return (
            !Number.isNaN(parsedRouteId) &&
            (parsedRouteId < TRAM_SERVICING_ROUTE_ID_MIN || parsedRouteId > TRAM_SERVICING_ROUTE_ID_MAX)
        );
    }

    private isRunNumberValid(runNumber: number): boolean {
        return runNumber < TRAM_UNUSUAL_RUN_NUMBER_MIN;
    }
}
