import { ICommonRunWithMessageDto } from "#sch/vehicle-positions/models/interfaces/ICommonRunWithMessageDto";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { ICommonMessageFilter } from "./interfaces/ICommonMessageFilter";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { TimestampValidator } from "./TimestampValidator";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";

const TROLLEYBUS_ROUTE_ID_MIN = 50;
const TROLLEYBUS_ROUTE_ID_MAX = 69;

@injectable()
export class BusMessageFilter implements ICommonMessageFilter {
    constructor(
        @inject(CoreToken.Logger) private logger: ILogger,
        @inject(VPContainerToken.TimestampValidator) private timestampValidator: TimestampValidator
    ) {}

    /**
     * Yield messages that are valid for processing (filter out internal bus lines)
     *   - route id is a string of length 3 or more
     *   - route id is a number in range of bus and trolleybus lines
     *     - route id >= 100 (bus) ∪ [50, 69] (trolleybus)
     */
    public *yieldFilteredMessages(messages: ICommonRunWithMessageDto[], timestamp: number): Generator<ICommonRunWithMessageDto> {
        for (const message of messages) {
            const { run } = message;

            if (!this.timestampValidator.isTimestampValid(timestamp, message.run_message.actual_stop_timestamp_real.toString())) {
                this.logger.error(
                    new GeneralError(
                        "Message timestamp 'takt' of value" +
                            message.run_message.actual_stop_timestamp_real.toISOString() +
                            `is not valid for line ${message.run.line_short_name}, run ${message.run.run_number}`,
                        this.constructor.name,
                        undefined,
                        undefined,
                        "pid"
                    )
                );
                continue;
            }
            if (run.route_id.length > 2) {
                yield message;
                continue;
            }

            const routeId = Number.parseInt(run.route_id);
            if (Number.isNaN(routeId) || routeId < TROLLEYBUS_ROUTE_ID_MIN || routeId > TROLLEYBUS_ROUTE_ID_MAX) {
                this.logger.info(`${this.constructor.name}: route id ${routeId} is invalid or out of range`);
                this.logger.debug(message);
                continue;
            }

            yield message;
        }
    }
}
