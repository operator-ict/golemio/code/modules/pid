import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class TimestampValidator {
    constructor(@inject(CoreToken.SimpleConfig) private config: ISimpleConfig) {}

    public isTimestampValid(now: number, timestamp: string): boolean {
        const convertedTimestamp = new Date(timestamp).getTime();

        let convertedNow = new Date(now);

        const before = convertedNow.setMinutes(
            convertedNow.getMinutes() -
                Number(this.config.getValue("module.pid.vehicle-positions.tcpTimestampTolerance.beforeTimestamp", 120))
        );
        convertedNow = new Date(now);
        const after = convertedNow.setMinutes(
            convertedNow.getMinutes() +
                Number(this.config.getValue("module.pid.vehicle-positions.tcpTimestampTolerance.afterTimestamp", 1))
        );

        return convertedTimestamp >= before && convertedTimestamp <= after;
    }
}
