import { log } from "@golemio/core/dist/integration-engine/helpers";
import { IProcessMetroRunsMessage } from "../interfaces/IProcessMetroRunsMessagesInput";

interface IValidMessagesWithTrackIdsOutput {
    filteredMessages: IProcessMetroRunsMessage[];
    trackIds: string[];
}

export class MetroMessageFilter {
    private static DELAY_ORIGIN_RANGE_MIN = -4 * 60 * 60; // 4 hours delay
    private static DELAY_ORIGIN_RANGE_MAX = 1 * 60 * 60; // 1 hour ahead of time

    public static getValidMessagesWithTrackIds(messages: IProcessMetroRunsMessage[]): IValidMessagesWithTrackIdsOutput {
        let filteredMessages: IProcessMetroRunsMessage[] = [];
        let trackIds: Set<string> = new Set();

        for (const message of messages) {
            const { delay_origin, track_id } = message;
            if (
                delay_origin < MetroMessageFilter.DELAY_ORIGIN_RANGE_MIN ||
                delay_origin > MetroMessageFilter.DELAY_ORIGIN_RANGE_MAX
            ) {
                log.warn(
                    `getValidMessagesWithTrackIds: delay origin ${delay_origin} is out of range ` +
                        `[${MetroMessageFilter.DELAY_ORIGIN_RANGE_MIN}, ${MetroMessageFilter.DELAY_ORIGIN_RANGE_MAX}]`
                );

                continue;
            }

            filteredMessages.push(message);
            trackIds.add(track_id);
        }

        return {
            filteredMessages,
            trackIds: Array.from(trackIds),
        };
    }
}
