import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { VPUtils } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/VPUtils";
import { IRegionalBusRunsMessagesModel } from "#sch/vehicle-positions/models/interfaces/IRegionalBusRunsMessagesModel";
import { IRegionalBusCisCacheDto } from "#sch/vehicle-positions/redis/interfaces/IRegionalBusCisCacheDto";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { RegionalBusCisCacheRepository } from "../../data-access/cache/RegionalBusCisCacheRepository";
import { ICisLookupManager } from "./interfaces/ICisLookupManager";

@injectable()
export class CisLookupManager implements ICisLookupManager {
    constructor(
        @inject(VPContainerToken.RegionalBusCisCacheRepository) private cisLookupCacheRepository: RegionalBusCisCacheRepository
    ) {}

    public async enrichMessagesWithCisData(messages: IRegionalBusRunsMessagesModel[]): Promise<IRegionalBusRunsMessagesModel[]> {
        const nextExpireTimestamp = VPUtils.getNextExpireTimestamp();
        const cisCacheData = await this.cisLookupCacheRepository.mget<IRegionalBusCisCacheDto>(
            messages.map((message) => message.external_trip_id)
        );

        for (let i = 0; i < messages.length; i++) {
            const message = messages[i];
            const cisCacheItem = cisCacheData[i];

            if (message.cis_line_id && message.cis_trip_number && message.registration_number) {
                await this.cisLookupCacheRepository.set(
                    message.external_trip_id,
                    {
                        cis_line_id: message.cis_line_id,
                        cis_trip_number: message.cis_trip_number,
                        registration_number: message.registration_number,
                    },
                    undefined,
                    nextExpireTimestamp
                );

                continue;
            }

            if (!cisCacheItem) {
                continue;
            }

            // Fill in the missing data from Redis cache
            message.cis_line_id = cisCacheItem.cis_line_id;
            message.cis_trip_number = cisCacheItem.cis_trip_number;
            message.registration_number = cisCacheItem.registration_number;
        }

        return messages;
    }
}
