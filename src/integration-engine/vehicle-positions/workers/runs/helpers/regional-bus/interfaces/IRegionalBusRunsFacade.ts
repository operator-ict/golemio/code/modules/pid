import { IRegionalBusRunsMessagesModel } from "#sch/vehicle-positions/models/interfaces/IRegionalBusRunsMessagesModel";
import { ITransformationInput } from "../../../interfaces/RegionalBusRunsMessageInterfaces";

export interface IRegionalBusRunsFacade {
    transformAndSaveMessages(data: ITransformationInput): Promise<IRegionalBusRunsMessagesModel[]>;
}
