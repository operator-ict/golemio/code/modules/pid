import { IRegionalBusRunsMessagesModel } from "#sch/vehicle-positions/models/interfaces/IRegionalBusRunsMessagesModel";

export interface ICisLookupManager {
    enrichMessagesWithCisData(messages: IRegionalBusRunsMessagesModel[]): Promise<IRegionalBusRunsMessagesModel[]>;
}
