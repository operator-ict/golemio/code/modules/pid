import { RouteSubAgencyDto } from "#sch/ropid-gtfs/models/RouteSubAgencyDto";
import { DescriptorModel } from "#sch/vehicle-descriptors/models";
import { IProcessRegionalBusRunMessage } from "../../../interfaces/IProcessRegionalBusRunMessagesInput";
import { IExtendedScheduleWithExternalId } from "./IExtendedSchedule";

export interface ITripScheduleManager {
    getScheduleInput(
        message: IProcessRegionalBusRunMessage,
        subAgencyEntities: RouteSubAgencyDto[],
        descriptorEntities: DescriptorModel[]
    ): Promise<IExtendedScheduleWithExternalId | null>;
}
