import { IRegionalBusRunsMessagesModel } from "#sch/vehicle-positions/models/interfaces/IRegionalBusRunsMessagesModel";

export interface IRegionalBusMessageFilter {
    getFilteredMessages(messages: IRegionalBusRunsMessagesModel[], timestamp: number): IRegionalBusRunsMessagesModel[];
}
