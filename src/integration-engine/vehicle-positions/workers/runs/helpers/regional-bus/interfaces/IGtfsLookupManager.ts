import { IRegionalBusRunsMessagesModel } from "#sch/vehicle-positions/models/interfaces/IRegionalBusRunsMessagesModel";
import { IExtendedSchedule, IExtendedScheduleWithExternalId } from "./IExtendedSchedule";

export interface IGtfsLookupManager {
    getScheduleInputData(messages: IRegionalBusRunsMessagesModel[]): Promise<Array<IExtendedSchedule | null>>;
    setGtfsScheduleLookup(scheduleInputData: IExtendedScheduleWithExternalId[]): Promise<void>;
}
