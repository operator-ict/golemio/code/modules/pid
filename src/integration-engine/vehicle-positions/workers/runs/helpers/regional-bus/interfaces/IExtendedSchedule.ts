import { IVehiclePositionsSchedule } from "#ie/vehicle-positions/workers/vehicle-positions/interfaces/VPInterfaces";

export interface IExtendedSchedule extends IVehiclePositionsSchedule {
    agency_name: string;
    is_wheelchair_accessible: boolean | null;
}

export interface IExtendedScheduleWithExternalId extends IExtendedSchedule {
    cache_lookup_key: string;
}
