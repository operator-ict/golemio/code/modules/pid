import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { IRegionalBusRunsMessagesModel } from "#sch/vehicle-positions/models/interfaces/IRegionalBusRunsMessagesModel";
import { IModel } from "@golemio/core/dist/integration-engine/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { ITransformationInput } from "../../interfaces/RegionalBusRunsMessageInterfaces";
import { RegionalBusRunsMessagesTransformation } from "../../transformations/RegionalBusRunsMessagesTransformation";
import { ICisLookupManager } from "./interfaces/ICisLookupManager";
import { IRegionalBusMessageFilter } from "./interfaces/IRegionalBusMessageFilter";
import { IRegionalBusRunsFacade } from "./interfaces/IRegionalBusRunsFacade";
import { RegionalBusMessageFilter } from "./RegionalBusMessageFilter";

@injectable()
export class RegionalBusRunsFacade implements IRegionalBusRunsFacade {
    constructor(
        @inject(VPContainerToken.RegionalBusRunsMessagesTransformation)
        private messageTransformation: RegionalBusRunsMessagesTransformation,
        @inject(VPContainerToken.RegionalBusRunsMessagesRepository)
        private messageRepository: IModel,
        @inject(VPContainerToken.RegionalBusMessageFilter)
        private messageFilter: RegionalBusMessageFilter,
        @inject(VPContainerToken.CisLookupManager)
        private cisLookupManager: ICisLookupManager
    ) {}

    public async transformAndSaveMessages({
        messages,
        timestamp,
    }: ITransformationInput): Promise<IRegionalBusRunsMessagesModel[]> {
        let transformedMessages = this.messageTransformation.transformArray(messages);
        if (transformedMessages.length === 0) {
            return [];
        }

        try {
            transformedMessages = await this.cisLookupManager.enrichMessagesWithCisData(transformedMessages);
        } catch (err) {
            throw new GeneralError(
                "transformAndFilterMessages: failed to enrich messages with CIS data",
                this.constructor.name,
                err
            );
        }

        const filteredMessages = this.messageFilter.getFilteredMessages(transformedMessages, timestamp);
        await this.messageRepository.bulkSave(filteredMessages);

        return filteredMessages;
    }
}
