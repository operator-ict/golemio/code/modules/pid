import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { IRegionalBusRunsMessagesModel } from "#sch/vehicle-positions/models/interfaces/IRegionalBusRunsMessagesModel";
import { IRegionalBusGtfsCacheDto } from "#sch/vehicle-positions/redis/interfaces/IRegionalBusGtfsCacheDto";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { RegionalBusGtfsCacheRepository } from "../../data-access/cache/RegionalBusGtfsCacheRepository";
import { IExtendedSchedule, IExtendedScheduleWithExternalId } from "./interfaces/IExtendedSchedule";
import { IGtfsLookupManager } from "./interfaces/IGtfsLookupManager";

@injectable()
export class GtfsLookupManager implements IGtfsLookupManager {
    constructor(
        @inject(VPContainerToken.RegionalBusGtfsCacheRepository) private gtfsLookupCacheRepository: RegionalBusGtfsCacheRepository
    ) {}

    public async getScheduleInputData(messages: IRegionalBusRunsMessagesModel[]): Promise<Array<IExtendedSchedule | null>> {
        const cacheLookupKeys = messages.map((message) => `${message.cis_line_id}_${message.cis_trip_number}`);
        const gtfsCacheData = await this.gtfsLookupCacheRepository.mget<IRegionalBusGtfsCacheDto>(cacheLookupKeys);
        let scheduleInputData: Array<IExtendedSchedule | null> = [];

        for (let i = 0; i < messages.length; i++) {
            const message = messages[i];
            const gtfsCacheItem = gtfsCacheData[i];

            if (!gtfsCacheItem) {
                scheduleInputData.push(null);
                continue;
            }

            scheduleInputData.push({
                route_id: gtfsCacheItem.route_name,
                run_number: gtfsCacheItem.run_number,
                msg_last_timestamp: message.vehicle_timestamp as string,
                gtfs_trip_id: gtfsCacheItem.gtfs_trip_id,
                agency_name: gtfsCacheItem.agency_name,
                is_wheelchair_accessible: gtfsCacheItem.is_wheelchair_accessible,
            });
        }

        return scheduleInputData;
    }

    public async setGtfsScheduleLookup(scheduleInputData: IExtendedScheduleWithExternalId[]): Promise<void> {
        for (const scheduleInputItem of scheduleInputData) {
            const gtfsCacheItem: IRegionalBusGtfsCacheDto = {
                gtfs_trip_id: scheduleInputItem.gtfs_trip_id!,
                route_name: scheduleInputItem.route_id,
                run_number: scheduleInputItem.run_number as number,
                agency_name: scheduleInputItem.agency_name,
                is_wheelchair_accessible: scheduleInputItem.is_wheelchair_accessible,
            };

            await this.gtfsLookupCacheRepository.set(scheduleInputItem.cache_lookup_key, gtfsCacheItem);
        }
    }
}
