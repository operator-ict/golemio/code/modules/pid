import { IRegionalBusRunsMessagesModel } from "#sch/vehicle-positions/models/interfaces/IRegionalBusRunsMessagesModel";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { IRegionalBusMessageFilter } from "./interfaces/IRegionalBusMessageFilter";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { TimestampValidator } from "../TimestampValidator";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";

@injectable()
export class RegionalBusMessageFilter implements IRegionalBusMessageFilter {
    constructor(
        @inject(CoreToken.Logger) private logger: ILogger,
        @inject(VPContainerToken.TimestampValidator) private timestampValidator: TimestampValidator
    ) {}

    /**
     * Get messages that are valid for processing
     */
    public getFilteredMessages(messages: IRegionalBusRunsMessagesModel[], timestamp: number): IRegionalBusRunsMessagesModel[] {
        let filteredMessages: IRegionalBusRunsMessagesModel[] = [];
        for (const message of messages) {
            const isValidMessage =
                message.cis_line_id && message.cis_trip_number && message.registration_number && message.events !== "";
            if (!this.timestampValidator.isTimestampValid(timestamp, message.timestamp!)) {
                this.logger.error(
                    new GeneralError(
                        `Message timestamp 'tm' of value ${message.timestamp} is not valid ` +
                            `(line ${message.cis_line_id}, trip id ${message.cis_trip_number})`,
                        this.constructor.name,
                        undefined,
                        undefined,
                        "pid"
                    )
                );
                continue;
            }

            if (isValidMessage) {
                filteredMessages.push(message);
            }
        }

        if (filteredMessages.length === 0) {
            this.logger.verbose("getFilteredMessages: no messages left after filtering");
        }

        return filteredMessages;
    }
}
