import { TripScheduleRepository } from "#ie/ropid-gtfs/data-access/precomputed/TripScheduleRepository";
import { RouteSubAgencyDto } from "#sch/ropid-gtfs/models/RouteSubAgencyDto";
import { DescriptorModel } from "#sch/vehicle-descriptors/models";
import { IScheduleDto } from "#sch/vehicle-positions/redis/interfaces/IGtfsRunTripCacheDto";
import { Op, Sequelize } from "@golemio/core/dist/shared/sequelize";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { IProcessRegionalBusRunMessage } from "../../interfaces/IProcessRegionalBusRunMessagesInput";
import { IExtendedScheduleWithExternalId } from "./interfaces/IExtendedSchedule";
import { ITripScheduleManager } from "./interfaces/ITripScheduleManager";

@injectable()
export class TripScheduleManager implements ITripScheduleManager {
    private readonly tripScheduleRepository: TripScheduleRepository;

    constructor() {
        this.tripScheduleRepository = new TripScheduleRepository();
    }

    public async getScheduleInput(
        message: IProcessRegionalBusRunMessage,
        subAgencyEntities: RouteSubAgencyDto[],
        descriptorEntities: DescriptorModel[]
    ): Promise<IExtendedScheduleWithExternalId | null> {
        const subAgencyEntity = subAgencyEntities.find(
            (subAgency) => subAgency.route_licence_number && subAgency.route_licence_number.toString() === message.cis_line_id
        );

        if (!subAgencyEntity) {
            return null;
        }

        const descriptorEntity = descriptorEntities.find(
            (descriptor) => descriptor.registration_number === message.registration_number
        );

        const gtfsScheduleData: IScheduleDto | null = await this.tripScheduleRepository.findOne({
            attributes: ["origin_route_name", "run_number", "trip_id"],
            where: {
                route_licence_number: message.cis_line_id,
                trip_number: message.cis_trip_number,
                [Op.and]: [Sequelize.literal("start_timestamp::date = current_date")],
            },
        });

        if (!gtfsScheduleData) {
            return null;
        }

        return {
            cache_lookup_key: `${message.cis_line_id}_${message.cis_trip_number}`,
            route_id: gtfsScheduleData.origin_route_name,
            run_number: gtfsScheduleData.run_number,
            msg_last_timestamp: message.vehicle_timestamp as string,
            gtfs_trip_id: gtfsScheduleData.trip_id,
            agency_name: subAgencyEntity.sub_agency_name,
            is_wheelchair_accessible: descriptorEntity?.is_wheelchair_accessible ?? null,
        };
    }
}
