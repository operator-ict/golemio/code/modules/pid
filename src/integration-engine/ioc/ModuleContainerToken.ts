const ModuleContainerToken = {
    GeoMeasurementHelper: Symbol(),
    DeparturePresetsRepository: Symbol(),
    RopidDeparturesPresetsTransformation: Symbol(),
    GrafanaLokiDataSourceProvider: Symbol(),
    PresetLogDataSourceFactory: Symbol(),
    PresetLogTransformation: Symbol(),
    PresetLogRepository: Symbol(),
    PresetLogFilter: Symbol(),
    RopidMonitoringService: Symbol(),
};

export { ModuleContainerToken };
