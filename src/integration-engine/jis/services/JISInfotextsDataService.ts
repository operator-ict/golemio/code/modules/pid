import { JISContainerToken } from "#ie/jis/ioc/JISContainerToken";
import { JISInfotextsRepository } from "#ie/jis/repositories/JISInfotextsRepository";
import { JISInfotextsRopidGTFSStopsRepository } from "#ie/jis/repositories/JISInfotextsRopidGTFSStopsRepository";
import { IJISInfotext, IJISInfotextsRopidGTFSStops } from "#sch/jis/models/interfaces";
import { ILogger } from "@golemio/core/dist/helpers";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class JISInfotextsDataService {
    constructor(
        @inject(JISContainerToken.JISInfotextsRepository)
        private infotextsRepository: JISInfotextsRepository,
        @inject(JISContainerToken.JISInfotextsRopidGTFSStopsRepository)
        private infotextsRopidGTFSStopsRepository: JISInfotextsRopidGTFSStopsRepository,
        @inject(CoreToken.PostgresConnector) private databaseConnector: IDatabaseConnector,
        @inject(CoreToken.Logger) private logger: ILogger
    ) {
        // do nothing
    }

    /**
     * Insert or update all given infotexts and infotextsRopidGTFSStops data. In case of an update, all attributes except for
     * `created_at` shall be overwritten. Delete all non-updated data from before the refresh.
     *
     * @param infotexts The infotexts data to be upserted
     * @param infotextsRopidGTFSStops The infotextsRopidGTFSStops data to be upserted
     */
    public async refreshData(infotexts: IJISInfotext[], infotextsRopidGTFSStops: IJISInfotextsRopidGTFSStops[]): Promise<void> {
        const connection = this.databaseConnector.getConnection();
        const transaction = await connection.transaction();
        const saveTime = new Date();

        try {
            if (infotexts.length === 0) {
                await this.infotextsRopidGTFSStopsRepository.deleteAll({ transaction });
                await this.infotextsRepository.deleteAll({ transaction });
                await transaction.commit();
                this.logger.info(
                    `${this.constructor.name}.refreshData: all JIS infotexts were deleted because 0 infotexts were imported`
                );
            } else {
                await this.infotextsRepository.refreshData(infotexts, { transaction });
                await this.infotextsRopidGTFSStopsRepository.upsertAll(infotextsRopidGTFSStops, { transaction });
                await this.infotextsRopidGTFSStopsRepository.deleteAllLastUpdatedBefore(saveTime, { transaction });
                await transaction.commit();
                this.logger.info(
                    `${this.constructor.name}.refreshData: ${infotexts.length} JIS infotexts and ` +
                        `${infotextsRopidGTFSStops.length} JIS infotextsRopidGTFSStops were saved`
                );
            }
        } catch (err) {
            await transaction.rollback();
            if (err instanceof GeneralError) throw err;
            throw new GeneralError("Refreshing JIS Infotexts in database failed", this.constructor.name, err);
        }
    }
}
