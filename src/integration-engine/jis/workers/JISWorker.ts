import { AbstractWorker } from "@golemio/core/dist/integration-engine/workers";
import { ITask } from "@golemio/core/dist/integration-engine/workers/interfaces";
import { JISContainer } from "../ioc/Di";
import { JISContainerToken } from "../ioc/JISContainerToken";
import { JIS_WORKER_NAME } from "./constants";
import { RefreshJISInfotextsTask } from "./tasks/RefreshJISInfotextsTask";
import { FetchJISInfotextsTask } from "./tasks/FetchJISInfotextsTask";

export class JISWorker extends AbstractWorker {
    protected name = JIS_WORKER_NAME.toLowerCase();

    constructor() {
        super();

        // Register tasks
        this.registerTask(JISContainer.resolve<RefreshJISInfotextsTask>(JISContainerToken.RefreshJISInfotextsTask));
        this.registerTask(JISContainer.resolve<FetchJISInfotextsTask>(JISContainerToken.FetchJISInfotextsTask));
    }

    public registerTask = (task: ITask): void => {
        super.registerTask(task);
        task.queuePrefix = this.getQueuePrefix();
    };
}
