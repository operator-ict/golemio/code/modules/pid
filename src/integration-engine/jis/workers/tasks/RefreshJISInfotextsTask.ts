import { JISContainerToken } from "#ie/jis/ioc/JISContainerToken";
import { JISInfotextsDataService } from "#ie/jis/services/JISInfotextsDataService";
import { JISInfotextsTransformation } from "#ie/jis/transformations/JISInfotextsTransformation";
import { IJISInfotextsRopidGTFSStops } from "#sch/jis/models/interfaces";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { JIS_WORKER_NAME } from "../constants";
import { AbstractTaskJsonSchema } from "@golemio/core/dist/integration-engine/workers/AbstractTaskJsonSchema";
import { jisInfotextsJsonSchema } from "#sch/jis/datasources/JISInfotextsJsonSchema";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { IJISInfotext } from "#sch/jis/datasources/interfaces";
import { IJISInfotext as IJISInfotextModel } from "#sch/jis/models/interfaces";

@injectable()
export class RefreshJISInfotextsTask extends AbstractTaskJsonSchema<IJISInfotext[]> {
    public readonly schema = new JSONSchemaValidator("jisInfotextsDataValidation", jisInfotextsJsonSchema);
    public readonly queueName = "refreshJISInfotexts";
    public readonly queueTtl = 60 * 1000; // 1 minute

    constructor(
        @inject(JISContainerToken.JISInfotextsDataService)
        private infotextsDataService: JISInfotextsDataService,
        @inject(JISContainerToken.JISInfotextsTransformation)
        private infotextsTransformation: JISInfotextsTransformation
    ) {
        super(JIS_WORKER_NAME.toLowerCase());
    }

    /** refresh JIS Infotexts data in database */
    protected async execute(data: IJISInfotext[]): Promise<void> {
        const transformedData = this.infotextsTransformation.transformArray(data);
        const { infotexts, infotextsRopidGTFSStops } = this.getEntitiesFromTransformationResult(transformedData);
        await this.infotextsDataService.refreshData(infotexts, infotextsRopidGTFSStops);
    }

    private getEntitiesFromTransformationResult(transformedData: ReturnType<JISInfotextsTransformation["transformArray"]>) {
        const infotextEntities: IJISInfotextModel[] = [];
        const infotextsRopidGTFSStopsEntities: IJISInfotextsRopidGTFSStops[] = [];
        for (const { infotext, infotextsRopidGTFSStops } of transformedData) {
            if (infotextsRopidGTFSStops.length === 0) {
                continue;
            }
            infotextEntities.push(infotext);
            infotextsRopidGTFSStopsEntities.push(...infotextsRopidGTFSStops);
        }
        return {
            infotexts: infotextEntities,
            infotextsRopidGTFSStops: infotextsRopidGTFSStopsEntities,
        };
    }
}
