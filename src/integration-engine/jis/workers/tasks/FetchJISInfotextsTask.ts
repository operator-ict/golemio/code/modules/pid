import { JISInfotextsDataSourceFactory } from "#ie/jis/datasources/JISInfotextsDataSourceFactory";
import { JISContainerToken } from "#ie/jis/ioc/JISContainerToken";
import { AbstractEmptyTask, QueueManager } from "@golemio/core/dist/integration-engine";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { JIS_WORKER_NAME } from "../constants";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";

@injectable()
export class FetchJISInfotextsTask extends AbstractEmptyTask {
    public readonly queueName = "fetchJISInfotexts";
    public readonly queueTtl = 20 * 1000; // 20 seconds

    constructor(
        @inject(JISContainerToken.JISInfotextsDataSourceFactory)
        private dataSourceFactory: JISInfotextsDataSourceFactory
    ) {
        super(JIS_WORKER_NAME.toLowerCase());
    }

    /** Fetch JIS Infotexts data source */
    protected async execute(): Promise<void> {
        try {
            const dataSource = this.dataSourceFactory.getDataSource();
            const data = await dataSource.getAll();

            await QueueManager.sendMessageToExchange(this.queuePrefix, "refreshJISInfotexts", data);
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            } else {
                throw new GeneralError("Error while fetching new JIS infotexts", this.constructor.name, err);
            }
        }
    }
}
