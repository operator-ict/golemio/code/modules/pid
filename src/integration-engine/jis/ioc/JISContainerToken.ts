const JISContainerToken = {
    /* Data Sources */
    JISInfotextsDataSourceFactory: Symbol(),
    /* Repositories */
    JISInfotextsRepository: Symbol(),
    JISInfotextsRopidGTFSStopsRepository: Symbol(),
    JISMetadataRepository: Symbol(),
    /* Services */
    JISInfotextsDataService: Symbol(),
    /* Transformations */
    JISInfotextsTransformation: Symbol(),
    /* Tasks */
    RefreshJISInfotextsTask: Symbol(),
    FetchJISInfotextsTask: Symbol(),
};

export { JISContainerToken };
