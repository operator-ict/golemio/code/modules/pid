import { PidContainer } from "#ie/ioc/Di";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import { JISInfotextsDataSourceFactory } from "../datasources/JISInfotextsDataSourceFactory";
import { JISInfotextsRepository } from "../repositories/JISInfotextsRepository";
import { JISInfotextsRopidGTFSStopsRepository } from "../repositories/JISInfotextsRopidGTFSStopsRepository";
import { JISMetadataRepository } from "../repositories/JISMetadataRepository";
import { JISInfotextsDataService } from "../services/JISInfotextsDataService";
import { JISInfotextsTransformation } from "../transformations/JISInfotextsTransformation";
import { RefreshJISInfotextsTask } from "../workers/tasks/RefreshJISInfotextsTask";
import { JISContainerToken } from "./JISContainerToken";
import { FetchJISInfotextsTask } from "../workers/tasks/FetchJISInfotextsTask";

//#region Initialization
const JISContainer: DependencyContainer = PidContainer.createChildContainer();
//#endregion

//#region Data Sources
JISContainer.registerSingleton(JISContainerToken.JISInfotextsDataSourceFactory, JISInfotextsDataSourceFactory);
//#endregion

//#region Repositories
JISContainer.registerSingleton(JISContainerToken.JISInfotextsRepository, JISInfotextsRepository);
JISContainer.registerSingleton(JISContainerToken.JISInfotextsRopidGTFSStopsRepository, JISInfotextsRopidGTFSStopsRepository);
JISContainer.registerSingleton(JISContainerToken.JISMetadataRepository, JISMetadataRepository);
//#endregion

//#region Services
JISContainer.register(JISContainerToken.JISInfotextsDataService, JISInfotextsDataService);
//#endregion

//#region Tasks
JISContainer.registerSingleton(JISContainerToken.RefreshJISInfotextsTask, RefreshJISInfotextsTask);
JISContainer.registerSingleton(JISContainerToken.FetchJISInfotextsTask, FetchJISInfotextsTask);
//#endregion

//#region Transformations
JISContainer.registerSingleton(JISContainerToken.JISInfotextsTransformation, JISInfotextsTransformation);
//#endregion

export { JISContainer };
