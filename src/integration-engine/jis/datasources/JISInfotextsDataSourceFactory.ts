import { IJISInfotext } from "#sch/jis/datasources/interfaces";
import { jisInfotextsJsonSchema } from "#sch/jis/datasources/JISInfotextsJsonSchema";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { DataSource, IDataSource, JSONDataTypeStrategy, ProtocolStrategy } from "@golemio/core/dist/integration-engine";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class JISInfotextsDataSourceFactory {
    constructor(@inject(CoreToken.SimpleConfig) private config: ISimpleConfig) {}

    private static DATASOURCE_NAME = "JISInfotextsDataSource";

    public getDataSource(etag?: string): IDataSource<IJISInfotext[]> {
        return new DataSource<IJISInfotext[]>(
            JISInfotextsDataSourceFactory.DATASOURCE_NAME,
            this.getProtocolStrategy(etag),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(JISInfotextsDataSourceFactory.DATASOURCE_NAME + "Validator", jisInfotextsJsonSchema)
        );
    }

    private getProtocolStrategy(etag?: string): ProtocolStrategy {
        const baseUrl = this.config.getValue<string>("module.pid.jis.baseUrl");
        let headers = this.config.getValue<HeadersInit>("module.pid.jis.infotexts.headers") as HeadersInit;
        const urlPath = this.config.getValue<string>("module.pid.jis.infotexts.path");
        const url = new URL(urlPath, baseUrl).toString();

        if (typeof etag === "string") {
            headers = { ...headers, "If-None-Match": etag, "Cache-Control": "no-store" };
        }

        return new HTTPFetchProtocolStrategy({
            method: "GET",
            url,
            headers,
            timeoutInSeconds: 20,
        });
    }
}
