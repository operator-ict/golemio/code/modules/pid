import { IJISInfotext as IJISInfotextDataSource } from "#sch/jis/datasources/interfaces";
import {
    IJISInfotext as IJISInfotextModel,
    IJISInfotextsRopidGTFSStops as IJISInfotextsRopidGTFSStopsModel,
} from "#sch/jis/models/interfaces";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

type TransformIn = IJISInfotextDataSource;
type TransformOut = {
    infotext: IJISInfotextModel;
    infotextsRopidGTFSStops: IJISInfotextsRopidGTFSStopsModel[];
};

@injectable()
export class JISInfotextsTransformation extends AbstractTransformation<TransformIn, TransformOut> {
    public name = "JISInfotextsTransformation";

    protected transformInternal = (data: TransformIn): TransformOut => {
        const infotext = {
            id: data.id,
            severity_level: data.severity_level,
            display_type: data.display_type,
            active_period_start: new Date(data.active_period.start),
            active_period_end: data.active_period.end ? new Date(data.active_period.end) : null,
            description_text: data.description_text,
            created_timestamp: new Date(data.created_timestamp),
            updated_timestamp: new Date(data.last_modified_timestamp),
        };
        const infotextsRopidGTFSStops =
            data.informed_entity?.stops?.map(({ stop_id }) => ({
                infotext_id: data.id,
                stop_id,
            })) ?? [];
        return {
            infotext,
            infotextsRopidGTFSStops,
        };
    };
}
