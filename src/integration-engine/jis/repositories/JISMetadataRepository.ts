import { JIS_METADATA_NAMESPACE_PREFIX } from "#sch/jis/redis/const";
import { IJISMetadataDto } from "#sch/jis/redis/interfaces/IJISMetadataDto";
import { JISMetadataDtoSchema } from "#sch/jis/redis/schemas/JISMetadataDtoSchema";
import { RedisModel } from "@golemio/core/dist/integration-engine/models/RedisModel";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class JISMetadataRepository extends RedisModel {
    public static readonly NAMESPACE_PREFIX = JIS_METADATA_NAMESPACE_PREFIX;

    protected readonly key = "main";

    constructor() {
        super(
            "JISMetadataRepository",
            {
                decodeDataAfterGet: JSON.parse,
                encodeDataBeforeSave: JSON.stringify,
                isKeyConstructedFromData: false,
                prefix: JISMetadataRepository.NAMESPACE_PREFIX,
            },
            new JSONSchemaValidator("JISMetadataRepositoryValidator", JISMetadataDtoSchema)
        );
    }

    /** Get the Etag of the last JIS Infotexts response */
    async getLastInfotextsEtag(): Promise<string | undefined> {
        try {
            const metadata: IJISMetadataDto | undefined = await this.get(this.key);
            return metadata?.infotexts.lastResponse.etag;
        } catch (err) {
            if (err instanceof GeneralError) throw err;
            throw new GeneralError("Error while getting last infotexts etag", this.constructor.name, err);
        }
    }

    /**
     * Set the Etag of the last JIS Infotexts response to a given new value
     *
     * @param etag The new Etag of the last JIS Infotexts response
     */
    async setLastInfotextsEtag(etag: string): Promise<void> {
        try {
            let metadata: IJISMetadataDto | undefined = await this.get(this.key);
            if (!metadata) metadata = this.initializeMetadata();
            metadata.infotexts.lastResponse.etag = etag;
            metadata.infotexts.lastResponse.updatedAt = new Date().toISOString();
            await this.set(this.key, metadata);
        } catch (err) {
            if (err instanceof GeneralError) throw err;
            throw new GeneralError("Error while updating last infotexts etag", this.constructor.name, err);
        }
    }

    private initializeMetadata(): IJISMetadataDto {
        return {
            infotexts: {
                lastResponse: {
                    etag: "",
                    updatedAt: new Date().toISOString(),
                },
            },
        };
    }
}
