import { PG_SCHEMA } from "#sch/const";
import { JISInfotextsModel } from "#sch/jis/models/JISInfotextsModel";
import { IJISInfotext } from "#sch/jis/models/interfaces";
import { ILogger } from "@golemio/core/dist/helpers";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { AbstractValidatableRepository } from "@golemio/core/dist/helpers/data-access/postgres/repositories/AbstractValidatableRepository";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { GeneralError, ValidationError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { ModelStatic, Op, ValidationError as SequelizeValidationError, Transaction } from "@golemio/core/dist/shared/sequelize";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

type RepositoryOptions = {
    transaction?: Transaction;
};

@injectable()
export class JISInfotextsRepository extends AbstractValidatableRepository {
    public validator: JSONSchemaValidator;
    public schema = PG_SCHEMA;
    public tableName = JISInfotextsModel.tableName;

    private sequelizeModel: ModelStatic<JISInfotextsModel>;

    constructor(
        @inject(CoreToken.PostgresConnector) connector: IDatabaseConnector,
        @inject(CoreToken.Logger) private logger: ILogger
    ) {
        super(connector, logger);
        this.validator = new JSONSchemaValidator("JISInfotextsRepository", JISInfotextsModel.jsonSchema);
        this.sequelizeModel = connector
            .getConnection()
            .define(this.tableName, JISInfotextsModel.attributeModel, { schema: this.schema });
    }

    /**
     * Delete all items last updated before a given date and time (where their `updated_at` is less than the given limit)
     *
     * @param dateTime The `updated_at` limit, where all items last updated before this limit shall be deleted
     * @param options Options for the operation
     * @returns The number of deleted items
     */
    public async deleteAllLastUpdatedBefore(dateTime: Date, options?: RepositoryOptions): Promise<number> {
        try {
            return await this.sequelizeModel.destroy({
                where: {
                    updated_at: { [Op.lt]: dateTime },
                },
                transaction: options?.transaction,
            });
        } catch (err) {
            throw new GeneralError("Error in deleteAllLastUpdatedBefore", this.constructor.name, err);
        }
    }

    /**
     * Refresh all data from VYMI and delete old data
     *
     * @param data The data to be upserted
     * @param options Options for the operation
     */
    public async refreshData(data: IJISInfotext[], options?: RepositoryOptions) {
        try {
            const currentInfotexts = await this.sequelizeModel.findAll({ transaction: options?.transaction });
            const currentInfotextsMap: Map<string, JISInfotextsModel> = new Map();

            for (const infotext of currentInfotexts) {
                currentInfotextsMap.set(infotext.id, infotext);
            }

            const toBeUpserted: IJISInfotext[] = [];

            for (const infotext of data) {
                const info = currentInfotextsMap.get(infotext.id);
                if (!info || info.updated_at <= infotext.updated_timestamp) {
                    toBeUpserted.push(infotext);
                }
                currentInfotextsMap.delete(infotext.id);
            }

            const toBeDeleted: string[] = [];

            for (const { id } of currentInfotextsMap.values()) {
                toBeDeleted.push(id);
            }

            const [upserted] = await Promise.all([
                this.sequelizeModel.bulkCreate(toBeUpserted, {
                    updateOnDuplicate: this.getUpdateAttributes() as any,
                    transaction: options?.transaction,
                }),
                this.sequelizeModel.destroy({
                    where: {
                        id: { [Op.in]: toBeDeleted },
                    },
                    transaction: options?.transaction,
                }),
            ]);
            return upserted;
        } catch (err) {
            if (err instanceof SequelizeValidationError && err.errors?.length > 0) {
                const mappedErrors = err.errors.map((e) => `${e.message} (${e.value})`).join(", ");
                throw new ValidationError(
                    `Validation error in upsertAll: ${mappedErrors}`,
                    this.constructor.name,
                    err,
                    undefined,
                    "pid"
                );
            }
            throw new GeneralError("Error in upsertAll", this.constructor.name, err);
        }
    }

    private getUpdateAttributes(): string[] {
        return Object.keys(JISInfotextsModel.attributeModel).filter((attribute) => !["created_at"].includes(attribute));
    }
    /**
     * @param options
     * @returns The number of deleted items
     */
    public async deleteAll(options?: RepositoryOptions) {
        try {
            return await this.sequelizeModel.destroy({ where: {}, transaction: options?.transaction });
        } catch (err) {
            throw new GeneralError("Error in deleteAll", this.constructor.name, err);
        }
    }
}
