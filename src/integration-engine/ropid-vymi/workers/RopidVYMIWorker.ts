import { RopidVYMI } from "#sch/ropid-vymi";
import { AbstractWorker } from "@golemio/core/dist/integration-engine/workers";
import { CheckForNewEventsTask } from "./tasks/CheckForNewEventsTask";
import { FetchAndProcessEventsTask } from "./tasks/FetchAndProcessEventsTask";

export class RopidVYMIWorker extends AbstractWorker {
    protected name = RopidVYMI.name.toLowerCase();

    constructor() {
        super();

        // Register tasks
        this.registerTask(new CheckForNewEventsTask(this.getQueuePrefix()));
        this.registerTask(new FetchAndProcessEventsTask(this.getQueuePrefix()));
    }
}
