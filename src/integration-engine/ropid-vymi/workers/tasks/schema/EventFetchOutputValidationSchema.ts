import { IRopidVYMIEvent } from "#sch/ropid-vymi";
import { Type } from "@golemio/core/dist/shared/class-transformer";
import { IsObject, IsString, ValidateNested } from "@golemio/core/dist/shared/class-validator";
import { IEventFetchOutput } from "../interfaces/IEventFetchOutput";
import { RopidVYMIEventValidationSchema } from "./RopidVYMIEventValidationSchema";

export class EventFetchOutputValidationSchema implements IEventFetchOutput {
    @IsString()
    digest!: string;

    @IsObject({ each: true })
    @ValidateNested()
    @Type(() => RopidVYMIEventValidationSchema)
    data!: IRopidVYMIEvent[];
}
