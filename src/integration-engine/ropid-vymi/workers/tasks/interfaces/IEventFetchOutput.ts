import { IRopidVYMIEvent } from "#sch/ropid-vymi";

export interface IEventFetchOutput {
    digest: string;
    data: IRopidVYMIEvent[];
}
