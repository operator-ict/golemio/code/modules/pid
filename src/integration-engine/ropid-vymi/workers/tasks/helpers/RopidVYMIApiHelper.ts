import { IRopidVYMIEvent, RopidVYMI } from "#sch/ropid-vymi";
import { DataSource, JSONDataTypeStrategy, log } from "@golemio/core/dist/integration-engine";
import { config } from "@golemio/core/dist/integration-engine/config";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import crypto, { BinaryLike } from "crypto";
import { IEventFetchOutput } from "../interfaces/IEventFetchOutput";
import { RopidVYMIApiOutputLevel } from "./RopidVYMIApiOutputLevel";

export default class RopidVYMIApiHelper {
    private static _instance: RopidVYMIApiHelper;
    private readonly datasource: DataSource<IRopidVYMIEvent[]>;

    public static getInstance = () => {
        if (!this._instance) {
            this._instance = new RopidVYMIApiHelper();
        }

        return this._instance;
    };

    private constructor() {
        this.datasource = new DataSource(
            RopidVYMI.events.name + "DataSource",
            new HTTPFetchProtocolStrategy({
                headers: config.datasources.RopidVYMIApiHeaders,
                method: "GET",
                timeoutInSeconds: 25,
                url: `${config.datasources.RopidVYMIApiUrl}?level=${RopidVYMIApiOutputLevel.Standard}`,
                responseType: "json",
            }),
            new JSONDataTypeStrategy({ resultsPath: "vymi-report.vymi-list" }),
            new JSONSchemaValidator(RopidVYMI.events.name + "DataSource", RopidVYMI.events.datasourceJsonSchema)
        );
    }

    public getAllEvents = async (): Promise<IEventFetchOutput> => {
        try {
            const events: IRopidVYMIEvent[] = await this.datasource.getAll();
            const digest = this.createDigest(JSON.stringify(events));

            return {
                data: events,
                digest,
            };
        } catch (err: any) {
            log.error(err);
            if (err instanceof AbstractGolemioError) {
                throw err;
            } else {
                throw new GeneralError("Error while getting data", this.constructor.name, err);
            }
        }
    };

    public createDigest = (data: BinaryLike): string => {
        return crypto.createHash("sha1").update(data).digest("hex");
    };
}
