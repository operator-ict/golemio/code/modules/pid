import { PG_SCHEMA } from "#sch/const";
import { IRopidVYMIEventOutput, RopidVYMI } from "#sch/ropid-vymi";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { Op } from "@golemio/core/dist/shared/sequelize";

/**
 * Custom Postgres model for Ropid VYMI Events
 */
export class RopidVYMIEventsModel extends PostgresModel implements IModel {
    constructor(addAuditAttributes: boolean = false) {
        super(
            RopidVYMI.events.name + "Model",
            {
                hasTmpTable: true,
                outputSequelizeAttributes: RopidVYMI.events.outputSequelizeAttributes,
                pgTableName: RopidVYMI.events.pgTableName,
                pgSchema: PG_SCHEMA,
                savingType: "insertOnly",
                addAuditAttributes: addAuditAttributes,
            },
            new JSONSchemaValidator(RopidVYMI.events.name + "ModelValidator", RopidVYMI.events.outputJsonSchema)
        );
    }

    public getCurrentAlerts(currentDate: Date): Promise<IRopidVYMIEventOutput[]> {
        return this.find({
            where: {
                record_type: 1,
                expiration_date: {
                    [Op.or]: {
                        [Op.eq]: null,
                        [Op.gte]: currentDate,
                    },
                },
            },
            raw: true,
        });
    }
}
