import { PG_SCHEMA } from "#sch/const";
import { IRopidVYMIEventRouteOutput, RopidVYMI } from "#sch/ropid-vymi";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

/**
 * Custom Postgres model for Ropid VYMI Events Routes
 */
export class RopidVYMIEventsRoutesModel extends PostgresModel implements IModel {
    constructor(addAuditAttributes: boolean = false) {
        super(
            RopidVYMI.eventsRoutes.name + "Model",
            {
                hasTmpTable: true,
                outputSequelizeAttributes: RopidVYMI.eventsRoutes.outputSequelizeAttributes,
                pgTableName: RopidVYMI.eventsRoutes.pgTableName,
                pgSchema: PG_SCHEMA,
                savingType: "insertOnly",
                addAuditAttributes: addAuditAttributes,
            },
            new JSONSchemaValidator(RopidVYMI.eventsRoutes.name + "ModelValidator", RopidVYMI.eventsRoutes.outputJsonSchema)
        );
    }

    public async findByEventId(eventId: number): Promise<IRopidVYMIEventRouteOutput[]> {
        return await this.find({
            where: {
                event_id: eventId,
            },
            raw: true,
        });
    }
}
