import { PG_SCHEMA } from "#sch/const";
import { RopidVYMI } from "#sch/ropid-vymi";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

/**
 * Custom Postgres model for Ropid VYMI Events Stops
 */
export class RopidVYMIEventsStopsModel extends PostgresModel implements IModel {
    constructor(addAuditAttributes: boolean = false) {
        super(
            RopidVYMI.eventsStops.name + "Model",
            {
                hasTmpTable: true,
                outputSequelizeAttributes: RopidVYMI.eventsStops.outputSequelizeAttributes,
                pgTableName: RopidVYMI.eventsStops.pgTableName,
                pgSchema: PG_SCHEMA,
                savingType: "insertOnly",
                addAuditAttributes: addAuditAttributes,
            },
            new JSONSchemaValidator(RopidVYMI.eventsStops.name + "ModelValidator", RopidVYMI.eventsStops.outputJsonSchema)
        );
    }
}
