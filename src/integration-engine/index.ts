import { AbstractWorker } from "@golemio/core/dist/integration-engine/workers";
import { DataRetentionWorker } from "./data-retention";
import { JISWorker } from "./jis";
import { PresetWorker } from "./ropid-gtfs/workers/presets/PresetWorker";
import { TimetableWorker } from "./ropid-gtfs/workers/timetables/TimetableWorker";
import { RopidVYMIWorker } from "./ropid-vymi/workers/RopidVYMIWorker";
import { DescriptorWorker, GtfsRealTimeWorker, RunsWorker, VPWorker } from "./vehicle-positions";

export * as RopidGTFS from "./ropid-gtfs";
export * as RopidVYMI from "./ropid-vymi";
export * as VehiclePositions from "./vehicle-positions";

export const workers: Array<new () => AbstractWorker> = [
    //#region Vehicle Positions
    VPWorker,
    DescriptorWorker,
    RunsWorker,
    GtfsRealTimeWorker,
    //#endregion

    //#region ROPID GTFS
    PresetWorker,
    TimetableWorker,
    //#endregion

    //#region ROPID VYMI
    RopidVYMIWorker,
    //#endregion

    //#region Data Retention
    DataRetentionWorker,
    //#endregion

    //#region JIS
    JISWorker,
    //#endregion
];
