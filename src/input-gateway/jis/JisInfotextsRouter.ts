import { checkContentTypeMiddleware } from "@golemio/core/dist/input-gateway/helpers/CheckContentTypeMiddleware";
import { NextFunction, Request, Response, Router } from "@golemio/core/dist/shared/express";
import { JisInfotextsController } from "./JisInfotextsController";

export class JisInfotextsRouter {
    public router: Router;
    private controller: JisInfotextsController;

    constructor() {
        this.router = Router();
        this.controller = new JisInfotextsController();
        this.initRoutes();
    }

    private initRoutes() {
        this.router.post("/infotexts", checkContentTypeMiddleware(["application/json"]), this.post);
    }

    private post = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            await this.controller.processData(req.body);
            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };
}
