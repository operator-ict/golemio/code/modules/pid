import { jisInfotextsJsonSchema } from "#sch/jis/datasources/JISInfotextsJsonSchema";
import { BaseController } from "@golemio/core/dist/input-gateway/controllers";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class JisInfotextsController extends BaseController {
    public name!: string;
    protected validator!: JSONSchemaValidator;
    protected queuePrefix!: string;

    constructor() {
        super("jis", new JSONSchemaValidator("jisInfotextsDataValidation", jisInfotextsJsonSchema));
    }

    public processData = async (inputData: any): Promise<void> => {
        try {
            await this.validator.Validate(inputData);
            await this.sendMessageToExchange("input." + this.queuePrefix + ".refreshJISInfotexts", JSON.stringify(inputData));
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            } else {
                throw new GeneralError("Error while validating input data.", this.constructor.name, err, 422);
            }
        }
    };
}
