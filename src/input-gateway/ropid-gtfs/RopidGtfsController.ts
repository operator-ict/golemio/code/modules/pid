import { RopidDeparturesPresets } from "#sch/ropid-departures-presets";
import { BaseController } from "@golemio/core/dist/input-gateway/controllers";
import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class RopidGtfsController extends BaseController {
    public name!: string;
    protected validator!: JSONSchemaValidator;
    protected queuePrefix!: string;

    constructor() {
        super(
            "RopidPresets",
            new JSONSchemaValidator(RopidDeparturesPresets.name + "Controller", RopidDeparturesPresets.datasourceJsonSchema)
        );
    }

    public processData = async (inputData: any): Promise<void> => {
        try {
            await this.validator.Validate(inputData);
            await this.sendMessageToExchange("input." + this.queuePrefix + ".savePresets", JSON.stringify(inputData), {
                persistent: true,
            });
        } catch (err) {
            if (err instanceof CustomError) {
                throw err;
            } else {
                throw new CustomError("Error while validating input data.", true, this.name, 422, err);
            }
        }
    };
}
