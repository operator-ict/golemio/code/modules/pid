import { VehiclePositions } from "#sch/vehicle-positions";
import { IMpvMessageInput } from "#sch/vehicle-positions/interfaces/IMpvMessageInterfaces";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { BaseController } from "@golemio/core/dist/input-gateway/controllers";
import { InputGatewayContainer } from "@golemio/core/dist/input-gateway/ioc";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

const DEFAULT_BATCH_SIZE = 500;

export class VehiclePositionsController extends BaseController {
    public name!: string;
    protected validator!: JSONSchemaValidator;
    protected queuePrefix!: string;
    private batchSize: number;

    constructor() {
        super(
            VehiclePositions.name,
            new JSONSchemaValidator(VehiclePositions.name + "Controller", VehiclePositions.datasourceJsonSchema)
        );
        const config = InputGatewayContainer.resolve<ISimpleConfig>(CoreToken.SimpleConfig);
        const batchSize = config.getValue("env.VEHICLE_POSITIONS_SAVE_DATA_BATCH_SIZE", DEFAULT_BATCH_SIZE.toString());
        this.batchSize = Number.parseInt(batchSize);
    }

    /**
     * Data processing
     */
    public processData = async (inputData: IMpvMessageInput): Promise<void> => {
        try {
            await this.validator.Validate(inputData); // throws an error
            const dataBatches = this.splitData(inputData);
            for (const batch of dataBatches) {
                await this.sendMessageToExchange("input." + this.queuePrefix + ".saveDataToDB", JSON.stringify(batch), {
                    persistent: true,
                    timestamp: new Date().getTime(),
                });
            }
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            } else {
                throw new GeneralError("Error while validating input data.", this.name, err, 422);
            }
        }
    };

    private splitData = (data: IMpvMessageInput): IMpvMessageInput[] => {
        if (!Array.isArray(data.m.spoj)) {
            return [data];
        }

        const result: IMpvMessageInput[] = [];
        for (let i = 0, chunkSize = this.batchSize; i < data.m.spoj.length; i += chunkSize) {
            const chunk = data.m.spoj.slice(i, i + chunkSize);
            result.push({ m: { spoj: chunk } });
        }
        return result;
    };
}
