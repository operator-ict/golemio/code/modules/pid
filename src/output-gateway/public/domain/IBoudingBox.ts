export interface IBoundingBox {
    topLeftLongitude: number;
    topLeftLatitude: number;
    bottomRightLongitude: number;
    bottomRightLatitude: number;
}

export interface IBoundingBoxWithCenter {
    centerPoint: IPoint;
    width: number;
    height: number;
}

export interface IPoint {
    longitude: number;
    latitude: number;
}
