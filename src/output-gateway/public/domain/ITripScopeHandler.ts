import { IPublicApiGtfsTrip } from "./PublicApiGtfsTripLookupInterfaces";

export interface ITripScopeHandler<T extends IPublicApiGtfsTrip, K> {
    handle(output: T, entity: K): T;
}

export interface ITripScopeAsyncHandler<T extends IPublicApiGtfsTrip, K> {
    handle(output: T, entity: K): Promise<T>;
}
