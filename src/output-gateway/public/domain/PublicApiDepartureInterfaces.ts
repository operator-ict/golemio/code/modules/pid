export interface IPublicApiDepartureInfo {
    timestamp_scheduled: string;
    timestamp_predicted: string;
    delay_seconds: number | null;
    minutes: number;
}

export interface IPublicApiDepartureStop {
    id: string;
    sequence: number;
    platform_code: string | null;
}

export interface IPublicApiDepartureRoute {
    type: string;
    short_name: string;
}

export interface IPublicApiDepartureTrip {
    id: string;
    headsign: string;
    is_canceled: boolean | null;
}

export interface IPublicApiDepartureVehicle {
    id: string | null;
    is_wheelchair_accessible: boolean | null;
    is_air_conditioned: boolean | null;
    has_charger: boolean | null;
}

export interface IPublicApiDeparture {
    departure: IPublicApiDepartureInfo;
    stop: IPublicApiDepartureStop;
    route: IPublicApiDepartureRoute;
    trip: IPublicApiDepartureTrip;
    vehicle: IPublicApiDepartureVehicle;
}

export type PublicApiDepartureBoardGroup = IPublicApiDeparture[];
