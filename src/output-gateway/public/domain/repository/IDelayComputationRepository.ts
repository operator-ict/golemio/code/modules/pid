import { IDelayComputationDto } from "#sch/ropid-gtfs/redis/interfaces/IDelayComputationDto";

export interface IDelayComputationRepository {
    getDelayComputationCache(gtfsTripId: string): Promise<IDelayComputationDto | null>;
}
