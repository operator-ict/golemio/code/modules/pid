import { IPublicGtfsDepartureCacheDto } from "#sch/ropid-gtfs/redis/interfaces/IPublicGtfsDepartureCacheDto";

export interface IGtfsDepartureRepository {
    getPublicGtfsDepartureCache(stopId: string[], minutesAfter: number): Promise<IPublicGtfsDepartureCacheDto[]>;
}
