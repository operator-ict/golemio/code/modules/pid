import { IPublicStopTimeCacheDto } from "#sch/vehicle-positions/redis/interfaces/IPublicStopTimeCacheDto";

export interface IStopTimeRepository {
    getPublicStopTimeCache(vehicleId: string, gtfsTripId: string): Promise<IPublicStopTimeCacheDto[]>;
}
