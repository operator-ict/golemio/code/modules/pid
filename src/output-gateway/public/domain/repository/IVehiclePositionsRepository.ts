import { IPublicApiCacheDto } from "#sch/vehicle-positions/redis/interfaces/IPublicApiCacheDto";
import { IBoundingBoxWithCenter } from "../IBoudingBox";

export interface IVehiclePositionsRepository {
    setCurrentSetName(name: string): void;
    getAllVehicleIds(boundingBox: IBoundingBoxWithCenter): Promise<string[]>;
    getAllVehiclePositions(vehicleIds: string[]): Promise<IPublicApiCacheDto[]>;
    getAllVehiclePositionsForMultipleTrips(tripIds: string[]): Promise<Map<string, IPublicApiCacheDto[]>>;
    getDetailedVehiclePosition(vehicleId: string, tripdId?: string): Promise<IPublicApiCacheDto | null>;
    getTripsWithUntrackedVehicles(tripIds: string[]): Promise<Set<string>>;
}
