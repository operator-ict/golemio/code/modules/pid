import { IGeoCoordinatesPoint, IGeoJSONFeatureCollection } from "@golemio/core/dist/output-gateway/Geo";
import { FeatureCollection, Point } from "@golemio/core/dist/shared/geojson";
import { StatePositionEnum } from "src/const";

export interface IPublicApiDetailedTripScopeInfo {
    gtfs_trip_id: string;
    route_type: string;
    route_short_name: string;
    shape_id: string | null;
    origin_route_name: string | null;
    run_number: number | null;
    trip_headsign: string | null;
    geometry: IGeoCoordinatesPoint;
    shape_dist_traveled: number | null;
    bearing: number | null;
    delay: number | null;
    state_position: StatePositionEnum;
    last_stop_sequence: number | null;
    origin_timestamp: string | null;
}

export interface IPublicApiDetailedTripScopeStopTimesProps {
    stop_name: string;
    stop_sequence: number;
    zone_id: string | null;
    is_wheelchair_accessible: boolean | null;
    shape_dist_traveled: number;
    arrival_time: string;
    departure_time: string;
    realtime_arrival_time: string | null;
    realtime_departure_time: string | null;
}

export interface IPublicApiDetailedTripScopeStopTimes
    extends FeatureCollection<Point, IPublicApiDetailedTripScopeStopTimesProps> {}

export interface IPublicApiDetailedTripScopeShapesProps {
    shape_dist_traveled: number;
}

export interface IPublicApiDetailedTripScopeShapes extends FeatureCollection<Point, IPublicApiDetailedTripScopeShapesProps> {}

export interface IPublicApiDetailedTripScopeDescriptor {
    operator: string | null;
    vehicle_type: string | null;
    is_wheelchair_accessible: boolean | null;
    is_air_conditioned: boolean | null;
    has_usb_chargers: boolean | null;
    vehicle_registration_number: string | null;
}

export interface IPublicApiDetailedTrip extends Partial<IPublicApiDetailedTripScopeInfo> {
    stop_times?: IGeoJSONFeatureCollection | IPublicApiDetailedTripScopeStopTimes;
    shapes?: IGeoJSONFeatureCollection | IPublicApiDetailedTripScopeShapes;
    vehicle_descriptor?: IPublicApiDetailedTripScopeDescriptor;
}
