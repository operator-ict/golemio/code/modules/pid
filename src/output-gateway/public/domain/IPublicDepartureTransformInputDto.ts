import { IPublicGtfsDepartureCacheDto } from "#sch/ropid-gtfs/redis/interfaces/IPublicGtfsDepartureCacheDto";
import { IDescriptorOutputDto } from "#sch/vehicle-descriptors/models/interfaces";
import { IPublicApiCacheDto } from "#sch/vehicle-positions/redis/interfaces/IPublicApiCacheDto";
import { IPublicStopTimeCacheDto } from "#sch/vehicle-positions/redis/interfaces/IPublicStopTimeCacheDto";

export interface IPublicDepartureTransformInputDto {
    departure: IPublicGtfsDepartureCacheDto;
    vehiclePosition: IPublicApiCacheDto | null;
    vehicleDescriptor: Partial<IDescriptorOutputDto> | null;
    stopTime: IPublicStopTimeCacheDto | null;
}
