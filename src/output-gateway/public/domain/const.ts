import { IBoundingBox } from "./IBoudingBox";

export const APP_NAME = "vp-litacka-api";

// currently bb around whole czechia
export const DEFAULT_BOUDINGBOX: IBoundingBox = {
    topLeftLongitude: 12.04,
    topLeftLatitude: 51.05,
    bottomRightLongitude: 19.01,
    bottomRightLatitude: 48.54,
};
