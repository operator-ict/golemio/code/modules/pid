import { IGeoJSONFeatureCollection } from "@golemio/core/dist/output-gateway/Geo";
import { FeatureCollection, Point } from "@golemio/core/dist/shared/geojson";

export interface IPublicApiGtfsTripScopeInfo {
    gtfs_trip_id: string;
    route_type: string;
    route_short_name: string;
    shape_id: string | null;
    origin_route_name: string | null;
    run_number: number | null;
    trip_headsign: string | null;
}

export interface IPublicApiGtfsTripScopeStopTimesProps {
    stop_name: string;
    stop_sequence: number;
    zone_id: string | null;
    is_wheelchair_accessible: boolean | null;
    shape_dist_traveled: number;
    arrival_time: string;
    departure_time: string;
}

export interface IPublicApiGtfsTripScopeStopTimes extends FeatureCollection<Point, IPublicApiGtfsTripScopeStopTimesProps> {}

export interface IPublicApiGtfsTripScopeShapesProps {
    shape_dist_traveled: number;
}

export interface IPublicApiGtfsTripScopeShapes extends FeatureCollection<Point, IPublicApiGtfsTripScopeShapesProps> {}

export interface IPublicApiGtfsTripScopeDescriptor {
    is_wheelchair_accessible: boolean | null;
}

export interface IPublicApiGtfsTrip extends Partial<IPublicApiGtfsTripScopeInfo> {
    stop_times?: IGeoJSONFeatureCollection | IPublicApiGtfsTripScopeStopTimes;
    shapes?: IGeoJSONFeatureCollection | IPublicApiGtfsTripScopeShapes;
    vehicle_descriptor?: IPublicApiGtfsTripScopeDescriptor;
}
