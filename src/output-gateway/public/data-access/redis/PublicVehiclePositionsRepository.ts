import { IBoundingBoxWithCenter } from "#og/public/domain/IBoudingBox";
import { IVehiclePositionsRepository } from "#og/public/domain/repository/IVehiclePositionsRepository";
import { IPublicApiCacheDto } from "#sch/vehicle-positions/redis/interfaces/IPublicApiCacheDto";
import { IoRedisConnector } from "@golemio/core/dist/helpers/data-access/redis/IoRedisConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { ContainerToken } from "@golemio/core/dist/output-gateway/ioc";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class PublicVehiclePositionsRepository implements IVehiclePositionsRepository {
    private setName: string | undefined = undefined; // loaded via pub sub

    constructor(
        @inject(ContainerToken.RedisConnector) private redisConnector: IoRedisConnector,
        @inject(CoreToken.Logger) private log: ILogger
    ) {}

    public setCurrentSetName(name: string): void {
        this.setName = name;
    }

    public async getAllVehicleIds(boundingBox: IBoundingBoxWithCenter): Promise<string[]> {
        if (!this.setName) {
            this.log.debug("Empty setName for public vehicle positions api.");
            return [];
        }

        if (!this.redisConnector.isConnected()) {
            await this.redisConnector.connect();
        }

        const connection = this.redisConnector.getConnection();
        let vehicleIds: string[] = [];

        try {
            vehicleIds = (await connection.geosearch(
                this.setName,
                "FROMLONLAT",
                boundingBox.centerPoint.longitude,
                boundingBox.centerPoint.latitude,
                "BYBOX",
                boundingBox.width,
                boundingBox.height,
                "m",
                "ASC"
            )) as string[];
        } catch (error) {
            throw new GeneralError("Cannot get vehicle ids from cache", this.constructor.name, error);
        }

        return vehicleIds;
    }

    public async getAllVehiclePositions(vehicleIds: string[], tripIds?: string[]): Promise<IPublicApiCacheDto[]> {
        if (!this.setName) {
            this.log.debug("Empty setName for public vehicle positions api.");
            return [];
        }

        const connection = this.redisConnector.getConnection();
        let vehiclePositions: Array<string | null> = [];

        try {
            const keys: string[] = [];
            if (tripIds) {
                for (const tripId of tripIds) {
                    keys.push(...vehicleIds.map((vehicleId) => `${this.setName}:future-vehicle-${vehicleId}-${tripId}`));
                }
            } else {
                keys.push(...vehicleIds.map((vehicleId) => `${this.setName}:vehicle-${vehicleId}`));
            }

            vehiclePositions = await connection.mget(keys);
        } catch (error) {
            throw new GeneralError("Cannot get vehicle positions from cache", this.constructor.name, error);
        }

        const parsedVehiclePositions: IPublicApiCacheDto[] = [];
        for (const vehiclePosition of vehiclePositions) {
            if (vehiclePosition === null) {
                continue;
            }

            try {
                parsedVehiclePositions.push(JSON.parse(vehiclePosition));
            } catch (error) {
                throw new GeneralError("Cannot parse vehicle position", this.constructor.name, error);
            }
        }

        return parsedVehiclePositions;
    }

    public async getAllVehiclePositionsForMultipleTrips(tripIds: string[]): Promise<Map<string, IPublicApiCacheDto[]>> {
        const positionsByTrip = new Map<string, IPublicApiCacheDto[]>();

        if (!this.setName || tripIds.length === 0) {
            this.log.debug("Empty setName for public vehicle positions api or empty trip ids.");
            return positionsByTrip;
        }

        if (!this.redisConnector.isConnected()) {
            await this.redisConnector.connect();
        }

        const [vehicleIdsCurrent, vehicleIdsFuture] = await Promise.all([
            this.getVehicleIdsForMultiple(tripIds, "trip"),
            this.getVehicleIdsForMultiple(tripIds, "future-trip"),
        ]);

        const vehicleIds = Array.from(new Set([...vehicleIdsCurrent, ...vehicleIdsFuture]));
        if (vehicleIds.length > 0) {
            const allPositions = await this.getAllVehiclePositions(vehicleIds);
            for (const position of allPositions) {
                if (!positionsByTrip.has(position.gtfs_trip_id)) {
                    positionsByTrip.set(position.gtfs_trip_id, []);
                }
                positionsByTrip.get(position.gtfs_trip_id)!.push(position);
            }

            const futurePositions = await this.getAllVehiclePositions(vehicleIds, tripIds);
            for (const position of futurePositions) {
                if (!positionsByTrip.has(position.gtfs_trip_id)) {
                    positionsByTrip.set(position.gtfs_trip_id, []);
                }
                positionsByTrip.get(position.gtfs_trip_id)!.push(position);
            }
        }
        const canceledTrips = await this.getCanceledTrips(tripIds);
        for (const canceledTrip of canceledTrips) {
            positionsByTrip.set(canceledTrip.gtfs_trip_id, [canceledTrip]);
        }

        return positionsByTrip;
    }

    public async getTripsWithUntrackedVehicles(tripIds: string[]): Promise<Set<string>> {
        const untrackedTrips = new Set<string>();

        if (!this.setName || tripIds.length === 0) {
            this.log.debug("Empty setName for public vehicle positions api or empty trip ids.");
            return untrackedTrips;
        }

        if (!this.redisConnector.isConnected()) {
            await this.redisConnector.connect();
        }

        const vehicleIds = await this.getVehicleIdsForMultiple(tripIds, "trip");

        try {
            // TODO optimization point here - use JSON.GET once it's available in the redis/valkey server
            const allPositions = await this.getAllVehiclePositions(vehicleIds);

            for (let i = 0; i < allPositions.length; i++) {
                if (allPositions[i].delay === null) {
                    untrackedTrips.add(allPositions[i].gtfs_trip_id);
                }
            }
        } catch (error) {
            throw new GeneralError("Cannot get vehicle positions from cache", this.constructor.name, error);
        }

        return untrackedTrips;
    }

    // Current trips are saved in the cache with key made from vehicleId only. For future trips tripId has to be also provided.
    public async getDetailedVehiclePosition(vehicleId: string, tripId?: string): Promise<IPublicApiCacheDto | null> {
        if (!this.setName) {
            this.log.debug("Empty setName for public vehicle positions api.");
            return null;
        }

        if (!this.redisConnector.isConnected()) {
            await this.redisConnector.connect();
        }

        const connection = this.redisConnector.getConnection();
        let vehiclePosition: string | null = null;

        try {
            const key = tripId ? `${this.setName}:future-trip-${vehicleId}-${tripId}` : `${this.setName}:vehicle-${vehicleId}`;
            vehiclePosition = await connection.get(key);
        } catch (error) {
            this.log.error(error, "Cannot get vehicle position from cache");
            throw new GeneralError("Cannot get vehicle position from cache", this.constructor.name, error);
        }

        if (vehiclePosition === null) {
            this.log.info(`${this.constructor.name}: Cannot find vehicle_id ${vehicleId}`);
            return null;
        }

        try {
            return JSON.parse(vehiclePosition);
        } catch (error) {
            this.log.error(error, `Cannot parse ${vehicleId}`);
            throw new GeneralError(`Cannot parse ${vehicleId}`, this.constructor.name, error);
        }
    }

    private async getVehicleIdsForMultiple(tripIds: string[], keyPrefix: string): Promise<string[]> {
        const vehicleIdsByTrip = new Map<string, string[]>();
        const connection = this.redisConnector.getConnection();
        const pipeline = connection.pipeline();

        for (const tripId of tripIds) {
            pipeline.lrange(`${this.setName}:${keyPrefix}-${tripId}`, 0, -1);
        }

        try {
            const trips = await pipeline.exec();

            for (const [error, vehicleIds] of trips!) {
                if (error) {
                    throw new GeneralError(`Cannot get vehicle ids for ${keyPrefix} from cache`, this.constructor.name, error);
                }

                vehicleIdsByTrip.set(tripIds[vehicleIdsByTrip.size], vehicleIds as string[]);
            }
        } catch (error) {
            if (error instanceof GeneralError) {
                throw error;
            }

            throw new GeneralError(`Cannot get vehicle ids for ${keyPrefix} from cache`, this.constructor.name, error);
        }

        return Array.from(vehicleIdsByTrip.values()).flat();
    }

    private async getCanceledTrips(tripIds: string[]): Promise<IPublicApiCacheDto[]> {
        const connection = this.redisConnector.getConnection();
        const parsedCanceledTrips: IPublicApiCacheDto[] = [];

        if (tripIds.length === 0) {
            return [];
        }

        const keys: string[] = [];
        for (const tripId of tripIds) {
            keys.push(`${this.setName}:canceled-trips-${tripId}`);
        }

        const canceledTrips = await connection.mget(keys);
        for (const canceledTrip of canceledTrips) {
            if (canceledTrip === null) {
                continue;
            } else {
                try {
                    parsedCanceledTrips.push(JSON.parse(canceledTrip));
                } catch (error) {
                    throw new GeneralError("Cannot parse canceled trip", this.constructor.name, error);
                }
            }
        }
        return parsedCanceledTrips;
    }
}
