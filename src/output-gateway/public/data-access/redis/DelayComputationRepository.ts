import { IDelayComputationRepository } from "#og/public/domain/repository/IDelayComputationRepository";
import { IDelayComputationDto } from "#sch/ropid-gtfs/redis/interfaces/IDelayComputationDto";
import { GTFS_DELAY_COMPUTATION_NAMESPACE_PREFIX } from "#sch/vehicle-positions/redis/const";
import { IoRedisConnector } from "@golemio/core/dist/helpers/data-access/redis/IoRedisConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { ContainerToken } from "@golemio/core/dist/output-gateway/ioc";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class DelayComputationRepository implements IDelayComputationRepository {
    constructor(
        @inject(ContainerToken.RedisConnector) private redisConnector: IoRedisConnector,
        @inject(CoreToken.Logger) private log: ILogger
    ) {}

    public async getDelayComputationCache(gtfsTripId: string): Promise<IDelayComputationDto | null> {
        if (!this.redisConnector.isConnected()) {
            await this.redisConnector.connect();
        }

        const connection = this.redisConnector.getConnection();
        let delayComputationCache: string | null = null;

        try {
            delayComputationCache = await connection.get(`${GTFS_DELAY_COMPUTATION_NAMESPACE_PREFIX}:${gtfsTripId}`);
        } catch (err) {
            throw new GeneralError(
                `${this.constructor.name}: Cannot get delay computation cache from cache: ${err.message}`,
                this.constructor.name,
                err
            );
        }

        if (delayComputationCache === null) {
            this.log.info(`${this.constructor.name}: Cannot find delay computation cache`);
            return null;
        }

        try {
            return JSON.parse(delayComputationCache);
        } catch (err) {
            throw new GeneralError(
                `${this.constructor.name}: Cannot parse delay computation cache: ${err.message}`,
                this.constructor.name,
                err
            );
        }
    }
}
