import { IGtfsDepartureRepository } from "#og/public/domain/repository/IGtfsDepartureRepository";
import { PUBLIC_DEPARTURE_NAMESPACE_PREFIX } from "#sch/ropid-gtfs/redis/const";
import { IPublicGtfsDepartureCacheDto } from "#sch/ropid-gtfs/redis/interfaces/IPublicGtfsDepartureCacheDto";
import { IoRedisConnector } from "@golemio/core/dist/helpers/data-access/redis/IoRedisConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { ContainerToken } from "@golemio/core/dist/output-gateway/ioc";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class PublicGtfsDepartureRepository implements IGtfsDepartureRepository {
    constructor(
        @inject(ContainerToken.RedisConnector) private redisConnector: IoRedisConnector,
        @inject(CoreToken.Logger) private log: ILogger
    ) {}

    public async getPublicGtfsDepartureCache(stopIds: string[], minutesAfter: number): Promise<IPublicGtfsDepartureCacheDto[]> {
        if (!this.redisConnector.isConnected()) {
            await this.redisConnector.connect();
        }

        const allDepartures: IPublicGtfsDepartureCacheDto[][] = await Promise.all(
            stopIds.map((stopId) => this.getAndParseDepartureCache(stopId, minutesAfter))
        );

        return allDepartures.flat();
    }

    private getAndParseDepartureCache = async (stopId: string, minutesAfter: number): Promise<IPublicGtfsDepartureCacheDto[]> => {
        const connection = this.redisConnector.getConnection();
        let departureCache: string[];

        try {
            departureCache = await connection.zrangebyscore(
                `${PUBLIC_DEPARTURE_NAMESPACE_PREFIX}:${stopId}`,
                "-inf",
                Math.round(new Date().getTime() / 1000) + minutesAfter * 60
            );
        } catch (err) {
            throw new GeneralError(
                `${this.constructor.name}: Cannot get public departure cache from cache: ${err.message}`,
                this.constructor.name,
                err
            );
        }

        if (departureCache.length === 0) {
            this.log.info(`${this.constructor.name}: Cannot find public departure cache for stop ${stopId} in cache`);
            return [];
        }

        try {
            return departureCache.map((cache) => JSON.parse(cache) as IPublicGtfsDepartureCacheDto);
        } catch (err) {
            throw new GeneralError(
                `${this.constructor.name}: Cannot parse public departure cache: ${err.message}`,
                this.constructor.name,
                err
            );
        }
    };
}
