import { IStopTimeRepository } from "#og/public/domain/repository/IStopTimeRepository";
import { PUBLIC_STOP_TIME_CACHE_NAMESPACE_PREFIX } from "#sch/vehicle-positions/redis/const";
import { IPublicStopTimeCacheDto } from "#sch/vehicle-positions/redis/interfaces/IPublicStopTimeCacheDto";
import { IoRedisConnector } from "@golemio/core/dist/helpers/data-access/redis/IoRedisConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { ContainerToken } from "@golemio/core/dist/output-gateway/ioc";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class PublicStopTimeRepository implements IStopTimeRepository {
    constructor(
        @inject(ContainerToken.RedisConnector) private redisConnector: IoRedisConnector,
        @inject(CoreToken.Logger) private log: ILogger
    ) {}

    public async getPublicStopTimeCache(vehicleId: string, gtfsTripId: string): Promise<IPublicStopTimeCacheDto[]> {
        if (!this.redisConnector.isConnected()) {
            await this.redisConnector.connect();
        }

        const connection = this.redisConnector.getConnection();
        let stopTimeCache: string | null = null;

        try {
            const key = `${vehicleId}-${gtfsTripId}`;
            stopTimeCache = await connection.get(`${PUBLIC_STOP_TIME_CACHE_NAMESPACE_PREFIX}:${key}`);
        } catch (err) {
            throw new GeneralError(
                `${this.constructor.name}: Cannot get public stop time cache from cache: ${err.message}`,
                this.constructor.name,
                err
            );
        }

        if (stopTimeCache === null) {
            this.log.info(`${this.constructor.name}: Cannot find public stop time cache`);
            return [];
        }

        try {
            return JSON.parse(stopTimeCache);
        } catch (err) {
            throw new GeneralError(
                `${this.constructor.name}: Cannot parse public stop time cache: ${err.message}`,
                this.constructor.name,
                err
            );
        }
    }
}
