import { GTFSRouteTypeEnum } from "#helpers/RouteTypeEnums";
import { PG_SCHEMA } from "#sch/const";
import { DescriptorModel } from "#sch/vehicle-descriptors/models";
import { IDescriptorOutputDto } from "#sch/vehicle-descriptors/models/interfaces/IDescriptorOutputDto";
import { ILogger } from "@golemio/core/dist/helpers";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { AbstractCachedRepository } from "@golemio/core/dist/helpers/data-access/postgres/repositories/AbstractCachedRepository";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { ModelStatic } from "@golemio/core/dist/shared/sequelize";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class VehicleDescriptorCachedRepository extends AbstractCachedRepository<IDescriptorOutputDto> {
    public schema = PG_SCHEMA;
    public tableName = DescriptorModel.tableName;
    private sequelizeModel: ModelStatic<DescriptorModel>;

    constructor(@inject(CoreToken.PostgresConnector) connector: IDatabaseConnector, @inject(CoreToken.Logger) logger: ILogger) {
        const ttlInSeconds = 4 * 60 * 60;
        super(connector, logger, ttlInSeconds);
        this.sequelizeModel = connector
            .getConnection()
            .define(this.tableName, DescriptorModel.attributeModel, { schema: this.schema, timestamps: false });
    }

    protected async getAllInternal(): Promise<IDescriptorOutputDto[]> {
        return this.sequelizeModel.findAll({ raw: true });
    }

    public async getOneByRegNumber(
        routeType: GTFSRouteTypeEnum,
        registrationNumber: number
    ): Promise<IDescriptorOutputDto | null> {
        try {
            const data = await this.getAll();
            const vehicleDescriptor = data.find(
                (el) => el.gtfs_route_type === routeType && el.registration_number === registrationNumber
            );

            if (!vehicleDescriptor) {
                return null;
            }

            return vehicleDescriptor;
        } catch (err) {
            throw new GeneralError(
                `${this.constructor.name}: Cannot get vehicle descriptor from cache: ${err.message}`,
                this.constructor.name,
                err
            );
        }
    }
}
