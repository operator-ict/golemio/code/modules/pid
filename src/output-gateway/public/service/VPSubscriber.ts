import { ISubscriberOptions, RedisSubscriber } from "@golemio/core/dist/helpers/data-access/pubsub/subscribers/RedisSubscriber";
import { IVehiclePositionsRepository } from "../domain/repository/IVehiclePositionsRepository";

export class VPSubscriber extends RedisSubscriber {
    constructor(options: ISubscriberOptions, private repository: IVehiclePositionsRepository) {
        super(options);
    }

    public async initialize(): Promise<void> {
        await this.subscribe();

        this.logger.debug("Redis subscriber subscribed to channel for Public API.");
        this.listen((message) => {
            this.logger.debug("Redis subscriber received a message to change repository source.");
            if (message !== undefined) {
                this.repository.setCurrentSetName(message);
            }
        });
    }
}
