import { GtfsTripWheelchairAccessEnum } from "#helpers/AccessibilityEnums";
import { IPublicApiGtfsTripScopeDescriptor } from "#og/public/domain/PublicApiGtfsTripLookupInterfaces";
import { ITripWithOptionalAssociationsDto } from "#sch/ropid-gtfs/interfaces/ITripWithOptionalAssociationsDto";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class PublicGtfsVehicleDescriptorTransformation extends AbstractTransformation<
    ITripWithOptionalAssociationsDto,
    IPublicApiGtfsTripScopeDescriptor
> {
    public name = "GtfsVehicleDescriptorTransformation";

    protected transformInternal = (gtfsTrip: ITripWithOptionalAssociationsDto) => {
        return {
            is_wheelchair_accessible: this.isVehicleWheelchairAccessible(gtfsTrip),
        };
    };

    private isVehicleWheelchairAccessible = ({ wheelchair_accessible }: ITripWithOptionalAssociationsDto): boolean | null => {
        if (wheelchair_accessible === null || wheelchair_accessible === GtfsTripWheelchairAccessEnum.NoInformation) {
            return null;
        }

        return wheelchair_accessible === GtfsTripWheelchairAccessEnum.AccessibleVehicle;
    };
}
