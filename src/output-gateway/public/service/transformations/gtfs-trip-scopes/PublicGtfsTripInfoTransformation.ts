import { GTFSRouteTypeEnum } from "#helpers/RouteTypeEnums";
import { IPublicApiGtfsTripScopeInfo } from "#og/public/domain/PublicApiGtfsTripLookupInterfaces";
import { ITripWithDefinedRouteDto } from "#sch/ropid-gtfs/interfaces/ITripWithOptionalAssociationsDto";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class PublicGtfsTripInfoTransformation extends AbstractTransformation<
    ITripWithDefinedRouteDto,
    IPublicApiGtfsTripScopeInfo
> {
    public name = "GtfsTripInfoTransformation";

    protected transformInternal = (gtfsTrip: ITripWithDefinedRouteDto) => {
        const routeType = gtfsTrip.route.route_type;
        return {
            gtfs_trip_id: gtfsTrip.trip_id,
            route_type: (GTFSRouteTypeEnum[routeType ?? 1700] ?? GTFSRouteTypeEnum[1700]).toLowerCase(),
            route_short_name: gtfsTrip.route.route_short_name,
            shape_id: gtfsTrip.shape_id,
            origin_route_name: gtfsTrip.route.route_id.substring(1), // L991 -> 991
            run_number: gtfsTrip.schedule?.run_number ?? null,
            trip_headsign: gtfsTrip.trip_headsign,
        };
    };
}
