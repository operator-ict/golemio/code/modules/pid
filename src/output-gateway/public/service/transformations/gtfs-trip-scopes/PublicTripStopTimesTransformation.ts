import { GtfsStopWheelchairBoardingEnum } from "#helpers/AccessibilityEnums";
import { IPublicApiGtfsTripScopeStopTimesProps } from "#og/public/domain/PublicApiGtfsTripLookupInterfaces";
import { IStopTimeWithStopDto } from "#sch/ropid-gtfs/interfaces/ITripWithOptionalAssociationsDto";
import { IStop } from "#sch/ropid-gtfs/redis/interfaces/IDelayComputationDto";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { IGeoJSONFeature, buildGeojsonFeature } from "@golemio/core/dist/output-gateway";
import { DateTime } from "@golemio/core/dist/shared/luxon";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class PublicGtfsTripStopTimesTransformation extends AbstractTransformation<IStopTimeWithStopDto, IGeoJSONFeature> {
    public name = "GtfsTripStopTimesTransformation";

    protected transformInternal = (stopTime: IStopTimeWithStopDto) => {
        const stop = stopTime.stop;

        const startDateTime = DateTime.now().startOf("day");
        const properties: IPublicApiGtfsTripScopeStopTimesProps & { lng: number; lat: number } = {
            stop_name: stop.stop_name,
            stop_sequence: stopTime.stop_sequence,
            zone_id: stop.zone_id,
            is_wheelchair_accessible: this.isStopWheelchairAccessible(stop),
            shape_dist_traveled: stopTime.shape_dist_traveled,
            arrival_time: startDateTime.plus({ seconds: stopTime.arrival_time_seconds }).toFormat("HH:mm:ss"),
            departure_time: startDateTime.plus({ seconds: stopTime.departure_time_seconds }).toFormat("HH:mm:ss"),
            lng: stop.stop_lon,
            lat: stop.stop_lat,
        };

        return buildGeojsonFeature(properties, "lng", "lat", true);
    };

    private isStopWheelchairAccessible = ({ wheelchair_boarding }: IStop): boolean | null => {
        if (wheelchair_boarding === null || wheelchair_boarding === GtfsStopWheelchairBoardingEnum.NoInformation) {
            return null;
        }

        return wheelchair_boarding === GtfsStopWheelchairBoardingEnum.AccessibleStation;
    };
}
