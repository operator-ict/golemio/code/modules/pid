import { IPublicApiDetailedTripScopeShapesProps } from "#og/public/domain/PublicApiDetailedTripInterfaces";
import { IShapeDto } from "#sch/ropid-gtfs/interfaces/IShapeDto";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { IGeoJSONFeature, buildGeojsonFeature } from "@golemio/core/dist/output-gateway";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class PublicGtfsTripShapesTransformation extends AbstractTransformation<IShapeDto, IGeoJSONFeature> {
    public name = "GtfsTripShapesTransformation";

    protected transformInternal = (shape: IShapeDto) => {
        const properties: IPublicApiDetailedTripScopeShapesProps & { lng: number; lat: number } = {
            shape_dist_traveled: shape.shape_dist_traveled,
            lng: shape.shape_pt_lon,
            lat: shape.shape_pt_lat,
        };

        return buildGeojsonFeature(properties, "lng", "lat", true);
    };
}
