import { GTFSRouteTypeEnum } from "#helpers/RouteTypeEnums";
import { IPublicApiDetailedTripScopeInfo } from "#og/public/domain/PublicApiDetailedTripInterfaces";
import { RopidRouterUtils } from "#og/shared";
import { IPublicApiCacheDto } from "#sch/vehicle-positions/redis/interfaces/IPublicApiCacheDto";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { GeoCoordinatesType } from "@golemio/core/dist/output-gateway";

export class PublicVPTripInfoTransformation extends AbstractTransformation<IPublicApiCacheDto, IPublicApiDetailedTripScopeInfo> {
    public name = "TripInfoTransformation";

    protected transformInternal = ({ detailed_info, ...element }: IPublicApiCacheDto) => {
        const originTimestamp = new Date(detailed_info.origin_timestamp);

        return {
            gtfs_trip_id: element.gtfs_trip_id,
            route_type: (GTFSRouteTypeEnum[element.route_type] ?? GTFSRouteTypeEnum[1700]).toLowerCase(),
            route_short_name: element.gtfs_route_short_name,
            shape_id: detailed_info.shape_id,
            origin_route_name: detailed_info.origin_route_name,
            run_number: detailed_info.run_number,
            trip_headsign: detailed_info.trip_headsign,
            geometry: {
                type: "Point" as GeoCoordinatesType.Point,
                coordinates: [element.lng, element.lat],
            },
            shape_dist_traveled: detailed_info.shape_dist_traveled,
            bearing: element.bearing,
            delay: element.delay,
            state_position: element.state_position,
            last_stop_sequence: detailed_info.last_stop_sequence,
            origin_timestamp: RopidRouterUtils.formatTimestamp(originTimestamp, "Europe/Prague"),
        };
    };
}
