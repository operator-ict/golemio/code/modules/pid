import { GtfsStopWheelchairBoardingEnum } from "#helpers/AccessibilityEnums";
import { DateTimeUtils } from "#helpers/DateTimeUtils";
import { IPublicApiDetailedTripScopeStopTimesProps } from "#og/public/domain/PublicApiDetailedTripInterfaces";
import { IStop, IStopTime } from "#sch/ropid-gtfs/redis/interfaces/IDelayComputationDto";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { IGeoJSONFeature, buildGeojsonFeature } from "@golemio/core/dist/output-gateway";

export class PublicVPTripStopTimesTransformation extends AbstractTransformation<IStopTime, IGeoJSONFeature> {
    public name = "TripStopTimesTransformation";

    protected transformInternal = ({ stop, ...stopTime }: IStopTime) => {
        const properties: IPublicApiDetailedTripScopeStopTimesProps & { lng: number; lat: number } = {
            stop_name: stop.stop_name,
            stop_sequence: stopTime.stop_sequence,
            zone_id: stop.zone_id,
            is_wheelchair_accessible: this.isStopWheelchairAccessible(stop),
            shape_dist_traveled: stopTime.shape_dist_traveled,
            arrival_time: DateTimeUtils.formatStopTime(DateTimeUtils.getStopDateTimeForDayStart(stopTime.arrival_time_seconds)),
            departure_time: DateTimeUtils.formatStopTime(
                DateTimeUtils.getStopDateTimeForDayStart(stopTime.departure_time_seconds)
            ),
            realtime_arrival_time: null, // later filled by realtime data
            realtime_departure_time: null, // later filled by realtime data
            lng: stop.stop_lon,
            lat: stop.stop_lat,
        };

        return buildGeojsonFeature(properties, "lng", "lat", true);
    };

    private isStopWheelchairAccessible = ({ wheelchair_boarding }: IStop): boolean | null => {
        if (wheelchair_boarding === null || wheelchair_boarding === GtfsStopWheelchairBoardingEnum.NoInformation) {
            return null;
        }

        return wheelchair_boarding === GtfsStopWheelchairBoardingEnum.AccessibleStation;
    };
}
