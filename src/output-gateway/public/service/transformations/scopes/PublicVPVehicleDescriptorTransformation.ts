import { IPublicApiDetailedTripScopeDescriptor } from "#og/public/domain/PublicApiDetailedTripInterfaces";
import { IPublicApiCacheDto } from "#sch/vehicle-positions/redis/interfaces/IPublicApiCacheDto";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";

export class PublicVPVehicleDescriptorTransformation extends AbstractTransformation<
    IPublicApiCacheDto,
    IPublicApiDetailedTripScopeDescriptor
> {
    public name = "VehicleDescriptorTransformation";

    protected transformInternal = ({ detailed_info }: IPublicApiCacheDto) => {
        return {
            operator: detailed_info.operator,
            vehicle_type: null, // later filled from vehicle descriptor cache
            vehicle_registration_number: detailed_info.registration_number ? detailed_info.registration_number.toString() : null,
            is_wheelchair_accessible: detailed_info.is_wheelchair_accessible,
            is_air_conditioned: null, // later filled from vehicle descriptor cache
            has_usb_chargers: null, // later filled from vehicle descriptor cache
        };
    };
}
