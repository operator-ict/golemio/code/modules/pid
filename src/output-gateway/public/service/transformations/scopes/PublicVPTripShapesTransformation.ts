import { IPublicApiDetailedTripScopeShapesProps } from "#og/public/domain/PublicApiDetailedTripInterfaces";
import { IComputationTripShape } from "#sch/ropid-gtfs/redis/interfaces/IDelayComputationDto";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { IGeoJSONFeature, buildGeojsonFeature } from "@golemio/core/dist/output-gateway";

export class PublicVPTripShapesTransformation extends AbstractTransformation<IComputationTripShape, IGeoJSONFeature> {
    public name = "TripShapesTransformation";

    protected transformInternal = (shape: IComputationTripShape) => {
        const properties: IPublicApiDetailedTripScopeShapesProps & { lng: number; lat: number } = {
            shape_dist_traveled: shape.dist,
            lng: shape.coords[0],
            lat: shape.coords[1],
        };

        return buildGeojsonFeature(properties, "lng", "lat", true);
    };
}
