import { GTFSRouteTypeEnum } from "#helpers/RouteTypeEnums";
import { IPublicDepartureTransformInputDto } from "#og/public/domain/IPublicDepartureTransformInputDto";
import { IPublicApiDeparture, IPublicApiDepartureVehicle } from "#og/public/domain/PublicApiDepartureInterfaces";
import { DepartureCalculator } from "#og/shared/DepartureCalculator";
import { RopidRouterUtils } from "#og/shared/RopidRouterUtils";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { StatePositionEnum } from "src/const";

export class PublicDepartureTransformation extends AbstractTransformation<
    IPublicDepartureTransformInputDto,
    IPublicApiDeparture
> {
    public name = "PublicDepartureTransformation";

    protected transformInternal = (inputDto: IPublicDepartureTransformInputDto) => {
        const { departure, vehiclePosition, vehicleDescriptor } = inputDto;

        const delayInSeconds = vehiclePosition?.delay ?? null;
        const departureTime = new Date(departure.departure_datetime);
        const arrivalTime = departure.arrival_datetime ? new Date(departure.arrival_datetime) : null;
        const predictedDepartureTime = DepartureCalculator.getPredictedDepartureTime(departureTime, arrivalTime, delayInSeconds);

        let vehicle: IPublicApiDepartureVehicle;

        if (vehiclePosition?.state_position === StatePositionEnum.CANCELED || !vehiclePosition) {
            vehicle = {
                id: null,
                is_wheelchair_accessible: null,
                is_air_conditioned: null,
                has_charger: null,
            };
        } else {
            vehicle = {
                id: vehiclePosition.vehicle_id ?? null,
                is_wheelchair_accessible: vehiclePosition.detailed_info.is_wheelchair_accessible ?? null,
                is_air_conditioned: vehicleDescriptor?.is_air_conditioned ?? null,
                has_charger: vehicleDescriptor?.has_usb_chargers ?? null,
            };
        }

        return {
            departure: {
                timestamp_scheduled: RopidRouterUtils.formatTimestamp(departureTime) ?? departure.departure_datetime,
                timestamp_predicted: RopidRouterUtils.formatTimestamp(predictedDepartureTime) ?? departure.departure_datetime,
                delay_seconds: delayInSeconds,
                minutes: DepartureCalculator.getDepartureMinutes(predictedDepartureTime),
            },
            stop: {
                id: departure.stop_id,
                sequence: departure.stop_sequence,
                platform_code: this.determinePlatformCode(inputDto),
            },
            route: {
                type: GTFSRouteTypeEnum[departure.route_type].toLowerCase(),
                short_name: departure.route_short_name,
            },
            trip: {
                id: departure.trip_id,
                headsign: departure.trip_headsign,
                is_canceled: vehiclePosition?.state_position === StatePositionEnum.CANCELED,
            },
            vehicle,
        };
    };

    /**
     * Determine platform code based on the following rules:
     *   if current stop time is known, use its RT CIS platform code (if available)
     *   otherwise use GTFS schedule platform code
     */
    private determinePlatformCode = (inputDto: IPublicDepartureTransformInputDto): string | null => {
        const { departure, stopTime } = inputDto;

        if (stopTime) {
            return stopTime.cis_stop_platform_code;
        }

        return departure.platform_code;
    };
}
