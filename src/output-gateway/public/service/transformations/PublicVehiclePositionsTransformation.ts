import { GTFSRouteTypeEnum } from "#helpers/RouteTypeEnums";
import { IPublicApiCacheDto } from "#sch/vehicle-positions/redis/interfaces/IPublicApiCacheDto";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { IGeoJSONFeature, buildGeojsonFeature } from "@golemio/core/dist/output-gateway/Geo";

export class PublicVehiclePositionsTransformation extends AbstractTransformation<IPublicApiCacheDto, IGeoJSONFeature> {
    public name = "OutputDtoTransformation";

    protected transformInternal = ({ detailed_info, ...element }: IPublicApiCacheDto) => {
        return buildGeojsonFeature(
            {
                ...element,
                route_type: (GTFSRouteTypeEnum[element.route_type] ?? GTFSRouteTypeEnum[1700]).toLowerCase(),
            },
            "lng",
            "lat",
            true
        );
    };
}
