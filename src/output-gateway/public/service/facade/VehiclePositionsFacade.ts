import { GTFSRouteTypeEnum } from "#helpers/RouteTypeEnums";
import { IBoundingBox } from "#og/public/domain/IBoudingBox";
import { IVehiclePositionsRepository } from "#og/public/domain/repository/IVehiclePositionsRepository";
import { IPublicApiCacheDto } from "#sch/vehicle-positions/redis/interfaces/IPublicApiCacheDto";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { IGeoJSONFeatureCollection, buildGeojsonFeatureCollection } from "@golemio/core/dist/output-gateway";
import { IBoundingBoxHelper } from "../helpers/interfaces/IBoundingBoxHelper";
import { PublicVehiclePositionsTransformation } from "../transformations/PublicVehiclePositionsTransformation";

export class PublicVehiclePositionsFacade {
    private transformation: PublicVehiclePositionsTransformation;

    constructor(
        private repository: IVehiclePositionsRepository,
        private boundingBoxHelper: IBoundingBoxHelper,
        private log: ILogger
    ) {
        this.transformation = new PublicVehiclePositionsTransformation();
    }

    public async getAll(
        boundingBox: IBoundingBox,
        routeShortName: string | string[] | undefined,
        type: GTFSRouteTypeEnum[] | undefined
    ): Promise<IGeoJSONFeatureCollection> {
        const boundingBoxWithCenter = this.boundingBoxHelper.mapToCenterFormat(boundingBox);
        this.log.debug(`width: ${boundingBoxWithCenter.width}m, height: ${boundingBoxWithCenter.height}m`);

        const vehicleIds = await this.repository.getAllVehicleIds(boundingBoxWithCenter);
        if (vehicleIds.length === 0) {
            this.log.debug("No vehicle ids found.");
            return buildGeojsonFeatureCollection([]);
        }

        const vehiclePositions = await this.repository.getAllVehiclePositions(vehicleIds);
        const filteredResults = vehiclePositions.filter((el) => this.filterClientSide(el, routeShortName, type));
        if (filteredResults.length === 0) {
            this.log.debug("No vehicle positions found.");
            return buildGeojsonFeatureCollection([]);
        }

        return buildGeojsonFeatureCollection(this.transformation.transformArray(filteredResults));
    }

    private filterClientSide(
        el: IPublicApiCacheDto,
        routeShortName: string | string[] | undefined,
        types: GTFSRouteTypeEnum[] | undefined
    ): boolean {
        if (routeShortName instanceof Array && routeShortName.includes(el.gtfs_route_short_name)) {
            return true;
        }

        if (routeShortName && el.gtfs_route_short_name !== routeShortName) {
            return false;
        }

        if (types && types.every((type) => type !== el.route_type)) {
            return false;
        }

        return true;
    }
}
