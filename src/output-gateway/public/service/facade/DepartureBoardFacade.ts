import { GTFSRouteTypeEnum } from "#helpers/RouteTypeEnums";
import { IDepartureBoardsStopIdGroups } from "#og/public/controllers/v2/interfaces/PublicParamsInterfaces";
import { VehicleDescriptorCachedRepository } from "#og/public/data-access/VehicleDescriptorCachedRepository";
import { PublicApiDepartureBoardGroup } from "#og/public/domain/PublicApiDepartureInterfaces";
import { IGtfsDepartureRepository } from "#og/public/domain/repository/IGtfsDepartureRepository";
import { IStopTimeRepository } from "#og/public/domain/repository/IStopTimeRepository";
import { IVehiclePositionsRepository } from "#og/public/domain/repository/IVehiclePositionsRepository";
import { IPublicGtfsDepartureCacheDto } from "#sch/ropid-gtfs/redis/interfaces/IPublicGtfsDepartureCacheDto";
import { IPublicApiCacheDto } from "#sch/vehicle-positions/redis/interfaces/IPublicApiCacheDto";
import { IPublicStopTimeCacheDto } from "#sch/vehicle-positions/redis/interfaces/IPublicStopTimeCacheDto";
import { createChildSpan } from "@golemio/core/dist/monitoring/opentelemetry/trace-provider";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { DepartureGroupRefiner } from "../helpers/DepartureGroupRefiner";
import { PublicDepartureTransformation } from "../transformations/PublicDepartureTransformation";

export class DepartureBoardFacade {
    private readonly transformation: PublicDepartureTransformation;

    constructor(
        private readonly departureRepository: IGtfsDepartureRepository,
        private readonly tripRepository: IVehiclePositionsRepository,
        private readonly stopTimeRepository: IStopTimeRepository,
        private readonly descriptorRepository: VehicleDescriptorCachedRepository
    ) {
        this.transformation = new PublicDepartureTransformation();
    }

    public async getAll(
        stopIds: Set<IDepartureBoardsStopIdGroups>,
        limit: number,
        routeShortNames: string[] | null,
        minutesAfter: number
    ): Promise<PublicApiDepartureBoardGroup[] | null> {
        let output: Array<PublicApiDepartureBoardGroup | null>;

        const sortedStopGroups = this.sortStopGroupsByPriority(stopIds);
        const promises = sortedStopGroups.map(
            async (stopGroup) => await this.handleStopGroupDepartures(stopGroup, limit, routeShortNames, minutesAfter)
        );

        try {
            output = await Promise.all(promises);
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            }

            throw new GeneralError("Error while retrieving cached data", this.constructor.name, err, 500);
        }
        if (output.every((value) => value === null)) {
            return null;
        } else {
            return output.map((value) => {
                if (value === null) {
                    return [];
                } else {
                    return value;
                }
            });
        }
    }

    private async handleStopGroupDepartures(
        stopGroup: IDepartureBoardsStopIdGroups,
        limit: number,
        routeShortNames: string[] | null,
        minutesAfter: number
    ): Promise<PublicApiDepartureBoardGroup | null> {
        let outputGroup: PublicApiDepartureBoardGroup = [];

        const spanDepartures = createChildSpan(`Departures.${stopGroup.priority}.getPublicDepartureCache`);
        const departures = await this.departureRepository.getPublicGtfsDepartureCache(stopGroup.stopIds, minutesAfter);
        spanDepartures?.end();
        if (departures.length === 0) {
            return null;
        }
        const spanPositions = createChildSpan(`Departures.${stopGroup.priority}.getAllVehiclePositions`);
        const positionsByTrip = await this.tripRepository.getAllVehiclePositionsForMultipleTrips(
            departures.map((departure) => departure.trip_id)
        );
        spanPositions?.end();

        const spanTransform = createChildSpan(`Departures.${stopGroup.priority}.transformDepartures`);
        try {
            for (const departure of departures) {
                if (Object.keys(departure).length === 0) {
                    continue;
                }
                const positions = positionsByTrip.get(departure.trip_id) ?? [];
                const duplicatedDepartures = await this.duplicateDeparturesWithDifferentVehicleIds(departure, positions);
                outputGroup = outputGroup.concat(duplicatedDepartures);
            }
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            }

            throw new GeneralError("Error while retrieving cached data", this.constructor.name, err, 500);
        } finally {
            spanTransform?.end();
        }

        const spanManipulate = createChildSpan(`Departures.${stopGroup.priority}.manipulateDepartures`);
        try {
            outputGroup = DepartureGroupRefiner.filterDeparturesByRouteShortNames(outputGroup, routeShortNames);
            outputGroup = DepartureGroupRefiner.sortDeparturesByPredictedTimestamp(outputGroup);
            // minutesBefore is set to 0 because we want to display departures that are already happening
            // this may change in the future
            outputGroup = DepartureGroupRefiner.removeDeparturesOutsideTimeRange(outputGroup, 0, minutesAfter).slice(0, limit);
        } catch (err) {
            throw new GeneralError("Error while manipulating departures", this.constructor.name, err, 500);
        } finally {
            spanManipulate?.end();
        }

        return outputGroup;
    }

    private async duplicateDeparturesWithDifferentVehicleIds(
        departure: IPublicGtfsDepartureCacheDto,
        vehiclePositions: IPublicApiCacheDto[]
    ): Promise<PublicApiDepartureBoardGroup> {
        const outputGroup: PublicApiDepartureBoardGroup = [];

        // If there are no RT data, we still want to display the departure
        // but without any RT/vehicle information
        if (vehiclePositions.length === 0) {
            outputGroup.push(
                this.transformation.transformElement({
                    departure,
                    vehiclePosition: null,
                    vehicleDescriptor: null,
                    stopTime: null,
                })
            );

            return outputGroup;
        }

        // Otherwise, we want to duplicate the departure for each vehicleId
        // (multiple vehicles can be on the same trip at the same time)
        for (const vehiclePosition of vehiclePositions) {
            const vehicleDescriptor = vehiclePosition?.detailed_info.registration_number
                ? await this.descriptorRepository.getOneByRegNumber(
                      departure.route_type,
                      vehiclePosition?.detailed_info.registration_number
                  )
                : null;

            let currentStopTime: IPublicStopTimeCacheDto | null = null;

            // If the vehicle is a train, we need to get the current stop time
            //   to later determine the RT CIS platform code (trains don't have platform codes in GTFS)
            if (vehiclePosition.route_type === GTFSRouteTypeEnum.TRAIN) {
                const stopTimes = await this.stopTimeRepository.getPublicStopTimeCache(
                    vehiclePosition.vehicle_id,
                    departure.trip_id
                );

                currentStopTime = stopTimes.find((stopTime) => stopTime.sequence === departure.stop_sequence) ?? null;
            }

            outputGroup.push(
                this.transformation.transformElement({
                    departure,
                    vehiclePosition,
                    vehicleDescriptor,
                    stopTime: currentStopTime,
                })
            );
        }

        return outputGroup;
    }

    private sortStopGroupsByPriority(stopIds: Set<IDepartureBoardsStopIdGroups>): IDepartureBoardsStopIdGroups[] {
        return Array.from(stopIds).sort((a, b) => a.priority - b.priority);
    }
}
