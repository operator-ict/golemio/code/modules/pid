import { IPublicApiDetailedTrip } from "#og/public/domain/PublicApiDetailedTripInterfaces";
import { IVehiclePositionsRepository } from "#og/public/domain/repository/IVehiclePositionsRepository";
import { DetailedTripScope } from "#og/public/routers/v2/helpers/DetailedTripScopeEnum";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { TripScopeHandlerFactory } from "../helpers/trip-scope/TripScopeHandlerFactory";

export class DetailedTripFacade {
    constructor(
        private scopeHandlerFactory: TripScopeHandlerFactory,
        private vehiclePositionRepository: IVehiclePositionsRepository
    ) {}

    public async getOneByVehicleId(
        vehicleId: string,
        scopes: DetailedTripScope[],
        tripId?: string
    ): Promise<IPublicApiDetailedTrip> {
        let vehiclePosition = await this.vehiclePositionRepository.getDetailedVehiclePosition(vehicleId);

        if (tripId && vehiclePosition?.gtfs_trip_id !== tripId) {
            vehiclePosition = await this.vehiclePositionRepository.getDetailedVehiclePosition(vehicleId, tripId);
        }

        if (!vehiclePosition) {
            throw new GeneralError("not_found", this.constructor.name, undefined, 404);
        }

        let output: IPublicApiDetailedTrip = {};
        if (scopes.includes(DetailedTripScope.Info)) {
            output = await this.scopeHandlerFactory.getStrategy(DetailedTripScope.Info).handle(output, vehiclePosition);
        }

        const promises = scopes.map(async (scope) => {
            if (scope === DetailedTripScope.Info) {
                return;
            }

            output = await this.scopeHandlerFactory.getStrategy(scope).handle(output, vehiclePosition!);
        });

        await Promise.all(promises);
        return output;
    }
}
