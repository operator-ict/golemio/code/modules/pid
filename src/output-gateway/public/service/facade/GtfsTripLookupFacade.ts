import { IPublicApiGtfsTrip } from "#og/public/domain/PublicApiGtfsTripLookupInterfaces";
import { DetailedTripScope } from "#og/public/routers/v2/helpers/DetailedTripScopeEnum";
import { models } from "#og/ropid-gtfs/models";
import { GTFSTripsModel } from "#og/ropid-gtfs/models/GTFSTripsModel";
import {
    ITripWithDefinedRouteDto,
    ITripWithOptionalAssociationsDto,
} from "#sch/ropid-gtfs/interfaces/ITripWithOptionalAssociationsDto";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { GtfsTripScopeHandlerFactory } from "../helpers/gtfs-trip-scope/GtfsTripScopeHandlerFactory";

export class GtfsTripLookupFacade {
    private readonly gtfsTripRepository: GTFSTripsModel;

    constructor(private scopeHandlerFactory: GtfsTripScopeHandlerFactory) {
        this.gtfsTripRepository = models.GTFSTripsModel;
    }

    public async getOneByGtfsTripId(gtfsTripId: string, scopes: DetailedTripScope[]): Promise<IPublicApiGtfsTrip> {
        const shouldIncludeStopTimes = scopes.includes(DetailedTripScope.StopTimes);
        const shouldIncludeShapes = scopes.includes(DetailedTripScope.Shapes);

        const gtfsTrip = await this.gtfsTripRepository.getOneForPublicGtfsLookup(gtfsTripId, {
            shouldIncludeStopTimes,
            shouldIncludeShapes,
        });

        if (!this.isGtfsTripValid(gtfsTrip)) {
            throw new GeneralError("GTFS trip info not found", this.constructor.name, undefined, 404);
        }

        let output: IPublicApiGtfsTrip = {};
        if (scopes.includes(DetailedTripScope.Info)) {
            output = this.scopeHandlerFactory.getStrategy(DetailedTripScope.Info).handle(output, gtfsTrip);
        }

        for (const scope of scopes) {
            if (scope === DetailedTripScope.Info) {
                continue;
            }

            output = this.scopeHandlerFactory.getStrategy(scope).handle(output, gtfsTrip);
        }

        return output;
    }

    private isGtfsTripValid(gtfsTrip: ITripWithOptionalAssociationsDto | null): gtfsTrip is ITripWithDefinedRouteDto {
        return !!gtfsTrip && !!gtfsTrip.route;
    }
}
