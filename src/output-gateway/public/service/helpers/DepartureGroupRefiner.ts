import { PublicApiDepartureBoardGroup } from "#og/public/domain/PublicApiDepartureInterfaces";

export class DepartureGroupRefiner {
    public static filterDeparturesByRouteShortNames(
        departureGroup: PublicApiDepartureBoardGroup,
        routeShortNames: string[] | null
    ): PublicApiDepartureBoardGroup {
        if (!routeShortNames) {
            return departureGroup;
        }

        return departureGroup.filter((departure) => routeShortNames.includes(departure.route.short_name));
    }

    public static sortDeparturesByPredictedTimestamp(departureGroup: PublicApiDepartureBoardGroup): PublicApiDepartureBoardGroup {
        return departureGroup.sort(
            (a, b) => new Date(a.departure.timestamp_predicted).getTime() - new Date(b.departure.timestamp_predicted).getTime()
        );
    }

    public static removeDeparturesOutsideTimeRange(
        departureGroup: PublicApiDepartureBoardGroup,
        minutesBefore: number,
        minutesAfter: number
    ): PublicApiDepartureBoardGroup {
        const now = new Date();
        const timeRangeStart = new Date(now.getTime() - minutesBefore * 60 * 1000);
        const timeRangeEnd = new Date(now.getTime() + minutesAfter * 60 * 1000);

        departureGroup = departureGroup.filter(
            (departure) =>
                new Date(departure.departure.timestamp_predicted) >= timeRangeStart &&
                new Date(departure.departure.timestamp_predicted) <= timeRangeEnd
        );

        return departureGroup;
    }
}
