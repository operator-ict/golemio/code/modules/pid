import { VehicleDescriptorCachedRepository } from "#og/public/data-access/VehicleDescriptorCachedRepository";
import { IPublicApiDetailedTrip } from "#og/public/domain/PublicApiDetailedTripInterfaces";
import { OgModuleToken } from "#og/public/ioc/OgModuleToken";
import { PublicVPVehicleDescriptorTransformation } from "#og/public/service/transformations/scopes/PublicVPVehicleDescriptorTransformation";
import { IPublicApiCacheDto } from "#sch/vehicle-positions/redis/interfaces/IPublicApiCacheDto";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { AbstractDetailedTripScopeHandler } from "./AbstractDetailedTripScopeHandler";

@injectable()
export class VehicleDescriptorTripScopeHandler extends AbstractDetailedTripScopeHandler {
    private vehicleDescriptorTransformation: PublicVPVehicleDescriptorTransformation;

    constructor(
        @inject(OgModuleToken.VehicleDescriptorCachedRepository)
        private vehicleDescriptorRepository: VehicleDescriptorCachedRepository
    ) {
        super();
        this.vehicleDescriptorTransformation = new PublicVPVehicleDescriptorTransformation();
    }

    public async handle(output: IPublicApiDetailedTrip, vehiclePosition: IPublicApiCacheDto): Promise<IPublicApiDetailedTrip> {
        output.vehicle_descriptor = this.vehicleDescriptorTransformation.transformElement(vehiclePosition);

        const registrationNumber = vehiclePosition.detailed_info.registration_number;
        if (registrationNumber) {
            const routeType = vehiclePosition.route_type;
            const descriptor = await this.vehicleDescriptorRepository.getOneByRegNumber(routeType, registrationNumber);

            if (!descriptor) {
                return output;
            }

            output.vehicle_descriptor = {
                ...output.vehicle_descriptor,
                vehicle_type: `${descriptor.manufacturer} ${descriptor.type}`,
                is_air_conditioned: descriptor.is_air_conditioned,
                has_usb_chargers: descriptor.has_usb_chargers,
            };
        }

        return output;
    }
}
