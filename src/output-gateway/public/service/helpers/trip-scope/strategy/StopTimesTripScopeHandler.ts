import {
    IPublicApiDetailedTrip,
    IPublicApiDetailedTripScopeStopTimesProps,
} from "#og/public/domain/PublicApiDetailedTripInterfaces";
import { IDelayComputationRepository } from "#og/public/domain/repository/IDelayComputationRepository";
import { IStopTimeRepository } from "#og/public/domain/repository/IStopTimeRepository";
import { OgModuleToken } from "#og/public/ioc/OgModuleToken";
import { PublicVPTripStopTimesTransformation } from "#og/public/service/transformations/scopes/PublicVPTripStopTimesTransformation";
import { IStopTime } from "#sch/ropid-gtfs/redis/interfaces/IDelayComputationDto";
import { IPublicApiCacheDto } from "#sch/vehicle-positions/redis/interfaces/IPublicApiCacheDto";
import { buildGeojsonFeatureCollection } from "@golemio/core/dist/output-gateway/Geo";
import { Feature, Point } from "@golemio/core/dist/shared/geojson";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { DateTime } from "@golemio/core/dist/shared/luxon";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { AbstractDetailedTripScopeHandler } from "./AbstractDetailedTripScopeHandler";

@injectable()
export class StopTimesTripScopeHandler extends AbstractDetailedTripScopeHandler {
    private tripStopTimesTransformation: PublicVPTripStopTimesTransformation;

    constructor(
        @inject(OgModuleToken.DelayComputationRepository) private delayComputationRepository: IDelayComputationRepository,
        @inject(OgModuleToken.PublicStopTimeRepository) private stopTimeRepository: IStopTimeRepository
    ) {
        super();
        this.tripStopTimesTransformation = new PublicVPTripStopTimesTransformation();
    }

    public async handle(output: IPublicApiDetailedTrip, vehiclePosition: IPublicApiCacheDto): Promise<IPublicApiDetailedTrip> {
        const delayComputationCache = await this.delayComputationRepository.getDelayComputationCache(
            vehiclePosition.gtfs_trip_id
        );

        if (!delayComputationCache) {
            throw new GeneralError("delay_computation_cache_not_found", this.constructor.name, undefined, 500);
        }

        const filteredStopTimes = this.filterNoStopWaypoints(delayComputationCache.stop_times);
        const stopTimes = this.tripStopTimesTransformation.transformArray(filteredStopTimes) as Array<
            Feature<Point, IPublicApiDetailedTripScopeStopTimesProps>
        >;

        const stopTimesWithDelay = await this.stopTimeRepository.getPublicStopTimeCache(
            vehiclePosition.vehicle_id,
            vehiclePosition.gtfs_trip_id
        );
        if (stopTimesWithDelay.length > 0) {
            let prevStopArrivalTime: null | DateTime = null;
            let prevStopDepartureTime: null | DateTime = null;

            for (const stopTimeWithDelay of stopTimesWithDelay) {
                // note: We can improve performance here if needed
                const stopTime = stopTimes.find(
                    (currentStopTime) => currentStopTime.properties.stop_sequence === stopTimeWithDelay.sequence
                );
                if (stopTime) {
                    // note: optimization point here in case of worsening performance on output API (additional cache)
                    const rtArrivalTime =
                        stopTimeWithDelay.arr_delay === null
                            ? null
                            : DateTime.fromFormat(stopTime.properties.arrival_time, "HH:mm:ss").plus({
                                  seconds: stopTimeWithDelay.arr_delay,
                              });

                    let rtDepartureTime =
                        stopTimeWithDelay.dep_delay === null
                            ? null
                            : DateTime.fromFormat(stopTime.properties.departure_time, "HH:mm:ss").plus({
                                  seconds: stopTimeWithDelay.dep_delay,
                              });
                    // note: for better performance, we should address this on the DB side in the future
                    const { currentArrival, currentDeparture } = this.checkStopTimesSequence(
                        rtArrivalTime!,
                        prevStopArrivalTime!,
                        rtDepartureTime!,
                        prevStopDepartureTime!
                    );
                    prevStopArrivalTime = currentArrival;
                    prevStopDepartureTime = currentDeparture;

                    stopTime.properties.realtime_arrival_time = currentArrival ? currentArrival.toFormat("HH:mm:ss") : null;
                    stopTime.properties.realtime_departure_time = currentDeparture ? currentDeparture.toFormat("HH:mm:ss") : null;
                }
            }
        }

        output.stop_times = buildGeojsonFeatureCollection(stopTimes);
        return output;
    }

    private filterNoStopWaypoints(stopTimes: IStopTime[]): IStopTime[] {
        return stopTimes.filter((stopTime: IStopTime) => !stopTime.is_no_stop_waypoint);
    }

    private checkStopTimesSequence(
        rtArrival: DateTime,
        prevArrival: DateTime,
        rtDeparture: DateTime,
        prevDeparture: DateTime
    ): { currentArrival: DateTime | null; currentDeparture: DateTime | null } {
        const returnValue: { currentArrival: DateTime | null; currentDeparture: DateTime | null } = {
            currentArrival: rtArrival,
            currentDeparture: rtDeparture,
        };
        if (rtArrival > rtDeparture || prevDeparture > rtDeparture || prevArrival > rtDeparture) {
            returnValue.currentDeparture = null;
        }
        if (prevDeparture > rtArrival || prevArrival > rtArrival) {
            returnValue.currentArrival = null;
        }
        return returnValue;
    }
}
