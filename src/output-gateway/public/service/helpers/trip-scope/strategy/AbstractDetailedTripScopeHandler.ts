import { ITripScopeAsyncHandler } from "#og/public/domain/ITripScopeHandler";
import { IPublicApiDetailedTrip } from "#og/public/domain/PublicApiDetailedTripInterfaces";
import { IPublicApiCacheDto } from "#sch/vehicle-positions/redis/interfaces/IPublicApiCacheDto";

export abstract class AbstractDetailedTripScopeHandler
    implements ITripScopeAsyncHandler<IPublicApiDetailedTrip, IPublicApiCacheDto>
{
    abstract handle(output: IPublicApiDetailedTrip, vehiclePosition: IPublicApiCacheDto): Promise<IPublicApiDetailedTrip>;
}
