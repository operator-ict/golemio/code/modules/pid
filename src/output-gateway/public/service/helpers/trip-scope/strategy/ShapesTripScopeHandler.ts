import { IPublicApiDetailedTrip } from "#og/public/domain/PublicApiDetailedTripInterfaces";
import { IDelayComputationRepository } from "#og/public/domain/repository/IDelayComputationRepository";
import { OgModuleToken } from "#og/public/ioc/OgModuleToken";
import { PublicVPTripShapesTransformation } from "#og/public/service/transformations/scopes/PublicVPTripShapesTransformation";
import { models } from "#og/ropid-gtfs/models";
import { GTFSShapesModel } from "#og/ropid-gtfs/models/GTFSShapesModel";
import { IPublicApiCacheDto } from "#sch/vehicle-positions/redis/interfaces/IPublicApiCacheDto";
import { buildGeojsonFeatureCollection } from "@golemio/core/dist/output-gateway/Geo";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { AbstractDetailedTripScopeHandler } from "./AbstractDetailedTripScopeHandler";

@injectable()
export class ShapesTripScopeHandler extends AbstractDetailedTripScopeHandler {
    private tripShapesTransformation: PublicVPTripShapesTransformation;
    protected shapeModel: GTFSShapesModel;

    constructor(
        @inject(OgModuleToken.DelayComputationRepository) private delayComputationRepository: IDelayComputationRepository
    ) {
        super();
        this.tripShapesTransformation = new PublicVPTripShapesTransformation();
        this.shapeModel = models.GTFSShapesModel;
    }

    public async handle(output: IPublicApiDetailedTrip, vehiclePosition: IPublicApiCacheDto): Promise<IPublicApiDetailedTrip> {
        const delayComputationCache = await this.delayComputationRepository.getDelayComputationCache(
            vehiclePosition.gtfs_trip_id
        );

        if (!delayComputationCache) {
            throw new GeneralError("delay_computation_cache_not_found", this.constructor.name, undefined, 500);
        }

        output.shapes = buildGeojsonFeatureCollection(
            delayComputationCache.shapes ? this.tripShapesTransformation.transformArray(delayComputationCache.shapes) : []
        );

        return output;
    }
}
