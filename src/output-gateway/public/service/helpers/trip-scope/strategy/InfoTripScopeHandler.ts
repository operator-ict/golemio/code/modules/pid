import { IPublicApiDetailedTrip } from "#og/public/domain/PublicApiDetailedTripInterfaces";
import { PublicVPTripInfoTransformation } from "#og/public/service/transformations/scopes/PublicVPTripInfoTransformation";
import { IPublicApiCacheDto } from "#sch/vehicle-positions/redis/interfaces/IPublicApiCacheDto";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { AbstractDetailedTripScopeHandler } from "./AbstractDetailedTripScopeHandler";

@injectable()
export class InfoTripScopeHandler extends AbstractDetailedTripScopeHandler {
    private tripInfoTransformation: PublicVPTripInfoTransformation;

    constructor() {
        super();
        this.tripInfoTransformation = new PublicVPTripInfoTransformation();
    }

    public async handle(output: IPublicApiDetailedTrip, vehiclePosition: IPublicApiCacheDto): Promise<IPublicApiDetailedTrip> {
        output = this.tripInfoTransformation.transformElement(vehiclePosition);
        return output;
    }
}
