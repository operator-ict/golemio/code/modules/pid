import { OgModuleToken } from "#og/public/ioc/OgModuleToken";
import { DetailedTripScope } from "#og/public/routers/v2/helpers/DetailedTripScopeEnum";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { AbstractDetailedTripScopeHandler } from "./strategy/AbstractDetailedTripScopeHandler";

@injectable()
export class TripScopeHandlerFactory {
    private dictionary: Map<DetailedTripScope, AbstractDetailedTripScopeHandler>;

    constructor(
        @inject(OgModuleToken.InfoTripScopeHandler) infoTripScopeHandler: AbstractDetailedTripScopeHandler,
        @inject(OgModuleToken.StopTimesTripScopeHandler)
        stopTimesTripScopeHandler: AbstractDetailedTripScopeHandler,
        @inject(OgModuleToken.ShapesTripScopeHandler) shapeTripScopeHandler: AbstractDetailedTripScopeHandler,
        @inject(OgModuleToken.VehicleDescriptorTripScopeHandler)
        vehicleDescriptorTripScopeHandler: AbstractDetailedTripScopeHandler
    ) {
        this.dictionary = new Map<DetailedTripScope, AbstractDetailedTripScopeHandler>();
        this.dictionary.set(DetailedTripScope.Info, infoTripScopeHandler);
        this.dictionary.set(DetailedTripScope.StopTimes, stopTimesTripScopeHandler);
        this.dictionary.set(DetailedTripScope.Shapes, shapeTripScopeHandler);
        this.dictionary.set(DetailedTripScope.VehicleDescriptor, vehicleDescriptorTripScopeHandler);
    }

    public getStrategy(scope: DetailedTripScope): AbstractDetailedTripScopeHandler {
        if (this.dictionary.has(scope)) {
            return this.dictionary.get(scope)!;
        }

        throw new GeneralError("Unknown detailed trip scope strategy", this.constructor.name, undefined, 500);
    }
}
