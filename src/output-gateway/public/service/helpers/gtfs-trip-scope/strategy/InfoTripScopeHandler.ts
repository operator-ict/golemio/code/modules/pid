import { IPublicApiGtfsTrip } from "#og/public/domain/PublicApiGtfsTripLookupInterfaces";
import { OgModuleToken } from "#og/public/ioc/OgModuleToken";
import { PublicGtfsTripInfoTransformation } from "#og/public/service/transformations/gtfs-trip-scopes/PublicGtfsTripInfoTransformation";
import { ITripWithDefinedRouteDto } from "#sch/ropid-gtfs/interfaces/ITripWithOptionalAssociationsDto";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { AbstractGtfsTripScopeHandler } from "./AbstractGtfsTripScopeHandler";

@injectable()
export class InfoGtfsTripScopeHandler extends AbstractGtfsTripScopeHandler {
    constructor(
        @inject(OgModuleToken.PublicGtfsTripInfoTransformation) private tripInfoTransformation: PublicGtfsTripInfoTransformation
    ) {
        super();
    }

    public handle(output: IPublicApiGtfsTrip, gtfsTrip: ITripWithDefinedRouteDto): IPublicApiGtfsTrip {
        output = this.tripInfoTransformation.transformElement(gtfsTrip);
        return output;
    }
}
