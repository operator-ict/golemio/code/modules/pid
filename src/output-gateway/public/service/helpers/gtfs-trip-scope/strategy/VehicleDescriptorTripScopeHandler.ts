import { IPublicApiGtfsTrip } from "#og/public/domain/PublicApiGtfsTripLookupInterfaces";
import { OgModuleToken } from "#og/public/ioc/OgModuleToken";
import { PublicGtfsVehicleDescriptorTransformation } from "#og/public/service/transformations/gtfs-trip-scopes/PublicGtfsVehicleDescriptorTransformation";
import { ITripWithDefinedRouteDto } from "#sch/ropid-gtfs/interfaces/ITripWithOptionalAssociationsDto";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { AbstractGtfsTripScopeHandler } from "./AbstractGtfsTripScopeHandler";

@injectable()
export class VehicleDescriptorGtfsTripScopeHandler extends AbstractGtfsTripScopeHandler {
    constructor(
        @inject(OgModuleToken.PublicGtfsVehicleDescriptorTransformation)
        private vehicleDescriptorTransformation: PublicGtfsVehicleDescriptorTransformation
    ) {
        super();
    }

    public handle(output: IPublicApiGtfsTrip, gtfsTrip: ITripWithDefinedRouteDto): IPublicApiGtfsTrip {
        output.vehicle_descriptor = this.vehicleDescriptorTransformation.transformElement(gtfsTrip);
        return output;
    }
}
