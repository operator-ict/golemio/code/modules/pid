import { IPublicApiGtfsTrip } from "#og/public/domain/PublicApiGtfsTripLookupInterfaces";
import { OgModuleToken } from "#og/public/ioc/OgModuleToken";
import { PublicGtfsTripStopTimesTransformation } from "#og/public/service/transformations/gtfs-trip-scopes/PublicTripStopTimesTransformation";
import { ITripWithDefinedRouteDto } from "#sch/ropid-gtfs/interfaces/ITripWithOptionalAssociationsDto";
import { buildGeojsonFeatureCollection } from "@golemio/core/dist/output-gateway/Geo";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { AbstractGtfsTripScopeHandler } from "./AbstractGtfsTripScopeHandler";

@injectable()
export class StopTimesGtfsTripScopeHandler extends AbstractGtfsTripScopeHandler {
    constructor(
        @inject(OgModuleToken.PublicGtfsTripStopTimesTransformation)
        private tripStopTimesTransformation: PublicGtfsTripStopTimesTransformation
    ) {
        super();
    }

    public handle(output: IPublicApiGtfsTrip, gtfsTrip: ITripWithDefinedRouteDto): IPublicApiGtfsTrip {
        const stopTimes = this.tripStopTimesTransformation.transformArray(gtfsTrip.stop_times ?? []);

        output.stop_times = buildGeojsonFeatureCollection(stopTimes);
        return output;
    }
}
