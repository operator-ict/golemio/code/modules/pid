import { IPublicApiGtfsTrip } from "#og/public/domain/PublicApiGtfsTripLookupInterfaces";
import { OgModuleToken } from "#og/public/ioc/OgModuleToken";
import { PublicGtfsTripShapesTransformation } from "#og/public/service/transformations/gtfs-trip-scopes/PublicGtfsTripShapesTransformation";
import { ITripWithDefinedRouteDto } from "#sch/ropid-gtfs/interfaces/ITripWithOptionalAssociationsDto";
import { buildGeojsonFeatureCollection } from "@golemio/core/dist/output-gateway/Geo";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { AbstractGtfsTripScopeHandler } from "./AbstractGtfsTripScopeHandler";

@injectable()
export class ShapesGtfsTripScopeHandler extends AbstractGtfsTripScopeHandler {
    constructor(
        @inject(OgModuleToken.PublicGtfsTripShapesTransformation)
        private tripShapesTransformation: PublicGtfsTripShapesTransformation
    ) {
        super();
    }

    public handle(output: IPublicApiGtfsTrip, gtfsTrip: ITripWithDefinedRouteDto): IPublicApiGtfsTrip {
        output.shapes = buildGeojsonFeatureCollection(
            gtfsTrip.shapes ? this.tripShapesTransformation.transformArray(gtfsTrip.shapes) : []
        );

        return output;
    }
}
