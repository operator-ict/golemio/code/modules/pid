import { ITripScopeHandler } from "#og/public/domain/ITripScopeHandler";
import { IPublicApiGtfsTrip } from "#og/public/domain/PublicApiGtfsTripLookupInterfaces";
import { ITripWithDefinedRouteDto } from "#sch/ropid-gtfs/interfaces/ITripWithOptionalAssociationsDto";

export abstract class AbstractGtfsTripScopeHandler implements ITripScopeHandler<IPublicApiGtfsTrip, ITripWithDefinedRouteDto> {
    abstract handle(output: IPublicApiGtfsTrip, gtfsTrip: ITripWithDefinedRouteDto): IPublicApiGtfsTrip;
}
