import { ITripScopeHandler } from "#og/public/domain/ITripScopeHandler";
import { IPublicApiGtfsTrip } from "#og/public/domain/PublicApiGtfsTripLookupInterfaces";
import { OgModuleToken } from "#og/public/ioc/OgModuleToken";
import { DetailedTripScope } from "#og/public/routers/v2/helpers/DetailedTripScopeEnum";
import { ITripWithOptionalAssociationsDto } from "#sch/ropid-gtfs/interfaces/ITripWithOptionalAssociationsDto";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

type GtfsTripScopeHandler = ITripScopeHandler<IPublicApiGtfsTrip, ITripWithOptionalAssociationsDto>;

@injectable()
export class GtfsTripScopeHandlerFactory {
    private dictionary: Map<DetailedTripScope, GtfsTripScopeHandler>;

    constructor(
        @inject(OgModuleToken.InfoGtfsTripScopeHandler) infoTripScopeHandler: GtfsTripScopeHandler,
        @inject(OgModuleToken.StopTimesGtfsTripScopeHandler)
        stopTimesTripScopeHandler: GtfsTripScopeHandler,
        @inject(OgModuleToken.ShapesGtfsTripScopeHandler) shapeTripScopeHandler: GtfsTripScopeHandler,
        @inject(OgModuleToken.VehicleDescriptorGtfsTripScopeHandler)
        vehicleDescriptorTripScopeHandler: GtfsTripScopeHandler
    ) {
        this.dictionary = new Map<DetailedTripScope, GtfsTripScopeHandler>();
        this.dictionary.set(DetailedTripScope.Info, infoTripScopeHandler);
        this.dictionary.set(DetailedTripScope.StopTimes, stopTimesTripScopeHandler);
        this.dictionary.set(DetailedTripScope.Shapes, shapeTripScopeHandler);
        this.dictionary.set(DetailedTripScope.VehicleDescriptor, vehicleDescriptorTripScopeHandler);
    }

    public getStrategy(scope: DetailedTripScope): GtfsTripScopeHandler {
        if (this.dictionary.has(scope)) {
            return this.dictionary.get(scope)!;
        }

        throw new GeneralError("Unknown GTFS trip scope strategy", this.constructor.name, undefined, 500);
    }
}
