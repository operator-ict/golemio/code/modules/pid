import { IBoundingBox, IBoundingBoxWithCenter } from "#og/public/domain/IBoudingBox";

export interface IBoundingBoxHelper {
    mapToCenterFormat(boundingBox: IBoundingBox): IBoundingBoxWithCenter;
}
