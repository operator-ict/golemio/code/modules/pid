import { IGeoMeasurementHelper } from "#helpers/geo/interfaces/IGeoMeasurementHelper";
import { IBoundingBox, IBoundingBoxWithCenter } from "#og/public/domain/IBoudingBox";
import { OgModuleToken } from "#og/public/ioc/OgModuleToken";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { IBoundingBoxHelper } from "./interfaces/IBoundingBoxHelper";

@injectable()
export class BoundingBoxHelper implements IBoundingBoxHelper {
    constructor(@inject(OgModuleToken.GeoMeasurementHelper) private geoHelper: IGeoMeasurementHelper) {}

    public mapToCenterFormat(boundingBox: IBoundingBox): IBoundingBoxWithCenter {
        const centerPoint = {
            longitude: (boundingBox.bottomRightLongitude + boundingBox.topLeftLongitude) / 2,
            latitude: (boundingBox.bottomRightLatitude + boundingBox.topLeftLatitude) / 2,
        };
        // Calculate the width and height in meters
        const width = this.geoHelper.getDistanceInMeters(
            [boundingBox.topLeftLongitude, boundingBox.topLeftLatitude],
            [boundingBox.bottomRightLongitude, boundingBox.topLeftLatitude]
        );
        const height = this.geoHelper.getDistanceInMeters(
            [boundingBox.topLeftLongitude, boundingBox.topLeftLatitude],
            [boundingBox.topLeftLongitude, boundingBox.bottomRightLatitude]
        );

        return { centerPoint, width, height };
    }
}
