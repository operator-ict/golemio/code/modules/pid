import { GeoMeasurementHelper } from "#helpers/geo/GeoMeasurementHelper";
import { PUBLIC_CACHE_NAMESPACE_PREFIX } from "#sch/vehicle-positions/redis/const";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { OutputGatewayContainer } from "@golemio/core/dist/output-gateway/ioc/Di";
import { instanceCachingFactory } from "@golemio/core/dist/shared/tsyringe";
import { VehicleDescriptorCachedRepository } from "../data-access/VehicleDescriptorCachedRepository";
import { DelayComputationRepository } from "../data-access/redis/DelayComputationRepository";
import { PublicGtfsDepartureRepository } from "../data-access/redis/PublicGtfsDepartureRepository";
import { PublicStopTimeRepository } from "../data-access/redis/PublicStopTimeRepository";
import { PublicVehiclePositionsRepository } from "../data-access/redis/PublicVehiclePositionsRepository";
import { IStopTimeRepository } from "../domain/repository/IStopTimeRepository";
import { IVehiclePositionsRepository } from "../domain/repository/IVehiclePositionsRepository";
import { VPSubscriber } from "../service/VPSubscriber";
import { DepartureBoardFacade } from "../service/facade/DepartureBoardFacade";
import { DetailedTripFacade } from "../service/facade/DetailedTripFacade";
import { GtfsTripLookupFacade } from "../service/facade/GtfsTripLookupFacade";
import { PublicVehiclePositionsFacade } from "../service/facade/VehiclePositionsFacade";
import { BoundingBoxHelper } from "../service/helpers/BoundingBoxHelper";
import { GtfsTripScopeHandlerFactory } from "../service/helpers/gtfs-trip-scope/GtfsTripScopeHandlerFactory";
import { InfoGtfsTripScopeHandler } from "../service/helpers/gtfs-trip-scope/strategy/InfoTripScopeHandler";
import { ShapesGtfsTripScopeHandler } from "../service/helpers/gtfs-trip-scope/strategy/ShapesTripScopeHandler";
import { StopTimesGtfsTripScopeHandler } from "../service/helpers/gtfs-trip-scope/strategy/StopTimesTripScopeHandler";
import { VehicleDescriptorGtfsTripScopeHandler } from "../service/helpers/gtfs-trip-scope/strategy/VehicleDescriptorTripScopeHandler";
import { IBoundingBoxHelper } from "../service/helpers/interfaces/IBoundingBoxHelper";
import { TripScopeHandlerFactory } from "../service/helpers/trip-scope/TripScopeHandlerFactory";
import { InfoTripScopeHandler } from "../service/helpers/trip-scope/strategy/InfoTripScopeHandler";
import { ShapesTripScopeHandler } from "../service/helpers/trip-scope/strategy/ShapesTripScopeHandler";
import { StopTimesTripScopeHandler } from "../service/helpers/trip-scope/strategy/StopTimesTripScopeHandler";
import { VehicleDescriptorTripScopeHandler } from "../service/helpers/trip-scope/strategy/VehicleDescriptorTripScopeHandler";
import { PublicGtfsTripInfoTransformation } from "../service/transformations/gtfs-trip-scopes/PublicGtfsTripInfoTransformation";
import { PublicGtfsTripShapesTransformation } from "../service/transformations/gtfs-trip-scopes/PublicGtfsTripShapesTransformation";
import { PublicGtfsVehicleDescriptorTransformation } from "../service/transformations/gtfs-trip-scopes/PublicGtfsVehicleDescriptorTransformation";
import { PublicGtfsTripStopTimesTransformation } from "../service/transformations/gtfs-trip-scopes/PublicTripStopTimesTransformation";
import { OgModuleToken } from "./OgModuleToken";

const ogPublicContainer = OutputGatewayContainer.createChildContainer();

ogPublicContainer.registerSingleton(OgModuleToken.GeoMeasurementHelper, GeoMeasurementHelper);
ogPublicContainer.registerSingleton(OgModuleToken.BoundingBoxHelper, BoundingBoxHelper);

ogPublicContainer.registerSingleton(OgModuleToken.VehicleDescriptorCachedRepository, VehicleDescriptorCachedRepository);
ogPublicContainer.registerSingleton(OgModuleToken.PublicVehiclePositionsRepository, PublicVehiclePositionsRepository);
ogPublicContainer.registerSingleton(OgModuleToken.DelayComputationRepository, DelayComputationRepository);
ogPublicContainer.registerSingleton(OgModuleToken.PublicStopTimeRepository, PublicStopTimeRepository);
ogPublicContainer.registerSingleton(OgModuleToken.PublicGtfsDepartureRepository, PublicGtfsDepartureRepository);

ogPublicContainer.registerSingleton(OgModuleToken.InfoTripScopeHandler, InfoTripScopeHandler);
ogPublicContainer.registerSingleton(OgModuleToken.StopTimesTripScopeHandler, StopTimesTripScopeHandler);
ogPublicContainer.registerSingleton(OgModuleToken.ShapesTripScopeHandler, ShapesTripScopeHandler);
ogPublicContainer.registerSingleton(OgModuleToken.VehicleDescriptorTripScopeHandler, VehicleDescriptorTripScopeHandler);
ogPublicContainer.registerSingleton(OgModuleToken.TripScopeHandlerFactory, TripScopeHandlerFactory);

//#region GtfsTripLookup scope handlers
ogPublicContainer.registerSingleton(OgModuleToken.PublicGtfsTripInfoTransformation, PublicGtfsTripInfoTransformation);
ogPublicContainer.registerSingleton(OgModuleToken.PublicGtfsTripShapesTransformation, PublicGtfsTripShapesTransformation);
ogPublicContainer.registerSingleton(
    OgModuleToken.PublicGtfsVehicleDescriptorTransformation,
    PublicGtfsVehicleDescriptorTransformation
);
ogPublicContainer.registerSingleton(OgModuleToken.PublicGtfsTripStopTimesTransformation, PublicGtfsTripStopTimesTransformation);
ogPublicContainer.registerSingleton(OgModuleToken.InfoGtfsTripScopeHandler, InfoGtfsTripScopeHandler);
ogPublicContainer.registerSingleton(OgModuleToken.StopTimesGtfsTripScopeHandler, StopTimesGtfsTripScopeHandler);
ogPublicContainer.registerSingleton(OgModuleToken.ShapesGtfsTripScopeHandler, ShapesGtfsTripScopeHandler);
ogPublicContainer.registerSingleton(OgModuleToken.VehicleDescriptorGtfsTripScopeHandler, VehicleDescriptorGtfsTripScopeHandler);
ogPublicContainer.registerSingleton(OgModuleToken.GtfsTripScopeHandlerFactory, GtfsTripScopeHandlerFactory);
//#endregion

ogPublicContainer.register(OgModuleToken.GtfsTripLookupFacade, {
    useFactory: instanceCachingFactory((c) => {
        return new GtfsTripLookupFacade(c.resolve<GtfsTripScopeHandlerFactory>(OgModuleToken.GtfsTripScopeHandlerFactory));
    }),
});

ogPublicContainer.register(OgModuleToken.PublicVehiclePositionsFacade, {
    useFactory: instanceCachingFactory((c) => {
        return new PublicVehiclePositionsFacade(
            c.resolve<IVehiclePositionsRepository>(OgModuleToken.PublicVehiclePositionsRepository),
            c.resolve<IBoundingBoxHelper>(OgModuleToken.BoundingBoxHelper),
            c.resolve<ILogger>(CoreToken.Logger)
        );
    }),
});

ogPublicContainer.register(OgModuleToken.DetailedTripFacade, {
    useFactory: instanceCachingFactory((c) => {
        return new DetailedTripFacade(
            c.resolve<TripScopeHandlerFactory>(OgModuleToken.TripScopeHandlerFactory),
            c.resolve<IVehiclePositionsRepository>(OgModuleToken.PublicVehiclePositionsRepository)
        );
    }),
});

ogPublicContainer.register(OgModuleToken.DepartureBoardFacade, {
    useFactory: instanceCachingFactory((c) => {
        return new DepartureBoardFacade(
            c.resolve<PublicGtfsDepartureRepository>(OgModuleToken.PublicGtfsDepartureRepository),
            c.resolve<IVehiclePositionsRepository>(OgModuleToken.PublicVehiclePositionsRepository),
            c.resolve<IStopTimeRepository>(OgModuleToken.PublicStopTimeRepository),
            c.resolve<VehicleDescriptorCachedRepository>(OgModuleToken.VehicleDescriptorCachedRepository)
        );
    }),
});

ogPublicContainer.register(OgModuleToken.VPSubscriber, {
    useFactory: instanceCachingFactory((c) => {
        const config = c.resolve<ISimpleConfig>(CoreToken.SimpleConfig);
        const logger = c.resolve<ILogger>(CoreToken.Logger);

        return new VPSubscriber(
            {
                channelName: PUBLIC_CACHE_NAMESPACE_PREFIX,
                redisConnectionString: config.getValue<string>("env.REDIS_CONN"),
                logger: logger,
            },
            c.resolve<IVehiclePositionsRepository>(OgModuleToken.PublicVehiclePositionsRepository)
        );
    }),
});

export { ogPublicContainer as OgPublicContainer };
