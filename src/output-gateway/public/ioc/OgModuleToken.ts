export const OgModuleToken = {
    GeoMeasurementHelper: Symbol(),
    BoundingBoxHelper: Symbol(),
    VehicleDescriptorCachedRepository: Symbol(),
    PublicVehiclePositionsRepository: Symbol(),
    PublicStopTimeRepository: Symbol(),
    DelayComputationRepository: Symbol(),
    PublicGtfsDepartureRepository: Symbol(),
    //#region DetailedTrip scope handlers
    InfoTripScopeHandler: Symbol(),
    StopTimesTripScopeHandler: Symbol(),
    ShapesTripScopeHandler: Symbol(),
    VehicleDescriptorTripScopeHandler: Symbol(),
    TripScopeHandlerFactory: Symbol(),
    //#endregion
    //#region GtfsTripLookup scope handlers
    PublicGtfsTripInfoTransformation: Symbol(),
    PublicGtfsTripShapesTransformation: Symbol(),
    PublicGtfsVehicleDescriptorTransformation: Symbol(),
    PublicGtfsTripStopTimesTransformation: Symbol(),
    InfoGtfsTripScopeHandler: Symbol(),
    StopTimesGtfsTripScopeHandler: Symbol(),
    ShapesGtfsTripScopeHandler: Symbol(),
    VehicleDescriptorGtfsTripScopeHandler: Symbol(),
    GtfsTripScopeHandlerFactory: Symbol(),
    //#endregion
    GtfsTripLookupFacade: Symbol(),
    PublicVehiclePositionsFacade: Symbol(),
    DetailedTripFacade: Symbol(),
    DepartureBoardFacade: Symbol(),
    VPSubscriber: Symbol(),
};
