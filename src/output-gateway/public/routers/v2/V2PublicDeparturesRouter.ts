import { V2PublicDepartureBoardsController } from "#og/public/controllers/v2/V2PublicDepartureBoardsController";
import { OgPublicContainer } from "#og/public/ioc/Di";
import { RouteVersion } from "#og/shared/constants";
import { AbstractRouter } from "@golemio/core/dist/helpers/routing/AbstractRouter";
import { CacheHeaderMiddleware } from "@golemio/core/dist/output-gateway";
import { CompressionByDefaultMiddleware } from "@golemio/core/dist/output-gateway/CompressionByDefaultMiddleware";
import { checkErrors } from "@golemio/core/dist/output-gateway/Validation";
import { ContainerToken } from "@golemio/core/dist/output-gateway/ioc";
import { Router } from "@golemio/core/dist/shared/express";
import { query } from "@golemio/core/dist/shared/express-validator";
import { CustomStopIdGroupValidator } from "./helpers/CustomStopIdGroupValidator";

export class V2PublicDeparturesRouter extends AbstractRouter {
    public router: Router;
    private cacheHeaderMiddleware: CacheHeaderMiddleware;
    private compressionByDefaultMiddleware: CompressionByDefaultMiddleware;
    private departureController: V2PublicDepartureBoardsController;

    constructor() {
        super(RouteVersion.v2, "public/departureboards");
        this.router = Router();
        this.cacheHeaderMiddleware = OgPublicContainer.resolve<CacheHeaderMiddleware>(ContainerToken.CacheHeaderMiddleware);
        this.compressionByDefaultMiddleware = OgPublicContainer.resolve<CompressionByDefaultMiddleware>(
            ContainerToken.CompressionByDefaultMiddleware
        );
        this.departureController = new V2PublicDepartureBoardsController();
        this.initRoutes();
    }

    protected initRoutes(): void {
        this.router.get(
            "/",
            [
                query("stopIds").exists().custom(CustomStopIdGroupValidator.validate),
                query("limit").optional().isInt({ min: 1, max: 30 }).not().isArray(),
                query("routeShortNames").optional().not().isEmpty({ ignore_whitespace: true }),
                query("minutesAfter").optional().isInt({ min: 1, max: 360 }).not().isArray(),
            ],
            checkErrors,
            // max-age 3 seconds, stale-while-revalidate 2 seconds
            this.cacheHeaderMiddleware.getMiddleware(3, 2),
            this.compressionByDefaultMiddleware.getMiddleware(),
            this.departureController.getAll
        );
    }
}

const v2PublicDeparturesRouter: AbstractRouter = new V2PublicDeparturesRouter();
export { v2PublicDeparturesRouter };
