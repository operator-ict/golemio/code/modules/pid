export enum DetailedTripScope {
    Info = "info",
    StopTimes = "stop_times",
    Shapes = "shapes",
    VehicleDescriptor = "vehicle_descriptor",
}
