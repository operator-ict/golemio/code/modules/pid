import { CustomValidator } from "@golemio/core/dist/shared/express-validator";

export class CustomBBoxValidator {
    /**
     * Bounding box format: topLat,topLon,bottomLat,bottomLon
     * 4 positive or negative floating point numbers separated by commas
     * @example 50.073619,14.414826,50.092867,14.438086
     */
    private static BOUNDING_BOX_REGEX = /^(?:-?\d+?(?:\.\d+?)?,){3}-?\d+?(?:\.\d+?)?$/;

    public static validate: CustomValidator = (value, _) => {
        if (typeof value !== "string" || value.trim().length === 0 || value.length > 80 || !this.BOUNDING_BOX_REGEX.test(value)) {
            return false;
        }

        const [topLat, topLon, bottomLat, bottomLon] = value.split(",").map((coordinate) => Number.parseFloat(coordinate));

        return (
            this.isLatitudeValid(topLat) &&
            this.isLatitudeValid(bottomLat) &&
            this.isLongitudeValid(topLon) &&
            this.isLongitudeValid(bottomLon)
        );
    };

    private static isLatitudeValid(latitude: number): boolean {
        return latitude >= -90 && latitude <= 90;
    }

    private static isLongitudeValid(longitude: number): boolean {
        return longitude >= -180 && longitude <= 180;
    }
}
