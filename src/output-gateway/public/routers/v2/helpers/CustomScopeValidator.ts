import { CustomValidator } from "@golemio/core/dist/shared/express-validator";
import { DetailedTripScope } from "./DetailedTripScopeEnum";

export class CustomScopeValidator {
    public static allowedScopes = Object.values(DetailedTripScope);

    public static validate: CustomValidator = (value, _) => {
        if (value instanceof Array) {
            return value.every((scope) => this.allowedScopes.includes(scope));
        }

        return this.allowedScopes.includes(value);
    };
}
