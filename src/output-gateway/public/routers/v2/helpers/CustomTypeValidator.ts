import { getGtfsRouteTypesAsStrings } from "#helpers/RouteTypeEnums";
import { CustomValidator } from "@golemio/core/dist/shared/express-validator";

export class CustomTypeValidator {
    public static allowedTypes: string[] = getGtfsRouteTypesAsStrings();

    public static validate: CustomValidator = (value, _) => {
        if (value instanceof Array) {
            return value.every((source: string) => this.allowedTypes.includes(source));
        } else {
            return this.allowedTypes.includes(value);
        }
    };
}
