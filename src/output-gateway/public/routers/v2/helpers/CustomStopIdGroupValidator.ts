import { CustomValidator } from "@golemio/core/dist/shared/express-validator";

const MAX_STOP_GROUPS = 50;
const MAX_STOPS_IN_GROUP = 50;
const MAX_STOPS_TOTAL = 50;

export class CustomStopIdGroupValidator {
    public static validate: CustomValidator = (value, _) => {
        if (value instanceof Array) {
            if (value.length === 0 || value.length > MAX_STOP_GROUPS) {
                return false;
            }

            let stopsTotal = 0;
            for (const stopIdGroup of value) {
                const stopIds = this.parseStopIdsFromGroup(stopIdGroup);

                if (!this.isStopIdGroupValid(stopIds)) {
                    return false;
                }

                stopsTotal += stopIds.length;
                if (stopsTotal > MAX_STOPS_TOTAL) {
                    return false;
                }
            }

            return true;
        }

        const stopIds = this.parseStopIdsFromGroup(value);
        return this.isStopIdGroupValid(stopIds);
    };

    private static isStopIdGroupValid(stopIds: string[] | null): stopIds is string[] {
        try {
            if (
                !Array.isArray(stopIds) ||
                stopIds.length === 0 ||
                stopIds.length > MAX_STOPS_IN_GROUP ||
                stopIds.some((stopId) => typeof stopId !== "string" || stopId.length === 0 || stopId.length > 30)
            ) {
                return false;
            }

            return true;
        } catch {
            return false;
        }
    }

    private static parseStopIdsFromGroup(value: string): string[] | null {
        try {
            const parsed = JSON.parse(value);
            const keys = Object.keys(parsed);
            if (keys.length !== 1) {
                return null;
            }

            const priority = Number.parseInt(keys[0]);
            if (Number.isNaN(priority)) {
                return null;
            }

            return parsed[priority];
        } catch {
            return null;
        }
    }
}
