import { V2PublicGtfsController } from "#og/public/controllers/v2/V2PublicGtfsController";
import { OgPublicContainer } from "#og/public/ioc/Di";
import { RouteVersion } from "#og/shared/constants";
import { AbstractRouter } from "@golemio/core/dist/helpers/routing/AbstractRouter";
import { CacheHeaderMiddleware } from "@golemio/core/dist/output-gateway/CacheHeaderMiddleware";
import { CompressionByDefaultMiddleware } from "@golemio/core/dist/output-gateway/CompressionByDefaultMiddleware";
import { checkErrors } from "@golemio/core/dist/output-gateway/Validation";
import { ContainerToken } from "@golemio/core/dist/output-gateway/ioc";
import { Router } from "@golemio/core/dist/shared/express";
import { param, query } from "@golemio/core/dist/shared/express-validator";
import { CustomScopeValidator } from "./helpers/CustomScopeValidator";

export class V2PublicGtfsRouter extends AbstractRouter {
    public router: Router;
    private cacheHeaderMiddleware: CacheHeaderMiddleware;
    private compressionByDefaultMiddleware: CompressionByDefaultMiddleware;
    private gtfsController: V2PublicGtfsController;

    constructor() {
        super(RouteVersion.v2, "public/gtfs");
        this.router = Router();
        this.cacheHeaderMiddleware = OgPublicContainer.resolve<CacheHeaderMiddleware>(ContainerToken.CacheHeaderMiddleware);
        this.compressionByDefaultMiddleware = OgPublicContainer.resolve<CompressionByDefaultMiddleware>(
            ContainerToken.CompressionByDefaultMiddleware
        );
        this.gtfsController = new V2PublicGtfsController();
        this.initRoutes();
    }

    protected initRoutes(): void {
        this.router.get(
            "/trips/:gtfsTripId",
            [param("gtfsTripId").exists(), query("scopes").custom(CustomScopeValidator.validate)],
            checkErrors,
            // max-age 4 hours, stale-while-revalidate 1 minute
            this.cacheHeaderMiddleware.getMiddleware(4 * 60 * 60, 60),
            this.compressionByDefaultMiddleware.getMiddleware(),
            this.gtfsController.getOneTrip
        );
    }
}

const v2PublicGtfsRouter: AbstractRouter = new V2PublicGtfsRouter();
export { v2PublicGtfsRouter };
