import { V2PublicVehiclePositionsController } from "#og/public/controllers/v2/V2PublicVehiclePositionsController";
import { OgPublicContainer } from "#og/public/ioc/Di";
import { OgModuleToken } from "#og/public/ioc/OgModuleToken";
import { VPSubscriber } from "#og/public/service/VPSubscriber";
import { RouteVersion } from "#og/shared/constants";
import { AbstractRouter } from "@golemio/core/dist/helpers/routing/AbstractRouter";
import { CacheHeaderMiddleware } from "@golemio/core/dist/output-gateway/CacheHeaderMiddleware";
import { CompressionByDefaultMiddleware } from "@golemio/core/dist/output-gateway/CompressionByDefaultMiddleware";
import { checkErrors } from "@golemio/core/dist/output-gateway/Validation";
import { ContainerToken } from "@golemio/core/dist/output-gateway/ioc";
import { Router } from "@golemio/core/dist/shared/express";
import { param, query } from "@golemio/core/dist/shared/express-validator";
import { CustomBBoxValidator } from "./helpers/CustomBBoxValidator";
import { CustomScopeValidator } from "./helpers/CustomScopeValidator";
import { CustomTypeValidator } from "./helpers/CustomTypeValidator";

export class V2PublicVehiclePositionsRouter extends AbstractRouter {
    public router: Router;
    private cacheHeaderMiddleware: CacheHeaderMiddleware;
    private compressionByDefaultMiddleware: CompressionByDefaultMiddleware;
    private vehiclePositionsController: V2PublicVehiclePositionsController;
    private redisSubscriber: VPSubscriber;

    constructor() {
        super(RouteVersion.v2, "public/vehiclepositions");
        this.router = Router();
        this.cacheHeaderMiddleware = OgPublicContainer.resolve<CacheHeaderMiddleware>(ContainerToken.CacheHeaderMiddleware);
        this.compressionByDefaultMiddleware = OgPublicContainer.resolve<CompressionByDefaultMiddleware>(
            ContainerToken.CompressionByDefaultMiddleware
        );
        this.vehiclePositionsController = new V2PublicVehiclePositionsController();
        this.initRoutes();
        this.redisSubscriber = OgPublicContainer.resolve<VPSubscriber>(OgModuleToken.VPSubscriber);
        this.redisSubscriber.initialize();
    }

    protected initRoutes(): void {
        this.router.get(
            "/",
            [
                query("boundingBox").optional().custom(CustomBBoxValidator.validate),
                query("routeShortName").optional().not().isEmpty({ ignore_whitespace: true }),
                query("routeType").optional().custom(CustomTypeValidator.validate),
            ],
            checkErrors,
            // max-age 5 seconds, stale-while-revalidate 5 seconds
            this.cacheHeaderMiddleware.getMiddleware(5, 5),
            this.compressionByDefaultMiddleware.getMiddleware(),
            this.vehiclePositionsController.getAll
        );

        this.router.get(
            "/:vehicleId;gtfsTripId=:gtfsTripId",
            [param("vehicleId").exists(), param("gtfsTripId").exists(), query("scopes").custom(CustomScopeValidator.validate)],
            checkErrors,
            // max-age 5 seconds, stale-while-revalidate 5 seconds
            this.cacheHeaderMiddleware.getMiddleware(5, 5),
            this.compressionByDefaultMiddleware.getMiddleware(),
            this.vehiclePositionsController.getOneByParamsCombination
        );

        this.router.get(
            "/:vehicleId",
            [param("vehicleId").exists(), query("scopes").custom(CustomScopeValidator.validate)],
            checkErrors,
            // max-age 5 seconds, stale-while-revalidate 5 seconds
            this.cacheHeaderMiddleware.getMiddleware(5, 5),
            this.compressionByDefaultMiddleware.getMiddleware(),
            this.vehiclePositionsController.getOneByVehicleId
        );
    }
}

const v2PublicVPRouter: AbstractRouter = new V2PublicVehiclePositionsRouter();

export { v2PublicVPRouter };
