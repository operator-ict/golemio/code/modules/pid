import { GTFSRouteTypeEnum } from "#helpers/RouteTypeEnums";
import { IBoundingBox } from "#og/public/domain/IBoudingBox";
import { DetailedTripScope } from "#og/public/routers/v2/helpers/DetailedTripScopeEnum";

export interface IVehiclePositionsParams {
    boundingBox: IBoundingBox;
    routeShortName: string | string[] | undefined;
    type: GTFSRouteTypeEnum[] | undefined;
}

export interface IDetailedInfoParams {
    vehicleId: string;
    tripId?: string;
    scopes: DetailedTripScope[];
}

export interface IGtfsTripLookupParams {
    gtfsTripId: string;
    scopes: DetailedTripScope[];
}

export interface IDepartureBoardsStopIdGroups {
    priority: number;
    stopIds: string[];
}

export interface IDepartureBoardsParams {
    stopIds: Set<IDepartureBoardsStopIdGroups>;
    limit: number;
    routeShortNames: string[] | null;
    minutesAfter: number;
}
