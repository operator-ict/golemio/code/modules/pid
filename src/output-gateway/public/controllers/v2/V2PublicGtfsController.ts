import { OgPublicContainer } from "#og/public/ioc/Di";
import { OgModuleToken } from "#og/public/ioc/OgModuleToken";
import { DetailedTripScope } from "#og/public/routers/v2/helpers/DetailedTripScopeEnum";
import { GtfsTripLookupFacade } from "#og/public/service/facade/GtfsTripLookupFacade";
import { createChildSpan } from "@golemio/core/dist/monitoring/opentelemetry/trace-provider";
import { Request, RequestHandler } from "@golemio/core/dist/shared/express";
import { IGtfsTripLookupParams } from "./interfaces/PublicParamsInterfaces";

export class V2PublicGtfsController {
    public getOneTrip: RequestHandler = async (req, res, next) => {
        const span = createChildSpan("V2PublicGtfsController.getOneTrip");

        try {
            const facade = OgPublicContainer.resolve<GtfsTripLookupFacade>(OgModuleToken.GtfsTripLookupFacade);
            const params = this.parseGtfsTripLookupParams(req);
            const info = await facade.getOneByGtfsTripId(params.gtfsTripId, params.scopes);

            res.json(info);
        } catch (err) {
            next(err);
        } finally {
            span?.end();
        }
    };

    private parseGtfsTripLookupParams(req: Request): IGtfsTripLookupParams {
        return {
            gtfsTripId: req.params.gtfsTripId,
            scopes:
                req.query.scopes instanceof Array
                    ? Array.from(new Set(req.query.scopes as DetailedTripScope[]))
                    : [req.query.scopes as DetailedTripScope],
        };
    }
}
