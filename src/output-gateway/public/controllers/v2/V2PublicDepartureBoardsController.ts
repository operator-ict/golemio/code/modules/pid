import { OgPublicContainer } from "#og/public/ioc/Di";
import { OgModuleToken } from "#og/public/ioc/OgModuleToken";
import { DepartureBoardFacade } from "#og/public/service/facade/DepartureBoardFacade";
import { RequestHandler } from "@golemio/core/dist/shared/express";
import { ParsedQs } from "@golemio/core/dist/shared/qs";
import { IDepartureBoardsParams, IDepartureBoardsStopIdGroups } from "./interfaces/PublicParamsInterfaces";
import { createChildSpan } from "@golemio/core/dist/monitoring/opentelemetry/trace-provider";

export class V2PublicDepartureBoardsController {
    constructor() {}

    public getAll: RequestHandler = async (req, res, next) => {
        const span = createChildSpan("V2PublicDepartureBoardsController.getAll");

        try {
            const facade = OgPublicContainer.resolve<DepartureBoardFacade>(OgModuleToken.DepartureBoardFacade);
            const params = this.parseDepartureParams(req.query);
            const result = await facade.getAll(params.stopIds, params.limit, params.routeShortNames, params.minutesAfter);

            if (result === null) {
                res.status(404).send([]);
                return;
            }
            res.json(result);
        } catch (err) {
            next(err);
        } finally {
            span?.end();
        }
    };

    private parseDepartureParams(query: ParsedQs): IDepartureBoardsParams {
        return {
            stopIds: this.parseStopIds(query.stopIds instanceof Array ? (query.stopIds as string[]) : [query.stopIds as string]),
            routeShortNames: query.routeShortNames
                ? query.routeShortNames instanceof Array
                    ? (query.routeShortNames as string[])
                    : [query.routeShortNames as string]
                : null,
            limit: query.limit ? parseInt(query.limit as string) : 5,
            minutesAfter: query.minutesAfter ? parseInt(query.minutesAfter as string) : 60,
        };
    }

    private parseStopIds(stopIdsFromInput: string[]): Set<IDepartureBoardsStopIdGroups> {
        let output: Set<IDepartureBoardsStopIdGroups> = new Set();

        for (const stopId of stopIdsFromInput) {
            const parsed = JSON.parse(stopId);
            const priority = Object.keys(parsed)[0];
            const stopIds: string[] = parsed[priority];

            output.add({
                priority: Number.parseInt(priority),
                stopIds: [...new Set(stopIds)],
            });
        }

        return output;
    }
}
