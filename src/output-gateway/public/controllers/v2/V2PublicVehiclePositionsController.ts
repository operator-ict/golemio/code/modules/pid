import { getGtfsRouteType } from "#helpers/RouteTypeEnums";
import { DEFAULT_BOUDINGBOX } from "#og/public/domain/const";
import { OgPublicContainer } from "#og/public/ioc/Di";
import { OgModuleToken } from "#og/public/ioc/OgModuleToken";
import { DetailedTripScope } from "#og/public/routers/v2/helpers/DetailedTripScopeEnum";
import { DetailedTripFacade } from "#og/public/service/facade/DetailedTripFacade";
import { PublicVehiclePositionsFacade } from "#og/public/service/facade/VehiclePositionsFacade";
import { Request, RequestHandler } from "@golemio/core/dist/shared/express";
import { ParsedQs } from "@golemio/core/dist/shared/qs";
import { IDetailedInfoParams, IVehiclePositionsParams } from "./interfaces/PublicParamsInterfaces";

export class V2PublicVehiclePositionsController {
    constructor() {}

    public getAll: RequestHandler = async (req, res, next) => {
        try {
            const facade = OgPublicContainer.resolve<PublicVehiclePositionsFacade>(OgModuleToken.PublicVehiclePositionsFacade);
            const params = this.parsePositionsParams(req.query);
            const result = await facade.getAll(params.boundingBox, params.routeShortName, params.type);

            res.json(result);
        } catch (err) {
            next(err);
        }
    };

    public getOneByVehicleId: RequestHandler = async (req, res, next) => {
        try {
            const facade = OgPublicContainer.resolve<DetailedTripFacade>(OgModuleToken.DetailedTripFacade);
            const params = this.parseDetailedParams(req);
            const info = await facade.getOneByVehicleId(params.vehicleId, params.scopes);

            res.json(info);
        } catch (err) {
            next(err);
        }
    };

    public getOneByParamsCombination: RequestHandler = async (req, res, next) => {
        try {
            const facade = OgPublicContainer.resolve<DetailedTripFacade>(OgModuleToken.DetailedTripFacade);
            const params = this.parseDetailedParams(req);
            const info = await facade.getOneByVehicleId(params.vehicleId, params.scopes, params.tripId);

            res.json(info);
        } catch (err) {
            next(err);
        }
    };

    private parsePositionsParams(query: ParsedQs): IVehiclePositionsParams {
        const boundingBox = query.boundingBox ? (query.boundingBox as string).split(",").map((x) => parseFloat(x)) : undefined;
        // format "topLeft.lat,topLeft.lon,bottomRight.lat,bottomRight.lon"
        return {
            boundingBox: {
                topLeftLongitude: boundingBox ? boundingBox[1] : DEFAULT_BOUDINGBOX.topLeftLongitude,
                topLeftLatitude: boundingBox ? boundingBox[0] : DEFAULT_BOUDINGBOX.topLeftLatitude,
                bottomRightLongitude: boundingBox ? boundingBox[3] : DEFAULT_BOUDINGBOX.bottomRightLongitude,
                bottomRightLatitude: boundingBox ? boundingBox[2] : DEFAULT_BOUDINGBOX.bottomRightLatitude,
            },
            routeShortName: query.routeShortName
                ? query.routeShortName instanceof Array
                    ? query.routeShortName.map((routeShortName) => routeShortName as string)
                    : (query.routeShortName as string)
                : undefined,
            type: query.routeType
                ? query.routeType instanceof Array
                    ? query.routeType.map((type) => getGtfsRouteType(type as string))
                    : [getGtfsRouteType(query.routeType as string)]
                : undefined,
        };
    }

    private parseDetailedParams(req: Request): IDetailedInfoParams {
        return {
            vehicleId: req.params.vehicleId,
            tripId: req.params.gtfsTripId,
            scopes:
                req.query.scopes instanceof Array
                    ? Array.from(new Set(req.query.scopes as DetailedTripScope[]))
                    : [req.query.scopes as DetailedTripScope],
        };
    }
}
