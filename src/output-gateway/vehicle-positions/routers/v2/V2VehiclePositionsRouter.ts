/**
 * Router /WEB LAYER/: maps routes to specific controller functions, passes request parameters and handles responses.
 * Handles web logic (http request, response). Sets response headers, handles error responses.
 */

import { RopidRouterUtils } from "#og/shared";
import { RouteVersion, ValidationArrays } from "#og/shared/constants";
import { repositories } from "#og/vehicle-positions/data-access";
import { ITripWithPositionRepository } from "#og/vehicle-positions/data-access/interfaces/ITripWithPositionRepository";
import { IoRedisConnector } from "@golemio/core/dist/helpers/data-access/redis/IoRedisConnector";
import { AbstractRouter } from "@golemio/core/dist/helpers/routing/AbstractRouter";
import { createChildSpan } from "@golemio/core/dist/monitoring/opentelemetry/trace-provider";
import { CacheHeaderMiddleware } from "@golemio/core/dist/output-gateway/CacheHeaderMiddleware";
import { CompressionByDefaultMiddleware } from "@golemio/core/dist/output-gateway/CompressionByDefaultMiddleware";
import { parseBooleanQueryParam } from "@golemio/core/dist/output-gateway/Utils";
import { checkErrors, pagination, paginationLimitMiddleware } from "@golemio/core/dist/output-gateway/Validation";
import { ContainerToken, OutputGatewayContainer } from "@golemio/core/dist/output-gateway/ioc";
import { NextFunction, Request, RequestHandler, Response, Router } from "@golemio/core/dist/shared/express";
import { param, query } from "@golemio/core/dist/shared/express-validator";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import moment from "@golemio/core/dist/shared/moment-timezone";

export class V2VehiclePositionsRouter extends AbstractRouter {
    public readonly router: Router;
    private readonly redisConnector: IoRedisConnector;
    private readonly cacheHeaderMiddleware: CacheHeaderMiddleware;
    private readonly compressionByDefaultMiddleware: CompressionByDefaultMiddleware;
    protected readonly tripWithPositionRepository: ITripWithPositionRepository;

    constructor() {
        super(RouteVersion.v2, "vehiclepositions");
        this.router = Router();
        this.redisConnector = OutputGatewayContainer.resolve<IoRedisConnector>(ContainerToken.RedisConnector);
        this.cacheHeaderMiddleware = OutputGatewayContainer.resolve<CacheHeaderMiddleware>(ContainerToken.CacheHeaderMiddleware);
        this.compressionByDefaultMiddleware = OutputGatewayContainer.resolve<CompressionByDefaultMiddleware>(
            ContainerToken.CompressionByDefaultMiddleware
        );
        this.tripWithPositionRepository = repositories.tripWithLastPositionRepository;
        this.initRoutes();
    }

    public GetAll = async (req: Request, res: Response, next: NextFunction) => {
        const preferredTimezone = RopidRouterUtils.getPreferredTimezone(req.query.preferredTimezone);
        const span = createChildSpan("V2VehiclePositionsRouter.GetAll");
        try {
            const result = await this.tripWithPositionRepository.GetAll({
                cisTripNumber: Number(req.query.cisTripNumber) || undefined,
                includeNotTracking: parseBooleanQueryParam(req.query.includeNotTracking as string),
                includeNotPublic: parseBooleanQueryParam(req.query.includeNotPublic as string),
                limit: Number(req.query.limit) || undefined,
                offset: Number(req.query.offset) || undefined,
                routeId: req.query.routeId as string,
                routeShortName: req.query.routeShortName as string,
                updatedSince: req.query.updatedSince ? new Date(req.query.updatedSince as string) : null,
                preferredTimezone,
            });

            res.set("X-Last-Modified", moment(result.metadata.maxUpdatedAt).toISOString()).status(200).send(result.data);
        } catch (err) {
            next(err);
        } finally {
            span?.end();
        }
    };

    public GetOne = async (req: Request, res: Response, next: NextFunction) => {
        const id: string = req.params.id;
        const preferredTimezone = RopidRouterUtils.getPreferredTimezone(req.query.preferredTimezone);

        try {
            const data = await this.tripWithPositionRepository.GetOneByGTFSTripId(id, {
                includeNotTracking: parseBooleanQueryParam(req.query.includeNotTracking as string),
                includePositions: parseBooleanQueryParam(req.query.includePositions as string),
                preferredTimezone,
            });
            if (!data) {
                throw new GeneralError("not_found", this.constructor.name, undefined, 404);
            }
            res.status(200).send(data);
        } catch (err) {
            next(err);
        }
    };

    private GetGtfsRtFeed = (fileName: string, sendAsJson = false): RequestHandler => {
        return async (_req, res, next) => {
            try {
                let file: string | null = null;
                let timestamp: string | null = null;

                try {
                    const connection = this.redisConnector.getConnection();
                    const result = await Promise.all([
                        connection.hget("files:gtfsRt", fileName + ".pb"),
                        connection.get("files:gtfsRt:" + fileName + "_timestamp"),
                    ]);

                    file = result[0];
                    timestamp = result[1];
                } catch (err) {
                    throw new GeneralError("Error while fetching GTFS-RT feed", this.constructor.name, err, 500);
                }

                if (!file) {
                    throw new GeneralError("GTFS-RT feed not found", this.constructor.name, undefined, 404);
                }

                res.setHeader("Last-Modified", new Date(timestamp ? parseInt(timestamp) * 1000 : 0).toISOString());
                res.setHeader("Content-Type", sendAsJson ? "application/json" : "application/octet-stream");
                res.status(200).send(sendAsJson ? file : Buffer.from(file, "binary"));
            } catch (err) {
                next(err);
            }
        };
    };

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     * @param {number|string} expire TTL for the caching middleware
     */
    protected initRoutes = (): void => {
        //#region PID Vehicle Positions
        this.router.get(
            "/",
            [
                query("cisTripNumber").optional().isNumeric().not().isArray(),
                query("routeId").optional().not().isArray(),
                query("routeShortName").optional().not().isArray(),
                query("includeNotTracking").optional().isBoolean().not().isArray(),
                query("includeNotPublic").optional().isBoolean().not().isArray(),
                query("updatedSince").optional().isISO8601().not().isArray(),
                query("preferredTimezone").optional().isIn(ValidationArrays.preferredTimezone).not().isArray(),
            ],
            pagination,
            checkErrors,
            paginationLimitMiddleware(this.constructor.name),
            // max-age 5 seconds, stale-while-revalidate 5 seconds
            this.cacheHeaderMiddleware.getMiddleware(5, 5),
            this.compressionByDefaultMiddleware.getMiddleware(),
            this.GetAll
        );
        this.router.get(
            "/:id",
            [
                param("id").exists(),
                query("includeNotTracking").optional().isBoolean().not().isArray(),
                query("includePositions").optional().isBoolean().not().isArray(),
                query("includeNotPublic").optional().isBoolean().not().isArray(),
                query("preferredTimezone").optional().not().isEmpty({ ignore_whitespace: true }),
            ],
            checkErrors,
            // max-age 5 seconds, stale-while-revalidate 5 seconds
            this.cacheHeaderMiddleware.getMiddleware(5, 5),
            this.compressionByDefaultMiddleware.getMiddleware(),
            this.GetOne
        );
        //#endregion

        //#region GTFS Realtime
        this.router.get(
            "/gtfsrt/trip_updates.pb",
            this.cacheHeaderMiddleware.getMiddleware(40, 50),
            this.GetGtfsRtFeed("trip_updates")
        );
        this.router.get(
            "/gtfsrt/vehicle_positions.pb",
            this.cacheHeaderMiddleware.getMiddleware(40, 50),
            this.GetGtfsRtFeed("vehicle_positions")
        );
        this.router.get("/gtfsrt/pid_feed.pb", this.cacheHeaderMiddleware.getMiddleware(40, 50), this.GetGtfsRtFeed("pid_feed"));
        this.router.get("/gtfsrt/alerts.pb", this.cacheHeaderMiddleware.getMiddleware(40, 50), this.GetGtfsRtFeed("alerts"));
        //#endregion
    };
}

const v2VehiclepositionsRouter: AbstractRouter = new V2VehiclePositionsRouter();
export { v2VehiclepositionsRouter };
