import { PG_SCHEMA } from "#sch/const";
import { VehiclePositions } from "#sch/vehicle-positions";
import { PositionDto } from "#sch/vehicle-positions/models/PositionDto";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";

export class PositionRepository extends SequelizeModel {
    constructor() {
        super(
            VehiclePositions.positions.name + "Repository",
            VehiclePositions.positions.pgTableName,
            PositionDto.attributeModel,
            {
                schema: PG_SCHEMA,
            }
        );
    }

    public GetAll(): never {
        throw new Error("Not implemented");
    }

    public GetOne(): never {
        throw new Error("Not implemented");
    }
}
