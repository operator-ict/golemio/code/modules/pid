import { TripWithLastPositionModel } from "#sch/vehicle-positions/models/views";
import { IGeoJSONFeature, buildGeojsonFeatureCollection, buildGeojsonFeatureLatLng } from "@golemio/core/dist/output-gateway/Geo";

type TripWithLastPositionModelWithCoords = TripWithLastPositionModel & {
    lat: NonNullable<TripWithLastPositionModel["lat"]>;
    lng: NonNullable<TripWithLastPositionModel["lng"]>;
};

export class PositionOutputMapper {
    public static mapTripModelToDto = (data: TripWithLastPositionModelWithCoords): IGeoJSONFeature => {
        const {
            vehicle_type,
            vehicle_descriptor,
            all_positions = [],
            ...tripWithPosition
        } = data.toJSON() as TripWithLastPositionModelWithCoords;

        const tripObject = {
            last_position: {
                bearing: tripWithPosition.bearing,
                delay: {
                    actual: tripWithPosition.delay,
                    last_stop_arrival: tripWithPosition.delay_stop_arrival,
                    last_stop_departure: tripWithPosition.delay_stop_departure,
                },
                is_canceled: tripWithPosition.is_canceled,
                last_stop: {
                    arrival_time: tripWithPosition.last_stop_arrival_time_isostring,
                    departure_time: tripWithPosition.last_stop_departure_time_isostring,
                    id: tripWithPosition.last_stop_id,
                    sequence: tripWithPosition.last_stop_sequence,
                },
                next_stop: {
                    arrival_time: tripWithPosition.next_stop_arrival_time_isostring,
                    departure_time: tripWithPosition.next_stop_departure_time_isostring,
                    id: tripWithPosition.next_stop_id,
                    sequence: tripWithPosition.next_stop_sequence,
                },
                origin_timestamp: tripWithPosition.origin_timestamp_isostring,
                shape_dist_traveled: tripWithPosition.shape_dist_traveled,
                speed: tripWithPosition.speed,
                state_position: tripWithPosition.state_position || null,
                tracking: tripWithPosition.is_tracked,
            },
            trip: {
                agency_name: {
                    real: tripWithPosition.agency_name_real,
                    scheduled: tripWithPosition.agency_name_scheduled,
                },
                cis: {
                    line_id: tripWithPosition.cis_line_id,
                    trip_number: tripWithPosition.cis_trip_number,
                },
                gtfs: {
                    route_id: tripWithPosition.gtfs_route_id,
                    route_short_name: tripWithPosition.gtfs_route_short_name,
                    route_type: tripWithPosition.gtfs_route_type,
                    trip_headsign: tripWithPosition.last_stop_headsign ?? tripWithPosition.gtfs_trip_headsign,
                    trip_id: tripWithPosition.gtfs_trip_id,
                    trip_short_name: tripWithPosition.gtfs_trip_short_name,
                },
                origin_route_name: tripWithPosition.origin_route_name,
                sequence_id: tripWithPosition.run_number,
                start_timestamp: tripWithPosition.start_timestamp_isostring,
                vehicle_registration_number: tripWithPosition.vehicle_registration_number,
                vehicle_type,
                wheelchair_accessible: tripWithPosition.wheelchair_accessible,
                air_conditioned: vehicle_descriptor?.is_air_conditioned ?? null,
                usb_chargers: vehicle_descriptor?.has_usb_chargers ?? null,
            },
            ...(all_positions.length && {
                all_positions: buildGeojsonFeatureCollection(
                    all_positions.map((position) => {
                        return {
                            bearing: position.bearing,
                            delay: {
                                actual: position.delay,
                                last_stop_arrival: position.delay_stop_arrival,
                                last_stop_departure: position.delay_stop_departure,
                            },
                            is_canceled: position.is_canceled,
                            last_stop: {
                                arrival_time: position.last_stop_arrival_time_isostring,
                                departure_time: position.last_stop_departure_time_isostring,
                                id: position.last_stop_id,
                                sequence: position.last_stop_sequence,
                            },
                            lat: position.lat,
                            lng: position.lng,
                            next_stop: {
                                arrival_time: position.next_stop_arrival_time_isostring,
                                departure_time: position.next_stop_departure_time_isostring,
                                id: position.next_stop_id,
                                sequence: position.next_stop_sequence,
                            },
                            origin_timestamp: position.origin_timestamp_isostring,
                            shape_dist_traveled: position.shape_dist_traveled,
                            speed: position.speed,
                        };
                    }),
                    "lng",
                    "lat",
                    true
                ),
            }),
        };

        return buildGeojsonFeatureLatLng(tripObject, tripWithPosition.lng, tripWithPosition.lat);
    };
}
