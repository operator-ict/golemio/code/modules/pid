import { PG_SCHEMA } from "#sch/const";
import { DescriptorModel } from "#sch/vehicle-descriptors/models";
import { VehiclePositions } from "#sch/vehicle-positions";
import { ProcessedPositionModel, TripWithLastPositionModel } from "#sch/vehicle-positions/models/views";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import { IGeoJSONFeature, buildGeojsonFeatureCollection } from "@golemio/core/dist/output-gateway/Geo";
import { ContainerToken, OutputGatewayContainer } from "@golemio/core/dist/output-gateway/ioc/";
import { FeatureCollection } from "@golemio/core/dist/shared/geojson";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import moment from "@golemio/core/dist/shared/moment-timezone";
import Sequelize, { Includeable, ModelStatic, Op } from "@golemio/core/dist/shared/sequelize";
import { ArrayNotPublicRegistrationNumbers, StatePositionEnum } from "src/const";
import { PositionOutputMapper } from "../helpers/PositionOutputMapper";
import { ITripGetAllOutput, ITripWithPositionRepository } from "../interfaces/ITripWithPositionRepository";
import { IVPRepositoryInstances } from "../interfaces/IVPRepositoryInstances";

export class TripWithLastPositionRepository extends SequelizeModel implements ITripWithPositionRepository {
    public sequelizeModel!: ModelStatic<TripWithLastPositionModel>;
    private dbConnector: IDatabaseConnector;

    constructor() {
        super("TripWithLastPositionRepository", TripWithLastPositionModel.tableName, TripWithLastPositionModel.attributeModel, {
            schema: PG_SCHEMA,
        });
        this.dbConnector = OutputGatewayContainer.resolve<IDatabaseConnector>(ContainerToken.PostgresDatabase);
    }

    public Associate = (repositories: IVPRepositoryInstances) => {
        this.sequelizeModel.hasMany(repositories.processedPositionRepository.sequelizeModel, {
            as: "all_positions",
            foreignKey: "trips_id",
            scope: {
                lat: { [Op.not]: null },
                lng: { [Op.not]: null },
            },
        });

        this.sequelizeModel.belongsTo(repositories.vehicleTypeRepository.sequelizeModel, {
            as: "vehicle_type",
            foreignKey: "vehicle_type_id",
        });

        this.sequelizeModel.belongsTo(repositories.vehicleDescriptorRepository.sequelizeModel, {
            as: "vehicle_descriptor",
            foreignKey: "vehicle_registration_number",
            targetKey: "registration_number",
            scope: {
                [Sequelize.Op.and]: [
                    Sequelize.literal(
                        `${TripWithLastPositionModel.tableName}.gtfs_route_type = vehicle_descriptor.gtfs_route_type`
                    ),
                ],
            },
        });
    };

    /** Retrieves all vehicle trips
     * @param {object} options Options object with params
     * @param {number} [options.limit] Limit
     * @param {number} [options.offset] Offset
     * @param {string} [options.routeId] Filter trips by specific route id
     * @param {string} [options.routeShortName] Filter trips by specific route short name
     * @param {string} [options.tripId] Filter trips by specific trip id
     * @param {boolean} [options.includeNotTracking] Should include not tracking vehicle positions (those off a trip)
     * @returns Array of the retrieved records
     */
    public GetAll = async (options: {
        cisTripNumber?: number;
        routeId?: string;
        routeShortName?: string;
        tripId?: string;
        includeNotTracking?: boolean;
        includeNotPublic?: boolean;
        limit?: number;
        offset?: number;
        updatedSince?: Date | null;
        preferredTimezone: string;
    }): Promise<ITripGetAllOutput> => {
        const { limit, offset, updatedSince } = options;
        const minUpdatedAt = moment().subtract(10, "minutes");

        // updatedSince cannot be more than 10 minutes ago
        const maxUpdatedAt = updatedSince && moment(updatedSince).isSameOrAfter(minUpdatedAt) ? updatedSince.toISOString() : null;
        const include = this.composeIncludes(options);

        try {
            const rows = await this.sequelizeModel.findAll({
                attributes: {
                    exclude: ["id", "vehicle_type_id"],
                },
                include,
                limit,
                offset,
                where: {
                    [Op.and]: [
                        {
                            state_position: {
                                [Op.in]: this.determinePossibleStatePositions(
                                    options.includeNotTracking,
                                    options.includeNotPublic
                                ),
                            },
                        },
                        {
                            [Op.or]: [
                                { gtfs_trip_id: { [Op.ne]: null } },
                                {
                                    vehicle_registration_number: { [Op.in]: ArrayNotPublicRegistrationNumbers },
                                },
                            ],
                        },
                        maxUpdatedAt
                            ? {
                                  updated_at: {
                                      [Op.gt]: maxUpdatedAt,
                                  },
                              }
                            : {},
                        options.cisTripNumber ? { cis_trip_number: options.cisTripNumber } : {},
                        options.routeId ? { gtfs_route_id: options.routeId } : {},
                        options.routeShortName ? { gtfs_route_short_name: options.routeShortName } : {},
                        options.tripId ? { gtfs_trip_id: options.tripId } : {},
                    ],
                },
            });

            if (rows.length === 0) {
                return {
                    data: buildGeojsonFeatureCollection([]) as FeatureCollection,
                    metadata: {
                        maxUpdatedAt: maxUpdatedAt || new Date().toISOString(),
                    },
                };
            }

            let returnData = [];
            let maxUpdatedAtData: Date | null = null;

            for (const row of rows) {
                returnData.push(
                    PositionOutputMapper.mapTripModelToDto(
                        row as typeof row & { lat: NonNullable<typeof row.lat>; lng: NonNullable<typeof row.lng> }
                    )
                );
                if (maxUpdatedAtData === null || row.updated_at > maxUpdatedAtData) {
                    maxUpdatedAtData = row.updated_at;
                }
            }

            return {
                data: buildGeojsonFeatureCollection(returnData) as FeatureCollection,
                metadata: {
                    maxUpdatedAt: maxUpdatedAtData?.toISOString() || new Date().toISOString(),
                },
            };
        } catch (err) {
            throw new GeneralError("Database error", "VehiclepositionsTripsModel", err, 500);
        }
    };

    public GetOne(): never {
        throw new Error("Not implemented");
    }

    /** Retrieves specific vehicle trip
     * @param {string} id Id of the trip
     * @param {object} [options] Options object with params
     * @param {string} [options.includeNotTracking] Returns last known trip even if it is not tracked at time
     * @param {boolean} [options.includePositions] Should include all vehicle positions
     * @returns Object of the retrieved record or null
     */
    public GetOneByGTFSTripId = async (
        id: string,
        options: {
            includeNotTracking?: boolean;
            includePositions?: boolean;
            includeNotPublic?: boolean;
            preferredTimezone: string;
        }
    ): Promise<IGeoJSONFeature | null> => {
        try {
            const include = this.composeIncludes(options);
            const data = await this.sequelizeModel.findOne({
                attributes: {
                    exclude: ["id", "vehicle_type_id"],
                    include: ["updated_at"],
                },
                include,
                where: {
                    gtfs_trip_id: id,
                },
                order: [["updated_at", "DESC"]],
            });

            if (!data) {
                return null;
            }
            if (data.lat === null || data.lng === null) {
                throw new GeneralError("Missing lat or lng", this.constructor.name, undefined, 500, "pid");
            }
            return PositionOutputMapper.mapTripModelToDto(
                data as typeof data & { lat: NonNullable<typeof data.lat>; lng: NonNullable<typeof data.lng> }
            );
        } catch (err) {
            throw new GeneralError("Database error", "VehiclePositionsTripsModel", err, 500);
        }
    };

    private composeIncludes = (options: {
        includeNotTracking?: boolean;
        includePositions?: boolean;
        includeNotPublic?: boolean;
        updatedSince?: Date | null;
        preferredTimezone: string;
    }): Includeable[] => {
        const possibleStatePositions = this.determinePossibleStatePositions(options.includeNotTracking, options.includeNotPublic);
        const include: Includeable[] = [
            {
                as: "vehicle_type",
                model: this.dbConnector.getConnection().models[VehiclePositions.vehicleTypes.pgTableName],
            },
            {
                as: "vehicle_descriptor",
                model: this.dbConnector.getConnection().models[DescriptorModel.tableName],
                attributes: ["is_air_conditioned", "has_usb_chargers"],
            },
        ];

        if (options.includePositions) {
            include.push({
                attributes: {
                    exclude: ["trips_id", "state_position"],
                },
                as: "all_positions",
                model: this.dbConnector.getConnection().models[ProcessedPositionModel.tableName],
                where: {
                    state_position: {
                        [Op.in]: possibleStatePositions,
                    },
                },
            });
        }

        return include;
    };

    private determinePossibleStatePositions = (
        includeNotTracking: boolean | undefined,
        includeNotPublic: boolean | undefined
    ): StatePositionEnum[] => {
        const possibleStatePosition = [StatePositionEnum.AT_STOP, StatePositionEnum.ON_TRACK];
        if (includeNotTracking) {
            possibleStatePosition.push(
                StatePositionEnum.AFTER_TRACK,
                StatePositionEnum.AFTER_TRACK_DELAYED,
                StatePositionEnum.BEFORE_TRACK,
                StatePositionEnum.BEFORE_TRACK_DELAYED,
                StatePositionEnum.CANCELED,
                StatePositionEnum.OFF_TRACK
            );
        }
        if (includeNotPublic) {
            possibleStatePosition.push(StatePositionEnum.NOT_PUBLIC);
        }

        return possibleStatePosition;
    };
}
