import { PG_SCHEMA } from "#sch/const";
import { ProcessedPositionModel } from "#sch/vehicle-positions/models/views";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import { ModelStatic } from "@golemio/core/dist/shared/sequelize";

export class ProcessedPositionRepository extends SequelizeModel {
    public sequelizeModel!: ModelStatic<ProcessedPositionModel>;

    constructor() {
        super("ProcessedPositionRepository", ProcessedPositionModel.tableName, ProcessedPositionModel.attributeModel, {
            schema: PG_SCHEMA,
        });
    }

    public GetAll(): never {
        throw new Error("Not implemented");
    }

    public GetOne(): never {
        throw new Error("Not implemented");
    }
}
