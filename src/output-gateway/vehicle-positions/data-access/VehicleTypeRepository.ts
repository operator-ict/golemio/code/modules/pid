import { PG_SCHEMA } from "#sch/const";
import { VehiclePositions } from "#sch/vehicle-positions";
import { VehicleTypeDto } from "#sch/vehicle-positions/models/VehicleTypeDto";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";

export class VehicleTypeRepository extends SequelizeModel {
    constructor() {
        super(
            VehiclePositions.vehicleTypes.name + "Repository",
            VehiclePositions.vehicleTypes.pgTableName,
            VehicleTypeDto.attributeModel,
            {
                schema: PG_SCHEMA,
            }
        );
    }

    public GetAll(): never {
        throw new Error("Not implemented");
    }

    public GetOne(): never {
        throw new Error("Not implemented");
    }
}
