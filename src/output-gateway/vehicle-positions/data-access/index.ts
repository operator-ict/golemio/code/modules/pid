import { PositionRepository } from "./PositionRepository";
import { TripRepository } from "./TripRepository";
import { VehicleDescriptorRepository } from "./VehicleDescriptorRepository";
import { VehicleTypeRepository } from "./VehicleTypeRepository";
import { IVPRepositoryInstances } from "./interfaces/IVPRepositoryInstances";
import { ProcessedPositionRepository } from "./views/ProcessedPositionRepository";
import { TripWithLastPositionRepository } from "./views/TripWithLastPositionRepository";

const repositories: IVPRepositoryInstances = {
    positionRepository: new PositionRepository(),
    tripRepository: new TripRepository(),
    vehicleTypeRepository: new VehicleTypeRepository(),
    vehicleDescriptorRepository: new VehicleDescriptorRepository(),
    tripWithLastPositionRepository: new TripWithLastPositionRepository(),
    processedPositionRepository: new ProcessedPositionRepository(),
};

for (const type of Object.keys(repositories)) {
    const model = repositories[type as keyof IVPRepositoryInstances];
    if (model.hasOwnProperty("Associate")) {
        model.Associate(repositories);
    }
}

export { repositories };
