import { PG_SCHEMA } from "#sch/const";
import { VPTripsModel } from "#sch/vehicle-positions/models";
import { SequelizeModel } from "@golemio/core/dist/output-gateway/models";
import { ModelStatic } from "@golemio/core/dist/shared/sequelize";

export class TripRepository extends SequelizeModel {
    public sequelizeModel!: ModelStatic<VPTripsModel>;

    constructor() {
        super("VPTripsRepository", VPTripsModel.TABLE_NAME, VPTripsModel.attributeModel, {
            schema: PG_SCHEMA,
        });

        // some attributes should never be consumed by API users
        this.sequelizeModel.removeAttribute("last_position_context");
        this.sequelizeModel.removeAttribute("provider_source_type");
        this.sequelizeModel.removeAttribute("gtfs_direction_id");
        this.sequelizeModel.removeAttribute("gtfs_shape_id");
    }

    public GetAll(): never {
        throw new Error("Not implemented");
    }

    public GetOne(): never {
        throw new Error("Not implemented");
    }
}
