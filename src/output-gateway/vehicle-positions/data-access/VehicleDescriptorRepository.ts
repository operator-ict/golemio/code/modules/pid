import { PG_SCHEMA } from "#sch/const";
import { DescriptorModel } from "#sch/vehicle-descriptors/models/DescriptorModel";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";

export class VehicleDescriptorRepository extends SequelizeModel {
    constructor() {
        super("VehicleDescriptorRepository", DescriptorModel.tableName, DescriptorModel.attributeModel, {
            schema: PG_SCHEMA,
        });
    }

    public GetAll(): never {
        throw new Error("Not implemented");
    }

    public GetOne(): never {
        throw new Error("Not implemented");
    }
}
