import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import { ITripWithPositionRepository } from "./ITripWithPositionRepository";

export interface IVPRepositoryInstances {
    positionRepository: SequelizeModel;
    tripRepository: SequelizeModel;
    vehicleTypeRepository: SequelizeModel;
    vehicleDescriptorRepository: SequelizeModel;
    tripWithLastPositionRepository: ITripWithPositionRepository;
    processedPositionRepository: SequelizeModel;
}
