import { IGeoJSONFeature, SequelizeModel } from "@golemio/core/dist/output-gateway";
import { FeatureCollection } from "@golemio/core/dist/shared/geojson";

export interface ITripGetAllOutput {
    data: FeatureCollection;
    metadata: {
        maxUpdatedAt: string;
    };
}

export interface ITripWithPositionRepository extends SequelizeModel {
    GetAll(options: {
        cisTripNumber?: number;
        routeId?: string;
        routeShortName?: string;
        tripId?: string;
        includeNotTracking?: boolean;
        includeNotPublic?: boolean;
        limit?: number;
        offset?: number;
        updatedSince?: Date | null;
        preferredTimezone: string;
    }): Promise<ITripGetAllOutput>;
    GetOneByGTFSTripId(
        id: string,
        options: { includeNotTracking?: boolean; includePositions?: boolean; preferredTimezone: string }
    ): Promise<IGeoJSONFeature | null>;
}
