import { InfotextPriority } from "#og/pid/domain/InfotextPriorityEnum";
import { InfotextSeverityLevel } from "#og/shared/constants/jis/InfotextSeverityLevelEnum";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";

export class PriorityMapper {
    /**
     * Map input severity level from DB to output priority for API
     *
     * @param input Severity level type from DB
     * @returns Priority for API
     */
    public static mapInputToOutputPriority(input: InfotextSeverityLevel): InfotextPriority {
        switch (input) {
            case InfotextSeverityLevel.Info:
                return InfotextPriority.Low;
            case InfotextSeverityLevel.Warning:
                return InfotextPriority.Normal;
            case InfotextSeverityLevel.Severe:
                return InfotextPriority.High;
            default:
                throw new GeneralError(`Unknown severity level: ${input}`);
        }
    }
}
