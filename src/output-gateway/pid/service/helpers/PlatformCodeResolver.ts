import { GTFSRouteTypeEnum } from "#helpers/RouteTypeEnums";
import { ITransferDeparture } from "#og/pid";

export class PlatformCodeResolver {
    /**
     * Platform code is determined by the following rules:
     *  - for trains, the last stop platform code is used (CIS) if available
     *    - otherwise, the departure platform code is used (GTFS)
     *  - for other route types, the departure platform code is used (GTFS)
     */
    public static resolve(departure: ITransferDeparture): string | null {
        if (departure.route_type === GTFSRouteTypeEnum.TRAIN && departure["trip.cis_stop_platform_code"] !== null) {
            return departure["trip.cis_stop_platform_code"];
        }

        return departure.platform_code;
    }
}
