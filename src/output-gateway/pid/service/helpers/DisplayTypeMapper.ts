import { InfotextDisplayType } from "#og/pid/domain/InfotextDisplayTypeEnum";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";

export class DisplayTypeMapper {
    /**
     * Map input display type from DB to output display type for API
     *
     * @param input Display type from DB
     * @returns Display type for API
     */
    public static mapInputToOutputDisplayType(input: string): InfotextDisplayType {
        switch (input) {
            case "INLINE":
                return InfotextDisplayType.Inline;
            case "GENERAL":
                return InfotextDisplayType.General;
            default:
                throw new GeneralError(`Unknown display type: ${input}`);
        }
    }
}
