import { IInfotextOverviewOutputDto } from "#og/pid/domain/InfotextInterfaces";
import { RopidRouterUtils } from "#og/shared";
import { JISInfotextsModel } from "#sch/jis/models/JISInfotextsModel";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { DisplayTypeMapper } from "../helpers/DisplayTypeMapper";
import { PriorityMapper } from "../helpers/PriorityMapper";

@injectable()
export class JISInfotextOverviewTransformation extends AbstractTransformation<JISInfotextsModel, IInfotextOverviewOutputDto> {
    public name = "JISInfotextOverviewTransformation";

    protected transformInternal = (inputDto: JISInfotextsModel) => {
        const stops = inputDto.stops ?? [];
        return {
            priority: PriorityMapper.mapInputToOutputPriority(inputDto.severity_level),
            display_type: DisplayTypeMapper.mapInputToOutputDisplayType(inputDto.display_type),
            text: inputDto.description_text.cs,
            text_en: inputDto.description_text.en ?? null,
            related_stops: stops.map((stop) => ({
                id: stop.stop_id,
                name: stop.stop_name,
                platform_code: stop.platform_code ?? null,
            })),
            valid_from:
                RopidRouterUtils.formatTimestamp(inputDto.active_period_start) ?? inputDto.active_period_start.toISOString(),
            valid_to: RopidRouterUtils.formatTimestamp(inputDto.active_period_end),
            id: inputDto.id,
        };
    };
}
