import { IInfotextTransferOutputDto } from "#og/pid/domain/InfotextInterfaces";
import { JISInfotextsModel } from "#sch/jis/models/JISInfotextsModel";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { DisplayTypeMapper } from "../helpers/DisplayTypeMapper";

@injectable()
export class JISInfotextTransferTransformation extends AbstractTransformation<JISInfotextsModel, IInfotextTransferOutputDto> {
    public name = "JISInfotextTransferTransformation";

    protected transformInternal = (infotext: JISInfotextsModel) => {
        return {
            display_type: DisplayTypeMapper.mapInputToOutputDisplayType(infotext.display_type),
            text: infotext.description_text.cs,
            text_en: infotext.description_text.en ?? null,
        };
    };
}
