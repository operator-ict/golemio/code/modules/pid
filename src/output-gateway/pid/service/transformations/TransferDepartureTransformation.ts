import { ITransferDeparture, ITransferOutputDto } from "#og/pid";
import { DepartureCalculator } from "#og/shared/DepartureCalculator";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { PlatformCodeResolver } from "../helpers/PlatformCodeResolver";

@injectable()
export class TransferDepartureTransformation extends AbstractTransformation<ITransferDeparture, ITransferOutputDto> {
    public name = "TransferDepartureTransformation";

    protected transformInternal = (departure: ITransferDeparture) => {
        return {
            departure_timestamp: {
                minutes: this.calculateDepartureMinutes(departure.departure_datetime_real),
            },
            route: {
                short_name: departure.route_short_name,
                type: departure.route_type,
            },
            stop: {
                platform_code: PlatformCodeResolver.resolve(departure),
            },
            trip: {
                headsign: departure.stop_headsign ?? departure.trip_headsign,
                id: departure.trip_id,
            },
        };
    };

    private calculateDepartureMinutes(predicted: Date | null): string | null {
        if (!predicted) {
            return null;
        }

        const departure_minutes = DepartureCalculator.getDepartureMinutes(predicted);
        if (departure_minutes < 1) {
            return "<1";
        }

        return departure_minutes.toString();
    }
}
