import { IInfotextDepartureInputDto, IInfotextDepartureOutputDto } from "#og/pid/domain/InfotextInterfaces";
import { RopidRouterUtils } from "#og/shared";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { DisplayTypeMapper } from "../helpers/DisplayTypeMapper";

@injectable()
export class JISInfotextDepartureTransformation extends AbstractTransformation<
    IInfotextDepartureInputDto,
    IInfotextDepartureOutputDto
> {
    public name = "JISInfotextDepartureTransformation";

    protected transformInternal = ({ data, timeZone }: IInfotextDepartureInputDto) => {
        const stops = data.stops ?? [];
        return {
            display_type: DisplayTypeMapper.mapInputToOutputDisplayType(data.display_type),
            text: data.description_text.cs,
            text_en: data.description_text.en ?? null,
            related_stops: stops.map((stop) => stop.stop_id),
            valid_from:
                RopidRouterUtils.formatTimestamp(data.active_period_start, timeZone) ?? data.active_period_start.toISOString(),
            valid_to: RopidRouterUtils.formatTimestamp(data.active_period_end, timeZone),
        };
    };
}
