import { JISInfotextRepository } from "#og/pid/data-access/JISInfotextRepository";
import {
    IInfotextDepartureOutputDto,
    IInfotextOverviewOutputDto,
    IInfotextTransferOutputDto,
} from "#og/pid/domain/InfotextInterfaces";
import { JISInfotextStopSuppressionFilter } from "#og/pid/helpers/JISInfotextStopSuppressionFilter";
import { OgPidToken } from "#og/pid/ioc/OgPidToken";
import { IGetDeparturesOptions } from "#og/pid/models/interfaces/IGetDeparturesOptions";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { Moment } from "@golemio/core/dist/shared/moment-timezone";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { JISInfotextDepartureTransformation } from "../transformations/JISInfotextDepartureTransformation";
import { JISInfotextOverviewTransformation } from "../transformations/JISInfotextOverviewTransformation";
import { JISInfotextTransferTransformation } from "../transformations/JISInfotextTransferTransformation";

@injectable()
export class InfotextFacade {
    constructor(
        @inject(OgPidToken.JISInfotextRepository) private infotextRepository: JISInfotextRepository,
        @inject(OgPidToken.JISInfotextDepartureTransformation)
        private infotextDepartureTransformation: JISInfotextDepartureTransformation,
        @inject(OgPidToken.JISInfotextTransferTransformation)
        private infotextTransferTransformation: JISInfotextTransferTransformation,
        @inject(OgPidToken.JISInfotextOverviewTransformation)
        private infotextOverviewTransformation: JISInfotextOverviewTransformation,
        @inject(OgPidToken.JISInfotextStopSuppressionFilter) private infotextStopFilter: JISInfotextStopSuppressionFilter
    ) {}

    public async getInfotextsForDepartureBoards(
        stopIds: string[],
        currentMoment: Moment,
        options: IGetDeparturesOptions
    ): Promise<IInfotextDepartureOutputDto[]> {
        try {
            const infotexts = await this.infotextRepository.findAllForDepartureBoard(
                stopIds,
                (options.timeFrom ?? currentMoment).toDate()
            );

            if (infotexts.length === 0) {
                return [];
            }

            const filteredInfotexts = this.infotextStopFilter.filterBySeverityLevel(infotexts);
            if (filteredInfotexts.length === 0) {
                return [];
            }

            const infotextsToInclude: IInfotextDepartureOutputDto[] = [];
            for (const infotext of filteredInfotexts) {
                const infotextDto = this.infotextDepartureTransformation.transformElement({
                    data: infotext,
                    timeZone: options.timezone,
                });

                infotextsToInclude.push(infotextDto);
            }

            return infotextsToInclude;
        } catch (error) {
            if (error instanceof AbstractGolemioError) {
                throw error;
            }

            throw new GeneralError("Failed to retrieve infotexts for departure boards", this.constructor.name, error, 500);
        }
    }

    public async getInfotextsForTransferBoards(
        stopIds: string[],
        currentMoment: Moment,
        timeFrom: Moment | null
    ): Promise<IInfotextTransferOutputDto[]> {
        try {
            const infotexts = await this.infotextRepository.findAllForDepartureBoard(
                stopIds,
                timeFrom ? timeFrom.toDate() : currentMoment.toDate()
            );

            if (infotexts.length === 0) {
                return [];
            }

            const filteredInfotexts = this.infotextStopFilter.filterBySeverityLevel(infotexts);
            if (filteredInfotexts.length === 0) {
                return [];
            }

            return this.infotextTransferTransformation.transformArray(filteredInfotexts);
        } catch (error) {
            if (error instanceof AbstractGolemioError) {
                throw error;
            }

            throw new GeneralError("Failed to retrieve infotexts for transfer boards", this.constructor.name, error, 500);
        }
    }

    public async getInfotextsForOverview(): Promise<IInfotextOverviewOutputDto[]> {
        try {
            const infotexts = await this.infotextRepository.findAllForOverview();
            if (infotexts.length === 0) {
                return [];
            }

            const filteredInfotexts = this.infotextStopFilter.filterBySeverityLevel(infotexts);
            if (filteredInfotexts.length === 0) {
                return [];
            }

            return this.infotextOverviewTransformation.transformArray(filteredInfotexts);
        } catch (error) {
            if (error instanceof AbstractGolemioError) {
                throw error;
            }

            throw new GeneralError("Failed to retrieve infotexts for overview", this.constructor.name, error, 500);
        }
    }
}
