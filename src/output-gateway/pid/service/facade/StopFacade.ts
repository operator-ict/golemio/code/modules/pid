import { OgPidToken } from "#og/pid/ioc/OgPidToken";
import { CisStopGroupRepository } from "#og/ropid-gtfs/data-access/CisStopGroupRepository";
import { models } from "#og/ropid-gtfs/models";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class StopFacade {
    constructor(@inject(OgPidToken.CisStopGroupRepository) private cisStopGroupsRepository: CisStopGroupRepository) {}

    /**
     * Retrieves ASW node ID for given CIS ID and then returns GTFS stop IDs for this ASW node
     */
    public async getStopIdsForTransferBoards(cisId: string): Promise<string[]> {
        //#region CIS stop node
        let aswNode: string;
        try {
            const cisStopNode = await this.cisStopGroupsRepository.getNodeByCisId(cisId);
            if (cisStopNode === null) {
                return [];
            }

            aswNode = cisStopNode;
        } catch (error) {
            if (error instanceof AbstractGolemioError) {
                throw error;
            }

            throw new GeneralError("Failed to retrieve CIS stop node", this.constructor.name, error, 500);
        }
        //#endregion

        //#region GTFS stop IDs
        let stopIds: string[] = [];
        try {
            stopIds = await models.GTFSStopModel.getMultipleIdsByAswNode(aswNode);
        } catch (error) {
            if (error instanceof AbstractGolemioError) {
                throw error;
            }

            throw new GeneralError("Failed to retrieve GTFS stops", this.constructor.name, error, 500);
        }
        //#endregion

        return stopIds;
    }
}
