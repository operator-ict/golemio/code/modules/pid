import { DepartureFilter, DepartureMode, DepartureOrder, DepartureSkip, ITransferDeparture, ITransferOutputDto } from "#og/pid";
import { DeparturesRepository } from "#og/pid/data-access";
import { OgPidToken } from "#og/pid/ioc/OgPidToken";
import PIDDeparturesModel from "#og/pid/models/helpers/PIDDepartureModel";
import IPIDDepartureQueryOptions from "#og/pid/models/helpers/interfaces/IPIDDepartureQueryOptions";
import { RopidRouterUtils } from "#og/shared";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { Moment } from "@golemio/core/dist/shared/moment-timezone";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { TransferDepartureTransformation } from "../transformations/TransferDepartureTransformation";

@injectable()
export class TransferFacade {
    constructor(
        @inject(OgPidToken.DeparturesRepository) private departureRepository: DeparturesRepository,
        @inject(OgPidToken.TransferDepartureTransformation) private departureTransformation: TransferDepartureTransformation
    ) {}

    /**
     * Retrieves transfer departures for given GTFS stop IDs
     *  - Get transfer departures from DB
     *  - Process them (filter, sort, etc.)
     *  - Transform them to DTO
     */
    public async getTransferDepartures(
        stopIds: string[],
        tripNumber: string | null,
        currentMoment: Moment,
        minutesOffset: number,
        timeZone = RopidRouterUtils.TIMEZONE
    ): Promise<ITransferOutputDto[]> {
        let departureEntities: ITransferDeparture[] = [];
        try {
            departureEntities = await this.departureRepository.getTransferDepartures({
                stopsIds: stopIds,
                currentMoment,
                minutesOffset,
            });
        } catch (error) {
            if (error instanceof AbstractGolemioError) {
                throw error;
            }

            throw new GeneralError("Failed to retrieve transfer departures", this.constructor.name, error, 500);
        }

        if (departureEntities.length === 0) {
            return [];
        }

        let transferDepartures: ITransferOutputDto[] = [];
        try {
            const processedDepartures = new PIDDeparturesModel(departureEntities, {
                ...this.defaultOptions,
                timezone: timeZone,
                tripNumber,
            }).processAndReturnTransfers();

            transferDepartures = this.departureTransformation.transformArray(processedDepartures);
        } catch (error) {
            if (error instanceof AbstractGolemioError) {
                throw error;
            }

            throw new GeneralError("Failed to process transfer departures", this.constructor.name, error, 500);
        }

        return transferDepartures;
    }

    /**
     * Fallback options as defined by ROPID
     */
    private get defaultOptions(): Omit<IPIDDepartureQueryOptions, "timezone" | "tripNumber"> {
        return {
            limit: 16,
            total: 16,
            offset: 0,
            mode: DepartureMode.DEPARTURES,
            order: DepartureOrder.REAL,
            filter: DepartureFilter.ROUTE_HEADING_ONCE_NOGAP_FILL,
            skip: [DepartureSkip.CANCELED],
            departuresDirections: [],
            runScheduleMap: null, // not used in this context
            untrackedTrips: null, // not used in this context
        };
    }
}
