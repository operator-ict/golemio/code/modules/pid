import { PG_SCHEMA } from "#sch/const";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { QueryTypes } from "@golemio/core/dist/shared/sequelize";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { DepartureMode, IPIDDeparture, ITransferDeparture } from "..";
import { IDeparturesViewOptions, ITimestampOptions, ITransfersViewOptions } from "./interfaces/DepartureInterfaces";

const ONE_MINUTE_IN_MILLIS = 60000;

@injectable()
export class DeparturesRepository {
    private schema: string;
    private isAirConditioningFeatureEnabled: boolean;

    constructor(
        @inject(CoreToken.PostgresConnector) private connector: IDatabaseConnector,
        @inject(CoreToken.SimpleConfig) config: ISimpleConfig
    ) {
        this.schema = PG_SCHEMA;
        this.isAirConditioningFeatureEnabled = config.getBoolean("env.VEHICLE_POSITIONS_IS_AIR_COND_ENABLED", true);
    }

    GetOne(id: any): Promise<any> {
        throw new Error("Method not implemented.");
    }

    /** Retrieves all departures
     *
     * @param {IDeparturesViewOptions} options - All query options
     * @returns {Promise<IPIDDeparture[]>} Array of the retrieved records
     */
    async GetAll(options: IDeparturesViewOptions): Promise<IPIDDeparture[]> {
        const { start, end, canceledStart, canceledEnd } = this.getDepartureTimestamps(options);
        const result = await this.connector.getConnection().query<IPIDDeparture>(
            `
            SELECT * from ${this.schema}.get_departures(
                $stopsids,
                $mode,
                $datedeparturebetweenstart,
                $datedeparturebetweenend,
                $isnegativeminutesbefore,
                $datecanceleddeparturebetweenstart,
                $datecanceleddeparturebetweenend,
                $airconditioninfoenabled);`,
            {
                type: QueryTypes.SELECT,
                bind: {
                    stopsids: options.stopsIds.join(","),
                    mode: options.mode === DepartureMode.ARRIVALS ? 2 : options.mode === DepartureMode.MIXED ? 3 : 1,
                    datedeparturebetweenstart: start,
                    datedeparturebetweenend: end,
                    isnegativeminutesbefore: options.minutesBefore < 0 ? 1 : 0,
                    datecanceleddeparturebetweenstart: canceledStart,
                    datecanceleddeparturebetweenend: canceledEnd,
                    airconditioninfoenabled: this.isAirConditioningFeatureEnabled && options.isAirCondition ? 1 : 0,
                },
            }
        );

        return result;
    }

    /** Retrieves transfer departures
     *
     * @param {ITransfersViewOptions} options - All query options
     * @returns {Promise<ITransferDeparture[]>} Array of the retrieved records
     */
    public async getTransferDepartures(options: ITransfersViewOptions): Promise<ITransferDeparture[]> {
        // Always fall back to 0 minutes before and 60 minutes after
        const minutesBefore = 0;
        const minutesAfter = 60;

        const { start, end, canceledStart, canceledEnd } = this.getDepartureTimestamps({
            currentMoment: options.currentMoment,
            minutesOffset: options.minutesOffset,
            minutesBefore,
            minutesAfter,
        });

        try {
            return await this.connector.getConnection().query<ITransferDeparture>(
                `
                select * from ${this.schema}.get_transfers(
                    $stopsids,
                    $datedeparturebetweenstart,
                    $datedeparturebetweenend,
                    $datecanceleddeparturebetweenstart,
                    $datecanceleddeparturebetweenend);`,
                {
                    type: QueryTypes.SELECT,
                    bind: {
                        stopsids: options.stopsIds.join(","),
                        datedeparturebetweenstart: start,
                        datedeparturebetweenend: end,
                        datecanceleddeparturebetweenstart: canceledStart,
                        datecanceleddeparturebetweenend: canceledEnd,
                    },
                }
            );
        } catch (error) {
            throw new GeneralError("Failed to retrieve transfer departures", this.constructor.name, error, 500);
        }
    }

    private getDepartureTimestamps(options: ITimestampOptions): {
        start: Date;
        end: Date;
        canceledStart: Date;
        canceledEnd: Date;
    } {
        const currentTime = options.currentMoment.valueOf(); // in milliseconds

        // Calculate base timestamps
        const baseStart = currentTime + (options.minutesOffset - options.minutesBefore) * ONE_MINUTE_IN_MILLIS;
        const baseEnd = currentTime + (options.minutesOffset + options.minutesAfter) * ONE_MINUTE_IN_MILLIS;

        // Calculate canceled timestamps
        const canceledBaseStart = currentTime + (options.minutesOffset - 3) * ONE_MINUTE_IN_MILLIS;
        const canceledBaseEnd = currentTime + (options.minutesOffset + 3) * ONE_MINUTE_IN_MILLIS;

        return {
            start: new Date(baseStart),
            end: new Date(baseEnd),
            canceledStart: new Date(Math.min(baseStart, canceledBaseStart)),
            canceledEnd: new Date(Math.max(baseEnd, canceledBaseEnd)),
        };
    }
}
