import { PG_SCHEMA } from "#sch/const";
import { JISInfotextsRopidGTFSStopsModel } from "#sch/jis/models/JISInfotextsRopidGTFSStopsModel";
import { ILogger } from "@golemio/core/dist/helpers";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { AbstractBasicRepository } from "@golemio/core/dist/helpers/data-access/postgres/repositories/AbstractBasicRepository";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ModelStatic } from "@golemio/core/dist/shared/sequelize";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class JISInfotextRopidGTFSStopsRepository extends AbstractBasicRepository {
    public schema = PG_SCHEMA;
    public tableName = JISInfotextsRopidGTFSStopsModel.tableName;
    public sequelizeModel: ModelStatic<JISInfotextsRopidGTFSStopsModel>;

    constructor(@inject(CoreToken.PostgresConnector) connector: IDatabaseConnector, @inject(CoreToken.Logger) logger: ILogger) {
        super(connector, logger);
        this.sequelizeModel = connector
            .getConnection()
            .define(this.tableName, JISInfotextsRopidGTFSStopsModel.attributeModel, { schema: this.schema, timestamps: false });
    }
}
