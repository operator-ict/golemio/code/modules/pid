import { GTFSStopModel } from "#og/ropid-gtfs/models/GTFSStopModel";
import { PG_SCHEMA } from "#sch/const";
import { JISInfotextsModel } from "#sch/jis/models/JISInfotextsModel";
import { ILogger } from "@golemio/core/dist/helpers";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { AbstractBasicRepository } from "@golemio/core/dist/helpers/data-access/postgres/repositories/AbstractBasicRepository";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { ModelStatic, Op } from "@golemio/core/dist/shared/sequelize";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { OgPidToken } from "../ioc/OgPidToken";
import { JISInfotextRopidGTFSStopsRepository } from "./JISInfotextRopidGTFSStopsRepository";

@injectable()
export class JISInfotextRepository extends AbstractBasicRepository {
    public schema = PG_SCHEMA;
    public tableName = JISInfotextsModel.tableName;
    private sequelizeModel: ModelStatic<JISInfotextsModel>;
    private gtfsStopRepository: GTFSStopModel;

    constructor(
        @inject(OgPidToken.JISInfotextRopidGTFSStopsRepository) infotextStopRepository: JISInfotextRopidGTFSStopsRepository,
        @inject(CoreToken.PostgresConnector) connector: IDatabaseConnector,
        @inject(CoreToken.Logger) logger: ILogger
    ) {
        super(connector, logger);
        this.sequelizeModel = connector
            .getConnection()
            .define(this.tableName, JISInfotextsModel.attributeModel, { schema: this.schema, timestamps: false });

        this.gtfsStopRepository = new GTFSStopModel();

        // associations
        this.sequelizeModel.belongsToMany(this.gtfsStopRepository.sequelizeModel, {
            through: infotextStopRepository.sequelizeModel,
            foreignKey: "infotext_id",
            otherKey: "stop_id",
            as: "stops",
        });
        this.gtfsStopRepository.sequelizeModel.belongsToMany(this.sequelizeModel, {
            through: infotextStopRepository.sequelizeModel,
            foreignKey: "stop_id",
            otherKey: "infotext_id",
        });
        infotextStopRepository.sequelizeModel.belongsTo(this.sequelizeModel, {
            targetKey: "id",
            foreignKey: "infotext_id",
        });
        infotextStopRepository.sequelizeModel.belongsTo(this.gtfsStopRepository.sequelizeModel, {
            targetKey: "stop_id",
            foreignKey: "stop_id",
        });
        this.sequelizeModel.hasMany(infotextStopRepository.sequelizeModel, {
            sourceKey: "id",
            foreignKey: "infotext_id",
        });
        this.gtfsStopRepository.sequelizeModel.hasMany(infotextStopRepository.sequelizeModel, {
            sourceKey: "stop_id",
            foreignKey: "stop_id",
        });
    }

    /**
     * Find all active infotexts for given stops and time for PID departure boards API
     *
     * @param stopsIds GTFS stop IDs for which the infotexts should be fetched
     * @param timeFrom Time from which the infotexts should be valid (default is current time)
     * @returns Active infotexts
     */
    public async findAllForDepartureBoard(stopsIds: string[], timeFrom = new Date()): Promise<JISInfotextsModel[]> {
        try {
            return await this.sequelizeModel.findAll({
                attributes: ["display_type", "active_period_start", "active_period_end", "description_text", "severity_level"],
                where: {
                    active_period_start: { [Op.lte]: timeFrom },
                    active_period_end: { [Op.or]: [{ [Op.gte]: timeFrom }, { [Op.is]: null }] },
                },
                include: [
                    {
                        model: this.gtfsStopRepository.sequelizeModel,
                        as: "stops",
                        attributes: ["stop_id"],
                        where: {
                            stop_id: { [Op.in]: stopsIds },
                        },
                        through: { attributes: [] },
                        required: true,
                    },
                ],
                order: [
                    // severity_level is an enum, see type jis_infotext_severity_level
                    ["severity_level", "DESC"],
                    ["created_timestamp", "DESC"],
                ],
            });
        } catch (err) {
            throw new GeneralError("Error while getting infotexts for departure board", this.constructor.name, err);
        }
    }

    /**
     * Find all active infotexts for given time for PID infotexts API
     *
     * @param timeFrom Time from which the infotexts should be valid (default is current time)
     * @returns Active infotexts
     */
    public async findAllForOverview(timeFrom = new Date()): Promise<JISInfotextsModel[]> {
        try {
            return await this.sequelizeModel.findAll({
                attributes: [
                    "id",
                    "display_type",
                    "active_period_start",
                    "active_period_end",
                    "description_text",
                    "severity_level",
                ],
                where: {
                    active_period_start: { [Op.lte]: timeFrom },
                    active_period_end: { [Op.or]: [{ [Op.gte]: timeFrom }, { [Op.is]: null }] },
                },
                include: [
                    {
                        model: this.gtfsStopRepository.sequelizeModel,
                        as: "stops",
                        attributes: ["stop_id", "stop_name", "platform_code"],
                        through: { attributes: [] },
                        required: true,
                    },
                ],
                order: [
                    // severity_level is an enum, see type jis_infotext_severity_level
                    ["severity_level", "DESC"],
                    ["created_timestamp", "DESC"],
                ],
            });
        } catch (err) {
            throw new GeneralError("Error while getting infotexts for overview", this.constructor.name, err);
        }
    }
}
