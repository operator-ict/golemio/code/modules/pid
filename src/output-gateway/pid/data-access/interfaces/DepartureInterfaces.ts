import type { DepartureMode } from "#og/pid";
import type { Moment } from "@golemio/core/dist/shared/moment-timezone";

export interface ITimestampOptions {
    currentMoment: Moment;
    minutesOffset: number;
    minutesBefore: number;
    minutesAfter: number;
}

export interface IDeparturesViewOptions extends ITimestampOptions {
    stopsIds: string[];
    mode: DepartureMode;
    isAirCondition: boolean;
}

export interface ITransfersViewOptions extends Pick<ITimestampOptions, "currentMoment" | "minutesOffset"> {
    stopsIds: string[];
}
