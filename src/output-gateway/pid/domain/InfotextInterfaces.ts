import { JISInfotextsModel } from "#sch/jis/models/JISInfotextsModel";
import { InfotextDisplayType } from "./InfotextDisplayTypeEnum";
import { InfotextPriority } from "./InfotextPriorityEnum";

export interface IInfotextDepartureInputDto {
    data: JISInfotextsModel;
    timeZone: string;
}

export interface IInfotextStopDto {
    id: string;
    name: string;
    platform_code: string | null;
}

export interface IInfotextDepartureOutputDto {
    display_type: InfotextDisplayType;
    text: string;
    text_en: string | null;
    related_stops: string[];
    valid_from: string;
    valid_to: string | null;
}

export interface IInfotextTransferOutputDto {
    display_type: InfotextDisplayType;
    text: string;
    text_en: string | null;
}

export interface IInfotextOverviewOutputDto {
    id: string;
    priority: InfotextPriority;
    display_type: InfotextDisplayType;
    text: string;
    text_en: string | null;
    related_stops: IInfotextStopDto[];
    valid_from: string;
    valid_to: string | null;
}
