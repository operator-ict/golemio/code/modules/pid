import type { IPIDDeparture, IStop } from "#og/pid";
import type { IInfotextDepartureOutputDto } from "#og/pid/domain/InfotextInterfaces";

export interface IDepartureBoardOutputDto {
    departures: IPIDDeparture[];
    infotexts: IInfotextDepartureOutputDto[];
    stops: IStop[];
}
