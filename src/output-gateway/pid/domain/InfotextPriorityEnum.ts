export enum InfotextPriority {
    Low = "low",
    Normal = "normal",
    High = "high",
}
