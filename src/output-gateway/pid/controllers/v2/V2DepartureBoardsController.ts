import { DepartureBoardsQueryDTO, DepartureBoardsResponseDTO } from "#og/pid/dto";
import { PIDDepartureBoardsModel } from "#og/pid/models/PIDDepartureBoardsModel";
import { RopidRouterUtils } from "#og/shared";
import { createChildSpan } from "@golemio/core/dist/monitoring/opentelemetry/trace-provider";
import { RequestHandler } from "@golemio/core/dist/shared/express";
import moment, { Moment } from "@golemio/core/dist/shared/moment-timezone";

export class V2DepartureBoardsController {
    private departureBoardsModel: PIDDepartureBoardsModel;

    constructor() {
        this.departureBoardsModel = new PIDDepartureBoardsModel();
    }

    public getDepartureBoard: RequestHandler = async (req, res, next) => {
        const query = RopidRouterUtils.mapObjectToDTOInstance(DepartureBoardsQueryDTO, req.query);
        const preferredTimezone = RopidRouterUtils.getPreferredTimezone(query.preferredTimezone);

        // datetime is valid ISO 8601 (by express validator)
        // it matches:
        // 2020-10-10T10:00:00Z
        // 2020-10-10 10:00:00.00Z
        // 2020-10-10T10:00:00+02
        // 2020-10-10T10:00:00+02:00
        const timezoneDefinedRegexp: RegExp = /[T ][\d:\.]+([zZ]|([\+-])([01]\d|2[0-3]):?([0-5]\d)?)/;
        // use timezone from ?preferredTimezone if not set in ?timeFrom
        const timeFrom: Moment | undefined = query.timeFrom
            ? query.timeFrom.match(timezoneDefinedRegexp)
                ? moment(query.timeFrom)
                : moment.tz(query.timeFrom, preferredTimezone)
            : undefined;

        const span = createChildSpan("V2DepartureBoardsController.getDepartureBoard");
        try {
            const data = await this.departureBoardsModel.getAll({
                aswIds: query.aswIds,
                cisIds: query.cisIds,
                gtfsIds: query.ids,
                names: query.names,
                minutesAfter: query.minutesAfter,
                minutesBefore: query.minutesBefore,
                timeFrom,
                mode: query.mode,
                order: query.order,
                filter: query.filter,
                skip: query.skip,
                limit: query.limit,
                offset: query.offset,
                total: query.total ?? query.limit,
                timezone: preferredTimezone,
                includeMetroTrains: query.includeMetroTrains,
                airCondition: query.airCondition,
            });
            res.status(200).send(RopidRouterUtils.mapObjectToDTOInstance(DepartureBoardsResponseDTO, data));
        } catch (err) {
            next(err);
        } finally {
            span?.end();
        }
    };
}
