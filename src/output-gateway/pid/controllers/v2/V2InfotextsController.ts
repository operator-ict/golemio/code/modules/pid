import { RequestHandler } from "@golemio/core/dist/shared/express";
import { createChildSpan } from "@golemio/core/dist/monitoring/opentelemetry/trace-provider";
import { config } from "@golemio/core/dist/output-gateway/config";
import { RopidVYMIEventsStopsModel } from "#og/pid/models";

export class V2InfotextsController {
    private readonly eventsStopsModel: RopidVYMIEventsStopsModel;

    constructor() {
        this.eventsStopsModel = new RopidVYMIEventsStopsModel();
    }

    public getInfotexts: RequestHandler = async (_req, res, next) => {
        const span = createChildSpan("V2InfotextsController.getInfotexts");

        try {
            const preferredTimezone = config.vehiclePositions.defaultTimezone;
            const data = await this.eventsStopsModel.GetAllWithRoutes(preferredTimezone);
            res.status(200).send(data);
        } catch (err) {
            next(err);
        } finally {
            span?.end();
        }
    };
}
