import { ITransferOutputDto } from "#og/pid";
import { IInfotextTransferOutputDto } from "#og/pid/domain/InfotextInterfaces";
import { OgPidToken } from "#og/pid/ioc/OgPidToken";
import { InfotextFacade } from "#og/pid/service/facade/InfotextFacade";
import { StopFacade } from "#og/pid/service/facade/StopFacade";
import { TransferFacade } from "#og/pid/service/facade/TransferFacade";
import { RopidRouterUtils } from "#og/shared/RopidRouterUtils";
import { createChildSpan } from "@golemio/core/dist/monitoring/opentelemetry/trace-provider";
import { RequestHandler } from "@golemio/core/dist/shared/express";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import moment, { Moment } from "@golemio/core/dist/shared/moment-timezone";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { Attributes, Span } from "@opentelemetry/api";
import { ParsedQs } from "@golemio/core/dist/shared/qs";

@injectable()
export class V3TransferBoardsController {
    constructor(
        @inject(OgPidToken.StopFacade) private stopFacade: StopFacade,
        @inject(OgPidToken.InfotextFacade) private infotextFacade: InfotextFacade,
        @inject(OgPidToken.TransferFacade) private transferFacade: TransferFacade
    ) {}

    public getTransferDepartures: RequestHandler = async (req, res, next) => {
        const span = createChildSpan("V3TransferBoardsController.getTransferDepartures");

        const cisId = req.query.cisId;
        const tripNumber = req.query.tripNumber ?? null;
        span?.setAttributes({ cisId, tripNumber } as Attributes);

        // Additional type check for query string parameters
        if (!this.isValidQueryStringParam(cisId)) {
            return next(this.recordBadRequestException("cisId is not valid", span));
        } else if (tripNumber && !this.isValidQueryStringParam(tripNumber)) {
            return next(this.recordBadRequestException("tripNumber is not valid", span));
        }

        const currentMoment = moment();
        let timeFrom: Moment | null = null;
        let minutesOffset = 0;

        if (req.query.timeFrom) {
            timeFrom = moment.tz(req.query.timeFrom as string, RopidRouterUtils.TIMEZONE);
            minutesOffset = timeFrom.diff(currentMoment, "minutes");
        }

        try {
            //#region GTFS stops
            let stopIds: string[] = [];
            try {
                stopIds = await this.stopFacade.getStopIdsForTransferBoards(cisId);
            } catch (error) {
                span?.recordException(error);
                throw error;
            }
            //#endregion

            if (stopIds.length === 0) {
                return res.status(404).json({
                    departures: [],
                    infotexts: [],
                });
            }

            //#region Transfer departures and infotexts
            let departuresToInclude: ITransferOutputDto[] = [];
            let infotextsToInclude: IInfotextTransferOutputDto[] = [];
            try {
                const [departures, infotexts] = await Promise.all([
                    this.transferFacade.getTransferDepartures(stopIds, tripNumber, currentMoment, minutesOffset),
                    this.infotextFacade.getInfotextsForTransferBoards(stopIds, currentMoment, timeFrom),
                ]);

                departuresToInclude = departures;
                infotextsToInclude = infotexts;
            } catch (error) {
                span?.recordException(error);
                throw error;
            }
            //#endregion

            return res.status(200).json({
                departures: departuresToInclude,
                infotexts: infotextsToInclude,
            });
        } catch (err) {
            next(err);
        } finally {
            span?.end();
        }
    };

    private recordBadRequestException = (errorMessage: string, span: Span | undefined): GeneralError => {
        const exception = new GeneralError(errorMessage, this.constructor.name, undefined, 400);
        span?.recordException(exception);
        return exception;
    };

    private isValidQueryStringParam = (param: ParsedQs[string]): param is string => {
        return typeof param === "string";
    };
}
