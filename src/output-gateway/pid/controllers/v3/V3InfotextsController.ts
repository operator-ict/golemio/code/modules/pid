import { OgPidToken } from "#og/pid/ioc/OgPidToken";
import { InfotextFacade } from "#og/pid/service/facade/InfotextFacade";
import { createChildSpan } from "@golemio/core/dist/monitoring/opentelemetry/trace-provider";
import { RequestHandler } from "@golemio/core/dist/shared/express";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class V3InfotextsController {
    constructor(@inject(OgPidToken.InfotextFacade) private infotextFacade: InfotextFacade) {}

    public getJisInfotexts: RequestHandler = async (_req, res, next) => {
        const span = createChildSpan("V3InfotextsController.getJisInfotexts");

        try {
            const outputDto = await this.infotextFacade.getInfotextsForOverview();
            return res.status(200).send(outputDto);
        } catch (err) {
            span?.recordException(err);
            next(err);
        } finally {
            span?.end();
        }
    };
}
