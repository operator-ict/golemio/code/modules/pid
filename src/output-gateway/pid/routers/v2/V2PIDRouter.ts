/**
 * Router /WEB LAYER/: maps routes to specific controller functions, passes request parameters and handles responses.
 * Handles web logic (http request, response). Sets response headers, handles error responses.
 */

import { V2DepartureBoardsController } from "#og/pid/controllers/v2/V2DepartureBoardsController";
import { V2InfotextsController } from "#og/pid/controllers/v2/V2InfotextsController";
import { ParamValidatorManager } from "#og/pid/helpers/ParamValidatorManager";
import { RouteVersion, ValidationArrays } from "#og/shared/constants";
import { AbstractRouter } from "@golemio/core/dist/helpers/routing/AbstractRouter";
import { CacheHeaderMiddleware } from "@golemio/core/dist/output-gateway/CacheHeaderMiddleware";
import { checkErrors, pagination, paginationLimitMiddleware } from "@golemio/core/dist/output-gateway/Validation";
import { ContainerToken, OutputGatewayContainer } from "@golemio/core/dist/output-gateway/ioc";
import { Router } from "@golemio/core/dist/shared/express";
import { oneOf, query } from "@golemio/core/dist/shared/express-validator";
import { DATA_RETENTION_IN_MINUTES, GTFS_CALENDAR_LIMIT_IN_MINUTES, PG_INT_MAX, PG_INT_MIN } from "src/const";

export class V2PIDRouter extends AbstractRouter {
    public readonly router: Router = Router();
    private cacheHeaderMiddleware: CacheHeaderMiddleware;
    private readonly departureBoardsController: V2DepartureBoardsController;
    private readonly infotextsController: V2InfotextsController;

    constructor() {
        super(RouteVersion.v2, "pid");
        this.cacheHeaderMiddleware = OutputGatewayContainer.resolve<CacheHeaderMiddleware>(ContainerToken.CacheHeaderMiddleware);
        this.departureBoardsController = new V2DepartureBoardsController();
        this.infotextsController = new V2InfotextsController();
        this.initRoutes();
    }

    protected initRoutes = (): void => {
        this.registerDepartureBoardsRoutes();
        this.registerInfotextsRoutes();
    };

    private registerDepartureBoardsRoutes = () => {
        const {
            preferredTimezone,
            departureBoards: { departureMode, order, filter, skip },
        } = ValidationArrays;
        const validation = [
            query("aswIds").optional(),
            query("cisIds").optional(),
            query("ids").optional(),
            query("names").optional(),
            oneOf([
                query("aswIds").exists().custom(ParamValidatorManager.getAswIdsValidator()),
                query("cisIds").exists().isInt({ min: PG_INT_MIN, max: PG_INT_MAX }),
                query("ids").exists().not().isEmpty({ ignore_whitespace: true }),
                query("names").exists().not().isEmpty({ ignore_whitespace: true }),
            ]),
            query("minutesBefore")
                .optional()
                .isInt(
                    process.env.NODE_ENV === "test"
                        ? {}
                        : { lt: DATA_RETENTION_IN_MINUTES + 1, gt: -1 * (GTFS_CALENDAR_LIMIT_IN_MINUTES + 1) }
                )
                .not()
                .isArray(),
            query("minutesAfter")
                .optional()
                .isInt({
                    lt: GTFS_CALENDAR_LIMIT_IN_MINUTES + 1,
                    gt: -1 * (GTFS_CALENDAR_LIMIT_IN_MINUTES + DATA_RETENTION_IN_MINUTES + 1),
                })
                .not()
                .isArray(),
            query("preferredTimezone").optional().isIn(preferredTimezone).not().isArray(),
            query("mode").optional().isIn(departureMode),
            query("order").optional().isIn(order).not().isArray(),
            query("filter").optional().isIn(filter).not().isArray(),
            query("skip").optional().isIn(skip),
            query("timeFrom").optional().isISO8601().custom(ParamValidatorManager.getTimeFromValidator()).not().isArray(),
            query("includeMetroTrains").optional().isBoolean().not().isArray(),
            query("airCondition").optional().isBoolean().not().isArray(),
            query("total").optional().isInt().not().isArray(),
        ];

        this.router.get(
            "/departureboards",
            validation,
            ...this.commonMiddleware("PIDDepartureBoards"),
            // max-age 5 seconds, stale-while-revalidate 5 seconds
            this.cacheHeaderMiddleware.getMiddleware(5, 5),
            this.departureBoardsController.getDepartureBoard
        );
    };

    private registerInfotextsRoutes = () => {
        this.router.get(
            "/infotexts",
            ...this.commonMiddleware("PIDInfotexts"),
            // max-age 25 seconds, stale-while-revalidate 10 seconds
            this.cacheHeaderMiddleware.getMiddleware(25, 10),
            this.infotextsController.getInfotexts
        );
    };

    private commonMiddleware = (name: string) => [pagination, checkErrors, paginationLimitMiddleware(name)];
}

const v2PidRouter: AbstractRouter = new V2PIDRouter();
export { v2PidRouter };
