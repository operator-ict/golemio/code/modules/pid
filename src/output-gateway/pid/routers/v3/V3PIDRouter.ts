import { V3InfotextsController } from "#og/pid/controllers/v3/V3InfotextsController";
import { V3TransferBoardsController } from "#og/pid/controllers/v3/V3TransferBoardsController";
import { ParamValidatorManager } from "#og/pid/helpers/ParamValidatorManager";
import { OgPidContainer } from "#og/pid/ioc/Di";
import { OgPidToken } from "#og/pid/ioc/OgPidToken";
import { RouteVersion } from "#og/shared/constants";
import { AbstractRouter } from "@golemio/core/dist/helpers/routing/AbstractRouter";
import { CacheHeaderMiddleware } from "@golemio/core/dist/output-gateway/CacheHeaderMiddleware";
import { checkErrors } from "@golemio/core/dist/output-gateway/Validation";
import { ContainerToken } from "@golemio/core/dist/output-gateway/ioc";
import { Router } from "@golemio/core/dist/shared/express";
import { query } from "@golemio/core/dist/shared/express-validator";
import { PG_INT_MAX, PG_INT_MIN } from "src/const";

export class V3PIDRouter extends AbstractRouter {
    public readonly router = Router();
    private readonly cacheHeaderMiddleware: CacheHeaderMiddleware;
    private readonly transferBoardsController: V3TransferBoardsController;
    private readonly infotextsController: V3InfotextsController;

    constructor() {
        super(RouteVersion.v3, "pid");
        this.cacheHeaderMiddleware = OgPidContainer.resolve<CacheHeaderMiddleware>(ContainerToken.CacheHeaderMiddleware);
        this.transferBoardsController = OgPidContainer.resolve<V3TransferBoardsController>(OgPidToken.V3TransferBoardsController);
        this.infotextsController = OgPidContainer.resolve<V3InfotextsController>(OgPidToken.V3InfotextsController);
        this.initRoutes();
    }

    protected initRoutes = (): void => {
        this.registerInfotextsRoutes();
    };

    private registerInfotextsRoutes = () => {
        this.router.get(
            "/transferboards",
            [
                query("cisId").exists().isInt({ min: PG_INT_MIN, max: PG_INT_MAX }).not().isArray(),
                query("tripNumber").optional().isInt({ min: 0 }).not().isArray(),
                query("timeFrom").optional().isISO8601().custom(ParamValidatorManager.getTimeFromValidator()).not().isArray(),
            ],
            checkErrors,
            // max-age 5 seconds, stale-while-revalidate 5 seconds
            this.cacheHeaderMiddleware.getMiddleware(5, 5),
            this.transferBoardsController.getTransferDepartures
        );

        this.router.get(
            "/infotexts",
            checkErrors,
            // max-age 10 seconds, stale-while-revalidate 10 seconds
            this.cacheHeaderMiddleware.getMiddleware(10, 10),
            this.infotextsController.getJisInfotexts
        );
    };
}

const v3PidRouter: AbstractRouter = new V3PIDRouter();
export { v3PidRouter };
