export const OgPidToken = {
    //#region Repositories
    CisStopGroupRepository: Symbol(),
    JISInfotextRopidGTFSStopsRepository: Symbol(),
    JISInfotextRepository: Symbol(),
    DeparturesRepository: Symbol(),
    RunTripsRedisRepository: Symbol(),
    PublicVehiclePositionsRepository: Symbol(),
    //#endregion

    //#region Transformations
    JISInfotextDepartureTransformation: Symbol(),
    JISInfotextTransferTransformation: Symbol(),
    JISInfotextOverviewTransformation: Symbol(),
    TransferDepartureTransformation: Symbol(),
    //#endregion

    //#region Facade
    StopFacade: Symbol(),
    InfotextFacade: Symbol(),
    TransferFacade: Symbol(),
    //#endregion

    //#region Controllers
    V3TransferBoardsController: Symbol(),
    V3InfotextsController: Symbol(),
    //#endregion

    //#region Helpers
    JISInfotextStopSuppressionFilter: Symbol(),
    //#endregion
};
