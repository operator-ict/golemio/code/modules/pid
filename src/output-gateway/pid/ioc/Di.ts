import { PublicVehiclePositionsRepository } from "#og/public/data-access/redis/PublicVehiclePositionsRepository";
import { CisStopGroupRepository } from "#og/ropid-gtfs/data-access/CisStopGroupRepository";
import { RunTripsRedisRepository } from "#og/ropid-gtfs/data-access/redis/RunTripsRedisRepository";
import { OutputGatewayContainer } from "@golemio/core/dist/output-gateway/ioc";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import { V3InfotextsController } from "../controllers/v3/V3InfotextsController";
import { V3TransferBoardsController } from "../controllers/v3/V3TransferBoardsController";
import { DeparturesRepository } from "../data-access";
import { JISInfotextRepository } from "../data-access/JISInfotextRepository";
import { JISInfotextRopidGTFSStopsRepository } from "../data-access/JISInfotextRopidGTFSStopsRepository";
import { JISInfotextStopSuppressionFilter } from "../helpers/JISInfotextStopSuppressionFilter";
import { InfotextFacade } from "../service/facade/InfotextFacade";
import { StopFacade } from "../service/facade/StopFacade";
import { TransferFacade } from "../service/facade/TransferFacade";
import { JISInfotextDepartureTransformation } from "../service/transformations/JISInfotextDepartureTransformation";
import { JISInfotextOverviewTransformation } from "../service/transformations/JISInfotextOverviewTransformation";
import { JISInfotextTransferTransformation } from "../service/transformations/JISInfotextTransferTransformation";
import { TransferDepartureTransformation } from "../service/transformations/TransferDepartureTransformation";
import { OgPidToken } from "./OgPidToken";

const ogPidContainer: DependencyContainer = OutputGatewayContainer.createChildContainer();

//#region Repositories
ogPidContainer.registerSingleton(OgPidToken.CisStopGroupRepository, CisStopGroupRepository);
ogPidContainer.registerSingleton(OgPidToken.JISInfotextRopidGTFSStopsRepository, JISInfotextRopidGTFSStopsRepository);
ogPidContainer.registerSingleton(OgPidToken.JISInfotextRopidGTFSStopsRepository, JISInfotextRopidGTFSStopsRepository);
ogPidContainer.registerSingleton(OgPidToken.JISInfotextRepository, JISInfotextRepository);
ogPidContainer.registerSingleton(OgPidToken.DeparturesRepository, DeparturesRepository);
ogPidContainer.registerSingleton(OgPidToken.RunTripsRedisRepository, RunTripsRedisRepository);
ogPidContainer.registerSingleton(OgPidToken.PublicVehiclePositionsRepository, PublicVehiclePositionsRepository);
//#endregion

//#region Transformations
ogPidContainer.registerSingleton(OgPidToken.JISInfotextDepartureTransformation, JISInfotextDepartureTransformation);
ogPidContainer.registerSingleton(OgPidToken.JISInfotextTransferTransformation, JISInfotextTransferTransformation);
ogPidContainer.registerSingleton(OgPidToken.JISInfotextOverviewTransformation, JISInfotextOverviewTransformation);
ogPidContainer.registerSingleton(OgPidToken.TransferDepartureTransformation, TransferDepartureTransformation);
//#endregion

//#region Facade
ogPidContainer.registerSingleton(OgPidToken.StopFacade, StopFacade);
ogPidContainer.registerSingleton(OgPidToken.InfotextFacade, InfotextFacade);
ogPidContainer.registerSingleton(OgPidToken.TransferFacade, TransferFacade);
//#endregion

//#region Controllers
ogPidContainer.registerSingleton(OgPidToken.V3TransferBoardsController, V3TransferBoardsController);
ogPidContainer.registerSingleton(OgPidToken.V3InfotextsController, V3InfotextsController);
//#endregion

//#region Helpers
ogPidContainer.registerSingleton(OgPidToken.JISInfotextStopSuppressionFilter, JISInfotextStopSuppressionFilter);
//#endregion

export { ogPidContainer as OgPidContainer };
