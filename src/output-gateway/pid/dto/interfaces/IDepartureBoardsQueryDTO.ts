import { DepartureMode, DepartureFilter, DepartureOrder, DepartureSkip } from "#og/pid";

export interface IDepartureBoardsQueryDTO {
    aswIds: string[];
    cisIds: string[];
    ids: string[];
    names: string[];
    skip: DepartureSkip[];
    limit: number;
    minutesAfter: number;
    minutesBefore: number;
    includeMetroTrains: boolean;
    airCondition: boolean;
    mode: DepartureMode;
    order: DepartureOrder;
    filter: DepartureFilter;
    offset: number;
    total?: number;
    preferredTimezone?: string;
    timeFrom?: string;
}
