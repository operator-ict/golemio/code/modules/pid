import { DepartureFilter, DepartureMode, DepartureOrder, DepartureSkip, IPIDDepartureOutput } from "#og/pid";
import { IGTFSStopGetAllOutput } from "#og/ropid-gtfs/models/GTFSStopModelInterfaces";
import { TransformArray, TransformBoolean, TransformInteger } from "#og/shared/decorators";
import { IInfotextDepartureOutputDto } from "../domain/InfotextInterfaces";
import { IDepartureBoardsQueryDTO } from "./interfaces/IDepartureBoardsQueryDTO";

export class DepartureBoardsQueryDTO implements IDepartureBoardsQueryDTO {
    @TransformArray()
    aswIds: string[] = [];

    @TransformArray()
    cisIds: string[] = [];

    @TransformArray()
    ids: string[] = [];

    @TransformArray()
    names: string[] = [];

    @TransformArray()
    skip: DepartureSkip[] = [];

    @TransformInteger()
    limit: number = 20;

    @TransformInteger()
    minutesAfter: number = 180;

    @TransformInteger()
    minutesBefore: number = 0;

    @TransformBoolean()
    includeMetroTrains: boolean = false;

    @TransformBoolean()
    airCondition: boolean = true;

    mode: DepartureMode = DepartureMode.DEPARTURES;

    order: DepartureOrder = DepartureOrder.REAL;

    filter: DepartureFilter = DepartureFilter.NONE;

    @TransformInteger()
    offset: number = 0;

    @TransformInteger()
    total?: number;

    preferredTimezone?: string;

    timeFrom?: string;
}

export class DepartureBoardsResponseDTO {
    stops: IGTFSStopGetAllOutput[] = [];
    departures: IPIDDepartureOutput[] = [];
    infotexts: IInfotextDepartureOutputDto[] = [];
}
