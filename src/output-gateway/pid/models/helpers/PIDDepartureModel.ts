import {
    DepartureFilter,
    DepartureMode,
    DepartureOrder,
    DepartureSkip,
    DepartureUnion,
    IPIDDeparture,
    ITransferDeparture,
} from "#og/pid";
import DepartureBoardMapper from "#og/pid/helpers/DepartureBoardMapper";
import { AccumulatorCondition, FilterHelper } from "./FilterHelper";
import { SkipHelper } from "./SkipHelper";
import IPIDDepartureQueryOptions from "./interfaces/IPIDDepartureQueryOptions";

export default class PIDDeparturesModel {
    private departures: DepartureUnion[];
    private options: IPIDDepartureQueryOptions;

    private static FilterConditionMap: Record<DepartureFilter, AccumulatorCondition | undefined> = {
        routeOnce: FilterHelper.routeOnceCondition,
        routeOnceFill: FilterHelper.routeOnceCondition,
        routeHeadingOnce: FilterHelper.routeOnceHeadingCondition,
        routeHeadingOnceFill: FilterHelper.routeOnceHeadingCondition,
        routeHeadingOnceNoGap: FilterHelper.routeOnceHeadingNoGapCondition,
        routeHeadingOnceNoGapFill: FilterHelper.routeOnceHeadingNoGapCondition,
        none: undefined,
    };

    constructor(departures: DepartureUnion[], options: IPIDDepartureQueryOptions) {
        this.departures = departures;
        this.options = options;
    }

    public toArray = (): IPIDDeparture[] => {
        this.toArrayInternal();

        return DepartureBoardMapper.mapDepartures(this, this.options.timezone, this.options.mode);
    };

    public processAndReturnTransfers(): ITransferDeparture[] {
        this.toArrayInternal(false);

        // ITransferDeparture is a subset of IPIDDeparture
        return this.departures as ITransferDeparture[];
    }

    private toArrayInternal = (shouldIncludeDirections = true) => {
        this.skip();
        this.sort();
        this.filterAndLimit();
        this.sort();

        if (shouldIncludeDirections) {
            this.addDirections();
        }
    };

    /** Orders departures in special way by given options
     */
    private filterAndLimit = (): void => {
        const finalDepartures = this.filterDepartures();

        if (FilterHelper.isFillFilterType(this.options.filter)) {
            // return all once in limit and if still under limit fill it with others
            this.departures = finalDepartures.once
                .slice(0, this.options.total)
                .concat(
                    finalDepartures.fill.slice(0, this.options.total - Math.min(finalDepartures.once.length, this.options.total))
                )
                .slice(this.options.offset, this.options.offset + this.options.limit);
        } else {
            this.departures = finalDepartures.once
                .slice(0, this.options.total)
                .slice(this.options.offset, this.options.offset + this.options.limit);
        }
    };

    /** Skips departures by given options
     */
    private skip = (): void => {
        this.departures = this.departures.filter((departure) => {
            if (this.options.skip.length === 0) {
                return true;
            }

            let isSkipUntracked = false;
            let isSkipMissing = false;
            let isSkipCanceled = false;
            let isSkipAtStop = false;

            for (const skip of this.options.skip) {
                if (skip === DepartureSkip.UNTRACKED) {
                    isSkipUntracked = true;
                } else if (skip === DepartureSkip.MISSING) {
                    isSkipMissing = true;
                } else if (skip === DepartureSkip.CANCELED) {
                    isSkipCanceled = true;
                } else if (skip === DepartureSkip.AT_STOP) {
                    isSkipAtStop = true;
                }
            }

            return !(
                (this.options.tripNumber && SkipHelper.isMatchingTripNumber(departure, this.options.tripNumber)) ||
                (isSkipUntracked && SkipHelper.hasNoDelayInfo(departure)) ||
                (!isSkipUntracked &&
                    isSkipMissing &&
                    SkipHelper.isVehicleMissing(departure, this.options.runScheduleMap, this.options.untrackedTrips)) ||
                (isSkipCanceled && SkipHelper.isTripCanceled(departure)) ||
                (isSkipAtStop && SkipHelper.isVehicleAtStop(departure))
            );
        });
    };

    /** Sorts by proper datetime by given options
     */
    private sort = (): void => {
        const datetimeToOrder =
            this.options.mode === DepartureMode.ARRIVALS
                ? this.options.order === DepartureOrder.REAL
                    ? "arrival_datetime_real"
                    : "arrival_datetime"
                : this.options.order === DepartureOrder.REAL
                ? "departure_datetime_real"
                : "departure_datetime";

        this.departures = this.departures.sort((a: any, b: any) => a[datetimeToOrder].getTime() - b[datetimeToOrder].getTime());
    };

    /** Fills direction with proper value based on departure direction ruls
     */
    private addDirections = (): void => {
        this.departures.forEach((departure) => {
            if (!("arrival_datetime" in departure)) {
                return;
            }

            departure.direction = departure.direction || null;
            const thisStopDeparturesDirections = this.options.departuresDirections.filter(
                (departuresDirection) => departuresDirection.departure_stop_id === departure.stop_id
            );
            if (thisStopDeparturesDirections.length > 0) {
                for (const thisStopDeparturesDirection of thisStopDeparturesDirections) {
                    if (
                        departure.stop_id === thisStopDeparturesDirection.departure_stop_id &&
                        departure.next_stop_id?.match(thisStopDeparturesDirection.next_stop_id_regexp)
                    ) {
                        departure.direction = thisStopDeparturesDirection.direction;
                    }
                }
            }
        });
    };

    private filterDepartures() {
        const filterCondition = PIDDeparturesModel.FilterConditionMap[this.options.filter];

        return filterCondition
            ? this.filterByCondition(filterCondition)
            : { once: this.departures, fill: [] as DepartureUnion[] };
    }

    private filterByCondition(conditionFunction: AccumulatorCondition) {
        return this.departures.reduce(
            (acc, currentDeparture) => {
                if (conditionFunction(acc, currentDeparture)) {
                    acc.once.push(currentDeparture);
                } else {
                    acc.fill.push(currentDeparture);
                }
                return acc;
            },
            { once: [] as DepartureUnion[], fill: [] as DepartureUnion[] }
        );
    }
}
