export class RunHelper {
    public static composeRunId(routeId: string, runNumber: number): string {
        // Route ID is guaranteed to start with the "L" letter
        return `${routeId.slice(1)}_${runNumber}`;
    }
}
