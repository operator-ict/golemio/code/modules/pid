import { DepartureFilter, DepartureMode, DepartureOrder, DepartureSkip } from "#og/pid";
import { IRopidDeparturesDirectionsOutput } from "#sch/ropid-departures-directions";
import { IScheduleDto } from "#sch/vehicle-positions/redis/interfaces/IGtfsRunTripCacheDto";

export default interface IPIDDepartureQueryOptions {
    limit: number;
    offset: number;
    total: number;
    mode: DepartureMode;
    order: DepartureOrder;
    filter: DepartureFilter;
    skip: DepartureSkip[];
    departuresDirections: IRopidDeparturesDirectionsOutput[];
    timezone: string;
    tripNumber: string | null;
    runScheduleMap: Map<string, IScheduleDto[]> | null;
    untrackedTrips: Set<string> | null;
}
