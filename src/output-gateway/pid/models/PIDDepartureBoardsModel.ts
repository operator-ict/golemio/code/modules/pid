import { GtfsStopLocationType } from "#helpers/StopEnums";
import { DeparturesRepository } from "#og/pid/data-access";
import { IVehiclePositionsRepository } from "#og/public/domain/repository/IVehiclePositionsRepository";
import { IRunTripsRedisRepository } from "#og/ropid-gtfs/domain/repository/IRunTripsRedisRepository";
import { models } from "#og/ropid-gtfs/models";
import { IGTFSStopGetAllOutput } from "#og/ropid-gtfs/models/GTFSStopModelInterfaces";
import { IRopidDeparturesDirectionsOutput } from "#sch/ropid-departures-directions";
import { IScheduleDto } from "#sch/vehicle-positions/redis/interfaces/IGtfsRunTripCacheDto";
import { ILogger } from "@golemio/core/dist/helpers";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { createChildSpan } from "@golemio/core/dist/monitoring/opentelemetry/trace-provider";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import moment, { Moment } from "@golemio/core/dist/shared/moment-timezone";
import { RopidDeparturesDirectionsModel } from ".";
import { DepartureSkip, IPIDDeparture } from "..";
import { IDepartureBoardOutputDto } from "../domain/DepartureInterfaces";
import { IInfotextDepartureOutputDto } from "../domain/InfotextInterfaces";
import { OgPidContainer } from "../ioc/Di";
import { OgPidToken } from "../ioc/OgPidToken";
import { InfotextFacade } from "../service/facade/InfotextFacade";
import PIDDeparturesModel from "./helpers/PIDDepartureModel";
import { RunHelper } from "./helpers/RunHelper";
import { IGetDeparturesOptions } from "./interfaces/IGetDeparturesOptions";

const STOPS_MAX_COUNT = 100;

// TODO refactor - split into facade and multiple service/strategy classes
// Check V3TransferBoardsController and TransferFacade for inspiration
class PIDDepartureBoardsModel {
    private readonly log: ILogger;
    private readonly ropidDeparturesDirectionsModel: RopidDeparturesDirectionsModel;
    private readonly departuresRepository: DeparturesRepository;
    private readonly runTripsRedisRepository: IRunTripsRedisRepository;
    private readonly positionsRedisRepository: IVehiclePositionsRepository;
    private readonly infotextFacade: InfotextFacade;

    constructor() {
        this.log = OgPidContainer.resolve<ILogger>(CoreToken.Logger);
        this.ropidDeparturesDirectionsModel = new RopidDeparturesDirectionsModel();
        this.departuresRepository = new DeparturesRepository(
            OgPidContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector),
            OgPidContainer.resolve<ISimpleConfig>(CoreToken.SimpleConfig)
        );
        this.runTripsRedisRepository = OgPidContainer.resolve<IRunTripsRedisRepository>(OgPidToken.RunTripsRedisRepository);
        this.positionsRedisRepository = OgPidContainer.resolve<IVehiclePositionsRepository>(
            OgPidToken.PublicVehiclePositionsRepository
        );
        this.infotextFacade = OgPidContainer.resolve<InfotextFacade>(OgPidToken.InfotextFacade);
    }

    public async getAll(options: IGetDeparturesOptions): Promise<IDepartureBoardOutputDto> {
        const currentMoment = moment();

        //#region Data retrieval from DB (GTFS stops, infotexts, directions, departures)
        const { stopsToInclude, stopIds } = await this.retrieveAndReturnStops(options);
        const [infotextsToInclude, directions, departures] = await Promise.all([
            this.retrieveAndReturnInfotexts(stopIds, currentMoment, options),
            this.retrieveAndReturnDirections(stopIds),
            this.retrieveAndReturnDepartures(stopIds, currentMoment, options),
        ]);
        //#endregion

        //#region Data retrieval from Redis (run schedule, untracked trips)
        let runScheduleMap = new Map<string, IScheduleDto[]>();
        let untrackedTrips = new Set<string>();

        const isMissingRequested =
            options.skip.includes(DepartureSkip.MISSING) && !options.skip.includes(DepartureSkip.UNTRACKED);

        // Note: The run schedule and untracked trips are populated only if the skip[]=missing (without untracked) is present
        // Check the PIDDeparturesModel.skip function. In other cases they are not needed
        if (isMissingRequested) {
            const runTuples = new Set<string>();

            for (const departure of departures) {
                if (!departure.route_id || !departure.run_number) {
                    continue;
                }

                runTuples.add(RunHelper.composeRunId(departure.route_id, departure.run_number));
            }

            const { runSchedule, runTripIds } = await this.retrieveAndReturnRunTrips(runTuples);

            runScheduleMap = runSchedule;
            untrackedTrips = await this.retrieveAndReturnUntrackedTrips(runTripIds);
        }
        //#endregion

        //#region Processing and returning departures
        const spanProcessing = createChildSpan("V2PIDRouter.Processing");
        try {
            const departuresToInclude = new PIDDeparturesModel(departures, {
                limit: options.limit,
                offset: options.offset,
                total: options.total,
                mode: options.mode,
                order: options.order,
                filter: options.filter,
                skip: options.skip,
                departuresDirections: directions,
                timezone: options.timezone,
                tripNumber: null, // not used in this context
                runScheduleMap,
                untrackedTrips,
            }).toArray();

            return {
                departures: departuresToInclude,
                infotexts: infotextsToInclude,
                stops: stopsToInclude,
            };
        } catch (error) {
            spanProcessing?.recordException(error);

            if (error instanceof AbstractGolemioError) {
                throw error;
            }

            throw new GeneralError("Error while processing departures", this.constructor.name, error, 500);
        } finally {
            spanProcessing?.end();
        }
        //#endregion
    }

    private async retrieveAndReturnStops(
        options: IGetDeparturesOptions
    ): Promise<{ stopsToInclude: IGTFSStopGetAllOutput[]; stopIds: string[] }> {
        const spanStops = createChildSpan("V2PIDRouter.DB.stops");
        spanStops?.setAttributes({ ...options, timeFrom: options.timeFrom?.toString() });

        try {
            const stopsToInclude = await models.GTFSStopModel.GetAll({
                includeMetroTrains: options.includeMetroTrains,
                appendAswId: true,
                aswIds: options.aswIds,
                cisIds: options.cisIds,
                gtfsIds: options.gtfsIds,
                limit: STOPS_MAX_COUNT + 1,
                locationType: GtfsStopLocationType.StopOrPlatform,
                names: options.names,
                returnRaw: true,
            });

            if (stopsToInclude.length === 0) {
                throw new GeneralError("No stops found.", this.constructor.name, undefined, 404);
            } else if (stopsToInclude.length > STOPS_MAX_COUNT) {
                throw new GeneralError(
                    `Too many stops, try lower number or split requests. The maximum is ${STOPS_MAX_COUNT} stops.`,
                    this.constructor.name,
                    undefined,
                    413
                );
            }

            const stopIds = stopsToInclude.map(({ stop_id }) => stop_id);
            return { stopsToInclude, stopIds };
        } catch (error) {
            spanStops?.recordException(error);

            if (error instanceof AbstractGolemioError) {
                throw error;
            }

            throw new GeneralError("Error while getting stops", this.constructor.name, error, 500);
        } finally {
            spanStops?.end();
        }
    }

    private async retrieveAndReturnInfotexts(
        stopIds: string[],
        currentMoment: Moment,
        options: IGetDeparturesOptions
    ): Promise<IInfotextDepartureOutputDto[]> {
        const spanInfotexts = createChildSpan("V2PIDRouter.DB.infotexts");
        try {
            return await this.infotextFacade.getInfotextsForDepartureBoards(stopIds, currentMoment, options);
        } catch (error) {
            spanInfotexts?.recordException(error);
            throw error;
        } finally {
            spanInfotexts?.end();
        }
    }

    private async retrieveAndReturnDirections(stopIds: string[]): Promise<IRopidDeparturesDirectionsOutput[]> {
        const spanDirections = createChildSpan("V2PIDRouter.DB.directions");
        try {
            return await this.ropidDeparturesDirectionsModel.GetAll(stopIds);
        } catch (error) {
            spanDirections?.recordException(error);

            if (error instanceof AbstractGolemioError) {
                throw error;
            }

            throw new GeneralError("Error while getting directions", this.constructor.name, error, 500);
        } finally {
            spanDirections?.end();
        }
    }

    private async retrieveAndReturnDepartures(
        stopIds: string[],
        currentMoment: Moment,
        options: IGetDeparturesOptions
    ): Promise<IPIDDeparture[]> {
        const spanDepartures = createChildSpan("V2PIDRouter.DB.departures");
        spanDepartures?.setAttributes({ stopIds });

        try {
            const minutesOffset = options.timeFrom ? options.timeFrom.diff(currentMoment, "minutes") : 0;
            return await this.departuresRepository.GetAll({
                currentMoment,
                minutesAfter: options.minutesAfter,
                minutesBefore: options.minutesBefore,
                minutesOffset,
                stopsIds: stopIds,
                mode: options.mode,
                isAirCondition: !!options.airCondition,
            });
        } catch (error) {
            spanDepartures?.recordException(error);

            if (error instanceof AbstractGolemioError) {
                throw error;
            }

            throw new GeneralError("Error while getting departures", this.constructor.name, error, 500);
        } finally {
            spanDepartures?.end();
        }
    }

    private async retrieveAndReturnRunTrips(runTuples: Set<string>): Promise<{
        runSchedule: Map<string, IScheduleDto[]>;
        runTripIds: Set<string>;
    }> {
        const runScheduleMap = new Map<string, IScheduleDto[]>();
        const tripIds = new Set<string>();
        const spanRunTrips = createChildSpan("V2PIDRouter.Redis.runTrips");

        try {
            const runTuplesArray = Array.from(runTuples);
            const runSchedule = await this.runTripsRedisRepository.getMultipleSchedule(runTuplesArray);

            for (let i = 0; i < runSchedule.length; i++) {
                const runTrip = runSchedule[i];
                if (runTrip) {
                    const tuple = runTuplesArray[i];
                    runScheduleMap.set(tuple, runTrip.schedule);

                    for (const trip of runTrip.schedule) {
                        tripIds.add(trip.trip_id);
                    }
                }
            }
        } catch (error) {
            spanRunTrips?.recordException(error);
            this.log.error(
                error,
                // eslint-disable-next-line max-len
                "Cannot retrieve run schedule. The API consumer will receive departures with missing vehicles even if they requested to skip them."
            );
        } finally {
            spanRunTrips?.end();
            return {
                runSchedule: runScheduleMap,
                runTripIds: tripIds,
            };
        }
    }

    private async retrieveAndReturnUntrackedTrips(tripIds: Set<string>): Promise<Set<string>> {
        let untrackedTrips: Set<string> = new Set();
        const spanUntrackedTrips = createChildSpan("V2PIDRouter.Redis.untrackedTrips");

        try {
            const tripIdsArray = Array.from(tripIds);
            untrackedTrips = await this.positionsRedisRepository.getTripsWithUntrackedVehicles(tripIdsArray);
        } catch (error) {
            spanUntrackedTrips?.recordException(error);
            this.log.error(
                error,
                // eslint-disable-next-line max-len
                "Cannot retrieve untracked trips. The API consumer will receive departures with missing vehicles even if they requested to skip them."
            );
        } finally {
            spanUntrackedTrips?.end();
            return untrackedTrips;
        }
    }
}

export { PIDDepartureBoardsModel };
