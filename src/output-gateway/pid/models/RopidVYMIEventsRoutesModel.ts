import { PG_SCHEMA } from "#sch/const";
import { RopidVYMI } from "#sch/ropid-vymi";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";

/**
 * Custom Postgres model for Ropid VYMI Events Routes
 */
export class RopidVYMIEventsRoutesModel extends SequelizeModel {
    GetAll(options?: any): Promise<any> {
        throw new Error("Method not implemented.");
    }
    GetOne(id: any): Promise<any> {
        throw new Error("Method not implemented.");
    }

    constructor() {
        super(
            RopidVYMI.eventsRoutes.name + "Model",
            RopidVYMI.eventsRoutes.pgTableName,
            RopidVYMI.eventsRoutes.outputSequelizeAttributes,
            { schema: PG_SCHEMA }
        );
    }
}
