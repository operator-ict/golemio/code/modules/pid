import type { DepartureFilter, DepartureMode, DepartureOrder, DepartureSkip } from "#og/pid";
import type { Moment } from "@golemio/core/dist/shared/moment-timezone";

export interface IGetDeparturesOptions {
    aswIds?: string[];
    cisIds?: string[];
    gtfsIds?: string[];
    names?: string[];
    total: number;
    limit: number;
    offset: number;
    mode: DepartureMode;
    minutesBefore: number;
    minutesAfter: number;
    timeFrom?: Moment;
    order: DepartureOrder;
    filter: DepartureFilter;
    skip: DepartureSkip[];
    includeMetroTrains?: boolean;
    airCondition?: boolean;
    timezone: string;
}
