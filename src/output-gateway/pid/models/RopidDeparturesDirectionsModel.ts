import { PG_SCHEMA } from "#sch/const";
import {
    DeparturesDirectionsEnum,
    IRopidDeparturesDirectionsOutput,
    IRopidDeparturesDirectionsSDMAOutput,
    RopidDeparturesDirections,
} from "#sch/ropid-departures-directions";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import Sequelize from "@golemio/core/dist/shared/sequelize";

/**
 * Custom Postgres model for Ropid Departures Directions
 */
export class RopidDeparturesDirectionsModel extends SequelizeModel {
    constructor() {
        super(
            RopidDeparturesDirections.departureDirections.name + "Model",
            RopidDeparturesDirections.departureDirections.pgTableName,
            RopidDeparturesDirections.departureDirections.outputSequelizeAttributes,
            { schema: PG_SCHEMA }
        );
    }

    GetOne(id: any): Promise<any> {
        throw new Error("Method not implemented.");
    }

    private convertItem = (item: IRopidDeparturesDirectionsSDMAOutput): IRopidDeparturesDirectionsOutput => {
        // converting types
        return {
            ...item,
            next_stop_id_regexp: new RegExp(item.next_stop_id_regexp),
            direction: item.direction as DeparturesDirectionsEnum,
        } as IRopidDeparturesDirectionsOutput;
    };

    private convertItems = (items: IRopidDeparturesDirectionsSDMAOutput[]): IRopidDeparturesDirectionsOutput[] => {
        return items.map((item) => this.convertItem(item));
    };

    async GetAll(stopsIds: string[]): Promise<IRopidDeparturesDirectionsOutput[]> {
        if (!stopsIds || stopsIds.length === 0) {
            return [] as IRopidDeparturesDirectionsOutput[];
        }
        return this.convertItems(
            (await this.sequelizeModel.findAll({
                where: {
                    departure_stop_id: {
                        [Sequelize.Op.in]: stopsIds,
                    },
                },
                order: [
                    ["departure_stop_id", "asc"],
                    ["rule_order", "asc"],
                ],
                raw: true,
            })) as IRopidDeparturesDirectionsSDMAOutput[]
        );
    }
}
