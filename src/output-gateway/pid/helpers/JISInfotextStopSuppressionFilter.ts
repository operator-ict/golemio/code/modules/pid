import { JISInfotextsModel } from "#sch/jis/models/JISInfotextsModel";
import { injectable } from "tsyringe";

const MAX_SEVERITY_LEVEL = 2;

@injectable()
export class JISInfotextStopSuppressionFilter {
    public filterBySeverityLevel(list: JISInfotextsModel[]): JISInfotextsModel[] {
        const stopsBySeverity = new Map<number, Set<string>>([
            [MAX_SEVERITY_LEVEL, new Set<string>()], // SEVERE
            [1, new Set<string>()], // WARNING
            [0, new Set<string>()], // INFO
        ]);

        let filteredList: JISInfotextsModel[] = [];

        for (const infotext of list) {
            if (!infotext.stops) {
                continue;
            }

            const severity = this.mapSeverityLevelToNumber(infotext.severity_level);

            for (let i = infotext.stops.length - 1; i >= 0; i--) {
                const stop = infotext.stops[i];
                const stopId = stop.getDataValue("stop_id");

                if (severity === MAX_SEVERITY_LEVEL) {
                    stopsBySeverity.get(severity)!.add(stopId);
                    continue;
                }

                let isStopPresent = this.isStopPresentInHigherSeverityLevels(severity, stopId, stopsBySeverity);

                if (isStopPresent) {
                    infotext.stops.splice(i, 1);
                } else {
                    stopsBySeverity.get(severity)!.add(stopId);
                }
            }

            if (infotext.stops.length > 0) {
                filteredList.push(infotext);
            }
        }

        return filteredList;
    }

    private isStopPresentInHigherSeverityLevels(
        currentSeverity: number,
        stopId: string,
        stopsBySeverity: Map<number, Set<string>>
    ): boolean {
        for (let i = currentSeverity + 1; i <= MAX_SEVERITY_LEVEL; i++) {
            if (stopsBySeverity.get(i)!.has(stopId)) {
                return true;
            }
        }

        return false;
    }

    private mapSeverityLevelToNumber(severityLevel: string): number {
        switch (severityLevel) {
            case "SEVERE":
                return MAX_SEVERITY_LEVEL;
            case "WARNING":
                return 1;
            case "INFO":
            default:
                return 0;
        }
    }
}
