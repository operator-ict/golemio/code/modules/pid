import { GtfsStopWheelchairBoardingEnum, GtfsTripWheelchairAccessEnum } from "#helpers/AccessibilityEnums";
import { GTFSRouteTypeEnum } from "#helpers/RouteTypeEnums";
import { RopidRouterUtils } from "#og/shared";
import { DepartureCalculator } from "#og/shared/DepartureCalculator";
import { log } from "@golemio/core/dist/output-gateway";
import { DepartureMode, IDepartureTime, IPIDDeparture, IPIDDepartureOutput } from "..";
import { PlatformCodeResolver } from "../service/helpers/PlatformCodeResolver";

export default class DepartureBoardMapper {
    public static mapDepartures(data: any, preferredTimezone: string, mode: string) {
        const result = data.departures.map((d: IPIDDeparture) => this.transformResponseData(d, preferredTimezone, mode));

        return result;
    }

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     */
    private static transformResponseData = (x: IPIDDeparture, preferredTimezone: string, mode?: string): IPIDDepartureOutput => {
        let isAtStop = x.is_delay_available ? x.stop_sequence === x["trip.last_position.this_stop_sequence"] : false;

        const arrivalTimestamp: IDepartureTime = {
            // with added delay
            predicted: RopidRouterUtils.formatTimestamp(x.arrival_datetime_real, preferredTimezone),
            // according to trip plan
            scheduled: RopidRouterUtils.formatTimestamp(x.arrival_datetime, preferredTimezone),
        };
        const departureTimestamp: IDepartureTime = {
            // with added delay
            predicted: RopidRouterUtils.formatTimestamp(x.departure_datetime_real, preferredTimezone),
            // according to trip plan
            scheduled: RopidRouterUtils.formatTimestamp(x.departure_datetime, preferredTimezone),
            // minutes till departure. if less than 30 sec return "<1"
            minutes: this.calculateDepartureMinutes(x.departure_datetime_real),
        };

        // default value
        mode = (mode as DepartureMode) || DepartureMode.DEPARTURES;
        // set null timestamps for first stop of the trip for departures and mixed mode
        if ((mode === DepartureMode.DEPARTURES || mode === DepartureMode.MIXED) && x.stop_sequence === x.min_stop_sequence) {
            arrivalTimestamp.predicted = null;
            arrivalTimestamp.scheduled = null;
        }

        // set null timestamps for last stop of the trip for arrivals and mixed mode
        if ((mode === DepartureMode.ARRIVALS || mode === DepartureMode.MIXED) && x.stop_sequence === x.max_stop_sequence) {
            departureTimestamp.predicted = null;
            departureTimestamp.scheduled = null;
        }

        return {
            arrival_timestamp: arrivalTimestamp,
            delay: {
                is_available: x.is_delay_available,
                minutes: x.delay_minutes, // TBD - now it could be negative!
                seconds: x.delay_seconds, // TBD - now it could be negative!
            },
            departure_timestamp: departureTimestamp,
            last_stop: {
                id: x["trip.last_position.last_stop_id"],
                name: x["trip.last_position.last_stop_name"],
            },
            route: {
                short_name: x.route_short_name,
                type: x.route_type,
                is_night: x.is_night === "1",
                is_regional: x.is_regional === "1",
                is_substitute_transport: x.is_substitute_transport === "1",
            },
            stop: {
                id: x.stop_id,
                platform_code: PlatformCodeResolver.resolve(x),
            },
            trip: {
                direction: x.direction,
                headsign: x.stop_headsign ?? x.trip_headsign,
                id: x.trip_id,
                is_at_stop: isAtStop,
                is_canceled: x.is_canceled || false,
                is_wheelchair_accessible: this.determineWheelchairAccessibility(x),
                is_air_conditioned: x["trip.vehicle_descriptor.is_air_conditioned"] ?? null,
                short_name: x.trip_short_name || null,
            },
        };
    };

    private static calculateDepartureMinutes(predicted: Date | null) {
        if (!predicted) return null;
        if (!(predicted instanceof Date)) {
            log.warn(`Unknown type of predicted date: ${predicted}`);
            return null;
        }

        const departure_minutes = DepartureCalculator.getDepartureMinutes(predicted);
        if (departure_minutes < 1) {
            return "<1";
        }

        return departure_minutes.toString();
    }

    private static determineWheelchairAccessibility = (departure: IPIDDeparture): boolean => {
        const { wheelchair_accessible, wheelchair_boarding, real_wheelchair_accessible } = departure;

        if (departure["route_type"] === GTFSRouteTypeEnum.METRO) {
            switch (wheelchair_boarding) {
                case GtfsStopWheelchairBoardingEnum.AccessibleStation:
                    return true;
                case GtfsStopWheelchairBoardingEnum.InaccessibleStation:
                case GtfsStopWheelchairBoardingEnum.NoInformation:
                default:
                    return false;
            }
        } else {
            return typeof real_wheelchair_accessible === "boolean"
                ? real_wheelchair_accessible
                : wheelchair_accessible === GtfsTripWheelchairAccessEnum.AccessibleVehicle;
        }
    };
}
