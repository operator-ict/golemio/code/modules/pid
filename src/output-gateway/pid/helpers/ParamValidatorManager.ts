import { CustomValidator } from "@golemio/core/dist/shared/express-validator";
import moment from "@golemio/core/dist/shared/moment-timezone";

export class ParamValidatorManager {
    private static ASW_ID_FORMAT_REGEXP = /^[0-9]+[_\/]?[0-9]*[_\/]?$/;

    public static getAswIdsValidator = (): CustomValidator => {
        return async (value) => {
            if (!value) {
                return Promise.reject("aswIds must be defined");
            }

            const aswIds = Array.isArray(value) ? value : [value];
            for (const aswId of aswIds) {
                if (typeof aswId !== "string" || !aswId.match(ParamValidatorManager.ASW_ID_FORMAT_REGEXP)) {
                    return Promise.reject("Invalid aswIds format");
                }
            }

            return true;
        };
    };

    public static getDateValidator = (): CustomValidator => {
        return async (date) => {
            if (!date) {
                return Promise.reject("Date must be defined");
            }

            const ms = new Date(date).valueOf();

            if (ms < 0) {
                return Promise.reject("Value is out of range");
            }

            return true;
        };
    };

    public static getTimeFromValidator = (): CustomValidator => {
        const defaultTimeQueryParams = {
            hoursBefore: 6,
            hoursAfter: 2 * 24,
        };

        return async (date) => {
            if (!date) {
                return Promise.reject("Date must be defined");
            }

            const timeFrom = moment(date);
            const currentMoment = moment();

            if (
                timeFrom &&
                !timeFrom.isBetween(
                    currentMoment.clone().subtract(defaultTimeQueryParams.hoursBefore, "hours"),
                    currentMoment.clone().add(defaultTimeQueryParams.hoursAfter, "hours"),
                    undefined,
                    "[]"
                )
            ) {
                return Promise.reject("Parameter timeFrom must be in interval [-6h; +2d]");
            }
        };
    };
}
