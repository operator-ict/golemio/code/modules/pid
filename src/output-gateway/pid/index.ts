import { GtfsStopWheelchairBoardingEnum, GtfsTripWheelchairAccessEnum } from "#helpers/AccessibilityEnums";
import { GTFSRouteTypeEnum } from "#helpers/RouteTypeEnums";
import { DeparturesDirectionsEnum } from "#sch/ropid-departures-directions";

export * from "./routers";
export * from "./models";

export type DepartureUnion = IPIDDeparture | ITransferDeparture;

export interface IPIDDeparture {
    stop_sequence: number;
    stop_headsign: string | null;
    arrival_datetime: Date | null;
    departure_datetime: Date | null;
    stop_id: string;
    platform_code: string | null;
    wheelchair_boarding: GtfsStopWheelchairBoardingEnum | null;
    min_stop_sequence: number | null;
    max_stop_sequence: number | null;
    trip_id: string;
    trip_headsign: string;
    trip_short_name: string | null;
    wheelchair_accessible: GtfsTripWheelchairAccessEnum | null;
    route_short_name: string | null;
    route_type: GTFSRouteTypeEnum;
    route_id: string | null;
    is_night: string | null;
    is_regional: string | null;
    is_substitute_transport: string | null;
    next_stop_id: string | null;
    direction: DeparturesDirectionsEnum | null;
    delay_seconds: number | null;
    delay_minutes: number | null;
    is_delay_available: boolean;
    arrival_datetime_real: Date | null;
    departure_datetime_real: Date | null;
    real_wheelchair_accessible: boolean | null;
    is_canceled: boolean;
    run_number: number | null;
    "trip.start_timestamp": string | null;
    "trip.last_position.last_stop_id": string | null;
    "trip.last_position.last_stop_sequence": number | null;
    "trip.last_position.last_stop_name": string | null;
    "trip.last_position.this_stop_sequence": number | null;
    "trip.cis_stop_platform_code": string | null;
    "trip.vehicle_descriptor.is_air_conditioned"?: boolean | null;
}

export interface ITransferDeparture {
    departure_datetime_real: Date | null;
    stop_headsign: string | null;
    departure_datetime: Date | null;
    stop_id: string;
    platform_code: string | null;
    trip_id: string;
    trip_headsign: string;
    trip_short_name: string | null;
    route_short_name: string | null;
    route_type: GTFSRouteTypeEnum;
    route_id: string | null;
    is_canceled: boolean;
    "trip.cis_stop_platform_code": string | null;
    is_delay_available: boolean;
}

export interface IPIDDepartureOutput {
    arrival_timestamp: IDepartureTime;
    delay: { is_available: boolean; minutes: number | null; seconds: number | null };
    departure_timestamp: IDepartureTime;
    last_stop: { id: string | null; name: string | null };
    route: {
        short_name: string | null;
        type: number | null;
        is_night: boolean;
        is_regional: boolean;
        is_substitute_transport: boolean;
    };
    stop: { id: string; platform_code: string | null };
    trip: {
        direction: DeparturesDirectionsEnum | null;
        headsign: string;
        id: string;
        is_at_stop: boolean;
        is_canceled: boolean;
        is_wheelchair_accessible: boolean;
        is_air_conditioned: boolean | null;
        short_name: string | null;
    };
}

export interface ITransferOutputDto {
    departure_timestamp: Pick<IDepartureTime, "minutes">;
    route: {
        short_name: string | null;
        type: number | null;
    };
    stop: {
        platform_code: string | null;
    };
    trip: {
        headsign: string;
        id: string;
    };
}

export interface IStop {
    stop_id: string;
    stop_name: string;
    platform_code: string;
}

export interface IDepartureTime {
    predicted: string | null;
    scheduled: string | null;
    minutes?: string | null;
}

export enum DepartureMode {
    DEPARTURES = "departures",
    ARRIVALS = "arrivals",
    MIXED = "mixed",
}

export enum DepartureOrder {
    REAL = "real",
    TIMETABLE = "timetable",
}

export enum DepartureFilter {
    NONE = "none",
    ROUTE_ONCE = "routeOnce",
    ROUTE_HEADING_ONCE = "routeHeadingOnce",
    ROUTE_ONCE_FILL = "routeOnceFill",
    ROUTE_HEADING_ONCE_FILL = "routeHeadingOnceFill",
    ROUTE_HEADING_ONCE_NOGAP = "routeHeadingOnceNoGap",
    ROUTE_HEADING_ONCE_NOGAP_FILL = "routeHeadingOnceNoGapFill",
}

export enum DepartureSkip {
    UNTRACKED = "untracked",
    MISSING = "missing",
    CANCELED = "canceled",
    AT_STOP = "atStop",
    // ON_INFOTEXT = "onInfotext",
}
