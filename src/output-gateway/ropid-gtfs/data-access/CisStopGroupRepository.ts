import { PG_SCHEMA } from "#sch/const";
import { RopidGTFS } from "#sch/ropid-gtfs";
import { ILogger } from "@golemio/core/dist/helpers";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { AbstractBasicRepository } from "@golemio/core/dist/helpers/data-access/postgres/repositories/AbstractBasicRepository";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { ModelStatic } from "@golemio/core/dist/shared/sequelize";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class CisStopGroupRepository extends AbstractBasicRepository {
    public schema = PG_SCHEMA;
    public tableName = RopidGTFS.cis_stop_groups.pgTableName;
    public sequelizeModel: ModelStatic<any>;

    constructor(@inject(CoreToken.PostgresConnector) connector: IDatabaseConnector, @inject(CoreToken.Logger) logger: ILogger) {
        super(connector, logger);

        this.sequelizeModel = connector
            .getConnection()
            .define(this.tableName, RopidGTFS.cis_stop_groups.outputSequelizeAttributes, { schema: this.schema });
    }

    public async getNodeByCisId(cisId: string): Promise<string | null> {
        try {
            const cisStopGroup = await this.sequelizeModel.findOne({
                attributes: ["node"],
                raw: true,
                where: { cis: cisId },
            });

            if (!cisStopGroup || cisStopGroup.node === null) {
                return null;
            }

            return cisStopGroup.node;
        } catch (error) {
            throw new GeneralError("Error while getting CIS stop groups", this.constructor.name, error, 500);
        }
    }
}
