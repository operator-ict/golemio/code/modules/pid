import { IRunTripsRedisRepository } from "#og/ropid-gtfs/domain/repository/IRunTripsRedisRepository";
import { GTFS_RUN_SCHEDULE_NAMESPACE_PREFIX } from "#sch/ropid-gtfs/redis/const";
import { IGtfsRunTripCacheDto } from "#sch/vehicle-positions/redis/interfaces/IGtfsRunTripCacheDto";
import { IoRedisConnector } from "@golemio/core/dist/helpers/data-access/redis/IoRedisConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { ContainerToken } from "@golemio/core/dist/output-gateway/ioc";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class RunTripsRedisRepository implements IRunTripsRedisRepository {
    constructor(
        @inject(ContainerToken.RedisConnector) private redisConnector: IoRedisConnector,
        @inject(CoreToken.Logger) private log: ILogger
    ) {}

    public async getMultipleSchedule(runTuples: string[]): Promise<Array<IGtfsRunTripCacheDto | null>> {
        if (!runTuples.length) {
            return [];
        }

        if (!this.redisConnector.isConnected()) {
            await this.redisConnector.connect();
        }

        const connection = this.redisConnector.getConnection();
        let runTripCache: Array<string | null> = [];

        try {
            runTripCache = await connection.mget(
                runTuples.map((runTuple) => `${GTFS_RUN_SCHEDULE_NAMESPACE_PREFIX}:${runTuple}`)
            );
        } catch (error) {
            throw new GeneralError("Cannot get run trips from cache", this.constructor.name, error);
        }

        let gtfsRunTripDtos: Array<IGtfsRunTripCacheDto | null> = [];
        for (const cache of runTripCache) {
            if (cache === null) {
                this.log.info(`${this.constructor.name}: Cannot find run trip cache`);
                gtfsRunTripDtos.push(null);
                continue;
            }

            try {
                gtfsRunTripDtos.push(JSON.parse(cache));
            } catch (error) {
                throw new GeneralError("Cannot parse run trip cache", this.constructor.name, error);
            }
        }

        return gtfsRunTripDtos;
    }
}
