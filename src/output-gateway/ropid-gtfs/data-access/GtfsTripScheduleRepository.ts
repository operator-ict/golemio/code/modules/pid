import { PG_SCHEMA } from "#sch/const";
import { RopidGTFS } from "#sch/ropid-gtfs";
import { ILogger } from "@golemio/core/dist/helpers";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { AbstractBasicRepository } from "@golemio/core/dist/helpers/data-access/postgres/repositories/AbstractBasicRepository";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { OutputGatewayContainer } from "@golemio/core/dist/output-gateway/ioc/Di";
import { ModelStatic } from "@golemio/core/dist/shared/sequelize";

export class GtfsTripScheduleRepository extends AbstractBasicRepository {
    public schema = PG_SCHEMA;
    public tableName = RopidGTFS.tripSchedule.pgTableName;
    public sequelizeModel: ModelStatic<any>;

    constructor() {
        const connector = OutputGatewayContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector);
        const logger = OutputGatewayContainer.resolve<ILogger>(CoreToken.Logger);
        super(connector, logger);

        this.sequelizeModel = connector
            .getConnection()
            .define(this.tableName, RopidGTFS.tripSchedule.outputSequelizeAttributes, { schema: this.schema });
    }
}
