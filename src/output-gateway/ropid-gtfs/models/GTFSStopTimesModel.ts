import { PG_SCHEMA } from "#sch/const";
import { RopidGTFS } from "#sch/ropid-gtfs";
import { StopTimesDto } from "#sch/ropid-gtfs/models/StopTimesDto";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import { FatalError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import Sequelize, { Includeable } from "@golemio/core/dist/shared/sequelize";
import { IGTFSModels } from ".";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { ContainerToken, OutputGatewayContainer } from "@golemio/core/dist/output-gateway/ioc/";

export class GTFSStopTimesModel extends SequelizeModel {
    private dbConnector: IDatabaseConnector;
    public constructor() {
        super(RopidGTFS.stop_times.name, RopidGTFS.stop_times.pgTableName, StopTimesDto.attributeModel, {
            schema: PG_SCHEMA,
        });
        this.dbConnector = OutputGatewayContainer.resolve<IDatabaseConnector>(ContainerToken.PostgresDatabase);
    }

    public Associate = (models: IGTFSModels) => {
        this.sequelizeModel.belongsTo(models.GTFSTripsModel.sequelizeModel, {
            foreignKey: "trip_id",
        });

        this.sequelizeModel.belongsTo(models.GTFSStopModel.sequelizeModel, {
            as: "stop",
            foreignKey: "stop_id",
            targetKey: "stop_id",
        });
    };

    /** Retrieves all gtfs stop times for specific stop id
     * @param {object} options Options object with params
     * @param {string} options.stopId Filter by specific stop id
     * @param {number} [options.limit] Limit
     * @param {number} [options.offset] Offset
     * @param {string} [options.from] Filter records since specific time in the 'H:mm:ss' format
     * @param {string} [options.to] Filter records until specific time in the 'H:mm:ss' format
     * @param {string} [options.date] Filter by specific date in the 'YYYY-MM-DD' format
     * @returns Array of the retrieved records
     */
    public GetAll = async (options: {
        stopId: string;
        limit?: number;
        offset?: number;
        from?: string;
        to?: string;
        date?: string;
        stop?: boolean;
    }): Promise<any> => {
        const { limit, offset, to, from, date, stopId, stop } = options;
        const include: Includeable[] = [];
        const where: Record<"stop_id" | typeof Sequelize.Op.and, any> = {
            stop_id: stopId,
            [Sequelize.Op.and]: [],
        };

        if (from) {
            where[Sequelize.Op.and].push(GTFSStopTimesModel.arrivalTimeComparison(from, "<="));
        }

        if (to) {
            where[Sequelize.Op.and].push(GTFSStopTimesModel.arrivalTimeComparison(to, ">="));
        }

        if (date) {
            include.push({
                attributes: [],
                include: [
                    {
                        as: "service",
                        attributes: [],
                        model: this.dbConnector.getConnection().models[RopidGTFS.calendar.pgTableName].scope({
                            method: ["forDate", date],
                        }),
                    },
                ],
                model: this.dbConnector.getConnection().models[RopidGTFS.trips.pgTableName],
                required: true,
            });
        }

        if (stop) {
            include.push({
                as: "stop",
                model: this.dbConnector.getConnection().models[RopidGTFS.stops.pgTableName],
            });
        }

        try {
            return await this.sequelizeModel.findAll({
                attributes: {
                    // Exclude computed fields from the response
                    exclude: ["computed_dwell_time_seconds", "arrival_time_seconds", "departure_time_seconds", "timepoint"],
                },
                include,
                limit,
                offset,
                order: [
                    ["stop_id", "ASC"],
                    ["departure_time", "ASC"],
                ],
                where,
            });
        } catch (err) {
            throw new GeneralError("Database error", "GTFSStopTimesModel", err, 500);
        }
    };

    public GetOne = (id: any): Promise<any> => {
        throw new FatalError("Method not implemented");
    };

    private static arrivalTimeComparison = (time: string, operator: "<=" | ">="): Sequelize.Utils.Literal => {
        // arrival_time 25:45:20 -> 01:45:20
        return Sequelize.literal(`'${time}'::time ${operator} cast(arrival_time as interval)::time`);
    };
}
