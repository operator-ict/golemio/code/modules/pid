import { GtfsStopTimeDropOffType, GtfsStopTimePickupType } from "#helpers/PassengerTransferEnums";
import { PG_SCHEMA } from "#sch/const";
import { RopidGTFS } from "#sch/ropid-gtfs";
import { ITripWithOptionalAssociationsDto } from "#sch/ropid-gtfs/interfaces/ITripWithOptionalAssociationsDto";
import { TripDto } from "#sch/ropid-gtfs/models/TripDto";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { buildGeojsonFeature } from "@golemio/core/dist/output-gateway/Geo";
import { ContainerToken, OutputGatewayContainer } from "@golemio/core/dist/output-gateway/ioc/";
import { SequelizeModel } from "@golemio/core/dist/output-gateway/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import Sequelize, { Includeable } from "@golemio/core/dist/shared/sequelize";
import { GtfsTripScheduleRepository } from "../data-access/GtfsTripScheduleRepository";

export class GTFSTripsModel extends SequelizeModel {
    private dbConnector: IDatabaseConnector;
    private tripScheduleRepository: GtfsTripScheduleRepository;

    public constructor() {
        super(RopidGTFS.trips.name, RopidGTFS.trips.pgTableName, TripDto.attributeModel, {
            schema: PG_SCHEMA,
        });
        this.dbConnector = OutputGatewayContainer.resolve<IDatabaseConnector>(ContainerToken.PostgresDatabase);
        this.tripScheduleRepository = new GtfsTripScheduleRepository();
    }

    public Associate = (models: any) => {
        this.sequelizeModel.hasMany(models.GTFSStopTimesModel.sequelizeModel, {
            as: "has_stop_id",
            foreignKey: "trip_id",
        });

        this.sequelizeModel.hasMany(models.GTFSStopTimesModel.sequelizeModel, {
            as: "stop_times",
            foreignKey: "trip_id",
        });

        this.sequelizeModel.belongsTo(models.GTFSCalendarModel.sequelizeModel, {
            as: "service",
            foreignKey: "service_id",
        });

        this.sequelizeModel.belongsTo(models.GTFSRoutesModel.sequelizeModel, {
            as: "route",
            foreignKey: "route_id",
        });

        this.sequelizeModel.hasMany(models.GTFSShapesModel.sequelizeModel, {
            as: "shapes",
            foreignKey: "shape_id",
            sourceKey: "shape_id",
        });

        this.sequelizeModel.belongsToMany(models.GTFSStopModel.sequelizeModel, {
            as: "stops",
            foreignKey: "trip_id",
            otherKey: "stop_id",
            through: models.GTFSStopTimesModel.sequelizeModel,
        });

        this.sequelizeModel.hasOne(this.tripScheduleRepository.sequelizeModel, {
            as: "schedule",
            foreignKey: "trip_id",
            sourceKey: "trip_id",
            scope: {
                [Sequelize.Op.and]: [Sequelize.literal(`start_timestamp::date = CURRENT_DATE`)],
            },
        });
    };

    /** Retrieves all gtfs trips
     * @param {object} options Options object with params
     * @param {number} [options.limit] Limit
     * @param {number} [options.offset] Offset
     * @param {boolean} [options.route] Enhance response with route data
     * @param {string} [options.stopId] Filter routes by specific stop
     * @param {string} [options.date] Filter by specific date in the 'YYYY-MM-DD' format
     * @returns Array of the retrieved records
     */
    public GetAll = async (
        options: {
            limit?: number;
            offset?: number;
            stopId?: string;
            date?: string;
        } = {}
    ): Promise<any> => {
        const { limit, offset, stopId, date } = options;
        try {
            const include: any = [];
            if (stopId) {
                include.push({
                    as: "has_stop_id",
                    attributes: [],
                    duplicating: false,
                    model: this.dbConnector.getConnection().models[RopidGTFS.stop_times.pgTableName],
                    where: {
                        stop_id: stopId,
                    },
                });
            }

            if (date) {
                include.push({
                    as: "service",
                    attributes: [],
                    model: this.dbConnector
                        .getConnection()
                        .models[RopidGTFS.calendar.pgTableName].scope({ method: ["forDate", date] }),
                });
            }

            const data = await this.sequelizeModel.findAll({
                include,
                limit,
                offset,
                order: [["trip_id", "DESC"]],
            });

            return data;
        } catch (err) {
            throw new GeneralError("Database error", "GTFSTripsModel", err, 500);
        }
    };

    /** Retrieves specific GTFS trip
     * @param {string} id Id of the trip
     * @param {object} [options] Options object with params
     * @param {boolean} [options.route] Enhance response with route data
     * @param {boolean} [options.shapes] Enhance response with shape data
     * @param {boolean} [options.service] Enhance response with service data
     * @param {boolean} [options.stops] Enhance response with stop data
     * @param {boolean} [options.stopTimes] Enhance response with stop times data
     * @param {string} [options.date] Filter by specific date in the 'YYYY-MM-DD' format
     * @returns Object of the retrieved record or null
     */
    public GetOne = async (
        id: string,
        options: {
            route?: boolean;
            shapes?: boolean;
            service?: boolean;
            stops?: boolean;
            stopTimes?: boolean;
            date?: string;
        } = {}
    ): Promise<object> => {
        const { route, stopTimes, stops, shapes } = options;
        let trip;
        try {
            trip = await this.sequelizeModel.findByPk(id, { include: this.GetInclusions(options) });
        } catch (err) {
            throw new GeneralError("Database error", "GTFSTripsModel", err, 500);
        }
        return trip ? this.ConvertItem(trip, { route, stops, shapes, stopTimes }) : null;
    };

    public getOneForPublicGtfsLookup = async (
        id: string,
        options: {
            shouldIncludeStopTimes?: boolean;
            shouldIncludeShapes?: boolean;
        } = {}
    ): Promise<ITripWithOptionalAssociationsDto | null> => {
        try {
            const tripEntity = await this.sequelizeModel.findByPk(id, {
                attributes: ["trip_id", "shape_id", "trip_headsign", "wheelchair_accessible"],
                include: this.getInclusionsForPublicGtfsLookup(options),
            });
            return tripEntity ? tripEntity.toJSON() : null;
        } catch (err) {
            throw new GeneralError("Cannot get GTFS trip from database", this.constructor.name, err, 500);
        }
    };

    /** Prepare ORM query with selected params
     * @param {object} options Options object with params
     * @param {boolean} [options.route] Enhance response with route data
     * @param {boolean} [options.shapes] Enhance response with shape data
     * @param {boolean} [options.service] Enhance response with service data
     * @param {boolean} [options.stops] Enhance response with stop data
     * @param {boolean} [options.stopTimes] Enhance response with stop times data
     * @param {string} [options.date] Filter by specific date in the 'YYYY-MM-DD' format
     * @returns Array of inclusions
     */
    protected GetInclusions = (options: {
        route?: boolean;
        shapes?: boolean;
        service?: boolean;
        stops?: boolean;
        stopTimes?: boolean;
        date?: string;
    }) => {
        const { stops, stopTimes, shapes, service, route, date } = options;
        const include: Includeable[] = [];

        // stop_times and stops both selected to include, nest them together
        if (stops && stopTimes) {
            include.push({
                as: "stop_times",
                attributes: {
                    exclude: ["arrival_time_seconds", "departure_time_seconds", "timepoint"],
                },
                include: [
                    {
                        as: "stop",
                        model: this.dbConnector.getConnection().models[RopidGTFS.stops.pgTableName],
                    },
                ],
                order: [["stop_sequence", "ASC"]],
                separate: true,
                model: this.dbConnector.getConnection().models[RopidGTFS.stop_times.pgTableName],
            });
            // Only stops or only stop times selected to include
        } else {
            stops &&
                include.push({
                    as: "stops",
                    model: this.dbConnector.getConnection().models[RopidGTFS.stops.pgTableName],
                    through: { attributes: [] },
                });

            stopTimes &&
                include.push({
                    as: "stop_times",
                    attributes: {
                        exclude: ["arrival_time_seconds", "departure_time_seconds", "timepoint"],
                    },
                    order: [["stop_sequence", "ASC"]],
                    separate: true,
                    model: this.dbConnector.getConnection().models[RopidGTFS.stop_times.pgTableName],
                });
        }

        shapes &&
            include.push({
                as: "shapes",
                model: this.dbConnector.getConnection().models[RopidGTFS.shapes.pgTableName],
            });

        if (date || service) {
            include.push({
                as: "service",
                model: this.dbConnector
                    .getConnection()
                    .models[RopidGTFS.calendar.pgTableName].scope({ method: ["forDate", date] }),
            });
        }

        route &&
            include.push({
                as: "route",
                model: this.dbConnector.getConnection().models[RopidGTFS.routes.pgTableName],
            });

        return include;
    };

    private getInclusionsForPublicGtfsLookup = (options: {
        shouldIncludeStopTimes?: boolean;
        shouldIncludeShapes?: boolean;
    }): Includeable[] => {
        const { shouldIncludeStopTimes, shouldIncludeShapes } = options;
        const include: Includeable[] = [];

        include.push({
            as: "route",
            attributes: ["route_id", "route_short_name", "route_type"],
            model: this.dbConnector.getConnection().models[RopidGTFS.routes.pgTableName],
        });

        include.push({
            as: "schedule",
            attributes: ["run_number"],
            model: this.dbConnector.getConnection().models[this.tripScheduleRepository.tableName],
        });

        if (shouldIncludeStopTimes) {
            include.push({
                as: "stop_times",
                attributes: [
                    "stop_sequence",
                    "shape_dist_traveled",
                    [Sequelize.literal(`EXTRACT(EPOCH FROM "arrival_time"::INTERVAL)::int`), "arrival_time_seconds"],
                    [Sequelize.literal(`EXTRACT(EPOCH FROM "departure_time"::INTERVAL)::int`), "departure_time_seconds"],
                ],
                include: [
                    {
                        as: "stop",
                        attributes: ["stop_name", "zone_id", "stop_lon", "stop_lat", "wheelchair_boarding"],
                        required: true,
                        model: this.dbConnector.getConnection().models[RopidGTFS.stops.pgTableName],
                    },
                ],
                where: {
                    // exclude no stop waypoints
                    drop_off_type: { [Sequelize.Op.ne]: GtfsStopTimeDropOffType.NoDropOff.toString() },
                    pickup_type: { [Sequelize.Op.ne]: GtfsStopTimePickupType.NoPickup.toString() },
                },
                order: [["stop_sequence", "ASC"]],
                separate: true,
                model: this.dbConnector.getConnection().models[RopidGTFS.stop_times.pgTableName],
            });
        }

        if (shouldIncludeShapes) {
            include.push({
                as: "shapes",
                attributes: ["shape_dist_traveled", "shape_pt_lon", "shape_pt_lat"],
                model: this.dbConnector.getConnection().models[RopidGTFS.shapes.pgTableName],
            });
        }

        return include;
    };

    /**
     * Convert a single db result to proper output format with all its included sub-elements
     * @param {object} trip Trip object
     * @param {object} options Options object with params
     * @param {boolean} [options.shapes] If shapes were included in filter then convert shapes payload
     * @param {boolean} [options.stops] If stops were included in filter then convert stops payload
     * @returns A converted item of the result
     */
    protected ConvertItem = (trip: any, options: { route?: boolean; stops?: boolean; shapes?: boolean; stopTimes?: boolean }) => {
        const { stop_times: stopTimesItems = [], stops: stopItems = [], shapes: shapeItems = [], ...item } = trip.toJSON();
        return {
            ...item,
            ...(options.route && {
                route: {
                    ...item.route,
                    is_night: item.route.is_night === "1",
                    is_regional: item.route.is_regional === "1",
                    is_substitute_transport: item.route.is_substitute_transport === "1",
                },
            }),
            ...(options.stops &&
                options.stopTimes && {
                    stop_times: stopTimesItems.map((stopTime: any) => {
                        // exclude computed fields from stop time
                        const { arrival_time_seconds, departure_time_seconds, ...convertedStopTime } = stopTime;
                        convertedStopTime.stop = this.BuildResponse(stopTime.stop, "stop_lon", "stop_lat");
                        return convertedStopTime;
                    }),
                }),
            ...(options.stops &&
                !options.stopTimes && {
                    stops: stopItems.map((stop: any) => this.BuildResponse(stop, "stop_lon", "stop_lat")),
                }),
            ...(!options.stops &&
                options.stopTimes && {
                    stop_times: stopTimesItems.map((stopTime: any) => {
                        // exclude computed fields from stop time
                        const { arrival_time_seconds, departure_time_seconds, ...convertedStopTime } = stopTime;
                        return convertedStopTime;
                    }),
                }),
            ...(options.shapes && {
                shapes: shapeItems.map((shape: any) => this.BuildResponse(shape, "shape_pt_lon", "shape_pt_lat")),
            }),
        };
    };

    /**
     * Builds the correct format of a response data
     */
    protected BuildResponse = (responseObject: any, longLoc: string, latLoc: string) => {
        return buildGeojsonFeature(responseObject, longLoc, latLoc, true);
    };
}
