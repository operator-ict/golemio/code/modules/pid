import { GtfsStopLocationType } from "#helpers/StopEnums";
import { PG_SCHEMA } from "#sch/const";
import { RopidGTFS } from "#sch/ropid-gtfs";
import { StopDto } from "#sch/ropid-gtfs/models/StopDto";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import {
    buildGeojsonFeature,
    buildGeojsonFeatureCollection,
    IGeoJSONFeature,
    IGeoJSONFeatureCollection,
} from "@golemio/core/dist/output-gateway/Geo";
import { OutputGatewayContainer } from "@golemio/core/dist/output-gateway/ioc";
import { SequelizeModel } from "@golemio/core/dist/output-gateway/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import Sequelize, { FindAttributeOptions, IncludeOptions, Order, WhereOptions } from "@golemio/core/dist/shared/sequelize";
import { GetAllOptions, GetAllWhereOptions, IGTFSStopGetAllOutput, IMetroFindOptions } from "./GTFSStopModelInterfaces";
import { PG_INT_MAX, PG_INT_MIN } from "src/const";

export class GTFSStopModel extends SequelizeModel {
    private readonly outputAttributes: FindAttributeOptions & string[] = [];
    private cisStopsModel: Sequelize.ModelCtor<any>;

    public constructor() {
        super(RopidGTFS.stops.name, RopidGTFS.stops.pgTableName, StopDto.attributeModel, {
            schema: PG_SCHEMA,
        });
        const connector = OutputGatewayContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector);

        this.outputAttributes = Object.keys(StopDto.attributeModel);

        const notUsedColumns = [
            "stop_code",
            "stop_desc",
            "stop_url",
            "stop_timezone",
            "asw_node_id",
            "asw_stop_id",
            "computed_cis_stop_id",
        ];

        for (const column of notUsedColumns) {
            this.sequelizeModel.removeAttribute(column);
            this.outputAttributes.splice(this.outputAttributes.indexOf(column), 1);
        }

        this.cisStopsModel = connector
            .getConnection()
            .define(RopidGTFS.cis_stops.pgTableName, RopidGTFS.cis_stops.outputSequelizeAttributes, {
                schema: PG_SCHEMA,
            }) as Sequelize.ModelCtor<any>;
    }

    public Associate = () => {
        this.sequelizeModel.hasMany(this.sequelizeModel, {
            as: "stops_self",
            foreignKey: "asw_node_id",
            sourceKey: "asw_node_id",
        });
    };

    /**
     * Retrieve all gtfs stops
     */
    public async GetAll<T extends boolean = false>(
        options: GetAllOptions<T>
    ): Promise<T extends true ? IGTFSStopGetAllOutput[] : IGeoJSONFeatureCollection>;
    public async GetAll(options: GetAllOptions): Promise<IGTFSStopGetAllOutput[] | IGeoJSONFeatureCollection> {
        let attributes: FindAttributeOptions = [...this.outputAttributes];
        let order: Order = [];
        let where: GetAllWhereOptions = {};
        let allGtfsIds: string[] = [];

        if (options.aswIds && options.aswIds.length > 0) {
            where[Sequelize.Op.or] = this.prepareAswWhereOptions(options.aswIds);
        }

        if (options.cisIds && options.cisIds.length > 0) {
            let stops;

            try {
                stops = await this.prepareCisStops(options.cisIds);
            } catch (err) {
                throw new GeneralError("Database error", "GTFSStopModel", err, 500);
            }

            // after all stops by other than GTFS ids are collected, we create proper GTFS ids with % like sign.
            // GTFS stops must be split into more ids due to more tarriff zones even if it is one physical stop.
            for (const stop of stops) {
                allGtfsIds.push("(^U" + stop.id.replace("/", "Z") + "(P|N)?(_\\d+)?$)");
            }

            // If aswIds and cisIds are not belong to any GTFS ids and no other gtfsIds or names are given,
            // then return empty stops array
            if (
                allGtfsIds.length === 0 &&
                (options.gtfsIds === undefined || options.gtfsIds?.length === 0) &&
                (options.names === undefined || options.names.length === 0)
            ) {
                return options.returnRaw ? [] : buildGeojsonFeatureCollection([], "stop_lon", "stop_lat", true);
            }
        }

        if (options.lng && options.lat) {
            const location = Sequelize.literal(`ST_GeomFromText('POINT(${options.lng} ${options.lat})')`);
            const distance = Sequelize.fn("ST_DistanceSphere", Sequelize.literal("ST_MakePoint(stop_lon, stop_lat)"), location);

            attributes.push([distance, "distance"]);
            order.push(["distance", "ASC"]);

            if (options.range) {
                where.distance = {
                    [Sequelize.Op.lte]: options.range,
                };
            }
        }

        order.push(["stop_id", "ASC"]);

        for (const stop of options.gtfsIds ?? []) {
            allGtfsIds.push("(^" + stop + "$)");
        }

        if (allGtfsIds.length > 0) {
            where.stop_id = {
                [Sequelize.Op.regexp]: allGtfsIds.join("|"),
            };
        }

        if (options.names && options.names?.length > 0) {
            where.stop_name = options.names;
        }

        if (options.locationType !== undefined) {
            where.location_type = options.locationType;
        }

        if (options.appendAswId) {
            const aswId = Sequelize.fn(
                "json_build_object",
                "node",
                Sequelize.col(`${RopidGTFS.stops.pgTableName}.asw_node_id`),
                "stop",
                Sequelize.col(`${RopidGTFS.stops.pgTableName}.asw_stop_id`)
            );

            attributes.push([aswId, "asw_id"]);
        }

        let include: IncludeOptions | undefined = undefined;
        if (options.includeMetroTrains) {
            // Workaround for distinct selection
            // https://github.com/sequelize/sequelize/issues/2996
            attributes[0] = [
                Sequelize.literal(
                    `DISTINCT ON(${RopidGTFS.stops.pgTableName}.stop_id) ${RopidGTFS.stops.pgTableName}.${attributes[0]}`
                ),
                attributes[0] as string,
            ];

            const findOptions = this.prepareFindOptionsForMetro(where);
            include = findOptions.include;
            where = findOptions.where;
        }

        let data: IGTFSStopGetAllOutput[] = [];
        try {
            data = await this.sequelizeModel.findAll({
                attributes,
                include,
                where,
                order,
                limit: options.limit,
                offset: options.offset,
                raw: true,
                subQuery: false,
            });
        } catch (err) {
            throw new GeneralError("Database error", "GTFSStopModel", err, 500);
        }

        return options.returnRaw ? data : buildGeojsonFeatureCollection(data, "stop_lon", "stop_lat", true);
    }

    /**
     *  Retrieve multiple GTFS stop ids by ASW node id
     */
    public getMultipleIdsByAswNode = async (aswNodeId: string): Promise<string[]> => {
        try {
            const stops = await this.sequelizeModel.findAll({
                attributes: ["stop_id"],
                raw: true,
                where: {
                    asw_node_id: aswNodeId,
                    location_type: GtfsStopLocationType.StopOrPlatform,
                },
            });

            if (!stops || stops.length === 0) {
                return [];
            }

            return stops.map((stop) => stop.stop_id);
        } catch (error) {
            throw new GeneralError("Error while getting GTFS stop ids", this.constructor.name, error, 500);
        }
    };

    /**
     * Retrieve specific gtfs stop
     */
    public GetOne = async (id: string): Promise<IGeoJSONFeature | null> => {
        return this.sequelizeModel.findByPk(id).then((data) => {
            if (data) {
                return buildGeojsonFeature(data, "stop_lon", "stop_lat", true);
            }
            return null;
        });
    };

    private isValidInteger(value: number | null): boolean {
        if (value === null) return true;
        return value >= PG_INT_MIN && value <= PG_INT_MAX;
    }

    private validateAswId(aswId: string, nodeId: string, stopId?: string): void {
        const nodeIdNum = Number(nodeId);
        const stopIdNum = stopId ? Number(stopId) : null;

        if (!this.isValidInteger(nodeIdNum) || !this.isValidInteger(stopIdNum)) {
            throw new GeneralError(`Invalid ASW ID: ${aswId}`, this.constructor.name, `Invalid ASW ID: ${aswId}`, 400);
        }
    }

    private prepareAswWhereOptions(aswIds: string[]): WhereOptions[] {
        let aswWhereOptions: WhereOptions[] = [];
        for (const aswId of aswIds) {
            let aswIdLike = aswId.replaceAll("_", "/");

            if (aswIdLike.slice(-1) === "/") {
                aswIdLike = aswIdLike.slice(0, -1);
            }

            const [nodeId, stopId] = aswIdLike.split("/");

            if (!nodeId) {
                continue;
            }

            this.validateAswId(aswId, nodeId, stopId);

            aswWhereOptions.push(
                Sequelize.and(
                    Sequelize.where(Sequelize.col("asw_node_id"), "=", nodeId),
                    stopId && Sequelize.where(Sequelize.col("asw_stop_id"), "=", stopId)
                )
            );
        }

        return aswWhereOptions;
    }

    private prepareCisStops = async (cisIds: string[]) => {
        const stops = await this.cisStopsModel.findAll({
            raw: true,
            where: {
                [Sequelize.Op.or]: {
                    cis: cisIds,
                },
            },
        });

        return stops;
    };

    private prepareFindOptionsForMetro(innerWhere: GetAllWhereOptions): IMetroFindOptions {
        // Select metro and train stops within a node
        //   matching the previous where criteria
        //   matching asw_node_id (stop_id U1040Z16P -> asw_node_id 1040)
        //   with asw_stop_id greater than or equal to 100 (stop_id U1040Z101P -> asw_stop_id 101)
        //     indicating a metro or train stop
        // EX: U1040Z16P Na Knížecí + U1040Z101P Anděl
        const include: IncludeOptions = {
            as: "stops_self",
            model: this.sequelizeModel,
            attributes: [],
            on: {
                asw_node_id: {
                    [Sequelize.Op.eq]: Sequelize.col(`${RopidGTFS.stops.pgTableName}.asw_node_id`),
                },
            },
            where: innerWhere,
        };

        const where: GetAllWhereOptions = {
            [Sequelize.Op.or]: [
                innerWhere,
                {
                    asw_stop_id: { [Sequelize.Op.gte]: 100 },
                },
            ],
        };

        return { include, where };
    }
}
