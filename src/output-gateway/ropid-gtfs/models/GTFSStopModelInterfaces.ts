import { IStopDto } from "#sch/ropid-gtfs/interfaces/IStopDto";
import { IncludeOptions, WhereOptions, WhereAttributeHash } from "@golemio/core/dist/shared/sequelize";

export type GetAllOptions<T extends boolean = false> = {
    limit?: number;
    offset?: number;
    lat?: number;
    lng?: number;
    range?: number;
    names?: string[];
    gtfsIds?: string[];
    aswIds?: string[];
    cisIds?: string[];
    locationType?: number;
    includeMetroTrains?: boolean;
    appendAswId?: boolean;
    returnRaw?: T;
};

export type GetAllWhereAttributes = WhereAttributeHash<{
    distance: string;
    stop_id: string;
    stop_name: string;
    location_type: string;
    asw_stop_id: string;
}>;

export type GetAllWhereOptions = WhereOptions<GetAllWhereAttributes> & { [key: symbol]: WhereOptions[] };

export interface IMetroFindOptions {
    include: IncludeOptions;
    where: GetAllWhereOptions;
}

type AswID = { node: number; stop: number };

export interface IGTFSStopGetAllOutput extends Omit<IStopDto, "asw_node_id" | "asw_stop_id"> {
    asw_id?: AswID | null;
}
