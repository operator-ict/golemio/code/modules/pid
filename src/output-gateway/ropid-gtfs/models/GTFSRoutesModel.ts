import { PG_SCHEMA } from "#sch/const";
import { RopidGTFS } from "#sch/ropid-gtfs";
import { RouteDto } from "#sch/ropid-gtfs/models/RouteDto";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";

export class GTFSRoutesModel extends SequelizeModel {
    public constructor() {
        super(RopidGTFS.routes.name, RopidGTFS.routes.pgTableName, RouteDto.attributeModel, {
            schema: PG_SCHEMA,
        });
    }

    /** Retrieves all gtfs routes
     * @param {object} [options] Options object with params
     * @param {number} [options.limit] Limit
     * @param {number} [options.offset] Offset
     * @returns Array of the retrieved records
     */
    public GetAll = async (
        options: {
            limit?: number;
            offset?: number;
        } = {}
    ): Promise<any> => {
        const { limit, offset } = options;
        try {
            const order: any = [];
            order.push([["route_id", "asc"]]);
            const data = await this.sequelizeModel.findAll({
                limit,
                offset,
                order,
            });
            return data.map((item) => this.ConvertItem(item));
        } catch (err) {
            throw new GeneralError("Database error", "GTFSRoutesModel", err, 500);
        }
    };

    /** Retrieves specific gtfs routes
     * @param {string} id Id of the route
     * @returns Object of the retrieved record or null
     */
    public GetOne = async (id: string): Promise<any> => this.ConvertItem(await this.sequelizeModel.findByPk(id));

    /**
     * Convert a single db result to proper output format with all tranformation
     * @param {object} route Route object
     * @returns A converted item of the result
     */
    private ConvertItem = (route: any) => {
        if (route === null) return null;

        // fallback for not plain Sequelize object
        if (route.toJSON) route = route.toJSON();

        // convert to booleans
        route.is_night = route.is_night === "1";
        route.is_regional = route.is_regional === "1";
        route.is_substitute_transport = route.is_substitute_transport === "1";

        return route;
    };
}
