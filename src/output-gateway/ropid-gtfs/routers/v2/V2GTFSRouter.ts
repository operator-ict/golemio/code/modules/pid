/**
 * Router /WEB LAYER/: maps routes to specific controller functions, passes request parameters and handles responses.
 * Handles web logic (http request, response). Sets response headers, handles error responses.
 */

import { ParamValidatorManager } from "#og/pid/helpers/ParamValidatorManager";
import { models } from "#og/ropid-gtfs/models";
import { GTFSCalendarModel } from "#og/ropid-gtfs/models/GTFSCalendarModel";
import { GTFSRoutesModel } from "#og/ropid-gtfs/models/GTFSRoutesModel";
import { GTFSShapesModel } from "#og/ropid-gtfs/models/GTFSShapesModel";
import { GTFSStopModel } from "#og/ropid-gtfs/models/GTFSStopModel";
import { GTFSStopTimesModel } from "#og/ropid-gtfs/models/GTFSStopTimesModel";
import { GTFSTripsModel } from "#og/ropid-gtfs/models/GTFSTripsModel";
import { RopidRouterUtils } from "#og/shared";
import { RouteVersion } from "#og/shared/constants";
import { AbstractRouter } from "@golemio/core/dist/helpers/routing/AbstractRouter";
import { CacheHeaderMiddleware } from "@golemio/core/dist/output-gateway/CacheHeaderMiddleware";
import { parseCoordinates } from "@golemio/core/dist/output-gateway/Geo";
import { checkErrors, pagination, paginationLimitMiddleware } from "@golemio/core/dist/output-gateway/Validation";
import { ContainerToken, OutputGatewayContainer } from "@golemio/core/dist/output-gateway/ioc";
import { NextFunction, Request, Response, Router } from "@golemio/core/dist/shared/express";
import { param, query } from "@golemio/core/dist/shared/express-validator";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import moment from "@golemio/core/dist/shared/moment-timezone";

export class V2GTFSRouter extends AbstractRouter {
    // Assign router to the express.Router() instance
    public router: Router = Router();
    private cacheHeaderMiddleware: CacheHeaderMiddleware;

    protected tripModel: GTFSTripsModel;
    protected serviceModel: GTFSCalendarModel;
    protected stopModel: GTFSStopModel;
    protected routeModel: GTFSRoutesModel;
    protected shapeModel: GTFSShapesModel;
    protected stopTimeModel: GTFSStopTimesModel;

    // Reg-ex to match a valid daytime format (ex. 21:43:56)
    private timeRegex = /(([0-1][0-9])|(2[0-3])):[0-5][0-9]:[0-5][0-9]/;

    private tripInclusions = [
        query("includeShapes").optional().isBoolean().not().isArray(),
        query("includeStops").optional().isBoolean().not().isArray(),
        query("includeStopTimes").optional().isBoolean().not().isArray(),
        query("includeService").optional().isBoolean().not().isArray(),
        query("includeRoute").optional().isBoolean().not().isArray(),
        query("date").optional().isISO8601().custom(ParamValidatorManager.getDateValidator()).not().isArray(),
    ];

    private stopTimesHandlers = [
        param("stopId").exists().not().isArray(),
        query("from").optional().matches(this.timeRegex).not().isArray(),
        query("to").optional().matches(this.timeRegex).not().isArray(),
        query("date").optional().isISO8601().custom(ParamValidatorManager.getDateValidator()).not().isArray(),
        query("includeStop").optional().isBoolean().not().isArray(),
    ];

    public constructor() {
        super(RouteVersion.v2, "gtfs");
        this.cacheHeaderMiddleware = OutputGatewayContainer.resolve<CacheHeaderMiddleware>(ContainerToken.CacheHeaderMiddleware);
        this.tripModel = models.GTFSTripsModel;
        this.stopModel = models.GTFSStopModel;
        this.stopTimeModel = models.GTFSStopTimesModel;
        this.routeModel = models.GTFSRoutesModel;
        this.shapeModel = models.GTFSShapesModel;
        this.serviceModel = models.GTFSCalendarModel;
        this.initRoutes();
    }

    public GetAllServices = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const data = await this.serviceModel.GetAll({
                date: (req.query.date as string) || undefined,
                limit: Number(req.query.limit) || undefined,
                offset: Number(req.query.offset) || undefined,
            });

            res.status(200).send(data);
        } catch (err) {
            next(err);
        }
    };

    public GetAllStopTimes = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const data = await this.stopTimeModel.GetAll({
                date: (req.query.date as string) || undefined,
                from: (req.query.from as string) || undefined,
                limit: Number(req.query.limit) || undefined,
                offset: Number(req.query.offset) || undefined,
                stop: !!req.query.includeStop,
                stopId: req.params.stopId,
                to: (req.query.to as string) || undefined,
            });

            res.status(200).send(data);
        } catch (err) {
            next(err);
        }
    };

    public GetAllTrips = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const data = await this.tripModel.GetAll({
                date: (req.query.date as string) || undefined,
                limit: Number(req.query.limit) || undefined,
                offset: Number(req.query.offset) || undefined,
                stopId: req.query.stopId as string,
            });

            res.status(200).send(data);
        } catch (err) {
            next(err);
        }
    };

    public GetOneTrip = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const id: string = req.params.id;
            const data = await this.tripModel.GetOne(id, {
                date: (req.query.date as string) || undefined,
                route: !!req.query.includeRoute,
                service: !!req.query.includeService,
                shapes: !!req.query.includeShapes,
                stopTimes: !!req.query.includeStopTimes,
                stops: !!req.query.includeStops,
            });

            if (!data) {
                throw new GeneralError("not_found", "GTFSRouter", undefined, 404);
            }
            res.status(200).send(data);
        } catch (err) {
            next(err);
        }
    };

    public GetAllStops = async (req: Request, res: Response, next: NextFunction) => {
        const names = RopidRouterUtils.convertParamToArray<string>(req.query.names);
        const aswIds = RopidRouterUtils.convertParamToArray<string>(req.query.aswIds);
        const cisIds = RopidRouterUtils.convertParamToArray<string>(req.query.cisIds);
        const gtfsIds = RopidRouterUtils.convertParamToArray<string>(req.query.ids);

        try {
            const coords = await parseCoordinates(req.query.latlng as string, req.query.range as string);
            const data = await this.stopModel.GetAll({
                aswIds,
                cisIds,
                gtfsIds,
                lat: coords.lat,
                limit: Number(req.query.limit) || undefined,
                lng: coords.lng,
                names,
                offset: Number(req.query.offset) || undefined,
                range: coords.range,
            });

            res.status(200).send(data);
        } catch (err) {
            next(err);
        }
    };

    public GetOneStop = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const id: string = req.params.id;
            const data = await this.stopModel.GetOne(id);
            if (!data) {
                throw new GeneralError("not_found", "GTFSRouter", undefined, 404);
            }
            res.status(200).send(data);
        } catch (err) {
            next(err);
        }
    };

    public GetAllRoutes = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const data = await this.routeModel.GetAll({
                limit: Number(req.query.limit) || undefined,
                offset: Number(req.query.offset) || undefined,
            });

            res.status(200).send(data);
        } catch (err) {
            next(err);
        }
    };

    public GetOneRoute = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const id: string = req.params.id;
            const data = await this.routeModel.GetOne(id);
            if (!data) {
                throw new GeneralError("not_found", "GTFSRouter", undefined, 404);
            }
            res.status(200).send(data);
        } catch (err) {
            next(err);
        }
    };

    public GetAllShapes = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const data = await this.shapeModel.GetAll({
                id: req.params.id,
                limit: Number(req.query.limit) || undefined,
                offset: Number(req.query.offset) || undefined,
            });
            if (!data) {
                throw new GeneralError("not_found", "GTFSRouter", undefined, 404);
            }
            res.status(200).send(data);
        } catch (err) {
            next(err);
        }
    };

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     */
    protected initRoutes = (): void => {
        const maxAge = 4 * 60 * 60; // 4 hours
        const staleWhileRevalidate = 60; // 1 minute

        this.initTripsEndpoints(maxAge, staleWhileRevalidate);
        this.initStopsEndpoints(maxAge, staleWhileRevalidate);
        this.initStopTimesEndpoints(maxAge, staleWhileRevalidate);
        this.initRoutesEndpoints(maxAge, staleWhileRevalidate);
        this.initShapesEndpoints(maxAge, staleWhileRevalidate);
        this.initServicesEndpoints(maxAge, staleWhileRevalidate);
    };

    private initRoutesEndpoints = (maxAge: number, staleWhileRevalidate: number): void => {
        this.router.get(
            "/routes",
            pagination,
            checkErrors,
            paginationLimitMiddleware("GTFSRouter"),
            this.cacheHeaderMiddleware.getMiddleware(maxAge, staleWhileRevalidate),
            this.GetAllRoutes
        );
        this.router.get(
            "/routes/:id",
            param("id").exists(),
            checkErrors,
            this.cacheHeaderMiddleware.getMiddleware(maxAge, staleWhileRevalidate),
            this.GetOneRoute
        );
    };

    private initServicesEndpoints = (maxAge: number, staleWhileRevalidate: number): void => {
        this.router.get(
            "/services",
            query("date").optional().isISO8601().custom(ParamValidatorManager.getDateValidator()).not().isArray(),
            pagination,
            checkErrors,
            paginationLimitMiddleware("GTFSRouter"),
            this.cacheHeaderMiddleware.getMiddleware(maxAge, staleWhileRevalidate),
            this.GetAllServices
        );
    };

    private initShapesEndpoints = (maxAge: number, staleWhileRevalidate: number): void => {
        this.router.get(
            "/shapes/:id",
            pagination,
            checkErrors,
            paginationLimitMiddleware("GTFSRouter"),
            param("id").exists(),
            this.cacheHeaderMiddleware.getMiddleware(maxAge, staleWhileRevalidate),
            this.GetAllShapes
        );
    };

    private initTripsEndpoints = (maxAge: number, staleWhileRevalidate: number): void => {
        this.router.get(
            "/trips",
            query("stopId").optional().not().isArray(),
            this.tripInclusions,
            pagination,
            checkErrors,
            paginationLimitMiddleware("GTFSRouter"),
            this.cacheHeaderMiddleware.getMiddleware(maxAge, staleWhileRevalidate),
            this.GetAllTrips
        );
        this.router.get(
            "/trips/:id",
            param("id").exists(),
            this.tripInclusions,
            checkErrors,
            this.cacheHeaderMiddleware.getMiddleware(maxAge, staleWhileRevalidate),
            this.GetOneTrip
        );
    };

    private initStopsEndpoints = (maxAge: number, staleWhileRevalidate: number): void => {
        this.router.get(
            "/stops",
            [
                query("latlng").optional().isLatLong().not().isArray(),
                query("range").optional().isNumeric().not().isArray(),
                query("names").optional().not().isEmpty({ ignore_whitespace: true }),
                query("aswIds").optional().not().isEmpty({ ignore_whitespace: true }),
                query("cisIds").optional().isInt(),
                query("ids").optional().not().isEmpty({ ignore_whitespace: true }),
            ],
            pagination,
            checkErrors,
            paginationLimitMiddleware("GTFSRouter"),
            this.cacheHeaderMiddleware.getMiddleware(maxAge, staleWhileRevalidate),
            this.GetAllStops
        );
        this.router.get(
            "/stops/:id",
            [param("id").exists()],
            checkErrors,
            this.cacheHeaderMiddleware.getMiddleware(maxAge, staleWhileRevalidate),
            this.GetOneStop
        );
    };

    private initStopTimesEndpoints = (maxAge: number, staleWhileRevalidate: number): void => {
        this.router.get(
            "/stoptimes/:stopId",
            this.stopTimesHandlers,
            pagination,
            paginationLimitMiddleware("GTFSRouter"),
            checkErrors,
            (req: Request, res: Response, next: NextFunction) => {
                if (
                    req.query.from &&
                    req.query.to &&
                    moment(req.query.from as string, "H:mm:ss").isAfter(moment(req.query.to as string, "H:mm:ss"))
                ) {
                    throw new GeneralError("Validation error", "GTFSRouter", new Error("'to' cannot be later than 'from'"), 400);
                }
                return next();
            },
            this.cacheHeaderMiddleware.getMiddleware(maxAge, staleWhileRevalidate),
            this.GetAllStopTimes
        );
    };
}

const v2GtfsRouter: AbstractRouter = new V2GTFSRouter();

export { v2GtfsRouter };
