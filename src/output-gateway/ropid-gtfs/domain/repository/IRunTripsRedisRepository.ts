import type { IGtfsRunTripCacheDto } from "#sch/vehicle-positions/redis/interfaces/IGtfsRunTripCacheDto";

export interface IRunTripsRedisRepository {
    getMultipleSchedule(runTuples: string[]): Promise<Array<IGtfsRunTripCacheDto | null>>;
}
