import { v2PidRouter, v3PidRouter } from "#og/pid";
import { v2PublicDeparturesRouter, v2PublicGtfsRouter, v2PublicVPRouter } from "#og/public";
import { v2GtfsRouter } from "#og/ropid-gtfs";
import { v2VehiclepositionsRouter } from "#og/vehicle-positions";

export * as PID from "./pid";
export * as RopidGTFS from "./ropid-gtfs";
export * as VehiclePositions from "./vehicle-positions";

export const routers = [
    //#region Ropid GTFS
    v2GtfsRouter,
    //#endregion

    //#region Vehicle positions
    v2VehiclepositionsRouter,

    //#region PID Departure boards
    v2PidRouter,
    v3PidRouter,
    //#endregion

    //#region Public Endpoints
    v2PublicDeparturesRouter,
    v2PublicGtfsRouter,
    v2PublicVPRouter,
    //#endregion
];
