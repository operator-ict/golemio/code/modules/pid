export class DepartureCalculator {
    /**
     * Calculates the predicted departure time based on the scheduled departure time, arrival time and delay.
     **/
    public static getPredictedDepartureTime = (
        departureTime: Date,
        arrivalTime: Date | null,
        delayInSeconds: number | null
    ): Date => {
        let delayInSecondsWithDefault = delayInSeconds ?? 0;
        let adjustedDepartureTime = new Date(departureTime.getTime() + delayInSecondsWithDefault * 1000);

        if (arrivalTime && delayInSecondsWithDefault > 0) {
            const dwellInSeconds = (departureTime.getTime() - arrivalTime.getTime()) / 1000;
            const minDwellInSeconds = Math.min(delayInSecondsWithDefault, dwellInSeconds);

            adjustedDepartureTime.setTime(adjustedDepartureTime.getTime() - minDwellInSeconds * 1000);
        }

        return adjustedDepartureTime;
    };

    /**
     * Calculates the number of minutes until the predicted departure time.
     **/
    public static getDepartureMinutes = (predictedDepartureTime: Date): number => {
        return Math.round((predictedDepartureTime.getTime() - new Date().getTime()) / (60 * 1000));
    };
}
