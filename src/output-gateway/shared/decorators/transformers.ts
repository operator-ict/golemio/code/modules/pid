import { Transform } from "@golemio/core/dist/shared/class-transformer";

export const TransformArray = () => Transform(({ value }) => (Array.isArray(value) ? value : [value]));
export const TransformInteger = () => Transform(({ value }) => parseInt(value, 10));
export const TransformBoolean = () => Transform(({ value }) => value === "true");
