import moment from "@golemio/core/dist/shared/moment-timezone";

export const ValidationArrays = {
    departureBoards: {
        departureMode: ["departures", "arrivals", "mixed"],
        order: ["real", "timetable"],
        filter: [
            "none",
            "routeOnce",
            "routeHeadingOnce",
            "routeOnceFill",
            "routeHeadingOnceFill",
            "routeHeadingOnceNoGap",
            "routeHeadingOnceNoGapFill",
        ],
        skip: ["untracked", "missing", "canceled", "atStop" /*, "onInfotext"*/],
    },
    // TODO replace moment.tz.names() with Intl.supportedValuesOf("timeZone") after migrating to Node 18
    preferredTimezone: [
        ...moment.tz.names(),
        ...moment.tz
            .names()
            // concats only those names that have character /
            .filter((name) => name.match(/\//g))
            // that is replaced with _
            .map((name) => name.replace(/\//g, "_")),
    ],
};
