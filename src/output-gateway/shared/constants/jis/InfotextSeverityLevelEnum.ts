export enum InfotextSeverityLevel {
    Info = "INFO",
    Warning = "WARNING",
    Severe = "SEVERE",
}
