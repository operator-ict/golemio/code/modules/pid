import { log } from "@golemio/core/dist/output-gateway/Logger";
import { config } from "@golemio/core/dist/output-gateway/config";
import { ClassConstructor, plainToInstance } from "@golemio/core/dist/shared/class-transformer";
import moment from "@golemio/core/dist/shared/moment-timezone";
import { ParsedQs } from "@golemio/core/dist/shared/qs";

export class RopidRouterUtils {
    public static TIMEZONE = "Europe/Prague";

    /**
     * Convert plain object to DTO class instance
     */
    public static mapObjectToDTOInstance = <T>(dto: ClassConstructor<T>, data: any) => {
        return plainToInstance(dto, data, { exposeDefaultValues: true });
    };

    /**
     * Format to ISO 8601 (without milliseconds)
     */
    public static formatTimestamp = (dateObject: Date | null, preferredTimezone = RopidRouterUtils.TIMEZONE): string | null => {
        if (!dateObject) return null;

        try {
            const datetime = moment(dateObject).tz(preferredTimezone).format();

            if (datetime === "Invalid date") {
                log.error("RopidRouterUtils.formatTimestamp Date conversion error");
                return null;
            }

            return datetime;
        } catch (err) {
            log.error("RopidRouterUtils.formatTimestamp Date conversion error", err);
            return null;
        }
    };

    /**
     * Validate and return timezone (it is possible to use _ symbol instead of URL encoded / symbol)
     */
    public static getPreferredTimezone = (preferredTimezone: ParsedQs[string]): string => {
        const timezone =
            typeof preferredTimezone === "string"
                ? preferredTimezone.replace(/_/s, "/")
                : config.vehiclePositions.defaultTimezone;

        return timezone;
    };

    /**
     * Convert query parameter to array  string | ParsedQs | (string | ParsedQs)[] | undefined
     */
    public static convertParamToArray = <T>(param: ParsedQs | T | Array<T | ParsedQs> | undefined): T[] => {
        if (!param) return [];

        if (Array.isArray(param)) {
            return param as T[];
        }

        return [param as T];
    };
}
